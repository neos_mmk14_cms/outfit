Exception #1180547966 in line 280 of /Applications/XAMPP/xamppfiles/htdocs/outfit/Data/Temporary/Development/Cache/Code/Flow_Object_Classes/TYPO3_TypoScript_Core_Parser.php: Syntax error in line 96. (Usage:)

29 TYPO3\TypoScript\Core\Parser_Original::parseTypoScriptLine("	Usage:")
28 TYPO3\TypoScript\Core\Parser_Original::parse("prototype(TYPO3.TypoScript:Array).@class = 'TYPO3\\TypoScript\\TypoScriptObjects\\ArrayImplementation'
prototype(TYPO3.TypoScript:RawArray).@class = 'TYPO3\\TypoScript\\TypoScriptObjects\\RawArrayImplementation'
prototype(TYPO3.TypoScript:Template).@class = 'TYPO3\\TypoScript\\TypoScriptObjects\\TemplateImplementation'
prototype(TYPO3.TypoScript:Case).@class = 'TYPO3\\TypoScript\\TypoScriptObjects\\CaseImplementation'
prototype(TYPO3.TypoScript:Matcher).@class = 'TYPO3\\TypoScript\\TypoScriptObjects\\MatcherImplementation'
prototype(TYPO3.TypoScript:Value).@class = 'TYPO3\\TypoScript\\TypoScriptObjects\\ValueImplementation'
prototype(TYPO3.TypoScript:Debug).@class = 'TYPO3\\TypoScript\\TypoScriptObjects\\DebugImplementation'
prototype(TYPO3.TypoScript:Collection) {
	@class = 'TYPO3\\TypoScript\\TypoScriptObjects\\CollectionImplementation'
	itemName = 'item'
}

# Render an HTTP response header
#
prototype(TYPO3.TypoScript:Http.ResponseHead) {
	@class = 'TYPO3\\TypoScript\\TypoScriptObjects\\Http\\ResponseHeadImplementation'
	headers = TYPO3.TypoScript:RawArray
}

# Render an HTTP message (response)
#
# This is a convenient base prototype for rendering documents.
#
# Usage:
# page = TYPO3.TypoScript:Http.Message {
#   httpResponseHead {
#     statusCode = 404
#     headers.Content-Type = 'application/json'
#   }
# }
#
#
prototype(TYPO3.TypoScript:Http.Message) < prototype(TYPO3.TypoScript:Array) {
	httpResponseHead = TYPO3.TypoScript:Http.ResponseHead
	httpResponseHead.@position = 'start 1000'
}

# Renders attributes of a HTML tag
#
# Usage:
# attributes = TYPO3.TypoScript:Attributes {
#   foo = 'bar'
#   class = TYPO3.TypoScript:RawArray {
#     class1 = 'class1'
#     class2 = 'class2'
#   }
# }
#
prototype(TYPO3.TypoScript:Attributes) {
	@class = 'TYPO3\\TypoScript\\TypoScriptObjects\\AttributesImplementation'
}

# Renders an HTML tag
#
# Usage:
# tag = TYPO3.TypoScript:Attributes {
#   tagName = 'h1'
#   attributes = {
#     class = 'some-class'
#   }
# }
#
prototype(TYPO3.TypoScript:Tag) {
	@class = 'TYPO3\\TypoScript\\TypoScriptObjects\\TagImplementation'
	attributes = TYPO3.TypoScript:Attributes
	omitClosingTag = FALSE
	selfClosingTag = FALSE
}

# Renders an URI pointing to a controller/action
#
# Usage:
# uri = TYPO3.TypoScript:UriBuilder {
#   package = 'Some.Package'
#   controller = 'Standard'
#   action = 'index'
# }
#
prototype(TYPO3.TypoScript:UriBuilder) {
	@class = 'TYPO3\\TypoScript\\TypoScriptObjects\\UriBuilderImplementation'
	additionalParams = TYPO3.TypoScript:RawArray
	arguments = TYPO3.TypoScript:RawArray
	argumentsToBeExcludedFromQueryString = TYPO3.TypoScript:RawArray

	@exceptionHandler = 'TYPO3\\TypoScript\\Core\\ExceptionHandlers\\AbsorbingHandler'
}

# Renders an URI pointing to a resource
#
# Usage:
# fileUri = TYPO3.TypoScript:ResourceUri {
#   path = 'resource://Some.Package/Public/Images/SomeImage.png'
# }

	Usage:
	fileUri = TYPO3.TypoScript:ResourceUri {
	path = 'resource://Neos.Outfit/Public/template_neos1/work.html'
	}
#
prototype(TYPO3.TypoScript:ResourceUri) {
	@class = 'TYPO3\\TypoScript\\TypoScriptObjects\\ResourceUriImplementation'
	localize = TRUE

	@exceptionHandler = 'TYPO3\\TypoScript\\Core\\ExceptionHandlers\\AbsorbingHandler'
}

# These are globally applied cache identifiers.
# If you don't make @cache.entryIdentifiers another prototype (like a TYPO3.TypoScript:RawArray)
# they will be rendered as this prototype, which means everything in here is added to ALL cached
# TypoScript parts.
# The baseUri contains the protocol (http/https), domain and port.
#
# WARNING: Do not delete this prototype, it is used inside of \TYPO3\TypoScript\Core\Cache\RuntimeContentCache::buildCacheIdentifierValues() as a fallback.
#
prototype(TYPO3.TypoScript:GlobalCacheIdentifiers) < prototype(TYPO3.TypoScript:RawArray) {
	format = ${request.format}
	baseUri = ${String.toString(request.httpRequest.baseUri)}
}
", "resource://TYPO3.TypoScript/Private/TypoScript/Root.ts2", array|1|, FALSE)
27 TYPO3\TypoScript\Core\Parser_Original::parseInclude("resource://TYPO3.TypoScript/Private/TypoScript/Root.ts2")
26 TYPO3\TypoScript\Core\Parser_Original::parseDeclaration("include: resource://TYPO3.TypoScript/Private/TypoScript/Root.ts2")
25 TYPO3\TypoScript\Core\Parser_Original::parseTypoScriptLine("include: resource://TYPO3.TypoScript/Private/TypoScript/Root.ts2")
24 TYPO3\TypoScript\Core\Parser_Original::parse("prototype(TYPO3.Neos:Shortcut) < prototype(TYPO3.Neos:Document) {
	templatePath = 'resource://TYPO3.Neos/Private/Templates/NodeTypes/Shortcut.html'
	title = ${q(node).property("title")}
	uriPathSegment = ${q(node).property("uriPathSegment")}
	targetMode = ${q(node).property("targetMode")}
	target = ${q(node).property("target")}
}
prototype(TYPO3.Neos:PluginView) < prototype(TYPO3.Neos:Content) {
	templatePath = 'resource://TYPO3.Neos/Private/Templates/NodeTypes/PluginView.html'
	plugin = ${q(node).property("plugin")}
	view = ${q(node).property("view")}
}
prototype(TYPO3.Neos:ContentCollection) < prototype(TYPO3.TypoScript:Template) {
	templatePath = 'resource://TYPO3.Neos/Private/Templates/NodeTypes/ContentCollection.html'
}
prototype(TYPO3.Neos:FallbackNode) < prototype(TYPO3.TypoScript:Template) {
	templatePath = 'resource://TYPO3.Neos/Private/Templates/NodeTypes/FallbackNode.html'
}
prototype(TYPO3.Neos.NodeTypes:Headline) < prototype(TYPO3.Neos:Content) {
	templatePath = 'resource://TYPO3.Neos.NodeTypes/Private/Templates/NodeTypes/Headline.html'
	title = ${q(node).property("title")}
	title.@process.convertUris = TYPO3.Neos:ConvertUris
}
prototype(TYPO3.Neos.NodeTypes:Text) < prototype(TYPO3.Neos:Content) {
	templatePath = 'resource://TYPO3.Neos.NodeTypes/Private/Templates/NodeTypes/Text.html'
	text = ${q(node).property("text")}
	text.@process.convertUris = TYPO3.Neos:ConvertUris
}
prototype(TYPO3.Neos.NodeTypes:Image) < prototype(TYPO3.Neos:Content) {
	templatePath = 'resource://TYPO3.Neos.NodeTypes/Private/Templates/NodeTypes/Image.html'
	image = ${q(node).property("image")}
	alternativeText = ${q(node).property("alternativeText")}
	title = ${q(node).property("title")}
	link = ${q(node).property("link")}
	hasCaption = ${q(node).property("hasCaption")}
	caption = ${q(node).property("caption")}
	caption.@process.convertUris = TYPO3.Neos:ConvertUris
	alignment = ${q(node).property("alignment")}
}
prototype(TYPO3.Neos.NodeTypes:TextWithImage) < prototype(TYPO3.Neos:Content) {
	templatePath = 'resource://TYPO3.Neos.NodeTypes/Private/Templates/NodeTypes/TextWithImage.html'
	text = ${q(node).property("text")}
	text.@process.convertUris = TYPO3.Neos:ConvertUris
	image = ${q(node).property("image")}
	alternativeText = ${q(node).property("alternativeText")}
	title = ${q(node).property("title")}
	link = ${q(node).property("link")}
	hasCaption = ${q(node).property("hasCaption")}
	caption = ${q(node).property("caption")}
	caption.@process.convertUris = TYPO3.Neos:ConvertUris
	alignment = ${q(node).property("alignment")}
}
prototype(TYPO3.Neos.NodeTypes:Html) < prototype(TYPO3.Neos:Content) {
	templatePath = 'resource://TYPO3.Neos.NodeTypes/Private/Templates/NodeTypes/Html.html'
	source = ${q(node).property("source")}
}
prototype(TYPO3.Neos.NodeTypes:Menu) < prototype(TYPO3.Neos:Content) {
	templatePath = 'resource://TYPO3.Neos.NodeTypes/Private/Templates/NodeTypes/Menu.html'
	startLevel = ${q(node).property("startLevel")}
	selection = ${q(node).property("selection")}
	startingPoint = ${q(node).property("startingPoint")}
	maximumLevels = ${q(node).property("maximumLevels")}
}
prototype(TYPO3.Neos.NodeTypes:TwoColumn) < prototype(TYPO3.Neos:Content) {
	templatePath = 'resource://TYPO3.Neos.NodeTypes/Private/Templates/NodeTypes/TwoColumn.html'
	layout = ${q(node).property("layout")}
}
prototype(TYPO3.Neos.NodeTypes:ThreeColumn) < prototype(TYPO3.Neos:Content) {
	templatePath = 'resource://TYPO3.Neos.NodeTypes/Private/Templates/NodeTypes/ThreeColumn.html'
	layout = ${q(node).property("layout")}
}
prototype(TYPO3.Neos.NodeTypes:FourColumn) < prototype(TYPO3.Neos:Content) {
	templatePath = 'resource://TYPO3.Neos.NodeTypes/Private/Templates/NodeTypes/FourColumn.html'
	layout = ${q(node).property("layout")}
}
prototype(TYPO3.Neos.NodeTypes:Form) < prototype(TYPO3.Neos:Content) {
	templatePath = 'resource://TYPO3.Neos.NodeTypes/Private/Templates/NodeTypes/Form.html'
	formIdentifier = ${q(node).property("formIdentifier")}
}
prototype(TYPO3.Neos.NodeTypes:AssetList) < prototype(TYPO3.Neos:Content) {
	templatePath = 'resource://TYPO3.Neos.NodeTypes/Private/Templates/NodeTypes/AssetList.html'
	assets = ${q(node).property("assets")}
}
prototype(TYPO3.Neos.NodeTypes:ContentReferences) < prototype(TYPO3.Neos:Content) {
	templatePath = 'resource://TYPO3.Neos.NodeTypes/Private/Templates/NodeTypes/ContentReferences.html'
	references = ${q(node).property("references")}
}
prototype(TYPO3.Neos.NodeTypes:Page) < prototype(TYPO3.Neos:Document) {
	templatePath = 'resource://TYPO3.Neos.NodeTypes/Private/Templates/NodeTypes/Page.html'
	titleOverride = ${q(node).property("titleOverride")}
	metaDescription = ${q(node).property("metaDescription")}
	metaKeywords = ${q(node).property("metaKeywords")}
	metaRobotsNoindex = ${q(node).property("metaRobotsNoindex")}
	metaRobotsNofollow = ${q(node).property("metaRobotsNofollow")}
	twitterCardType = ${q(node).property("twitterCardType")}
	twitterCardCreator = ${q(node).property("twitterCardCreator")}
	twitterCardTitle = ${q(node).property("twitterCardTitle")}
	twitterCardDescription = ${q(node).property("twitterCardDescription")}
	twitterCardImage = ${q(node).property("twitterCardImage")}
	canonicalLink = ${q(node).property("canonicalLink")}
	openGraphType = ${q(node).property("openGraphType")}
	openGraphTitle = ${q(node).property("openGraphTitle")}
	openGraphDescription = ${q(node).property("openGraphDescription")}
	openGraphImage = ${q(node).property("openGraphImage")}
	xmlSitemapChangeFrequency = ${q(node).property("xmlSitemapChangeFrequency")}
	xmlSitemapPriority = ${q(node).property("xmlSitemapPriority")}
	title = ${q(node).property("title")}
	uriPathSegment = ${q(node).property("uriPathSegment")}
	layout = ${q(node).property("layout")}
	subpageLayout = ${q(node).property("subpageLayout")}
}
prototype(Flowpack.Neos.FrontendLogin:LoginForm) < prototype(TYPO3.Neos:Content) {
	templatePath = 'resource://Flowpack.Neos.FrontendLogin/Private/Templates/NodeTypes/LoginForm.html'
	redirectAfterLogin = ${q(node).property("redirectAfterLogin")}
	redirectAfterLogout = ${q(node).property("redirectAfterLogout")}
}

include: resource://TYPO3.TypoScript/Private/TypoScript/Root.ts2
include: resource://TYPO3.Neos/Private/TypoScript/Root.ts2
include: resource://TYPO3.Neos.NodeTypes/Private/TypoScript/Root.ts2
include: resource://Flowpack.Neos.FrontendLogin/Private/TypoScript/Root.ts2
include: resource://TYPO3.Neos.Seo/Private/TypoScript/Root.ts2



/**
 * Root TypoScript template for the Demonstration site site
 */
page = TYPO3.Neos:Page {
	head {
		stylesheets.site = TYPO3.TypoScript:Template {
			templatePath = 'resource://MH.DemoSite/Private/Templates/Page/Default.html'
			sectionName = 'stylesheets'
		}

		javascripts.site = TYPO3.TypoScript:Template {
			templatePath = 'resource://MH.DemoSite/Private/Templates/Page/Default.html'
			sectionName = 'headScripts'
		}
	}

	body {
		templatePath = 'resource://MH.DemoSite/Private/Templates/Page/Default.html'
		sectionName = 'body'
		parts {
			menu = TYPO3.Neos:Menu
		}

		// These are your content areas, you can define as many as you want, just name them and the nodePath.
		content {
			// The default content section
			main = TYPO3.Neos:PrimaryContent {
				nodePath = 'main'
			}
		}

		javascripts.site = TYPO3.TypoScript:Template {
			templatePath = 'resource://MH.DemoSite/Private/Templates/Page/Default.html'
			sectionName = 'bodyScripts'
		}
	}
}


", "resource://MH.DemoSite/Private/TypoScript/Root.ts2")
23 TYPO3\Neos\Domain\Service\TypoScriptService_Original::getMergedTypoScriptObjectTree(TYPO3\TYPO3CR\Domain\Model\Node)
22 TYPO3\Neos\Domain\Service\TypoScriptService_Original::createRuntime(TYPO3\TYPO3CR\Domain\Model\Node, TYPO3\Flow\Mvc\Controller\ControllerContext)
21 TYPO3\Neos\View\TypoScriptView_Original::getTypoScriptRuntime(TYPO3\TYPO3CR\Domain\Model\Node)
20 TYPO3\Neos\View\TypoScriptView_Original::canRenderWithNodeAndPath()
19 TYPO3\Neos\Controller\Frontend\NodeController_Original::showAction(TYPO3\TYPO3CR\Domain\Model\Node)
18 TYPO3\Neos\Controller\Frontend\NodeController::showAction(TYPO3\TYPO3CR\Domain\Model\Node)
17 call_user_func_array(array|2|, array|1|)
16 TYPO3\Neos\Controller\Frontend\NodeController::Flow_Aop_Proxy_invokeJoinPoint(TYPO3\Flow\Aop\JoinPoint)
15 TYPO3\Flow\Aop\Advice\AdviceChain::proceed(TYPO3\Flow\Aop\JoinPoint)
14 TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect_Original::enforcePolicy(TYPO3\Flow\Aop\JoinPoint)
13 TYPO3\Flow\Aop\Advice\AroundAdvice::invoke(TYPO3\Flow\Aop\JoinPoint)
12 TYPO3\Flow\Aop\Advice\AdviceChain::proceed(TYPO3\Flow\Aop\JoinPoint)
11 TYPO3\Neos\Controller\Frontend\NodeController::showAction(TYPO3\TYPO3CR\Domain\Model\Node)
10 call_user_func_array(array|2|, array|1|)
9 TYPO3\Flow\Mvc\Controller\ActionController_Original::callActionMethod()
8 TYPO3\Flow\Mvc\Controller\ActionController_Original::processRequest(TYPO3\Flow\Mvc\ActionRequest, TYPO3\Flow\Http\Response)
7 TYPO3\Flow\Mvc\Dispatcher_Original::initiateDispatchLoop(TYPO3\Flow\Mvc\ActionRequest, TYPO3\Flow\Http\Response)
6 TYPO3\Flow\Mvc\Dispatcher_Original::dispatch(TYPO3\Flow\Mvc\ActionRequest, TYPO3\Flow\Http\Response)
5 TYPO3\Flow\Mvc\DispatchComponent_Original::handle(TYPO3\Flow\Http\Component\ComponentContext)
4 TYPO3\Flow\Http\Component\ComponentChain_Original::handle(TYPO3\Flow\Http\Component\ComponentContext)
3 TYPO3\Flow\Http\Component\ComponentChain_Original::handle(TYPO3\Flow\Http\Component\ComponentContext)
2 TYPO3\Flow\Http\RequestHandler::handleRequest()
1 TYPO3\Flow\Core\Bootstrap::run()

HTTP REQUEST:
GET /@user-Julia HTTP/1.1
Host: 127.0.0.1:8081
Connection: keep-alive
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Referer: http://127.0.0.1:8081/neos/administration/configuration/?moduleArguments%5Btype%5D=Objects
Accept-Encoding: gzip, deflate, sdch, br
Accept-Language: de-DE,de;q=0.8,en-US;q=0.6,en;q=0.4
Cache-Control: max-age=0



HTTP RESPONSE:
[response was empty]

PHP PROCESS:
Inode: 21308954
PID: 15725
UID: 0
GID: 80
User: root
