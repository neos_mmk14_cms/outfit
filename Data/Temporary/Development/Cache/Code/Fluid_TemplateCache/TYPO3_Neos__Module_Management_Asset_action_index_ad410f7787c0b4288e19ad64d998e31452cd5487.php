<?php class FluidCache_TYPO3_Neos__Module_Management_Asset_action_index_ad410f7787c0b4288e19ad64d998e31452cd5487 extends \TYPO3\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// TODO
	return new \TYPO3\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;

return 'Default';
}
public function hasLayout() {
return TRUE;
}

/**
 * section Title
 */
public function section_768e0c1c69573fb588f61f1308a015c11468e05f(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;

return 'Index view';
}
/**
 * section Options
 */
public function section_6bf5da9c080bee3a8142586c412aa39971137eee(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output0 = '';

$output0 .= '
	<div class="neos-file-options">
		<span class="count">
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments2 = array();
$arguments2['id'] = 'media.search.itemCount';
// Rendering Array
$array3 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments4 = array();
$arguments4['subject'] = NULL;
$renderChildrenClosure5 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assets', $renderingContext);
};
$viewHelper6 = $self->getViewHelper('$viewHelper6', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper6->setArguments($arguments4);
$viewHelper6->setRenderingContext($renderingContext);
$viewHelper6->setRenderChildrenClosure($renderChildrenClosure5);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$array3['0'] = $viewHelper6->initializeArgumentsAndRender();
$arguments2['arguments'] = $array3;
$arguments2['source'] = 'Modules';
$arguments2['package'] = 'TYPO3.Neos';
$arguments2['value'] = NULL;
$arguments2['quantity'] = NULL;
$arguments2['languageIdentifier'] = NULL;
$renderChildrenClosure7 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper8 = $self->getViewHelper('$viewHelper8', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper8->setArguments($arguments2);
$viewHelper8->setRenderingContext($renderingContext);
$viewHelper8->setRenderChildrenClosure($renderChildrenClosure7);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1['value'] = $viewHelper8->initializeArgumentsAndRender();
$arguments1['keepQuotes'] = false;
$arguments1['encoding'] = 'UTF-8';
$arguments1['doubleEncode'] = true;
$renderChildrenClosure9 = function() use ($renderingContext, $self) {
return NULL;
};
$value10 = ($arguments1['value'] !== NULL ? $arguments1['value'] : $renderChildrenClosure9());

$output0 .= !is_string($value10) && !(is_object($value10) && method_exists($value10, '__toString')) ? $value10 : htmlspecialchars($value10, ($arguments1['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1['encoding'], $arguments1['doubleEncode']);

$output0 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments11 = array();
// Rendering Boolean node
$arguments11['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'searchTerm', $renderingContext));
$arguments11['then'] = NULL;
$arguments11['else'] = NULL;
$renderChildrenClosure12 = function() use ($renderingContext, $self) {
$output13 = '';

$output13 .= ' ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments14 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments15 = array();
$arguments15['id'] = 'media.search.foundMatches';
// Rendering Array
$array16 = array();
$array16['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'searchTerm', $renderingContext);
$arguments15['arguments'] = $array16;
$arguments15['source'] = 'Modules';
$arguments15['package'] = 'TYPO3.Neos';
$arguments15['value'] = NULL;
$arguments15['quantity'] = NULL;
$arguments15['languageIdentifier'] = NULL;
$renderChildrenClosure17 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper18 = $self->getViewHelper('$viewHelper18', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper18->setArguments($arguments15);
$viewHelper18->setRenderingContext($renderingContext);
$viewHelper18->setRenderChildrenClosure($renderChildrenClosure17);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments14['value'] = $viewHelper18->initializeArgumentsAndRender();
$arguments14['keepQuotes'] = false;
$arguments14['encoding'] = 'UTF-8';
$arguments14['doubleEncode'] = true;
$renderChildrenClosure19 = function() use ($renderingContext, $self) {
return NULL;
};
$value20 = ($arguments14['value'] !== NULL ? $arguments14['value'] : $renderChildrenClosure19());

$output13 .= !is_string($value20) && !(is_object($value20) && method_exists($value20, '__toString')) ? $value20 : htmlspecialchars($value20, ($arguments14['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments14['encoding'], $arguments14['doubleEncode']);
return $output13;
};
$viewHelper21 = $self->getViewHelper('$viewHelper21', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper21->setArguments($arguments11);
$viewHelper21->setRenderingContext($renderingContext);
$viewHelper21->setRenderChildrenClosure($renderChildrenClosure12);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output0 .= $viewHelper21->initializeArgumentsAndRender();

$output0 .= '
		</span>
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments22 = array();
$arguments22['action'] = 'new';
$arguments22['additionalAttributes'] = NULL;
$arguments22['data'] = NULL;
$arguments22['arguments'] = array (
);
$arguments22['controller'] = NULL;
$arguments22['package'] = NULL;
$arguments22['subpackage'] = NULL;
$arguments22['section'] = '';
$arguments22['format'] = '';
$arguments22['additionalParams'] = array (
);
$arguments22['addQueryString'] = false;
$arguments22['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments22['useParentRequest'] = false;
$arguments22['absolute'] = true;
$arguments22['class'] = NULL;
$arguments22['dir'] = NULL;
$arguments22['id'] = NULL;
$arguments22['lang'] = NULL;
$arguments22['style'] = NULL;
$arguments22['title'] = NULL;
$arguments22['accesskey'] = NULL;
$arguments22['tabindex'] = NULL;
$arguments22['onclick'] = NULL;
$arguments22['name'] = NULL;
$arguments22['rel'] = NULL;
$arguments22['rev'] = NULL;
$arguments22['target'] = NULL;
$renderChildrenClosure23 = function() use ($renderingContext, $self) {
$output24 = '';

$output24 .= '<i class="icon-upload"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments25 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments26 = array();
$arguments26['id'] = 'media.upload';
$arguments26['source'] = 'Modules';
$arguments26['package'] = 'TYPO3.Neos';
$arguments26['value'] = NULL;
$arguments26['arguments'] = array (
);
$arguments26['quantity'] = NULL;
$arguments26['languageIdentifier'] = NULL;
$renderChildrenClosure27 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper28 = $self->getViewHelper('$viewHelper28', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper28->setArguments($arguments26);
$viewHelper28->setRenderingContext($renderingContext);
$viewHelper28->setRenderChildrenClosure($renderChildrenClosure27);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments25['value'] = $viewHelper28->initializeArgumentsAndRender();
$arguments25['keepQuotes'] = false;
$arguments25['encoding'] = 'UTF-8';
$arguments25['doubleEncode'] = true;
$renderChildrenClosure29 = function() use ($renderingContext, $self) {
return NULL;
};
$value30 = ($arguments25['value'] !== NULL ? $arguments25['value'] : $renderChildrenClosure29());

$output24 .= !is_string($value30) && !(is_object($value30) && method_exists($value30, '__toString')) ? $value30 : htmlspecialchars($value30, ($arguments25['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments25['encoding'], $arguments25['doubleEncode']);
return $output24;
};
$viewHelper31 = $self->getViewHelper('$viewHelper31', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper31->setArguments($arguments22);
$viewHelper31->setRenderingContext($renderingContext);
$viewHelper31->setRenderChildrenClosure($renderChildrenClosure23);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output0 .= $viewHelper31->initializeArgumentsAndRender();

$output0 .= '
	</div>
	<div class="neos-view-options">
		<div class="neos-dropdown" id="neos-filter-menu">
			<span title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments32 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments33 = array();
$arguments33['id'] = 'media.filterOptions';
$arguments33['source'] = 'Modules';
$arguments33['package'] = 'TYPO3.Neos';
$arguments33['value'] = NULL;
$arguments33['arguments'] = array (
);
$arguments33['quantity'] = NULL;
$arguments33['languageIdentifier'] = NULL;
$renderChildrenClosure34 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper35 = $self->getViewHelper('$viewHelper35', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper35->setArguments($arguments33);
$viewHelper35->setRenderingContext($renderingContext);
$viewHelper35->setRenderChildrenClosure($renderChildrenClosure34);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments32['value'] = $viewHelper35->initializeArgumentsAndRender();
$arguments32['keepQuotes'] = false;
$arguments32['encoding'] = 'UTF-8';
$arguments32['doubleEncode'] = true;
$renderChildrenClosure36 = function() use ($renderingContext, $self) {
return NULL;
};
$value37 = ($arguments32['value'] !== NULL ? $arguments32['value'] : $renderChildrenClosure36());

$output0 .= !is_string($value37) && !(is_object($value37) && method_exists($value37, '__toString')) ? $value37 : htmlspecialchars($value37, ($arguments32['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments32['encoding'], $arguments32['doubleEncode']);

$output0 .= '" data-neos-toggle="tooltip">
				<a class="dropdown-toggle';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments38 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments39 = array();
$arguments39['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'filter', $renderingContext);
$arguments39['keepQuotes'] = false;
$arguments39['encoding'] = 'UTF-8';
$arguments39['doubleEncode'] = true;
$renderChildrenClosure40 = function() use ($renderingContext, $self) {
return NULL;
};
$value41 = ($arguments39['value'] !== NULL ? $arguments39['value'] : $renderChildrenClosure40());
$arguments38['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('!=', !is_string($value41) && !(is_object($value41) && method_exists($value41, '__toString')) ? $value41 : htmlspecialchars($value41, ($arguments39['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments39['encoding'], $arguments39['doubleEncode']), 'All');
$arguments38['then'] = ' neos-active';
$arguments38['else'] = NULL;
$renderChildrenClosure42 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper43 = $self->getViewHelper('$viewHelper43', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper43->setArguments($arguments38);
$viewHelper43->setRenderingContext($renderingContext);
$viewHelper43->setRenderChildrenClosure($renderChildrenClosure42);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output0 .= $viewHelper43->initializeArgumentsAndRender();

$output0 .= '" href="#" data-neos-toggle="dropdown" data-target="#neos-filter-menu">
					<i class="icon-filter"></i>
				</a>
			</span>
			<ul class="neos-dropdown-menu neos-pull-right" role="menu">
				<li>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments44 = array();
$arguments44['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments45 = array();
$arguments45['id'] = 'media.filter.title.all';
$arguments45['source'] = 'Modules';
$arguments45['package'] = 'TYPO3.Neos';
$arguments45['value'] = NULL;
$arguments45['arguments'] = array (
);
$arguments45['quantity'] = NULL;
$arguments45['languageIdentifier'] = NULL;
$renderChildrenClosure46 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper47 = $self->getViewHelper('$viewHelper47', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper47->setArguments($arguments45);
$viewHelper47->setRenderingContext($renderingContext);
$viewHelper47->setRenderChildrenClosure($renderChildrenClosure46);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments44['title'] = $viewHelper47->initializeArgumentsAndRender();
// Rendering Array
$array48 = array();
$array48['filter'] = 'All';
$arguments44['arguments'] = $array48;
// Rendering Boolean node
$arguments44['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments49 = array();
// Rendering Boolean node
$arguments49['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'filter', $renderingContext), 'All');
$arguments49['then'] = 'neos-active';
$arguments49['else'] = NULL;
$renderChildrenClosure50 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper51 = $self->getViewHelper('$viewHelper51', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper51->setArguments($arguments49);
$viewHelper51->setRenderingContext($renderingContext);
$viewHelper51->setRenderChildrenClosure($renderChildrenClosure50);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments44['class'] = $viewHelper51->initializeArgumentsAndRender();
$arguments44['additionalAttributes'] = NULL;
$arguments44['data'] = NULL;
$arguments44['controller'] = NULL;
$arguments44['package'] = NULL;
$arguments44['subpackage'] = NULL;
$arguments44['section'] = '';
$arguments44['format'] = '';
$arguments44['additionalParams'] = array (
);
$arguments44['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments44['useParentRequest'] = false;
$arguments44['absolute'] = true;
$arguments44['dir'] = NULL;
$arguments44['id'] = NULL;
$arguments44['lang'] = NULL;
$arguments44['style'] = NULL;
$arguments44['accesskey'] = NULL;
$arguments44['tabindex'] = NULL;
$arguments44['onclick'] = NULL;
$arguments44['name'] = NULL;
$arguments44['rel'] = NULL;
$arguments44['rev'] = NULL;
$arguments44['target'] = NULL;
$renderChildrenClosure52 = function() use ($renderingContext, $self) {
$output53 = '';

$output53 .= '<i class="icon-filter"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments54 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments55 = array();
$arguments55['id'] = 'media.filter.all';
$arguments55['source'] = 'Modules';
$arguments55['package'] = 'TYPO3.Neos';
$arguments55['value'] = NULL;
$arguments55['arguments'] = array (
);
$arguments55['quantity'] = NULL;
$arguments55['languageIdentifier'] = NULL;
$renderChildrenClosure56 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper57 = $self->getViewHelper('$viewHelper57', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper57->setArguments($arguments55);
$viewHelper57->setRenderingContext($renderingContext);
$viewHelper57->setRenderChildrenClosure($renderChildrenClosure56);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments54['value'] = $viewHelper57->initializeArgumentsAndRender();
$arguments54['keepQuotes'] = false;
$arguments54['encoding'] = 'UTF-8';
$arguments54['doubleEncode'] = true;
$renderChildrenClosure58 = function() use ($renderingContext, $self) {
return NULL;
};
$value59 = ($arguments54['value'] !== NULL ? $arguments54['value'] : $renderChildrenClosure58());

$output53 .= !is_string($value59) && !(is_object($value59) && method_exists($value59, '__toString')) ? $value59 : htmlspecialchars($value59, ($arguments54['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments54['encoding'], $arguments54['doubleEncode']);
return $output53;
};
$viewHelper60 = $self->getViewHelper('$viewHelper60', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper60->setArguments($arguments44);
$viewHelper60->setRenderingContext($renderingContext);
$viewHelper60->setRenderChildrenClosure($renderChildrenClosure52);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output0 .= $viewHelper60->initializeArgumentsAndRender();

$output0 .= '
				</li>
				<li>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments61 = array();
$arguments61['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments62 = array();
$arguments62['id'] = 'media.filter.title.images';
$arguments62['source'] = 'Modules';
$arguments62['package'] = 'TYPO3.Neos';
$arguments62['value'] = NULL;
$arguments62['arguments'] = array (
);
$arguments62['quantity'] = NULL;
$arguments62['languageIdentifier'] = NULL;
$renderChildrenClosure63 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper64 = $self->getViewHelper('$viewHelper64', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper64->setArguments($arguments62);
$viewHelper64->setRenderingContext($renderingContext);
$viewHelper64->setRenderChildrenClosure($renderChildrenClosure63);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments61['title'] = $viewHelper64->initializeArgumentsAndRender();
// Rendering Array
$array65 = array();
$array65['filter'] = 'Image';
$arguments61['arguments'] = $array65;
// Rendering Boolean node
$arguments61['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments66 = array();
// Rendering Boolean node
$arguments66['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'filter', $renderingContext), 'Image');
$arguments66['then'] = 'neos-active';
$arguments66['else'] = NULL;
$renderChildrenClosure67 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper68 = $self->getViewHelper('$viewHelper68', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper68->setArguments($arguments66);
$viewHelper68->setRenderingContext($renderingContext);
$viewHelper68->setRenderChildrenClosure($renderChildrenClosure67);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments61['class'] = $viewHelper68->initializeArgumentsAndRender();
$arguments61['additionalAttributes'] = NULL;
$arguments61['data'] = NULL;
$arguments61['controller'] = NULL;
$arguments61['package'] = NULL;
$arguments61['subpackage'] = NULL;
$arguments61['section'] = '';
$arguments61['format'] = '';
$arguments61['additionalParams'] = array (
);
$arguments61['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments61['useParentRequest'] = false;
$arguments61['absolute'] = true;
$arguments61['dir'] = NULL;
$arguments61['id'] = NULL;
$arguments61['lang'] = NULL;
$arguments61['style'] = NULL;
$arguments61['accesskey'] = NULL;
$arguments61['tabindex'] = NULL;
$arguments61['onclick'] = NULL;
$arguments61['name'] = NULL;
$arguments61['rel'] = NULL;
$arguments61['rev'] = NULL;
$arguments61['target'] = NULL;
$renderChildrenClosure69 = function() use ($renderingContext, $self) {
$output70 = '';

$output70 .= '<i class="icon-picture"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments71 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments72 = array();
$arguments72['id'] = 'media.filter.images';
$arguments72['source'] = 'Modules';
$arguments72['package'] = 'TYPO3.Neos';
$arguments72['value'] = NULL;
$arguments72['arguments'] = array (
);
$arguments72['quantity'] = NULL;
$arguments72['languageIdentifier'] = NULL;
$renderChildrenClosure73 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper74 = $self->getViewHelper('$viewHelper74', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper74->setArguments($arguments72);
$viewHelper74->setRenderingContext($renderingContext);
$viewHelper74->setRenderChildrenClosure($renderChildrenClosure73);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments71['value'] = $viewHelper74->initializeArgumentsAndRender();
$arguments71['keepQuotes'] = false;
$arguments71['encoding'] = 'UTF-8';
$arguments71['doubleEncode'] = true;
$renderChildrenClosure75 = function() use ($renderingContext, $self) {
return NULL;
};
$value76 = ($arguments71['value'] !== NULL ? $arguments71['value'] : $renderChildrenClosure75());

$output70 .= !is_string($value76) && !(is_object($value76) && method_exists($value76, '__toString')) ? $value76 : htmlspecialchars($value76, ($arguments71['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments71['encoding'], $arguments71['doubleEncode']);
return $output70;
};
$viewHelper77 = $self->getViewHelper('$viewHelper77', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper77->setArguments($arguments61);
$viewHelper77->setRenderingContext($renderingContext);
$viewHelper77->setRenderChildrenClosure($renderChildrenClosure69);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output0 .= $viewHelper77->initializeArgumentsAndRender();

$output0 .= '
				</li>
				<li>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments78 = array();
$arguments78['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments79 = array();
$arguments79['id'] = 'media.filter.title.documents';
$arguments79['source'] = 'Modules';
$arguments79['package'] = 'TYPO3.Neos';
$arguments79['value'] = NULL;
$arguments79['arguments'] = array (
);
$arguments79['quantity'] = NULL;
$arguments79['languageIdentifier'] = NULL;
$renderChildrenClosure80 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper81 = $self->getViewHelper('$viewHelper81', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper81->setArguments($arguments79);
$viewHelper81->setRenderingContext($renderingContext);
$viewHelper81->setRenderChildrenClosure($renderChildrenClosure80);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments78['title'] = $viewHelper81->initializeArgumentsAndRender();
// Rendering Array
$array82 = array();
$array82['filter'] = 'Document';
$arguments78['arguments'] = $array82;
// Rendering Boolean node
$arguments78['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments83 = array();
// Rendering Boolean node
$arguments83['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'filter', $renderingContext), 'Document');
$arguments83['then'] = 'neos-active';
$arguments83['else'] = NULL;
$renderChildrenClosure84 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper85 = $self->getViewHelper('$viewHelper85', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper85->setArguments($arguments83);
$viewHelper85->setRenderingContext($renderingContext);
$viewHelper85->setRenderChildrenClosure($renderChildrenClosure84);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments78['class'] = $viewHelper85->initializeArgumentsAndRender();
$arguments78['additionalAttributes'] = NULL;
$arguments78['data'] = NULL;
$arguments78['controller'] = NULL;
$arguments78['package'] = NULL;
$arguments78['subpackage'] = NULL;
$arguments78['section'] = '';
$arguments78['format'] = '';
$arguments78['additionalParams'] = array (
);
$arguments78['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments78['useParentRequest'] = false;
$arguments78['absolute'] = true;
$arguments78['dir'] = NULL;
$arguments78['id'] = NULL;
$arguments78['lang'] = NULL;
$arguments78['style'] = NULL;
$arguments78['accesskey'] = NULL;
$arguments78['tabindex'] = NULL;
$arguments78['onclick'] = NULL;
$arguments78['name'] = NULL;
$arguments78['rel'] = NULL;
$arguments78['rev'] = NULL;
$arguments78['target'] = NULL;
$renderChildrenClosure86 = function() use ($renderingContext, $self) {
$output87 = '';

$output87 .= '<i class="icon-file-text"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments88 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments89 = array();
$arguments89['id'] = 'media.filter.documents';
$arguments89['source'] = 'Modules';
$arguments89['package'] = 'TYPO3.Neos';
$arguments89['value'] = NULL;
$arguments89['arguments'] = array (
);
$arguments89['quantity'] = NULL;
$arguments89['languageIdentifier'] = NULL;
$renderChildrenClosure90 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper91 = $self->getViewHelper('$viewHelper91', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper91->setArguments($arguments89);
$viewHelper91->setRenderingContext($renderingContext);
$viewHelper91->setRenderChildrenClosure($renderChildrenClosure90);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments88['value'] = $viewHelper91->initializeArgumentsAndRender();
$arguments88['keepQuotes'] = false;
$arguments88['encoding'] = 'UTF-8';
$arguments88['doubleEncode'] = true;
$renderChildrenClosure92 = function() use ($renderingContext, $self) {
return NULL;
};
$value93 = ($arguments88['value'] !== NULL ? $arguments88['value'] : $renderChildrenClosure92());

$output87 .= !is_string($value93) && !(is_object($value93) && method_exists($value93, '__toString')) ? $value93 : htmlspecialchars($value93, ($arguments88['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments88['encoding'], $arguments88['doubleEncode']);
return $output87;
};
$viewHelper94 = $self->getViewHelper('$viewHelper94', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper94->setArguments($arguments78);
$viewHelper94->setRenderingContext($renderingContext);
$viewHelper94->setRenderChildrenClosure($renderChildrenClosure86);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output0 .= $viewHelper94->initializeArgumentsAndRender();

$output0 .= '
				</li>
				<li>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments95 = array();
$arguments95['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments96 = array();
$arguments96['id'] = 'media.filter.title.video';
$arguments96['source'] = 'Modules';
$arguments96['package'] = 'TYPO3.Neos';
$arguments96['value'] = NULL;
$arguments96['arguments'] = array (
);
$arguments96['quantity'] = NULL;
$arguments96['languageIdentifier'] = NULL;
$renderChildrenClosure97 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper98 = $self->getViewHelper('$viewHelper98', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper98->setArguments($arguments96);
$viewHelper98->setRenderingContext($renderingContext);
$viewHelper98->setRenderChildrenClosure($renderChildrenClosure97);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments95['title'] = $viewHelper98->initializeArgumentsAndRender();
// Rendering Array
$array99 = array();
$array99['filter'] = 'Video';
$arguments95['arguments'] = $array99;
// Rendering Boolean node
$arguments95['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments100 = array();
// Rendering Boolean node
$arguments100['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'filter', $renderingContext), 'Video');
$arguments100['then'] = 'neos-active';
$arguments100['else'] = NULL;
$renderChildrenClosure101 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper102 = $self->getViewHelper('$viewHelper102', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper102->setArguments($arguments100);
$viewHelper102->setRenderingContext($renderingContext);
$viewHelper102->setRenderChildrenClosure($renderChildrenClosure101);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments95['class'] = $viewHelper102->initializeArgumentsAndRender();
$arguments95['additionalAttributes'] = NULL;
$arguments95['data'] = NULL;
$arguments95['controller'] = NULL;
$arguments95['package'] = NULL;
$arguments95['subpackage'] = NULL;
$arguments95['section'] = '';
$arguments95['format'] = '';
$arguments95['additionalParams'] = array (
);
$arguments95['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments95['useParentRequest'] = false;
$arguments95['absolute'] = true;
$arguments95['dir'] = NULL;
$arguments95['id'] = NULL;
$arguments95['lang'] = NULL;
$arguments95['style'] = NULL;
$arguments95['accesskey'] = NULL;
$arguments95['tabindex'] = NULL;
$arguments95['onclick'] = NULL;
$arguments95['name'] = NULL;
$arguments95['rel'] = NULL;
$arguments95['rev'] = NULL;
$arguments95['target'] = NULL;
$renderChildrenClosure103 = function() use ($renderingContext, $self) {
$output104 = '';

$output104 .= '<i class="icon-film"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments105 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments106 = array();
$arguments106['id'] = 'media.filter.video';
$arguments106['source'] = 'Modules';
$arguments106['package'] = 'TYPO3.Neos';
$arguments106['value'] = NULL;
$arguments106['arguments'] = array (
);
$arguments106['quantity'] = NULL;
$arguments106['languageIdentifier'] = NULL;
$renderChildrenClosure107 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper108 = $self->getViewHelper('$viewHelper108', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper108->setArguments($arguments106);
$viewHelper108->setRenderingContext($renderingContext);
$viewHelper108->setRenderChildrenClosure($renderChildrenClosure107);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments105['value'] = $viewHelper108->initializeArgumentsAndRender();
$arguments105['keepQuotes'] = false;
$arguments105['encoding'] = 'UTF-8';
$arguments105['doubleEncode'] = true;
$renderChildrenClosure109 = function() use ($renderingContext, $self) {
return NULL;
};
$value110 = ($arguments105['value'] !== NULL ? $arguments105['value'] : $renderChildrenClosure109());

$output104 .= !is_string($value110) && !(is_object($value110) && method_exists($value110, '__toString')) ? $value110 : htmlspecialchars($value110, ($arguments105['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments105['encoding'], $arguments105['doubleEncode']);
return $output104;
};
$viewHelper111 = $self->getViewHelper('$viewHelper111', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper111->setArguments($arguments95);
$viewHelper111->setRenderingContext($renderingContext);
$viewHelper111->setRenderChildrenClosure($renderChildrenClosure103);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output0 .= $viewHelper111->initializeArgumentsAndRender();

$output0 .= '
				</li>
				<li>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments112 = array();
$arguments112['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments113 = array();
$arguments113['id'] = 'media.filter.title.audio';
$arguments113['source'] = 'Modules';
$arguments113['package'] = 'TYPO3.Neos';
$arguments113['value'] = NULL;
$arguments113['arguments'] = array (
);
$arguments113['quantity'] = NULL;
$arguments113['languageIdentifier'] = NULL;
$renderChildrenClosure114 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper115 = $self->getViewHelper('$viewHelper115', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper115->setArguments($arguments113);
$viewHelper115->setRenderingContext($renderingContext);
$viewHelper115->setRenderChildrenClosure($renderChildrenClosure114);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments112['title'] = $viewHelper115->initializeArgumentsAndRender();
// Rendering Array
$array116 = array();
$array116['filter'] = 'Audio';
$arguments112['arguments'] = $array116;
// Rendering Boolean node
$arguments112['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments117 = array();
// Rendering Boolean node
$arguments117['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'filter', $renderingContext), 'Audio');
$arguments117['then'] = 'neos-active';
$arguments117['else'] = NULL;
$renderChildrenClosure118 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper119 = $self->getViewHelper('$viewHelper119', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper119->setArguments($arguments117);
$viewHelper119->setRenderingContext($renderingContext);
$viewHelper119->setRenderChildrenClosure($renderChildrenClosure118);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments112['class'] = $viewHelper119->initializeArgumentsAndRender();
$arguments112['additionalAttributes'] = NULL;
$arguments112['data'] = NULL;
$arguments112['controller'] = NULL;
$arguments112['package'] = NULL;
$arguments112['subpackage'] = NULL;
$arguments112['section'] = '';
$arguments112['format'] = '';
$arguments112['additionalParams'] = array (
);
$arguments112['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments112['useParentRequest'] = false;
$arguments112['absolute'] = true;
$arguments112['dir'] = NULL;
$arguments112['id'] = NULL;
$arguments112['lang'] = NULL;
$arguments112['style'] = NULL;
$arguments112['accesskey'] = NULL;
$arguments112['tabindex'] = NULL;
$arguments112['onclick'] = NULL;
$arguments112['name'] = NULL;
$arguments112['rel'] = NULL;
$arguments112['rev'] = NULL;
$arguments112['target'] = NULL;
$renderChildrenClosure120 = function() use ($renderingContext, $self) {
$output121 = '';

$output121 .= '<i class="icon-music"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments122 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments123 = array();
$arguments123['id'] = 'media.filter.audio';
$arguments123['source'] = 'Modules';
$arguments123['package'] = 'TYPO3.Neos';
$arguments123['value'] = NULL;
$arguments123['arguments'] = array (
);
$arguments123['quantity'] = NULL;
$arguments123['languageIdentifier'] = NULL;
$renderChildrenClosure124 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper125 = $self->getViewHelper('$viewHelper125', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper125->setArguments($arguments123);
$viewHelper125->setRenderingContext($renderingContext);
$viewHelper125->setRenderChildrenClosure($renderChildrenClosure124);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments122['value'] = $viewHelper125->initializeArgumentsAndRender();
$arguments122['keepQuotes'] = false;
$arguments122['encoding'] = 'UTF-8';
$arguments122['doubleEncode'] = true;
$renderChildrenClosure126 = function() use ($renderingContext, $self) {
return NULL;
};
$value127 = ($arguments122['value'] !== NULL ? $arguments122['value'] : $renderChildrenClosure126());

$output121 .= !is_string($value127) && !(is_object($value127) && method_exists($value127, '__toString')) ? $value127 : htmlspecialchars($value127, ($arguments122['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments122['encoding'], $arguments122['doubleEncode']);
return $output121;
};
$viewHelper128 = $self->getViewHelper('$viewHelper128', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper128->setArguments($arguments112);
$viewHelper128->setRenderingContext($renderingContext);
$viewHelper128->setRenderChildrenClosure($renderChildrenClosure120);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output0 .= $viewHelper128->initializeArgumentsAndRender();

$output0 .= '
				</li>
			</ul>
		</div>
		<div class="neos-dropdown" id="neos-sort-menu">
			<span title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments129 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments130 = array();
$arguments130['id'] = 'media.tooltip.sortOptions';
$arguments130['source'] = 'Modules';
$arguments130['package'] = 'TYPO3.Neos';
$arguments130['value'] = NULL;
$arguments130['arguments'] = array (
);
$arguments130['quantity'] = NULL;
$arguments130['languageIdentifier'] = NULL;
$renderChildrenClosure131 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper132 = $self->getViewHelper('$viewHelper132', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper132->setArguments($arguments130);
$viewHelper132->setRenderingContext($renderingContext);
$viewHelper132->setRenderChildrenClosure($renderChildrenClosure131);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments129['value'] = $viewHelper132->initializeArgumentsAndRender();
$arguments129['keepQuotes'] = false;
$arguments129['encoding'] = 'UTF-8';
$arguments129['doubleEncode'] = true;
$renderChildrenClosure133 = function() use ($renderingContext, $self) {
return NULL;
};
$value134 = ($arguments129['value'] !== NULL ? $arguments129['value'] : $renderChildrenClosure133());

$output0 .= !is_string($value134) && !(is_object($value134) && method_exists($value134, '__toString')) ? $value134 : htmlspecialchars($value134, ($arguments129['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments129['encoding'], $arguments129['doubleEncode']);

$output0 .= '" data-neos-toggle="tooltip">
				<a class="dropdown-toggle" href="#" data-neos-toggle="dropdown" data-target="#neos-sort-menu">
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments135 = array();
// Rendering Boolean node
$arguments135['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortDirection', $renderingContext), 'ASC');
$arguments135['then'] = NULL;
$arguments135['else'] = NULL;
$renderChildrenClosure136 = function() use ($renderingContext, $self) {
$output137 = '';

$output137 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments138 = array();
$renderChildrenClosure139 = function() use ($renderingContext, $self) {
$output140 = '';

$output140 .= '
							<i class="icon-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments141 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments142 = array();
$arguments142['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext);
$arguments142['keepQuotes'] = false;
$arguments142['encoding'] = 'UTF-8';
$arguments142['doubleEncode'] = true;
$renderChildrenClosure143 = function() use ($renderingContext, $self) {
return NULL;
};
$value144 = ($arguments142['value'] !== NULL ? $arguments142['value'] : $renderChildrenClosure143());
$arguments141['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', !is_string($value144) && !(is_object($value144) && method_exists($value144, '__toString')) ? $value144 : htmlspecialchars($value144, ($arguments142['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments142['encoding'], $arguments142['doubleEncode']), 'Modified');
$arguments141['then'] = 'sort-by-order';
$arguments141['else'] = 'sort-by-alphabet';
$renderChildrenClosure145 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper146 = $self->getViewHelper('$viewHelper146', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper146->setArguments($arguments141);
$viewHelper146->setRenderingContext($renderingContext);
$viewHelper146->setRenderChildrenClosure($renderChildrenClosure145);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output140 .= $viewHelper146->initializeArgumentsAndRender();

$output140 .= '"></i>
						';
return $output140;
};
$viewHelper147 = $self->getViewHelper('$viewHelper147', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper147->setArguments($arguments138);
$viewHelper147->setRenderingContext($renderingContext);
$viewHelper147->setRenderChildrenClosure($renderChildrenClosure139);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output137 .= $viewHelper147->initializeArgumentsAndRender();

$output137 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments148 = array();
$renderChildrenClosure149 = function() use ($renderingContext, $self) {
$output150 = '';

$output150 .= '
							<i class="icon-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments151 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments152 = array();
$arguments152['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext);
$arguments152['keepQuotes'] = false;
$arguments152['encoding'] = 'UTF-8';
$arguments152['doubleEncode'] = true;
$renderChildrenClosure153 = function() use ($renderingContext, $self) {
return NULL;
};
$value154 = ($arguments152['value'] !== NULL ? $arguments152['value'] : $renderChildrenClosure153());
$arguments151['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', !is_string($value154) && !(is_object($value154) && method_exists($value154, '__toString')) ? $value154 : htmlspecialchars($value154, ($arguments152['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments152['encoding'], $arguments152['doubleEncode']), 'Modified');
$arguments151['then'] = 'sort-by-order-alt';
$arguments151['else'] = 'sort-by-alphabet-alt';
$renderChildrenClosure155 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper156 = $self->getViewHelper('$viewHelper156', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper156->setArguments($arguments151);
$viewHelper156->setRenderingContext($renderingContext);
$viewHelper156->setRenderChildrenClosure($renderChildrenClosure155);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output150 .= $viewHelper156->initializeArgumentsAndRender();

$output150 .= '"></i>
						';
return $output150;
};
$viewHelper157 = $self->getViewHelper('$viewHelper157', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper157->setArguments($arguments148);
$viewHelper157->setRenderingContext($renderingContext);
$viewHelper157->setRenderChildrenClosure($renderChildrenClosure149);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output137 .= $viewHelper157->initializeArgumentsAndRender();

$output137 .= '
					';
return $output137;
};
$arguments135['__thenClosure'] = function() use ($renderingContext, $self) {
$output158 = '';

$output158 .= '
							<i class="icon-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments159 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments160 = array();
$arguments160['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext);
$arguments160['keepQuotes'] = false;
$arguments160['encoding'] = 'UTF-8';
$arguments160['doubleEncode'] = true;
$renderChildrenClosure161 = function() use ($renderingContext, $self) {
return NULL;
};
$value162 = ($arguments160['value'] !== NULL ? $arguments160['value'] : $renderChildrenClosure161());
$arguments159['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', !is_string($value162) && !(is_object($value162) && method_exists($value162, '__toString')) ? $value162 : htmlspecialchars($value162, ($arguments160['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments160['encoding'], $arguments160['doubleEncode']), 'Modified');
$arguments159['then'] = 'sort-by-order';
$arguments159['else'] = 'sort-by-alphabet';
$renderChildrenClosure163 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper164 = $self->getViewHelper('$viewHelper164', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper164->setArguments($arguments159);
$viewHelper164->setRenderingContext($renderingContext);
$viewHelper164->setRenderChildrenClosure($renderChildrenClosure163);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output158 .= $viewHelper164->initializeArgumentsAndRender();

$output158 .= '"></i>
						';
return $output158;
};
$arguments135['__elseClosure'] = function() use ($renderingContext, $self) {
$output165 = '';

$output165 .= '
							<i class="icon-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments166 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments167 = array();
$arguments167['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext);
$arguments167['keepQuotes'] = false;
$arguments167['encoding'] = 'UTF-8';
$arguments167['doubleEncode'] = true;
$renderChildrenClosure168 = function() use ($renderingContext, $self) {
return NULL;
};
$value169 = ($arguments167['value'] !== NULL ? $arguments167['value'] : $renderChildrenClosure168());
$arguments166['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', !is_string($value169) && !(is_object($value169) && method_exists($value169, '__toString')) ? $value169 : htmlspecialchars($value169, ($arguments167['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments167['encoding'], $arguments167['doubleEncode']), 'Modified');
$arguments166['then'] = 'sort-by-order-alt';
$arguments166['else'] = 'sort-by-alphabet-alt';
$renderChildrenClosure170 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper171 = $self->getViewHelper('$viewHelper171', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper171->setArguments($arguments166);
$viewHelper171->setRenderingContext($renderingContext);
$viewHelper171->setRenderChildrenClosure($renderChildrenClosure170);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output165 .= $viewHelper171->initializeArgumentsAndRender();

$output165 .= '"></i>
						';
return $output165;
};
$viewHelper172 = $self->getViewHelper('$viewHelper172', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper172->setArguments($arguments135);
$viewHelper172->setRenderingContext($renderingContext);
$viewHelper172->setRenderChildrenClosure($renderChildrenClosure136);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output0 .= $viewHelper172->initializeArgumentsAndRender();

$output0 .= '
				</a>
			</span>
			<div class="neos-dropdown-menu-list neos-pull-right" role="menu">
				<span class="neos-dropdown-menu-list-title" title="">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments173 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments174 = array();
$arguments174['id'] = 'media.sortby';
$arguments174['source'] = 'Modules';
$arguments174['package'] = 'TYPO3.Neos';
$arguments174['value'] = NULL;
$arguments174['arguments'] = array (
);
$arguments174['quantity'] = NULL;
$arguments174['languageIdentifier'] = NULL;
$renderChildrenClosure175 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper176 = $self->getViewHelper('$viewHelper176', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper176->setArguments($arguments174);
$viewHelper176->setRenderingContext($renderingContext);
$viewHelper176->setRenderChildrenClosure($renderChildrenClosure175);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments173['value'] = $viewHelper176->initializeArgumentsAndRender();
$arguments173['keepQuotes'] = false;
$arguments173['encoding'] = 'UTF-8';
$arguments173['doubleEncode'] = true;
$renderChildrenClosure177 = function() use ($renderingContext, $self) {
return NULL;
};
$value178 = ($arguments173['value'] !== NULL ? $arguments173['value'] : $renderChildrenClosure177());

$output0 .= !is_string($value178) && !(is_object($value178) && method_exists($value178, '__toString')) ? $value178 : htmlspecialchars($value178, ($arguments173['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments173['encoding'], $arguments173['doubleEncode']);

$output0 .= '</span>
				<ul>
					<li>
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments179 = array();
$arguments179['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments180 = array();
$arguments180['id'] = 'media.sortByLastModified';
$arguments180['source'] = 'Modules';
$arguments180['package'] = 'TYPO3.Neos';
$arguments180['value'] = NULL;
$arguments180['arguments'] = array (
);
$arguments180['quantity'] = NULL;
$arguments180['languageIdentifier'] = NULL;
$renderChildrenClosure181 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper182 = $self->getViewHelper('$viewHelper182', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper182->setArguments($arguments180);
$viewHelper182->setRenderingContext($renderingContext);
$viewHelper182->setRenderChildrenClosure($renderChildrenClosure181);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments179['title'] = $viewHelper182->initializeArgumentsAndRender();
// Rendering Array
$array183 = array();
$array183['sortBy'] = 'Modified';
$arguments179['arguments'] = $array183;
// Rendering Boolean node
$arguments179['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments184 = array();
// Rendering Boolean node
$arguments184['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext), 'Modified');
$arguments184['then'] = 'neos-active';
$arguments184['else'] = NULL;
$renderChildrenClosure185 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper186 = $self->getViewHelper('$viewHelper186', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper186->setArguments($arguments184);
$viewHelper186->setRenderingContext($renderingContext);
$viewHelper186->setRenderChildrenClosure($renderChildrenClosure185);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments179['class'] = $viewHelper186->initializeArgumentsAndRender();
$arguments179['additionalAttributes'] = NULL;
$arguments179['data'] = NULL;
$arguments179['controller'] = NULL;
$arguments179['package'] = NULL;
$arguments179['subpackage'] = NULL;
$arguments179['section'] = '';
$arguments179['format'] = '';
$arguments179['additionalParams'] = array (
);
$arguments179['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments179['useParentRequest'] = false;
$arguments179['absolute'] = true;
$arguments179['dir'] = NULL;
$arguments179['id'] = NULL;
$arguments179['lang'] = NULL;
$arguments179['style'] = NULL;
$arguments179['accesskey'] = NULL;
$arguments179['tabindex'] = NULL;
$arguments179['onclick'] = NULL;
$arguments179['name'] = NULL;
$arguments179['rel'] = NULL;
$arguments179['rev'] = NULL;
$arguments179['target'] = NULL;
$renderChildrenClosure187 = function() use ($renderingContext, $self) {
$output188 = '';

$output188 .= '<i class="icon-sort-by-order"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments189 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments190 = array();
$arguments190['id'] = 'media.field.lastModified';
$arguments190['source'] = 'Modules';
$arguments190['package'] = 'TYPO3.Neos';
$arguments190['value'] = NULL;
$arguments190['arguments'] = array (
);
$arguments190['quantity'] = NULL;
$arguments190['languageIdentifier'] = NULL;
$renderChildrenClosure191 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper192 = $self->getViewHelper('$viewHelper192', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper192->setArguments($arguments190);
$viewHelper192->setRenderingContext($renderingContext);
$viewHelper192->setRenderChildrenClosure($renderChildrenClosure191);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments189['value'] = $viewHelper192->initializeArgumentsAndRender();
$arguments189['keepQuotes'] = false;
$arguments189['encoding'] = 'UTF-8';
$arguments189['doubleEncode'] = true;
$renderChildrenClosure193 = function() use ($renderingContext, $self) {
return NULL;
};
$value194 = ($arguments189['value'] !== NULL ? $arguments189['value'] : $renderChildrenClosure193());

$output188 .= !is_string($value194) && !(is_object($value194) && method_exists($value194, '__toString')) ? $value194 : htmlspecialchars($value194, ($arguments189['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments189['encoding'], $arguments189['doubleEncode']);
return $output188;
};
$viewHelper195 = $self->getViewHelper('$viewHelper195', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper195->setArguments($arguments179);
$viewHelper195->setRenderingContext($renderingContext);
$viewHelper195->setRenderChildrenClosure($renderChildrenClosure187);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output0 .= $viewHelper195->initializeArgumentsAndRender();

$output0 .= '
					</li>
					<li>
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments196 = array();
$arguments196['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments197 = array();
$arguments197['id'] = 'media.sortByName';
$arguments197['source'] = 'Modules';
$arguments197['package'] = 'TYPO3.Neos';
$arguments197['value'] = NULL;
$arguments197['arguments'] = array (
);
$arguments197['quantity'] = NULL;
$arguments197['languageIdentifier'] = NULL;
$renderChildrenClosure198 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper199 = $self->getViewHelper('$viewHelper199', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper199->setArguments($arguments197);
$viewHelper199->setRenderingContext($renderingContext);
$viewHelper199->setRenderChildrenClosure($renderChildrenClosure198);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments196['title'] = $viewHelper199->initializeArgumentsAndRender();
// Rendering Array
$array200 = array();
$array200['sortBy'] = 'Name';
$arguments196['arguments'] = $array200;
// Rendering Boolean node
$arguments196['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments201 = array();
// Rendering Boolean node
$arguments201['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext), 'Name');
$arguments201['then'] = 'neos-active';
$arguments201['else'] = NULL;
$renderChildrenClosure202 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper203 = $self->getViewHelper('$viewHelper203', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper203->setArguments($arguments201);
$viewHelper203->setRenderingContext($renderingContext);
$viewHelper203->setRenderChildrenClosure($renderChildrenClosure202);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments196['class'] = $viewHelper203->initializeArgumentsAndRender();
$arguments196['additionalAttributes'] = NULL;
$arguments196['data'] = NULL;
$arguments196['controller'] = NULL;
$arguments196['package'] = NULL;
$arguments196['subpackage'] = NULL;
$arguments196['section'] = '';
$arguments196['format'] = '';
$arguments196['additionalParams'] = array (
);
$arguments196['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments196['useParentRequest'] = false;
$arguments196['absolute'] = true;
$arguments196['dir'] = NULL;
$arguments196['id'] = NULL;
$arguments196['lang'] = NULL;
$arguments196['style'] = NULL;
$arguments196['accesskey'] = NULL;
$arguments196['tabindex'] = NULL;
$arguments196['onclick'] = NULL;
$arguments196['name'] = NULL;
$arguments196['rel'] = NULL;
$arguments196['rev'] = NULL;
$arguments196['target'] = NULL;
$renderChildrenClosure204 = function() use ($renderingContext, $self) {
$output205 = '';

$output205 .= '<i class="icon-sort-by-alphabet"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments206 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments207 = array();
$arguments207['id'] = 'media.field.name';
$arguments207['source'] = 'Modules';
$arguments207['package'] = 'TYPO3.Neos';
$arguments207['value'] = NULL;
$arguments207['arguments'] = array (
);
$arguments207['quantity'] = NULL;
$arguments207['languageIdentifier'] = NULL;
$renderChildrenClosure208 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper209 = $self->getViewHelper('$viewHelper209', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper209->setArguments($arguments207);
$viewHelper209->setRenderingContext($renderingContext);
$viewHelper209->setRenderChildrenClosure($renderChildrenClosure208);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments206['value'] = $viewHelper209->initializeArgumentsAndRender();
$arguments206['keepQuotes'] = false;
$arguments206['encoding'] = 'UTF-8';
$arguments206['doubleEncode'] = true;
$renderChildrenClosure210 = function() use ($renderingContext, $self) {
return NULL;
};
$value211 = ($arguments206['value'] !== NULL ? $arguments206['value'] : $renderChildrenClosure210());

$output205 .= !is_string($value211) && !(is_object($value211) && method_exists($value211, '__toString')) ? $value211 : htmlspecialchars($value211, ($arguments206['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments206['encoding'], $arguments206['doubleEncode']);
return $output205;
};
$viewHelper212 = $self->getViewHelper('$viewHelper212', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper212->setArguments($arguments196);
$viewHelper212->setRenderingContext($renderingContext);
$viewHelper212->setRenderChildrenClosure($renderChildrenClosure204);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output0 .= $viewHelper212->initializeArgumentsAndRender();

$output0 .= '
					</li>
				</ul>
				<span class="neos-dropdown-menu-list-title">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments213 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments214 = array();
$arguments214['id'] = 'media.sortdirection';
$arguments214['source'] = 'Modules';
$arguments214['package'] = 'TYPO3.Neos';
$arguments214['value'] = NULL;
$arguments214['arguments'] = array (
);
$arguments214['quantity'] = NULL;
$arguments214['languageIdentifier'] = NULL;
$renderChildrenClosure215 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper216 = $self->getViewHelper('$viewHelper216', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper216->setArguments($arguments214);
$viewHelper216->setRenderingContext($renderingContext);
$viewHelper216->setRenderChildrenClosure($renderChildrenClosure215);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments213['value'] = $viewHelper216->initializeArgumentsAndRender();
$arguments213['keepQuotes'] = false;
$arguments213['encoding'] = 'UTF-8';
$arguments213['doubleEncode'] = true;
$renderChildrenClosure217 = function() use ($renderingContext, $self) {
return NULL;
};
$value218 = ($arguments213['value'] !== NULL ? $arguments213['value'] : $renderChildrenClosure217());

$output0 .= !is_string($value218) && !(is_object($value218) && method_exists($value218, '__toString')) ? $value218 : htmlspecialchars($value218, ($arguments213['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments213['encoding'], $arguments213['doubleEncode']);

$output0 .= '</span>
				<ul>
					<li>
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments219 = array();
$arguments219['action'] = 'index';
$arguments219['title'] = 'Sort direction Ascending';
// Rendering Array
$array220 = array();
$array220['sortDirection'] = 'ASC';
$arguments219['arguments'] = $array220;
// Rendering Boolean node
$arguments219['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments221 = array();
// Rendering Boolean node
$arguments221['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortDirection', $renderingContext), 'ASC');
$arguments221['then'] = 'neos-active';
$arguments221['else'] = NULL;
$renderChildrenClosure222 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper223 = $self->getViewHelper('$viewHelper223', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper223->setArguments($arguments221);
$viewHelper223->setRenderingContext($renderingContext);
$viewHelper223->setRenderChildrenClosure($renderChildrenClosure222);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments219['class'] = $viewHelper223->initializeArgumentsAndRender();
$arguments219['additionalAttributes'] = NULL;
$arguments219['data'] = NULL;
$arguments219['controller'] = NULL;
$arguments219['package'] = NULL;
$arguments219['subpackage'] = NULL;
$arguments219['section'] = '';
$arguments219['format'] = '';
$arguments219['additionalParams'] = array (
);
$arguments219['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments219['useParentRequest'] = false;
$arguments219['absolute'] = true;
$arguments219['dir'] = NULL;
$arguments219['id'] = NULL;
$arguments219['lang'] = NULL;
$arguments219['style'] = NULL;
$arguments219['accesskey'] = NULL;
$arguments219['tabindex'] = NULL;
$arguments219['onclick'] = NULL;
$arguments219['name'] = NULL;
$arguments219['rel'] = NULL;
$arguments219['rev'] = NULL;
$arguments219['target'] = NULL;
$renderChildrenClosure224 = function() use ($renderingContext, $self) {
$output225 = '';

$output225 .= '<i class="icon-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments226 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments227 = array();
$arguments227['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext);
$arguments227['keepQuotes'] = false;
$arguments227['encoding'] = 'UTF-8';
$arguments227['doubleEncode'] = true;
$renderChildrenClosure228 = function() use ($renderingContext, $self) {
return NULL;
};
$value229 = ($arguments227['value'] !== NULL ? $arguments227['value'] : $renderChildrenClosure228());
$arguments226['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', !is_string($value229) && !(is_object($value229) && method_exists($value229, '__toString')) ? $value229 : htmlspecialchars($value229, ($arguments227['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments227['encoding'], $arguments227['doubleEncode']), 'Name');
$arguments226['then'] = 'sort-by-alphabet';
$arguments226['else'] = 'sort-by-order';
$renderChildrenClosure230 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper231 = $self->getViewHelper('$viewHelper231', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper231->setArguments($arguments226);
$viewHelper231->setRenderingContext($renderingContext);
$viewHelper231->setRenderChildrenClosure($renderChildrenClosure230);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output225 .= $viewHelper231->initializeArgumentsAndRender();

$output225 .= '"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments232 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments233 = array();
$arguments233['id'] = 'media.sortdirection.asc';
$arguments233['source'] = 'Modules';
$arguments233['package'] = 'TYPO3.Neos';
$arguments233['value'] = NULL;
$arguments233['arguments'] = array (
);
$arguments233['quantity'] = NULL;
$arguments233['languageIdentifier'] = NULL;
$renderChildrenClosure234 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper235 = $self->getViewHelper('$viewHelper235', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper235->setArguments($arguments233);
$viewHelper235->setRenderingContext($renderingContext);
$viewHelper235->setRenderChildrenClosure($renderChildrenClosure234);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments232['value'] = $viewHelper235->initializeArgumentsAndRender();
$arguments232['keepQuotes'] = false;
$arguments232['encoding'] = 'UTF-8';
$arguments232['doubleEncode'] = true;
$renderChildrenClosure236 = function() use ($renderingContext, $self) {
return NULL;
};
$value237 = ($arguments232['value'] !== NULL ? $arguments232['value'] : $renderChildrenClosure236());

$output225 .= !is_string($value237) && !(is_object($value237) && method_exists($value237, '__toString')) ? $value237 : htmlspecialchars($value237, ($arguments232['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments232['encoding'], $arguments232['doubleEncode']);
return $output225;
};
$viewHelper238 = $self->getViewHelper('$viewHelper238', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper238->setArguments($arguments219);
$viewHelper238->setRenderingContext($renderingContext);
$viewHelper238->setRenderChildrenClosure($renderChildrenClosure224);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output0 .= $viewHelper238->initializeArgumentsAndRender();

$output0 .= '
					</li>
					<li>
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments239 = array();
$arguments239['action'] = 'index';
$arguments239['title'] = 'Sort direction Descending';
// Rendering Array
$array240 = array();
$array240['sortDirection'] = 'DESC';
$arguments239['arguments'] = $array240;
// Rendering Boolean node
$arguments239['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments241 = array();
// Rendering Boolean node
$arguments241['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortDirection', $renderingContext), 'DESC');
$arguments241['then'] = 'neos-active';
$arguments241['else'] = NULL;
$renderChildrenClosure242 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper243 = $self->getViewHelper('$viewHelper243', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper243->setArguments($arguments241);
$viewHelper243->setRenderingContext($renderingContext);
$viewHelper243->setRenderChildrenClosure($renderChildrenClosure242);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments239['class'] = $viewHelper243->initializeArgumentsAndRender();
$arguments239['additionalAttributes'] = NULL;
$arguments239['data'] = NULL;
$arguments239['controller'] = NULL;
$arguments239['package'] = NULL;
$arguments239['subpackage'] = NULL;
$arguments239['section'] = '';
$arguments239['format'] = '';
$arguments239['additionalParams'] = array (
);
$arguments239['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments239['useParentRequest'] = false;
$arguments239['absolute'] = true;
$arguments239['dir'] = NULL;
$arguments239['id'] = NULL;
$arguments239['lang'] = NULL;
$arguments239['style'] = NULL;
$arguments239['accesskey'] = NULL;
$arguments239['tabindex'] = NULL;
$arguments239['onclick'] = NULL;
$arguments239['name'] = NULL;
$arguments239['rel'] = NULL;
$arguments239['rev'] = NULL;
$arguments239['target'] = NULL;
$renderChildrenClosure244 = function() use ($renderingContext, $self) {
$output245 = '';

$output245 .= '<i class="icon-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments246 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments247 = array();
$arguments247['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext);
$arguments247['keepQuotes'] = false;
$arguments247['encoding'] = 'UTF-8';
$arguments247['doubleEncode'] = true;
$renderChildrenClosure248 = function() use ($renderingContext, $self) {
return NULL;
};
$value249 = ($arguments247['value'] !== NULL ? $arguments247['value'] : $renderChildrenClosure248());
$arguments246['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', !is_string($value249) && !(is_object($value249) && method_exists($value249, '__toString')) ? $value249 : htmlspecialchars($value249, ($arguments247['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments247['encoding'], $arguments247['doubleEncode']), 'Name');
$arguments246['then'] = 'sort-by-alphabet-alt';
$arguments246['else'] = 'sort-by-order-alt';
$renderChildrenClosure250 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper251 = $self->getViewHelper('$viewHelper251', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper251->setArguments($arguments246);
$viewHelper251->setRenderingContext($renderingContext);
$viewHelper251->setRenderChildrenClosure($renderChildrenClosure250);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output245 .= $viewHelper251->initializeArgumentsAndRender();

$output245 .= '"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments252 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments253 = array();
$arguments253['id'] = 'media.sortdirection.desc';
$arguments253['source'] = 'Modules';
$arguments253['package'] = 'TYPO3.Neos';
$arguments253['value'] = NULL;
$arguments253['arguments'] = array (
);
$arguments253['quantity'] = NULL;
$arguments253['languageIdentifier'] = NULL;
$renderChildrenClosure254 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper255 = $self->getViewHelper('$viewHelper255', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper255->setArguments($arguments253);
$viewHelper255->setRenderingContext($renderingContext);
$viewHelper255->setRenderChildrenClosure($renderChildrenClosure254);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments252['value'] = $viewHelper255->initializeArgumentsAndRender();
$arguments252['keepQuotes'] = false;
$arguments252['encoding'] = 'UTF-8';
$arguments252['doubleEncode'] = true;
$renderChildrenClosure256 = function() use ($renderingContext, $self) {
return NULL;
};
$value257 = ($arguments252['value'] !== NULL ? $arguments252['value'] : $renderChildrenClosure256());

$output245 .= !is_string($value257) && !(is_object($value257) && method_exists($value257, '__toString')) ? $value257 : htmlspecialchars($value257, ($arguments252['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments252['encoding'], $arguments252['doubleEncode']);
return $output245;
};
$viewHelper258 = $self->getViewHelper('$viewHelper258', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper258->setArguments($arguments239);
$viewHelper258->setRenderingContext($renderingContext);
$viewHelper258->setRenderChildrenClosure($renderChildrenClosure244);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output0 .= $viewHelper258->initializeArgumentsAndRender();

$output0 .= '
					</li>
				</ul>
			</div>
		</div>
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments259 = array();
// Rendering Boolean node
$arguments259['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'view', $renderingContext), 'Thumbnail');
$arguments259['then'] = NULL;
$arguments259['else'] = NULL;
$renderChildrenClosure260 = function() use ($renderingContext, $self) {
$output261 = '';

$output261 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments262 = array();
$renderChildrenClosure263 = function() use ($renderingContext, $self) {
$output264 = '';

$output264 .= '
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments265 = array();
$arguments265['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments266 = array();
$arguments266['id'] = 'media.tooltip.listView';
$arguments266['source'] = 'Modules';
$arguments266['package'] = 'TYPO3.Neos';
$arguments266['value'] = NULL;
$arguments266['arguments'] = array (
);
$arguments266['quantity'] = NULL;
$arguments266['languageIdentifier'] = NULL;
$renderChildrenClosure267 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper268 = $self->getViewHelper('$viewHelper268', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper268->setArguments($arguments266);
$viewHelper268->setRenderingContext($renderingContext);
$viewHelper268->setRenderChildrenClosure($renderChildrenClosure267);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments265['title'] = $viewHelper268->initializeArgumentsAndRender();
// Rendering Array
$array269 = array();
$array269['view'] = 'List';
$arguments265['arguments'] = $array269;
// Rendering Boolean node
$arguments265['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering Array
$array270 = array();
$array270['neos-toggle'] = 'tooltip';
$arguments265['data'] = $array270;
$arguments265['additionalAttributes'] = NULL;
$arguments265['controller'] = NULL;
$arguments265['package'] = NULL;
$arguments265['subpackage'] = NULL;
$arguments265['section'] = '';
$arguments265['format'] = '';
$arguments265['additionalParams'] = array (
);
$arguments265['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments265['useParentRequest'] = false;
$arguments265['absolute'] = true;
$arguments265['class'] = NULL;
$arguments265['dir'] = NULL;
$arguments265['id'] = NULL;
$arguments265['lang'] = NULL;
$arguments265['style'] = NULL;
$arguments265['accesskey'] = NULL;
$arguments265['tabindex'] = NULL;
$arguments265['onclick'] = NULL;
$arguments265['name'] = NULL;
$arguments265['rel'] = NULL;
$arguments265['rev'] = NULL;
$arguments265['target'] = NULL;
$renderChildrenClosure271 = function() use ($renderingContext, $self) {
return '<i class="icon-th-list"></i>';
};
$viewHelper272 = $self->getViewHelper('$viewHelper272', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper272->setArguments($arguments265);
$viewHelper272->setRenderingContext($renderingContext);
$viewHelper272->setRenderChildrenClosure($renderChildrenClosure271);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output264 .= $viewHelper272->initializeArgumentsAndRender();

$output264 .= '
			';
return $output264;
};
$viewHelper273 = $self->getViewHelper('$viewHelper273', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper273->setArguments($arguments262);
$viewHelper273->setRenderingContext($renderingContext);
$viewHelper273->setRenderChildrenClosure($renderChildrenClosure263);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output261 .= $viewHelper273->initializeArgumentsAndRender();

$output261 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments274 = array();
$renderChildrenClosure275 = function() use ($renderingContext, $self) {
$output276 = '';

$output276 .= '
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments277 = array();
$arguments277['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments278 = array();
$arguments278['id'] = 'media.tooltip.thumbnailView';
$arguments278['source'] = 'Modules';
$arguments278['package'] = 'TYPO3.Neos';
$arguments278['value'] = NULL;
$arguments278['arguments'] = array (
);
$arguments278['quantity'] = NULL;
$arguments278['languageIdentifier'] = NULL;
$renderChildrenClosure279 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper280 = $self->getViewHelper('$viewHelper280', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper280->setArguments($arguments278);
$viewHelper280->setRenderingContext($renderingContext);
$viewHelper280->setRenderChildrenClosure($renderChildrenClosure279);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments277['title'] = $viewHelper280->initializeArgumentsAndRender();
// Rendering Array
$array281 = array();
$array281['view'] = 'Thumbnail';
$arguments277['arguments'] = $array281;
// Rendering Boolean node
$arguments277['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering Array
$array282 = array();
$array282['neos-toggle'] = 'tooltip';
$arguments277['data'] = $array282;
$arguments277['additionalAttributes'] = NULL;
$arguments277['controller'] = NULL;
$arguments277['package'] = NULL;
$arguments277['subpackage'] = NULL;
$arguments277['section'] = '';
$arguments277['format'] = '';
$arguments277['additionalParams'] = array (
);
$arguments277['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments277['useParentRequest'] = false;
$arguments277['absolute'] = true;
$arguments277['class'] = NULL;
$arguments277['dir'] = NULL;
$arguments277['id'] = NULL;
$arguments277['lang'] = NULL;
$arguments277['style'] = NULL;
$arguments277['accesskey'] = NULL;
$arguments277['tabindex'] = NULL;
$arguments277['onclick'] = NULL;
$arguments277['name'] = NULL;
$arguments277['rel'] = NULL;
$arguments277['rev'] = NULL;
$arguments277['target'] = NULL;
$renderChildrenClosure283 = function() use ($renderingContext, $self) {
return '<i class="icon-th"></i>';
};
$viewHelper284 = $self->getViewHelper('$viewHelper284', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper284->setArguments($arguments277);
$viewHelper284->setRenderingContext($renderingContext);
$viewHelper284->setRenderChildrenClosure($renderChildrenClosure283);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output276 .= $viewHelper284->initializeArgumentsAndRender();

$output276 .= '
			';
return $output276;
};
$viewHelper285 = $self->getViewHelper('$viewHelper285', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper285->setArguments($arguments274);
$viewHelper285->setRenderingContext($renderingContext);
$viewHelper285->setRenderChildrenClosure($renderChildrenClosure275);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output261 .= $viewHelper285->initializeArgumentsAndRender();

$output261 .= '
		';
return $output261;
};
$arguments259['__thenClosure'] = function() use ($renderingContext, $self) {
$output286 = '';

$output286 .= '
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments287 = array();
$arguments287['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments288 = array();
$arguments288['id'] = 'media.tooltip.listView';
$arguments288['source'] = 'Modules';
$arguments288['package'] = 'TYPO3.Neos';
$arguments288['value'] = NULL;
$arguments288['arguments'] = array (
);
$arguments288['quantity'] = NULL;
$arguments288['languageIdentifier'] = NULL;
$renderChildrenClosure289 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper290 = $self->getViewHelper('$viewHelper290', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper290->setArguments($arguments288);
$viewHelper290->setRenderingContext($renderingContext);
$viewHelper290->setRenderChildrenClosure($renderChildrenClosure289);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments287['title'] = $viewHelper290->initializeArgumentsAndRender();
// Rendering Array
$array291 = array();
$array291['view'] = 'List';
$arguments287['arguments'] = $array291;
// Rendering Boolean node
$arguments287['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering Array
$array292 = array();
$array292['neos-toggle'] = 'tooltip';
$arguments287['data'] = $array292;
$arguments287['additionalAttributes'] = NULL;
$arguments287['controller'] = NULL;
$arguments287['package'] = NULL;
$arguments287['subpackage'] = NULL;
$arguments287['section'] = '';
$arguments287['format'] = '';
$arguments287['additionalParams'] = array (
);
$arguments287['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments287['useParentRequest'] = false;
$arguments287['absolute'] = true;
$arguments287['class'] = NULL;
$arguments287['dir'] = NULL;
$arguments287['id'] = NULL;
$arguments287['lang'] = NULL;
$arguments287['style'] = NULL;
$arguments287['accesskey'] = NULL;
$arguments287['tabindex'] = NULL;
$arguments287['onclick'] = NULL;
$arguments287['name'] = NULL;
$arguments287['rel'] = NULL;
$arguments287['rev'] = NULL;
$arguments287['target'] = NULL;
$renderChildrenClosure293 = function() use ($renderingContext, $self) {
return '<i class="icon-th-list"></i>';
};
$viewHelper294 = $self->getViewHelper('$viewHelper294', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper294->setArguments($arguments287);
$viewHelper294->setRenderingContext($renderingContext);
$viewHelper294->setRenderChildrenClosure($renderChildrenClosure293);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output286 .= $viewHelper294->initializeArgumentsAndRender();

$output286 .= '
			';
return $output286;
};
$arguments259['__elseClosure'] = function() use ($renderingContext, $self) {
$output295 = '';

$output295 .= '
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments296 = array();
$arguments296['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments297 = array();
$arguments297['id'] = 'media.tooltip.thumbnailView';
$arguments297['source'] = 'Modules';
$arguments297['package'] = 'TYPO3.Neos';
$arguments297['value'] = NULL;
$arguments297['arguments'] = array (
);
$arguments297['quantity'] = NULL;
$arguments297['languageIdentifier'] = NULL;
$renderChildrenClosure298 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper299 = $self->getViewHelper('$viewHelper299', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper299->setArguments($arguments297);
$viewHelper299->setRenderingContext($renderingContext);
$viewHelper299->setRenderChildrenClosure($renderChildrenClosure298);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments296['title'] = $viewHelper299->initializeArgumentsAndRender();
// Rendering Array
$array300 = array();
$array300['view'] = 'Thumbnail';
$arguments296['arguments'] = $array300;
// Rendering Boolean node
$arguments296['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering Array
$array301 = array();
$array301['neos-toggle'] = 'tooltip';
$arguments296['data'] = $array301;
$arguments296['additionalAttributes'] = NULL;
$arguments296['controller'] = NULL;
$arguments296['package'] = NULL;
$arguments296['subpackage'] = NULL;
$arguments296['section'] = '';
$arguments296['format'] = '';
$arguments296['additionalParams'] = array (
);
$arguments296['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments296['useParentRequest'] = false;
$arguments296['absolute'] = true;
$arguments296['class'] = NULL;
$arguments296['dir'] = NULL;
$arguments296['id'] = NULL;
$arguments296['lang'] = NULL;
$arguments296['style'] = NULL;
$arguments296['accesskey'] = NULL;
$arguments296['tabindex'] = NULL;
$arguments296['onclick'] = NULL;
$arguments296['name'] = NULL;
$arguments296['rel'] = NULL;
$arguments296['rev'] = NULL;
$arguments296['target'] = NULL;
$renderChildrenClosure302 = function() use ($renderingContext, $self) {
return '<i class="icon-th"></i>';
};
$viewHelper303 = $self->getViewHelper('$viewHelper303', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper303->setArguments($arguments296);
$viewHelper303->setRenderingContext($renderingContext);
$viewHelper303->setRenderChildrenClosure($renderChildrenClosure302);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output295 .= $viewHelper303->initializeArgumentsAndRender();

$output295 .= '
			';
return $output295;
};
$viewHelper304 = $self->getViewHelper('$viewHelper304', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper304->setArguments($arguments259);
$viewHelper304->setRenderingContext($renderingContext);
$viewHelper304->setRenderChildrenClosure($renderChildrenClosure260);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output0 .= $viewHelper304->initializeArgumentsAndRender();

$output0 .= '
	</div>
';

return $output0;
}
/**
 * section Sidebar
 */
public function section_f5171c931c5c70d4dc3557fd20c356b636c92e04(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output305 = '';

$output305 .= '
	<form action="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments306 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments307 = array();
$arguments307['action'] = 'index';
$arguments307['arguments'] = array (
);
$arguments307['controller'] = NULL;
$arguments307['package'] = NULL;
$arguments307['subpackage'] = NULL;
$arguments307['section'] = '';
$arguments307['format'] = '';
$arguments307['additionalParams'] = array (
);
$arguments307['absolute'] = false;
$arguments307['addQueryString'] = false;
$arguments307['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments307['useParentRequest'] = false;
$renderChildrenClosure308 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper309 = $self->getViewHelper('$viewHelper309', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper309->setArguments($arguments307);
$viewHelper309->setRenderingContext($renderingContext);
$viewHelper309->setRenderChildrenClosure($renderChildrenClosure308);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments306['value'] = $viewHelper309->initializeArgumentsAndRender();
$arguments306['keepQuotes'] = false;
$arguments306['encoding'] = 'UTF-8';
$arguments306['doubleEncode'] = true;
$renderChildrenClosure310 = function() use ($renderingContext, $self) {
return NULL;
};
$value311 = ($arguments306['value'] !== NULL ? $arguments306['value'] : $renderChildrenClosure310());

$output305 .= !is_string($value311) && !(is_object($value311) && method_exists($value311, '__toString')) ? $value311 : htmlspecialchars($value311, ($arguments306['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments306['encoding'], $arguments306['doubleEncode']);

$output305 .= '" method="get" class="neos-search">
		<button type="submit" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments312 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments313 = array();
$arguments313['id'] = 'media.search.title';
$arguments313['source'] = 'Modules';
$arguments313['package'] = 'TYPO3.Neos';
$arguments313['value'] = NULL;
$arguments313['arguments'] = array (
);
$arguments313['quantity'] = NULL;
$arguments313['languageIdentifier'] = NULL;
$renderChildrenClosure314 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper315 = $self->getViewHelper('$viewHelper315', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper315->setArguments($arguments313);
$viewHelper315->setRenderingContext($renderingContext);
$viewHelper315->setRenderChildrenClosure($renderChildrenClosure314);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments312['value'] = $viewHelper315->initializeArgumentsAndRender();
$arguments312['keepQuotes'] = false;
$arguments312['encoding'] = 'UTF-8';
$arguments312['doubleEncode'] = true;
$renderChildrenClosure316 = function() use ($renderingContext, $self) {
return NULL;
};
$value317 = ($arguments312['value'] !== NULL ? $arguments312['value'] : $renderChildrenClosure316());

$output305 .= !is_string($value317) && !(is_object($value317) && method_exists($value317, '__toString')) ? $value317 : htmlspecialchars($value317, ($arguments312['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments312['encoding'], $arguments312['doubleEncode']);

$output305 .= '" data-neos-toggle="tooltip"><i class="icon-search"></i></button>
		<div>
			<input type="search" name="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments318 = array();
// Rendering Boolean node
$arguments318['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'argumentNamespace', $renderingContext));
$output319 = '';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments320 = array();
$arguments320['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'argumentNamespace', $renderingContext);
$arguments320['keepQuotes'] = false;
$arguments320['encoding'] = 'UTF-8';
$arguments320['doubleEncode'] = true;
$renderChildrenClosure321 = function() use ($renderingContext, $self) {
return NULL;
};
$value322 = ($arguments320['value'] !== NULL ? $arguments320['value'] : $renderChildrenClosure321());

$output319 .= !is_string($value322) && !(is_object($value322) && method_exists($value322, '__toString')) ? $value322 : htmlspecialchars($value322, ($arguments320['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments320['encoding'], $arguments320['doubleEncode']);

$output319 .= '[searchTerm]';
$arguments318['then'] = $output319;
$arguments318['else'] = 'searchTerm';
$renderChildrenClosure323 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper324 = $self->getViewHelper('$viewHelper324', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper324->setArguments($arguments318);
$viewHelper324->setRenderingContext($renderingContext);
$viewHelper324->setRenderChildrenClosure($renderChildrenClosure323);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output305 .= $viewHelper324->initializeArgumentsAndRender();

$output305 .= '" placeholder="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments325 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments326 = array();
$arguments326['id'] = 'media.search.placeholder';
$arguments326['source'] = 'Modules';
$arguments326['package'] = 'TYPO3.Neos';
$arguments326['value'] = NULL;
$arguments326['arguments'] = array (
);
$arguments326['quantity'] = NULL;
$arguments326['languageIdentifier'] = NULL;
$renderChildrenClosure327 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper328 = $self->getViewHelper('$viewHelper328', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper328->setArguments($arguments326);
$viewHelper328->setRenderingContext($renderingContext);
$viewHelper328->setRenderChildrenClosure($renderChildrenClosure327);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments325['value'] = $viewHelper328->initializeArgumentsAndRender();
$arguments325['keepQuotes'] = false;
$arguments325['encoding'] = 'UTF-8';
$arguments325['doubleEncode'] = true;
$renderChildrenClosure329 = function() use ($renderingContext, $self) {
return NULL;
};
$value330 = ($arguments325['value'] !== NULL ? $arguments325['value'] : $renderChildrenClosure329());

$output305 .= !is_string($value330) && !(is_object($value330) && method_exists($value330, '__toString')) ? $value330 : htmlspecialchars($value330, ($arguments325['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments325['encoding'], $arguments325['doubleEncode']);

$output305 .= '" value="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments331 = array();
$arguments331['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'searchTerm', $renderingContext);
$arguments331['keepQuotes'] = false;
$arguments331['encoding'] = 'UTF-8';
$arguments331['doubleEncode'] = true;
$renderChildrenClosure332 = function() use ($renderingContext, $self) {
return NULL;
};
$value333 = ($arguments331['value'] !== NULL ? $arguments331['value'] : $renderChildrenClosure332());

$output305 .= !is_string($value333) && !(is_object($value333) && method_exists($value333, '__toString')) ? $value333 : htmlspecialchars($value333, ($arguments331['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments331['encoding'], $arguments331['doubleEncode']);

$output305 .= '"';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments334 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments335 = array();
$arguments335['subject'] = NULL;
$renderChildrenClosure336 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tags', $renderingContext);
};
$viewHelper337 = $self->getViewHelper('$viewHelper337', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper337->setArguments($arguments335);
$viewHelper337->setRenderingContext($renderingContext);
$viewHelper337->setRenderChildrenClosure($renderChildrenClosure336);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments334['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('<=', $viewHelper337->initializeArgumentsAndRender(), 25);
$arguments334['then'] = ' autofocus="autofocus"';
$arguments334['else'] = NULL;
$renderChildrenClosure338 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper339 = $self->getViewHelper('$viewHelper339', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper339->setArguments($arguments334);
$viewHelper339->setRenderingContext($renderingContext);
$viewHelper339->setRenderChildrenClosure($renderChildrenClosure338);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output305 .= $viewHelper339->initializeArgumentsAndRender();

$output305 .= ' />
		</div>
	</form>
	<div class="neos-media-aside-group">
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Security\IfAccessViewHelper
$arguments340 = array();
$arguments340['privilegeTarget'] = 'TYPO3.Media:ManageAssetCollections';
$arguments340['then'] = NULL;
$arguments340['else'] = NULL;
$arguments340['parameters'] = array (
);
$renderChildrenClosure341 = function() use ($renderingContext, $self) {
$output342 = '';

$output342 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments343 = array();
$renderChildrenClosure344 = function() use ($renderingContext, $self) {
$output345 = '';

$output345 .= '
				<h2>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments346 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments347 = array();
$arguments347['id'] = 'media.collections';
$arguments347['source'] = 'Modules';
$arguments347['package'] = 'TYPO3.Neos';
$arguments347['value'] = NULL;
$arguments347['arguments'] = array (
);
$arguments347['quantity'] = NULL;
$arguments347['languageIdentifier'] = NULL;
$renderChildrenClosure348 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper349 = $self->getViewHelper('$viewHelper349', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper349->setArguments($arguments347);
$viewHelper349->setRenderingContext($renderingContext);
$viewHelper349->setRenderChildrenClosure($renderChildrenClosure348);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments346['value'] = $viewHelper349->initializeArgumentsAndRender();
$arguments346['keepQuotes'] = false;
$arguments346['encoding'] = 'UTF-8';
$arguments346['doubleEncode'] = true;
$renderChildrenClosure350 = function() use ($renderingContext, $self) {
return NULL;
};
$value351 = ($arguments346['value'] !== NULL ? $arguments346['value'] : $renderChildrenClosure350());

$output345 .= !is_string($value351) && !(is_object($value351) && method_exists($value351, '__toString')) ? $value351 : htmlspecialchars($value351, ($arguments346['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments346['encoding'], $arguments346['doubleEncode']);

$output345 .= '
					<span class="neos-media-aside-list-edit-toggle neos-button" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments352 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments353 = array();
$arguments353['id'] = 'media.editCollections';
$arguments353['source'] = 'Modules';
$arguments353['package'] = 'TYPO3.Neos';
$arguments353['value'] = NULL;
$arguments353['arguments'] = array (
);
$arguments353['quantity'] = NULL;
$arguments353['languageIdentifier'] = NULL;
$renderChildrenClosure354 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper355 = $self->getViewHelper('$viewHelper355', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper355->setArguments($arguments353);
$viewHelper355->setRenderingContext($renderingContext);
$viewHelper355->setRenderChildrenClosure($renderChildrenClosure354);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments352['value'] = $viewHelper355->initializeArgumentsAndRender();
$arguments352['keepQuotes'] = false;
$arguments352['encoding'] = 'UTF-8';
$arguments352['doubleEncode'] = true;
$renderChildrenClosure356 = function() use ($renderingContext, $self) {
return NULL;
};
$value357 = ($arguments352['value'] !== NULL ? $arguments352['value'] : $renderChildrenClosure356());

$output345 .= !is_string($value357) && !(is_object($value357) && method_exists($value357, '__toString')) ? $value357 : htmlspecialchars($value357, ($arguments352['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments352['encoding'], $arguments352['doubleEncode']);

$output345 .= '" data-neos-toggle="tooltip"><i class="icon-pencil"></i></span>
				</h2>
			';
return $output345;
};
$viewHelper358 = $self->getViewHelper('$viewHelper358', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper358->setArguments($arguments343);
$viewHelper358->setRenderingContext($renderingContext);
$viewHelper358->setRenderChildrenClosure($renderChildrenClosure344);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output342 .= $viewHelper358->initializeArgumentsAndRender();

$output342 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments359 = array();
$renderChildrenClosure360 = function() use ($renderingContext, $self) {
$output361 = '';

$output361 .= '
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments362 = array();
// Rendering Boolean node
$arguments362['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollections', $renderingContext));
$arguments362['then'] = NULL;
$arguments362['else'] = NULL;
$renderChildrenClosure363 = function() use ($renderingContext, $self) {
$output364 = '';

$output364 .= '
					<h2>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments365 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments366 = array();
$arguments366['id'] = 'media.collections';
$arguments366['source'] = 'Modules';
$arguments366['package'] = 'TYPO3.Neos';
$arguments366['value'] = NULL;
$arguments366['arguments'] = array (
);
$arguments366['quantity'] = NULL;
$arguments366['languageIdentifier'] = NULL;
$renderChildrenClosure367 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper368 = $self->getViewHelper('$viewHelper368', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper368->setArguments($arguments366);
$viewHelper368->setRenderingContext($renderingContext);
$viewHelper368->setRenderChildrenClosure($renderChildrenClosure367);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments365['value'] = $viewHelper368->initializeArgumentsAndRender();
$arguments365['keepQuotes'] = false;
$arguments365['encoding'] = 'UTF-8';
$arguments365['doubleEncode'] = true;
$renderChildrenClosure369 = function() use ($renderingContext, $self) {
return NULL;
};
$value370 = ($arguments365['value'] !== NULL ? $arguments365['value'] : $renderChildrenClosure369());

$output364 .= !is_string($value370) && !(is_object($value370) && method_exists($value370, '__toString')) ? $value370 : htmlspecialchars($value370, ($arguments365['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments365['encoding'], $arguments365['doubleEncode']);

$output364 .= '</h2>
				';
return $output364;
};
$viewHelper371 = $self->getViewHelper('$viewHelper371', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper371->setArguments($arguments362);
$viewHelper371->setRenderingContext($renderingContext);
$viewHelper371->setRenderChildrenClosure($renderChildrenClosure363);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output361 .= $viewHelper371->initializeArgumentsAndRender();

$output361 .= '
			';
return $output361;
};
$viewHelper372 = $self->getViewHelper('$viewHelper372', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper372->setArguments($arguments359);
$viewHelper372->setRenderingContext($renderingContext);
$viewHelper372->setRenderChildrenClosure($renderChildrenClosure360);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output342 .= $viewHelper372->initializeArgumentsAndRender();

$output342 .= '
		';
return $output342;
};
$arguments340['__thenClosure'] = function() use ($renderingContext, $self) {
$output373 = '';

$output373 .= '
				<h2>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments374 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments375 = array();
$arguments375['id'] = 'media.collections';
$arguments375['source'] = 'Modules';
$arguments375['package'] = 'TYPO3.Neos';
$arguments375['value'] = NULL;
$arguments375['arguments'] = array (
);
$arguments375['quantity'] = NULL;
$arguments375['languageIdentifier'] = NULL;
$renderChildrenClosure376 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper377 = $self->getViewHelper('$viewHelper377', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper377->setArguments($arguments375);
$viewHelper377->setRenderingContext($renderingContext);
$viewHelper377->setRenderChildrenClosure($renderChildrenClosure376);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments374['value'] = $viewHelper377->initializeArgumentsAndRender();
$arguments374['keepQuotes'] = false;
$arguments374['encoding'] = 'UTF-8';
$arguments374['doubleEncode'] = true;
$renderChildrenClosure378 = function() use ($renderingContext, $self) {
return NULL;
};
$value379 = ($arguments374['value'] !== NULL ? $arguments374['value'] : $renderChildrenClosure378());

$output373 .= !is_string($value379) && !(is_object($value379) && method_exists($value379, '__toString')) ? $value379 : htmlspecialchars($value379, ($arguments374['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments374['encoding'], $arguments374['doubleEncode']);

$output373 .= '
					<span class="neos-media-aside-list-edit-toggle neos-button" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments380 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments381 = array();
$arguments381['id'] = 'media.editCollections';
$arguments381['source'] = 'Modules';
$arguments381['package'] = 'TYPO3.Neos';
$arguments381['value'] = NULL;
$arguments381['arguments'] = array (
);
$arguments381['quantity'] = NULL;
$arguments381['languageIdentifier'] = NULL;
$renderChildrenClosure382 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper383 = $self->getViewHelper('$viewHelper383', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper383->setArguments($arguments381);
$viewHelper383->setRenderingContext($renderingContext);
$viewHelper383->setRenderChildrenClosure($renderChildrenClosure382);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments380['value'] = $viewHelper383->initializeArgumentsAndRender();
$arguments380['keepQuotes'] = false;
$arguments380['encoding'] = 'UTF-8';
$arguments380['doubleEncode'] = true;
$renderChildrenClosure384 = function() use ($renderingContext, $self) {
return NULL;
};
$value385 = ($arguments380['value'] !== NULL ? $arguments380['value'] : $renderChildrenClosure384());

$output373 .= !is_string($value385) && !(is_object($value385) && method_exists($value385, '__toString')) ? $value385 : htmlspecialchars($value385, ($arguments380['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments380['encoding'], $arguments380['doubleEncode']);

$output373 .= '" data-neos-toggle="tooltip"><i class="icon-pencil"></i></span>
				</h2>
			';
return $output373;
};
$arguments340['__elseClosure'] = function() use ($renderingContext, $self) {
$output386 = '';

$output386 .= '
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments387 = array();
// Rendering Boolean node
$arguments387['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollections', $renderingContext));
$arguments387['then'] = NULL;
$arguments387['else'] = NULL;
$renderChildrenClosure388 = function() use ($renderingContext, $self) {
$output389 = '';

$output389 .= '
					<h2>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments390 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments391 = array();
$arguments391['id'] = 'media.collections';
$arguments391['source'] = 'Modules';
$arguments391['package'] = 'TYPO3.Neos';
$arguments391['value'] = NULL;
$arguments391['arguments'] = array (
);
$arguments391['quantity'] = NULL;
$arguments391['languageIdentifier'] = NULL;
$renderChildrenClosure392 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper393 = $self->getViewHelper('$viewHelper393', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper393->setArguments($arguments391);
$viewHelper393->setRenderingContext($renderingContext);
$viewHelper393->setRenderChildrenClosure($renderChildrenClosure392);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments390['value'] = $viewHelper393->initializeArgumentsAndRender();
$arguments390['keepQuotes'] = false;
$arguments390['encoding'] = 'UTF-8';
$arguments390['doubleEncode'] = true;
$renderChildrenClosure394 = function() use ($renderingContext, $self) {
return NULL;
};
$value395 = ($arguments390['value'] !== NULL ? $arguments390['value'] : $renderChildrenClosure394());

$output389 .= !is_string($value395) && !(is_object($value395) && method_exists($value395, '__toString')) ? $value395 : htmlspecialchars($value395, ($arguments390['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments390['encoding'], $arguments390['doubleEncode']);

$output389 .= '</h2>
				';
return $output389;
};
$viewHelper396 = $self->getViewHelper('$viewHelper396', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper396->setArguments($arguments387);
$viewHelper396->setRenderingContext($renderingContext);
$viewHelper396->setRenderChildrenClosure($renderChildrenClosure388);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output386 .= $viewHelper396->initializeArgumentsAndRender();

$output386 .= '
			';
return $output386;
};
$viewHelper397 = $self->getViewHelper('$viewHelper397', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Security\IfAccessViewHelper');
$viewHelper397->setArguments($arguments340);
$viewHelper397->setRenderingContext($renderingContext);
$viewHelper397->setRenderChildrenClosure($renderChildrenClosure341);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Security\IfAccessViewHelper

$output305 .= $viewHelper397->initializeArgumentsAndRender();

$output305 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments398 = array();
// Rendering Boolean node
$arguments398['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollections', $renderingContext));
$arguments398['then'] = NULL;
$arguments398['else'] = NULL;
$renderChildrenClosure399 = function() use ($renderingContext, $self) {
$output400 = '';

$output400 .= '
			<ul class="neos-media-aside-list">
				<li>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments401 = array();
$arguments401['action'] = 'index';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments402 = array();
// Rendering Boolean node
$arguments402['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'activeAssetCollection', $renderingContext));
$arguments402['else'] = ' neos-active';
$arguments402['then'] = NULL;
$renderChildrenClosure403 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper404 = $self->getViewHelper('$viewHelper404', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper404->setArguments($arguments402);
$viewHelper404->setRenderingContext($renderingContext);
$viewHelper404->setRenderChildrenClosure($renderChildrenClosure403);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments401['class'] = $viewHelper404->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments405 = array();
$arguments405['id'] = 'media.allCollections';
$arguments405['source'] = 'Modules';
$arguments405['package'] = 'TYPO3.Neos';
$arguments405['value'] = NULL;
$arguments405['arguments'] = array (
);
$arguments405['quantity'] = NULL;
$arguments405['languageIdentifier'] = NULL;
$renderChildrenClosure406 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper407 = $self->getViewHelper('$viewHelper407', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper407->setArguments($arguments405);
$viewHelper407->setRenderingContext($renderingContext);
$viewHelper407->setRenderChildrenClosure($renderChildrenClosure406);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments401['title'] = $viewHelper407->initializeArgumentsAndRender();
// Rendering Array
$array408 = array();
$array408['view'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'view', $renderingContext);
$array408['collectionMode'] = 1;
$arguments401['arguments'] = $array408;
$arguments401['additionalAttributes'] = NULL;
$arguments401['data'] = NULL;
$arguments401['controller'] = NULL;
$arguments401['package'] = NULL;
$arguments401['subpackage'] = NULL;
$arguments401['section'] = '';
$arguments401['format'] = '';
$arguments401['additionalParams'] = array (
);
$arguments401['addQueryString'] = false;
$arguments401['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments401['useParentRequest'] = false;
$arguments401['absolute'] = true;
$arguments401['dir'] = NULL;
$arguments401['id'] = NULL;
$arguments401['lang'] = NULL;
$arguments401['style'] = NULL;
$arguments401['accesskey'] = NULL;
$arguments401['tabindex'] = NULL;
$arguments401['onclick'] = NULL;
$arguments401['name'] = NULL;
$arguments401['rel'] = NULL;
$arguments401['rev'] = NULL;
$arguments401['target'] = NULL;
$renderChildrenClosure409 = function() use ($renderingContext, $self) {
$output410 = '';

$output410 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments411 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments412 = array();
$arguments412['id'] = 'media.filter.all';
$arguments412['source'] = 'Modules';
$arguments412['package'] = 'TYPO3.Neos';
$arguments412['value'] = NULL;
$arguments412['arguments'] = array (
);
$arguments412['quantity'] = NULL;
$arguments412['languageIdentifier'] = NULL;
$renderChildrenClosure413 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper414 = $self->getViewHelper('$viewHelper414', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper414->setArguments($arguments412);
$viewHelper414->setRenderingContext($renderingContext);
$viewHelper414->setRenderChildrenClosure($renderChildrenClosure413);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments411['value'] = $viewHelper414->initializeArgumentsAndRender();
$arguments411['keepQuotes'] = false;
$arguments411['encoding'] = 'UTF-8';
$arguments411['doubleEncode'] = true;
$renderChildrenClosure415 = function() use ($renderingContext, $self) {
return NULL;
};
$value416 = ($arguments411['value'] !== NULL ? $arguments411['value'] : $renderChildrenClosure415());

$output410 .= !is_string($value416) && !(is_object($value416) && method_exists($value416, '__toString')) ? $value416 : htmlspecialchars($value416, ($arguments411['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments411['encoding'], $arguments411['doubleEncode']);

$output410 .= '
						<span class="count">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments417 = array();
$arguments417['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'allCollectionsCount', $renderingContext);
$arguments417['keepQuotes'] = false;
$arguments417['encoding'] = 'UTF-8';
$arguments417['doubleEncode'] = true;
$renderChildrenClosure418 = function() use ($renderingContext, $self) {
return NULL;
};
$value419 = ($arguments417['value'] !== NULL ? $arguments417['value'] : $renderChildrenClosure418());

$output410 .= !is_string($value419) && !(is_object($value419) && method_exists($value419, '__toString')) ? $value419 : htmlspecialchars($value419, ($arguments417['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments417['encoding'], $arguments417['doubleEncode']);

$output410 .= '</span>
					';
return $output410;
};
$viewHelper420 = $self->getViewHelper('$viewHelper420', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper420->setArguments($arguments401);
$viewHelper420->setRenderingContext($renderingContext);
$viewHelper420->setRenderChildrenClosure($renderChildrenClosure409);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output400 .= $viewHelper420->initializeArgumentsAndRender();

$output400 .= '
				</li>
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments421 = array();
$arguments421['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollections', $renderingContext);
$arguments421['as'] = 'assetCollection';
$arguments421['key'] = '';
$arguments421['reverse'] = false;
$arguments421['iteration'] = NULL;
$renderChildrenClosure422 = function() use ($renderingContext, $self) {
$output423 = '';

$output423 .= '
					<li>
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments424 = array();
$arguments424['action'] = 'index';
$arguments424['title'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollection.object.title', $renderingContext);
$output425 = '';

$output425 .= 'droppable-assetcollection';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments426 = array();
// Rendering Boolean node
$arguments426['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollection.object', $renderingContext), \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'activeAssetCollection', $renderingContext));
$arguments426['then'] = ' neos-active';
$arguments426['else'] = NULL;
$renderChildrenClosure427 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper428 = $self->getViewHelper('$viewHelper428', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper428->setArguments($arguments426);
$viewHelper428->setRenderingContext($renderingContext);
$viewHelper428->setRenderChildrenClosure($renderChildrenClosure427);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output425 .= $viewHelper428->initializeArgumentsAndRender();
$arguments424['class'] = $output425;
// Rendering Array
$array429 = array();
$array429['view'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'view', $renderingContext);
$array429['assetCollection'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollection.object', $renderingContext);
$arguments424['arguments'] = $array429;
// Rendering Array
$array430 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper
$arguments431 = array();
$arguments431['value'] = NULL;
$renderChildrenClosure432 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollection.object', $renderingContext);
};
$viewHelper433 = $self->getViewHelper('$viewHelper433', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper');
$viewHelper433->setArguments($arguments431);
$viewHelper433->setRenderingContext($renderingContext);
$viewHelper433->setRenderChildrenClosure($renderChildrenClosure432);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper
$array430['assetcollection-identifier'] = $viewHelper433->initializeArgumentsAndRender();
$arguments424['data'] = $array430;
$arguments424['additionalAttributes'] = NULL;
$arguments424['controller'] = NULL;
$arguments424['package'] = NULL;
$arguments424['subpackage'] = NULL;
$arguments424['section'] = '';
$arguments424['format'] = '';
$arguments424['additionalParams'] = array (
);
$arguments424['addQueryString'] = false;
$arguments424['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments424['useParentRequest'] = false;
$arguments424['absolute'] = true;
$arguments424['dir'] = NULL;
$arguments424['id'] = NULL;
$arguments424['lang'] = NULL;
$arguments424['style'] = NULL;
$arguments424['accesskey'] = NULL;
$arguments424['tabindex'] = NULL;
$arguments424['onclick'] = NULL;
$arguments424['name'] = NULL;
$arguments424['rel'] = NULL;
$arguments424['rev'] = NULL;
$arguments424['target'] = NULL;
$renderChildrenClosure434 = function() use ($renderingContext, $self) {
$output435 = '';

$output435 .= '
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments436 = array();
$arguments436['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollection.object.title', $renderingContext);
$arguments436['keepQuotes'] = false;
$arguments436['encoding'] = 'UTF-8';
$arguments436['doubleEncode'] = true;
$renderChildrenClosure437 = function() use ($renderingContext, $self) {
return NULL;
};
$value438 = ($arguments436['value'] !== NULL ? $arguments436['value'] : $renderChildrenClosure437());

$output435 .= !is_string($value438) && !(is_object($value438) && method_exists($value438, '__toString')) ? $value438 : htmlspecialchars($value438, ($arguments436['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments436['encoding'], $arguments436['doubleEncode']);

$output435 .= '
							<span class="count">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments439 = array();
$arguments439['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollection.count', $renderingContext);
$arguments439['keepQuotes'] = false;
$arguments439['encoding'] = 'UTF-8';
$arguments439['doubleEncode'] = true;
$renderChildrenClosure440 = function() use ($renderingContext, $self) {
return NULL;
};
$value441 = ($arguments439['value'] !== NULL ? $arguments439['value'] : $renderChildrenClosure440());

$output435 .= !is_string($value441) && !(is_object($value441) && method_exists($value441, '__toString')) ? $value441 : htmlspecialchars($value441, ($arguments439['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments439['encoding'], $arguments439['doubleEncode']);

$output435 .= '</span>
						';
return $output435;
};
$viewHelper442 = $self->getViewHelper('$viewHelper442', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper442->setArguments($arguments424);
$viewHelper442->setRenderingContext($renderingContext);
$viewHelper442->setRenderChildrenClosure($renderChildrenClosure434);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output423 .= $viewHelper442->initializeArgumentsAndRender();

$output423 .= '
						<div class="neos-sidelist-edit-actions">
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments443 = array();
$arguments443['class'] = 'neos-button';
$arguments443['action'] = 'editAssetCollection';
// Rendering Array
$array444 = array();
$array444['assetCollection'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollection.object', $renderingContext);
$arguments443['arguments'] = $array444;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments445 = array();
$arguments445['id'] = 'media.editCollection';
$arguments445['source'] = 'Modules';
$arguments445['package'] = 'TYPO3.Neos';
$arguments445['value'] = NULL;
$arguments445['arguments'] = array (
);
$arguments445['quantity'] = NULL;
$arguments445['languageIdentifier'] = NULL;
$renderChildrenClosure446 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper447 = $self->getViewHelper('$viewHelper447', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper447->setArguments($arguments445);
$viewHelper447->setRenderingContext($renderingContext);
$viewHelper447->setRenderChildrenClosure($renderChildrenClosure446);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments443['title'] = $viewHelper447->initializeArgumentsAndRender();
// Rendering Array
$array448 = array();
$array448['neos-toggle'] = 'tooltip';
$arguments443['data'] = $array448;
$arguments443['additionalAttributes'] = NULL;
$arguments443['controller'] = NULL;
$arguments443['package'] = NULL;
$arguments443['subpackage'] = NULL;
$arguments443['section'] = '';
$arguments443['format'] = '';
$arguments443['additionalParams'] = array (
);
$arguments443['addQueryString'] = false;
$arguments443['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments443['useParentRequest'] = false;
$arguments443['absolute'] = true;
$arguments443['dir'] = NULL;
$arguments443['id'] = NULL;
$arguments443['lang'] = NULL;
$arguments443['style'] = NULL;
$arguments443['accesskey'] = NULL;
$arguments443['tabindex'] = NULL;
$arguments443['onclick'] = NULL;
$arguments443['name'] = NULL;
$arguments443['rel'] = NULL;
$arguments443['rev'] = NULL;
$arguments443['target'] = NULL;
$renderChildrenClosure449 = function() use ($renderingContext, $self) {
return '<i class="icon-pencil"></i>';
};
$viewHelper450 = $self->getViewHelper('$viewHelper450', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper450->setArguments($arguments443);
$viewHelper450->setRenderingContext($renderingContext);
$viewHelper450->setRenderChildrenClosure($renderChildrenClosure449);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output423 .= $viewHelper450->initializeArgumentsAndRender();

$output423 .= '
							<button type="submit" class="neos-button-danger" data-modal="delete-assetcollection-modal" data-object-identifier="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments451 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper
$arguments452 = array();
$arguments452['value'] = NULL;
$renderChildrenClosure453 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollection.object', $renderingContext);
};
$viewHelper454 = $self->getViewHelper('$viewHelper454', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper');
$viewHelper454->setArguments($arguments452);
$viewHelper454->setRenderingContext($renderingContext);
$viewHelper454->setRenderChildrenClosure($renderChildrenClosure453);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper
$arguments451['value'] = $viewHelper454->initializeArgumentsAndRender();
$arguments451['keepQuotes'] = false;
$arguments451['encoding'] = 'UTF-8';
$arguments451['doubleEncode'] = true;
$renderChildrenClosure455 = function() use ($renderingContext, $self) {
return NULL;
};
$value456 = ($arguments451['value'] !== NULL ? $arguments451['value'] : $renderChildrenClosure455());

$output423 .= !is_string($value456) && !(is_object($value456) && method_exists($value456, '__toString')) ? $value456 : htmlspecialchars($value456, ($arguments451['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments451['encoding'], $arguments451['doubleEncode']);

$output423 .= '" data-label="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments457 = array();
$arguments457['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollection.object.title', $renderingContext);
$arguments457['keepQuotes'] = false;
$arguments457['encoding'] = 'UTF-8';
$arguments457['doubleEncode'] = true;
$renderChildrenClosure458 = function() use ($renderingContext, $self) {
return NULL;
};
$value459 = ($arguments457['value'] !== NULL ? $arguments457['value'] : $renderChildrenClosure458());

$output423 .= !is_string($value459) && !(is_object($value459) && method_exists($value459, '__toString')) ? $value459 : htmlspecialchars($value459, ($arguments457['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments457['encoding'], $arguments457['doubleEncode']);

$output423 .= '" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments460 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments461 = array();
$arguments461['id'] = 'media.deleteCollection';
$arguments461['source'] = 'Modules';
$arguments461['package'] = 'TYPO3.Neos';
$arguments461['value'] = NULL;
$arguments461['arguments'] = array (
);
$arguments461['quantity'] = NULL;
$arguments461['languageIdentifier'] = NULL;
$renderChildrenClosure462 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper463 = $self->getViewHelper('$viewHelper463', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper463->setArguments($arguments461);
$viewHelper463->setRenderingContext($renderingContext);
$viewHelper463->setRenderChildrenClosure($renderChildrenClosure462);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments460['value'] = $viewHelper463->initializeArgumentsAndRender();
$arguments460['keepQuotes'] = false;
$arguments460['encoding'] = 'UTF-8';
$arguments460['doubleEncode'] = true;
$renderChildrenClosure464 = function() use ($renderingContext, $self) {
return NULL;
};
$value465 = ($arguments460['value'] !== NULL ? $arguments460['value'] : $renderChildrenClosure464());

$output423 .= !is_string($value465) && !(is_object($value465) && method_exists($value465, '__toString')) ? $value465 : htmlspecialchars($value465, ($arguments460['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments460['encoding'], $arguments460['doubleEncode']);

$output423 .= '" data-neos-toggle="tooltip"><i class="icon-trash"></i></button>
						</div>
					</li>
				';
return $output423;
};

$output400 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments421, $renderChildrenClosure422, $renderingContext);

$output400 .= '
			</ul>
			<div class="neos-hide" id="delete-assetcollection-modal">
				<div class="neos-modal-centered">
					<div class="neos-modal-content">
						<div class="neos-modal-header">
							<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
							<div class="neos-header">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments466 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments467 = array();
$arguments467['id'] = 'media.message.reallyDeleteCollection';
$arguments467['source'] = 'Modules';
$arguments467['package'] = 'TYPO3.Neos';
$arguments467['value'] = NULL;
$arguments467['arguments'] = array (
);
$arguments467['quantity'] = NULL;
$arguments467['languageIdentifier'] = NULL;
$renderChildrenClosure468 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper469 = $self->getViewHelper('$viewHelper469', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper469->setArguments($arguments467);
$viewHelper469->setRenderingContext($renderingContext);
$viewHelper469->setRenderChildrenClosure($renderChildrenClosure468);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments466['value'] = $viewHelper469->initializeArgumentsAndRender();
$arguments466['keepQuotes'] = false;
$arguments466['encoding'] = 'UTF-8';
$arguments466['doubleEncode'] = true;
$renderChildrenClosure470 = function() use ($renderingContext, $self) {
return NULL;
};
$value471 = ($arguments466['value'] !== NULL ? $arguments466['value'] : $renderChildrenClosure470());

$output400 .= !is_string($value471) && !(is_object($value471) && method_exists($value471, '__toString')) ? $value471 : htmlspecialchars($value471, ($arguments466['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments466['encoding'], $arguments466['doubleEncode']);

$output400 .= '</div>
							<div>
								<div class="neos-subheader">
									<p>
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments472 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments473 = array();
$arguments473['id'] = 'media.message.willDeleteCollection';
$arguments473['source'] = 'Modules';
$arguments473['package'] = 'TYPO3.Neos';
$arguments473['value'] = NULL;
$arguments473['arguments'] = array (
);
$arguments473['quantity'] = NULL;
$arguments473['languageIdentifier'] = NULL;
$renderChildrenClosure474 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper475 = $self->getViewHelper('$viewHelper475', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper475->setArguments($arguments473);
$viewHelper475->setRenderingContext($renderingContext);
$viewHelper475->setRenderChildrenClosure($renderChildrenClosure474);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments472['value'] = $viewHelper475->initializeArgumentsAndRender();
$arguments472['keepQuotes'] = false;
$arguments472['encoding'] = 'UTF-8';
$arguments472['doubleEncode'] = true;
$renderChildrenClosure476 = function() use ($renderingContext, $self) {
return NULL;
};
$value477 = ($arguments472['value'] !== NULL ? $arguments472['value'] : $renderChildrenClosure476());

$output400 .= !is_string($value477) && !(is_object($value477) && method_exists($value477, '__toString')) ? $value477 : htmlspecialchars($value477, ($arguments472['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments472['encoding'], $arguments472['doubleEncode']);

$output400 .= '<br />
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments478 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments479 = array();
$arguments479['id'] = 'media.message.operationCannotBeUndone';
$arguments479['source'] = 'Modules';
$arguments479['package'] = 'TYPO3.Neos';
$arguments479['value'] = NULL;
$arguments479['arguments'] = array (
);
$arguments479['quantity'] = NULL;
$arguments479['languageIdentifier'] = NULL;
$renderChildrenClosure480 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper481 = $self->getViewHelper('$viewHelper481', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper481->setArguments($arguments479);
$viewHelper481->setRenderingContext($renderingContext);
$viewHelper481->setRenderChildrenClosure($renderChildrenClosure480);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments478['value'] = $viewHelper481->initializeArgumentsAndRender();
$arguments478['keepQuotes'] = false;
$arguments478['encoding'] = 'UTF-8';
$arguments478['doubleEncode'] = true;
$renderChildrenClosure482 = function() use ($renderingContext, $self) {
return NULL;
};
$value483 = ($arguments478['value'] !== NULL ? $arguments478['value'] : $renderChildrenClosure482());

$output400 .= !is_string($value483) && !(is_object($value483) && method_exists($value483, '__toString')) ? $value483 : htmlspecialchars($value483, ($arguments478['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments478['encoding'], $arguments478['doubleEncode']);

$output400 .= '
									</p>
								</div>
							</div>
						</div>
						<div class="neos-modal-footer">
							<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments484 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments485 = array();
$arguments485['id'] = 'media.cancel';
$arguments485['source'] = 'Modules';
$arguments485['package'] = 'TYPO3.Neos';
$arguments485['value'] = NULL;
$arguments485['arguments'] = array (
);
$arguments485['quantity'] = NULL;
$arguments485['languageIdentifier'] = NULL;
$renderChildrenClosure486 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper487 = $self->getViewHelper('$viewHelper487', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper487->setArguments($arguments485);
$viewHelper487->setRenderingContext($renderingContext);
$viewHelper487->setRenderChildrenClosure($renderChildrenClosure486);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments484['value'] = $viewHelper487->initializeArgumentsAndRender();
$arguments484['keepQuotes'] = false;
$arguments484['encoding'] = 'UTF-8';
$arguments484['doubleEncode'] = true;
$renderChildrenClosure488 = function() use ($renderingContext, $self) {
return NULL;
};
$value489 = ($arguments484['value'] !== NULL ? $arguments484['value'] : $renderChildrenClosure488());

$output400 .= !is_string($value489) && !(is_object($value489) && method_exists($value489, '__toString')) ? $value489 : htmlspecialchars($value489, ($arguments484['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments484['encoding'], $arguments484['doubleEncode']);

$output400 .= '</a>
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments490 = array();
$arguments490['action'] = 'deleteAssetCollection';
$arguments490['class'] = 'neos-inline';
$arguments490['additionalAttributes'] = NULL;
$arguments490['data'] = NULL;
$arguments490['arguments'] = array (
);
$arguments490['controller'] = NULL;
$arguments490['package'] = NULL;
$arguments490['subpackage'] = NULL;
$arguments490['object'] = NULL;
$arguments490['section'] = '';
$arguments490['format'] = '';
$arguments490['additionalParams'] = array (
);
$arguments490['absolute'] = false;
$arguments490['addQueryString'] = false;
$arguments490['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments490['fieldNamePrefix'] = NULL;
$arguments490['actionUri'] = NULL;
$arguments490['objectName'] = NULL;
$arguments490['useParentRequest'] = false;
$arguments490['enctype'] = NULL;
$arguments490['method'] = NULL;
$arguments490['name'] = NULL;
$arguments490['onreset'] = NULL;
$arguments490['onsubmit'] = NULL;
$arguments490['dir'] = NULL;
$arguments490['id'] = NULL;
$arguments490['lang'] = NULL;
$arguments490['style'] = NULL;
$arguments490['title'] = NULL;
$arguments490['accesskey'] = NULL;
$arguments490['tabindex'] = NULL;
$arguments490['onclick'] = NULL;
$renderChildrenClosure491 = function() use ($renderingContext, $self) {
$output492 = '';

$output492 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments493 = array();
$arguments493['name'] = 'assetCollection';
$arguments493['id'] = 'modal-form-object';
$arguments493['additionalAttributes'] = NULL;
$arguments493['data'] = NULL;
$arguments493['value'] = NULL;
$arguments493['property'] = NULL;
$arguments493['class'] = NULL;
$arguments493['dir'] = NULL;
$arguments493['lang'] = NULL;
$arguments493['style'] = NULL;
$arguments493['title'] = NULL;
$arguments493['accesskey'] = NULL;
$arguments493['tabindex'] = NULL;
$arguments493['onclick'] = NULL;
$renderChildrenClosure494 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper495 = $self->getViewHelper('$viewHelper495', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper495->setArguments($arguments493);
$viewHelper495->setRenderingContext($renderingContext);
$viewHelper495->setRenderChildrenClosure($renderChildrenClosure494);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output492 .= $viewHelper495->initializeArgumentsAndRender();

$output492 .= '
								<button type="submit" class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments496 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments497 = array();
$arguments497['id'] = 'media.deleteCollection';
$arguments497['source'] = 'Modules';
$arguments497['package'] = 'TYPO3.Neos';
$arguments497['value'] = NULL;
$arguments497['arguments'] = array (
);
$arguments497['quantity'] = NULL;
$arguments497['languageIdentifier'] = NULL;
$renderChildrenClosure498 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper499 = $self->getViewHelper('$viewHelper499', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper499->setArguments($arguments497);
$viewHelper499->setRenderingContext($renderingContext);
$viewHelper499->setRenderChildrenClosure($renderChildrenClosure498);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments496['value'] = $viewHelper499->initializeArgumentsAndRender();
$arguments496['keepQuotes'] = false;
$arguments496['encoding'] = 'UTF-8';
$arguments496['doubleEncode'] = true;
$renderChildrenClosure500 = function() use ($renderingContext, $self) {
return NULL;
};
$value501 = ($arguments496['value'] !== NULL ? $arguments496['value'] : $renderChildrenClosure500());

$output492 .= !is_string($value501) && !(is_object($value501) && method_exists($value501, '__toString')) ? $value501 : htmlspecialchars($value501, ($arguments496['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments496['encoding'], $arguments496['doubleEncode']);

$output492 .= '">
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments502 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments503 = array();
$arguments503['id'] = 'media.message.confirmDeleteCollection';
$arguments503['source'] = 'Modules';
$arguments503['package'] = 'TYPO3.Neos';
$arguments503['value'] = NULL;
$arguments503['arguments'] = array (
);
$arguments503['quantity'] = NULL;
$arguments503['languageIdentifier'] = NULL;
$renderChildrenClosure504 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper505 = $self->getViewHelper('$viewHelper505', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper505->setArguments($arguments503);
$viewHelper505->setRenderingContext($renderingContext);
$viewHelper505->setRenderChildrenClosure($renderChildrenClosure504);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments502['value'] = $viewHelper505->initializeArgumentsAndRender();
$arguments502['keepQuotes'] = false;
$arguments502['encoding'] = 'UTF-8';
$arguments502['doubleEncode'] = true;
$renderChildrenClosure506 = function() use ($renderingContext, $self) {
return NULL;
};
$value507 = ($arguments502['value'] !== NULL ? $arguments502['value'] : $renderChildrenClosure506());

$output492 .= !is_string($value507) && !(is_object($value507) && method_exists($value507, '__toString')) ? $value507 : htmlspecialchars($value507, ($arguments502['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments502['encoding'], $arguments502['doubleEncode']);

$output492 .= '
								</button>
							';
return $output492;
};
$viewHelper508 = $self->getViewHelper('$viewHelper508', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper508->setArguments($arguments490);
$viewHelper508->setRenderingContext($renderingContext);
$viewHelper508->setRenderChildrenClosure($renderChildrenClosure491);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output400 .= $viewHelper508->initializeArgumentsAndRender();

$output400 .= '
						</div>
					</div>
				</div>
				<div class="neos-modal-backdrop neos-in"></div>
			</div>
		';
return $output400;
};
$viewHelper509 = $self->getViewHelper('$viewHelper509', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper509->setArguments($arguments398);
$viewHelper509->setRenderingContext($renderingContext);
$viewHelper509->setRenderChildrenClosure($renderChildrenClosure399);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output305 .= $viewHelper509->initializeArgumentsAndRender();

$output305 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Security\IfAccessViewHelper
$arguments510 = array();
$arguments510['privilegeTarget'] = 'TYPO3.Media:ManageAssetCollections';
$arguments510['then'] = NULL;
$arguments510['else'] = NULL;
$arguments510['parameters'] = array (
);
$renderChildrenClosure511 = function() use ($renderingContext, $self) {
$output512 = '';

$output512 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments513 = array();
$arguments513['action'] = 'createAssetCollection';
$arguments513['id'] = 'neos-assetcollections-create-form';
$arguments513['additionalAttributes'] = NULL;
$arguments513['data'] = NULL;
$arguments513['arguments'] = array (
);
$arguments513['controller'] = NULL;
$arguments513['package'] = NULL;
$arguments513['subpackage'] = NULL;
$arguments513['object'] = NULL;
$arguments513['section'] = '';
$arguments513['format'] = '';
$arguments513['additionalParams'] = array (
);
$arguments513['absolute'] = false;
$arguments513['addQueryString'] = false;
$arguments513['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments513['fieldNamePrefix'] = NULL;
$arguments513['actionUri'] = NULL;
$arguments513['objectName'] = NULL;
$arguments513['useParentRequest'] = false;
$arguments513['enctype'] = NULL;
$arguments513['method'] = NULL;
$arguments513['name'] = NULL;
$arguments513['onreset'] = NULL;
$arguments513['onsubmit'] = NULL;
$arguments513['class'] = NULL;
$arguments513['dir'] = NULL;
$arguments513['lang'] = NULL;
$arguments513['style'] = NULL;
$arguments513['title'] = NULL;
$arguments513['accesskey'] = NULL;
$arguments513['tabindex'] = NULL;
$arguments513['onclick'] = NULL;
$renderChildrenClosure514 = function() use ($renderingContext, $self) {
$output515 = '';

$output515 .= '
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments516 = array();
$arguments516['name'] = 'title';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments517 = array();
$arguments517['id'] = 'media.newCollection.placeholder';
$arguments517['source'] = 'Modules';
$arguments517['package'] = 'TYPO3.Neos';
$arguments517['value'] = NULL;
$arguments517['arguments'] = array (
);
$arguments517['quantity'] = NULL;
$arguments517['languageIdentifier'] = NULL;
$renderChildrenClosure518 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper519 = $self->getViewHelper('$viewHelper519', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper519->setArguments($arguments517);
$viewHelper519->setRenderingContext($renderingContext);
$viewHelper519->setRenderChildrenClosure($renderChildrenClosure518);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments516['placeholder'] = $viewHelper519->initializeArgumentsAndRender();
$arguments516['additionalAttributes'] = NULL;
$arguments516['data'] = NULL;
$arguments516['required'] = false;
$arguments516['type'] = 'text';
$arguments516['value'] = NULL;
$arguments516['property'] = NULL;
$arguments516['disabled'] = NULL;
$arguments516['maxlength'] = NULL;
$arguments516['readonly'] = NULL;
$arguments516['size'] = NULL;
$arguments516['autofocus'] = NULL;
$arguments516['errorClass'] = 'f3-form-error';
$arguments516['class'] = NULL;
$arguments516['dir'] = NULL;
$arguments516['id'] = NULL;
$arguments516['lang'] = NULL;
$arguments516['style'] = NULL;
$arguments516['title'] = NULL;
$arguments516['accesskey'] = NULL;
$arguments516['tabindex'] = NULL;
$arguments516['onclick'] = NULL;
$renderChildrenClosure520 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper521 = $self->getViewHelper('$viewHelper521', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper521->setArguments($arguments516);
$viewHelper521->setRenderingContext($renderingContext);
$viewHelper521->setRenderChildrenClosure($renderChildrenClosure520);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output515 .= $viewHelper521->initializeArgumentsAndRender();

$output515 .= '<br /><br />
				<button type="submit" class="neos-button neos-button-primary">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments522 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments523 = array();
$arguments523['id'] = 'media.createCollection';
$arguments523['source'] = 'Modules';
$arguments523['package'] = 'TYPO3.Neos';
$arguments523['value'] = NULL;
$arguments523['arguments'] = array (
);
$arguments523['quantity'] = NULL;
$arguments523['languageIdentifier'] = NULL;
$renderChildrenClosure524 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper525 = $self->getViewHelper('$viewHelper525', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper525->setArguments($arguments523);
$viewHelper525->setRenderingContext($renderingContext);
$viewHelper525->setRenderChildrenClosure($renderChildrenClosure524);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments522['value'] = $viewHelper525->initializeArgumentsAndRender();
$arguments522['keepQuotes'] = false;
$arguments522['encoding'] = 'UTF-8';
$arguments522['doubleEncode'] = true;
$renderChildrenClosure526 = function() use ($renderingContext, $self) {
return NULL;
};
$value527 = ($arguments522['value'] !== NULL ? $arguments522['value'] : $renderChildrenClosure526());

$output515 .= !is_string($value527) && !(is_object($value527) && method_exists($value527, '__toString')) ? $value527 : htmlspecialchars($value527, ($arguments522['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments522['encoding'], $arguments522['doubleEncode']);

$output515 .= '</button>
			';
return $output515;
};
$viewHelper528 = $self->getViewHelper('$viewHelper528', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper528->setArguments($arguments513);
$viewHelper528->setRenderingContext($renderingContext);
$viewHelper528->setRenderChildrenClosure($renderChildrenClosure514);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output512 .= $viewHelper528->initializeArgumentsAndRender();

$output512 .= '
		';
return $output512;
};
$viewHelper529 = $self->getViewHelper('$viewHelper529', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Security\IfAccessViewHelper');
$viewHelper529->setArguments($arguments510);
$viewHelper529->setRenderingContext($renderingContext);
$viewHelper529->setRenderChildrenClosure($renderChildrenClosure511);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Security\IfAccessViewHelper

$output305 .= $viewHelper529->initializeArgumentsAndRender();

$output305 .= '
	</div>

	<div class="neos-media-aside-group">
		<h2>
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments530 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments531 = array();
$arguments531['id'] = 'media.tags';
$arguments531['source'] = 'Modules';
$arguments531['package'] = 'TYPO3.Neos';
$arguments531['value'] = NULL;
$arguments531['arguments'] = array (
);
$arguments531['quantity'] = NULL;
$arguments531['languageIdentifier'] = NULL;
$renderChildrenClosure532 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper533 = $self->getViewHelper('$viewHelper533', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper533->setArguments($arguments531);
$viewHelper533->setRenderingContext($renderingContext);
$viewHelper533->setRenderChildrenClosure($renderChildrenClosure532);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments530['value'] = $viewHelper533->initializeArgumentsAndRender();
$arguments530['keepQuotes'] = false;
$arguments530['encoding'] = 'UTF-8';
$arguments530['doubleEncode'] = true;
$renderChildrenClosure534 = function() use ($renderingContext, $self) {
return NULL;
};
$value535 = ($arguments530['value'] !== NULL ? $arguments530['value'] : $renderChildrenClosure534());

$output305 .= !is_string($value535) && !(is_object($value535) && method_exists($value535, '__toString')) ? $value535 : htmlspecialchars($value535, ($arguments530['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments530['encoding'], $arguments530['doubleEncode']);

$output305 .= '
			<span class="neos-media-aside-list-edit-toggle neos-button" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments536 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments537 = array();
$arguments537['id'] = 'media.editTags';
$arguments537['source'] = 'Modules';
$arguments537['package'] = 'TYPO3.Neos';
$arguments537['value'] = NULL;
$arguments537['arguments'] = array (
);
$arguments537['quantity'] = NULL;
$arguments537['languageIdentifier'] = NULL;
$renderChildrenClosure538 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper539 = $self->getViewHelper('$viewHelper539', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper539->setArguments($arguments537);
$viewHelper539->setRenderingContext($renderingContext);
$viewHelper539->setRenderChildrenClosure($renderChildrenClosure538);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments536['value'] = $viewHelper539->initializeArgumentsAndRender();
$arguments536['keepQuotes'] = false;
$arguments536['encoding'] = 'UTF-8';
$arguments536['doubleEncode'] = true;
$renderChildrenClosure540 = function() use ($renderingContext, $self) {
return NULL;
};
$value541 = ($arguments536['value'] !== NULL ? $arguments536['value'] : $renderChildrenClosure540());

$output305 .= !is_string($value541) && !(is_object($value541) && method_exists($value541, '__toString')) ? $value541 : htmlspecialchars($value541, ($arguments536['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments536['encoding'], $arguments536['doubleEncode']);

$output305 .= '" data-neos-toggle="tooltip"><i class="icon-pencil"></i></span>
		</h2>
		<ul class="neos-media-aside-list">
			<li class="neos-media-list-all">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments542 = array();
$arguments542['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments543 = array();
$arguments543['id'] = 'media.tags.title.all';
$arguments543['source'] = 'Modules';
$arguments543['package'] = 'TYPO3.Neos';
$arguments543['value'] = NULL;
$arguments543['arguments'] = array (
);
$arguments543['quantity'] = NULL;
$arguments543['languageIdentifier'] = NULL;
$renderChildrenClosure544 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper545 = $self->getViewHelper('$viewHelper545', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper545->setArguments($arguments543);
$viewHelper545->setRenderingContext($renderingContext);
$viewHelper545->setRenderChildrenClosure($renderChildrenClosure544);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments542['title'] = $viewHelper545->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments546 = array();
// Rendering Boolean node
$arguments546['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tagMode', $renderingContext), 1);
$arguments546['then'] = 'neos-active';
$arguments546['else'] = NULL;
$renderChildrenClosure547 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper548 = $self->getViewHelper('$viewHelper548', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper548->setArguments($arguments546);
$viewHelper548->setRenderingContext($renderingContext);
$viewHelper548->setRenderChildrenClosure($renderChildrenClosure547);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments542['class'] = $viewHelper548->initializeArgumentsAndRender();
// Rendering Array
$array549 = array();
$array549['tagMode'] = 1;
$arguments542['arguments'] = $array549;
$arguments542['additionalAttributes'] = NULL;
$arguments542['data'] = NULL;
$arguments542['controller'] = NULL;
$arguments542['package'] = NULL;
$arguments542['subpackage'] = NULL;
$arguments542['section'] = '';
$arguments542['format'] = '';
$arguments542['additionalParams'] = array (
);
$arguments542['addQueryString'] = false;
$arguments542['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments542['useParentRequest'] = false;
$arguments542['absolute'] = true;
$arguments542['dir'] = NULL;
$arguments542['id'] = NULL;
$arguments542['lang'] = NULL;
$arguments542['style'] = NULL;
$arguments542['accesskey'] = NULL;
$arguments542['tabindex'] = NULL;
$arguments542['onclick'] = NULL;
$arguments542['name'] = NULL;
$arguments542['rel'] = NULL;
$arguments542['rev'] = NULL;
$arguments542['target'] = NULL;
$renderChildrenClosure550 = function() use ($renderingContext, $self) {
$output551 = '';

$output551 .= '
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments552 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments553 = array();
$arguments553['id'] = 'media.tags.all';
$arguments553['source'] = 'Modules';
$arguments553['package'] = 'TYPO3.Neos';
$arguments553['value'] = NULL;
$arguments553['arguments'] = array (
);
$arguments553['quantity'] = NULL;
$arguments553['languageIdentifier'] = NULL;
$renderChildrenClosure554 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper555 = $self->getViewHelper('$viewHelper555', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper555->setArguments($arguments553);
$viewHelper555->setRenderingContext($renderingContext);
$viewHelper555->setRenderChildrenClosure($renderChildrenClosure554);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments552['value'] = $viewHelper555->initializeArgumentsAndRender();
$arguments552['keepQuotes'] = false;
$arguments552['encoding'] = 'UTF-8';
$arguments552['doubleEncode'] = true;
$renderChildrenClosure556 = function() use ($renderingContext, $self) {
return NULL;
};
$value557 = ($arguments552['value'] !== NULL ? $arguments552['value'] : $renderChildrenClosure556());

$output551 .= !is_string($value557) && !(is_object($value557) && method_exists($value557, '__toString')) ? $value557 : htmlspecialchars($value557, ($arguments552['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments552['encoding'], $arguments552['doubleEncode']);

$output551 .= '
					<span class="count">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments558 = array();
$arguments558['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'allCount', $renderingContext);
$arguments558['keepQuotes'] = false;
$arguments558['encoding'] = 'UTF-8';
$arguments558['doubleEncode'] = true;
$renderChildrenClosure559 = function() use ($renderingContext, $self) {
return NULL;
};
$value560 = ($arguments558['value'] !== NULL ? $arguments558['value'] : $renderChildrenClosure559());

$output551 .= !is_string($value560) && !(is_object($value560) && method_exists($value560, '__toString')) ? $value560 : htmlspecialchars($value560, ($arguments558['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments558['encoding'], $arguments558['doubleEncode']);

$output551 .= '</span>
				';
return $output551;
};
$viewHelper561 = $self->getViewHelper('$viewHelper561', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper561->setArguments($arguments542);
$viewHelper561->setRenderingContext($renderingContext);
$viewHelper561->setRenderChildrenClosure($renderChildrenClosure550);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output305 .= $viewHelper561->initializeArgumentsAndRender();

$output305 .= '
			</li>
			<li class="neos-media-list-untagged">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments562 = array();
$arguments562['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments563 = array();
$arguments563['id'] = 'media.untaggedAssets';
$arguments563['source'] = 'Modules';
$arguments563['package'] = 'TYPO3.Neos';
$arguments563['value'] = NULL;
$arguments563['arguments'] = array (
);
$arguments563['quantity'] = NULL;
$arguments563['languageIdentifier'] = NULL;
$renderChildrenClosure564 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper565 = $self->getViewHelper('$viewHelper565', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper565->setArguments($arguments563);
$viewHelper565->setRenderingContext($renderingContext);
$viewHelper565->setRenderChildrenClosure($renderChildrenClosure564);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments562['title'] = $viewHelper565->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments566 = array();
// Rendering Boolean node
$arguments566['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tagMode', $renderingContext), 2);
$arguments566['then'] = 'neos-active';
$arguments566['else'] = NULL;
$renderChildrenClosure567 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper568 = $self->getViewHelper('$viewHelper568', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper568->setArguments($arguments566);
$viewHelper568->setRenderingContext($renderingContext);
$viewHelper568->setRenderChildrenClosure($renderChildrenClosure567);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments562['class'] = $viewHelper568->initializeArgumentsAndRender();
// Rendering Array
$array569 = array();
$array569['tagMode'] = 2;
$arguments562['arguments'] = $array569;
$arguments562['additionalAttributes'] = NULL;
$arguments562['data'] = NULL;
$arguments562['controller'] = NULL;
$arguments562['package'] = NULL;
$arguments562['subpackage'] = NULL;
$arguments562['section'] = '';
$arguments562['format'] = '';
$arguments562['additionalParams'] = array (
);
$arguments562['addQueryString'] = false;
$arguments562['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments562['useParentRequest'] = false;
$arguments562['absolute'] = true;
$arguments562['dir'] = NULL;
$arguments562['id'] = NULL;
$arguments562['lang'] = NULL;
$arguments562['style'] = NULL;
$arguments562['accesskey'] = NULL;
$arguments562['tabindex'] = NULL;
$arguments562['onclick'] = NULL;
$arguments562['name'] = NULL;
$arguments562['rel'] = NULL;
$arguments562['rev'] = NULL;
$arguments562['target'] = NULL;
$renderChildrenClosure570 = function() use ($renderingContext, $self) {
$output571 = '';

$output571 .= '
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments572 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments573 = array();
$arguments573['id'] = 'media.untagged';
$arguments573['source'] = 'Modules';
$arguments573['package'] = 'TYPO3.Neos';
$arguments573['value'] = NULL;
$arguments573['arguments'] = array (
);
$arguments573['quantity'] = NULL;
$arguments573['languageIdentifier'] = NULL;
$renderChildrenClosure574 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper575 = $self->getViewHelper('$viewHelper575', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper575->setArguments($arguments573);
$viewHelper575->setRenderingContext($renderingContext);
$viewHelper575->setRenderChildrenClosure($renderChildrenClosure574);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments572['value'] = $viewHelper575->initializeArgumentsAndRender();
$arguments572['keepQuotes'] = false;
$arguments572['encoding'] = 'UTF-8';
$arguments572['doubleEncode'] = true;
$renderChildrenClosure576 = function() use ($renderingContext, $self) {
return NULL;
};
$value577 = ($arguments572['value'] !== NULL ? $arguments572['value'] : $renderChildrenClosure576());

$output571 .= !is_string($value577) && !(is_object($value577) && method_exists($value577, '__toString')) ? $value577 : htmlspecialchars($value577, ($arguments572['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments572['encoding'], $arguments572['doubleEncode']);

$output571 .= '
					<span class="count">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments578 = array();
$arguments578['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'untaggedCount', $renderingContext);
$arguments578['keepQuotes'] = false;
$arguments578['encoding'] = 'UTF-8';
$arguments578['doubleEncode'] = true;
$renderChildrenClosure579 = function() use ($renderingContext, $self) {
return NULL;
};
$value580 = ($arguments578['value'] !== NULL ? $arguments578['value'] : $renderChildrenClosure579());

$output571 .= !is_string($value580) && !(is_object($value580) && method_exists($value580, '__toString')) ? $value580 : htmlspecialchars($value580, ($arguments578['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments578['encoding'], $arguments578['doubleEncode']);

$output571 .= '</span>
				';
return $output571;
};
$viewHelper581 = $self->getViewHelper('$viewHelper581', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper581->setArguments($arguments562);
$viewHelper581->setRenderingContext($renderingContext);
$viewHelper581->setRenderChildrenClosure($renderChildrenClosure570);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output305 .= $viewHelper581->initializeArgumentsAndRender();

$output305 .= '
			</li>
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments582 = array();
$arguments582['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tags', $renderingContext);
$arguments582['as'] = 'tag';
$arguments582['key'] = '';
$arguments582['reverse'] = false;
$arguments582['iteration'] = NULL;
$renderChildrenClosure583 = function() use ($renderingContext, $self) {
$output584 = '';

$output584 .= '
				<li>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments585 = array();
$arguments585['action'] = 'index';
$arguments585['title'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.object.label', $renderingContext);
$output586 = '';

$output586 .= 'droppable-tag';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments587 = array();
// Rendering Boolean node
$arguments587['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.object', $renderingContext), \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'activeTag', $renderingContext));
$arguments587['then'] = ' neos-active';
$arguments587['else'] = NULL;
$renderChildrenClosure588 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper589 = $self->getViewHelper('$viewHelper589', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper589->setArguments($arguments587);
$viewHelper589->setRenderingContext($renderingContext);
$viewHelper589->setRenderChildrenClosure($renderChildrenClosure588);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output586 .= $viewHelper589->initializeArgumentsAndRender();
$arguments585['class'] = $output586;
// Rendering Array
$array590 = array();
$array590['tag'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.object', $renderingContext);
$arguments585['arguments'] = $array590;
// Rendering Array
$array591 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper
$arguments592 = array();
$arguments592['value'] = NULL;
$renderChildrenClosure593 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.object', $renderingContext);
};
$viewHelper594 = $self->getViewHelper('$viewHelper594', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper');
$viewHelper594->setArguments($arguments592);
$viewHelper594->setRenderingContext($renderingContext);
$viewHelper594->setRenderChildrenClosure($renderChildrenClosure593);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper
$array591['tag-identifier'] = $viewHelper594->initializeArgumentsAndRender();
$arguments585['data'] = $array591;
$arguments585['additionalAttributes'] = NULL;
$arguments585['controller'] = NULL;
$arguments585['package'] = NULL;
$arguments585['subpackage'] = NULL;
$arguments585['section'] = '';
$arguments585['format'] = '';
$arguments585['additionalParams'] = array (
);
$arguments585['addQueryString'] = false;
$arguments585['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments585['useParentRequest'] = false;
$arguments585['absolute'] = true;
$arguments585['dir'] = NULL;
$arguments585['id'] = NULL;
$arguments585['lang'] = NULL;
$arguments585['style'] = NULL;
$arguments585['accesskey'] = NULL;
$arguments585['tabindex'] = NULL;
$arguments585['onclick'] = NULL;
$arguments585['name'] = NULL;
$arguments585['rel'] = NULL;
$arguments585['rev'] = NULL;
$arguments585['target'] = NULL;
$renderChildrenClosure595 = function() use ($renderingContext, $self) {
$output596 = '';

$output596 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments597 = array();
$arguments597['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.object.label', $renderingContext);
$arguments597['keepQuotes'] = false;
$arguments597['encoding'] = 'UTF-8';
$arguments597['doubleEncode'] = true;
$renderChildrenClosure598 = function() use ($renderingContext, $self) {
return NULL;
};
$value599 = ($arguments597['value'] !== NULL ? $arguments597['value'] : $renderChildrenClosure598());

$output596 .= !is_string($value599) && !(is_object($value599) && method_exists($value599, '__toString')) ? $value599 : htmlspecialchars($value599, ($arguments597['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments597['encoding'], $arguments597['doubleEncode']);

$output596 .= '
						<span class="count">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments600 = array();
$arguments600['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.count', $renderingContext);
$arguments600['keepQuotes'] = false;
$arguments600['encoding'] = 'UTF-8';
$arguments600['doubleEncode'] = true;
$renderChildrenClosure601 = function() use ($renderingContext, $self) {
return NULL;
};
$value602 = ($arguments600['value'] !== NULL ? $arguments600['value'] : $renderChildrenClosure601());

$output596 .= !is_string($value602) && !(is_object($value602) && method_exists($value602, '__toString')) ? $value602 : htmlspecialchars($value602, ($arguments600['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments600['encoding'], $arguments600['doubleEncode']);

$output596 .= '</span>
					';
return $output596;
};
$viewHelper603 = $self->getViewHelper('$viewHelper603', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper603->setArguments($arguments585);
$viewHelper603->setRenderingContext($renderingContext);
$viewHelper603->setRenderChildrenClosure($renderChildrenClosure595);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output584 .= $viewHelper603->initializeArgumentsAndRender();

$output584 .= '
					<div class="neos-sidelist-edit-actions">
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments604 = array();
$arguments604['class'] = 'neos-button';
$arguments604['action'] = 'editTag';
// Rendering Array
$array605 = array();
$array605['tag'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.object', $renderingContext);
$arguments604['arguments'] = $array605;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments606 = array();
$arguments606['id'] = 'media.editTag';
$arguments606['source'] = 'Modules';
$arguments606['package'] = 'TYPO3.Neos';
$arguments606['value'] = NULL;
$arguments606['arguments'] = array (
);
$arguments606['quantity'] = NULL;
$arguments606['languageIdentifier'] = NULL;
$renderChildrenClosure607 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper608 = $self->getViewHelper('$viewHelper608', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper608->setArguments($arguments606);
$viewHelper608->setRenderingContext($renderingContext);
$viewHelper608->setRenderChildrenClosure($renderChildrenClosure607);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments604['title'] = $viewHelper608->initializeArgumentsAndRender();
// Rendering Array
$array609 = array();
$array609['neos-toggle'] = 'tooltip';
$arguments604['data'] = $array609;
$arguments604['additionalAttributes'] = NULL;
$arguments604['controller'] = NULL;
$arguments604['package'] = NULL;
$arguments604['subpackage'] = NULL;
$arguments604['section'] = '';
$arguments604['format'] = '';
$arguments604['additionalParams'] = array (
);
$arguments604['addQueryString'] = false;
$arguments604['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments604['useParentRequest'] = false;
$arguments604['absolute'] = true;
$arguments604['dir'] = NULL;
$arguments604['id'] = NULL;
$arguments604['lang'] = NULL;
$arguments604['style'] = NULL;
$arguments604['accesskey'] = NULL;
$arguments604['tabindex'] = NULL;
$arguments604['onclick'] = NULL;
$arguments604['name'] = NULL;
$arguments604['rel'] = NULL;
$arguments604['rev'] = NULL;
$arguments604['target'] = NULL;
$renderChildrenClosure610 = function() use ($renderingContext, $self) {
return '<i class="icon-pencil"></i>';
};
$viewHelper611 = $self->getViewHelper('$viewHelper611', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper611->setArguments($arguments604);
$viewHelper611->setRenderingContext($renderingContext);
$viewHelper611->setRenderChildrenClosure($renderChildrenClosure610);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output584 .= $viewHelper611->initializeArgumentsAndRender();

$output584 .= '
						<button class="neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments612 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments613 = array();
$arguments613['id'] = 'media.deleteTag';
$arguments613['source'] = 'Modules';
$arguments613['package'] = 'TYPO3.Neos';
$arguments613['value'] = NULL;
$arguments613['arguments'] = array (
);
$arguments613['quantity'] = NULL;
$arguments613['languageIdentifier'] = NULL;
$renderChildrenClosure614 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper615 = $self->getViewHelper('$viewHelper615', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper615->setArguments($arguments613);
$viewHelper615->setRenderingContext($renderingContext);
$viewHelper615->setRenderChildrenClosure($renderChildrenClosure614);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments612['value'] = $viewHelper615->initializeArgumentsAndRender();
$arguments612['keepQuotes'] = false;
$arguments612['encoding'] = 'UTF-8';
$arguments612['doubleEncode'] = true;
$renderChildrenClosure616 = function() use ($renderingContext, $self) {
return NULL;
};
$value617 = ($arguments612['value'] !== NULL ? $arguments612['value'] : $renderChildrenClosure616());

$output584 .= !is_string($value617) && !(is_object($value617) && method_exists($value617, '__toString')) ? $value617 : htmlspecialchars($value617, ($arguments612['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments612['encoding'], $arguments612['doubleEncode']);

$output584 .= '" data-neos-toggle="tooltip" data-modal="delete-tag-modal" data-object-identifier="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments618 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper
$arguments619 = array();
$arguments619['value'] = NULL;
$renderChildrenClosure620 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.object', $renderingContext);
};
$viewHelper621 = $self->getViewHelper('$viewHelper621', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper');
$viewHelper621->setArguments($arguments619);
$viewHelper621->setRenderingContext($renderingContext);
$viewHelper621->setRenderChildrenClosure($renderChildrenClosure620);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper
$arguments618['value'] = $viewHelper621->initializeArgumentsAndRender();
$arguments618['keepQuotes'] = false;
$arguments618['encoding'] = 'UTF-8';
$arguments618['doubleEncode'] = true;
$renderChildrenClosure622 = function() use ($renderingContext, $self) {
return NULL;
};
$value623 = ($arguments618['value'] !== NULL ? $arguments618['value'] : $renderChildrenClosure622());

$output584 .= !is_string($value623) && !(is_object($value623) && method_exists($value623, '__toString')) ? $value623 : htmlspecialchars($value623, ($arguments618['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments618['encoding'], $arguments618['doubleEncode']);

$output584 .= '" data-label="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments624 = array();
$arguments624['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.object.label', $renderingContext);
$arguments624['keepQuotes'] = false;
$arguments624['encoding'] = 'UTF-8';
$arguments624['doubleEncode'] = true;
$renderChildrenClosure625 = function() use ($renderingContext, $self) {
return NULL;
};
$value626 = ($arguments624['value'] !== NULL ? $arguments624['value'] : $renderChildrenClosure625());

$output584 .= !is_string($value626) && !(is_object($value626) && method_exists($value626, '__toString')) ? $value626 : htmlspecialchars($value626, ($arguments624['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments624['encoding'], $arguments624['doubleEncode']);

$output584 .= '"><i class="icon-trash"></i></button>
					</div>
				</li>
			';
return $output584;
};

$output305 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments582, $renderChildrenClosure583, $renderingContext);

$output305 .= '
			<div class="neos-hide" id="delete-tag-modal">
				<div class="neos-modal-centered">
					<div class="neos-modal-content">
						<div class="neos-modal-header">
							<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
							<div class="neos-header">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments627 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments628 = array();
$arguments628['id'] = 'media.message.reallyDeleteTag';
$arguments628['source'] = 'Modules';
$arguments628['package'] = 'TYPO3.Neos';
$arguments628['value'] = NULL;
$arguments628['arguments'] = array (
);
$arguments628['quantity'] = NULL;
$arguments628['languageIdentifier'] = NULL;
$renderChildrenClosure629 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper630 = $self->getViewHelper('$viewHelper630', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper630->setArguments($arguments628);
$viewHelper630->setRenderingContext($renderingContext);
$viewHelper630->setRenderChildrenClosure($renderChildrenClosure629);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments627['value'] = $viewHelper630->initializeArgumentsAndRender();
$arguments627['keepQuotes'] = false;
$arguments627['encoding'] = 'UTF-8';
$arguments627['doubleEncode'] = true;
$renderChildrenClosure631 = function() use ($renderingContext, $self) {
return NULL;
};
$value632 = ($arguments627['value'] !== NULL ? $arguments627['value'] : $renderChildrenClosure631());

$output305 .= !is_string($value632) && !(is_object($value632) && method_exists($value632, '__toString')) ? $value632 : htmlspecialchars($value632, ($arguments627['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments627['encoding'], $arguments627['doubleEncode']);

$output305 .= '</div>
							<div>
								<div class="neos-subheader">
									<p>
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments633 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments634 = array();
$arguments634['id'] = 'media.message.willDeleteTag';
$arguments634['source'] = 'Modules';
$arguments634['package'] = 'TYPO3.Neos';
$arguments634['value'] = NULL;
$arguments634['arguments'] = array (
);
$arguments634['quantity'] = NULL;
$arguments634['languageIdentifier'] = NULL;
$renderChildrenClosure635 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper636 = $self->getViewHelper('$viewHelper636', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper636->setArguments($arguments634);
$viewHelper636->setRenderingContext($renderingContext);
$viewHelper636->setRenderChildrenClosure($renderChildrenClosure635);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments633['value'] = $viewHelper636->initializeArgumentsAndRender();
$arguments633['keepQuotes'] = false;
$arguments633['encoding'] = 'UTF-8';
$arguments633['doubleEncode'] = true;
$renderChildrenClosure637 = function() use ($renderingContext, $self) {
return NULL;
};
$value638 = ($arguments633['value'] !== NULL ? $arguments633['value'] : $renderChildrenClosure637());

$output305 .= !is_string($value638) && !(is_object($value638) && method_exists($value638, '__toString')) ? $value638 : htmlspecialchars($value638, ($arguments633['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments633['encoding'], $arguments633['doubleEncode']);

$output305 .= '<br />
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments639 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments640 = array();
$arguments640['id'] = 'media.message.operationCannotBeUndone';
$arguments640['source'] = 'Modules';
$arguments640['package'] = 'TYPO3.Neos';
$arguments640['value'] = NULL;
$arguments640['arguments'] = array (
);
$arguments640['quantity'] = NULL;
$arguments640['languageIdentifier'] = NULL;
$renderChildrenClosure641 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper642 = $self->getViewHelper('$viewHelper642', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper642->setArguments($arguments640);
$viewHelper642->setRenderingContext($renderingContext);
$viewHelper642->setRenderChildrenClosure($renderChildrenClosure641);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments639['value'] = $viewHelper642->initializeArgumentsAndRender();
$arguments639['keepQuotes'] = false;
$arguments639['encoding'] = 'UTF-8';
$arguments639['doubleEncode'] = true;
$renderChildrenClosure643 = function() use ($renderingContext, $self) {
return NULL;
};
$value644 = ($arguments639['value'] !== NULL ? $arguments639['value'] : $renderChildrenClosure643());

$output305 .= !is_string($value644) && !(is_object($value644) && method_exists($value644, '__toString')) ? $value644 : htmlspecialchars($value644, ($arguments639['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments639['encoding'], $arguments639['doubleEncode']);

$output305 .= '
									</p>
								</div>
							</div>
						</div>
						<div class="neos-modal-footer">
							<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments645 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments646 = array();
$arguments646['id'] = 'media.cancel';
$arguments646['source'] = 'Modules';
$arguments646['package'] = 'TYPO3.Neos';
$arguments646['value'] = NULL;
$arguments646['arguments'] = array (
);
$arguments646['quantity'] = NULL;
$arguments646['languageIdentifier'] = NULL;
$renderChildrenClosure647 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper648 = $self->getViewHelper('$viewHelper648', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper648->setArguments($arguments646);
$viewHelper648->setRenderingContext($renderingContext);
$viewHelper648->setRenderChildrenClosure($renderChildrenClosure647);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments645['value'] = $viewHelper648->initializeArgumentsAndRender();
$arguments645['keepQuotes'] = false;
$arguments645['encoding'] = 'UTF-8';
$arguments645['doubleEncode'] = true;
$renderChildrenClosure649 = function() use ($renderingContext, $self) {
return NULL;
};
$value650 = ($arguments645['value'] !== NULL ? $arguments645['value'] : $renderChildrenClosure649());

$output305 .= !is_string($value650) && !(is_object($value650) && method_exists($value650, '__toString')) ? $value650 : htmlspecialchars($value650, ($arguments645['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments645['encoding'], $arguments645['doubleEncode']);

$output305 .= '</a>
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments651 = array();
$arguments651['action'] = 'deleteTag';
$arguments651['class'] = 'neos-inline';
$arguments651['additionalAttributes'] = NULL;
$arguments651['data'] = NULL;
$arguments651['arguments'] = array (
);
$arguments651['controller'] = NULL;
$arguments651['package'] = NULL;
$arguments651['subpackage'] = NULL;
$arguments651['object'] = NULL;
$arguments651['section'] = '';
$arguments651['format'] = '';
$arguments651['additionalParams'] = array (
);
$arguments651['absolute'] = false;
$arguments651['addQueryString'] = false;
$arguments651['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments651['fieldNamePrefix'] = NULL;
$arguments651['actionUri'] = NULL;
$arguments651['objectName'] = NULL;
$arguments651['useParentRequest'] = false;
$arguments651['enctype'] = NULL;
$arguments651['method'] = NULL;
$arguments651['name'] = NULL;
$arguments651['onreset'] = NULL;
$arguments651['onsubmit'] = NULL;
$arguments651['dir'] = NULL;
$arguments651['id'] = NULL;
$arguments651['lang'] = NULL;
$arguments651['style'] = NULL;
$arguments651['title'] = NULL;
$arguments651['accesskey'] = NULL;
$arguments651['tabindex'] = NULL;
$arguments651['onclick'] = NULL;
$renderChildrenClosure652 = function() use ($renderingContext, $self) {
$output653 = '';

$output653 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments654 = array();
$arguments654['name'] = 'tag';
$arguments654['id'] = 'modal-form-object';
$arguments654['additionalAttributes'] = NULL;
$arguments654['data'] = NULL;
$arguments654['value'] = NULL;
$arguments654['property'] = NULL;
$arguments654['class'] = NULL;
$arguments654['dir'] = NULL;
$arguments654['lang'] = NULL;
$arguments654['style'] = NULL;
$arguments654['title'] = NULL;
$arguments654['accesskey'] = NULL;
$arguments654['tabindex'] = NULL;
$arguments654['onclick'] = NULL;
$renderChildrenClosure655 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper656 = $self->getViewHelper('$viewHelper656', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper656->setArguments($arguments654);
$viewHelper656->setRenderingContext($renderingContext);
$viewHelper656->setRenderChildrenClosure($renderChildrenClosure655);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output653 .= $viewHelper656->initializeArgumentsAndRender();

$output653 .= '
								<button type="submit" class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments657 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments658 = array();
$arguments658['id'] = 'media.deleteTag';
$arguments658['source'] = 'Modules';
$arguments658['package'] = 'TYPO3.Neos';
$arguments658['value'] = NULL;
$arguments658['arguments'] = array (
);
$arguments658['quantity'] = NULL;
$arguments658['languageIdentifier'] = NULL;
$renderChildrenClosure659 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper660 = $self->getViewHelper('$viewHelper660', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper660->setArguments($arguments658);
$viewHelper660->setRenderingContext($renderingContext);
$viewHelper660->setRenderChildrenClosure($renderChildrenClosure659);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments657['value'] = $viewHelper660->initializeArgumentsAndRender();
$arguments657['keepQuotes'] = false;
$arguments657['encoding'] = 'UTF-8';
$arguments657['doubleEncode'] = true;
$renderChildrenClosure661 = function() use ($renderingContext, $self) {
return NULL;
};
$value662 = ($arguments657['value'] !== NULL ? $arguments657['value'] : $renderChildrenClosure661());

$output653 .= !is_string($value662) && !(is_object($value662) && method_exists($value662, '__toString')) ? $value662 : htmlspecialchars($value662, ($arguments657['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments657['encoding'], $arguments657['doubleEncode']);

$output653 .= '">
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments663 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments664 = array();
$arguments664['id'] = 'media.message.confirmDeleteTag';
$arguments664['source'] = 'Modules';
$arguments664['package'] = 'TYPO3.Neos';
$arguments664['value'] = NULL;
$arguments664['arguments'] = array (
);
$arguments664['quantity'] = NULL;
$arguments664['languageIdentifier'] = NULL;
$renderChildrenClosure665 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper666 = $self->getViewHelper('$viewHelper666', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper666->setArguments($arguments664);
$viewHelper666->setRenderingContext($renderingContext);
$viewHelper666->setRenderChildrenClosure($renderChildrenClosure665);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments663['value'] = $viewHelper666->initializeArgumentsAndRender();
$arguments663['keepQuotes'] = false;
$arguments663['encoding'] = 'UTF-8';
$arguments663['doubleEncode'] = true;
$renderChildrenClosure667 = function() use ($renderingContext, $self) {
return NULL;
};
$value668 = ($arguments663['value'] !== NULL ? $arguments663['value'] : $renderChildrenClosure667());

$output653 .= !is_string($value668) && !(is_object($value668) && method_exists($value668, '__toString')) ? $value668 : htmlspecialchars($value668, ($arguments663['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments663['encoding'], $arguments663['doubleEncode']);

$output653 .= '
								</button>
							';
return $output653;
};
$viewHelper669 = $self->getViewHelper('$viewHelper669', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper669->setArguments($arguments651);
$viewHelper669->setRenderingContext($renderingContext);
$viewHelper669->setRenderChildrenClosure($renderChildrenClosure652);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output305 .= $viewHelper669->initializeArgumentsAndRender();

$output305 .= '
						</div>
					</div>
				</div>
				<div class="neos-modal-backdrop neos-in"></div>
			</div>
		</ul>
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments670 = array();
$arguments670['action'] = 'createTag';
$arguments670['id'] = 'neos-tags-create-form';
$arguments670['additionalAttributes'] = NULL;
$arguments670['data'] = NULL;
$arguments670['arguments'] = array (
);
$arguments670['controller'] = NULL;
$arguments670['package'] = NULL;
$arguments670['subpackage'] = NULL;
$arguments670['object'] = NULL;
$arguments670['section'] = '';
$arguments670['format'] = '';
$arguments670['additionalParams'] = array (
);
$arguments670['absolute'] = false;
$arguments670['addQueryString'] = false;
$arguments670['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments670['fieldNamePrefix'] = NULL;
$arguments670['actionUri'] = NULL;
$arguments670['objectName'] = NULL;
$arguments670['useParentRequest'] = false;
$arguments670['enctype'] = NULL;
$arguments670['method'] = NULL;
$arguments670['name'] = NULL;
$arguments670['onreset'] = NULL;
$arguments670['onsubmit'] = NULL;
$arguments670['class'] = NULL;
$arguments670['dir'] = NULL;
$arguments670['lang'] = NULL;
$arguments670['style'] = NULL;
$arguments670['title'] = NULL;
$arguments670['accesskey'] = NULL;
$arguments670['tabindex'] = NULL;
$arguments670['onclick'] = NULL;
$renderChildrenClosure671 = function() use ($renderingContext, $self) {
$output672 = '';

$output672 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments673 = array();
$arguments673['name'] = 'label';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments674 = array();
$arguments674['id'] = 'media.placeholder.createTag';
$arguments674['source'] = 'Modules';
$arguments674['package'] = 'TYPO3.Neos';
$arguments674['value'] = NULL;
$arguments674['arguments'] = array (
);
$arguments674['quantity'] = NULL;
$arguments674['languageIdentifier'] = NULL;
$renderChildrenClosure675 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper676 = $self->getViewHelper('$viewHelper676', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper676->setArguments($arguments674);
$viewHelper676->setRenderingContext($renderingContext);
$viewHelper676->setRenderChildrenClosure($renderChildrenClosure675);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments673['placeholder'] = $viewHelper676->initializeArgumentsAndRender();
$arguments673['additionalAttributes'] = NULL;
$arguments673['data'] = NULL;
$arguments673['required'] = false;
$arguments673['type'] = 'text';
$arguments673['value'] = NULL;
$arguments673['property'] = NULL;
$arguments673['disabled'] = NULL;
$arguments673['maxlength'] = NULL;
$arguments673['readonly'] = NULL;
$arguments673['size'] = NULL;
$arguments673['autofocus'] = NULL;
$arguments673['errorClass'] = 'f3-form-error';
$arguments673['class'] = NULL;
$arguments673['dir'] = NULL;
$arguments673['id'] = NULL;
$arguments673['lang'] = NULL;
$arguments673['style'] = NULL;
$arguments673['title'] = NULL;
$arguments673['accesskey'] = NULL;
$arguments673['tabindex'] = NULL;
$arguments673['onclick'] = NULL;
$renderChildrenClosure677 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper678 = $self->getViewHelper('$viewHelper678', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper678->setArguments($arguments673);
$viewHelper678->setRenderingContext($renderingContext);
$viewHelper678->setRenderChildrenClosure($renderChildrenClosure677);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output672 .= $viewHelper678->initializeArgumentsAndRender();

$output672 .= '<br /><br />
			<button type="submit" class="neos-button neos-button-primary">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments679 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments680 = array();
$arguments680['id'] = 'media.createTag';
$arguments680['source'] = 'Modules';
$arguments680['package'] = 'TYPO3.Neos';
$arguments680['value'] = NULL;
$arguments680['arguments'] = array (
);
$arguments680['quantity'] = NULL;
$arguments680['languageIdentifier'] = NULL;
$renderChildrenClosure681 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper682 = $self->getViewHelper('$viewHelper682', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper682->setArguments($arguments680);
$viewHelper682->setRenderingContext($renderingContext);
$viewHelper682->setRenderChildrenClosure($renderChildrenClosure681);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments679['value'] = $viewHelper682->initializeArgumentsAndRender();
$arguments679['keepQuotes'] = false;
$arguments679['encoding'] = 'UTF-8';
$arguments679['doubleEncode'] = true;
$renderChildrenClosure683 = function() use ($renderingContext, $self) {
return NULL;
};
$value684 = ($arguments679['value'] !== NULL ? $arguments679['value'] : $renderChildrenClosure683());

$output672 .= !is_string($value684) && !(is_object($value684) && method_exists($value684, '__toString')) ? $value684 : htmlspecialchars($value684, ($arguments679['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments679['encoding'], $arguments679['doubleEncode']);

$output672 .= '</button>
		';
return $output672;
};
$viewHelper685 = $self->getViewHelper('$viewHelper685', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper685->setArguments($arguments670);
$viewHelper685->setRenderingContext($renderingContext);
$viewHelper685->setRenderChildrenClosure($renderChildrenClosure671);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output305 .= $viewHelper685->initializeArgumentsAndRender();

$output305 .= '
	</div>
';

return $output305;
}
/**
 * section Content
 */
public function section_4f9be057f0ea5d2ba72fd2c810e8d7b9aa98b469(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output686 = '';

$output686 .= '
	<div id="dropzone" class="neos-upload-area">
		<div title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments687 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments688 = array();
$arguments688['id'] = 'media.maxUploadSize';
// Rendering Array
$array689 = array();
$array689['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'humanReadableMaximumFileUploadSize', $renderingContext);
$arguments688['arguments'] = $array689;
$arguments688['source'] = 'Modules';
$arguments688['package'] = 'TYPO3.Neos';
$arguments688['value'] = NULL;
$arguments688['quantity'] = NULL;
$arguments688['languageIdentifier'] = NULL;
$renderChildrenClosure690 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper691 = $self->getViewHelper('$viewHelper691', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper691->setArguments($arguments688);
$viewHelper691->setRenderingContext($renderingContext);
$viewHelper691->setRenderChildrenClosure($renderChildrenClosure690);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments687['value'] = $viewHelper691->initializeArgumentsAndRender();
$arguments687['keepQuotes'] = false;
$arguments687['encoding'] = 'UTF-8';
$arguments687['doubleEncode'] = true;
$renderChildrenClosure692 = function() use ($renderingContext, $self) {
return NULL;
};
$value693 = ($arguments687['value'] !== NULL ? $arguments687['value'] : $renderChildrenClosure692());

$output686 .= !is_string($value693) && !(is_object($value693) && method_exists($value693, '__toString')) ? $value693 : htmlspecialchars($value693, ($arguments687['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments687['encoding'], $arguments687['doubleEncode']);

$output686 .= '" data-neos-toggle="tooltip">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments694 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments695 = array();
$arguments695['id'] = 'media.dropFiles';
$arguments695['source'] = 'Modules';
$arguments695['package'] = 'TYPO3.Neos';
$arguments695['value'] = NULL;
$arguments695['arguments'] = array (
);
$arguments695['quantity'] = NULL;
$arguments695['languageIdentifier'] = NULL;
$renderChildrenClosure696 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper697 = $self->getViewHelper('$viewHelper697', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper697->setArguments($arguments695);
$viewHelper697->setRenderingContext($renderingContext);
$viewHelper697->setRenderChildrenClosure($renderChildrenClosure696);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments694['value'] = $viewHelper697->initializeArgumentsAndRender();
$arguments694['keepQuotes'] = false;
$arguments694['encoding'] = 'UTF-8';
$arguments694['doubleEncode'] = true;
$renderChildrenClosure698 = function() use ($renderingContext, $self) {
return NULL;
};
$value699 = ($arguments694['value'] !== NULL ? $arguments694['value'] : $renderChildrenClosure698());

$output686 .= !is_string($value699) && !(is_object($value699) && method_exists($value699, '__toString')) ? $value699 : htmlspecialchars($value699, ($arguments694['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments694['encoding'], $arguments694['doubleEncode']);

$output686 .= '<i class="icon-arrow-down"></i><span> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments700 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments701 = array();
$arguments701['id'] = 'media.clickToUpload';
$arguments701['source'] = 'Modules';
$arguments701['package'] = 'TYPO3.Neos';
$arguments701['value'] = NULL;
$arguments701['arguments'] = array (
);
$arguments701['quantity'] = NULL;
$arguments701['languageIdentifier'] = NULL;
$renderChildrenClosure702 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper703 = $self->getViewHelper('$viewHelper703', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper703->setArguments($arguments701);
$viewHelper703->setRenderingContext($renderingContext);
$viewHelper703->setRenderChildrenClosure($renderChildrenClosure702);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments700['value'] = $viewHelper703->initializeArgumentsAndRender();
$arguments700['keepQuotes'] = false;
$arguments700['encoding'] = 'UTF-8';
$arguments700['doubleEncode'] = true;
$renderChildrenClosure704 = function() use ($renderingContext, $self) {
return NULL;
};
$value705 = ($arguments700['value'] !== NULL ? $arguments700['value'] : $renderChildrenClosure704());

$output686 .= !is_string($value705) && !(is_object($value705) && method_exists($value705, '__toString')) ? $value705 : htmlspecialchars($value705, ($arguments700['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments700['encoding'], $arguments700['doubleEncode']);

$output686 .= '</span></div>
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments706 = array();
$arguments706['method'] = 'post';
$arguments706['action'] = 'create';
$arguments706['object'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'asset', $renderingContext);
$arguments706['objectName'] = 'asset';
$arguments706['enctype'] = 'multipart/form-data';
$arguments706['additionalAttributes'] = NULL;
$arguments706['data'] = NULL;
$arguments706['arguments'] = array (
);
$arguments706['controller'] = NULL;
$arguments706['package'] = NULL;
$arguments706['subpackage'] = NULL;
$arguments706['section'] = '';
$arguments706['format'] = '';
$arguments706['additionalParams'] = array (
);
$arguments706['absolute'] = false;
$arguments706['addQueryString'] = false;
$arguments706['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments706['fieldNamePrefix'] = NULL;
$arguments706['actionUri'] = NULL;
$arguments706['useParentRequest'] = false;
$arguments706['name'] = NULL;
$arguments706['onreset'] = NULL;
$arguments706['onsubmit'] = NULL;
$arguments706['class'] = NULL;
$arguments706['dir'] = NULL;
$arguments706['id'] = NULL;
$arguments706['lang'] = NULL;
$arguments706['style'] = NULL;
$arguments706['title'] = NULL;
$arguments706['accesskey'] = NULL;
$arguments706['tabindex'] = NULL;
$arguments706['onclick'] = NULL;
$renderChildrenClosure707 = function() use ($renderingContext, $self) {
$output708 = '';

$output708 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\UploadViewHelper
$arguments709 = array();
$arguments709['id'] = 'resource';
$arguments709['property'] = 'resource';
// Rendering Array
$array710 = array();
$array710['required'] = 'required';
$arguments709['additionalAttributes'] = $array710;
$arguments709['data'] = NULL;
$arguments709['name'] = NULL;
$arguments709['value'] = NULL;
$arguments709['disabled'] = NULL;
$arguments709['errorClass'] = 'f3-form-error';
$arguments709['collection'] = '';
$arguments709['class'] = NULL;
$arguments709['dir'] = NULL;
$arguments709['lang'] = NULL;
$arguments709['style'] = NULL;
$arguments709['title'] = NULL;
$arguments709['accesskey'] = NULL;
$arguments709['tabindex'] = NULL;
$arguments709['onclick'] = NULL;
$renderChildrenClosure711 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper712 = $self->getViewHelper('$viewHelper712', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\UploadViewHelper');
$viewHelper712->setArguments($arguments709);
$viewHelper712->setRenderingContext($renderingContext);
$viewHelper712->setRenderChildrenClosure($renderChildrenClosure711);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\UploadViewHelper

$output708 .= $viewHelper712->initializeArgumentsAndRender();

$output708 .= '
		';
return $output708;
};
$viewHelper713 = $self->getViewHelper('$viewHelper713', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper713->setArguments($arguments706);
$viewHelper713->setRenderingContext($renderingContext);
$viewHelper713->setRenderChildrenClosure($renderChildrenClosure707);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output686 .= $viewHelper713->initializeArgumentsAndRender();

$output686 .= '
	</div>
	<div id="uploader">
		<div id="filelist"></div>
	</div>
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments714 = array();
// Rendering Boolean node
$arguments714['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assets', $renderingContext));
$arguments714['then'] = NULL;
$arguments714['else'] = NULL;
$renderChildrenClosure715 = function() use ($renderingContext, $self) {
$output716 = '';

$output716 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments717 = array();
$renderChildrenClosure718 = function() use ($renderingContext, $self) {
$output719 = '';

$output719 .= '
			<div class="neos-media-content-help">
				<i class="icon-info-circle"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments720 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments721 = array();
$arguments721['id'] = 'media.dragHelp';
$arguments721['source'] = 'Modules';
$arguments721['package'] = 'TYPO3.Neos';
$arguments721['value'] = NULL;
$arguments721['arguments'] = array (
);
$arguments721['quantity'] = NULL;
$arguments721['languageIdentifier'] = NULL;
$renderChildrenClosure722 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper723 = $self->getViewHelper('$viewHelper723', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper723->setArguments($arguments721);
$viewHelper723->setRenderingContext($renderingContext);
$viewHelper723->setRenderChildrenClosure($renderChildrenClosure722);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments720['value'] = $viewHelper723->initializeArgumentsAndRender();
$arguments720['keepQuotes'] = false;
$arguments720['encoding'] = 'UTF-8';
$arguments720['doubleEncode'] = true;
$renderChildrenClosure724 = function() use ($renderingContext, $self) {
return NULL;
};
$value725 = ($arguments720['value'] !== NULL ? $arguments720['value'] : $renderChildrenClosure724());

$output719 .= !is_string($value725) && !(is_object($value725) && method_exists($value725, '__toString')) ? $value725 : htmlspecialchars($value725, ($arguments720['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments720['encoding'], $arguments720['doubleEncode']);

$output719 .= '
			</div>
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper
$arguments726 = array();
$output727 = '';

$output727 .= \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'view', $renderingContext);

$output727 .= 'View';
$arguments726['partial'] = $output727;
// Rendering Array
$array728 = array();
$array728['assets'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assets', $renderingContext);
$array728['sortBy'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext);
$array728['sortDirection'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortDirection', $renderingContext);
$arguments726['arguments'] = $array728;
$arguments726['section'] = NULL;
$arguments726['optional'] = false;
$renderChildrenClosure729 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper730 = $self->getViewHelper('$viewHelper730', $renderingContext, 'TYPO3\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper730->setArguments($arguments726);
$viewHelper730->setRenderingContext($renderingContext);
$viewHelper730->setRenderChildrenClosure($renderChildrenClosure729);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper

$output719 .= $viewHelper730->initializeArgumentsAndRender();

$output719 .= '
			<div class="neos-hide" id="delete-asset-modal">
				<div class="neos-modal-centered">
					<div class="neos-modal-content">
						<div class="neos-modal-header">
							<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
							<div class="neos-header">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments731 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments732 = array();
$arguments732['id'] = 'media.message.reallyDeleteAsset';
$arguments732['source'] = 'Modules';
$arguments732['package'] = 'TYPO3.Neos';
$arguments732['value'] = NULL;
$arguments732['arguments'] = array (
);
$arguments732['quantity'] = NULL;
$arguments732['languageIdentifier'] = NULL;
$renderChildrenClosure733 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper734 = $self->getViewHelper('$viewHelper734', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper734->setArguments($arguments732);
$viewHelper734->setRenderingContext($renderingContext);
$viewHelper734->setRenderChildrenClosure($renderChildrenClosure733);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments731['value'] = $viewHelper734->initializeArgumentsAndRender();
$arguments731['keepQuotes'] = false;
$arguments731['encoding'] = 'UTF-8';
$arguments731['doubleEncode'] = true;
$renderChildrenClosure735 = function() use ($renderingContext, $self) {
return NULL;
};
$value736 = ($arguments731['value'] !== NULL ? $arguments731['value'] : $renderChildrenClosure735());

$output719 .= !is_string($value736) && !(is_object($value736) && method_exists($value736, '__toString')) ? $value736 : htmlspecialchars($value736, ($arguments731['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments731['encoding'], $arguments731['doubleEncode']);

$output719 .= '</div>
							<div>
								<div class="neos-subheader">
									<p>
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments737 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments738 = array();
$arguments738['id'] = 'media.message.willBeDeleted';
$arguments738['source'] = 'Modules';
$arguments738['package'] = 'TYPO3.Neos';
$arguments738['value'] = NULL;
$arguments738['arguments'] = array (
);
$arguments738['quantity'] = NULL;
$arguments738['languageIdentifier'] = NULL;
$renderChildrenClosure739 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper740 = $self->getViewHelper('$viewHelper740', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper740->setArguments($arguments738);
$viewHelper740->setRenderingContext($renderingContext);
$viewHelper740->setRenderChildrenClosure($renderChildrenClosure739);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments737['value'] = $viewHelper740->initializeArgumentsAndRender();
$arguments737['keepQuotes'] = false;
$arguments737['encoding'] = 'UTF-8';
$arguments737['doubleEncode'] = true;
$renderChildrenClosure741 = function() use ($renderingContext, $self) {
return NULL;
};
$value742 = ($arguments737['value'] !== NULL ? $arguments737['value'] : $renderChildrenClosure741());

$output719 .= !is_string($value742) && !(is_object($value742) && method_exists($value742, '__toString')) ? $value742 : htmlspecialchars($value742, ($arguments737['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments737['encoding'], $arguments737['doubleEncode']);

$output719 .= '<br />
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments743 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments744 = array();
$arguments744['id'] = 'media.message.operationCannotBeUndone';
$arguments744['source'] = 'Modules';
$arguments744['package'] = 'TYPO3.Neos';
$arguments744['value'] = NULL;
$arguments744['arguments'] = array (
);
$arguments744['quantity'] = NULL;
$arguments744['languageIdentifier'] = NULL;
$renderChildrenClosure745 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper746 = $self->getViewHelper('$viewHelper746', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper746->setArguments($arguments744);
$viewHelper746->setRenderingContext($renderingContext);
$viewHelper746->setRenderChildrenClosure($renderChildrenClosure745);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments743['value'] = $viewHelper746->initializeArgumentsAndRender();
$arguments743['keepQuotes'] = false;
$arguments743['encoding'] = 'UTF-8';
$arguments743['doubleEncode'] = true;
$renderChildrenClosure747 = function() use ($renderingContext, $self) {
return NULL;
};
$value748 = ($arguments743['value'] !== NULL ? $arguments743['value'] : $renderChildrenClosure747());

$output719 .= !is_string($value748) && !(is_object($value748) && method_exists($value748, '__toString')) ? $value748 : htmlspecialchars($value748, ($arguments743['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments743['encoding'], $arguments743['doubleEncode']);

$output719 .= '
									</p>
								</div>
							</div>
						</div>
						<div class="neos-modal-footer">
							<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments749 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments750 = array();
$arguments750['id'] = 'media.cancel';
$arguments750['source'] = 'Modules';
$arguments750['package'] = 'TYPO3.Neos';
$arguments750['value'] = NULL;
$arguments750['arguments'] = array (
);
$arguments750['quantity'] = NULL;
$arguments750['languageIdentifier'] = NULL;
$renderChildrenClosure751 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper752 = $self->getViewHelper('$viewHelper752', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper752->setArguments($arguments750);
$viewHelper752->setRenderingContext($renderingContext);
$viewHelper752->setRenderChildrenClosure($renderChildrenClosure751);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments749['value'] = $viewHelper752->initializeArgumentsAndRender();
$arguments749['keepQuotes'] = false;
$arguments749['encoding'] = 'UTF-8';
$arguments749['doubleEncode'] = true;
$renderChildrenClosure753 = function() use ($renderingContext, $self) {
return NULL;
};
$value754 = ($arguments749['value'] !== NULL ? $arguments749['value'] : $renderChildrenClosure753());

$output719 .= !is_string($value754) && !(is_object($value754) && method_exists($value754, '__toString')) ? $value754 : htmlspecialchars($value754, ($arguments749['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments749['encoding'], $arguments749['doubleEncode']);

$output719 .= '</a>
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments755 = array();
$arguments755['action'] = 'delete';
$arguments755['method'] = 'post';
$arguments755['class'] = 'neos-inline';
$arguments755['additionalAttributes'] = NULL;
$arguments755['data'] = NULL;
$arguments755['arguments'] = array (
);
$arguments755['controller'] = NULL;
$arguments755['package'] = NULL;
$arguments755['subpackage'] = NULL;
$arguments755['object'] = NULL;
$arguments755['section'] = '';
$arguments755['format'] = '';
$arguments755['additionalParams'] = array (
);
$arguments755['absolute'] = false;
$arguments755['addQueryString'] = false;
$arguments755['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments755['fieldNamePrefix'] = NULL;
$arguments755['actionUri'] = NULL;
$arguments755['objectName'] = NULL;
$arguments755['useParentRequest'] = false;
$arguments755['enctype'] = NULL;
$arguments755['name'] = NULL;
$arguments755['onreset'] = NULL;
$arguments755['onsubmit'] = NULL;
$arguments755['dir'] = NULL;
$arguments755['id'] = NULL;
$arguments755['lang'] = NULL;
$arguments755['style'] = NULL;
$arguments755['title'] = NULL;
$arguments755['accesskey'] = NULL;
$arguments755['tabindex'] = NULL;
$arguments755['onclick'] = NULL;
$renderChildrenClosure756 = function() use ($renderingContext, $self) {
$output757 = '';

$output757 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments758 = array();
$arguments758['name'] = 'asset';
$arguments758['id'] = 'modal-form-object';
$arguments758['additionalAttributes'] = NULL;
$arguments758['data'] = NULL;
$arguments758['value'] = NULL;
$arguments758['property'] = NULL;
$arguments758['class'] = NULL;
$arguments758['dir'] = NULL;
$arguments758['lang'] = NULL;
$arguments758['style'] = NULL;
$arguments758['title'] = NULL;
$arguments758['accesskey'] = NULL;
$arguments758['tabindex'] = NULL;
$arguments758['onclick'] = NULL;
$renderChildrenClosure759 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper760 = $self->getViewHelper('$viewHelper760', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper760->setArguments($arguments758);
$viewHelper760->setRenderingContext($renderingContext);
$viewHelper760->setRenderChildrenClosure($renderChildrenClosure759);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output757 .= $viewHelper760->initializeArgumentsAndRender();

$output757 .= '
								<button type="submit" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments761 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments762 = array();
$arguments762['id'] = 'media.tooltip.deleteAsset';
$arguments762['source'] = 'Modules';
$arguments762['package'] = 'TYPO3.Neos';
$arguments762['value'] = NULL;
$arguments762['arguments'] = array (
);
$arguments762['quantity'] = NULL;
$arguments762['languageIdentifier'] = NULL;
$renderChildrenClosure763 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper764 = $self->getViewHelper('$viewHelper764', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper764->setArguments($arguments762);
$viewHelper764->setRenderingContext($renderingContext);
$viewHelper764->setRenderChildrenClosure($renderChildrenClosure763);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments761['value'] = $viewHelper764->initializeArgumentsAndRender();
$arguments761['keepQuotes'] = false;
$arguments761['encoding'] = 'UTF-8';
$arguments761['doubleEncode'] = true;
$renderChildrenClosure765 = function() use ($renderingContext, $self) {
return NULL;
};
$value766 = ($arguments761['value'] !== NULL ? $arguments761['value'] : $renderChildrenClosure765());

$output757 .= !is_string($value766) && !(is_object($value766) && method_exists($value766, '__toString')) ? $value766 : htmlspecialchars($value766, ($arguments761['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments761['encoding'], $arguments761['doubleEncode']);

$output757 .= '" class="neos-button neos-button-mini neos-button-danger">
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments767 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments768 = array();
$arguments768['id'] = 'media.message.confirmDelete';
$arguments768['source'] = 'Modules';
$arguments768['package'] = 'TYPO3.Neos';
$arguments768['value'] = NULL;
$arguments768['arguments'] = array (
);
$arguments768['quantity'] = NULL;
$arguments768['languageIdentifier'] = NULL;
$renderChildrenClosure769 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper770 = $self->getViewHelper('$viewHelper770', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper770->setArguments($arguments768);
$viewHelper770->setRenderingContext($renderingContext);
$viewHelper770->setRenderChildrenClosure($renderChildrenClosure769);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments767['value'] = $viewHelper770->initializeArgumentsAndRender();
$arguments767['keepQuotes'] = false;
$arguments767['encoding'] = 'UTF-8';
$arguments767['doubleEncode'] = true;
$renderChildrenClosure771 = function() use ($renderingContext, $self) {
return NULL;
};
$value772 = ($arguments767['value'] !== NULL ? $arguments767['value'] : $renderChildrenClosure771());

$output757 .= !is_string($value772) && !(is_object($value772) && method_exists($value772, '__toString')) ? $value772 : htmlspecialchars($value772, ($arguments767['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments767['encoding'], $arguments767['doubleEncode']);

$output757 .= '
								</button>
							';
return $output757;
};
$viewHelper773 = $self->getViewHelper('$viewHelper773', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper773->setArguments($arguments755);
$viewHelper773->setRenderingContext($renderingContext);
$viewHelper773->setRenderChildrenClosure($renderChildrenClosure756);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output719 .= $viewHelper773->initializeArgumentsAndRender();

$output719 .= '
						</div>
					</div>
				</div>
				<div class="neos-modal-backdrop neos-in"></div>
			</div>
		';
return $output719;
};
$viewHelper774 = $self->getViewHelper('$viewHelper774', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper774->setArguments($arguments717);
$viewHelper774->setRenderingContext($renderingContext);
$viewHelper774->setRenderChildrenClosure($renderChildrenClosure718);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output716 .= $viewHelper774->initializeArgumentsAndRender();

$output716 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments775 = array();
$renderChildrenClosure776 = function() use ($renderingContext, $self) {
$output777 = '';

$output777 .= '
			<p>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments778 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments779 = array();
$arguments779['id'] = 'media.noAssetsFound';
$arguments779['source'] = 'Modules';
$arguments779['package'] = 'TYPO3.Neos';
$arguments779['value'] = NULL;
$arguments779['arguments'] = array (
);
$arguments779['quantity'] = NULL;
$arguments779['languageIdentifier'] = NULL;
$renderChildrenClosure780 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper781 = $self->getViewHelper('$viewHelper781', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper781->setArguments($arguments779);
$viewHelper781->setRenderingContext($renderingContext);
$viewHelper781->setRenderChildrenClosure($renderChildrenClosure780);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments778['value'] = $viewHelper781->initializeArgumentsAndRender();
$arguments778['keepQuotes'] = false;
$arguments778['encoding'] = 'UTF-8';
$arguments778['doubleEncode'] = true;
$renderChildrenClosure782 = function() use ($renderingContext, $self) {
return NULL;
};
$value783 = ($arguments778['value'] !== NULL ? $arguments778['value'] : $renderChildrenClosure782());

$output777 .= !is_string($value783) && !(is_object($value783) && method_exists($value783, '__toString')) ? $value783 : htmlspecialchars($value783, ($arguments778['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments778['encoding'], $arguments778['doubleEncode']);

$output777 .= '</p>
		';
return $output777;
};
$viewHelper784 = $self->getViewHelper('$viewHelper784', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper784->setArguments($arguments775);
$viewHelper784->setRenderingContext($renderingContext);
$viewHelper784->setRenderChildrenClosure($renderChildrenClosure776);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output716 .= $viewHelper784->initializeArgumentsAndRender();

$output716 .= '
	';
return $output716;
};
$arguments714['__thenClosure'] = function() use ($renderingContext, $self) {
$output785 = '';

$output785 .= '
			<div class="neos-media-content-help">
				<i class="icon-info-circle"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments786 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments787 = array();
$arguments787['id'] = 'media.dragHelp';
$arguments787['source'] = 'Modules';
$arguments787['package'] = 'TYPO3.Neos';
$arguments787['value'] = NULL;
$arguments787['arguments'] = array (
);
$arguments787['quantity'] = NULL;
$arguments787['languageIdentifier'] = NULL;
$renderChildrenClosure788 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper789 = $self->getViewHelper('$viewHelper789', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper789->setArguments($arguments787);
$viewHelper789->setRenderingContext($renderingContext);
$viewHelper789->setRenderChildrenClosure($renderChildrenClosure788);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments786['value'] = $viewHelper789->initializeArgumentsAndRender();
$arguments786['keepQuotes'] = false;
$arguments786['encoding'] = 'UTF-8';
$arguments786['doubleEncode'] = true;
$renderChildrenClosure790 = function() use ($renderingContext, $self) {
return NULL;
};
$value791 = ($arguments786['value'] !== NULL ? $arguments786['value'] : $renderChildrenClosure790());

$output785 .= !is_string($value791) && !(is_object($value791) && method_exists($value791, '__toString')) ? $value791 : htmlspecialchars($value791, ($arguments786['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments786['encoding'], $arguments786['doubleEncode']);

$output785 .= '
			</div>
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper
$arguments792 = array();
$output793 = '';

$output793 .= \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'view', $renderingContext);

$output793 .= 'View';
$arguments792['partial'] = $output793;
// Rendering Array
$array794 = array();
$array794['assets'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assets', $renderingContext);
$array794['sortBy'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext);
$array794['sortDirection'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortDirection', $renderingContext);
$arguments792['arguments'] = $array794;
$arguments792['section'] = NULL;
$arguments792['optional'] = false;
$renderChildrenClosure795 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper796 = $self->getViewHelper('$viewHelper796', $renderingContext, 'TYPO3\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper796->setArguments($arguments792);
$viewHelper796->setRenderingContext($renderingContext);
$viewHelper796->setRenderChildrenClosure($renderChildrenClosure795);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper

$output785 .= $viewHelper796->initializeArgumentsAndRender();

$output785 .= '
			<div class="neos-hide" id="delete-asset-modal">
				<div class="neos-modal-centered">
					<div class="neos-modal-content">
						<div class="neos-modal-header">
							<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
							<div class="neos-header">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments797 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments798 = array();
$arguments798['id'] = 'media.message.reallyDeleteAsset';
$arguments798['source'] = 'Modules';
$arguments798['package'] = 'TYPO3.Neos';
$arguments798['value'] = NULL;
$arguments798['arguments'] = array (
);
$arguments798['quantity'] = NULL;
$arguments798['languageIdentifier'] = NULL;
$renderChildrenClosure799 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper800 = $self->getViewHelper('$viewHelper800', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper800->setArguments($arguments798);
$viewHelper800->setRenderingContext($renderingContext);
$viewHelper800->setRenderChildrenClosure($renderChildrenClosure799);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments797['value'] = $viewHelper800->initializeArgumentsAndRender();
$arguments797['keepQuotes'] = false;
$arguments797['encoding'] = 'UTF-8';
$arguments797['doubleEncode'] = true;
$renderChildrenClosure801 = function() use ($renderingContext, $self) {
return NULL;
};
$value802 = ($arguments797['value'] !== NULL ? $arguments797['value'] : $renderChildrenClosure801());

$output785 .= !is_string($value802) && !(is_object($value802) && method_exists($value802, '__toString')) ? $value802 : htmlspecialchars($value802, ($arguments797['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments797['encoding'], $arguments797['doubleEncode']);

$output785 .= '</div>
							<div>
								<div class="neos-subheader">
									<p>
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments803 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments804 = array();
$arguments804['id'] = 'media.message.willBeDeleted';
$arguments804['source'] = 'Modules';
$arguments804['package'] = 'TYPO3.Neos';
$arguments804['value'] = NULL;
$arguments804['arguments'] = array (
);
$arguments804['quantity'] = NULL;
$arguments804['languageIdentifier'] = NULL;
$renderChildrenClosure805 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper806 = $self->getViewHelper('$viewHelper806', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper806->setArguments($arguments804);
$viewHelper806->setRenderingContext($renderingContext);
$viewHelper806->setRenderChildrenClosure($renderChildrenClosure805);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments803['value'] = $viewHelper806->initializeArgumentsAndRender();
$arguments803['keepQuotes'] = false;
$arguments803['encoding'] = 'UTF-8';
$arguments803['doubleEncode'] = true;
$renderChildrenClosure807 = function() use ($renderingContext, $self) {
return NULL;
};
$value808 = ($arguments803['value'] !== NULL ? $arguments803['value'] : $renderChildrenClosure807());

$output785 .= !is_string($value808) && !(is_object($value808) && method_exists($value808, '__toString')) ? $value808 : htmlspecialchars($value808, ($arguments803['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments803['encoding'], $arguments803['doubleEncode']);

$output785 .= '<br />
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments809 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments810 = array();
$arguments810['id'] = 'media.message.operationCannotBeUndone';
$arguments810['source'] = 'Modules';
$arguments810['package'] = 'TYPO3.Neos';
$arguments810['value'] = NULL;
$arguments810['arguments'] = array (
);
$arguments810['quantity'] = NULL;
$arguments810['languageIdentifier'] = NULL;
$renderChildrenClosure811 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper812 = $self->getViewHelper('$viewHelper812', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper812->setArguments($arguments810);
$viewHelper812->setRenderingContext($renderingContext);
$viewHelper812->setRenderChildrenClosure($renderChildrenClosure811);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments809['value'] = $viewHelper812->initializeArgumentsAndRender();
$arguments809['keepQuotes'] = false;
$arguments809['encoding'] = 'UTF-8';
$arguments809['doubleEncode'] = true;
$renderChildrenClosure813 = function() use ($renderingContext, $self) {
return NULL;
};
$value814 = ($arguments809['value'] !== NULL ? $arguments809['value'] : $renderChildrenClosure813());

$output785 .= !is_string($value814) && !(is_object($value814) && method_exists($value814, '__toString')) ? $value814 : htmlspecialchars($value814, ($arguments809['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments809['encoding'], $arguments809['doubleEncode']);

$output785 .= '
									</p>
								</div>
							</div>
						</div>
						<div class="neos-modal-footer">
							<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments815 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments816 = array();
$arguments816['id'] = 'media.cancel';
$arguments816['source'] = 'Modules';
$arguments816['package'] = 'TYPO3.Neos';
$arguments816['value'] = NULL;
$arguments816['arguments'] = array (
);
$arguments816['quantity'] = NULL;
$arguments816['languageIdentifier'] = NULL;
$renderChildrenClosure817 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper818 = $self->getViewHelper('$viewHelper818', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper818->setArguments($arguments816);
$viewHelper818->setRenderingContext($renderingContext);
$viewHelper818->setRenderChildrenClosure($renderChildrenClosure817);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments815['value'] = $viewHelper818->initializeArgumentsAndRender();
$arguments815['keepQuotes'] = false;
$arguments815['encoding'] = 'UTF-8';
$arguments815['doubleEncode'] = true;
$renderChildrenClosure819 = function() use ($renderingContext, $self) {
return NULL;
};
$value820 = ($arguments815['value'] !== NULL ? $arguments815['value'] : $renderChildrenClosure819());

$output785 .= !is_string($value820) && !(is_object($value820) && method_exists($value820, '__toString')) ? $value820 : htmlspecialchars($value820, ($arguments815['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments815['encoding'], $arguments815['doubleEncode']);

$output785 .= '</a>
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments821 = array();
$arguments821['action'] = 'delete';
$arguments821['method'] = 'post';
$arguments821['class'] = 'neos-inline';
$arguments821['additionalAttributes'] = NULL;
$arguments821['data'] = NULL;
$arguments821['arguments'] = array (
);
$arguments821['controller'] = NULL;
$arguments821['package'] = NULL;
$arguments821['subpackage'] = NULL;
$arguments821['object'] = NULL;
$arguments821['section'] = '';
$arguments821['format'] = '';
$arguments821['additionalParams'] = array (
);
$arguments821['absolute'] = false;
$arguments821['addQueryString'] = false;
$arguments821['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments821['fieldNamePrefix'] = NULL;
$arguments821['actionUri'] = NULL;
$arguments821['objectName'] = NULL;
$arguments821['useParentRequest'] = false;
$arguments821['enctype'] = NULL;
$arguments821['name'] = NULL;
$arguments821['onreset'] = NULL;
$arguments821['onsubmit'] = NULL;
$arguments821['dir'] = NULL;
$arguments821['id'] = NULL;
$arguments821['lang'] = NULL;
$arguments821['style'] = NULL;
$arguments821['title'] = NULL;
$arguments821['accesskey'] = NULL;
$arguments821['tabindex'] = NULL;
$arguments821['onclick'] = NULL;
$renderChildrenClosure822 = function() use ($renderingContext, $self) {
$output823 = '';

$output823 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments824 = array();
$arguments824['name'] = 'asset';
$arguments824['id'] = 'modal-form-object';
$arguments824['additionalAttributes'] = NULL;
$arguments824['data'] = NULL;
$arguments824['value'] = NULL;
$arguments824['property'] = NULL;
$arguments824['class'] = NULL;
$arguments824['dir'] = NULL;
$arguments824['lang'] = NULL;
$arguments824['style'] = NULL;
$arguments824['title'] = NULL;
$arguments824['accesskey'] = NULL;
$arguments824['tabindex'] = NULL;
$arguments824['onclick'] = NULL;
$renderChildrenClosure825 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper826 = $self->getViewHelper('$viewHelper826', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper826->setArguments($arguments824);
$viewHelper826->setRenderingContext($renderingContext);
$viewHelper826->setRenderChildrenClosure($renderChildrenClosure825);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output823 .= $viewHelper826->initializeArgumentsAndRender();

$output823 .= '
								<button type="submit" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments827 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments828 = array();
$arguments828['id'] = 'media.tooltip.deleteAsset';
$arguments828['source'] = 'Modules';
$arguments828['package'] = 'TYPO3.Neos';
$arguments828['value'] = NULL;
$arguments828['arguments'] = array (
);
$arguments828['quantity'] = NULL;
$arguments828['languageIdentifier'] = NULL;
$renderChildrenClosure829 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper830 = $self->getViewHelper('$viewHelper830', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper830->setArguments($arguments828);
$viewHelper830->setRenderingContext($renderingContext);
$viewHelper830->setRenderChildrenClosure($renderChildrenClosure829);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments827['value'] = $viewHelper830->initializeArgumentsAndRender();
$arguments827['keepQuotes'] = false;
$arguments827['encoding'] = 'UTF-8';
$arguments827['doubleEncode'] = true;
$renderChildrenClosure831 = function() use ($renderingContext, $self) {
return NULL;
};
$value832 = ($arguments827['value'] !== NULL ? $arguments827['value'] : $renderChildrenClosure831());

$output823 .= !is_string($value832) && !(is_object($value832) && method_exists($value832, '__toString')) ? $value832 : htmlspecialchars($value832, ($arguments827['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments827['encoding'], $arguments827['doubleEncode']);

$output823 .= '" class="neos-button neos-button-mini neos-button-danger">
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments833 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments834 = array();
$arguments834['id'] = 'media.message.confirmDelete';
$arguments834['source'] = 'Modules';
$arguments834['package'] = 'TYPO3.Neos';
$arguments834['value'] = NULL;
$arguments834['arguments'] = array (
);
$arguments834['quantity'] = NULL;
$arguments834['languageIdentifier'] = NULL;
$renderChildrenClosure835 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper836 = $self->getViewHelper('$viewHelper836', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper836->setArguments($arguments834);
$viewHelper836->setRenderingContext($renderingContext);
$viewHelper836->setRenderChildrenClosure($renderChildrenClosure835);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments833['value'] = $viewHelper836->initializeArgumentsAndRender();
$arguments833['keepQuotes'] = false;
$arguments833['encoding'] = 'UTF-8';
$arguments833['doubleEncode'] = true;
$renderChildrenClosure837 = function() use ($renderingContext, $self) {
return NULL;
};
$value838 = ($arguments833['value'] !== NULL ? $arguments833['value'] : $renderChildrenClosure837());

$output823 .= !is_string($value838) && !(is_object($value838) && method_exists($value838, '__toString')) ? $value838 : htmlspecialchars($value838, ($arguments833['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments833['encoding'], $arguments833['doubleEncode']);

$output823 .= '
								</button>
							';
return $output823;
};
$viewHelper839 = $self->getViewHelper('$viewHelper839', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper839->setArguments($arguments821);
$viewHelper839->setRenderingContext($renderingContext);
$viewHelper839->setRenderChildrenClosure($renderChildrenClosure822);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output785 .= $viewHelper839->initializeArgumentsAndRender();

$output785 .= '
						</div>
					</div>
				</div>
				<div class="neos-modal-backdrop neos-in"></div>
			</div>
		';
return $output785;
};
$arguments714['__elseClosure'] = function() use ($renderingContext, $self) {
$output840 = '';

$output840 .= '
			<p>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments841 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments842 = array();
$arguments842['id'] = 'media.noAssetsFound';
$arguments842['source'] = 'Modules';
$arguments842['package'] = 'TYPO3.Neos';
$arguments842['value'] = NULL;
$arguments842['arguments'] = array (
);
$arguments842['quantity'] = NULL;
$arguments842['languageIdentifier'] = NULL;
$renderChildrenClosure843 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper844 = $self->getViewHelper('$viewHelper844', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper844->setArguments($arguments842);
$viewHelper844->setRenderingContext($renderingContext);
$viewHelper844->setRenderChildrenClosure($renderChildrenClosure843);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments841['value'] = $viewHelper844->initializeArgumentsAndRender();
$arguments841['keepQuotes'] = false;
$arguments841['encoding'] = 'UTF-8';
$arguments841['doubleEncode'] = true;
$renderChildrenClosure845 = function() use ($renderingContext, $self) {
return NULL;
};
$value846 = ($arguments841['value'] !== NULL ? $arguments841['value'] : $renderChildrenClosure845());

$output840 .= !is_string($value846) && !(is_object($value846) && method_exists($value846, '__toString')) ? $value846 : htmlspecialchars($value846, ($arguments841['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments841['encoding'], $arguments841['doubleEncode']);

$output840 .= '</p>
		';
return $output840;
};
$viewHelper847 = $self->getViewHelper('$viewHelper847', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper847->setArguments($arguments714);
$viewHelper847->setRenderingContext($renderingContext);
$viewHelper847->setRenderChildrenClosure($renderChildrenClosure715);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output686 .= $viewHelper847->initializeArgumentsAndRender();

$output686 .= '
';

return $output686;
}
/**
 * section Scripts
 */
public function section_381e3298b2b8f6caeb2208b57d805ada38402f0b(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output848 = '';

$output848 .= '
	<script type="text/javascript">
		var uploadUrl = \'';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments849 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments850 = array();
$arguments850['action'] = 'upload';
// Rendering Array
$array851 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Security\CsrfTokenViewHelper
$arguments852 = array();
$renderChildrenClosure853 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper854 = $self->getViewHelper('$viewHelper854', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Security\CsrfTokenViewHelper');
$viewHelper854->setArguments($arguments852);
$viewHelper854->setRenderingContext($renderingContext);
$viewHelper854->setRenderChildrenClosure($renderChildrenClosure853);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Security\CsrfTokenViewHelper
$array851['__csrfToken'] = $viewHelper854->initializeArgumentsAndRender();
$arguments850['additionalParams'] = $array851;
// Rendering Boolean node
$arguments850['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('true');
$arguments850['arguments'] = array (
);
$arguments850['controller'] = NULL;
$arguments850['package'] = NULL;
$arguments850['subpackage'] = NULL;
$arguments850['section'] = '';
$arguments850['format'] = '';
$arguments850['addQueryString'] = false;
$arguments850['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments850['useParentRequest'] = false;
$renderChildrenClosure855 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper856 = $self->getViewHelper('$viewHelper856', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper856->setArguments($arguments850);
$viewHelper856->setRenderingContext($renderingContext);
$viewHelper856->setRenderChildrenClosure($renderChildrenClosure855);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments849['value'] = $viewHelper856->initializeArgumentsAndRender();
$arguments849['keepQuotes'] = false;
$arguments849['encoding'] = 'UTF-8';
$arguments849['doubleEncode'] = true;
$renderChildrenClosure857 = function() use ($renderingContext, $self) {
return NULL;
};
$value858 = ($arguments849['value'] !== NULL ? $arguments849['value'] : $renderChildrenClosure857());

$output848 .= !is_string($value858) && !(is_object($value858) && method_exists($value858, '__toString')) ? $value858 : htmlspecialchars($value858, ($arguments849['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments849['encoding'], $arguments849['doubleEncode']);

$output848 .= '\';
		var maximumFileUploadSize = ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments859 = array();
$arguments859['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'maximumFileUploadSize', $renderingContext);
$arguments859['keepQuotes'] = false;
$arguments859['encoding'] = 'UTF-8';
$arguments859['doubleEncode'] = true;
$renderChildrenClosure860 = function() use ($renderingContext, $self) {
return NULL;
};
$value861 = ($arguments859['value'] !== NULL ? $arguments859['value'] : $renderChildrenClosure860());

$output848 .= !is_string($value861) && !(is_object($value861) && method_exists($value861, '__toString')) ? $value861 : htmlspecialchars($value861, ($arguments859['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments859['encoding'], $arguments859['doubleEncode']);

$output848 .= ';

		if (window.parent !== window && window.parent.Typo3MediaBrowserCallbacks && window.parent.Typo3MediaBrowserCallbacks.refreshThumbnail) {
			window.parent.Typo3MediaBrowserCallbacks.refreshThumbnail();
		}
	</script>
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments862 = array();
$arguments862['action'] = 'tagAsset';
$arguments862['id'] = 'tag-asset-form';
$arguments862['format'] = 'json';
$arguments862['additionalAttributes'] = NULL;
$arguments862['data'] = NULL;
$arguments862['arguments'] = array (
);
$arguments862['controller'] = NULL;
$arguments862['package'] = NULL;
$arguments862['subpackage'] = NULL;
$arguments862['object'] = NULL;
$arguments862['section'] = '';
$arguments862['additionalParams'] = array (
);
$arguments862['absolute'] = false;
$arguments862['addQueryString'] = false;
$arguments862['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments862['fieldNamePrefix'] = NULL;
$arguments862['actionUri'] = NULL;
$arguments862['objectName'] = NULL;
$arguments862['useParentRequest'] = false;
$arguments862['enctype'] = NULL;
$arguments862['method'] = NULL;
$arguments862['name'] = NULL;
$arguments862['onreset'] = NULL;
$arguments862['onsubmit'] = NULL;
$arguments862['class'] = NULL;
$arguments862['dir'] = NULL;
$arguments862['lang'] = NULL;
$arguments862['style'] = NULL;
$arguments862['title'] = NULL;
$arguments862['accesskey'] = NULL;
$arguments862['tabindex'] = NULL;
$arguments862['onclick'] = NULL;
$renderChildrenClosure863 = function() use ($renderingContext, $self) {
$output864 = '';

$output864 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments865 = array();
$arguments865['name'] = 'asset[__identity]';
$arguments865['id'] = 'tag-asset-form-asset';
$arguments865['additionalAttributes'] = NULL;
$arguments865['data'] = NULL;
$arguments865['value'] = NULL;
$arguments865['property'] = NULL;
$arguments865['class'] = NULL;
$arguments865['dir'] = NULL;
$arguments865['lang'] = NULL;
$arguments865['style'] = NULL;
$arguments865['title'] = NULL;
$arguments865['accesskey'] = NULL;
$arguments865['tabindex'] = NULL;
$arguments865['onclick'] = NULL;
$renderChildrenClosure866 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper867 = $self->getViewHelper('$viewHelper867', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper867->setArguments($arguments865);
$viewHelper867->setRenderingContext($renderingContext);
$viewHelper867->setRenderChildrenClosure($renderChildrenClosure866);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output864 .= $viewHelper867->initializeArgumentsAndRender();

$output864 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments868 = array();
$arguments868['name'] = 'tag[__identity]';
$arguments868['id'] = 'tag-asset-form-tag';
$arguments868['additionalAttributes'] = NULL;
$arguments868['data'] = NULL;
$arguments868['value'] = NULL;
$arguments868['property'] = NULL;
$arguments868['class'] = NULL;
$arguments868['dir'] = NULL;
$arguments868['lang'] = NULL;
$arguments868['style'] = NULL;
$arguments868['title'] = NULL;
$arguments868['accesskey'] = NULL;
$arguments868['tabindex'] = NULL;
$arguments868['onclick'] = NULL;
$renderChildrenClosure869 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper870 = $self->getViewHelper('$viewHelper870', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper870->setArguments($arguments868);
$viewHelper870->setRenderingContext($renderingContext);
$viewHelper870->setRenderChildrenClosure($renderChildrenClosure869);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output864 .= $viewHelper870->initializeArgumentsAndRender();

$output864 .= '
	';
return $output864;
};
$viewHelper871 = $self->getViewHelper('$viewHelper871', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper871->setArguments($arguments862);
$viewHelper871->setRenderingContext($renderingContext);
$viewHelper871->setRenderChildrenClosure($renderChildrenClosure863);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output848 .= $viewHelper871->initializeArgumentsAndRender();

$output848 .= '
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments872 = array();
$arguments872['action'] = 'addAssetToCollection';
$arguments872['id'] = 'link-asset-to-assetcollection-form';
$arguments872['format'] = 'json';
$arguments872['additionalAttributes'] = NULL;
$arguments872['data'] = NULL;
$arguments872['arguments'] = array (
);
$arguments872['controller'] = NULL;
$arguments872['package'] = NULL;
$arguments872['subpackage'] = NULL;
$arguments872['object'] = NULL;
$arguments872['section'] = '';
$arguments872['additionalParams'] = array (
);
$arguments872['absolute'] = false;
$arguments872['addQueryString'] = false;
$arguments872['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments872['fieldNamePrefix'] = NULL;
$arguments872['actionUri'] = NULL;
$arguments872['objectName'] = NULL;
$arguments872['useParentRequest'] = false;
$arguments872['enctype'] = NULL;
$arguments872['method'] = NULL;
$arguments872['name'] = NULL;
$arguments872['onreset'] = NULL;
$arguments872['onsubmit'] = NULL;
$arguments872['class'] = NULL;
$arguments872['dir'] = NULL;
$arguments872['lang'] = NULL;
$arguments872['style'] = NULL;
$arguments872['title'] = NULL;
$arguments872['accesskey'] = NULL;
$arguments872['tabindex'] = NULL;
$arguments872['onclick'] = NULL;
$renderChildrenClosure873 = function() use ($renderingContext, $self) {
$output874 = '';

$output874 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments875 = array();
$arguments875['name'] = 'asset[__identity]';
$arguments875['id'] = 'link-asset-to-assetcollection-form-asset';
$arguments875['additionalAttributes'] = NULL;
$arguments875['data'] = NULL;
$arguments875['value'] = NULL;
$arguments875['property'] = NULL;
$arguments875['class'] = NULL;
$arguments875['dir'] = NULL;
$arguments875['lang'] = NULL;
$arguments875['style'] = NULL;
$arguments875['title'] = NULL;
$arguments875['accesskey'] = NULL;
$arguments875['tabindex'] = NULL;
$arguments875['onclick'] = NULL;
$renderChildrenClosure876 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper877 = $self->getViewHelper('$viewHelper877', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper877->setArguments($arguments875);
$viewHelper877->setRenderingContext($renderingContext);
$viewHelper877->setRenderChildrenClosure($renderChildrenClosure876);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output874 .= $viewHelper877->initializeArgumentsAndRender();

$output874 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments878 = array();
$arguments878['name'] = 'assetCollection[__identity]';
$arguments878['id'] = 'link-asset-to-assetcollection-form-assetcollection';
$arguments878['additionalAttributes'] = NULL;
$arguments878['data'] = NULL;
$arguments878['value'] = NULL;
$arguments878['property'] = NULL;
$arguments878['class'] = NULL;
$arguments878['dir'] = NULL;
$arguments878['lang'] = NULL;
$arguments878['style'] = NULL;
$arguments878['title'] = NULL;
$arguments878['accesskey'] = NULL;
$arguments878['tabindex'] = NULL;
$arguments878['onclick'] = NULL;
$renderChildrenClosure879 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper880 = $self->getViewHelper('$viewHelper880', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper880->setArguments($arguments878);
$viewHelper880->setRenderingContext($renderingContext);
$viewHelper880->setRenderChildrenClosure($renderChildrenClosure879);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output874 .= $viewHelper880->initializeArgumentsAndRender();

$output874 .= '
	';
return $output874;
};
$viewHelper881 = $self->getViewHelper('$viewHelper881', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper881->setArguments($arguments872);
$viewHelper881->setRenderingContext($renderingContext);
$viewHelper881->setRenderChildrenClosure($renderChildrenClosure873);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output848 .= $viewHelper881->initializeArgumentsAndRender();

$output848 .= '
	<script type="text/javascript" src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments882 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments883 = array();
$arguments883['package'] = 'TYPO3.Media';
$arguments883['path'] = 'Libraries/plupload/plupload.full.js';
$arguments883['resource'] = NULL;
$arguments883['localize'] = true;
$renderChildrenClosure884 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper885 = $self->getViewHelper('$viewHelper885', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper885->setArguments($arguments883);
$viewHelper885->setRenderingContext($renderingContext);
$viewHelper885->setRenderChildrenClosure($renderChildrenClosure884);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments882['value'] = $viewHelper885->initializeArgumentsAndRender();
$arguments882['keepQuotes'] = false;
$arguments882['encoding'] = 'UTF-8';
$arguments882['doubleEncode'] = true;
$renderChildrenClosure886 = function() use ($renderingContext, $self) {
return NULL;
};
$value887 = ($arguments882['value'] !== NULL ? $arguments882['value'] : $renderChildrenClosure886());

$output848 .= !is_string($value887) && !(is_object($value887) && method_exists($value887, '__toString')) ? $value887 : htmlspecialchars($value887, ($arguments882['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments882['encoding'], $arguments882['doubleEncode']);

$output848 .= '"></script>
	<script type="text/javascript" src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments888 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments889 = array();
$arguments889['package'] = 'TYPO3.Media';
$arguments889['path'] = 'Libraries/jquery-ui/js/jquery-ui-1.10.3.custom.js';
$arguments889['resource'] = NULL;
$arguments889['localize'] = true;
$renderChildrenClosure890 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper891 = $self->getViewHelper('$viewHelper891', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper891->setArguments($arguments889);
$viewHelper891->setRenderingContext($renderingContext);
$viewHelper891->setRenderChildrenClosure($renderChildrenClosure890);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments888['value'] = $viewHelper891->initializeArgumentsAndRender();
$arguments888['keepQuotes'] = false;
$arguments888['encoding'] = 'UTF-8';
$arguments888['doubleEncode'] = true;
$renderChildrenClosure892 = function() use ($renderingContext, $self) {
return NULL;
};
$value893 = ($arguments888['value'] !== NULL ? $arguments888['value'] : $renderChildrenClosure892());

$output848 .= !is_string($value893) && !(is_object($value893) && method_exists($value893, '__toString')) ? $value893 : htmlspecialchars($value893, ($arguments888['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments888['encoding'], $arguments888['doubleEncode']);

$output848 .= '"></script>
	<script type="text/javascript" src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments894 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments895 = array();
$arguments895['package'] = 'TYPO3.Media';
$arguments895['path'] = 'Scripts/upload.js';
$arguments895['resource'] = NULL;
$arguments895['localize'] = true;
$renderChildrenClosure896 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper897 = $self->getViewHelper('$viewHelper897', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper897->setArguments($arguments895);
$viewHelper897->setRenderingContext($renderingContext);
$viewHelper897->setRenderChildrenClosure($renderChildrenClosure896);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments894['value'] = $viewHelper897->initializeArgumentsAndRender();
$arguments894['keepQuotes'] = false;
$arguments894['encoding'] = 'UTF-8';
$arguments894['doubleEncode'] = true;
$renderChildrenClosure898 = function() use ($renderingContext, $self) {
return NULL;
};
$value899 = ($arguments894['value'] !== NULL ? $arguments894['value'] : $renderChildrenClosure898());

$output848 .= !is_string($value899) && !(is_object($value899) && method_exists($value899, '__toString')) ? $value899 : htmlspecialchars($value899, ($arguments894['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments894['encoding'], $arguments894['doubleEncode']);

$output848 .= '"></script>
	<script type="text/javascript" src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments900 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments901 = array();
$arguments901['package'] = 'TYPO3.Media';
$arguments901['path'] = 'Scripts/collections-and-tagging.js';
$arguments901['resource'] = NULL;
$arguments901['localize'] = true;
$renderChildrenClosure902 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper903 = $self->getViewHelper('$viewHelper903', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper903->setArguments($arguments901);
$viewHelper903->setRenderingContext($renderingContext);
$viewHelper903->setRenderChildrenClosure($renderChildrenClosure902);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments900['value'] = $viewHelper903->initializeArgumentsAndRender();
$arguments900['keepQuotes'] = false;
$arguments900['encoding'] = 'UTF-8';
$arguments900['doubleEncode'] = true;
$renderChildrenClosure904 = function() use ($renderingContext, $self) {
return NULL;
};
$value905 = ($arguments900['value'] !== NULL ? $arguments900['value'] : $renderChildrenClosure904());

$output848 .= !is_string($value905) && !(is_object($value905) && method_exists($value905, '__toString')) ? $value905 : htmlspecialchars($value905, ($arguments900['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments900['encoding'], $arguments900['doubleEncode']);

$output848 .= '"></script>
';

return $output848;
}
/**
 * Main Render function
 */
public function render(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output906 = '';

$output906 .= '
';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments907 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\LayoutViewHelper
$arguments908 = array();
$arguments908['name'] = 'Default';
$renderChildrenClosure909 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper910 = $self->getViewHelper('$viewHelper910', $renderingContext, 'TYPO3\Fluid\ViewHelpers\LayoutViewHelper');
$viewHelper910->setArguments($arguments908);
$viewHelper910->setRenderingContext($renderingContext);
$viewHelper910->setRenderChildrenClosure($renderChildrenClosure909);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\LayoutViewHelper
$arguments907['value'] = $viewHelper910->initializeArgumentsAndRender();
$arguments907['keepQuotes'] = false;
$arguments907['encoding'] = 'UTF-8';
$arguments907['doubleEncode'] = true;
$renderChildrenClosure911 = function() use ($renderingContext, $self) {
return NULL;
};
$value912 = ($arguments907['value'] !== NULL ? $arguments907['value'] : $renderChildrenClosure911());

$output906 .= !is_string($value912) && !(is_object($value912) && method_exists($value912, '__toString')) ? $value912 : htmlspecialchars($value912, ($arguments907['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments907['encoding'], $arguments907['doubleEncode']);

$output906 .= '

';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\SectionViewHelper
$arguments913 = array();
$arguments913['name'] = 'Title';
$renderChildrenClosure914 = function() use ($renderingContext, $self) {
return 'Index view';
};

$output906 .= '';

$output906 .= '

';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\SectionViewHelper
$arguments915 = array();
$arguments915['name'] = 'Options';
$renderChildrenClosure916 = function() use ($renderingContext, $self) {
$output917 = '';

$output917 .= '
	<div class="neos-file-options">
		<span class="count">
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments918 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments919 = array();
$arguments919['id'] = 'media.search.itemCount';
// Rendering Array
$array920 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments921 = array();
$arguments921['subject'] = NULL;
$renderChildrenClosure922 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assets', $renderingContext);
};
$viewHelper923 = $self->getViewHelper('$viewHelper923', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper923->setArguments($arguments921);
$viewHelper923->setRenderingContext($renderingContext);
$viewHelper923->setRenderChildrenClosure($renderChildrenClosure922);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$array920['0'] = $viewHelper923->initializeArgumentsAndRender();
$arguments919['arguments'] = $array920;
$arguments919['source'] = 'Modules';
$arguments919['package'] = 'TYPO3.Neos';
$arguments919['value'] = NULL;
$arguments919['quantity'] = NULL;
$arguments919['languageIdentifier'] = NULL;
$renderChildrenClosure924 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper925 = $self->getViewHelper('$viewHelper925', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper925->setArguments($arguments919);
$viewHelper925->setRenderingContext($renderingContext);
$viewHelper925->setRenderChildrenClosure($renderChildrenClosure924);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments918['value'] = $viewHelper925->initializeArgumentsAndRender();
$arguments918['keepQuotes'] = false;
$arguments918['encoding'] = 'UTF-8';
$arguments918['doubleEncode'] = true;
$renderChildrenClosure926 = function() use ($renderingContext, $self) {
return NULL;
};
$value927 = ($arguments918['value'] !== NULL ? $arguments918['value'] : $renderChildrenClosure926());

$output917 .= !is_string($value927) && !(is_object($value927) && method_exists($value927, '__toString')) ? $value927 : htmlspecialchars($value927, ($arguments918['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments918['encoding'], $arguments918['doubleEncode']);

$output917 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments928 = array();
// Rendering Boolean node
$arguments928['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'searchTerm', $renderingContext));
$arguments928['then'] = NULL;
$arguments928['else'] = NULL;
$renderChildrenClosure929 = function() use ($renderingContext, $self) {
$output930 = '';

$output930 .= ' ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments931 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments932 = array();
$arguments932['id'] = 'media.search.foundMatches';
// Rendering Array
$array933 = array();
$array933['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'searchTerm', $renderingContext);
$arguments932['arguments'] = $array933;
$arguments932['source'] = 'Modules';
$arguments932['package'] = 'TYPO3.Neos';
$arguments932['value'] = NULL;
$arguments932['quantity'] = NULL;
$arguments932['languageIdentifier'] = NULL;
$renderChildrenClosure934 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper935 = $self->getViewHelper('$viewHelper935', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper935->setArguments($arguments932);
$viewHelper935->setRenderingContext($renderingContext);
$viewHelper935->setRenderChildrenClosure($renderChildrenClosure934);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments931['value'] = $viewHelper935->initializeArgumentsAndRender();
$arguments931['keepQuotes'] = false;
$arguments931['encoding'] = 'UTF-8';
$arguments931['doubleEncode'] = true;
$renderChildrenClosure936 = function() use ($renderingContext, $self) {
return NULL;
};
$value937 = ($arguments931['value'] !== NULL ? $arguments931['value'] : $renderChildrenClosure936());

$output930 .= !is_string($value937) && !(is_object($value937) && method_exists($value937, '__toString')) ? $value937 : htmlspecialchars($value937, ($arguments931['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments931['encoding'], $arguments931['doubleEncode']);
return $output930;
};
$viewHelper938 = $self->getViewHelper('$viewHelper938', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper938->setArguments($arguments928);
$viewHelper938->setRenderingContext($renderingContext);
$viewHelper938->setRenderChildrenClosure($renderChildrenClosure929);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output917 .= $viewHelper938->initializeArgumentsAndRender();

$output917 .= '
		</span>
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments939 = array();
$arguments939['action'] = 'new';
$arguments939['additionalAttributes'] = NULL;
$arguments939['data'] = NULL;
$arguments939['arguments'] = array (
);
$arguments939['controller'] = NULL;
$arguments939['package'] = NULL;
$arguments939['subpackage'] = NULL;
$arguments939['section'] = '';
$arguments939['format'] = '';
$arguments939['additionalParams'] = array (
);
$arguments939['addQueryString'] = false;
$arguments939['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments939['useParentRequest'] = false;
$arguments939['absolute'] = true;
$arguments939['class'] = NULL;
$arguments939['dir'] = NULL;
$arguments939['id'] = NULL;
$arguments939['lang'] = NULL;
$arguments939['style'] = NULL;
$arguments939['title'] = NULL;
$arguments939['accesskey'] = NULL;
$arguments939['tabindex'] = NULL;
$arguments939['onclick'] = NULL;
$arguments939['name'] = NULL;
$arguments939['rel'] = NULL;
$arguments939['rev'] = NULL;
$arguments939['target'] = NULL;
$renderChildrenClosure940 = function() use ($renderingContext, $self) {
$output941 = '';

$output941 .= '<i class="icon-upload"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments942 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments943 = array();
$arguments943['id'] = 'media.upload';
$arguments943['source'] = 'Modules';
$arguments943['package'] = 'TYPO3.Neos';
$arguments943['value'] = NULL;
$arguments943['arguments'] = array (
);
$arguments943['quantity'] = NULL;
$arguments943['languageIdentifier'] = NULL;
$renderChildrenClosure944 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper945 = $self->getViewHelper('$viewHelper945', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper945->setArguments($arguments943);
$viewHelper945->setRenderingContext($renderingContext);
$viewHelper945->setRenderChildrenClosure($renderChildrenClosure944);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments942['value'] = $viewHelper945->initializeArgumentsAndRender();
$arguments942['keepQuotes'] = false;
$arguments942['encoding'] = 'UTF-8';
$arguments942['doubleEncode'] = true;
$renderChildrenClosure946 = function() use ($renderingContext, $self) {
return NULL;
};
$value947 = ($arguments942['value'] !== NULL ? $arguments942['value'] : $renderChildrenClosure946());

$output941 .= !is_string($value947) && !(is_object($value947) && method_exists($value947, '__toString')) ? $value947 : htmlspecialchars($value947, ($arguments942['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments942['encoding'], $arguments942['doubleEncode']);
return $output941;
};
$viewHelper948 = $self->getViewHelper('$viewHelper948', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper948->setArguments($arguments939);
$viewHelper948->setRenderingContext($renderingContext);
$viewHelper948->setRenderChildrenClosure($renderChildrenClosure940);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output917 .= $viewHelper948->initializeArgumentsAndRender();

$output917 .= '
	</div>
	<div class="neos-view-options">
		<div class="neos-dropdown" id="neos-filter-menu">
			<span title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments949 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments950 = array();
$arguments950['id'] = 'media.filterOptions';
$arguments950['source'] = 'Modules';
$arguments950['package'] = 'TYPO3.Neos';
$arguments950['value'] = NULL;
$arguments950['arguments'] = array (
);
$arguments950['quantity'] = NULL;
$arguments950['languageIdentifier'] = NULL;
$renderChildrenClosure951 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper952 = $self->getViewHelper('$viewHelper952', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper952->setArguments($arguments950);
$viewHelper952->setRenderingContext($renderingContext);
$viewHelper952->setRenderChildrenClosure($renderChildrenClosure951);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments949['value'] = $viewHelper952->initializeArgumentsAndRender();
$arguments949['keepQuotes'] = false;
$arguments949['encoding'] = 'UTF-8';
$arguments949['doubleEncode'] = true;
$renderChildrenClosure953 = function() use ($renderingContext, $self) {
return NULL;
};
$value954 = ($arguments949['value'] !== NULL ? $arguments949['value'] : $renderChildrenClosure953());

$output917 .= !is_string($value954) && !(is_object($value954) && method_exists($value954, '__toString')) ? $value954 : htmlspecialchars($value954, ($arguments949['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments949['encoding'], $arguments949['doubleEncode']);

$output917 .= '" data-neos-toggle="tooltip">
				<a class="dropdown-toggle';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments955 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments956 = array();
$arguments956['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'filter', $renderingContext);
$arguments956['keepQuotes'] = false;
$arguments956['encoding'] = 'UTF-8';
$arguments956['doubleEncode'] = true;
$renderChildrenClosure957 = function() use ($renderingContext, $self) {
return NULL;
};
$value958 = ($arguments956['value'] !== NULL ? $arguments956['value'] : $renderChildrenClosure957());
$arguments955['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('!=', !is_string($value958) && !(is_object($value958) && method_exists($value958, '__toString')) ? $value958 : htmlspecialchars($value958, ($arguments956['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments956['encoding'], $arguments956['doubleEncode']), 'All');
$arguments955['then'] = ' neos-active';
$arguments955['else'] = NULL;
$renderChildrenClosure959 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper960 = $self->getViewHelper('$viewHelper960', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper960->setArguments($arguments955);
$viewHelper960->setRenderingContext($renderingContext);
$viewHelper960->setRenderChildrenClosure($renderChildrenClosure959);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output917 .= $viewHelper960->initializeArgumentsAndRender();

$output917 .= '" href="#" data-neos-toggle="dropdown" data-target="#neos-filter-menu">
					<i class="icon-filter"></i>
				</a>
			</span>
			<ul class="neos-dropdown-menu neos-pull-right" role="menu">
				<li>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments961 = array();
$arguments961['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments962 = array();
$arguments962['id'] = 'media.filter.title.all';
$arguments962['source'] = 'Modules';
$arguments962['package'] = 'TYPO3.Neos';
$arguments962['value'] = NULL;
$arguments962['arguments'] = array (
);
$arguments962['quantity'] = NULL;
$arguments962['languageIdentifier'] = NULL;
$renderChildrenClosure963 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper964 = $self->getViewHelper('$viewHelper964', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper964->setArguments($arguments962);
$viewHelper964->setRenderingContext($renderingContext);
$viewHelper964->setRenderChildrenClosure($renderChildrenClosure963);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments961['title'] = $viewHelper964->initializeArgumentsAndRender();
// Rendering Array
$array965 = array();
$array965['filter'] = 'All';
$arguments961['arguments'] = $array965;
// Rendering Boolean node
$arguments961['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments966 = array();
// Rendering Boolean node
$arguments966['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'filter', $renderingContext), 'All');
$arguments966['then'] = 'neos-active';
$arguments966['else'] = NULL;
$renderChildrenClosure967 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper968 = $self->getViewHelper('$viewHelper968', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper968->setArguments($arguments966);
$viewHelper968->setRenderingContext($renderingContext);
$viewHelper968->setRenderChildrenClosure($renderChildrenClosure967);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments961['class'] = $viewHelper968->initializeArgumentsAndRender();
$arguments961['additionalAttributes'] = NULL;
$arguments961['data'] = NULL;
$arguments961['controller'] = NULL;
$arguments961['package'] = NULL;
$arguments961['subpackage'] = NULL;
$arguments961['section'] = '';
$arguments961['format'] = '';
$arguments961['additionalParams'] = array (
);
$arguments961['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments961['useParentRequest'] = false;
$arguments961['absolute'] = true;
$arguments961['dir'] = NULL;
$arguments961['id'] = NULL;
$arguments961['lang'] = NULL;
$arguments961['style'] = NULL;
$arguments961['accesskey'] = NULL;
$arguments961['tabindex'] = NULL;
$arguments961['onclick'] = NULL;
$arguments961['name'] = NULL;
$arguments961['rel'] = NULL;
$arguments961['rev'] = NULL;
$arguments961['target'] = NULL;
$renderChildrenClosure969 = function() use ($renderingContext, $self) {
$output970 = '';

$output970 .= '<i class="icon-filter"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments971 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments972 = array();
$arguments972['id'] = 'media.filter.all';
$arguments972['source'] = 'Modules';
$arguments972['package'] = 'TYPO3.Neos';
$arguments972['value'] = NULL;
$arguments972['arguments'] = array (
);
$arguments972['quantity'] = NULL;
$arguments972['languageIdentifier'] = NULL;
$renderChildrenClosure973 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper974 = $self->getViewHelper('$viewHelper974', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper974->setArguments($arguments972);
$viewHelper974->setRenderingContext($renderingContext);
$viewHelper974->setRenderChildrenClosure($renderChildrenClosure973);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments971['value'] = $viewHelper974->initializeArgumentsAndRender();
$arguments971['keepQuotes'] = false;
$arguments971['encoding'] = 'UTF-8';
$arguments971['doubleEncode'] = true;
$renderChildrenClosure975 = function() use ($renderingContext, $self) {
return NULL;
};
$value976 = ($arguments971['value'] !== NULL ? $arguments971['value'] : $renderChildrenClosure975());

$output970 .= !is_string($value976) && !(is_object($value976) && method_exists($value976, '__toString')) ? $value976 : htmlspecialchars($value976, ($arguments971['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments971['encoding'], $arguments971['doubleEncode']);
return $output970;
};
$viewHelper977 = $self->getViewHelper('$viewHelper977', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper977->setArguments($arguments961);
$viewHelper977->setRenderingContext($renderingContext);
$viewHelper977->setRenderChildrenClosure($renderChildrenClosure969);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output917 .= $viewHelper977->initializeArgumentsAndRender();

$output917 .= '
				</li>
				<li>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments978 = array();
$arguments978['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments979 = array();
$arguments979['id'] = 'media.filter.title.images';
$arguments979['source'] = 'Modules';
$arguments979['package'] = 'TYPO3.Neos';
$arguments979['value'] = NULL;
$arguments979['arguments'] = array (
);
$arguments979['quantity'] = NULL;
$arguments979['languageIdentifier'] = NULL;
$renderChildrenClosure980 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper981 = $self->getViewHelper('$viewHelper981', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper981->setArguments($arguments979);
$viewHelper981->setRenderingContext($renderingContext);
$viewHelper981->setRenderChildrenClosure($renderChildrenClosure980);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments978['title'] = $viewHelper981->initializeArgumentsAndRender();
// Rendering Array
$array982 = array();
$array982['filter'] = 'Image';
$arguments978['arguments'] = $array982;
// Rendering Boolean node
$arguments978['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments983 = array();
// Rendering Boolean node
$arguments983['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'filter', $renderingContext), 'Image');
$arguments983['then'] = 'neos-active';
$arguments983['else'] = NULL;
$renderChildrenClosure984 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper985 = $self->getViewHelper('$viewHelper985', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper985->setArguments($arguments983);
$viewHelper985->setRenderingContext($renderingContext);
$viewHelper985->setRenderChildrenClosure($renderChildrenClosure984);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments978['class'] = $viewHelper985->initializeArgumentsAndRender();
$arguments978['additionalAttributes'] = NULL;
$arguments978['data'] = NULL;
$arguments978['controller'] = NULL;
$arguments978['package'] = NULL;
$arguments978['subpackage'] = NULL;
$arguments978['section'] = '';
$arguments978['format'] = '';
$arguments978['additionalParams'] = array (
);
$arguments978['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments978['useParentRequest'] = false;
$arguments978['absolute'] = true;
$arguments978['dir'] = NULL;
$arguments978['id'] = NULL;
$arguments978['lang'] = NULL;
$arguments978['style'] = NULL;
$arguments978['accesskey'] = NULL;
$arguments978['tabindex'] = NULL;
$arguments978['onclick'] = NULL;
$arguments978['name'] = NULL;
$arguments978['rel'] = NULL;
$arguments978['rev'] = NULL;
$arguments978['target'] = NULL;
$renderChildrenClosure986 = function() use ($renderingContext, $self) {
$output987 = '';

$output987 .= '<i class="icon-picture"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments988 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments989 = array();
$arguments989['id'] = 'media.filter.images';
$arguments989['source'] = 'Modules';
$arguments989['package'] = 'TYPO3.Neos';
$arguments989['value'] = NULL;
$arguments989['arguments'] = array (
);
$arguments989['quantity'] = NULL;
$arguments989['languageIdentifier'] = NULL;
$renderChildrenClosure990 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper991 = $self->getViewHelper('$viewHelper991', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper991->setArguments($arguments989);
$viewHelper991->setRenderingContext($renderingContext);
$viewHelper991->setRenderChildrenClosure($renderChildrenClosure990);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments988['value'] = $viewHelper991->initializeArgumentsAndRender();
$arguments988['keepQuotes'] = false;
$arguments988['encoding'] = 'UTF-8';
$arguments988['doubleEncode'] = true;
$renderChildrenClosure992 = function() use ($renderingContext, $self) {
return NULL;
};
$value993 = ($arguments988['value'] !== NULL ? $arguments988['value'] : $renderChildrenClosure992());

$output987 .= !is_string($value993) && !(is_object($value993) && method_exists($value993, '__toString')) ? $value993 : htmlspecialchars($value993, ($arguments988['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments988['encoding'], $arguments988['doubleEncode']);
return $output987;
};
$viewHelper994 = $self->getViewHelper('$viewHelper994', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper994->setArguments($arguments978);
$viewHelper994->setRenderingContext($renderingContext);
$viewHelper994->setRenderChildrenClosure($renderChildrenClosure986);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output917 .= $viewHelper994->initializeArgumentsAndRender();

$output917 .= '
				</li>
				<li>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments995 = array();
$arguments995['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments996 = array();
$arguments996['id'] = 'media.filter.title.documents';
$arguments996['source'] = 'Modules';
$arguments996['package'] = 'TYPO3.Neos';
$arguments996['value'] = NULL;
$arguments996['arguments'] = array (
);
$arguments996['quantity'] = NULL;
$arguments996['languageIdentifier'] = NULL;
$renderChildrenClosure997 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper998 = $self->getViewHelper('$viewHelper998', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper998->setArguments($arguments996);
$viewHelper998->setRenderingContext($renderingContext);
$viewHelper998->setRenderChildrenClosure($renderChildrenClosure997);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments995['title'] = $viewHelper998->initializeArgumentsAndRender();
// Rendering Array
$array999 = array();
$array999['filter'] = 'Document';
$arguments995['arguments'] = $array999;
// Rendering Boolean node
$arguments995['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1000 = array();
// Rendering Boolean node
$arguments1000['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'filter', $renderingContext), 'Document');
$arguments1000['then'] = 'neos-active';
$arguments1000['else'] = NULL;
$renderChildrenClosure1001 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1002 = $self->getViewHelper('$viewHelper1002', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1002->setArguments($arguments1000);
$viewHelper1002->setRenderingContext($renderingContext);
$viewHelper1002->setRenderChildrenClosure($renderChildrenClosure1001);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments995['class'] = $viewHelper1002->initializeArgumentsAndRender();
$arguments995['additionalAttributes'] = NULL;
$arguments995['data'] = NULL;
$arguments995['controller'] = NULL;
$arguments995['package'] = NULL;
$arguments995['subpackage'] = NULL;
$arguments995['section'] = '';
$arguments995['format'] = '';
$arguments995['additionalParams'] = array (
);
$arguments995['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments995['useParentRequest'] = false;
$arguments995['absolute'] = true;
$arguments995['dir'] = NULL;
$arguments995['id'] = NULL;
$arguments995['lang'] = NULL;
$arguments995['style'] = NULL;
$arguments995['accesskey'] = NULL;
$arguments995['tabindex'] = NULL;
$arguments995['onclick'] = NULL;
$arguments995['name'] = NULL;
$arguments995['rel'] = NULL;
$arguments995['rev'] = NULL;
$arguments995['target'] = NULL;
$renderChildrenClosure1003 = function() use ($renderingContext, $self) {
$output1004 = '';

$output1004 .= '<i class="icon-file-text"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1005 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1006 = array();
$arguments1006['id'] = 'media.filter.documents';
$arguments1006['source'] = 'Modules';
$arguments1006['package'] = 'TYPO3.Neos';
$arguments1006['value'] = NULL;
$arguments1006['arguments'] = array (
);
$arguments1006['quantity'] = NULL;
$arguments1006['languageIdentifier'] = NULL;
$renderChildrenClosure1007 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1008 = $self->getViewHelper('$viewHelper1008', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1008->setArguments($arguments1006);
$viewHelper1008->setRenderingContext($renderingContext);
$viewHelper1008->setRenderChildrenClosure($renderChildrenClosure1007);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1005['value'] = $viewHelper1008->initializeArgumentsAndRender();
$arguments1005['keepQuotes'] = false;
$arguments1005['encoding'] = 'UTF-8';
$arguments1005['doubleEncode'] = true;
$renderChildrenClosure1009 = function() use ($renderingContext, $self) {
return NULL;
};
$value1010 = ($arguments1005['value'] !== NULL ? $arguments1005['value'] : $renderChildrenClosure1009());

$output1004 .= !is_string($value1010) && !(is_object($value1010) && method_exists($value1010, '__toString')) ? $value1010 : htmlspecialchars($value1010, ($arguments1005['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1005['encoding'], $arguments1005['doubleEncode']);
return $output1004;
};
$viewHelper1011 = $self->getViewHelper('$viewHelper1011', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper1011->setArguments($arguments995);
$viewHelper1011->setRenderingContext($renderingContext);
$viewHelper1011->setRenderChildrenClosure($renderChildrenClosure1003);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output917 .= $viewHelper1011->initializeArgumentsAndRender();

$output917 .= '
				</li>
				<li>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments1012 = array();
$arguments1012['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1013 = array();
$arguments1013['id'] = 'media.filter.title.video';
$arguments1013['source'] = 'Modules';
$arguments1013['package'] = 'TYPO3.Neos';
$arguments1013['value'] = NULL;
$arguments1013['arguments'] = array (
);
$arguments1013['quantity'] = NULL;
$arguments1013['languageIdentifier'] = NULL;
$renderChildrenClosure1014 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1015 = $self->getViewHelper('$viewHelper1015', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1015->setArguments($arguments1013);
$viewHelper1015->setRenderingContext($renderingContext);
$viewHelper1015->setRenderChildrenClosure($renderChildrenClosure1014);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1012['title'] = $viewHelper1015->initializeArgumentsAndRender();
// Rendering Array
$array1016 = array();
$array1016['filter'] = 'Video';
$arguments1012['arguments'] = $array1016;
// Rendering Boolean node
$arguments1012['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1017 = array();
// Rendering Boolean node
$arguments1017['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'filter', $renderingContext), 'Video');
$arguments1017['then'] = 'neos-active';
$arguments1017['else'] = NULL;
$renderChildrenClosure1018 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1019 = $self->getViewHelper('$viewHelper1019', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1019->setArguments($arguments1017);
$viewHelper1019->setRenderingContext($renderingContext);
$viewHelper1019->setRenderChildrenClosure($renderChildrenClosure1018);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1012['class'] = $viewHelper1019->initializeArgumentsAndRender();
$arguments1012['additionalAttributes'] = NULL;
$arguments1012['data'] = NULL;
$arguments1012['controller'] = NULL;
$arguments1012['package'] = NULL;
$arguments1012['subpackage'] = NULL;
$arguments1012['section'] = '';
$arguments1012['format'] = '';
$arguments1012['additionalParams'] = array (
);
$arguments1012['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1012['useParentRequest'] = false;
$arguments1012['absolute'] = true;
$arguments1012['dir'] = NULL;
$arguments1012['id'] = NULL;
$arguments1012['lang'] = NULL;
$arguments1012['style'] = NULL;
$arguments1012['accesskey'] = NULL;
$arguments1012['tabindex'] = NULL;
$arguments1012['onclick'] = NULL;
$arguments1012['name'] = NULL;
$arguments1012['rel'] = NULL;
$arguments1012['rev'] = NULL;
$arguments1012['target'] = NULL;
$renderChildrenClosure1020 = function() use ($renderingContext, $self) {
$output1021 = '';

$output1021 .= '<i class="icon-film"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1022 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1023 = array();
$arguments1023['id'] = 'media.filter.video';
$arguments1023['source'] = 'Modules';
$arguments1023['package'] = 'TYPO3.Neos';
$arguments1023['value'] = NULL;
$arguments1023['arguments'] = array (
);
$arguments1023['quantity'] = NULL;
$arguments1023['languageIdentifier'] = NULL;
$renderChildrenClosure1024 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1025 = $self->getViewHelper('$viewHelper1025', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1025->setArguments($arguments1023);
$viewHelper1025->setRenderingContext($renderingContext);
$viewHelper1025->setRenderChildrenClosure($renderChildrenClosure1024);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1022['value'] = $viewHelper1025->initializeArgumentsAndRender();
$arguments1022['keepQuotes'] = false;
$arguments1022['encoding'] = 'UTF-8';
$arguments1022['doubleEncode'] = true;
$renderChildrenClosure1026 = function() use ($renderingContext, $self) {
return NULL;
};
$value1027 = ($arguments1022['value'] !== NULL ? $arguments1022['value'] : $renderChildrenClosure1026());

$output1021 .= !is_string($value1027) && !(is_object($value1027) && method_exists($value1027, '__toString')) ? $value1027 : htmlspecialchars($value1027, ($arguments1022['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1022['encoding'], $arguments1022['doubleEncode']);
return $output1021;
};
$viewHelper1028 = $self->getViewHelper('$viewHelper1028', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper1028->setArguments($arguments1012);
$viewHelper1028->setRenderingContext($renderingContext);
$viewHelper1028->setRenderChildrenClosure($renderChildrenClosure1020);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output917 .= $viewHelper1028->initializeArgumentsAndRender();

$output917 .= '
				</li>
				<li>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments1029 = array();
$arguments1029['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1030 = array();
$arguments1030['id'] = 'media.filter.title.audio';
$arguments1030['source'] = 'Modules';
$arguments1030['package'] = 'TYPO3.Neos';
$arguments1030['value'] = NULL;
$arguments1030['arguments'] = array (
);
$arguments1030['quantity'] = NULL;
$arguments1030['languageIdentifier'] = NULL;
$renderChildrenClosure1031 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1032 = $self->getViewHelper('$viewHelper1032', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1032->setArguments($arguments1030);
$viewHelper1032->setRenderingContext($renderingContext);
$viewHelper1032->setRenderChildrenClosure($renderChildrenClosure1031);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1029['title'] = $viewHelper1032->initializeArgumentsAndRender();
// Rendering Array
$array1033 = array();
$array1033['filter'] = 'Audio';
$arguments1029['arguments'] = $array1033;
// Rendering Boolean node
$arguments1029['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1034 = array();
// Rendering Boolean node
$arguments1034['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'filter', $renderingContext), 'Audio');
$arguments1034['then'] = 'neos-active';
$arguments1034['else'] = NULL;
$renderChildrenClosure1035 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1036 = $self->getViewHelper('$viewHelper1036', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1036->setArguments($arguments1034);
$viewHelper1036->setRenderingContext($renderingContext);
$viewHelper1036->setRenderChildrenClosure($renderChildrenClosure1035);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1029['class'] = $viewHelper1036->initializeArgumentsAndRender();
$arguments1029['additionalAttributes'] = NULL;
$arguments1029['data'] = NULL;
$arguments1029['controller'] = NULL;
$arguments1029['package'] = NULL;
$arguments1029['subpackage'] = NULL;
$arguments1029['section'] = '';
$arguments1029['format'] = '';
$arguments1029['additionalParams'] = array (
);
$arguments1029['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1029['useParentRequest'] = false;
$arguments1029['absolute'] = true;
$arguments1029['dir'] = NULL;
$arguments1029['id'] = NULL;
$arguments1029['lang'] = NULL;
$arguments1029['style'] = NULL;
$arguments1029['accesskey'] = NULL;
$arguments1029['tabindex'] = NULL;
$arguments1029['onclick'] = NULL;
$arguments1029['name'] = NULL;
$arguments1029['rel'] = NULL;
$arguments1029['rev'] = NULL;
$arguments1029['target'] = NULL;
$renderChildrenClosure1037 = function() use ($renderingContext, $self) {
$output1038 = '';

$output1038 .= '<i class="icon-music"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1039 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1040 = array();
$arguments1040['id'] = 'media.filter.audio';
$arguments1040['source'] = 'Modules';
$arguments1040['package'] = 'TYPO3.Neos';
$arguments1040['value'] = NULL;
$arguments1040['arguments'] = array (
);
$arguments1040['quantity'] = NULL;
$arguments1040['languageIdentifier'] = NULL;
$renderChildrenClosure1041 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1042 = $self->getViewHelper('$viewHelper1042', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1042->setArguments($arguments1040);
$viewHelper1042->setRenderingContext($renderingContext);
$viewHelper1042->setRenderChildrenClosure($renderChildrenClosure1041);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1039['value'] = $viewHelper1042->initializeArgumentsAndRender();
$arguments1039['keepQuotes'] = false;
$arguments1039['encoding'] = 'UTF-8';
$arguments1039['doubleEncode'] = true;
$renderChildrenClosure1043 = function() use ($renderingContext, $self) {
return NULL;
};
$value1044 = ($arguments1039['value'] !== NULL ? $arguments1039['value'] : $renderChildrenClosure1043());

$output1038 .= !is_string($value1044) && !(is_object($value1044) && method_exists($value1044, '__toString')) ? $value1044 : htmlspecialchars($value1044, ($arguments1039['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1039['encoding'], $arguments1039['doubleEncode']);
return $output1038;
};
$viewHelper1045 = $self->getViewHelper('$viewHelper1045', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper1045->setArguments($arguments1029);
$viewHelper1045->setRenderingContext($renderingContext);
$viewHelper1045->setRenderChildrenClosure($renderChildrenClosure1037);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output917 .= $viewHelper1045->initializeArgumentsAndRender();

$output917 .= '
				</li>
			</ul>
		</div>
		<div class="neos-dropdown" id="neos-sort-menu">
			<span title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1046 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1047 = array();
$arguments1047['id'] = 'media.tooltip.sortOptions';
$arguments1047['source'] = 'Modules';
$arguments1047['package'] = 'TYPO3.Neos';
$arguments1047['value'] = NULL;
$arguments1047['arguments'] = array (
);
$arguments1047['quantity'] = NULL;
$arguments1047['languageIdentifier'] = NULL;
$renderChildrenClosure1048 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1049 = $self->getViewHelper('$viewHelper1049', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1049->setArguments($arguments1047);
$viewHelper1049->setRenderingContext($renderingContext);
$viewHelper1049->setRenderChildrenClosure($renderChildrenClosure1048);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1046['value'] = $viewHelper1049->initializeArgumentsAndRender();
$arguments1046['keepQuotes'] = false;
$arguments1046['encoding'] = 'UTF-8';
$arguments1046['doubleEncode'] = true;
$renderChildrenClosure1050 = function() use ($renderingContext, $self) {
return NULL;
};
$value1051 = ($arguments1046['value'] !== NULL ? $arguments1046['value'] : $renderChildrenClosure1050());

$output917 .= !is_string($value1051) && !(is_object($value1051) && method_exists($value1051, '__toString')) ? $value1051 : htmlspecialchars($value1051, ($arguments1046['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1046['encoding'], $arguments1046['doubleEncode']);

$output917 .= '" data-neos-toggle="tooltip">
				<a class="dropdown-toggle" href="#" data-neos-toggle="dropdown" data-target="#neos-sort-menu">
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1052 = array();
// Rendering Boolean node
$arguments1052['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortDirection', $renderingContext), 'ASC');
$arguments1052['then'] = NULL;
$arguments1052['else'] = NULL;
$renderChildrenClosure1053 = function() use ($renderingContext, $self) {
$output1054 = '';

$output1054 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments1055 = array();
$renderChildrenClosure1056 = function() use ($renderingContext, $self) {
$output1057 = '';

$output1057 .= '
							<i class="icon-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1058 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1059 = array();
$arguments1059['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext);
$arguments1059['keepQuotes'] = false;
$arguments1059['encoding'] = 'UTF-8';
$arguments1059['doubleEncode'] = true;
$renderChildrenClosure1060 = function() use ($renderingContext, $self) {
return NULL;
};
$value1061 = ($arguments1059['value'] !== NULL ? $arguments1059['value'] : $renderChildrenClosure1060());
$arguments1058['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', !is_string($value1061) && !(is_object($value1061) && method_exists($value1061, '__toString')) ? $value1061 : htmlspecialchars($value1061, ($arguments1059['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1059['encoding'], $arguments1059['doubleEncode']), 'Modified');
$arguments1058['then'] = 'sort-by-order';
$arguments1058['else'] = 'sort-by-alphabet';
$renderChildrenClosure1062 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1063 = $self->getViewHelper('$viewHelper1063', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1063->setArguments($arguments1058);
$viewHelper1063->setRenderingContext($renderingContext);
$viewHelper1063->setRenderChildrenClosure($renderChildrenClosure1062);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output1057 .= $viewHelper1063->initializeArgumentsAndRender();

$output1057 .= '"></i>
						';
return $output1057;
};
$viewHelper1064 = $self->getViewHelper('$viewHelper1064', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper1064->setArguments($arguments1055);
$viewHelper1064->setRenderingContext($renderingContext);
$viewHelper1064->setRenderChildrenClosure($renderChildrenClosure1056);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output1054 .= $viewHelper1064->initializeArgumentsAndRender();

$output1054 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments1065 = array();
$renderChildrenClosure1066 = function() use ($renderingContext, $self) {
$output1067 = '';

$output1067 .= '
							<i class="icon-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1068 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1069 = array();
$arguments1069['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext);
$arguments1069['keepQuotes'] = false;
$arguments1069['encoding'] = 'UTF-8';
$arguments1069['doubleEncode'] = true;
$renderChildrenClosure1070 = function() use ($renderingContext, $self) {
return NULL;
};
$value1071 = ($arguments1069['value'] !== NULL ? $arguments1069['value'] : $renderChildrenClosure1070());
$arguments1068['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', !is_string($value1071) && !(is_object($value1071) && method_exists($value1071, '__toString')) ? $value1071 : htmlspecialchars($value1071, ($arguments1069['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1069['encoding'], $arguments1069['doubleEncode']), 'Modified');
$arguments1068['then'] = 'sort-by-order-alt';
$arguments1068['else'] = 'sort-by-alphabet-alt';
$renderChildrenClosure1072 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1073 = $self->getViewHelper('$viewHelper1073', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1073->setArguments($arguments1068);
$viewHelper1073->setRenderingContext($renderingContext);
$viewHelper1073->setRenderChildrenClosure($renderChildrenClosure1072);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output1067 .= $viewHelper1073->initializeArgumentsAndRender();

$output1067 .= '"></i>
						';
return $output1067;
};
$viewHelper1074 = $self->getViewHelper('$viewHelper1074', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper1074->setArguments($arguments1065);
$viewHelper1074->setRenderingContext($renderingContext);
$viewHelper1074->setRenderChildrenClosure($renderChildrenClosure1066);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output1054 .= $viewHelper1074->initializeArgumentsAndRender();

$output1054 .= '
					';
return $output1054;
};
$arguments1052['__thenClosure'] = function() use ($renderingContext, $self) {
$output1075 = '';

$output1075 .= '
							<i class="icon-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1076 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1077 = array();
$arguments1077['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext);
$arguments1077['keepQuotes'] = false;
$arguments1077['encoding'] = 'UTF-8';
$arguments1077['doubleEncode'] = true;
$renderChildrenClosure1078 = function() use ($renderingContext, $self) {
return NULL;
};
$value1079 = ($arguments1077['value'] !== NULL ? $arguments1077['value'] : $renderChildrenClosure1078());
$arguments1076['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', !is_string($value1079) && !(is_object($value1079) && method_exists($value1079, '__toString')) ? $value1079 : htmlspecialchars($value1079, ($arguments1077['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1077['encoding'], $arguments1077['doubleEncode']), 'Modified');
$arguments1076['then'] = 'sort-by-order';
$arguments1076['else'] = 'sort-by-alphabet';
$renderChildrenClosure1080 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1081 = $self->getViewHelper('$viewHelper1081', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1081->setArguments($arguments1076);
$viewHelper1081->setRenderingContext($renderingContext);
$viewHelper1081->setRenderChildrenClosure($renderChildrenClosure1080);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output1075 .= $viewHelper1081->initializeArgumentsAndRender();

$output1075 .= '"></i>
						';
return $output1075;
};
$arguments1052['__elseClosure'] = function() use ($renderingContext, $self) {
$output1082 = '';

$output1082 .= '
							<i class="icon-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1083 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1084 = array();
$arguments1084['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext);
$arguments1084['keepQuotes'] = false;
$arguments1084['encoding'] = 'UTF-8';
$arguments1084['doubleEncode'] = true;
$renderChildrenClosure1085 = function() use ($renderingContext, $self) {
return NULL;
};
$value1086 = ($arguments1084['value'] !== NULL ? $arguments1084['value'] : $renderChildrenClosure1085());
$arguments1083['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', !is_string($value1086) && !(is_object($value1086) && method_exists($value1086, '__toString')) ? $value1086 : htmlspecialchars($value1086, ($arguments1084['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1084['encoding'], $arguments1084['doubleEncode']), 'Modified');
$arguments1083['then'] = 'sort-by-order-alt';
$arguments1083['else'] = 'sort-by-alphabet-alt';
$renderChildrenClosure1087 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1088 = $self->getViewHelper('$viewHelper1088', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1088->setArguments($arguments1083);
$viewHelper1088->setRenderingContext($renderingContext);
$viewHelper1088->setRenderChildrenClosure($renderChildrenClosure1087);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output1082 .= $viewHelper1088->initializeArgumentsAndRender();

$output1082 .= '"></i>
						';
return $output1082;
};
$viewHelper1089 = $self->getViewHelper('$viewHelper1089', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1089->setArguments($arguments1052);
$viewHelper1089->setRenderingContext($renderingContext);
$viewHelper1089->setRenderChildrenClosure($renderChildrenClosure1053);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output917 .= $viewHelper1089->initializeArgumentsAndRender();

$output917 .= '
				</a>
			</span>
			<div class="neos-dropdown-menu-list neos-pull-right" role="menu">
				<span class="neos-dropdown-menu-list-title" title="">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1090 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1091 = array();
$arguments1091['id'] = 'media.sortby';
$arguments1091['source'] = 'Modules';
$arguments1091['package'] = 'TYPO3.Neos';
$arguments1091['value'] = NULL;
$arguments1091['arguments'] = array (
);
$arguments1091['quantity'] = NULL;
$arguments1091['languageIdentifier'] = NULL;
$renderChildrenClosure1092 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1093 = $self->getViewHelper('$viewHelper1093', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1093->setArguments($arguments1091);
$viewHelper1093->setRenderingContext($renderingContext);
$viewHelper1093->setRenderChildrenClosure($renderChildrenClosure1092);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1090['value'] = $viewHelper1093->initializeArgumentsAndRender();
$arguments1090['keepQuotes'] = false;
$arguments1090['encoding'] = 'UTF-8';
$arguments1090['doubleEncode'] = true;
$renderChildrenClosure1094 = function() use ($renderingContext, $self) {
return NULL;
};
$value1095 = ($arguments1090['value'] !== NULL ? $arguments1090['value'] : $renderChildrenClosure1094());

$output917 .= !is_string($value1095) && !(is_object($value1095) && method_exists($value1095, '__toString')) ? $value1095 : htmlspecialchars($value1095, ($arguments1090['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1090['encoding'], $arguments1090['doubleEncode']);

$output917 .= '</span>
				<ul>
					<li>
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments1096 = array();
$arguments1096['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1097 = array();
$arguments1097['id'] = 'media.sortByLastModified';
$arguments1097['source'] = 'Modules';
$arguments1097['package'] = 'TYPO3.Neos';
$arguments1097['value'] = NULL;
$arguments1097['arguments'] = array (
);
$arguments1097['quantity'] = NULL;
$arguments1097['languageIdentifier'] = NULL;
$renderChildrenClosure1098 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1099 = $self->getViewHelper('$viewHelper1099', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1099->setArguments($arguments1097);
$viewHelper1099->setRenderingContext($renderingContext);
$viewHelper1099->setRenderChildrenClosure($renderChildrenClosure1098);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1096['title'] = $viewHelper1099->initializeArgumentsAndRender();
// Rendering Array
$array1100 = array();
$array1100['sortBy'] = 'Modified';
$arguments1096['arguments'] = $array1100;
// Rendering Boolean node
$arguments1096['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1101 = array();
// Rendering Boolean node
$arguments1101['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext), 'Modified');
$arguments1101['then'] = 'neos-active';
$arguments1101['else'] = NULL;
$renderChildrenClosure1102 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1103 = $self->getViewHelper('$viewHelper1103', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1103->setArguments($arguments1101);
$viewHelper1103->setRenderingContext($renderingContext);
$viewHelper1103->setRenderChildrenClosure($renderChildrenClosure1102);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1096['class'] = $viewHelper1103->initializeArgumentsAndRender();
$arguments1096['additionalAttributes'] = NULL;
$arguments1096['data'] = NULL;
$arguments1096['controller'] = NULL;
$arguments1096['package'] = NULL;
$arguments1096['subpackage'] = NULL;
$arguments1096['section'] = '';
$arguments1096['format'] = '';
$arguments1096['additionalParams'] = array (
);
$arguments1096['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1096['useParentRequest'] = false;
$arguments1096['absolute'] = true;
$arguments1096['dir'] = NULL;
$arguments1096['id'] = NULL;
$arguments1096['lang'] = NULL;
$arguments1096['style'] = NULL;
$arguments1096['accesskey'] = NULL;
$arguments1096['tabindex'] = NULL;
$arguments1096['onclick'] = NULL;
$arguments1096['name'] = NULL;
$arguments1096['rel'] = NULL;
$arguments1096['rev'] = NULL;
$arguments1096['target'] = NULL;
$renderChildrenClosure1104 = function() use ($renderingContext, $self) {
$output1105 = '';

$output1105 .= '<i class="icon-sort-by-order"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1106 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1107 = array();
$arguments1107['id'] = 'media.field.lastModified';
$arguments1107['source'] = 'Modules';
$arguments1107['package'] = 'TYPO3.Neos';
$arguments1107['value'] = NULL;
$arguments1107['arguments'] = array (
);
$arguments1107['quantity'] = NULL;
$arguments1107['languageIdentifier'] = NULL;
$renderChildrenClosure1108 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1109 = $self->getViewHelper('$viewHelper1109', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1109->setArguments($arguments1107);
$viewHelper1109->setRenderingContext($renderingContext);
$viewHelper1109->setRenderChildrenClosure($renderChildrenClosure1108);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1106['value'] = $viewHelper1109->initializeArgumentsAndRender();
$arguments1106['keepQuotes'] = false;
$arguments1106['encoding'] = 'UTF-8';
$arguments1106['doubleEncode'] = true;
$renderChildrenClosure1110 = function() use ($renderingContext, $self) {
return NULL;
};
$value1111 = ($arguments1106['value'] !== NULL ? $arguments1106['value'] : $renderChildrenClosure1110());

$output1105 .= !is_string($value1111) && !(is_object($value1111) && method_exists($value1111, '__toString')) ? $value1111 : htmlspecialchars($value1111, ($arguments1106['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1106['encoding'], $arguments1106['doubleEncode']);
return $output1105;
};
$viewHelper1112 = $self->getViewHelper('$viewHelper1112', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper1112->setArguments($arguments1096);
$viewHelper1112->setRenderingContext($renderingContext);
$viewHelper1112->setRenderChildrenClosure($renderChildrenClosure1104);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output917 .= $viewHelper1112->initializeArgumentsAndRender();

$output917 .= '
					</li>
					<li>
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments1113 = array();
$arguments1113['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1114 = array();
$arguments1114['id'] = 'media.sortByName';
$arguments1114['source'] = 'Modules';
$arguments1114['package'] = 'TYPO3.Neos';
$arguments1114['value'] = NULL;
$arguments1114['arguments'] = array (
);
$arguments1114['quantity'] = NULL;
$arguments1114['languageIdentifier'] = NULL;
$renderChildrenClosure1115 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1116 = $self->getViewHelper('$viewHelper1116', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1116->setArguments($arguments1114);
$viewHelper1116->setRenderingContext($renderingContext);
$viewHelper1116->setRenderChildrenClosure($renderChildrenClosure1115);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1113['title'] = $viewHelper1116->initializeArgumentsAndRender();
// Rendering Array
$array1117 = array();
$array1117['sortBy'] = 'Name';
$arguments1113['arguments'] = $array1117;
// Rendering Boolean node
$arguments1113['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1118 = array();
// Rendering Boolean node
$arguments1118['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext), 'Name');
$arguments1118['then'] = 'neos-active';
$arguments1118['else'] = NULL;
$renderChildrenClosure1119 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1120 = $self->getViewHelper('$viewHelper1120', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1120->setArguments($arguments1118);
$viewHelper1120->setRenderingContext($renderingContext);
$viewHelper1120->setRenderChildrenClosure($renderChildrenClosure1119);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1113['class'] = $viewHelper1120->initializeArgumentsAndRender();
$arguments1113['additionalAttributes'] = NULL;
$arguments1113['data'] = NULL;
$arguments1113['controller'] = NULL;
$arguments1113['package'] = NULL;
$arguments1113['subpackage'] = NULL;
$arguments1113['section'] = '';
$arguments1113['format'] = '';
$arguments1113['additionalParams'] = array (
);
$arguments1113['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1113['useParentRequest'] = false;
$arguments1113['absolute'] = true;
$arguments1113['dir'] = NULL;
$arguments1113['id'] = NULL;
$arguments1113['lang'] = NULL;
$arguments1113['style'] = NULL;
$arguments1113['accesskey'] = NULL;
$arguments1113['tabindex'] = NULL;
$arguments1113['onclick'] = NULL;
$arguments1113['name'] = NULL;
$arguments1113['rel'] = NULL;
$arguments1113['rev'] = NULL;
$arguments1113['target'] = NULL;
$renderChildrenClosure1121 = function() use ($renderingContext, $self) {
$output1122 = '';

$output1122 .= '<i class="icon-sort-by-alphabet"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1123 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1124 = array();
$arguments1124['id'] = 'media.field.name';
$arguments1124['source'] = 'Modules';
$arguments1124['package'] = 'TYPO3.Neos';
$arguments1124['value'] = NULL;
$arguments1124['arguments'] = array (
);
$arguments1124['quantity'] = NULL;
$arguments1124['languageIdentifier'] = NULL;
$renderChildrenClosure1125 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1126 = $self->getViewHelper('$viewHelper1126', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1126->setArguments($arguments1124);
$viewHelper1126->setRenderingContext($renderingContext);
$viewHelper1126->setRenderChildrenClosure($renderChildrenClosure1125);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1123['value'] = $viewHelper1126->initializeArgumentsAndRender();
$arguments1123['keepQuotes'] = false;
$arguments1123['encoding'] = 'UTF-8';
$arguments1123['doubleEncode'] = true;
$renderChildrenClosure1127 = function() use ($renderingContext, $self) {
return NULL;
};
$value1128 = ($arguments1123['value'] !== NULL ? $arguments1123['value'] : $renderChildrenClosure1127());

$output1122 .= !is_string($value1128) && !(is_object($value1128) && method_exists($value1128, '__toString')) ? $value1128 : htmlspecialchars($value1128, ($arguments1123['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1123['encoding'], $arguments1123['doubleEncode']);
return $output1122;
};
$viewHelper1129 = $self->getViewHelper('$viewHelper1129', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper1129->setArguments($arguments1113);
$viewHelper1129->setRenderingContext($renderingContext);
$viewHelper1129->setRenderChildrenClosure($renderChildrenClosure1121);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output917 .= $viewHelper1129->initializeArgumentsAndRender();

$output917 .= '
					</li>
				</ul>
				<span class="neos-dropdown-menu-list-title">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1130 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1131 = array();
$arguments1131['id'] = 'media.sortdirection';
$arguments1131['source'] = 'Modules';
$arguments1131['package'] = 'TYPO3.Neos';
$arguments1131['value'] = NULL;
$arguments1131['arguments'] = array (
);
$arguments1131['quantity'] = NULL;
$arguments1131['languageIdentifier'] = NULL;
$renderChildrenClosure1132 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1133 = $self->getViewHelper('$viewHelper1133', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1133->setArguments($arguments1131);
$viewHelper1133->setRenderingContext($renderingContext);
$viewHelper1133->setRenderChildrenClosure($renderChildrenClosure1132);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1130['value'] = $viewHelper1133->initializeArgumentsAndRender();
$arguments1130['keepQuotes'] = false;
$arguments1130['encoding'] = 'UTF-8';
$arguments1130['doubleEncode'] = true;
$renderChildrenClosure1134 = function() use ($renderingContext, $self) {
return NULL;
};
$value1135 = ($arguments1130['value'] !== NULL ? $arguments1130['value'] : $renderChildrenClosure1134());

$output917 .= !is_string($value1135) && !(is_object($value1135) && method_exists($value1135, '__toString')) ? $value1135 : htmlspecialchars($value1135, ($arguments1130['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1130['encoding'], $arguments1130['doubleEncode']);

$output917 .= '</span>
				<ul>
					<li>
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments1136 = array();
$arguments1136['action'] = 'index';
$arguments1136['title'] = 'Sort direction Ascending';
// Rendering Array
$array1137 = array();
$array1137['sortDirection'] = 'ASC';
$arguments1136['arguments'] = $array1137;
// Rendering Boolean node
$arguments1136['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1138 = array();
// Rendering Boolean node
$arguments1138['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortDirection', $renderingContext), 'ASC');
$arguments1138['then'] = 'neos-active';
$arguments1138['else'] = NULL;
$renderChildrenClosure1139 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1140 = $self->getViewHelper('$viewHelper1140', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1140->setArguments($arguments1138);
$viewHelper1140->setRenderingContext($renderingContext);
$viewHelper1140->setRenderChildrenClosure($renderChildrenClosure1139);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1136['class'] = $viewHelper1140->initializeArgumentsAndRender();
$arguments1136['additionalAttributes'] = NULL;
$arguments1136['data'] = NULL;
$arguments1136['controller'] = NULL;
$arguments1136['package'] = NULL;
$arguments1136['subpackage'] = NULL;
$arguments1136['section'] = '';
$arguments1136['format'] = '';
$arguments1136['additionalParams'] = array (
);
$arguments1136['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1136['useParentRequest'] = false;
$arguments1136['absolute'] = true;
$arguments1136['dir'] = NULL;
$arguments1136['id'] = NULL;
$arguments1136['lang'] = NULL;
$arguments1136['style'] = NULL;
$arguments1136['accesskey'] = NULL;
$arguments1136['tabindex'] = NULL;
$arguments1136['onclick'] = NULL;
$arguments1136['name'] = NULL;
$arguments1136['rel'] = NULL;
$arguments1136['rev'] = NULL;
$arguments1136['target'] = NULL;
$renderChildrenClosure1141 = function() use ($renderingContext, $self) {
$output1142 = '';

$output1142 .= '<i class="icon-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1143 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1144 = array();
$arguments1144['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext);
$arguments1144['keepQuotes'] = false;
$arguments1144['encoding'] = 'UTF-8';
$arguments1144['doubleEncode'] = true;
$renderChildrenClosure1145 = function() use ($renderingContext, $self) {
return NULL;
};
$value1146 = ($arguments1144['value'] !== NULL ? $arguments1144['value'] : $renderChildrenClosure1145());
$arguments1143['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', !is_string($value1146) && !(is_object($value1146) && method_exists($value1146, '__toString')) ? $value1146 : htmlspecialchars($value1146, ($arguments1144['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1144['encoding'], $arguments1144['doubleEncode']), 'Name');
$arguments1143['then'] = 'sort-by-alphabet';
$arguments1143['else'] = 'sort-by-order';
$renderChildrenClosure1147 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1148 = $self->getViewHelper('$viewHelper1148', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1148->setArguments($arguments1143);
$viewHelper1148->setRenderingContext($renderingContext);
$viewHelper1148->setRenderChildrenClosure($renderChildrenClosure1147);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output1142 .= $viewHelper1148->initializeArgumentsAndRender();

$output1142 .= '"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1149 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1150 = array();
$arguments1150['id'] = 'media.sortdirection.asc';
$arguments1150['source'] = 'Modules';
$arguments1150['package'] = 'TYPO3.Neos';
$arguments1150['value'] = NULL;
$arguments1150['arguments'] = array (
);
$arguments1150['quantity'] = NULL;
$arguments1150['languageIdentifier'] = NULL;
$renderChildrenClosure1151 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1152 = $self->getViewHelper('$viewHelper1152', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1152->setArguments($arguments1150);
$viewHelper1152->setRenderingContext($renderingContext);
$viewHelper1152->setRenderChildrenClosure($renderChildrenClosure1151);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1149['value'] = $viewHelper1152->initializeArgumentsAndRender();
$arguments1149['keepQuotes'] = false;
$arguments1149['encoding'] = 'UTF-8';
$arguments1149['doubleEncode'] = true;
$renderChildrenClosure1153 = function() use ($renderingContext, $self) {
return NULL;
};
$value1154 = ($arguments1149['value'] !== NULL ? $arguments1149['value'] : $renderChildrenClosure1153());

$output1142 .= !is_string($value1154) && !(is_object($value1154) && method_exists($value1154, '__toString')) ? $value1154 : htmlspecialchars($value1154, ($arguments1149['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1149['encoding'], $arguments1149['doubleEncode']);
return $output1142;
};
$viewHelper1155 = $self->getViewHelper('$viewHelper1155', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper1155->setArguments($arguments1136);
$viewHelper1155->setRenderingContext($renderingContext);
$viewHelper1155->setRenderChildrenClosure($renderChildrenClosure1141);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output917 .= $viewHelper1155->initializeArgumentsAndRender();

$output917 .= '
					</li>
					<li>
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments1156 = array();
$arguments1156['action'] = 'index';
$arguments1156['title'] = 'Sort direction Descending';
// Rendering Array
$array1157 = array();
$array1157['sortDirection'] = 'DESC';
$arguments1156['arguments'] = $array1157;
// Rendering Boolean node
$arguments1156['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1158 = array();
// Rendering Boolean node
$arguments1158['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortDirection', $renderingContext), 'DESC');
$arguments1158['then'] = 'neos-active';
$arguments1158['else'] = NULL;
$renderChildrenClosure1159 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1160 = $self->getViewHelper('$viewHelper1160', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1160->setArguments($arguments1158);
$viewHelper1160->setRenderingContext($renderingContext);
$viewHelper1160->setRenderChildrenClosure($renderChildrenClosure1159);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1156['class'] = $viewHelper1160->initializeArgumentsAndRender();
$arguments1156['additionalAttributes'] = NULL;
$arguments1156['data'] = NULL;
$arguments1156['controller'] = NULL;
$arguments1156['package'] = NULL;
$arguments1156['subpackage'] = NULL;
$arguments1156['section'] = '';
$arguments1156['format'] = '';
$arguments1156['additionalParams'] = array (
);
$arguments1156['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1156['useParentRequest'] = false;
$arguments1156['absolute'] = true;
$arguments1156['dir'] = NULL;
$arguments1156['id'] = NULL;
$arguments1156['lang'] = NULL;
$arguments1156['style'] = NULL;
$arguments1156['accesskey'] = NULL;
$arguments1156['tabindex'] = NULL;
$arguments1156['onclick'] = NULL;
$arguments1156['name'] = NULL;
$arguments1156['rel'] = NULL;
$arguments1156['rev'] = NULL;
$arguments1156['target'] = NULL;
$renderChildrenClosure1161 = function() use ($renderingContext, $self) {
$output1162 = '';

$output1162 .= '<i class="icon-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1163 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1164 = array();
$arguments1164['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext);
$arguments1164['keepQuotes'] = false;
$arguments1164['encoding'] = 'UTF-8';
$arguments1164['doubleEncode'] = true;
$renderChildrenClosure1165 = function() use ($renderingContext, $self) {
return NULL;
};
$value1166 = ($arguments1164['value'] !== NULL ? $arguments1164['value'] : $renderChildrenClosure1165());
$arguments1163['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', !is_string($value1166) && !(is_object($value1166) && method_exists($value1166, '__toString')) ? $value1166 : htmlspecialchars($value1166, ($arguments1164['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1164['encoding'], $arguments1164['doubleEncode']), 'Name');
$arguments1163['then'] = 'sort-by-alphabet-alt';
$arguments1163['else'] = 'sort-by-order-alt';
$renderChildrenClosure1167 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1168 = $self->getViewHelper('$viewHelper1168', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1168->setArguments($arguments1163);
$viewHelper1168->setRenderingContext($renderingContext);
$viewHelper1168->setRenderChildrenClosure($renderChildrenClosure1167);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output1162 .= $viewHelper1168->initializeArgumentsAndRender();

$output1162 .= '"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1169 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1170 = array();
$arguments1170['id'] = 'media.sortdirection.desc';
$arguments1170['source'] = 'Modules';
$arguments1170['package'] = 'TYPO3.Neos';
$arguments1170['value'] = NULL;
$arguments1170['arguments'] = array (
);
$arguments1170['quantity'] = NULL;
$arguments1170['languageIdentifier'] = NULL;
$renderChildrenClosure1171 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1172 = $self->getViewHelper('$viewHelper1172', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1172->setArguments($arguments1170);
$viewHelper1172->setRenderingContext($renderingContext);
$viewHelper1172->setRenderChildrenClosure($renderChildrenClosure1171);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1169['value'] = $viewHelper1172->initializeArgumentsAndRender();
$arguments1169['keepQuotes'] = false;
$arguments1169['encoding'] = 'UTF-8';
$arguments1169['doubleEncode'] = true;
$renderChildrenClosure1173 = function() use ($renderingContext, $self) {
return NULL;
};
$value1174 = ($arguments1169['value'] !== NULL ? $arguments1169['value'] : $renderChildrenClosure1173());

$output1162 .= !is_string($value1174) && !(is_object($value1174) && method_exists($value1174, '__toString')) ? $value1174 : htmlspecialchars($value1174, ($arguments1169['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1169['encoding'], $arguments1169['doubleEncode']);
return $output1162;
};
$viewHelper1175 = $self->getViewHelper('$viewHelper1175', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper1175->setArguments($arguments1156);
$viewHelper1175->setRenderingContext($renderingContext);
$viewHelper1175->setRenderChildrenClosure($renderChildrenClosure1161);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output917 .= $viewHelper1175->initializeArgumentsAndRender();

$output917 .= '
					</li>
				</ul>
			</div>
		</div>
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1176 = array();
// Rendering Boolean node
$arguments1176['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'view', $renderingContext), 'Thumbnail');
$arguments1176['then'] = NULL;
$arguments1176['else'] = NULL;
$renderChildrenClosure1177 = function() use ($renderingContext, $self) {
$output1178 = '';

$output1178 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments1179 = array();
$renderChildrenClosure1180 = function() use ($renderingContext, $self) {
$output1181 = '';

$output1181 .= '
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments1182 = array();
$arguments1182['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1183 = array();
$arguments1183['id'] = 'media.tooltip.listView';
$arguments1183['source'] = 'Modules';
$arguments1183['package'] = 'TYPO3.Neos';
$arguments1183['value'] = NULL;
$arguments1183['arguments'] = array (
);
$arguments1183['quantity'] = NULL;
$arguments1183['languageIdentifier'] = NULL;
$renderChildrenClosure1184 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1185 = $self->getViewHelper('$viewHelper1185', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1185->setArguments($arguments1183);
$viewHelper1185->setRenderingContext($renderingContext);
$viewHelper1185->setRenderChildrenClosure($renderChildrenClosure1184);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1182['title'] = $viewHelper1185->initializeArgumentsAndRender();
// Rendering Array
$array1186 = array();
$array1186['view'] = 'List';
$arguments1182['arguments'] = $array1186;
// Rendering Boolean node
$arguments1182['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering Array
$array1187 = array();
$array1187['neos-toggle'] = 'tooltip';
$arguments1182['data'] = $array1187;
$arguments1182['additionalAttributes'] = NULL;
$arguments1182['controller'] = NULL;
$arguments1182['package'] = NULL;
$arguments1182['subpackage'] = NULL;
$arguments1182['section'] = '';
$arguments1182['format'] = '';
$arguments1182['additionalParams'] = array (
);
$arguments1182['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1182['useParentRequest'] = false;
$arguments1182['absolute'] = true;
$arguments1182['class'] = NULL;
$arguments1182['dir'] = NULL;
$arguments1182['id'] = NULL;
$arguments1182['lang'] = NULL;
$arguments1182['style'] = NULL;
$arguments1182['accesskey'] = NULL;
$arguments1182['tabindex'] = NULL;
$arguments1182['onclick'] = NULL;
$arguments1182['name'] = NULL;
$arguments1182['rel'] = NULL;
$arguments1182['rev'] = NULL;
$arguments1182['target'] = NULL;
$renderChildrenClosure1188 = function() use ($renderingContext, $self) {
return '<i class="icon-th-list"></i>';
};
$viewHelper1189 = $self->getViewHelper('$viewHelper1189', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper1189->setArguments($arguments1182);
$viewHelper1189->setRenderingContext($renderingContext);
$viewHelper1189->setRenderChildrenClosure($renderChildrenClosure1188);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output1181 .= $viewHelper1189->initializeArgumentsAndRender();

$output1181 .= '
			';
return $output1181;
};
$viewHelper1190 = $self->getViewHelper('$viewHelper1190', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper1190->setArguments($arguments1179);
$viewHelper1190->setRenderingContext($renderingContext);
$viewHelper1190->setRenderChildrenClosure($renderChildrenClosure1180);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output1178 .= $viewHelper1190->initializeArgumentsAndRender();

$output1178 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments1191 = array();
$renderChildrenClosure1192 = function() use ($renderingContext, $self) {
$output1193 = '';

$output1193 .= '
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments1194 = array();
$arguments1194['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1195 = array();
$arguments1195['id'] = 'media.tooltip.thumbnailView';
$arguments1195['source'] = 'Modules';
$arguments1195['package'] = 'TYPO3.Neos';
$arguments1195['value'] = NULL;
$arguments1195['arguments'] = array (
);
$arguments1195['quantity'] = NULL;
$arguments1195['languageIdentifier'] = NULL;
$renderChildrenClosure1196 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1197 = $self->getViewHelper('$viewHelper1197', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1197->setArguments($arguments1195);
$viewHelper1197->setRenderingContext($renderingContext);
$viewHelper1197->setRenderChildrenClosure($renderChildrenClosure1196);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1194['title'] = $viewHelper1197->initializeArgumentsAndRender();
// Rendering Array
$array1198 = array();
$array1198['view'] = 'Thumbnail';
$arguments1194['arguments'] = $array1198;
// Rendering Boolean node
$arguments1194['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering Array
$array1199 = array();
$array1199['neos-toggle'] = 'tooltip';
$arguments1194['data'] = $array1199;
$arguments1194['additionalAttributes'] = NULL;
$arguments1194['controller'] = NULL;
$arguments1194['package'] = NULL;
$arguments1194['subpackage'] = NULL;
$arguments1194['section'] = '';
$arguments1194['format'] = '';
$arguments1194['additionalParams'] = array (
);
$arguments1194['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1194['useParentRequest'] = false;
$arguments1194['absolute'] = true;
$arguments1194['class'] = NULL;
$arguments1194['dir'] = NULL;
$arguments1194['id'] = NULL;
$arguments1194['lang'] = NULL;
$arguments1194['style'] = NULL;
$arguments1194['accesskey'] = NULL;
$arguments1194['tabindex'] = NULL;
$arguments1194['onclick'] = NULL;
$arguments1194['name'] = NULL;
$arguments1194['rel'] = NULL;
$arguments1194['rev'] = NULL;
$arguments1194['target'] = NULL;
$renderChildrenClosure1200 = function() use ($renderingContext, $self) {
return '<i class="icon-th"></i>';
};
$viewHelper1201 = $self->getViewHelper('$viewHelper1201', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper1201->setArguments($arguments1194);
$viewHelper1201->setRenderingContext($renderingContext);
$viewHelper1201->setRenderChildrenClosure($renderChildrenClosure1200);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output1193 .= $viewHelper1201->initializeArgumentsAndRender();

$output1193 .= '
			';
return $output1193;
};
$viewHelper1202 = $self->getViewHelper('$viewHelper1202', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper1202->setArguments($arguments1191);
$viewHelper1202->setRenderingContext($renderingContext);
$viewHelper1202->setRenderChildrenClosure($renderChildrenClosure1192);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output1178 .= $viewHelper1202->initializeArgumentsAndRender();

$output1178 .= '
		';
return $output1178;
};
$arguments1176['__thenClosure'] = function() use ($renderingContext, $self) {
$output1203 = '';

$output1203 .= '
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments1204 = array();
$arguments1204['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1205 = array();
$arguments1205['id'] = 'media.tooltip.listView';
$arguments1205['source'] = 'Modules';
$arguments1205['package'] = 'TYPO3.Neos';
$arguments1205['value'] = NULL;
$arguments1205['arguments'] = array (
);
$arguments1205['quantity'] = NULL;
$arguments1205['languageIdentifier'] = NULL;
$renderChildrenClosure1206 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1207 = $self->getViewHelper('$viewHelper1207', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1207->setArguments($arguments1205);
$viewHelper1207->setRenderingContext($renderingContext);
$viewHelper1207->setRenderChildrenClosure($renderChildrenClosure1206);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1204['title'] = $viewHelper1207->initializeArgumentsAndRender();
// Rendering Array
$array1208 = array();
$array1208['view'] = 'List';
$arguments1204['arguments'] = $array1208;
// Rendering Boolean node
$arguments1204['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering Array
$array1209 = array();
$array1209['neos-toggle'] = 'tooltip';
$arguments1204['data'] = $array1209;
$arguments1204['additionalAttributes'] = NULL;
$arguments1204['controller'] = NULL;
$arguments1204['package'] = NULL;
$arguments1204['subpackage'] = NULL;
$arguments1204['section'] = '';
$arguments1204['format'] = '';
$arguments1204['additionalParams'] = array (
);
$arguments1204['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1204['useParentRequest'] = false;
$arguments1204['absolute'] = true;
$arguments1204['class'] = NULL;
$arguments1204['dir'] = NULL;
$arguments1204['id'] = NULL;
$arguments1204['lang'] = NULL;
$arguments1204['style'] = NULL;
$arguments1204['accesskey'] = NULL;
$arguments1204['tabindex'] = NULL;
$arguments1204['onclick'] = NULL;
$arguments1204['name'] = NULL;
$arguments1204['rel'] = NULL;
$arguments1204['rev'] = NULL;
$arguments1204['target'] = NULL;
$renderChildrenClosure1210 = function() use ($renderingContext, $self) {
return '<i class="icon-th-list"></i>';
};
$viewHelper1211 = $self->getViewHelper('$viewHelper1211', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper1211->setArguments($arguments1204);
$viewHelper1211->setRenderingContext($renderingContext);
$viewHelper1211->setRenderChildrenClosure($renderChildrenClosure1210);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output1203 .= $viewHelper1211->initializeArgumentsAndRender();

$output1203 .= '
			';
return $output1203;
};
$arguments1176['__elseClosure'] = function() use ($renderingContext, $self) {
$output1212 = '';

$output1212 .= '
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments1213 = array();
$arguments1213['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1214 = array();
$arguments1214['id'] = 'media.tooltip.thumbnailView';
$arguments1214['source'] = 'Modules';
$arguments1214['package'] = 'TYPO3.Neos';
$arguments1214['value'] = NULL;
$arguments1214['arguments'] = array (
);
$arguments1214['quantity'] = NULL;
$arguments1214['languageIdentifier'] = NULL;
$renderChildrenClosure1215 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1216 = $self->getViewHelper('$viewHelper1216', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1216->setArguments($arguments1214);
$viewHelper1216->setRenderingContext($renderingContext);
$viewHelper1216->setRenderChildrenClosure($renderChildrenClosure1215);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1213['title'] = $viewHelper1216->initializeArgumentsAndRender();
// Rendering Array
$array1217 = array();
$array1217['view'] = 'Thumbnail';
$arguments1213['arguments'] = $array1217;
// Rendering Boolean node
$arguments1213['addQueryString'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('TRUE');
// Rendering Array
$array1218 = array();
$array1218['neos-toggle'] = 'tooltip';
$arguments1213['data'] = $array1218;
$arguments1213['additionalAttributes'] = NULL;
$arguments1213['controller'] = NULL;
$arguments1213['package'] = NULL;
$arguments1213['subpackage'] = NULL;
$arguments1213['section'] = '';
$arguments1213['format'] = '';
$arguments1213['additionalParams'] = array (
);
$arguments1213['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1213['useParentRequest'] = false;
$arguments1213['absolute'] = true;
$arguments1213['class'] = NULL;
$arguments1213['dir'] = NULL;
$arguments1213['id'] = NULL;
$arguments1213['lang'] = NULL;
$arguments1213['style'] = NULL;
$arguments1213['accesskey'] = NULL;
$arguments1213['tabindex'] = NULL;
$arguments1213['onclick'] = NULL;
$arguments1213['name'] = NULL;
$arguments1213['rel'] = NULL;
$arguments1213['rev'] = NULL;
$arguments1213['target'] = NULL;
$renderChildrenClosure1219 = function() use ($renderingContext, $self) {
return '<i class="icon-th"></i>';
};
$viewHelper1220 = $self->getViewHelper('$viewHelper1220', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper1220->setArguments($arguments1213);
$viewHelper1220->setRenderingContext($renderingContext);
$viewHelper1220->setRenderChildrenClosure($renderChildrenClosure1219);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output1212 .= $viewHelper1220->initializeArgumentsAndRender();

$output1212 .= '
			';
return $output1212;
};
$viewHelper1221 = $self->getViewHelper('$viewHelper1221', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1221->setArguments($arguments1176);
$viewHelper1221->setRenderingContext($renderingContext);
$viewHelper1221->setRenderChildrenClosure($renderChildrenClosure1177);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output917 .= $viewHelper1221->initializeArgumentsAndRender();

$output917 .= '
	</div>
';
return $output917;
};

$output906 .= '';

$output906 .= '

';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\SectionViewHelper
$arguments1222 = array();
$arguments1222['name'] = 'Sidebar';
$renderChildrenClosure1223 = function() use ($renderingContext, $self) {
$output1224 = '';

$output1224 .= '
	<form action="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1225 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments1226 = array();
$arguments1226['action'] = 'index';
$arguments1226['arguments'] = array (
);
$arguments1226['controller'] = NULL;
$arguments1226['package'] = NULL;
$arguments1226['subpackage'] = NULL;
$arguments1226['section'] = '';
$arguments1226['format'] = '';
$arguments1226['additionalParams'] = array (
);
$arguments1226['absolute'] = false;
$arguments1226['addQueryString'] = false;
$arguments1226['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1226['useParentRequest'] = false;
$renderChildrenClosure1227 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1228 = $self->getViewHelper('$viewHelper1228', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper1228->setArguments($arguments1226);
$viewHelper1228->setRenderingContext($renderingContext);
$viewHelper1228->setRenderChildrenClosure($renderChildrenClosure1227);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments1225['value'] = $viewHelper1228->initializeArgumentsAndRender();
$arguments1225['keepQuotes'] = false;
$arguments1225['encoding'] = 'UTF-8';
$arguments1225['doubleEncode'] = true;
$renderChildrenClosure1229 = function() use ($renderingContext, $self) {
return NULL;
};
$value1230 = ($arguments1225['value'] !== NULL ? $arguments1225['value'] : $renderChildrenClosure1229());

$output1224 .= !is_string($value1230) && !(is_object($value1230) && method_exists($value1230, '__toString')) ? $value1230 : htmlspecialchars($value1230, ($arguments1225['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1225['encoding'], $arguments1225['doubleEncode']);

$output1224 .= '" method="get" class="neos-search">
		<button type="submit" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1231 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1232 = array();
$arguments1232['id'] = 'media.search.title';
$arguments1232['source'] = 'Modules';
$arguments1232['package'] = 'TYPO3.Neos';
$arguments1232['value'] = NULL;
$arguments1232['arguments'] = array (
);
$arguments1232['quantity'] = NULL;
$arguments1232['languageIdentifier'] = NULL;
$renderChildrenClosure1233 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1234 = $self->getViewHelper('$viewHelper1234', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1234->setArguments($arguments1232);
$viewHelper1234->setRenderingContext($renderingContext);
$viewHelper1234->setRenderChildrenClosure($renderChildrenClosure1233);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1231['value'] = $viewHelper1234->initializeArgumentsAndRender();
$arguments1231['keepQuotes'] = false;
$arguments1231['encoding'] = 'UTF-8';
$arguments1231['doubleEncode'] = true;
$renderChildrenClosure1235 = function() use ($renderingContext, $self) {
return NULL;
};
$value1236 = ($arguments1231['value'] !== NULL ? $arguments1231['value'] : $renderChildrenClosure1235());

$output1224 .= !is_string($value1236) && !(is_object($value1236) && method_exists($value1236, '__toString')) ? $value1236 : htmlspecialchars($value1236, ($arguments1231['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1231['encoding'], $arguments1231['doubleEncode']);

$output1224 .= '" data-neos-toggle="tooltip"><i class="icon-search"></i></button>
		<div>
			<input type="search" name="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1237 = array();
// Rendering Boolean node
$arguments1237['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'argumentNamespace', $renderingContext));
$output1238 = '';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1239 = array();
$arguments1239['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'argumentNamespace', $renderingContext);
$arguments1239['keepQuotes'] = false;
$arguments1239['encoding'] = 'UTF-8';
$arguments1239['doubleEncode'] = true;
$renderChildrenClosure1240 = function() use ($renderingContext, $self) {
return NULL;
};
$value1241 = ($arguments1239['value'] !== NULL ? $arguments1239['value'] : $renderChildrenClosure1240());

$output1238 .= !is_string($value1241) && !(is_object($value1241) && method_exists($value1241, '__toString')) ? $value1241 : htmlspecialchars($value1241, ($arguments1239['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1239['encoding'], $arguments1239['doubleEncode']);

$output1238 .= '[searchTerm]';
$arguments1237['then'] = $output1238;
$arguments1237['else'] = 'searchTerm';
$renderChildrenClosure1242 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1243 = $self->getViewHelper('$viewHelper1243', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1243->setArguments($arguments1237);
$viewHelper1243->setRenderingContext($renderingContext);
$viewHelper1243->setRenderChildrenClosure($renderChildrenClosure1242);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output1224 .= $viewHelper1243->initializeArgumentsAndRender();

$output1224 .= '" placeholder="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1244 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1245 = array();
$arguments1245['id'] = 'media.search.placeholder';
$arguments1245['source'] = 'Modules';
$arguments1245['package'] = 'TYPO3.Neos';
$arguments1245['value'] = NULL;
$arguments1245['arguments'] = array (
);
$arguments1245['quantity'] = NULL;
$arguments1245['languageIdentifier'] = NULL;
$renderChildrenClosure1246 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1247 = $self->getViewHelper('$viewHelper1247', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1247->setArguments($arguments1245);
$viewHelper1247->setRenderingContext($renderingContext);
$viewHelper1247->setRenderChildrenClosure($renderChildrenClosure1246);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1244['value'] = $viewHelper1247->initializeArgumentsAndRender();
$arguments1244['keepQuotes'] = false;
$arguments1244['encoding'] = 'UTF-8';
$arguments1244['doubleEncode'] = true;
$renderChildrenClosure1248 = function() use ($renderingContext, $self) {
return NULL;
};
$value1249 = ($arguments1244['value'] !== NULL ? $arguments1244['value'] : $renderChildrenClosure1248());

$output1224 .= !is_string($value1249) && !(is_object($value1249) && method_exists($value1249, '__toString')) ? $value1249 : htmlspecialchars($value1249, ($arguments1244['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1244['encoding'], $arguments1244['doubleEncode']);

$output1224 .= '" value="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1250 = array();
$arguments1250['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'searchTerm', $renderingContext);
$arguments1250['keepQuotes'] = false;
$arguments1250['encoding'] = 'UTF-8';
$arguments1250['doubleEncode'] = true;
$renderChildrenClosure1251 = function() use ($renderingContext, $self) {
return NULL;
};
$value1252 = ($arguments1250['value'] !== NULL ? $arguments1250['value'] : $renderChildrenClosure1251());

$output1224 .= !is_string($value1252) && !(is_object($value1252) && method_exists($value1252, '__toString')) ? $value1252 : htmlspecialchars($value1252, ($arguments1250['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1250['encoding'], $arguments1250['doubleEncode']);

$output1224 .= '"';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1253 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments1254 = array();
$arguments1254['subject'] = NULL;
$renderChildrenClosure1255 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tags', $renderingContext);
};
$viewHelper1256 = $self->getViewHelper('$viewHelper1256', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper1256->setArguments($arguments1254);
$viewHelper1256->setRenderingContext($renderingContext);
$viewHelper1256->setRenderChildrenClosure($renderChildrenClosure1255);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments1253['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('<=', $viewHelper1256->initializeArgumentsAndRender(), 25);
$arguments1253['then'] = ' autofocus="autofocus"';
$arguments1253['else'] = NULL;
$renderChildrenClosure1257 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1258 = $self->getViewHelper('$viewHelper1258', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1258->setArguments($arguments1253);
$viewHelper1258->setRenderingContext($renderingContext);
$viewHelper1258->setRenderChildrenClosure($renderChildrenClosure1257);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output1224 .= $viewHelper1258->initializeArgumentsAndRender();

$output1224 .= ' />
		</div>
	</form>
	<div class="neos-media-aside-group">
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Security\IfAccessViewHelper
$arguments1259 = array();
$arguments1259['privilegeTarget'] = 'TYPO3.Media:ManageAssetCollections';
$arguments1259['then'] = NULL;
$arguments1259['else'] = NULL;
$arguments1259['parameters'] = array (
);
$renderChildrenClosure1260 = function() use ($renderingContext, $self) {
$output1261 = '';

$output1261 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments1262 = array();
$renderChildrenClosure1263 = function() use ($renderingContext, $self) {
$output1264 = '';

$output1264 .= '
				<h2>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1265 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1266 = array();
$arguments1266['id'] = 'media.collections';
$arguments1266['source'] = 'Modules';
$arguments1266['package'] = 'TYPO3.Neos';
$arguments1266['value'] = NULL;
$arguments1266['arguments'] = array (
);
$arguments1266['quantity'] = NULL;
$arguments1266['languageIdentifier'] = NULL;
$renderChildrenClosure1267 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1268 = $self->getViewHelper('$viewHelper1268', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1268->setArguments($arguments1266);
$viewHelper1268->setRenderingContext($renderingContext);
$viewHelper1268->setRenderChildrenClosure($renderChildrenClosure1267);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1265['value'] = $viewHelper1268->initializeArgumentsAndRender();
$arguments1265['keepQuotes'] = false;
$arguments1265['encoding'] = 'UTF-8';
$arguments1265['doubleEncode'] = true;
$renderChildrenClosure1269 = function() use ($renderingContext, $self) {
return NULL;
};
$value1270 = ($arguments1265['value'] !== NULL ? $arguments1265['value'] : $renderChildrenClosure1269());

$output1264 .= !is_string($value1270) && !(is_object($value1270) && method_exists($value1270, '__toString')) ? $value1270 : htmlspecialchars($value1270, ($arguments1265['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1265['encoding'], $arguments1265['doubleEncode']);

$output1264 .= '
					<span class="neos-media-aside-list-edit-toggle neos-button" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1271 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1272 = array();
$arguments1272['id'] = 'media.editCollections';
$arguments1272['source'] = 'Modules';
$arguments1272['package'] = 'TYPO3.Neos';
$arguments1272['value'] = NULL;
$arguments1272['arguments'] = array (
);
$arguments1272['quantity'] = NULL;
$arguments1272['languageIdentifier'] = NULL;
$renderChildrenClosure1273 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1274 = $self->getViewHelper('$viewHelper1274', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1274->setArguments($arguments1272);
$viewHelper1274->setRenderingContext($renderingContext);
$viewHelper1274->setRenderChildrenClosure($renderChildrenClosure1273);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1271['value'] = $viewHelper1274->initializeArgumentsAndRender();
$arguments1271['keepQuotes'] = false;
$arguments1271['encoding'] = 'UTF-8';
$arguments1271['doubleEncode'] = true;
$renderChildrenClosure1275 = function() use ($renderingContext, $self) {
return NULL;
};
$value1276 = ($arguments1271['value'] !== NULL ? $arguments1271['value'] : $renderChildrenClosure1275());

$output1264 .= !is_string($value1276) && !(is_object($value1276) && method_exists($value1276, '__toString')) ? $value1276 : htmlspecialchars($value1276, ($arguments1271['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1271['encoding'], $arguments1271['doubleEncode']);

$output1264 .= '" data-neos-toggle="tooltip"><i class="icon-pencil"></i></span>
				</h2>
			';
return $output1264;
};
$viewHelper1277 = $self->getViewHelper('$viewHelper1277', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper1277->setArguments($arguments1262);
$viewHelper1277->setRenderingContext($renderingContext);
$viewHelper1277->setRenderChildrenClosure($renderChildrenClosure1263);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output1261 .= $viewHelper1277->initializeArgumentsAndRender();

$output1261 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments1278 = array();
$renderChildrenClosure1279 = function() use ($renderingContext, $self) {
$output1280 = '';

$output1280 .= '
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1281 = array();
// Rendering Boolean node
$arguments1281['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollections', $renderingContext));
$arguments1281['then'] = NULL;
$arguments1281['else'] = NULL;
$renderChildrenClosure1282 = function() use ($renderingContext, $self) {
$output1283 = '';

$output1283 .= '
					<h2>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1284 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1285 = array();
$arguments1285['id'] = 'media.collections';
$arguments1285['source'] = 'Modules';
$arguments1285['package'] = 'TYPO3.Neos';
$arguments1285['value'] = NULL;
$arguments1285['arguments'] = array (
);
$arguments1285['quantity'] = NULL;
$arguments1285['languageIdentifier'] = NULL;
$renderChildrenClosure1286 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1287 = $self->getViewHelper('$viewHelper1287', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1287->setArguments($arguments1285);
$viewHelper1287->setRenderingContext($renderingContext);
$viewHelper1287->setRenderChildrenClosure($renderChildrenClosure1286);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1284['value'] = $viewHelper1287->initializeArgumentsAndRender();
$arguments1284['keepQuotes'] = false;
$arguments1284['encoding'] = 'UTF-8';
$arguments1284['doubleEncode'] = true;
$renderChildrenClosure1288 = function() use ($renderingContext, $self) {
return NULL;
};
$value1289 = ($arguments1284['value'] !== NULL ? $arguments1284['value'] : $renderChildrenClosure1288());

$output1283 .= !is_string($value1289) && !(is_object($value1289) && method_exists($value1289, '__toString')) ? $value1289 : htmlspecialchars($value1289, ($arguments1284['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1284['encoding'], $arguments1284['doubleEncode']);

$output1283 .= '</h2>
				';
return $output1283;
};
$viewHelper1290 = $self->getViewHelper('$viewHelper1290', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1290->setArguments($arguments1281);
$viewHelper1290->setRenderingContext($renderingContext);
$viewHelper1290->setRenderChildrenClosure($renderChildrenClosure1282);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output1280 .= $viewHelper1290->initializeArgumentsAndRender();

$output1280 .= '
			';
return $output1280;
};
$viewHelper1291 = $self->getViewHelper('$viewHelper1291', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper1291->setArguments($arguments1278);
$viewHelper1291->setRenderingContext($renderingContext);
$viewHelper1291->setRenderChildrenClosure($renderChildrenClosure1279);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output1261 .= $viewHelper1291->initializeArgumentsAndRender();

$output1261 .= '
		';
return $output1261;
};
$arguments1259['__thenClosure'] = function() use ($renderingContext, $self) {
$output1292 = '';

$output1292 .= '
				<h2>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1293 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1294 = array();
$arguments1294['id'] = 'media.collections';
$arguments1294['source'] = 'Modules';
$arguments1294['package'] = 'TYPO3.Neos';
$arguments1294['value'] = NULL;
$arguments1294['arguments'] = array (
);
$arguments1294['quantity'] = NULL;
$arguments1294['languageIdentifier'] = NULL;
$renderChildrenClosure1295 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1296 = $self->getViewHelper('$viewHelper1296', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1296->setArguments($arguments1294);
$viewHelper1296->setRenderingContext($renderingContext);
$viewHelper1296->setRenderChildrenClosure($renderChildrenClosure1295);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1293['value'] = $viewHelper1296->initializeArgumentsAndRender();
$arguments1293['keepQuotes'] = false;
$arguments1293['encoding'] = 'UTF-8';
$arguments1293['doubleEncode'] = true;
$renderChildrenClosure1297 = function() use ($renderingContext, $self) {
return NULL;
};
$value1298 = ($arguments1293['value'] !== NULL ? $arguments1293['value'] : $renderChildrenClosure1297());

$output1292 .= !is_string($value1298) && !(is_object($value1298) && method_exists($value1298, '__toString')) ? $value1298 : htmlspecialchars($value1298, ($arguments1293['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1293['encoding'], $arguments1293['doubleEncode']);

$output1292 .= '
					<span class="neos-media-aside-list-edit-toggle neos-button" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1299 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1300 = array();
$arguments1300['id'] = 'media.editCollections';
$arguments1300['source'] = 'Modules';
$arguments1300['package'] = 'TYPO3.Neos';
$arguments1300['value'] = NULL;
$arguments1300['arguments'] = array (
);
$arguments1300['quantity'] = NULL;
$arguments1300['languageIdentifier'] = NULL;
$renderChildrenClosure1301 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1302 = $self->getViewHelper('$viewHelper1302', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1302->setArguments($arguments1300);
$viewHelper1302->setRenderingContext($renderingContext);
$viewHelper1302->setRenderChildrenClosure($renderChildrenClosure1301);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1299['value'] = $viewHelper1302->initializeArgumentsAndRender();
$arguments1299['keepQuotes'] = false;
$arguments1299['encoding'] = 'UTF-8';
$arguments1299['doubleEncode'] = true;
$renderChildrenClosure1303 = function() use ($renderingContext, $self) {
return NULL;
};
$value1304 = ($arguments1299['value'] !== NULL ? $arguments1299['value'] : $renderChildrenClosure1303());

$output1292 .= !is_string($value1304) && !(is_object($value1304) && method_exists($value1304, '__toString')) ? $value1304 : htmlspecialchars($value1304, ($arguments1299['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1299['encoding'], $arguments1299['doubleEncode']);

$output1292 .= '" data-neos-toggle="tooltip"><i class="icon-pencil"></i></span>
				</h2>
			';
return $output1292;
};
$arguments1259['__elseClosure'] = function() use ($renderingContext, $self) {
$output1305 = '';

$output1305 .= '
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1306 = array();
// Rendering Boolean node
$arguments1306['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollections', $renderingContext));
$arguments1306['then'] = NULL;
$arguments1306['else'] = NULL;
$renderChildrenClosure1307 = function() use ($renderingContext, $self) {
$output1308 = '';

$output1308 .= '
					<h2>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1309 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1310 = array();
$arguments1310['id'] = 'media.collections';
$arguments1310['source'] = 'Modules';
$arguments1310['package'] = 'TYPO3.Neos';
$arguments1310['value'] = NULL;
$arguments1310['arguments'] = array (
);
$arguments1310['quantity'] = NULL;
$arguments1310['languageIdentifier'] = NULL;
$renderChildrenClosure1311 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1312 = $self->getViewHelper('$viewHelper1312', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1312->setArguments($arguments1310);
$viewHelper1312->setRenderingContext($renderingContext);
$viewHelper1312->setRenderChildrenClosure($renderChildrenClosure1311);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1309['value'] = $viewHelper1312->initializeArgumentsAndRender();
$arguments1309['keepQuotes'] = false;
$arguments1309['encoding'] = 'UTF-8';
$arguments1309['doubleEncode'] = true;
$renderChildrenClosure1313 = function() use ($renderingContext, $self) {
return NULL;
};
$value1314 = ($arguments1309['value'] !== NULL ? $arguments1309['value'] : $renderChildrenClosure1313());

$output1308 .= !is_string($value1314) && !(is_object($value1314) && method_exists($value1314, '__toString')) ? $value1314 : htmlspecialchars($value1314, ($arguments1309['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1309['encoding'], $arguments1309['doubleEncode']);

$output1308 .= '</h2>
				';
return $output1308;
};
$viewHelper1315 = $self->getViewHelper('$viewHelper1315', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1315->setArguments($arguments1306);
$viewHelper1315->setRenderingContext($renderingContext);
$viewHelper1315->setRenderChildrenClosure($renderChildrenClosure1307);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output1305 .= $viewHelper1315->initializeArgumentsAndRender();

$output1305 .= '
			';
return $output1305;
};
$viewHelper1316 = $self->getViewHelper('$viewHelper1316', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Security\IfAccessViewHelper');
$viewHelper1316->setArguments($arguments1259);
$viewHelper1316->setRenderingContext($renderingContext);
$viewHelper1316->setRenderChildrenClosure($renderChildrenClosure1260);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Security\IfAccessViewHelper

$output1224 .= $viewHelper1316->initializeArgumentsAndRender();

$output1224 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1317 = array();
// Rendering Boolean node
$arguments1317['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollections', $renderingContext));
$arguments1317['then'] = NULL;
$arguments1317['else'] = NULL;
$renderChildrenClosure1318 = function() use ($renderingContext, $self) {
$output1319 = '';

$output1319 .= '
			<ul class="neos-media-aside-list">
				<li>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments1320 = array();
$arguments1320['action'] = 'index';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1321 = array();
// Rendering Boolean node
$arguments1321['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'activeAssetCollection', $renderingContext));
$arguments1321['else'] = ' neos-active';
$arguments1321['then'] = NULL;
$renderChildrenClosure1322 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1323 = $self->getViewHelper('$viewHelper1323', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1323->setArguments($arguments1321);
$viewHelper1323->setRenderingContext($renderingContext);
$viewHelper1323->setRenderChildrenClosure($renderChildrenClosure1322);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1320['class'] = $viewHelper1323->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1324 = array();
$arguments1324['id'] = 'media.allCollections';
$arguments1324['source'] = 'Modules';
$arguments1324['package'] = 'TYPO3.Neos';
$arguments1324['value'] = NULL;
$arguments1324['arguments'] = array (
);
$arguments1324['quantity'] = NULL;
$arguments1324['languageIdentifier'] = NULL;
$renderChildrenClosure1325 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1326 = $self->getViewHelper('$viewHelper1326', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1326->setArguments($arguments1324);
$viewHelper1326->setRenderingContext($renderingContext);
$viewHelper1326->setRenderChildrenClosure($renderChildrenClosure1325);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1320['title'] = $viewHelper1326->initializeArgumentsAndRender();
// Rendering Array
$array1327 = array();
$array1327['view'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'view', $renderingContext);
$array1327['collectionMode'] = 1;
$arguments1320['arguments'] = $array1327;
$arguments1320['additionalAttributes'] = NULL;
$arguments1320['data'] = NULL;
$arguments1320['controller'] = NULL;
$arguments1320['package'] = NULL;
$arguments1320['subpackage'] = NULL;
$arguments1320['section'] = '';
$arguments1320['format'] = '';
$arguments1320['additionalParams'] = array (
);
$arguments1320['addQueryString'] = false;
$arguments1320['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1320['useParentRequest'] = false;
$arguments1320['absolute'] = true;
$arguments1320['dir'] = NULL;
$arguments1320['id'] = NULL;
$arguments1320['lang'] = NULL;
$arguments1320['style'] = NULL;
$arguments1320['accesskey'] = NULL;
$arguments1320['tabindex'] = NULL;
$arguments1320['onclick'] = NULL;
$arguments1320['name'] = NULL;
$arguments1320['rel'] = NULL;
$arguments1320['rev'] = NULL;
$arguments1320['target'] = NULL;
$renderChildrenClosure1328 = function() use ($renderingContext, $self) {
$output1329 = '';

$output1329 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1330 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1331 = array();
$arguments1331['id'] = 'media.filter.all';
$arguments1331['source'] = 'Modules';
$arguments1331['package'] = 'TYPO3.Neos';
$arguments1331['value'] = NULL;
$arguments1331['arguments'] = array (
);
$arguments1331['quantity'] = NULL;
$arguments1331['languageIdentifier'] = NULL;
$renderChildrenClosure1332 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1333 = $self->getViewHelper('$viewHelper1333', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1333->setArguments($arguments1331);
$viewHelper1333->setRenderingContext($renderingContext);
$viewHelper1333->setRenderChildrenClosure($renderChildrenClosure1332);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1330['value'] = $viewHelper1333->initializeArgumentsAndRender();
$arguments1330['keepQuotes'] = false;
$arguments1330['encoding'] = 'UTF-8';
$arguments1330['doubleEncode'] = true;
$renderChildrenClosure1334 = function() use ($renderingContext, $self) {
return NULL;
};
$value1335 = ($arguments1330['value'] !== NULL ? $arguments1330['value'] : $renderChildrenClosure1334());

$output1329 .= !is_string($value1335) && !(is_object($value1335) && method_exists($value1335, '__toString')) ? $value1335 : htmlspecialchars($value1335, ($arguments1330['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1330['encoding'], $arguments1330['doubleEncode']);

$output1329 .= '
						<span class="count">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1336 = array();
$arguments1336['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'allCollectionsCount', $renderingContext);
$arguments1336['keepQuotes'] = false;
$arguments1336['encoding'] = 'UTF-8';
$arguments1336['doubleEncode'] = true;
$renderChildrenClosure1337 = function() use ($renderingContext, $self) {
return NULL;
};
$value1338 = ($arguments1336['value'] !== NULL ? $arguments1336['value'] : $renderChildrenClosure1337());

$output1329 .= !is_string($value1338) && !(is_object($value1338) && method_exists($value1338, '__toString')) ? $value1338 : htmlspecialchars($value1338, ($arguments1336['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1336['encoding'], $arguments1336['doubleEncode']);

$output1329 .= '</span>
					';
return $output1329;
};
$viewHelper1339 = $self->getViewHelper('$viewHelper1339', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper1339->setArguments($arguments1320);
$viewHelper1339->setRenderingContext($renderingContext);
$viewHelper1339->setRenderChildrenClosure($renderChildrenClosure1328);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output1319 .= $viewHelper1339->initializeArgumentsAndRender();

$output1319 .= '
				</li>
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments1340 = array();
$arguments1340['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollections', $renderingContext);
$arguments1340['as'] = 'assetCollection';
$arguments1340['key'] = '';
$arguments1340['reverse'] = false;
$arguments1340['iteration'] = NULL;
$renderChildrenClosure1341 = function() use ($renderingContext, $self) {
$output1342 = '';

$output1342 .= '
					<li>
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments1343 = array();
$arguments1343['action'] = 'index';
$arguments1343['title'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollection.object.title', $renderingContext);
$output1344 = '';

$output1344 .= 'droppable-assetcollection';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1345 = array();
// Rendering Boolean node
$arguments1345['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollection.object', $renderingContext), \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'activeAssetCollection', $renderingContext));
$arguments1345['then'] = ' neos-active';
$arguments1345['else'] = NULL;
$renderChildrenClosure1346 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1347 = $self->getViewHelper('$viewHelper1347', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1347->setArguments($arguments1345);
$viewHelper1347->setRenderingContext($renderingContext);
$viewHelper1347->setRenderChildrenClosure($renderChildrenClosure1346);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output1344 .= $viewHelper1347->initializeArgumentsAndRender();
$arguments1343['class'] = $output1344;
// Rendering Array
$array1348 = array();
$array1348['view'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'view', $renderingContext);
$array1348['assetCollection'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollection.object', $renderingContext);
$arguments1343['arguments'] = $array1348;
// Rendering Array
$array1349 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper
$arguments1350 = array();
$arguments1350['value'] = NULL;
$renderChildrenClosure1351 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollection.object', $renderingContext);
};
$viewHelper1352 = $self->getViewHelper('$viewHelper1352', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper');
$viewHelper1352->setArguments($arguments1350);
$viewHelper1352->setRenderingContext($renderingContext);
$viewHelper1352->setRenderChildrenClosure($renderChildrenClosure1351);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper
$array1349['assetcollection-identifier'] = $viewHelper1352->initializeArgumentsAndRender();
$arguments1343['data'] = $array1349;
$arguments1343['additionalAttributes'] = NULL;
$arguments1343['controller'] = NULL;
$arguments1343['package'] = NULL;
$arguments1343['subpackage'] = NULL;
$arguments1343['section'] = '';
$arguments1343['format'] = '';
$arguments1343['additionalParams'] = array (
);
$arguments1343['addQueryString'] = false;
$arguments1343['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1343['useParentRequest'] = false;
$arguments1343['absolute'] = true;
$arguments1343['dir'] = NULL;
$arguments1343['id'] = NULL;
$arguments1343['lang'] = NULL;
$arguments1343['style'] = NULL;
$arguments1343['accesskey'] = NULL;
$arguments1343['tabindex'] = NULL;
$arguments1343['onclick'] = NULL;
$arguments1343['name'] = NULL;
$arguments1343['rel'] = NULL;
$arguments1343['rev'] = NULL;
$arguments1343['target'] = NULL;
$renderChildrenClosure1353 = function() use ($renderingContext, $self) {
$output1354 = '';

$output1354 .= '
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1355 = array();
$arguments1355['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollection.object.title', $renderingContext);
$arguments1355['keepQuotes'] = false;
$arguments1355['encoding'] = 'UTF-8';
$arguments1355['doubleEncode'] = true;
$renderChildrenClosure1356 = function() use ($renderingContext, $self) {
return NULL;
};
$value1357 = ($arguments1355['value'] !== NULL ? $arguments1355['value'] : $renderChildrenClosure1356());

$output1354 .= !is_string($value1357) && !(is_object($value1357) && method_exists($value1357, '__toString')) ? $value1357 : htmlspecialchars($value1357, ($arguments1355['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1355['encoding'], $arguments1355['doubleEncode']);

$output1354 .= '
							<span class="count">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1358 = array();
$arguments1358['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollection.count', $renderingContext);
$arguments1358['keepQuotes'] = false;
$arguments1358['encoding'] = 'UTF-8';
$arguments1358['doubleEncode'] = true;
$renderChildrenClosure1359 = function() use ($renderingContext, $self) {
return NULL;
};
$value1360 = ($arguments1358['value'] !== NULL ? $arguments1358['value'] : $renderChildrenClosure1359());

$output1354 .= !is_string($value1360) && !(is_object($value1360) && method_exists($value1360, '__toString')) ? $value1360 : htmlspecialchars($value1360, ($arguments1358['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1358['encoding'], $arguments1358['doubleEncode']);

$output1354 .= '</span>
						';
return $output1354;
};
$viewHelper1361 = $self->getViewHelper('$viewHelper1361', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper1361->setArguments($arguments1343);
$viewHelper1361->setRenderingContext($renderingContext);
$viewHelper1361->setRenderChildrenClosure($renderChildrenClosure1353);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output1342 .= $viewHelper1361->initializeArgumentsAndRender();

$output1342 .= '
						<div class="neos-sidelist-edit-actions">
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments1362 = array();
$arguments1362['class'] = 'neos-button';
$arguments1362['action'] = 'editAssetCollection';
// Rendering Array
$array1363 = array();
$array1363['assetCollection'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollection.object', $renderingContext);
$arguments1362['arguments'] = $array1363;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1364 = array();
$arguments1364['id'] = 'media.editCollection';
$arguments1364['source'] = 'Modules';
$arguments1364['package'] = 'TYPO3.Neos';
$arguments1364['value'] = NULL;
$arguments1364['arguments'] = array (
);
$arguments1364['quantity'] = NULL;
$arguments1364['languageIdentifier'] = NULL;
$renderChildrenClosure1365 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1366 = $self->getViewHelper('$viewHelper1366', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1366->setArguments($arguments1364);
$viewHelper1366->setRenderingContext($renderingContext);
$viewHelper1366->setRenderChildrenClosure($renderChildrenClosure1365);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1362['title'] = $viewHelper1366->initializeArgumentsAndRender();
// Rendering Array
$array1367 = array();
$array1367['neos-toggle'] = 'tooltip';
$arguments1362['data'] = $array1367;
$arguments1362['additionalAttributes'] = NULL;
$arguments1362['controller'] = NULL;
$arguments1362['package'] = NULL;
$arguments1362['subpackage'] = NULL;
$arguments1362['section'] = '';
$arguments1362['format'] = '';
$arguments1362['additionalParams'] = array (
);
$arguments1362['addQueryString'] = false;
$arguments1362['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1362['useParentRequest'] = false;
$arguments1362['absolute'] = true;
$arguments1362['dir'] = NULL;
$arguments1362['id'] = NULL;
$arguments1362['lang'] = NULL;
$arguments1362['style'] = NULL;
$arguments1362['accesskey'] = NULL;
$arguments1362['tabindex'] = NULL;
$arguments1362['onclick'] = NULL;
$arguments1362['name'] = NULL;
$arguments1362['rel'] = NULL;
$arguments1362['rev'] = NULL;
$arguments1362['target'] = NULL;
$renderChildrenClosure1368 = function() use ($renderingContext, $self) {
return '<i class="icon-pencil"></i>';
};
$viewHelper1369 = $self->getViewHelper('$viewHelper1369', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper1369->setArguments($arguments1362);
$viewHelper1369->setRenderingContext($renderingContext);
$viewHelper1369->setRenderChildrenClosure($renderChildrenClosure1368);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output1342 .= $viewHelper1369->initializeArgumentsAndRender();

$output1342 .= '
							<button type="submit" class="neos-button-danger" data-modal="delete-assetcollection-modal" data-object-identifier="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1370 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper
$arguments1371 = array();
$arguments1371['value'] = NULL;
$renderChildrenClosure1372 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollection.object', $renderingContext);
};
$viewHelper1373 = $self->getViewHelper('$viewHelper1373', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper');
$viewHelper1373->setArguments($arguments1371);
$viewHelper1373->setRenderingContext($renderingContext);
$viewHelper1373->setRenderChildrenClosure($renderChildrenClosure1372);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper
$arguments1370['value'] = $viewHelper1373->initializeArgumentsAndRender();
$arguments1370['keepQuotes'] = false;
$arguments1370['encoding'] = 'UTF-8';
$arguments1370['doubleEncode'] = true;
$renderChildrenClosure1374 = function() use ($renderingContext, $self) {
return NULL;
};
$value1375 = ($arguments1370['value'] !== NULL ? $arguments1370['value'] : $renderChildrenClosure1374());

$output1342 .= !is_string($value1375) && !(is_object($value1375) && method_exists($value1375, '__toString')) ? $value1375 : htmlspecialchars($value1375, ($arguments1370['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1370['encoding'], $arguments1370['doubleEncode']);

$output1342 .= '" data-label="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1376 = array();
$arguments1376['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assetCollection.object.title', $renderingContext);
$arguments1376['keepQuotes'] = false;
$arguments1376['encoding'] = 'UTF-8';
$arguments1376['doubleEncode'] = true;
$renderChildrenClosure1377 = function() use ($renderingContext, $self) {
return NULL;
};
$value1378 = ($arguments1376['value'] !== NULL ? $arguments1376['value'] : $renderChildrenClosure1377());

$output1342 .= !is_string($value1378) && !(is_object($value1378) && method_exists($value1378, '__toString')) ? $value1378 : htmlspecialchars($value1378, ($arguments1376['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1376['encoding'], $arguments1376['doubleEncode']);

$output1342 .= '" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1379 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1380 = array();
$arguments1380['id'] = 'media.deleteCollection';
$arguments1380['source'] = 'Modules';
$arguments1380['package'] = 'TYPO3.Neos';
$arguments1380['value'] = NULL;
$arguments1380['arguments'] = array (
);
$arguments1380['quantity'] = NULL;
$arguments1380['languageIdentifier'] = NULL;
$renderChildrenClosure1381 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1382 = $self->getViewHelper('$viewHelper1382', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1382->setArguments($arguments1380);
$viewHelper1382->setRenderingContext($renderingContext);
$viewHelper1382->setRenderChildrenClosure($renderChildrenClosure1381);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1379['value'] = $viewHelper1382->initializeArgumentsAndRender();
$arguments1379['keepQuotes'] = false;
$arguments1379['encoding'] = 'UTF-8';
$arguments1379['doubleEncode'] = true;
$renderChildrenClosure1383 = function() use ($renderingContext, $self) {
return NULL;
};
$value1384 = ($arguments1379['value'] !== NULL ? $arguments1379['value'] : $renderChildrenClosure1383());

$output1342 .= !is_string($value1384) && !(is_object($value1384) && method_exists($value1384, '__toString')) ? $value1384 : htmlspecialchars($value1384, ($arguments1379['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1379['encoding'], $arguments1379['doubleEncode']);

$output1342 .= '" data-neos-toggle="tooltip"><i class="icon-trash"></i></button>
						</div>
					</li>
				';
return $output1342;
};

$output1319 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments1340, $renderChildrenClosure1341, $renderingContext);

$output1319 .= '
			</ul>
			<div class="neos-hide" id="delete-assetcollection-modal">
				<div class="neos-modal-centered">
					<div class="neos-modal-content">
						<div class="neos-modal-header">
							<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
							<div class="neos-header">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1385 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1386 = array();
$arguments1386['id'] = 'media.message.reallyDeleteCollection';
$arguments1386['source'] = 'Modules';
$arguments1386['package'] = 'TYPO3.Neos';
$arguments1386['value'] = NULL;
$arguments1386['arguments'] = array (
);
$arguments1386['quantity'] = NULL;
$arguments1386['languageIdentifier'] = NULL;
$renderChildrenClosure1387 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1388 = $self->getViewHelper('$viewHelper1388', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1388->setArguments($arguments1386);
$viewHelper1388->setRenderingContext($renderingContext);
$viewHelper1388->setRenderChildrenClosure($renderChildrenClosure1387);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1385['value'] = $viewHelper1388->initializeArgumentsAndRender();
$arguments1385['keepQuotes'] = false;
$arguments1385['encoding'] = 'UTF-8';
$arguments1385['doubleEncode'] = true;
$renderChildrenClosure1389 = function() use ($renderingContext, $self) {
return NULL;
};
$value1390 = ($arguments1385['value'] !== NULL ? $arguments1385['value'] : $renderChildrenClosure1389());

$output1319 .= !is_string($value1390) && !(is_object($value1390) && method_exists($value1390, '__toString')) ? $value1390 : htmlspecialchars($value1390, ($arguments1385['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1385['encoding'], $arguments1385['doubleEncode']);

$output1319 .= '</div>
							<div>
								<div class="neos-subheader">
									<p>
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1391 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1392 = array();
$arguments1392['id'] = 'media.message.willDeleteCollection';
$arguments1392['source'] = 'Modules';
$arguments1392['package'] = 'TYPO3.Neos';
$arguments1392['value'] = NULL;
$arguments1392['arguments'] = array (
);
$arguments1392['quantity'] = NULL;
$arguments1392['languageIdentifier'] = NULL;
$renderChildrenClosure1393 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1394 = $self->getViewHelper('$viewHelper1394', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1394->setArguments($arguments1392);
$viewHelper1394->setRenderingContext($renderingContext);
$viewHelper1394->setRenderChildrenClosure($renderChildrenClosure1393);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1391['value'] = $viewHelper1394->initializeArgumentsAndRender();
$arguments1391['keepQuotes'] = false;
$arguments1391['encoding'] = 'UTF-8';
$arguments1391['doubleEncode'] = true;
$renderChildrenClosure1395 = function() use ($renderingContext, $self) {
return NULL;
};
$value1396 = ($arguments1391['value'] !== NULL ? $arguments1391['value'] : $renderChildrenClosure1395());

$output1319 .= !is_string($value1396) && !(is_object($value1396) && method_exists($value1396, '__toString')) ? $value1396 : htmlspecialchars($value1396, ($arguments1391['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1391['encoding'], $arguments1391['doubleEncode']);

$output1319 .= '<br />
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1397 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1398 = array();
$arguments1398['id'] = 'media.message.operationCannotBeUndone';
$arguments1398['source'] = 'Modules';
$arguments1398['package'] = 'TYPO3.Neos';
$arguments1398['value'] = NULL;
$arguments1398['arguments'] = array (
);
$arguments1398['quantity'] = NULL;
$arguments1398['languageIdentifier'] = NULL;
$renderChildrenClosure1399 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1400 = $self->getViewHelper('$viewHelper1400', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1400->setArguments($arguments1398);
$viewHelper1400->setRenderingContext($renderingContext);
$viewHelper1400->setRenderChildrenClosure($renderChildrenClosure1399);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1397['value'] = $viewHelper1400->initializeArgumentsAndRender();
$arguments1397['keepQuotes'] = false;
$arguments1397['encoding'] = 'UTF-8';
$arguments1397['doubleEncode'] = true;
$renderChildrenClosure1401 = function() use ($renderingContext, $self) {
return NULL;
};
$value1402 = ($arguments1397['value'] !== NULL ? $arguments1397['value'] : $renderChildrenClosure1401());

$output1319 .= !is_string($value1402) && !(is_object($value1402) && method_exists($value1402, '__toString')) ? $value1402 : htmlspecialchars($value1402, ($arguments1397['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1397['encoding'], $arguments1397['doubleEncode']);

$output1319 .= '
									</p>
								</div>
							</div>
						</div>
						<div class="neos-modal-footer">
							<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1403 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1404 = array();
$arguments1404['id'] = 'media.cancel';
$arguments1404['source'] = 'Modules';
$arguments1404['package'] = 'TYPO3.Neos';
$arguments1404['value'] = NULL;
$arguments1404['arguments'] = array (
);
$arguments1404['quantity'] = NULL;
$arguments1404['languageIdentifier'] = NULL;
$renderChildrenClosure1405 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1406 = $self->getViewHelper('$viewHelper1406', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1406->setArguments($arguments1404);
$viewHelper1406->setRenderingContext($renderingContext);
$viewHelper1406->setRenderChildrenClosure($renderChildrenClosure1405);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1403['value'] = $viewHelper1406->initializeArgumentsAndRender();
$arguments1403['keepQuotes'] = false;
$arguments1403['encoding'] = 'UTF-8';
$arguments1403['doubleEncode'] = true;
$renderChildrenClosure1407 = function() use ($renderingContext, $self) {
return NULL;
};
$value1408 = ($arguments1403['value'] !== NULL ? $arguments1403['value'] : $renderChildrenClosure1407());

$output1319 .= !is_string($value1408) && !(is_object($value1408) && method_exists($value1408, '__toString')) ? $value1408 : htmlspecialchars($value1408, ($arguments1403['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1403['encoding'], $arguments1403['doubleEncode']);

$output1319 .= '</a>
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments1409 = array();
$arguments1409['action'] = 'deleteAssetCollection';
$arguments1409['class'] = 'neos-inline';
$arguments1409['additionalAttributes'] = NULL;
$arguments1409['data'] = NULL;
$arguments1409['arguments'] = array (
);
$arguments1409['controller'] = NULL;
$arguments1409['package'] = NULL;
$arguments1409['subpackage'] = NULL;
$arguments1409['object'] = NULL;
$arguments1409['section'] = '';
$arguments1409['format'] = '';
$arguments1409['additionalParams'] = array (
);
$arguments1409['absolute'] = false;
$arguments1409['addQueryString'] = false;
$arguments1409['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1409['fieldNamePrefix'] = NULL;
$arguments1409['actionUri'] = NULL;
$arguments1409['objectName'] = NULL;
$arguments1409['useParentRequest'] = false;
$arguments1409['enctype'] = NULL;
$arguments1409['method'] = NULL;
$arguments1409['name'] = NULL;
$arguments1409['onreset'] = NULL;
$arguments1409['onsubmit'] = NULL;
$arguments1409['dir'] = NULL;
$arguments1409['id'] = NULL;
$arguments1409['lang'] = NULL;
$arguments1409['style'] = NULL;
$arguments1409['title'] = NULL;
$arguments1409['accesskey'] = NULL;
$arguments1409['tabindex'] = NULL;
$arguments1409['onclick'] = NULL;
$renderChildrenClosure1410 = function() use ($renderingContext, $self) {
$output1411 = '';

$output1411 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments1412 = array();
$arguments1412['name'] = 'assetCollection';
$arguments1412['id'] = 'modal-form-object';
$arguments1412['additionalAttributes'] = NULL;
$arguments1412['data'] = NULL;
$arguments1412['value'] = NULL;
$arguments1412['property'] = NULL;
$arguments1412['class'] = NULL;
$arguments1412['dir'] = NULL;
$arguments1412['lang'] = NULL;
$arguments1412['style'] = NULL;
$arguments1412['title'] = NULL;
$arguments1412['accesskey'] = NULL;
$arguments1412['tabindex'] = NULL;
$arguments1412['onclick'] = NULL;
$renderChildrenClosure1413 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1414 = $self->getViewHelper('$viewHelper1414', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper1414->setArguments($arguments1412);
$viewHelper1414->setRenderingContext($renderingContext);
$viewHelper1414->setRenderChildrenClosure($renderChildrenClosure1413);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output1411 .= $viewHelper1414->initializeArgumentsAndRender();

$output1411 .= '
								<button type="submit" class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1415 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1416 = array();
$arguments1416['id'] = 'media.deleteCollection';
$arguments1416['source'] = 'Modules';
$arguments1416['package'] = 'TYPO3.Neos';
$arguments1416['value'] = NULL;
$arguments1416['arguments'] = array (
);
$arguments1416['quantity'] = NULL;
$arguments1416['languageIdentifier'] = NULL;
$renderChildrenClosure1417 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1418 = $self->getViewHelper('$viewHelper1418', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1418->setArguments($arguments1416);
$viewHelper1418->setRenderingContext($renderingContext);
$viewHelper1418->setRenderChildrenClosure($renderChildrenClosure1417);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1415['value'] = $viewHelper1418->initializeArgumentsAndRender();
$arguments1415['keepQuotes'] = false;
$arguments1415['encoding'] = 'UTF-8';
$arguments1415['doubleEncode'] = true;
$renderChildrenClosure1419 = function() use ($renderingContext, $self) {
return NULL;
};
$value1420 = ($arguments1415['value'] !== NULL ? $arguments1415['value'] : $renderChildrenClosure1419());

$output1411 .= !is_string($value1420) && !(is_object($value1420) && method_exists($value1420, '__toString')) ? $value1420 : htmlspecialchars($value1420, ($arguments1415['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1415['encoding'], $arguments1415['doubleEncode']);

$output1411 .= '">
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1421 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1422 = array();
$arguments1422['id'] = 'media.message.confirmDeleteCollection';
$arguments1422['source'] = 'Modules';
$arguments1422['package'] = 'TYPO3.Neos';
$arguments1422['value'] = NULL;
$arguments1422['arguments'] = array (
);
$arguments1422['quantity'] = NULL;
$arguments1422['languageIdentifier'] = NULL;
$renderChildrenClosure1423 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1424 = $self->getViewHelper('$viewHelper1424', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1424->setArguments($arguments1422);
$viewHelper1424->setRenderingContext($renderingContext);
$viewHelper1424->setRenderChildrenClosure($renderChildrenClosure1423);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1421['value'] = $viewHelper1424->initializeArgumentsAndRender();
$arguments1421['keepQuotes'] = false;
$arguments1421['encoding'] = 'UTF-8';
$arguments1421['doubleEncode'] = true;
$renderChildrenClosure1425 = function() use ($renderingContext, $self) {
return NULL;
};
$value1426 = ($arguments1421['value'] !== NULL ? $arguments1421['value'] : $renderChildrenClosure1425());

$output1411 .= !is_string($value1426) && !(is_object($value1426) && method_exists($value1426, '__toString')) ? $value1426 : htmlspecialchars($value1426, ($arguments1421['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1421['encoding'], $arguments1421['doubleEncode']);

$output1411 .= '
								</button>
							';
return $output1411;
};
$viewHelper1427 = $self->getViewHelper('$viewHelper1427', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper1427->setArguments($arguments1409);
$viewHelper1427->setRenderingContext($renderingContext);
$viewHelper1427->setRenderChildrenClosure($renderChildrenClosure1410);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output1319 .= $viewHelper1427->initializeArgumentsAndRender();

$output1319 .= '
						</div>
					</div>
				</div>
				<div class="neos-modal-backdrop neos-in"></div>
			</div>
		';
return $output1319;
};
$viewHelper1428 = $self->getViewHelper('$viewHelper1428', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1428->setArguments($arguments1317);
$viewHelper1428->setRenderingContext($renderingContext);
$viewHelper1428->setRenderChildrenClosure($renderChildrenClosure1318);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output1224 .= $viewHelper1428->initializeArgumentsAndRender();

$output1224 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Security\IfAccessViewHelper
$arguments1429 = array();
$arguments1429['privilegeTarget'] = 'TYPO3.Media:ManageAssetCollections';
$arguments1429['then'] = NULL;
$arguments1429['else'] = NULL;
$arguments1429['parameters'] = array (
);
$renderChildrenClosure1430 = function() use ($renderingContext, $self) {
$output1431 = '';

$output1431 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments1432 = array();
$arguments1432['action'] = 'createAssetCollection';
$arguments1432['id'] = 'neos-assetcollections-create-form';
$arguments1432['additionalAttributes'] = NULL;
$arguments1432['data'] = NULL;
$arguments1432['arguments'] = array (
);
$arguments1432['controller'] = NULL;
$arguments1432['package'] = NULL;
$arguments1432['subpackage'] = NULL;
$arguments1432['object'] = NULL;
$arguments1432['section'] = '';
$arguments1432['format'] = '';
$arguments1432['additionalParams'] = array (
);
$arguments1432['absolute'] = false;
$arguments1432['addQueryString'] = false;
$arguments1432['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1432['fieldNamePrefix'] = NULL;
$arguments1432['actionUri'] = NULL;
$arguments1432['objectName'] = NULL;
$arguments1432['useParentRequest'] = false;
$arguments1432['enctype'] = NULL;
$arguments1432['method'] = NULL;
$arguments1432['name'] = NULL;
$arguments1432['onreset'] = NULL;
$arguments1432['onsubmit'] = NULL;
$arguments1432['class'] = NULL;
$arguments1432['dir'] = NULL;
$arguments1432['lang'] = NULL;
$arguments1432['style'] = NULL;
$arguments1432['title'] = NULL;
$arguments1432['accesskey'] = NULL;
$arguments1432['tabindex'] = NULL;
$arguments1432['onclick'] = NULL;
$renderChildrenClosure1433 = function() use ($renderingContext, $self) {
$output1434 = '';

$output1434 .= '
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments1435 = array();
$arguments1435['name'] = 'title';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1436 = array();
$arguments1436['id'] = 'media.newCollection.placeholder';
$arguments1436['source'] = 'Modules';
$arguments1436['package'] = 'TYPO3.Neos';
$arguments1436['value'] = NULL;
$arguments1436['arguments'] = array (
);
$arguments1436['quantity'] = NULL;
$arguments1436['languageIdentifier'] = NULL;
$renderChildrenClosure1437 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1438 = $self->getViewHelper('$viewHelper1438', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1438->setArguments($arguments1436);
$viewHelper1438->setRenderingContext($renderingContext);
$viewHelper1438->setRenderChildrenClosure($renderChildrenClosure1437);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1435['placeholder'] = $viewHelper1438->initializeArgumentsAndRender();
$arguments1435['additionalAttributes'] = NULL;
$arguments1435['data'] = NULL;
$arguments1435['required'] = false;
$arguments1435['type'] = 'text';
$arguments1435['value'] = NULL;
$arguments1435['property'] = NULL;
$arguments1435['disabled'] = NULL;
$arguments1435['maxlength'] = NULL;
$arguments1435['readonly'] = NULL;
$arguments1435['size'] = NULL;
$arguments1435['autofocus'] = NULL;
$arguments1435['errorClass'] = 'f3-form-error';
$arguments1435['class'] = NULL;
$arguments1435['dir'] = NULL;
$arguments1435['id'] = NULL;
$arguments1435['lang'] = NULL;
$arguments1435['style'] = NULL;
$arguments1435['title'] = NULL;
$arguments1435['accesskey'] = NULL;
$arguments1435['tabindex'] = NULL;
$arguments1435['onclick'] = NULL;
$renderChildrenClosure1439 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1440 = $self->getViewHelper('$viewHelper1440', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper1440->setArguments($arguments1435);
$viewHelper1440->setRenderingContext($renderingContext);
$viewHelper1440->setRenderChildrenClosure($renderChildrenClosure1439);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output1434 .= $viewHelper1440->initializeArgumentsAndRender();

$output1434 .= '<br /><br />
				<button type="submit" class="neos-button neos-button-primary">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1441 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1442 = array();
$arguments1442['id'] = 'media.createCollection';
$arguments1442['source'] = 'Modules';
$arguments1442['package'] = 'TYPO3.Neos';
$arguments1442['value'] = NULL;
$arguments1442['arguments'] = array (
);
$arguments1442['quantity'] = NULL;
$arguments1442['languageIdentifier'] = NULL;
$renderChildrenClosure1443 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1444 = $self->getViewHelper('$viewHelper1444', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1444->setArguments($arguments1442);
$viewHelper1444->setRenderingContext($renderingContext);
$viewHelper1444->setRenderChildrenClosure($renderChildrenClosure1443);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1441['value'] = $viewHelper1444->initializeArgumentsAndRender();
$arguments1441['keepQuotes'] = false;
$arguments1441['encoding'] = 'UTF-8';
$arguments1441['doubleEncode'] = true;
$renderChildrenClosure1445 = function() use ($renderingContext, $self) {
return NULL;
};
$value1446 = ($arguments1441['value'] !== NULL ? $arguments1441['value'] : $renderChildrenClosure1445());

$output1434 .= !is_string($value1446) && !(is_object($value1446) && method_exists($value1446, '__toString')) ? $value1446 : htmlspecialchars($value1446, ($arguments1441['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1441['encoding'], $arguments1441['doubleEncode']);

$output1434 .= '</button>
			';
return $output1434;
};
$viewHelper1447 = $self->getViewHelper('$viewHelper1447', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper1447->setArguments($arguments1432);
$viewHelper1447->setRenderingContext($renderingContext);
$viewHelper1447->setRenderChildrenClosure($renderChildrenClosure1433);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output1431 .= $viewHelper1447->initializeArgumentsAndRender();

$output1431 .= '
		';
return $output1431;
};
$viewHelper1448 = $self->getViewHelper('$viewHelper1448', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Security\IfAccessViewHelper');
$viewHelper1448->setArguments($arguments1429);
$viewHelper1448->setRenderingContext($renderingContext);
$viewHelper1448->setRenderChildrenClosure($renderChildrenClosure1430);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Security\IfAccessViewHelper

$output1224 .= $viewHelper1448->initializeArgumentsAndRender();

$output1224 .= '
	</div>

	<div class="neos-media-aside-group">
		<h2>
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1449 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1450 = array();
$arguments1450['id'] = 'media.tags';
$arguments1450['source'] = 'Modules';
$arguments1450['package'] = 'TYPO3.Neos';
$arguments1450['value'] = NULL;
$arguments1450['arguments'] = array (
);
$arguments1450['quantity'] = NULL;
$arguments1450['languageIdentifier'] = NULL;
$renderChildrenClosure1451 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1452 = $self->getViewHelper('$viewHelper1452', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1452->setArguments($arguments1450);
$viewHelper1452->setRenderingContext($renderingContext);
$viewHelper1452->setRenderChildrenClosure($renderChildrenClosure1451);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1449['value'] = $viewHelper1452->initializeArgumentsAndRender();
$arguments1449['keepQuotes'] = false;
$arguments1449['encoding'] = 'UTF-8';
$arguments1449['doubleEncode'] = true;
$renderChildrenClosure1453 = function() use ($renderingContext, $self) {
return NULL;
};
$value1454 = ($arguments1449['value'] !== NULL ? $arguments1449['value'] : $renderChildrenClosure1453());

$output1224 .= !is_string($value1454) && !(is_object($value1454) && method_exists($value1454, '__toString')) ? $value1454 : htmlspecialchars($value1454, ($arguments1449['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1449['encoding'], $arguments1449['doubleEncode']);

$output1224 .= '
			<span class="neos-media-aside-list-edit-toggle neos-button" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1455 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1456 = array();
$arguments1456['id'] = 'media.editTags';
$arguments1456['source'] = 'Modules';
$arguments1456['package'] = 'TYPO3.Neos';
$arguments1456['value'] = NULL;
$arguments1456['arguments'] = array (
);
$arguments1456['quantity'] = NULL;
$arguments1456['languageIdentifier'] = NULL;
$renderChildrenClosure1457 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1458 = $self->getViewHelper('$viewHelper1458', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1458->setArguments($arguments1456);
$viewHelper1458->setRenderingContext($renderingContext);
$viewHelper1458->setRenderChildrenClosure($renderChildrenClosure1457);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1455['value'] = $viewHelper1458->initializeArgumentsAndRender();
$arguments1455['keepQuotes'] = false;
$arguments1455['encoding'] = 'UTF-8';
$arguments1455['doubleEncode'] = true;
$renderChildrenClosure1459 = function() use ($renderingContext, $self) {
return NULL;
};
$value1460 = ($arguments1455['value'] !== NULL ? $arguments1455['value'] : $renderChildrenClosure1459());

$output1224 .= !is_string($value1460) && !(is_object($value1460) && method_exists($value1460, '__toString')) ? $value1460 : htmlspecialchars($value1460, ($arguments1455['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1455['encoding'], $arguments1455['doubleEncode']);

$output1224 .= '" data-neos-toggle="tooltip"><i class="icon-pencil"></i></span>
		</h2>
		<ul class="neos-media-aside-list">
			<li class="neos-media-list-all">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments1461 = array();
$arguments1461['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1462 = array();
$arguments1462['id'] = 'media.tags.title.all';
$arguments1462['source'] = 'Modules';
$arguments1462['package'] = 'TYPO3.Neos';
$arguments1462['value'] = NULL;
$arguments1462['arguments'] = array (
);
$arguments1462['quantity'] = NULL;
$arguments1462['languageIdentifier'] = NULL;
$renderChildrenClosure1463 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1464 = $self->getViewHelper('$viewHelper1464', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1464->setArguments($arguments1462);
$viewHelper1464->setRenderingContext($renderingContext);
$viewHelper1464->setRenderChildrenClosure($renderChildrenClosure1463);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1461['title'] = $viewHelper1464->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1465 = array();
// Rendering Boolean node
$arguments1465['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tagMode', $renderingContext), 1);
$arguments1465['then'] = 'neos-active';
$arguments1465['else'] = NULL;
$renderChildrenClosure1466 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1467 = $self->getViewHelper('$viewHelper1467', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1467->setArguments($arguments1465);
$viewHelper1467->setRenderingContext($renderingContext);
$viewHelper1467->setRenderChildrenClosure($renderChildrenClosure1466);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1461['class'] = $viewHelper1467->initializeArgumentsAndRender();
// Rendering Array
$array1468 = array();
$array1468['tagMode'] = 1;
$arguments1461['arguments'] = $array1468;
$arguments1461['additionalAttributes'] = NULL;
$arguments1461['data'] = NULL;
$arguments1461['controller'] = NULL;
$arguments1461['package'] = NULL;
$arguments1461['subpackage'] = NULL;
$arguments1461['section'] = '';
$arguments1461['format'] = '';
$arguments1461['additionalParams'] = array (
);
$arguments1461['addQueryString'] = false;
$arguments1461['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1461['useParentRequest'] = false;
$arguments1461['absolute'] = true;
$arguments1461['dir'] = NULL;
$arguments1461['id'] = NULL;
$arguments1461['lang'] = NULL;
$arguments1461['style'] = NULL;
$arguments1461['accesskey'] = NULL;
$arguments1461['tabindex'] = NULL;
$arguments1461['onclick'] = NULL;
$arguments1461['name'] = NULL;
$arguments1461['rel'] = NULL;
$arguments1461['rev'] = NULL;
$arguments1461['target'] = NULL;
$renderChildrenClosure1469 = function() use ($renderingContext, $self) {
$output1470 = '';

$output1470 .= '
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1471 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1472 = array();
$arguments1472['id'] = 'media.tags.all';
$arguments1472['source'] = 'Modules';
$arguments1472['package'] = 'TYPO3.Neos';
$arguments1472['value'] = NULL;
$arguments1472['arguments'] = array (
);
$arguments1472['quantity'] = NULL;
$arguments1472['languageIdentifier'] = NULL;
$renderChildrenClosure1473 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1474 = $self->getViewHelper('$viewHelper1474', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1474->setArguments($arguments1472);
$viewHelper1474->setRenderingContext($renderingContext);
$viewHelper1474->setRenderChildrenClosure($renderChildrenClosure1473);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1471['value'] = $viewHelper1474->initializeArgumentsAndRender();
$arguments1471['keepQuotes'] = false;
$arguments1471['encoding'] = 'UTF-8';
$arguments1471['doubleEncode'] = true;
$renderChildrenClosure1475 = function() use ($renderingContext, $self) {
return NULL;
};
$value1476 = ($arguments1471['value'] !== NULL ? $arguments1471['value'] : $renderChildrenClosure1475());

$output1470 .= !is_string($value1476) && !(is_object($value1476) && method_exists($value1476, '__toString')) ? $value1476 : htmlspecialchars($value1476, ($arguments1471['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1471['encoding'], $arguments1471['doubleEncode']);

$output1470 .= '
					<span class="count">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1477 = array();
$arguments1477['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'allCount', $renderingContext);
$arguments1477['keepQuotes'] = false;
$arguments1477['encoding'] = 'UTF-8';
$arguments1477['doubleEncode'] = true;
$renderChildrenClosure1478 = function() use ($renderingContext, $self) {
return NULL;
};
$value1479 = ($arguments1477['value'] !== NULL ? $arguments1477['value'] : $renderChildrenClosure1478());

$output1470 .= !is_string($value1479) && !(is_object($value1479) && method_exists($value1479, '__toString')) ? $value1479 : htmlspecialchars($value1479, ($arguments1477['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1477['encoding'], $arguments1477['doubleEncode']);

$output1470 .= '</span>
				';
return $output1470;
};
$viewHelper1480 = $self->getViewHelper('$viewHelper1480', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper1480->setArguments($arguments1461);
$viewHelper1480->setRenderingContext($renderingContext);
$viewHelper1480->setRenderChildrenClosure($renderChildrenClosure1469);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output1224 .= $viewHelper1480->initializeArgumentsAndRender();

$output1224 .= '
			</li>
			<li class="neos-media-list-untagged">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments1481 = array();
$arguments1481['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1482 = array();
$arguments1482['id'] = 'media.untaggedAssets';
$arguments1482['source'] = 'Modules';
$arguments1482['package'] = 'TYPO3.Neos';
$arguments1482['value'] = NULL;
$arguments1482['arguments'] = array (
);
$arguments1482['quantity'] = NULL;
$arguments1482['languageIdentifier'] = NULL;
$renderChildrenClosure1483 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1484 = $self->getViewHelper('$viewHelper1484', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1484->setArguments($arguments1482);
$viewHelper1484->setRenderingContext($renderingContext);
$viewHelper1484->setRenderChildrenClosure($renderChildrenClosure1483);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1481['title'] = $viewHelper1484->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1485 = array();
// Rendering Boolean node
$arguments1485['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tagMode', $renderingContext), 2);
$arguments1485['then'] = 'neos-active';
$arguments1485['else'] = NULL;
$renderChildrenClosure1486 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1487 = $self->getViewHelper('$viewHelper1487', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1487->setArguments($arguments1485);
$viewHelper1487->setRenderingContext($renderingContext);
$viewHelper1487->setRenderChildrenClosure($renderChildrenClosure1486);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1481['class'] = $viewHelper1487->initializeArgumentsAndRender();
// Rendering Array
$array1488 = array();
$array1488['tagMode'] = 2;
$arguments1481['arguments'] = $array1488;
$arguments1481['additionalAttributes'] = NULL;
$arguments1481['data'] = NULL;
$arguments1481['controller'] = NULL;
$arguments1481['package'] = NULL;
$arguments1481['subpackage'] = NULL;
$arguments1481['section'] = '';
$arguments1481['format'] = '';
$arguments1481['additionalParams'] = array (
);
$arguments1481['addQueryString'] = false;
$arguments1481['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1481['useParentRequest'] = false;
$arguments1481['absolute'] = true;
$arguments1481['dir'] = NULL;
$arguments1481['id'] = NULL;
$arguments1481['lang'] = NULL;
$arguments1481['style'] = NULL;
$arguments1481['accesskey'] = NULL;
$arguments1481['tabindex'] = NULL;
$arguments1481['onclick'] = NULL;
$arguments1481['name'] = NULL;
$arguments1481['rel'] = NULL;
$arguments1481['rev'] = NULL;
$arguments1481['target'] = NULL;
$renderChildrenClosure1489 = function() use ($renderingContext, $self) {
$output1490 = '';

$output1490 .= '
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1491 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1492 = array();
$arguments1492['id'] = 'media.untagged';
$arguments1492['source'] = 'Modules';
$arguments1492['package'] = 'TYPO3.Neos';
$arguments1492['value'] = NULL;
$arguments1492['arguments'] = array (
);
$arguments1492['quantity'] = NULL;
$arguments1492['languageIdentifier'] = NULL;
$renderChildrenClosure1493 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1494 = $self->getViewHelper('$viewHelper1494', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1494->setArguments($arguments1492);
$viewHelper1494->setRenderingContext($renderingContext);
$viewHelper1494->setRenderChildrenClosure($renderChildrenClosure1493);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1491['value'] = $viewHelper1494->initializeArgumentsAndRender();
$arguments1491['keepQuotes'] = false;
$arguments1491['encoding'] = 'UTF-8';
$arguments1491['doubleEncode'] = true;
$renderChildrenClosure1495 = function() use ($renderingContext, $self) {
return NULL;
};
$value1496 = ($arguments1491['value'] !== NULL ? $arguments1491['value'] : $renderChildrenClosure1495());

$output1490 .= !is_string($value1496) && !(is_object($value1496) && method_exists($value1496, '__toString')) ? $value1496 : htmlspecialchars($value1496, ($arguments1491['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1491['encoding'], $arguments1491['doubleEncode']);

$output1490 .= '
					<span class="count">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1497 = array();
$arguments1497['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'untaggedCount', $renderingContext);
$arguments1497['keepQuotes'] = false;
$arguments1497['encoding'] = 'UTF-8';
$arguments1497['doubleEncode'] = true;
$renderChildrenClosure1498 = function() use ($renderingContext, $self) {
return NULL;
};
$value1499 = ($arguments1497['value'] !== NULL ? $arguments1497['value'] : $renderChildrenClosure1498());

$output1490 .= !is_string($value1499) && !(is_object($value1499) && method_exists($value1499, '__toString')) ? $value1499 : htmlspecialchars($value1499, ($arguments1497['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1497['encoding'], $arguments1497['doubleEncode']);

$output1490 .= '</span>
				';
return $output1490;
};
$viewHelper1500 = $self->getViewHelper('$viewHelper1500', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper1500->setArguments($arguments1481);
$viewHelper1500->setRenderingContext($renderingContext);
$viewHelper1500->setRenderChildrenClosure($renderChildrenClosure1489);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output1224 .= $viewHelper1500->initializeArgumentsAndRender();

$output1224 .= '
			</li>
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments1501 = array();
$arguments1501['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tags', $renderingContext);
$arguments1501['as'] = 'tag';
$arguments1501['key'] = '';
$arguments1501['reverse'] = false;
$arguments1501['iteration'] = NULL;
$renderChildrenClosure1502 = function() use ($renderingContext, $self) {
$output1503 = '';

$output1503 .= '
				<li>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments1504 = array();
$arguments1504['action'] = 'index';
$arguments1504['title'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.object.label', $renderingContext);
$output1505 = '';

$output1505 .= 'droppable-tag';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1506 = array();
// Rendering Boolean node
$arguments1506['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.object', $renderingContext), \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'activeTag', $renderingContext));
$arguments1506['then'] = ' neos-active';
$arguments1506['else'] = NULL;
$renderChildrenClosure1507 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1508 = $self->getViewHelper('$viewHelper1508', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1508->setArguments($arguments1506);
$viewHelper1508->setRenderingContext($renderingContext);
$viewHelper1508->setRenderChildrenClosure($renderChildrenClosure1507);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output1505 .= $viewHelper1508->initializeArgumentsAndRender();
$arguments1504['class'] = $output1505;
// Rendering Array
$array1509 = array();
$array1509['tag'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.object', $renderingContext);
$arguments1504['arguments'] = $array1509;
// Rendering Array
$array1510 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper
$arguments1511 = array();
$arguments1511['value'] = NULL;
$renderChildrenClosure1512 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.object', $renderingContext);
};
$viewHelper1513 = $self->getViewHelper('$viewHelper1513', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper');
$viewHelper1513->setArguments($arguments1511);
$viewHelper1513->setRenderingContext($renderingContext);
$viewHelper1513->setRenderChildrenClosure($renderChildrenClosure1512);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper
$array1510['tag-identifier'] = $viewHelper1513->initializeArgumentsAndRender();
$arguments1504['data'] = $array1510;
$arguments1504['additionalAttributes'] = NULL;
$arguments1504['controller'] = NULL;
$arguments1504['package'] = NULL;
$arguments1504['subpackage'] = NULL;
$arguments1504['section'] = '';
$arguments1504['format'] = '';
$arguments1504['additionalParams'] = array (
);
$arguments1504['addQueryString'] = false;
$arguments1504['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1504['useParentRequest'] = false;
$arguments1504['absolute'] = true;
$arguments1504['dir'] = NULL;
$arguments1504['id'] = NULL;
$arguments1504['lang'] = NULL;
$arguments1504['style'] = NULL;
$arguments1504['accesskey'] = NULL;
$arguments1504['tabindex'] = NULL;
$arguments1504['onclick'] = NULL;
$arguments1504['name'] = NULL;
$arguments1504['rel'] = NULL;
$arguments1504['rev'] = NULL;
$arguments1504['target'] = NULL;
$renderChildrenClosure1514 = function() use ($renderingContext, $self) {
$output1515 = '';

$output1515 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1516 = array();
$arguments1516['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.object.label', $renderingContext);
$arguments1516['keepQuotes'] = false;
$arguments1516['encoding'] = 'UTF-8';
$arguments1516['doubleEncode'] = true;
$renderChildrenClosure1517 = function() use ($renderingContext, $self) {
return NULL;
};
$value1518 = ($arguments1516['value'] !== NULL ? $arguments1516['value'] : $renderChildrenClosure1517());

$output1515 .= !is_string($value1518) && !(is_object($value1518) && method_exists($value1518, '__toString')) ? $value1518 : htmlspecialchars($value1518, ($arguments1516['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1516['encoding'], $arguments1516['doubleEncode']);

$output1515 .= '
						<span class="count">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1519 = array();
$arguments1519['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.count', $renderingContext);
$arguments1519['keepQuotes'] = false;
$arguments1519['encoding'] = 'UTF-8';
$arguments1519['doubleEncode'] = true;
$renderChildrenClosure1520 = function() use ($renderingContext, $self) {
return NULL;
};
$value1521 = ($arguments1519['value'] !== NULL ? $arguments1519['value'] : $renderChildrenClosure1520());

$output1515 .= !is_string($value1521) && !(is_object($value1521) && method_exists($value1521, '__toString')) ? $value1521 : htmlspecialchars($value1521, ($arguments1519['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1519['encoding'], $arguments1519['doubleEncode']);

$output1515 .= '</span>
					';
return $output1515;
};
$viewHelper1522 = $self->getViewHelper('$viewHelper1522', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper1522->setArguments($arguments1504);
$viewHelper1522->setRenderingContext($renderingContext);
$viewHelper1522->setRenderChildrenClosure($renderChildrenClosure1514);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output1503 .= $viewHelper1522->initializeArgumentsAndRender();

$output1503 .= '
					<div class="neos-sidelist-edit-actions">
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments1523 = array();
$arguments1523['class'] = 'neos-button';
$arguments1523['action'] = 'editTag';
// Rendering Array
$array1524 = array();
$array1524['tag'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.object', $renderingContext);
$arguments1523['arguments'] = $array1524;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1525 = array();
$arguments1525['id'] = 'media.editTag';
$arguments1525['source'] = 'Modules';
$arguments1525['package'] = 'TYPO3.Neos';
$arguments1525['value'] = NULL;
$arguments1525['arguments'] = array (
);
$arguments1525['quantity'] = NULL;
$arguments1525['languageIdentifier'] = NULL;
$renderChildrenClosure1526 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1527 = $self->getViewHelper('$viewHelper1527', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1527->setArguments($arguments1525);
$viewHelper1527->setRenderingContext($renderingContext);
$viewHelper1527->setRenderChildrenClosure($renderChildrenClosure1526);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1523['title'] = $viewHelper1527->initializeArgumentsAndRender();
// Rendering Array
$array1528 = array();
$array1528['neos-toggle'] = 'tooltip';
$arguments1523['data'] = $array1528;
$arguments1523['additionalAttributes'] = NULL;
$arguments1523['controller'] = NULL;
$arguments1523['package'] = NULL;
$arguments1523['subpackage'] = NULL;
$arguments1523['section'] = '';
$arguments1523['format'] = '';
$arguments1523['additionalParams'] = array (
);
$arguments1523['addQueryString'] = false;
$arguments1523['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1523['useParentRequest'] = false;
$arguments1523['absolute'] = true;
$arguments1523['dir'] = NULL;
$arguments1523['id'] = NULL;
$arguments1523['lang'] = NULL;
$arguments1523['style'] = NULL;
$arguments1523['accesskey'] = NULL;
$arguments1523['tabindex'] = NULL;
$arguments1523['onclick'] = NULL;
$arguments1523['name'] = NULL;
$arguments1523['rel'] = NULL;
$arguments1523['rev'] = NULL;
$arguments1523['target'] = NULL;
$renderChildrenClosure1529 = function() use ($renderingContext, $self) {
return '<i class="icon-pencil"></i>';
};
$viewHelper1530 = $self->getViewHelper('$viewHelper1530', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper1530->setArguments($arguments1523);
$viewHelper1530->setRenderingContext($renderingContext);
$viewHelper1530->setRenderChildrenClosure($renderChildrenClosure1529);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output1503 .= $viewHelper1530->initializeArgumentsAndRender();

$output1503 .= '
						<button class="neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1531 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1532 = array();
$arguments1532['id'] = 'media.deleteTag';
$arguments1532['source'] = 'Modules';
$arguments1532['package'] = 'TYPO3.Neos';
$arguments1532['value'] = NULL;
$arguments1532['arguments'] = array (
);
$arguments1532['quantity'] = NULL;
$arguments1532['languageIdentifier'] = NULL;
$renderChildrenClosure1533 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1534 = $self->getViewHelper('$viewHelper1534', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1534->setArguments($arguments1532);
$viewHelper1534->setRenderingContext($renderingContext);
$viewHelper1534->setRenderChildrenClosure($renderChildrenClosure1533);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1531['value'] = $viewHelper1534->initializeArgumentsAndRender();
$arguments1531['keepQuotes'] = false;
$arguments1531['encoding'] = 'UTF-8';
$arguments1531['doubleEncode'] = true;
$renderChildrenClosure1535 = function() use ($renderingContext, $self) {
return NULL;
};
$value1536 = ($arguments1531['value'] !== NULL ? $arguments1531['value'] : $renderChildrenClosure1535());

$output1503 .= !is_string($value1536) && !(is_object($value1536) && method_exists($value1536, '__toString')) ? $value1536 : htmlspecialchars($value1536, ($arguments1531['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1531['encoding'], $arguments1531['doubleEncode']);

$output1503 .= '" data-neos-toggle="tooltip" data-modal="delete-tag-modal" data-object-identifier="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1537 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper
$arguments1538 = array();
$arguments1538['value'] = NULL;
$renderChildrenClosure1539 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.object', $renderingContext);
};
$viewHelper1540 = $self->getViewHelper('$viewHelper1540', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper');
$viewHelper1540->setArguments($arguments1538);
$viewHelper1540->setRenderingContext($renderingContext);
$viewHelper1540->setRenderChildrenClosure($renderChildrenClosure1539);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\IdentifierViewHelper
$arguments1537['value'] = $viewHelper1540->initializeArgumentsAndRender();
$arguments1537['keepQuotes'] = false;
$arguments1537['encoding'] = 'UTF-8';
$arguments1537['doubleEncode'] = true;
$renderChildrenClosure1541 = function() use ($renderingContext, $self) {
return NULL;
};
$value1542 = ($arguments1537['value'] !== NULL ? $arguments1537['value'] : $renderChildrenClosure1541());

$output1503 .= !is_string($value1542) && !(is_object($value1542) && method_exists($value1542, '__toString')) ? $value1542 : htmlspecialchars($value1542, ($arguments1537['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1537['encoding'], $arguments1537['doubleEncode']);

$output1503 .= '" data-label="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1543 = array();
$arguments1543['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.object.label', $renderingContext);
$arguments1543['keepQuotes'] = false;
$arguments1543['encoding'] = 'UTF-8';
$arguments1543['doubleEncode'] = true;
$renderChildrenClosure1544 = function() use ($renderingContext, $self) {
return NULL;
};
$value1545 = ($arguments1543['value'] !== NULL ? $arguments1543['value'] : $renderChildrenClosure1544());

$output1503 .= !is_string($value1545) && !(is_object($value1545) && method_exists($value1545, '__toString')) ? $value1545 : htmlspecialchars($value1545, ($arguments1543['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1543['encoding'], $arguments1543['doubleEncode']);

$output1503 .= '"><i class="icon-trash"></i></button>
					</div>
				</li>
			';
return $output1503;
};

$output1224 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments1501, $renderChildrenClosure1502, $renderingContext);

$output1224 .= '
			<div class="neos-hide" id="delete-tag-modal">
				<div class="neos-modal-centered">
					<div class="neos-modal-content">
						<div class="neos-modal-header">
							<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
							<div class="neos-header">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1546 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1547 = array();
$arguments1547['id'] = 'media.message.reallyDeleteTag';
$arguments1547['source'] = 'Modules';
$arguments1547['package'] = 'TYPO3.Neos';
$arguments1547['value'] = NULL;
$arguments1547['arguments'] = array (
);
$arguments1547['quantity'] = NULL;
$arguments1547['languageIdentifier'] = NULL;
$renderChildrenClosure1548 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1549 = $self->getViewHelper('$viewHelper1549', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1549->setArguments($arguments1547);
$viewHelper1549->setRenderingContext($renderingContext);
$viewHelper1549->setRenderChildrenClosure($renderChildrenClosure1548);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1546['value'] = $viewHelper1549->initializeArgumentsAndRender();
$arguments1546['keepQuotes'] = false;
$arguments1546['encoding'] = 'UTF-8';
$arguments1546['doubleEncode'] = true;
$renderChildrenClosure1550 = function() use ($renderingContext, $self) {
return NULL;
};
$value1551 = ($arguments1546['value'] !== NULL ? $arguments1546['value'] : $renderChildrenClosure1550());

$output1224 .= !is_string($value1551) && !(is_object($value1551) && method_exists($value1551, '__toString')) ? $value1551 : htmlspecialchars($value1551, ($arguments1546['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1546['encoding'], $arguments1546['doubleEncode']);

$output1224 .= '</div>
							<div>
								<div class="neos-subheader">
									<p>
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1552 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1553 = array();
$arguments1553['id'] = 'media.message.willDeleteTag';
$arguments1553['source'] = 'Modules';
$arguments1553['package'] = 'TYPO3.Neos';
$arguments1553['value'] = NULL;
$arguments1553['arguments'] = array (
);
$arguments1553['quantity'] = NULL;
$arguments1553['languageIdentifier'] = NULL;
$renderChildrenClosure1554 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1555 = $self->getViewHelper('$viewHelper1555', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1555->setArguments($arguments1553);
$viewHelper1555->setRenderingContext($renderingContext);
$viewHelper1555->setRenderChildrenClosure($renderChildrenClosure1554);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1552['value'] = $viewHelper1555->initializeArgumentsAndRender();
$arguments1552['keepQuotes'] = false;
$arguments1552['encoding'] = 'UTF-8';
$arguments1552['doubleEncode'] = true;
$renderChildrenClosure1556 = function() use ($renderingContext, $self) {
return NULL;
};
$value1557 = ($arguments1552['value'] !== NULL ? $arguments1552['value'] : $renderChildrenClosure1556());

$output1224 .= !is_string($value1557) && !(is_object($value1557) && method_exists($value1557, '__toString')) ? $value1557 : htmlspecialchars($value1557, ($arguments1552['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1552['encoding'], $arguments1552['doubleEncode']);

$output1224 .= '<br />
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1558 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1559 = array();
$arguments1559['id'] = 'media.message.operationCannotBeUndone';
$arguments1559['source'] = 'Modules';
$arguments1559['package'] = 'TYPO3.Neos';
$arguments1559['value'] = NULL;
$arguments1559['arguments'] = array (
);
$arguments1559['quantity'] = NULL;
$arguments1559['languageIdentifier'] = NULL;
$renderChildrenClosure1560 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1561 = $self->getViewHelper('$viewHelper1561', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1561->setArguments($arguments1559);
$viewHelper1561->setRenderingContext($renderingContext);
$viewHelper1561->setRenderChildrenClosure($renderChildrenClosure1560);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1558['value'] = $viewHelper1561->initializeArgumentsAndRender();
$arguments1558['keepQuotes'] = false;
$arguments1558['encoding'] = 'UTF-8';
$arguments1558['doubleEncode'] = true;
$renderChildrenClosure1562 = function() use ($renderingContext, $self) {
return NULL;
};
$value1563 = ($arguments1558['value'] !== NULL ? $arguments1558['value'] : $renderChildrenClosure1562());

$output1224 .= !is_string($value1563) && !(is_object($value1563) && method_exists($value1563, '__toString')) ? $value1563 : htmlspecialchars($value1563, ($arguments1558['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1558['encoding'], $arguments1558['doubleEncode']);

$output1224 .= '
									</p>
								</div>
							</div>
						</div>
						<div class="neos-modal-footer">
							<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1564 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1565 = array();
$arguments1565['id'] = 'media.cancel';
$arguments1565['source'] = 'Modules';
$arguments1565['package'] = 'TYPO3.Neos';
$arguments1565['value'] = NULL;
$arguments1565['arguments'] = array (
);
$arguments1565['quantity'] = NULL;
$arguments1565['languageIdentifier'] = NULL;
$renderChildrenClosure1566 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1567 = $self->getViewHelper('$viewHelper1567', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1567->setArguments($arguments1565);
$viewHelper1567->setRenderingContext($renderingContext);
$viewHelper1567->setRenderChildrenClosure($renderChildrenClosure1566);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1564['value'] = $viewHelper1567->initializeArgumentsAndRender();
$arguments1564['keepQuotes'] = false;
$arguments1564['encoding'] = 'UTF-8';
$arguments1564['doubleEncode'] = true;
$renderChildrenClosure1568 = function() use ($renderingContext, $self) {
return NULL;
};
$value1569 = ($arguments1564['value'] !== NULL ? $arguments1564['value'] : $renderChildrenClosure1568());

$output1224 .= !is_string($value1569) && !(is_object($value1569) && method_exists($value1569, '__toString')) ? $value1569 : htmlspecialchars($value1569, ($arguments1564['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1564['encoding'], $arguments1564['doubleEncode']);

$output1224 .= '</a>
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments1570 = array();
$arguments1570['action'] = 'deleteTag';
$arguments1570['class'] = 'neos-inline';
$arguments1570['additionalAttributes'] = NULL;
$arguments1570['data'] = NULL;
$arguments1570['arguments'] = array (
);
$arguments1570['controller'] = NULL;
$arguments1570['package'] = NULL;
$arguments1570['subpackage'] = NULL;
$arguments1570['object'] = NULL;
$arguments1570['section'] = '';
$arguments1570['format'] = '';
$arguments1570['additionalParams'] = array (
);
$arguments1570['absolute'] = false;
$arguments1570['addQueryString'] = false;
$arguments1570['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1570['fieldNamePrefix'] = NULL;
$arguments1570['actionUri'] = NULL;
$arguments1570['objectName'] = NULL;
$arguments1570['useParentRequest'] = false;
$arguments1570['enctype'] = NULL;
$arguments1570['method'] = NULL;
$arguments1570['name'] = NULL;
$arguments1570['onreset'] = NULL;
$arguments1570['onsubmit'] = NULL;
$arguments1570['dir'] = NULL;
$arguments1570['id'] = NULL;
$arguments1570['lang'] = NULL;
$arguments1570['style'] = NULL;
$arguments1570['title'] = NULL;
$arguments1570['accesskey'] = NULL;
$arguments1570['tabindex'] = NULL;
$arguments1570['onclick'] = NULL;
$renderChildrenClosure1571 = function() use ($renderingContext, $self) {
$output1572 = '';

$output1572 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments1573 = array();
$arguments1573['name'] = 'tag';
$arguments1573['id'] = 'modal-form-object';
$arguments1573['additionalAttributes'] = NULL;
$arguments1573['data'] = NULL;
$arguments1573['value'] = NULL;
$arguments1573['property'] = NULL;
$arguments1573['class'] = NULL;
$arguments1573['dir'] = NULL;
$arguments1573['lang'] = NULL;
$arguments1573['style'] = NULL;
$arguments1573['title'] = NULL;
$arguments1573['accesskey'] = NULL;
$arguments1573['tabindex'] = NULL;
$arguments1573['onclick'] = NULL;
$renderChildrenClosure1574 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1575 = $self->getViewHelper('$viewHelper1575', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper1575->setArguments($arguments1573);
$viewHelper1575->setRenderingContext($renderingContext);
$viewHelper1575->setRenderChildrenClosure($renderChildrenClosure1574);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output1572 .= $viewHelper1575->initializeArgumentsAndRender();

$output1572 .= '
								<button type="submit" class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1576 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1577 = array();
$arguments1577['id'] = 'media.deleteTag';
$arguments1577['source'] = 'Modules';
$arguments1577['package'] = 'TYPO3.Neos';
$arguments1577['value'] = NULL;
$arguments1577['arguments'] = array (
);
$arguments1577['quantity'] = NULL;
$arguments1577['languageIdentifier'] = NULL;
$renderChildrenClosure1578 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1579 = $self->getViewHelper('$viewHelper1579', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1579->setArguments($arguments1577);
$viewHelper1579->setRenderingContext($renderingContext);
$viewHelper1579->setRenderChildrenClosure($renderChildrenClosure1578);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1576['value'] = $viewHelper1579->initializeArgumentsAndRender();
$arguments1576['keepQuotes'] = false;
$arguments1576['encoding'] = 'UTF-8';
$arguments1576['doubleEncode'] = true;
$renderChildrenClosure1580 = function() use ($renderingContext, $self) {
return NULL;
};
$value1581 = ($arguments1576['value'] !== NULL ? $arguments1576['value'] : $renderChildrenClosure1580());

$output1572 .= !is_string($value1581) && !(is_object($value1581) && method_exists($value1581, '__toString')) ? $value1581 : htmlspecialchars($value1581, ($arguments1576['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1576['encoding'], $arguments1576['doubleEncode']);

$output1572 .= '">
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1582 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1583 = array();
$arguments1583['id'] = 'media.message.confirmDeleteTag';
$arguments1583['source'] = 'Modules';
$arguments1583['package'] = 'TYPO3.Neos';
$arguments1583['value'] = NULL;
$arguments1583['arguments'] = array (
);
$arguments1583['quantity'] = NULL;
$arguments1583['languageIdentifier'] = NULL;
$renderChildrenClosure1584 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1585 = $self->getViewHelper('$viewHelper1585', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1585->setArguments($arguments1583);
$viewHelper1585->setRenderingContext($renderingContext);
$viewHelper1585->setRenderChildrenClosure($renderChildrenClosure1584);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1582['value'] = $viewHelper1585->initializeArgumentsAndRender();
$arguments1582['keepQuotes'] = false;
$arguments1582['encoding'] = 'UTF-8';
$arguments1582['doubleEncode'] = true;
$renderChildrenClosure1586 = function() use ($renderingContext, $self) {
return NULL;
};
$value1587 = ($arguments1582['value'] !== NULL ? $arguments1582['value'] : $renderChildrenClosure1586());

$output1572 .= !is_string($value1587) && !(is_object($value1587) && method_exists($value1587, '__toString')) ? $value1587 : htmlspecialchars($value1587, ($arguments1582['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1582['encoding'], $arguments1582['doubleEncode']);

$output1572 .= '
								</button>
							';
return $output1572;
};
$viewHelper1588 = $self->getViewHelper('$viewHelper1588', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper1588->setArguments($arguments1570);
$viewHelper1588->setRenderingContext($renderingContext);
$viewHelper1588->setRenderChildrenClosure($renderChildrenClosure1571);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output1224 .= $viewHelper1588->initializeArgumentsAndRender();

$output1224 .= '
						</div>
					</div>
				</div>
				<div class="neos-modal-backdrop neos-in"></div>
			</div>
		</ul>
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments1589 = array();
$arguments1589['action'] = 'createTag';
$arguments1589['id'] = 'neos-tags-create-form';
$arguments1589['additionalAttributes'] = NULL;
$arguments1589['data'] = NULL;
$arguments1589['arguments'] = array (
);
$arguments1589['controller'] = NULL;
$arguments1589['package'] = NULL;
$arguments1589['subpackage'] = NULL;
$arguments1589['object'] = NULL;
$arguments1589['section'] = '';
$arguments1589['format'] = '';
$arguments1589['additionalParams'] = array (
);
$arguments1589['absolute'] = false;
$arguments1589['addQueryString'] = false;
$arguments1589['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1589['fieldNamePrefix'] = NULL;
$arguments1589['actionUri'] = NULL;
$arguments1589['objectName'] = NULL;
$arguments1589['useParentRequest'] = false;
$arguments1589['enctype'] = NULL;
$arguments1589['method'] = NULL;
$arguments1589['name'] = NULL;
$arguments1589['onreset'] = NULL;
$arguments1589['onsubmit'] = NULL;
$arguments1589['class'] = NULL;
$arguments1589['dir'] = NULL;
$arguments1589['lang'] = NULL;
$arguments1589['style'] = NULL;
$arguments1589['title'] = NULL;
$arguments1589['accesskey'] = NULL;
$arguments1589['tabindex'] = NULL;
$arguments1589['onclick'] = NULL;
$renderChildrenClosure1590 = function() use ($renderingContext, $self) {
$output1591 = '';

$output1591 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments1592 = array();
$arguments1592['name'] = 'label';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1593 = array();
$arguments1593['id'] = 'media.placeholder.createTag';
$arguments1593['source'] = 'Modules';
$arguments1593['package'] = 'TYPO3.Neos';
$arguments1593['value'] = NULL;
$arguments1593['arguments'] = array (
);
$arguments1593['quantity'] = NULL;
$arguments1593['languageIdentifier'] = NULL;
$renderChildrenClosure1594 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1595 = $self->getViewHelper('$viewHelper1595', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1595->setArguments($arguments1593);
$viewHelper1595->setRenderingContext($renderingContext);
$viewHelper1595->setRenderChildrenClosure($renderChildrenClosure1594);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1592['placeholder'] = $viewHelper1595->initializeArgumentsAndRender();
$arguments1592['additionalAttributes'] = NULL;
$arguments1592['data'] = NULL;
$arguments1592['required'] = false;
$arguments1592['type'] = 'text';
$arguments1592['value'] = NULL;
$arguments1592['property'] = NULL;
$arguments1592['disabled'] = NULL;
$arguments1592['maxlength'] = NULL;
$arguments1592['readonly'] = NULL;
$arguments1592['size'] = NULL;
$arguments1592['autofocus'] = NULL;
$arguments1592['errorClass'] = 'f3-form-error';
$arguments1592['class'] = NULL;
$arguments1592['dir'] = NULL;
$arguments1592['id'] = NULL;
$arguments1592['lang'] = NULL;
$arguments1592['style'] = NULL;
$arguments1592['title'] = NULL;
$arguments1592['accesskey'] = NULL;
$arguments1592['tabindex'] = NULL;
$arguments1592['onclick'] = NULL;
$renderChildrenClosure1596 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1597 = $self->getViewHelper('$viewHelper1597', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper1597->setArguments($arguments1592);
$viewHelper1597->setRenderingContext($renderingContext);
$viewHelper1597->setRenderChildrenClosure($renderChildrenClosure1596);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output1591 .= $viewHelper1597->initializeArgumentsAndRender();

$output1591 .= '<br /><br />
			<button type="submit" class="neos-button neos-button-primary">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1598 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1599 = array();
$arguments1599['id'] = 'media.createTag';
$arguments1599['source'] = 'Modules';
$arguments1599['package'] = 'TYPO3.Neos';
$arguments1599['value'] = NULL;
$arguments1599['arguments'] = array (
);
$arguments1599['quantity'] = NULL;
$arguments1599['languageIdentifier'] = NULL;
$renderChildrenClosure1600 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1601 = $self->getViewHelper('$viewHelper1601', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1601->setArguments($arguments1599);
$viewHelper1601->setRenderingContext($renderingContext);
$viewHelper1601->setRenderChildrenClosure($renderChildrenClosure1600);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1598['value'] = $viewHelper1601->initializeArgumentsAndRender();
$arguments1598['keepQuotes'] = false;
$arguments1598['encoding'] = 'UTF-8';
$arguments1598['doubleEncode'] = true;
$renderChildrenClosure1602 = function() use ($renderingContext, $self) {
return NULL;
};
$value1603 = ($arguments1598['value'] !== NULL ? $arguments1598['value'] : $renderChildrenClosure1602());

$output1591 .= !is_string($value1603) && !(is_object($value1603) && method_exists($value1603, '__toString')) ? $value1603 : htmlspecialchars($value1603, ($arguments1598['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1598['encoding'], $arguments1598['doubleEncode']);

$output1591 .= '</button>
		';
return $output1591;
};
$viewHelper1604 = $self->getViewHelper('$viewHelper1604', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper1604->setArguments($arguments1589);
$viewHelper1604->setRenderingContext($renderingContext);
$viewHelper1604->setRenderChildrenClosure($renderChildrenClosure1590);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output1224 .= $viewHelper1604->initializeArgumentsAndRender();

$output1224 .= '
	</div>
';
return $output1224;
};

$output906 .= '';

$output906 .= '

';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\SectionViewHelper
$arguments1605 = array();
$arguments1605['name'] = 'Content';
$renderChildrenClosure1606 = function() use ($renderingContext, $self) {
$output1607 = '';

$output1607 .= '
	<div id="dropzone" class="neos-upload-area">
		<div title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1608 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1609 = array();
$arguments1609['id'] = 'media.maxUploadSize';
// Rendering Array
$array1610 = array();
$array1610['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'humanReadableMaximumFileUploadSize', $renderingContext);
$arguments1609['arguments'] = $array1610;
$arguments1609['source'] = 'Modules';
$arguments1609['package'] = 'TYPO3.Neos';
$arguments1609['value'] = NULL;
$arguments1609['quantity'] = NULL;
$arguments1609['languageIdentifier'] = NULL;
$renderChildrenClosure1611 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1612 = $self->getViewHelper('$viewHelper1612', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1612->setArguments($arguments1609);
$viewHelper1612->setRenderingContext($renderingContext);
$viewHelper1612->setRenderChildrenClosure($renderChildrenClosure1611);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1608['value'] = $viewHelper1612->initializeArgumentsAndRender();
$arguments1608['keepQuotes'] = false;
$arguments1608['encoding'] = 'UTF-8';
$arguments1608['doubleEncode'] = true;
$renderChildrenClosure1613 = function() use ($renderingContext, $self) {
return NULL;
};
$value1614 = ($arguments1608['value'] !== NULL ? $arguments1608['value'] : $renderChildrenClosure1613());

$output1607 .= !is_string($value1614) && !(is_object($value1614) && method_exists($value1614, '__toString')) ? $value1614 : htmlspecialchars($value1614, ($arguments1608['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1608['encoding'], $arguments1608['doubleEncode']);

$output1607 .= '" data-neos-toggle="tooltip">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1615 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1616 = array();
$arguments1616['id'] = 'media.dropFiles';
$arguments1616['source'] = 'Modules';
$arguments1616['package'] = 'TYPO3.Neos';
$arguments1616['value'] = NULL;
$arguments1616['arguments'] = array (
);
$arguments1616['quantity'] = NULL;
$arguments1616['languageIdentifier'] = NULL;
$renderChildrenClosure1617 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1618 = $self->getViewHelper('$viewHelper1618', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1618->setArguments($arguments1616);
$viewHelper1618->setRenderingContext($renderingContext);
$viewHelper1618->setRenderChildrenClosure($renderChildrenClosure1617);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1615['value'] = $viewHelper1618->initializeArgumentsAndRender();
$arguments1615['keepQuotes'] = false;
$arguments1615['encoding'] = 'UTF-8';
$arguments1615['doubleEncode'] = true;
$renderChildrenClosure1619 = function() use ($renderingContext, $self) {
return NULL;
};
$value1620 = ($arguments1615['value'] !== NULL ? $arguments1615['value'] : $renderChildrenClosure1619());

$output1607 .= !is_string($value1620) && !(is_object($value1620) && method_exists($value1620, '__toString')) ? $value1620 : htmlspecialchars($value1620, ($arguments1615['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1615['encoding'], $arguments1615['doubleEncode']);

$output1607 .= '<i class="icon-arrow-down"></i><span> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1621 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1622 = array();
$arguments1622['id'] = 'media.clickToUpload';
$arguments1622['source'] = 'Modules';
$arguments1622['package'] = 'TYPO3.Neos';
$arguments1622['value'] = NULL;
$arguments1622['arguments'] = array (
);
$arguments1622['quantity'] = NULL;
$arguments1622['languageIdentifier'] = NULL;
$renderChildrenClosure1623 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1624 = $self->getViewHelper('$viewHelper1624', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1624->setArguments($arguments1622);
$viewHelper1624->setRenderingContext($renderingContext);
$viewHelper1624->setRenderChildrenClosure($renderChildrenClosure1623);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1621['value'] = $viewHelper1624->initializeArgumentsAndRender();
$arguments1621['keepQuotes'] = false;
$arguments1621['encoding'] = 'UTF-8';
$arguments1621['doubleEncode'] = true;
$renderChildrenClosure1625 = function() use ($renderingContext, $self) {
return NULL;
};
$value1626 = ($arguments1621['value'] !== NULL ? $arguments1621['value'] : $renderChildrenClosure1625());

$output1607 .= !is_string($value1626) && !(is_object($value1626) && method_exists($value1626, '__toString')) ? $value1626 : htmlspecialchars($value1626, ($arguments1621['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1621['encoding'], $arguments1621['doubleEncode']);

$output1607 .= '</span></div>
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments1627 = array();
$arguments1627['method'] = 'post';
$arguments1627['action'] = 'create';
$arguments1627['object'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'asset', $renderingContext);
$arguments1627['objectName'] = 'asset';
$arguments1627['enctype'] = 'multipart/form-data';
$arguments1627['additionalAttributes'] = NULL;
$arguments1627['data'] = NULL;
$arguments1627['arguments'] = array (
);
$arguments1627['controller'] = NULL;
$arguments1627['package'] = NULL;
$arguments1627['subpackage'] = NULL;
$arguments1627['section'] = '';
$arguments1627['format'] = '';
$arguments1627['additionalParams'] = array (
);
$arguments1627['absolute'] = false;
$arguments1627['addQueryString'] = false;
$arguments1627['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1627['fieldNamePrefix'] = NULL;
$arguments1627['actionUri'] = NULL;
$arguments1627['useParentRequest'] = false;
$arguments1627['name'] = NULL;
$arguments1627['onreset'] = NULL;
$arguments1627['onsubmit'] = NULL;
$arguments1627['class'] = NULL;
$arguments1627['dir'] = NULL;
$arguments1627['id'] = NULL;
$arguments1627['lang'] = NULL;
$arguments1627['style'] = NULL;
$arguments1627['title'] = NULL;
$arguments1627['accesskey'] = NULL;
$arguments1627['tabindex'] = NULL;
$arguments1627['onclick'] = NULL;
$renderChildrenClosure1628 = function() use ($renderingContext, $self) {
$output1629 = '';

$output1629 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\UploadViewHelper
$arguments1630 = array();
$arguments1630['id'] = 'resource';
$arguments1630['property'] = 'resource';
// Rendering Array
$array1631 = array();
$array1631['required'] = 'required';
$arguments1630['additionalAttributes'] = $array1631;
$arguments1630['data'] = NULL;
$arguments1630['name'] = NULL;
$arguments1630['value'] = NULL;
$arguments1630['disabled'] = NULL;
$arguments1630['errorClass'] = 'f3-form-error';
$arguments1630['collection'] = '';
$arguments1630['class'] = NULL;
$arguments1630['dir'] = NULL;
$arguments1630['lang'] = NULL;
$arguments1630['style'] = NULL;
$arguments1630['title'] = NULL;
$arguments1630['accesskey'] = NULL;
$arguments1630['tabindex'] = NULL;
$arguments1630['onclick'] = NULL;
$renderChildrenClosure1632 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1633 = $self->getViewHelper('$viewHelper1633', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\UploadViewHelper');
$viewHelper1633->setArguments($arguments1630);
$viewHelper1633->setRenderingContext($renderingContext);
$viewHelper1633->setRenderChildrenClosure($renderChildrenClosure1632);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\UploadViewHelper

$output1629 .= $viewHelper1633->initializeArgumentsAndRender();

$output1629 .= '
		';
return $output1629;
};
$viewHelper1634 = $self->getViewHelper('$viewHelper1634', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper1634->setArguments($arguments1627);
$viewHelper1634->setRenderingContext($renderingContext);
$viewHelper1634->setRenderChildrenClosure($renderChildrenClosure1628);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output1607 .= $viewHelper1634->initializeArgumentsAndRender();

$output1607 .= '
	</div>
	<div id="uploader">
		<div id="filelist"></div>
	</div>
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments1635 = array();
// Rendering Boolean node
$arguments1635['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assets', $renderingContext));
$arguments1635['then'] = NULL;
$arguments1635['else'] = NULL;
$renderChildrenClosure1636 = function() use ($renderingContext, $self) {
$output1637 = '';

$output1637 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments1638 = array();
$renderChildrenClosure1639 = function() use ($renderingContext, $self) {
$output1640 = '';

$output1640 .= '
			<div class="neos-media-content-help">
				<i class="icon-info-circle"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1641 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1642 = array();
$arguments1642['id'] = 'media.dragHelp';
$arguments1642['source'] = 'Modules';
$arguments1642['package'] = 'TYPO3.Neos';
$arguments1642['value'] = NULL;
$arguments1642['arguments'] = array (
);
$arguments1642['quantity'] = NULL;
$arguments1642['languageIdentifier'] = NULL;
$renderChildrenClosure1643 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1644 = $self->getViewHelper('$viewHelper1644', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1644->setArguments($arguments1642);
$viewHelper1644->setRenderingContext($renderingContext);
$viewHelper1644->setRenderChildrenClosure($renderChildrenClosure1643);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1641['value'] = $viewHelper1644->initializeArgumentsAndRender();
$arguments1641['keepQuotes'] = false;
$arguments1641['encoding'] = 'UTF-8';
$arguments1641['doubleEncode'] = true;
$renderChildrenClosure1645 = function() use ($renderingContext, $self) {
return NULL;
};
$value1646 = ($arguments1641['value'] !== NULL ? $arguments1641['value'] : $renderChildrenClosure1645());

$output1640 .= !is_string($value1646) && !(is_object($value1646) && method_exists($value1646, '__toString')) ? $value1646 : htmlspecialchars($value1646, ($arguments1641['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1641['encoding'], $arguments1641['doubleEncode']);

$output1640 .= '
			</div>
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper
$arguments1647 = array();
$output1648 = '';

$output1648 .= \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'view', $renderingContext);

$output1648 .= 'View';
$arguments1647['partial'] = $output1648;
// Rendering Array
$array1649 = array();
$array1649['assets'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assets', $renderingContext);
$array1649['sortBy'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext);
$array1649['sortDirection'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortDirection', $renderingContext);
$arguments1647['arguments'] = $array1649;
$arguments1647['section'] = NULL;
$arguments1647['optional'] = false;
$renderChildrenClosure1650 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1651 = $self->getViewHelper('$viewHelper1651', $renderingContext, 'TYPO3\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper1651->setArguments($arguments1647);
$viewHelper1651->setRenderingContext($renderingContext);
$viewHelper1651->setRenderChildrenClosure($renderChildrenClosure1650);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper

$output1640 .= $viewHelper1651->initializeArgumentsAndRender();

$output1640 .= '
			<div class="neos-hide" id="delete-asset-modal">
				<div class="neos-modal-centered">
					<div class="neos-modal-content">
						<div class="neos-modal-header">
							<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
							<div class="neos-header">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1652 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1653 = array();
$arguments1653['id'] = 'media.message.reallyDeleteAsset';
$arguments1653['source'] = 'Modules';
$arguments1653['package'] = 'TYPO3.Neos';
$arguments1653['value'] = NULL;
$arguments1653['arguments'] = array (
);
$arguments1653['quantity'] = NULL;
$arguments1653['languageIdentifier'] = NULL;
$renderChildrenClosure1654 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1655 = $self->getViewHelper('$viewHelper1655', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1655->setArguments($arguments1653);
$viewHelper1655->setRenderingContext($renderingContext);
$viewHelper1655->setRenderChildrenClosure($renderChildrenClosure1654);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1652['value'] = $viewHelper1655->initializeArgumentsAndRender();
$arguments1652['keepQuotes'] = false;
$arguments1652['encoding'] = 'UTF-8';
$arguments1652['doubleEncode'] = true;
$renderChildrenClosure1656 = function() use ($renderingContext, $self) {
return NULL;
};
$value1657 = ($arguments1652['value'] !== NULL ? $arguments1652['value'] : $renderChildrenClosure1656());

$output1640 .= !is_string($value1657) && !(is_object($value1657) && method_exists($value1657, '__toString')) ? $value1657 : htmlspecialchars($value1657, ($arguments1652['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1652['encoding'], $arguments1652['doubleEncode']);

$output1640 .= '</div>
							<div>
								<div class="neos-subheader">
									<p>
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1658 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1659 = array();
$arguments1659['id'] = 'media.message.willBeDeleted';
$arguments1659['source'] = 'Modules';
$arguments1659['package'] = 'TYPO3.Neos';
$arguments1659['value'] = NULL;
$arguments1659['arguments'] = array (
);
$arguments1659['quantity'] = NULL;
$arguments1659['languageIdentifier'] = NULL;
$renderChildrenClosure1660 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1661 = $self->getViewHelper('$viewHelper1661', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1661->setArguments($arguments1659);
$viewHelper1661->setRenderingContext($renderingContext);
$viewHelper1661->setRenderChildrenClosure($renderChildrenClosure1660);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1658['value'] = $viewHelper1661->initializeArgumentsAndRender();
$arguments1658['keepQuotes'] = false;
$arguments1658['encoding'] = 'UTF-8';
$arguments1658['doubleEncode'] = true;
$renderChildrenClosure1662 = function() use ($renderingContext, $self) {
return NULL;
};
$value1663 = ($arguments1658['value'] !== NULL ? $arguments1658['value'] : $renderChildrenClosure1662());

$output1640 .= !is_string($value1663) && !(is_object($value1663) && method_exists($value1663, '__toString')) ? $value1663 : htmlspecialchars($value1663, ($arguments1658['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1658['encoding'], $arguments1658['doubleEncode']);

$output1640 .= '<br />
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1664 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1665 = array();
$arguments1665['id'] = 'media.message.operationCannotBeUndone';
$arguments1665['source'] = 'Modules';
$arguments1665['package'] = 'TYPO3.Neos';
$arguments1665['value'] = NULL;
$arguments1665['arguments'] = array (
);
$arguments1665['quantity'] = NULL;
$arguments1665['languageIdentifier'] = NULL;
$renderChildrenClosure1666 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1667 = $self->getViewHelper('$viewHelper1667', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1667->setArguments($arguments1665);
$viewHelper1667->setRenderingContext($renderingContext);
$viewHelper1667->setRenderChildrenClosure($renderChildrenClosure1666);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1664['value'] = $viewHelper1667->initializeArgumentsAndRender();
$arguments1664['keepQuotes'] = false;
$arguments1664['encoding'] = 'UTF-8';
$arguments1664['doubleEncode'] = true;
$renderChildrenClosure1668 = function() use ($renderingContext, $self) {
return NULL;
};
$value1669 = ($arguments1664['value'] !== NULL ? $arguments1664['value'] : $renderChildrenClosure1668());

$output1640 .= !is_string($value1669) && !(is_object($value1669) && method_exists($value1669, '__toString')) ? $value1669 : htmlspecialchars($value1669, ($arguments1664['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1664['encoding'], $arguments1664['doubleEncode']);

$output1640 .= '
									</p>
								</div>
							</div>
						</div>
						<div class="neos-modal-footer">
							<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1670 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1671 = array();
$arguments1671['id'] = 'media.cancel';
$arguments1671['source'] = 'Modules';
$arguments1671['package'] = 'TYPO3.Neos';
$arguments1671['value'] = NULL;
$arguments1671['arguments'] = array (
);
$arguments1671['quantity'] = NULL;
$arguments1671['languageIdentifier'] = NULL;
$renderChildrenClosure1672 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1673 = $self->getViewHelper('$viewHelper1673', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1673->setArguments($arguments1671);
$viewHelper1673->setRenderingContext($renderingContext);
$viewHelper1673->setRenderChildrenClosure($renderChildrenClosure1672);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1670['value'] = $viewHelper1673->initializeArgumentsAndRender();
$arguments1670['keepQuotes'] = false;
$arguments1670['encoding'] = 'UTF-8';
$arguments1670['doubleEncode'] = true;
$renderChildrenClosure1674 = function() use ($renderingContext, $self) {
return NULL;
};
$value1675 = ($arguments1670['value'] !== NULL ? $arguments1670['value'] : $renderChildrenClosure1674());

$output1640 .= !is_string($value1675) && !(is_object($value1675) && method_exists($value1675, '__toString')) ? $value1675 : htmlspecialchars($value1675, ($arguments1670['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1670['encoding'], $arguments1670['doubleEncode']);

$output1640 .= '</a>
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments1676 = array();
$arguments1676['action'] = 'delete';
$arguments1676['method'] = 'post';
$arguments1676['class'] = 'neos-inline';
$arguments1676['additionalAttributes'] = NULL;
$arguments1676['data'] = NULL;
$arguments1676['arguments'] = array (
);
$arguments1676['controller'] = NULL;
$arguments1676['package'] = NULL;
$arguments1676['subpackage'] = NULL;
$arguments1676['object'] = NULL;
$arguments1676['section'] = '';
$arguments1676['format'] = '';
$arguments1676['additionalParams'] = array (
);
$arguments1676['absolute'] = false;
$arguments1676['addQueryString'] = false;
$arguments1676['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1676['fieldNamePrefix'] = NULL;
$arguments1676['actionUri'] = NULL;
$arguments1676['objectName'] = NULL;
$arguments1676['useParentRequest'] = false;
$arguments1676['enctype'] = NULL;
$arguments1676['name'] = NULL;
$arguments1676['onreset'] = NULL;
$arguments1676['onsubmit'] = NULL;
$arguments1676['dir'] = NULL;
$arguments1676['id'] = NULL;
$arguments1676['lang'] = NULL;
$arguments1676['style'] = NULL;
$arguments1676['title'] = NULL;
$arguments1676['accesskey'] = NULL;
$arguments1676['tabindex'] = NULL;
$arguments1676['onclick'] = NULL;
$renderChildrenClosure1677 = function() use ($renderingContext, $self) {
$output1678 = '';

$output1678 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments1679 = array();
$arguments1679['name'] = 'asset';
$arguments1679['id'] = 'modal-form-object';
$arguments1679['additionalAttributes'] = NULL;
$arguments1679['data'] = NULL;
$arguments1679['value'] = NULL;
$arguments1679['property'] = NULL;
$arguments1679['class'] = NULL;
$arguments1679['dir'] = NULL;
$arguments1679['lang'] = NULL;
$arguments1679['style'] = NULL;
$arguments1679['title'] = NULL;
$arguments1679['accesskey'] = NULL;
$arguments1679['tabindex'] = NULL;
$arguments1679['onclick'] = NULL;
$renderChildrenClosure1680 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1681 = $self->getViewHelper('$viewHelper1681', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper1681->setArguments($arguments1679);
$viewHelper1681->setRenderingContext($renderingContext);
$viewHelper1681->setRenderChildrenClosure($renderChildrenClosure1680);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output1678 .= $viewHelper1681->initializeArgumentsAndRender();

$output1678 .= '
								<button type="submit" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1682 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1683 = array();
$arguments1683['id'] = 'media.tooltip.deleteAsset';
$arguments1683['source'] = 'Modules';
$arguments1683['package'] = 'TYPO3.Neos';
$arguments1683['value'] = NULL;
$arguments1683['arguments'] = array (
);
$arguments1683['quantity'] = NULL;
$arguments1683['languageIdentifier'] = NULL;
$renderChildrenClosure1684 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1685 = $self->getViewHelper('$viewHelper1685', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1685->setArguments($arguments1683);
$viewHelper1685->setRenderingContext($renderingContext);
$viewHelper1685->setRenderChildrenClosure($renderChildrenClosure1684);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1682['value'] = $viewHelper1685->initializeArgumentsAndRender();
$arguments1682['keepQuotes'] = false;
$arguments1682['encoding'] = 'UTF-8';
$arguments1682['doubleEncode'] = true;
$renderChildrenClosure1686 = function() use ($renderingContext, $self) {
return NULL;
};
$value1687 = ($arguments1682['value'] !== NULL ? $arguments1682['value'] : $renderChildrenClosure1686());

$output1678 .= !is_string($value1687) && !(is_object($value1687) && method_exists($value1687, '__toString')) ? $value1687 : htmlspecialchars($value1687, ($arguments1682['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1682['encoding'], $arguments1682['doubleEncode']);

$output1678 .= '" class="neos-button neos-button-mini neos-button-danger">
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1688 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1689 = array();
$arguments1689['id'] = 'media.message.confirmDelete';
$arguments1689['source'] = 'Modules';
$arguments1689['package'] = 'TYPO3.Neos';
$arguments1689['value'] = NULL;
$arguments1689['arguments'] = array (
);
$arguments1689['quantity'] = NULL;
$arguments1689['languageIdentifier'] = NULL;
$renderChildrenClosure1690 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1691 = $self->getViewHelper('$viewHelper1691', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1691->setArguments($arguments1689);
$viewHelper1691->setRenderingContext($renderingContext);
$viewHelper1691->setRenderChildrenClosure($renderChildrenClosure1690);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1688['value'] = $viewHelper1691->initializeArgumentsAndRender();
$arguments1688['keepQuotes'] = false;
$arguments1688['encoding'] = 'UTF-8';
$arguments1688['doubleEncode'] = true;
$renderChildrenClosure1692 = function() use ($renderingContext, $self) {
return NULL;
};
$value1693 = ($arguments1688['value'] !== NULL ? $arguments1688['value'] : $renderChildrenClosure1692());

$output1678 .= !is_string($value1693) && !(is_object($value1693) && method_exists($value1693, '__toString')) ? $value1693 : htmlspecialchars($value1693, ($arguments1688['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1688['encoding'], $arguments1688['doubleEncode']);

$output1678 .= '
								</button>
							';
return $output1678;
};
$viewHelper1694 = $self->getViewHelper('$viewHelper1694', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper1694->setArguments($arguments1676);
$viewHelper1694->setRenderingContext($renderingContext);
$viewHelper1694->setRenderChildrenClosure($renderChildrenClosure1677);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output1640 .= $viewHelper1694->initializeArgumentsAndRender();

$output1640 .= '
						</div>
					</div>
				</div>
				<div class="neos-modal-backdrop neos-in"></div>
			</div>
		';
return $output1640;
};
$viewHelper1695 = $self->getViewHelper('$viewHelper1695', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper1695->setArguments($arguments1638);
$viewHelper1695->setRenderingContext($renderingContext);
$viewHelper1695->setRenderChildrenClosure($renderChildrenClosure1639);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output1637 .= $viewHelper1695->initializeArgumentsAndRender();

$output1637 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments1696 = array();
$renderChildrenClosure1697 = function() use ($renderingContext, $self) {
$output1698 = '';

$output1698 .= '
			<p>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1699 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1700 = array();
$arguments1700['id'] = 'media.noAssetsFound';
$arguments1700['source'] = 'Modules';
$arguments1700['package'] = 'TYPO3.Neos';
$arguments1700['value'] = NULL;
$arguments1700['arguments'] = array (
);
$arguments1700['quantity'] = NULL;
$arguments1700['languageIdentifier'] = NULL;
$renderChildrenClosure1701 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1702 = $self->getViewHelper('$viewHelper1702', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1702->setArguments($arguments1700);
$viewHelper1702->setRenderingContext($renderingContext);
$viewHelper1702->setRenderChildrenClosure($renderChildrenClosure1701);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1699['value'] = $viewHelper1702->initializeArgumentsAndRender();
$arguments1699['keepQuotes'] = false;
$arguments1699['encoding'] = 'UTF-8';
$arguments1699['doubleEncode'] = true;
$renderChildrenClosure1703 = function() use ($renderingContext, $self) {
return NULL;
};
$value1704 = ($arguments1699['value'] !== NULL ? $arguments1699['value'] : $renderChildrenClosure1703());

$output1698 .= !is_string($value1704) && !(is_object($value1704) && method_exists($value1704, '__toString')) ? $value1704 : htmlspecialchars($value1704, ($arguments1699['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1699['encoding'], $arguments1699['doubleEncode']);

$output1698 .= '</p>
		';
return $output1698;
};
$viewHelper1705 = $self->getViewHelper('$viewHelper1705', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper1705->setArguments($arguments1696);
$viewHelper1705->setRenderingContext($renderingContext);
$viewHelper1705->setRenderChildrenClosure($renderChildrenClosure1697);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output1637 .= $viewHelper1705->initializeArgumentsAndRender();

$output1637 .= '
	';
return $output1637;
};
$arguments1635['__thenClosure'] = function() use ($renderingContext, $self) {
$output1706 = '';

$output1706 .= '
			<div class="neos-media-content-help">
				<i class="icon-info-circle"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1707 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1708 = array();
$arguments1708['id'] = 'media.dragHelp';
$arguments1708['source'] = 'Modules';
$arguments1708['package'] = 'TYPO3.Neos';
$arguments1708['value'] = NULL;
$arguments1708['arguments'] = array (
);
$arguments1708['quantity'] = NULL;
$arguments1708['languageIdentifier'] = NULL;
$renderChildrenClosure1709 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1710 = $self->getViewHelper('$viewHelper1710', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1710->setArguments($arguments1708);
$viewHelper1710->setRenderingContext($renderingContext);
$viewHelper1710->setRenderChildrenClosure($renderChildrenClosure1709);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1707['value'] = $viewHelper1710->initializeArgumentsAndRender();
$arguments1707['keepQuotes'] = false;
$arguments1707['encoding'] = 'UTF-8';
$arguments1707['doubleEncode'] = true;
$renderChildrenClosure1711 = function() use ($renderingContext, $self) {
return NULL;
};
$value1712 = ($arguments1707['value'] !== NULL ? $arguments1707['value'] : $renderChildrenClosure1711());

$output1706 .= !is_string($value1712) && !(is_object($value1712) && method_exists($value1712, '__toString')) ? $value1712 : htmlspecialchars($value1712, ($arguments1707['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1707['encoding'], $arguments1707['doubleEncode']);

$output1706 .= '
			</div>
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper
$arguments1713 = array();
$output1714 = '';

$output1714 .= \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'view', $renderingContext);

$output1714 .= 'View';
$arguments1713['partial'] = $output1714;
// Rendering Array
$array1715 = array();
$array1715['assets'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'assets', $renderingContext);
$array1715['sortBy'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortBy', $renderingContext);
$array1715['sortDirection'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sortDirection', $renderingContext);
$arguments1713['arguments'] = $array1715;
$arguments1713['section'] = NULL;
$arguments1713['optional'] = false;
$renderChildrenClosure1716 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1717 = $self->getViewHelper('$viewHelper1717', $renderingContext, 'TYPO3\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper1717->setArguments($arguments1713);
$viewHelper1717->setRenderingContext($renderingContext);
$viewHelper1717->setRenderChildrenClosure($renderChildrenClosure1716);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper

$output1706 .= $viewHelper1717->initializeArgumentsAndRender();

$output1706 .= '
			<div class="neos-hide" id="delete-asset-modal">
				<div class="neos-modal-centered">
					<div class="neos-modal-content">
						<div class="neos-modal-header">
							<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
							<div class="neos-header">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1718 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1719 = array();
$arguments1719['id'] = 'media.message.reallyDeleteAsset';
$arguments1719['source'] = 'Modules';
$arguments1719['package'] = 'TYPO3.Neos';
$arguments1719['value'] = NULL;
$arguments1719['arguments'] = array (
);
$arguments1719['quantity'] = NULL;
$arguments1719['languageIdentifier'] = NULL;
$renderChildrenClosure1720 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1721 = $self->getViewHelper('$viewHelper1721', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1721->setArguments($arguments1719);
$viewHelper1721->setRenderingContext($renderingContext);
$viewHelper1721->setRenderChildrenClosure($renderChildrenClosure1720);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1718['value'] = $viewHelper1721->initializeArgumentsAndRender();
$arguments1718['keepQuotes'] = false;
$arguments1718['encoding'] = 'UTF-8';
$arguments1718['doubleEncode'] = true;
$renderChildrenClosure1722 = function() use ($renderingContext, $self) {
return NULL;
};
$value1723 = ($arguments1718['value'] !== NULL ? $arguments1718['value'] : $renderChildrenClosure1722());

$output1706 .= !is_string($value1723) && !(is_object($value1723) && method_exists($value1723, '__toString')) ? $value1723 : htmlspecialchars($value1723, ($arguments1718['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1718['encoding'], $arguments1718['doubleEncode']);

$output1706 .= '</div>
							<div>
								<div class="neos-subheader">
									<p>
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1724 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1725 = array();
$arguments1725['id'] = 'media.message.willBeDeleted';
$arguments1725['source'] = 'Modules';
$arguments1725['package'] = 'TYPO3.Neos';
$arguments1725['value'] = NULL;
$arguments1725['arguments'] = array (
);
$arguments1725['quantity'] = NULL;
$arguments1725['languageIdentifier'] = NULL;
$renderChildrenClosure1726 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1727 = $self->getViewHelper('$viewHelper1727', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1727->setArguments($arguments1725);
$viewHelper1727->setRenderingContext($renderingContext);
$viewHelper1727->setRenderChildrenClosure($renderChildrenClosure1726);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1724['value'] = $viewHelper1727->initializeArgumentsAndRender();
$arguments1724['keepQuotes'] = false;
$arguments1724['encoding'] = 'UTF-8';
$arguments1724['doubleEncode'] = true;
$renderChildrenClosure1728 = function() use ($renderingContext, $self) {
return NULL;
};
$value1729 = ($arguments1724['value'] !== NULL ? $arguments1724['value'] : $renderChildrenClosure1728());

$output1706 .= !is_string($value1729) && !(is_object($value1729) && method_exists($value1729, '__toString')) ? $value1729 : htmlspecialchars($value1729, ($arguments1724['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1724['encoding'], $arguments1724['doubleEncode']);

$output1706 .= '<br />
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1730 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1731 = array();
$arguments1731['id'] = 'media.message.operationCannotBeUndone';
$arguments1731['source'] = 'Modules';
$arguments1731['package'] = 'TYPO3.Neos';
$arguments1731['value'] = NULL;
$arguments1731['arguments'] = array (
);
$arguments1731['quantity'] = NULL;
$arguments1731['languageIdentifier'] = NULL;
$renderChildrenClosure1732 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1733 = $self->getViewHelper('$viewHelper1733', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1733->setArguments($arguments1731);
$viewHelper1733->setRenderingContext($renderingContext);
$viewHelper1733->setRenderChildrenClosure($renderChildrenClosure1732);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1730['value'] = $viewHelper1733->initializeArgumentsAndRender();
$arguments1730['keepQuotes'] = false;
$arguments1730['encoding'] = 'UTF-8';
$arguments1730['doubleEncode'] = true;
$renderChildrenClosure1734 = function() use ($renderingContext, $self) {
return NULL;
};
$value1735 = ($arguments1730['value'] !== NULL ? $arguments1730['value'] : $renderChildrenClosure1734());

$output1706 .= !is_string($value1735) && !(is_object($value1735) && method_exists($value1735, '__toString')) ? $value1735 : htmlspecialchars($value1735, ($arguments1730['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1730['encoding'], $arguments1730['doubleEncode']);

$output1706 .= '
									</p>
								</div>
							</div>
						</div>
						<div class="neos-modal-footer">
							<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1736 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1737 = array();
$arguments1737['id'] = 'media.cancel';
$arguments1737['source'] = 'Modules';
$arguments1737['package'] = 'TYPO3.Neos';
$arguments1737['value'] = NULL;
$arguments1737['arguments'] = array (
);
$arguments1737['quantity'] = NULL;
$arguments1737['languageIdentifier'] = NULL;
$renderChildrenClosure1738 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1739 = $self->getViewHelper('$viewHelper1739', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1739->setArguments($arguments1737);
$viewHelper1739->setRenderingContext($renderingContext);
$viewHelper1739->setRenderChildrenClosure($renderChildrenClosure1738);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1736['value'] = $viewHelper1739->initializeArgumentsAndRender();
$arguments1736['keepQuotes'] = false;
$arguments1736['encoding'] = 'UTF-8';
$arguments1736['doubleEncode'] = true;
$renderChildrenClosure1740 = function() use ($renderingContext, $self) {
return NULL;
};
$value1741 = ($arguments1736['value'] !== NULL ? $arguments1736['value'] : $renderChildrenClosure1740());

$output1706 .= !is_string($value1741) && !(is_object($value1741) && method_exists($value1741, '__toString')) ? $value1741 : htmlspecialchars($value1741, ($arguments1736['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1736['encoding'], $arguments1736['doubleEncode']);

$output1706 .= '</a>
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments1742 = array();
$arguments1742['action'] = 'delete';
$arguments1742['method'] = 'post';
$arguments1742['class'] = 'neos-inline';
$arguments1742['additionalAttributes'] = NULL;
$arguments1742['data'] = NULL;
$arguments1742['arguments'] = array (
);
$arguments1742['controller'] = NULL;
$arguments1742['package'] = NULL;
$arguments1742['subpackage'] = NULL;
$arguments1742['object'] = NULL;
$arguments1742['section'] = '';
$arguments1742['format'] = '';
$arguments1742['additionalParams'] = array (
);
$arguments1742['absolute'] = false;
$arguments1742['addQueryString'] = false;
$arguments1742['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1742['fieldNamePrefix'] = NULL;
$arguments1742['actionUri'] = NULL;
$arguments1742['objectName'] = NULL;
$arguments1742['useParentRequest'] = false;
$arguments1742['enctype'] = NULL;
$arguments1742['name'] = NULL;
$arguments1742['onreset'] = NULL;
$arguments1742['onsubmit'] = NULL;
$arguments1742['dir'] = NULL;
$arguments1742['id'] = NULL;
$arguments1742['lang'] = NULL;
$arguments1742['style'] = NULL;
$arguments1742['title'] = NULL;
$arguments1742['accesskey'] = NULL;
$arguments1742['tabindex'] = NULL;
$arguments1742['onclick'] = NULL;
$renderChildrenClosure1743 = function() use ($renderingContext, $self) {
$output1744 = '';

$output1744 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments1745 = array();
$arguments1745['name'] = 'asset';
$arguments1745['id'] = 'modal-form-object';
$arguments1745['additionalAttributes'] = NULL;
$arguments1745['data'] = NULL;
$arguments1745['value'] = NULL;
$arguments1745['property'] = NULL;
$arguments1745['class'] = NULL;
$arguments1745['dir'] = NULL;
$arguments1745['lang'] = NULL;
$arguments1745['style'] = NULL;
$arguments1745['title'] = NULL;
$arguments1745['accesskey'] = NULL;
$arguments1745['tabindex'] = NULL;
$arguments1745['onclick'] = NULL;
$renderChildrenClosure1746 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1747 = $self->getViewHelper('$viewHelper1747', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper1747->setArguments($arguments1745);
$viewHelper1747->setRenderingContext($renderingContext);
$viewHelper1747->setRenderChildrenClosure($renderChildrenClosure1746);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output1744 .= $viewHelper1747->initializeArgumentsAndRender();

$output1744 .= '
								<button type="submit" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1748 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1749 = array();
$arguments1749['id'] = 'media.tooltip.deleteAsset';
$arguments1749['source'] = 'Modules';
$arguments1749['package'] = 'TYPO3.Neos';
$arguments1749['value'] = NULL;
$arguments1749['arguments'] = array (
);
$arguments1749['quantity'] = NULL;
$arguments1749['languageIdentifier'] = NULL;
$renderChildrenClosure1750 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1751 = $self->getViewHelper('$viewHelper1751', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1751->setArguments($arguments1749);
$viewHelper1751->setRenderingContext($renderingContext);
$viewHelper1751->setRenderChildrenClosure($renderChildrenClosure1750);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1748['value'] = $viewHelper1751->initializeArgumentsAndRender();
$arguments1748['keepQuotes'] = false;
$arguments1748['encoding'] = 'UTF-8';
$arguments1748['doubleEncode'] = true;
$renderChildrenClosure1752 = function() use ($renderingContext, $self) {
return NULL;
};
$value1753 = ($arguments1748['value'] !== NULL ? $arguments1748['value'] : $renderChildrenClosure1752());

$output1744 .= !is_string($value1753) && !(is_object($value1753) && method_exists($value1753, '__toString')) ? $value1753 : htmlspecialchars($value1753, ($arguments1748['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1748['encoding'], $arguments1748['doubleEncode']);

$output1744 .= '" class="neos-button neos-button-mini neos-button-danger">
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1754 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1755 = array();
$arguments1755['id'] = 'media.message.confirmDelete';
$arguments1755['source'] = 'Modules';
$arguments1755['package'] = 'TYPO3.Neos';
$arguments1755['value'] = NULL;
$arguments1755['arguments'] = array (
);
$arguments1755['quantity'] = NULL;
$arguments1755['languageIdentifier'] = NULL;
$renderChildrenClosure1756 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1757 = $self->getViewHelper('$viewHelper1757', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1757->setArguments($arguments1755);
$viewHelper1757->setRenderingContext($renderingContext);
$viewHelper1757->setRenderChildrenClosure($renderChildrenClosure1756);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1754['value'] = $viewHelper1757->initializeArgumentsAndRender();
$arguments1754['keepQuotes'] = false;
$arguments1754['encoding'] = 'UTF-8';
$arguments1754['doubleEncode'] = true;
$renderChildrenClosure1758 = function() use ($renderingContext, $self) {
return NULL;
};
$value1759 = ($arguments1754['value'] !== NULL ? $arguments1754['value'] : $renderChildrenClosure1758());

$output1744 .= !is_string($value1759) && !(is_object($value1759) && method_exists($value1759, '__toString')) ? $value1759 : htmlspecialchars($value1759, ($arguments1754['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1754['encoding'], $arguments1754['doubleEncode']);

$output1744 .= '
								</button>
							';
return $output1744;
};
$viewHelper1760 = $self->getViewHelper('$viewHelper1760', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper1760->setArguments($arguments1742);
$viewHelper1760->setRenderingContext($renderingContext);
$viewHelper1760->setRenderChildrenClosure($renderChildrenClosure1743);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output1706 .= $viewHelper1760->initializeArgumentsAndRender();

$output1706 .= '
						</div>
					</div>
				</div>
				<div class="neos-modal-backdrop neos-in"></div>
			</div>
		';
return $output1706;
};
$arguments1635['__elseClosure'] = function() use ($renderingContext, $self) {
$output1761 = '';

$output1761 .= '
			<p>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1762 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1763 = array();
$arguments1763['id'] = 'media.noAssetsFound';
$arguments1763['source'] = 'Modules';
$arguments1763['package'] = 'TYPO3.Neos';
$arguments1763['value'] = NULL;
$arguments1763['arguments'] = array (
);
$arguments1763['quantity'] = NULL;
$arguments1763['languageIdentifier'] = NULL;
$renderChildrenClosure1764 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1765 = $self->getViewHelper('$viewHelper1765', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1765->setArguments($arguments1763);
$viewHelper1765->setRenderingContext($renderingContext);
$viewHelper1765->setRenderChildrenClosure($renderChildrenClosure1764);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1762['value'] = $viewHelper1765->initializeArgumentsAndRender();
$arguments1762['keepQuotes'] = false;
$arguments1762['encoding'] = 'UTF-8';
$arguments1762['doubleEncode'] = true;
$renderChildrenClosure1766 = function() use ($renderingContext, $self) {
return NULL;
};
$value1767 = ($arguments1762['value'] !== NULL ? $arguments1762['value'] : $renderChildrenClosure1766());

$output1761 .= !is_string($value1767) && !(is_object($value1767) && method_exists($value1767, '__toString')) ? $value1767 : htmlspecialchars($value1767, ($arguments1762['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1762['encoding'], $arguments1762['doubleEncode']);

$output1761 .= '</p>
		';
return $output1761;
};
$viewHelper1768 = $self->getViewHelper('$viewHelper1768', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper1768->setArguments($arguments1635);
$viewHelper1768->setRenderingContext($renderingContext);
$viewHelper1768->setRenderChildrenClosure($renderChildrenClosure1636);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output1607 .= $viewHelper1768->initializeArgumentsAndRender();

$output1607 .= '
';
return $output1607;
};

$output906 .= '';

$output906 .= '

';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\SectionViewHelper
$arguments1769 = array();
$arguments1769['name'] = 'Scripts';
$renderChildrenClosure1770 = function() use ($renderingContext, $self) {
$output1771 = '';

$output1771 .= '
	<script type="text/javascript">
		var uploadUrl = \'';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1772 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments1773 = array();
$arguments1773['action'] = 'upload';
// Rendering Array
$array1774 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Security\CsrfTokenViewHelper
$arguments1775 = array();
$renderChildrenClosure1776 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1777 = $self->getViewHelper('$viewHelper1777', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Security\CsrfTokenViewHelper');
$viewHelper1777->setArguments($arguments1775);
$viewHelper1777->setRenderingContext($renderingContext);
$viewHelper1777->setRenderChildrenClosure($renderChildrenClosure1776);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Security\CsrfTokenViewHelper
$array1774['__csrfToken'] = $viewHelper1777->initializeArgumentsAndRender();
$arguments1773['additionalParams'] = $array1774;
// Rendering Boolean node
$arguments1773['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('true');
$arguments1773['arguments'] = array (
);
$arguments1773['controller'] = NULL;
$arguments1773['package'] = NULL;
$arguments1773['subpackage'] = NULL;
$arguments1773['section'] = '';
$arguments1773['format'] = '';
$arguments1773['addQueryString'] = false;
$arguments1773['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1773['useParentRequest'] = false;
$renderChildrenClosure1778 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1779 = $self->getViewHelper('$viewHelper1779', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper1779->setArguments($arguments1773);
$viewHelper1779->setRenderingContext($renderingContext);
$viewHelper1779->setRenderChildrenClosure($renderChildrenClosure1778);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments1772['value'] = $viewHelper1779->initializeArgumentsAndRender();
$arguments1772['keepQuotes'] = false;
$arguments1772['encoding'] = 'UTF-8';
$arguments1772['doubleEncode'] = true;
$renderChildrenClosure1780 = function() use ($renderingContext, $self) {
return NULL;
};
$value1781 = ($arguments1772['value'] !== NULL ? $arguments1772['value'] : $renderChildrenClosure1780());

$output1771 .= !is_string($value1781) && !(is_object($value1781) && method_exists($value1781, '__toString')) ? $value1781 : htmlspecialchars($value1781, ($arguments1772['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1772['encoding'], $arguments1772['doubleEncode']);

$output1771 .= '\';
		var maximumFileUploadSize = ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1782 = array();
$arguments1782['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'maximumFileUploadSize', $renderingContext);
$arguments1782['keepQuotes'] = false;
$arguments1782['encoding'] = 'UTF-8';
$arguments1782['doubleEncode'] = true;
$renderChildrenClosure1783 = function() use ($renderingContext, $self) {
return NULL;
};
$value1784 = ($arguments1782['value'] !== NULL ? $arguments1782['value'] : $renderChildrenClosure1783());

$output1771 .= !is_string($value1784) && !(is_object($value1784) && method_exists($value1784, '__toString')) ? $value1784 : htmlspecialchars($value1784, ($arguments1782['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1782['encoding'], $arguments1782['doubleEncode']);

$output1771 .= ';

		if (window.parent !== window && window.parent.Typo3MediaBrowserCallbacks && window.parent.Typo3MediaBrowserCallbacks.refreshThumbnail) {
			window.parent.Typo3MediaBrowserCallbacks.refreshThumbnail();
		}
	</script>
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments1785 = array();
$arguments1785['action'] = 'tagAsset';
$arguments1785['id'] = 'tag-asset-form';
$arguments1785['format'] = 'json';
$arguments1785['additionalAttributes'] = NULL;
$arguments1785['data'] = NULL;
$arguments1785['arguments'] = array (
);
$arguments1785['controller'] = NULL;
$arguments1785['package'] = NULL;
$arguments1785['subpackage'] = NULL;
$arguments1785['object'] = NULL;
$arguments1785['section'] = '';
$arguments1785['additionalParams'] = array (
);
$arguments1785['absolute'] = false;
$arguments1785['addQueryString'] = false;
$arguments1785['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1785['fieldNamePrefix'] = NULL;
$arguments1785['actionUri'] = NULL;
$arguments1785['objectName'] = NULL;
$arguments1785['useParentRequest'] = false;
$arguments1785['enctype'] = NULL;
$arguments1785['method'] = NULL;
$arguments1785['name'] = NULL;
$arguments1785['onreset'] = NULL;
$arguments1785['onsubmit'] = NULL;
$arguments1785['class'] = NULL;
$arguments1785['dir'] = NULL;
$arguments1785['lang'] = NULL;
$arguments1785['style'] = NULL;
$arguments1785['title'] = NULL;
$arguments1785['accesskey'] = NULL;
$arguments1785['tabindex'] = NULL;
$arguments1785['onclick'] = NULL;
$renderChildrenClosure1786 = function() use ($renderingContext, $self) {
$output1787 = '';

$output1787 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments1788 = array();
$arguments1788['name'] = 'asset[__identity]';
$arguments1788['id'] = 'tag-asset-form-asset';
$arguments1788['additionalAttributes'] = NULL;
$arguments1788['data'] = NULL;
$arguments1788['value'] = NULL;
$arguments1788['property'] = NULL;
$arguments1788['class'] = NULL;
$arguments1788['dir'] = NULL;
$arguments1788['lang'] = NULL;
$arguments1788['style'] = NULL;
$arguments1788['title'] = NULL;
$arguments1788['accesskey'] = NULL;
$arguments1788['tabindex'] = NULL;
$arguments1788['onclick'] = NULL;
$renderChildrenClosure1789 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1790 = $self->getViewHelper('$viewHelper1790', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper1790->setArguments($arguments1788);
$viewHelper1790->setRenderingContext($renderingContext);
$viewHelper1790->setRenderChildrenClosure($renderChildrenClosure1789);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output1787 .= $viewHelper1790->initializeArgumentsAndRender();

$output1787 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments1791 = array();
$arguments1791['name'] = 'tag[__identity]';
$arguments1791['id'] = 'tag-asset-form-tag';
$arguments1791['additionalAttributes'] = NULL;
$arguments1791['data'] = NULL;
$arguments1791['value'] = NULL;
$arguments1791['property'] = NULL;
$arguments1791['class'] = NULL;
$arguments1791['dir'] = NULL;
$arguments1791['lang'] = NULL;
$arguments1791['style'] = NULL;
$arguments1791['title'] = NULL;
$arguments1791['accesskey'] = NULL;
$arguments1791['tabindex'] = NULL;
$arguments1791['onclick'] = NULL;
$renderChildrenClosure1792 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1793 = $self->getViewHelper('$viewHelper1793', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper1793->setArguments($arguments1791);
$viewHelper1793->setRenderingContext($renderingContext);
$viewHelper1793->setRenderChildrenClosure($renderChildrenClosure1792);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output1787 .= $viewHelper1793->initializeArgumentsAndRender();

$output1787 .= '
	';
return $output1787;
};
$viewHelper1794 = $self->getViewHelper('$viewHelper1794', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper1794->setArguments($arguments1785);
$viewHelper1794->setRenderingContext($renderingContext);
$viewHelper1794->setRenderChildrenClosure($renderChildrenClosure1786);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output1771 .= $viewHelper1794->initializeArgumentsAndRender();

$output1771 .= '
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments1795 = array();
$arguments1795['action'] = 'addAssetToCollection';
$arguments1795['id'] = 'link-asset-to-assetcollection-form';
$arguments1795['format'] = 'json';
$arguments1795['additionalAttributes'] = NULL;
$arguments1795['data'] = NULL;
$arguments1795['arguments'] = array (
);
$arguments1795['controller'] = NULL;
$arguments1795['package'] = NULL;
$arguments1795['subpackage'] = NULL;
$arguments1795['object'] = NULL;
$arguments1795['section'] = '';
$arguments1795['additionalParams'] = array (
);
$arguments1795['absolute'] = false;
$arguments1795['addQueryString'] = false;
$arguments1795['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1795['fieldNamePrefix'] = NULL;
$arguments1795['actionUri'] = NULL;
$arguments1795['objectName'] = NULL;
$arguments1795['useParentRequest'] = false;
$arguments1795['enctype'] = NULL;
$arguments1795['method'] = NULL;
$arguments1795['name'] = NULL;
$arguments1795['onreset'] = NULL;
$arguments1795['onsubmit'] = NULL;
$arguments1795['class'] = NULL;
$arguments1795['dir'] = NULL;
$arguments1795['lang'] = NULL;
$arguments1795['style'] = NULL;
$arguments1795['title'] = NULL;
$arguments1795['accesskey'] = NULL;
$arguments1795['tabindex'] = NULL;
$arguments1795['onclick'] = NULL;
$renderChildrenClosure1796 = function() use ($renderingContext, $self) {
$output1797 = '';

$output1797 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments1798 = array();
$arguments1798['name'] = 'asset[__identity]';
$arguments1798['id'] = 'link-asset-to-assetcollection-form-asset';
$arguments1798['additionalAttributes'] = NULL;
$arguments1798['data'] = NULL;
$arguments1798['value'] = NULL;
$arguments1798['property'] = NULL;
$arguments1798['class'] = NULL;
$arguments1798['dir'] = NULL;
$arguments1798['lang'] = NULL;
$arguments1798['style'] = NULL;
$arguments1798['title'] = NULL;
$arguments1798['accesskey'] = NULL;
$arguments1798['tabindex'] = NULL;
$arguments1798['onclick'] = NULL;
$renderChildrenClosure1799 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1800 = $self->getViewHelper('$viewHelper1800', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper1800->setArguments($arguments1798);
$viewHelper1800->setRenderingContext($renderingContext);
$viewHelper1800->setRenderChildrenClosure($renderChildrenClosure1799);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output1797 .= $viewHelper1800->initializeArgumentsAndRender();

$output1797 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments1801 = array();
$arguments1801['name'] = 'assetCollection[__identity]';
$arguments1801['id'] = 'link-asset-to-assetcollection-form-assetcollection';
$arguments1801['additionalAttributes'] = NULL;
$arguments1801['data'] = NULL;
$arguments1801['value'] = NULL;
$arguments1801['property'] = NULL;
$arguments1801['class'] = NULL;
$arguments1801['dir'] = NULL;
$arguments1801['lang'] = NULL;
$arguments1801['style'] = NULL;
$arguments1801['title'] = NULL;
$arguments1801['accesskey'] = NULL;
$arguments1801['tabindex'] = NULL;
$arguments1801['onclick'] = NULL;
$renderChildrenClosure1802 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1803 = $self->getViewHelper('$viewHelper1803', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper1803->setArguments($arguments1801);
$viewHelper1803->setRenderingContext($renderingContext);
$viewHelper1803->setRenderChildrenClosure($renderChildrenClosure1802);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output1797 .= $viewHelper1803->initializeArgumentsAndRender();

$output1797 .= '
	';
return $output1797;
};
$viewHelper1804 = $self->getViewHelper('$viewHelper1804', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper1804->setArguments($arguments1795);
$viewHelper1804->setRenderingContext($renderingContext);
$viewHelper1804->setRenderChildrenClosure($renderChildrenClosure1796);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output1771 .= $viewHelper1804->initializeArgumentsAndRender();

$output1771 .= '
	<script type="text/javascript" src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1805 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments1806 = array();
$arguments1806['package'] = 'TYPO3.Media';
$arguments1806['path'] = 'Libraries/plupload/plupload.full.js';
$arguments1806['resource'] = NULL;
$arguments1806['localize'] = true;
$renderChildrenClosure1807 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1808 = $self->getViewHelper('$viewHelper1808', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper1808->setArguments($arguments1806);
$viewHelper1808->setRenderingContext($renderingContext);
$viewHelper1808->setRenderChildrenClosure($renderChildrenClosure1807);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments1805['value'] = $viewHelper1808->initializeArgumentsAndRender();
$arguments1805['keepQuotes'] = false;
$arguments1805['encoding'] = 'UTF-8';
$arguments1805['doubleEncode'] = true;
$renderChildrenClosure1809 = function() use ($renderingContext, $self) {
return NULL;
};
$value1810 = ($arguments1805['value'] !== NULL ? $arguments1805['value'] : $renderChildrenClosure1809());

$output1771 .= !is_string($value1810) && !(is_object($value1810) && method_exists($value1810, '__toString')) ? $value1810 : htmlspecialchars($value1810, ($arguments1805['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1805['encoding'], $arguments1805['doubleEncode']);

$output1771 .= '"></script>
	<script type="text/javascript" src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1811 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments1812 = array();
$arguments1812['package'] = 'TYPO3.Media';
$arguments1812['path'] = 'Libraries/jquery-ui/js/jquery-ui-1.10.3.custom.js';
$arguments1812['resource'] = NULL;
$arguments1812['localize'] = true;
$renderChildrenClosure1813 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1814 = $self->getViewHelper('$viewHelper1814', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper1814->setArguments($arguments1812);
$viewHelper1814->setRenderingContext($renderingContext);
$viewHelper1814->setRenderChildrenClosure($renderChildrenClosure1813);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments1811['value'] = $viewHelper1814->initializeArgumentsAndRender();
$arguments1811['keepQuotes'] = false;
$arguments1811['encoding'] = 'UTF-8';
$arguments1811['doubleEncode'] = true;
$renderChildrenClosure1815 = function() use ($renderingContext, $self) {
return NULL;
};
$value1816 = ($arguments1811['value'] !== NULL ? $arguments1811['value'] : $renderChildrenClosure1815());

$output1771 .= !is_string($value1816) && !(is_object($value1816) && method_exists($value1816, '__toString')) ? $value1816 : htmlspecialchars($value1816, ($arguments1811['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1811['encoding'], $arguments1811['doubleEncode']);

$output1771 .= '"></script>
	<script type="text/javascript" src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1817 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments1818 = array();
$arguments1818['package'] = 'TYPO3.Media';
$arguments1818['path'] = 'Scripts/upload.js';
$arguments1818['resource'] = NULL;
$arguments1818['localize'] = true;
$renderChildrenClosure1819 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1820 = $self->getViewHelper('$viewHelper1820', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper1820->setArguments($arguments1818);
$viewHelper1820->setRenderingContext($renderingContext);
$viewHelper1820->setRenderChildrenClosure($renderChildrenClosure1819);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments1817['value'] = $viewHelper1820->initializeArgumentsAndRender();
$arguments1817['keepQuotes'] = false;
$arguments1817['encoding'] = 'UTF-8';
$arguments1817['doubleEncode'] = true;
$renderChildrenClosure1821 = function() use ($renderingContext, $self) {
return NULL;
};
$value1822 = ($arguments1817['value'] !== NULL ? $arguments1817['value'] : $renderChildrenClosure1821());

$output1771 .= !is_string($value1822) && !(is_object($value1822) && method_exists($value1822, '__toString')) ? $value1822 : htmlspecialchars($value1822, ($arguments1817['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1817['encoding'], $arguments1817['doubleEncode']);

$output1771 .= '"></script>
	<script type="text/javascript" src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1823 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments1824 = array();
$arguments1824['package'] = 'TYPO3.Media';
$arguments1824['path'] = 'Scripts/collections-and-tagging.js';
$arguments1824['resource'] = NULL;
$arguments1824['localize'] = true;
$renderChildrenClosure1825 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1826 = $self->getViewHelper('$viewHelper1826', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper1826->setArguments($arguments1824);
$viewHelper1826->setRenderingContext($renderingContext);
$viewHelper1826->setRenderChildrenClosure($renderChildrenClosure1825);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments1823['value'] = $viewHelper1826->initializeArgumentsAndRender();
$arguments1823['keepQuotes'] = false;
$arguments1823['encoding'] = 'UTF-8';
$arguments1823['doubleEncode'] = true;
$renderChildrenClosure1827 = function() use ($renderingContext, $self) {
return NULL;
};
$value1828 = ($arguments1823['value'] !== NULL ? $arguments1823['value'] : $renderChildrenClosure1827());

$output1771 .= !is_string($value1828) && !(is_object($value1828) && method_exists($value1828, '__toString')) ? $value1828 : htmlspecialchars($value1828, ($arguments1823['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1823['encoding'], $arguments1823['doubleEncode']);

$output1771 .= '"></script>
';
return $output1771;
};

$output906 .= '';

$output906 .= '
';

return $output906;
}


}
#0             544708    