<?php class FluidCache_TYPO3_Neos__Module_Administration_Users_action_index_177e4203b51d9a4f67eebc7c73f61a6fa301810a extends \TYPO3\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// TODO
	return new \TYPO3\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;

return 'BackendSubModule';
}
public function hasLayout() {
return TRUE;
}

/**
 * section content
 */
public function section_040f06fd774092478d450774f5ba30c5da78acc8(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output0 = '';

$output0 .= '
	<div class="neos-row-fluid">
		<table class="neos-table">
			<thead>
				<tr>
					<th>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments2 = array();
$arguments2['id'] = 'users.name';
$arguments2['source'] = 'Modules';
$arguments2['package'] = 'TYPO3.Neos';
$arguments2['value'] = NULL;
$arguments2['arguments'] = array (
);
$arguments2['quantity'] = NULL;
$arguments2['languageIdentifier'] = NULL;
$renderChildrenClosure3 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper4 = $self->getViewHelper('$viewHelper4', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper4->setArguments($arguments2);
$viewHelper4->setRenderingContext($renderingContext);
$viewHelper4->setRenderChildrenClosure($renderChildrenClosure3);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1['value'] = $viewHelper4->initializeArgumentsAndRender();
$arguments1['keepQuotes'] = false;
$arguments1['encoding'] = 'UTF-8';
$arguments1['doubleEncode'] = true;
$renderChildrenClosure5 = function() use ($renderingContext, $self) {
return NULL;
};
$value6 = ($arguments1['value'] !== NULL ? $arguments1['value'] : $renderChildrenClosure5());

$output0 .= !is_string($value6) && !(is_object($value6) && method_exists($value6, '__toString')) ? $value6 : htmlspecialchars($value6, ($arguments1['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1['encoding'], $arguments1['doubleEncode']);

$output0 .= '</th>
					<th>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments7 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments8 = array();
$arguments8['id'] = 'users.accountsAndRoles';
$arguments8['source'] = 'Modules';
$arguments8['package'] = 'TYPO3.Neos';
$arguments8['value'] = NULL;
$arguments8['arguments'] = array (
);
$arguments8['quantity'] = NULL;
$arguments8['languageIdentifier'] = NULL;
$renderChildrenClosure9 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper10 = $self->getViewHelper('$viewHelper10', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper10->setArguments($arguments8);
$viewHelper10->setRenderingContext($renderingContext);
$viewHelper10->setRenderChildrenClosure($renderChildrenClosure9);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments7['value'] = $viewHelper10->initializeArgumentsAndRender();
$arguments7['keepQuotes'] = false;
$arguments7['encoding'] = 'UTF-8';
$arguments7['doubleEncode'] = true;
$renderChildrenClosure11 = function() use ($renderingContext, $self) {
return NULL;
};
$value12 = ($arguments7['value'] !== NULL ? $arguments7['value'] : $renderChildrenClosure11());

$output0 .= !is_string($value12) && !(is_object($value12) && method_exists($value12, '__toString')) ? $value12 : htmlspecialchars($value12, ($arguments7['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments7['encoding'], $arguments7['doubleEncode']);

$output0 .= '</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments13 = array();
$arguments13['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'users', $renderingContext);
$arguments13['key'] = 'index';
$arguments13['as'] = 'user';
$arguments13['reverse'] = false;
$arguments13['iteration'] = NULL;
$renderChildrenClosure14 = function() use ($renderingContext, $self) {
$output15 = '';

$output15 .= '
				<tr>
					<td>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments16 = array();
$arguments16['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user.name.fullName', $renderingContext);
$arguments16['keepQuotes'] = false;
$arguments16['encoding'] = 'UTF-8';
$arguments16['doubleEncode'] = true;
$renderChildrenClosure17 = function() use ($renderingContext, $self) {
return NULL;
};
$value18 = ($arguments16['value'] !== NULL ? $arguments16['value'] : $renderChildrenClosure17());

$output15 .= !is_string($value18) && !(is_object($value18) && method_exists($value18, '__toString')) ? $value18 : htmlspecialchars($value18, ($arguments16['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments16['encoding'], $arguments16['doubleEncode']);

$output15 .= '</td>
					<td>
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments19 = array();
$arguments19['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user.accounts', $renderingContext);
$arguments19['as'] = 'account';
$arguments19['key'] = '';
$arguments19['reverse'] = false;
$arguments19['iteration'] = NULL;
$renderChildrenClosure20 = function() use ($renderingContext, $self) {
$output21 = '';

$output21 .= '
							<i class="icon-user icon-white"></i>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments22 = array();
$arguments22['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'account.accountIdentifier', $renderingContext);
$arguments22['keepQuotes'] = false;
$arguments22['encoding'] = 'UTF-8';
$arguments22['doubleEncode'] = true;
$renderChildrenClosure23 = function() use ($renderingContext, $self) {
return NULL;
};
$value24 = ($arguments22['value'] !== NULL ? $arguments22['value'] : $renderChildrenClosure23());

$output21 .= !is_string($value24) && !(is_object($value24) && method_exists($value24, '__toString')) ? $value24 : htmlspecialchars($value24, ($arguments22['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments22['encoding'], $arguments22['doubleEncode']);

$output21 .= ' ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper
$arguments25 = array();
$arguments25['partial'] = 'Module/Shared/Roles';
// Rendering Array
$array26 = array();
$array26['roles'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'account.roles', $renderingContext);
$arguments25['arguments'] = $array26;
$arguments25['section'] = NULL;
$arguments25['optional'] = false;
$renderChildrenClosure27 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper28 = $self->getViewHelper('$viewHelper28', $renderingContext, 'TYPO3\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper28->setArguments($arguments25);
$viewHelper28->setRenderingContext($renderingContext);
$viewHelper28->setRenderChildrenClosure($renderChildrenClosure27);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper

$output21 .= $viewHelper28->initializeArgumentsAndRender();

$output21 .= '
						';
return $output21;
};

$output15 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments19, $renderChildrenClosure20, $renderingContext);

$output15 .= '
					</td>
					<td class="neos-action">
						<div class="neos-pull-right">
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments29 = array();
$arguments29['action'] = 'show';
// Rendering Array
$array30 = array();
$array30['user'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user', $renderingContext);
$arguments29['arguments'] = $array30;
$arguments29['class'] = 'neos-button neos-button-primary';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments31 = array();
$arguments31['id'] = 'users.tooltip.view';
$arguments31['source'] = 'Modules';
$arguments31['package'] = 'TYPO3.Neos';
$arguments31['value'] = NULL;
$arguments31['arguments'] = array (
);
$arguments31['quantity'] = NULL;
$arguments31['languageIdentifier'] = NULL;
$renderChildrenClosure32 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper33 = $self->getViewHelper('$viewHelper33', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper33->setArguments($arguments31);
$viewHelper33->setRenderingContext($renderingContext);
$viewHelper33->setRenderChildrenClosure($renderChildrenClosure32);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments29['title'] = $viewHelper33->initializeArgumentsAndRender();
// Rendering Array
$array34 = array();
$array34['data-neos-toggle'] = 'tooltip';
$arguments29['additionalAttributes'] = $array34;
$arguments29['data'] = NULL;
$arguments29['controller'] = NULL;
$arguments29['package'] = NULL;
$arguments29['subpackage'] = NULL;
$arguments29['section'] = '';
$arguments29['format'] = '';
$arguments29['additionalParams'] = array (
);
$arguments29['addQueryString'] = false;
$arguments29['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments29['useParentRequest'] = false;
$arguments29['absolute'] = true;
$arguments29['dir'] = NULL;
$arguments29['id'] = NULL;
$arguments29['lang'] = NULL;
$arguments29['style'] = NULL;
$arguments29['accesskey'] = NULL;
$arguments29['tabindex'] = NULL;
$arguments29['onclick'] = NULL;
$arguments29['name'] = NULL;
$arguments29['rel'] = NULL;
$arguments29['rev'] = NULL;
$arguments29['target'] = NULL;
$renderChildrenClosure35 = function() use ($renderingContext, $self) {
return '
								<i class="icon-info-sign icon-white"></i>
							';
};
$viewHelper36 = $self->getViewHelper('$viewHelper36', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper36->setArguments($arguments29);
$viewHelper36->setRenderingContext($renderingContext);
$viewHelper36->setRenderChildrenClosure($renderChildrenClosure35);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output15 .= $viewHelper36->initializeArgumentsAndRender();

$output15 .= '
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments37 = array();
$arguments37['action'] = 'edit';
// Rendering Array
$array38 = array();
$array38['user'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user', $renderingContext);
$arguments37['arguments'] = $array38;
$arguments37['class'] = 'neos-button neos-button-primary';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments39 = array();
$arguments39['id'] = 'users.tooltip.edit';
$arguments39['source'] = 'Modules';
$arguments39['package'] = 'TYPO3.Neos';
$arguments39['value'] = NULL;
$arguments39['arguments'] = array (
);
$arguments39['quantity'] = NULL;
$arguments39['languageIdentifier'] = NULL;
$renderChildrenClosure40 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper41 = $self->getViewHelper('$viewHelper41', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper41->setArguments($arguments39);
$viewHelper41->setRenderingContext($renderingContext);
$viewHelper41->setRenderChildrenClosure($renderChildrenClosure40);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments37['title'] = $viewHelper41->initializeArgumentsAndRender();
// Rendering Array
$array42 = array();
$array42['data-neos-toggle'] = 'tooltip';
$arguments37['additionalAttributes'] = $array42;
$arguments37['data'] = NULL;
$arguments37['controller'] = NULL;
$arguments37['package'] = NULL;
$arguments37['subpackage'] = NULL;
$arguments37['section'] = '';
$arguments37['format'] = '';
$arguments37['additionalParams'] = array (
);
$arguments37['addQueryString'] = false;
$arguments37['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments37['useParentRequest'] = false;
$arguments37['absolute'] = true;
$arguments37['dir'] = NULL;
$arguments37['id'] = NULL;
$arguments37['lang'] = NULL;
$arguments37['style'] = NULL;
$arguments37['accesskey'] = NULL;
$arguments37['tabindex'] = NULL;
$arguments37['onclick'] = NULL;
$arguments37['name'] = NULL;
$arguments37['rel'] = NULL;
$arguments37['rev'] = NULL;
$arguments37['target'] = NULL;
$renderChildrenClosure43 = function() use ($renderingContext, $self) {
return '
								<i class="icon-pencil icon-white"></i>
							';
};
$viewHelper44 = $self->getViewHelper('$viewHelper44', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper44->setArguments($arguments37);
$viewHelper44->setRenderingContext($renderingContext);
$viewHelper44->setRenderChildrenClosure($renderChildrenClosure43);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output15 .= $viewHelper44->initializeArgumentsAndRender();

$output15 .= '

							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments45 = array();
// Rendering Boolean node
$arguments45['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'currentUser', $renderingContext), \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user', $renderingContext));
$arguments45['then'] = NULL;
$arguments45['else'] = NULL;
$renderChildrenClosure46 = function() use ($renderingContext, $self) {
$output47 = '';

$output47 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments48 = array();
$renderChildrenClosure49 = function() use ($renderingContext, $self) {
$output50 = '';

$output50 .= '
									<button class="neos-button neos-button-danger neos-disabled" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments51 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments52 = array();
$arguments52['id'] = 'users.tooltip.cannotDeleteYourself';
$arguments52['source'] = 'Modules';
$arguments52['package'] = 'TYPO3.Neos';
$arguments52['value'] = NULL;
$arguments52['arguments'] = array (
);
$arguments52['quantity'] = NULL;
$arguments52['languageIdentifier'] = NULL;
$renderChildrenClosure53 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper54 = $self->getViewHelper('$viewHelper54', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper54->setArguments($arguments52);
$viewHelper54->setRenderingContext($renderingContext);
$viewHelper54->setRenderChildrenClosure($renderChildrenClosure53);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments51['value'] = $viewHelper54->initializeArgumentsAndRender();
$arguments51['keepQuotes'] = false;
$arguments51['encoding'] = 'UTF-8';
$arguments51['doubleEncode'] = true;
$renderChildrenClosure55 = function() use ($renderingContext, $self) {
return NULL;
};
$value56 = ($arguments51['value'] !== NULL ? $arguments51['value'] : $renderChildrenClosure55());

$output50 .= !is_string($value56) && !(is_object($value56) && method_exists($value56, '__toString')) ? $value56 : htmlspecialchars($value56, ($arguments51['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments51['encoding'], $arguments51['doubleEncode']);

$output50 .= '" data-neos-toggle="tooltip"><i class="icon-trash icon-white"></i></button>
								';
return $output50;
};
$viewHelper57 = $self->getViewHelper('$viewHelper57', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper57->setArguments($arguments48);
$viewHelper57->setRenderingContext($renderingContext);
$viewHelper57->setRenderChildrenClosure($renderChildrenClosure49);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output47 .= $viewHelper57->initializeArgumentsAndRender();

$output47 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments58 = array();
$renderChildrenClosure59 = function() use ($renderingContext, $self) {
$output60 = '';

$output60 .= '
									<button class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments61 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments62 = array();
$arguments62['id'] = 'users.tooltip.delete';
$arguments62['source'] = 'Modules';
$arguments62['package'] = 'TYPO3.Neos';
$arguments62['value'] = NULL;
$arguments62['arguments'] = array (
);
$arguments62['quantity'] = NULL;
$arguments62['languageIdentifier'] = NULL;
$renderChildrenClosure63 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper64 = $self->getViewHelper('$viewHelper64', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper64->setArguments($arguments62);
$viewHelper64->setRenderingContext($renderingContext);
$viewHelper64->setRenderChildrenClosure($renderChildrenClosure63);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments61['value'] = $viewHelper64->initializeArgumentsAndRender();
$arguments61['keepQuotes'] = false;
$arguments61['encoding'] = 'UTF-8';
$arguments61['doubleEncode'] = true;
$renderChildrenClosure65 = function() use ($renderingContext, $self) {
return NULL;
};
$value66 = ($arguments61['value'] !== NULL ? $arguments61['value'] : $renderChildrenClosure65());

$output60 .= !is_string($value66) && !(is_object($value66) && method_exists($value66, '__toString')) ? $value66 : htmlspecialchars($value66, ($arguments61['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments61['encoding'], $arguments61['doubleEncode']);

$output60 .= '" data-toggle="modal" href="#user-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments67 = array();
$arguments67['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'index', $renderingContext);
$arguments67['keepQuotes'] = false;
$arguments67['encoding'] = 'UTF-8';
$arguments67['doubleEncode'] = true;
$renderChildrenClosure68 = function() use ($renderingContext, $self) {
return NULL;
};
$value69 = ($arguments67['value'] !== NULL ? $arguments67['value'] : $renderChildrenClosure68());

$output60 .= !is_string($value69) && !(is_object($value69) && method_exists($value69, '__toString')) ? $value69 : htmlspecialchars($value69, ($arguments67['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments67['encoding'], $arguments67['doubleEncode']);

$output60 .= '" data-neos-toggle="tooltip">
										<i class="icon-trash icon-white"></i>
									</button>
									<div class="neos-hide" id="user-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments70 = array();
$arguments70['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'index', $renderingContext);
$arguments70['keepQuotes'] = false;
$arguments70['encoding'] = 'UTF-8';
$arguments70['doubleEncode'] = true;
$renderChildrenClosure71 = function() use ($renderingContext, $self) {
return NULL;
};
$value72 = ($arguments70['value'] !== NULL ? $arguments70['value'] : $renderChildrenClosure71());

$output60 .= !is_string($value72) && !(is_object($value72) && method_exists($value72, '__toString')) ? $value72 : htmlspecialchars($value72, ($arguments70['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments70['encoding'], $arguments70['doubleEncode']);

$output60 .= '">
										<div class="neos-modal-centered">
											<div class="neos-modal-content">
												<div class="neos-modal-header">
													<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
													<div class="neos-header">
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments73 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments74 = array();
$arguments74['id'] = 'users.message.reallyDelete';
// Rendering Array
$array75 = array();
$array75['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user.name', $renderingContext);
$arguments74['arguments'] = $array75;
$arguments74['source'] = 'Modules';
$arguments74['package'] = 'TYPO3.Neos';
$arguments74['value'] = NULL;
$arguments74['quantity'] = NULL;
$arguments74['languageIdentifier'] = NULL;
$renderChildrenClosure76 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper77 = $self->getViewHelper('$viewHelper77', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper77->setArguments($arguments74);
$viewHelper77->setRenderingContext($renderingContext);
$viewHelper77->setRenderChildrenClosure($renderChildrenClosure76);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments73['value'] = $viewHelper77->initializeArgumentsAndRender();
$arguments73['keepQuotes'] = false;
$arguments73['encoding'] = 'UTF-8';
$arguments73['doubleEncode'] = true;
$renderChildrenClosure78 = function() use ($renderingContext, $self) {
return NULL;
};
$value79 = ($arguments73['value'] !== NULL ? $arguments73['value'] : $renderChildrenClosure78());

$output60 .= !is_string($value79) && !(is_object($value79) && method_exists($value79, '__toString')) ? $value79 : htmlspecialchars($value79, ($arguments73['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments73['encoding'], $arguments73['doubleEncode']);

$output60 .= '
													</div>
													<div>
														<div class="neos-subheader">
															<p>
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments80 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments81 = array();
$arguments81['id'] = 'users.message.willBeDeleted';
$arguments81['source'] = 'Modules';
$arguments81['package'] = 'TYPO3.Neos';
$arguments81['value'] = NULL;
$arguments81['arguments'] = array (
);
$arguments81['quantity'] = NULL;
$arguments81['languageIdentifier'] = NULL;
$renderChildrenClosure82 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper83 = $self->getViewHelper('$viewHelper83', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper83->setArguments($arguments81);
$viewHelper83->setRenderingContext($renderingContext);
$viewHelper83->setRenderChildrenClosure($renderChildrenClosure82);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments80['value'] = $viewHelper83->initializeArgumentsAndRender();
$arguments80['keepQuotes'] = false;
$arguments80['encoding'] = 'UTF-8';
$arguments80['doubleEncode'] = true;
$renderChildrenClosure84 = function() use ($renderingContext, $self) {
return NULL;
};
$value85 = ($arguments80['value'] !== NULL ? $arguments80['value'] : $renderChildrenClosure84());

$output60 .= !is_string($value85) && !(is_object($value85) && method_exists($value85, '__toString')) ? $value85 : htmlspecialchars($value85, ($arguments80['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments80['encoding'], $arguments80['doubleEncode']);

$output60 .= '<br/>
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments86 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments87 = array();
$arguments87['id'] = 'operationCannotBeUndone';
$arguments87['package'] = 'TYPO3.Neos';
$arguments87['value'] = NULL;
$arguments87['arguments'] = array (
);
$arguments87['source'] = 'Main';
$arguments87['quantity'] = NULL;
$arguments87['languageIdentifier'] = NULL;
$renderChildrenClosure88 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper89 = $self->getViewHelper('$viewHelper89', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper89->setArguments($arguments87);
$viewHelper89->setRenderingContext($renderingContext);
$viewHelper89->setRenderChildrenClosure($renderChildrenClosure88);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments86['value'] = $viewHelper89->initializeArgumentsAndRender();
$arguments86['keepQuotes'] = false;
$arguments86['encoding'] = 'UTF-8';
$arguments86['doubleEncode'] = true;
$renderChildrenClosure90 = function() use ($renderingContext, $self) {
return NULL;
};
$value91 = ($arguments86['value'] !== NULL ? $arguments86['value'] : $renderChildrenClosure90());

$output60 .= !is_string($value91) && !(is_object($value91) && method_exists($value91, '__toString')) ? $value91 : htmlspecialchars($value91, ($arguments86['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments86['encoding'], $arguments86['doubleEncode']);

$output60 .= '
															</p>
														</div>
													</div>
												</div>
												<div class="neos-modal-footer">
													<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments92 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments93 = array();
$arguments93['id'] = 'cancel';
$arguments93['value'] = 'Cancel';
$arguments93['arguments'] = array (
);
$arguments93['source'] = 'Main';
$arguments93['package'] = NULL;
$arguments93['quantity'] = NULL;
$arguments93['languageIdentifier'] = NULL;
$renderChildrenClosure94 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper95 = $self->getViewHelper('$viewHelper95', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper95->setArguments($arguments93);
$viewHelper95->setRenderingContext($renderingContext);
$viewHelper95->setRenderChildrenClosure($renderChildrenClosure94);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments92['value'] = $viewHelper95->initializeArgumentsAndRender();
$arguments92['keepQuotes'] = false;
$arguments92['encoding'] = 'UTF-8';
$arguments92['doubleEncode'] = true;
$renderChildrenClosure96 = function() use ($renderingContext, $self) {
return NULL;
};
$value97 = ($arguments92['value'] !== NULL ? $arguments92['value'] : $renderChildrenClosure96());

$output60 .= !is_string($value97) && !(is_object($value97) && method_exists($value97, '__toString')) ? $value97 : htmlspecialchars($value97, ($arguments92['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments92['encoding'], $arguments92['doubleEncode']);

$output60 .= '</a>
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments98 = array();
$arguments98['action'] = 'delete';
// Rendering Array
$array99 = array();
$array99['user'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user', $renderingContext);
$arguments98['arguments'] = $array99;
$arguments98['class'] = 'neos-inline';
$arguments98['additionalAttributes'] = NULL;
$arguments98['data'] = NULL;
$arguments98['controller'] = NULL;
$arguments98['package'] = NULL;
$arguments98['subpackage'] = NULL;
$arguments98['object'] = NULL;
$arguments98['section'] = '';
$arguments98['format'] = '';
$arguments98['additionalParams'] = array (
);
$arguments98['absolute'] = false;
$arguments98['addQueryString'] = false;
$arguments98['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments98['fieldNamePrefix'] = NULL;
$arguments98['actionUri'] = NULL;
$arguments98['objectName'] = NULL;
$arguments98['useParentRequest'] = false;
$arguments98['enctype'] = NULL;
$arguments98['method'] = NULL;
$arguments98['name'] = NULL;
$arguments98['onreset'] = NULL;
$arguments98['onsubmit'] = NULL;
$arguments98['dir'] = NULL;
$arguments98['id'] = NULL;
$arguments98['lang'] = NULL;
$arguments98['style'] = NULL;
$arguments98['title'] = NULL;
$arguments98['accesskey'] = NULL;
$arguments98['tabindex'] = NULL;
$arguments98['onclick'] = NULL;
$renderChildrenClosure100 = function() use ($renderingContext, $self) {
$output101 = '';

$output101 .= '
														<button type="submit" class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments102 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments103 = array();
$arguments103['id'] = 'users.tooltip.delete';
$arguments103['source'] = 'Modules';
$arguments103['package'] = 'TYPO3.Neos';
$arguments103['value'] = NULL;
$arguments103['arguments'] = array (
);
$arguments103['quantity'] = NULL;
$arguments103['languageIdentifier'] = NULL;
$renderChildrenClosure104 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper105 = $self->getViewHelper('$viewHelper105', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper105->setArguments($arguments103);
$viewHelper105->setRenderingContext($renderingContext);
$viewHelper105->setRenderChildrenClosure($renderChildrenClosure104);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments102['value'] = $viewHelper105->initializeArgumentsAndRender();
$arguments102['keepQuotes'] = false;
$arguments102['encoding'] = 'UTF-8';
$arguments102['doubleEncode'] = true;
$renderChildrenClosure106 = function() use ($renderingContext, $self) {
return NULL;
};
$value107 = ($arguments102['value'] !== NULL ? $arguments102['value'] : $renderChildrenClosure106());

$output101 .= !is_string($value107) && !(is_object($value107) && method_exists($value107, '__toString')) ? $value107 : htmlspecialchars($value107, ($arguments102['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments102['encoding'], $arguments102['doubleEncode']);

$output101 .= '">
															';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments108 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments109 = array();
$arguments109['id'] = 'users.message.confirmDelete';
$arguments109['source'] = 'Modules';
$arguments109['package'] = 'TYPO3.Neos';
$arguments109['value'] = NULL;
$arguments109['arguments'] = array (
);
$arguments109['quantity'] = NULL;
$arguments109['languageIdentifier'] = NULL;
$renderChildrenClosure110 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper111 = $self->getViewHelper('$viewHelper111', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper111->setArguments($arguments109);
$viewHelper111->setRenderingContext($renderingContext);
$viewHelper111->setRenderChildrenClosure($renderChildrenClosure110);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments108['value'] = $viewHelper111->initializeArgumentsAndRender();
$arguments108['keepQuotes'] = false;
$arguments108['encoding'] = 'UTF-8';
$arguments108['doubleEncode'] = true;
$renderChildrenClosure112 = function() use ($renderingContext, $self) {
return NULL;
};
$value113 = ($arguments108['value'] !== NULL ? $arguments108['value'] : $renderChildrenClosure112());

$output101 .= !is_string($value113) && !(is_object($value113) && method_exists($value113, '__toString')) ? $value113 : htmlspecialchars($value113, ($arguments108['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments108['encoding'], $arguments108['doubleEncode']);

$output101 .= '
														</button>
													';
return $output101;
};
$viewHelper114 = $self->getViewHelper('$viewHelper114', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper114->setArguments($arguments98);
$viewHelper114->setRenderingContext($renderingContext);
$viewHelper114->setRenderChildrenClosure($renderChildrenClosure100);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output60 .= $viewHelper114->initializeArgumentsAndRender();

$output60 .= '
												</div>
											</div>
										</div>
										<div class="neos-modal-backdrop neos-in"></div>
									</div>
								';
return $output60;
};
$viewHelper115 = $self->getViewHelper('$viewHelper115', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper115->setArguments($arguments58);
$viewHelper115->setRenderingContext($renderingContext);
$viewHelper115->setRenderChildrenClosure($renderChildrenClosure59);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output47 .= $viewHelper115->initializeArgumentsAndRender();

$output47 .= '
							';
return $output47;
};
$arguments45['__thenClosure'] = function() use ($renderingContext, $self) {
$output116 = '';

$output116 .= '
									<button class="neos-button neos-button-danger neos-disabled" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments117 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments118 = array();
$arguments118['id'] = 'users.tooltip.cannotDeleteYourself';
$arguments118['source'] = 'Modules';
$arguments118['package'] = 'TYPO3.Neos';
$arguments118['value'] = NULL;
$arguments118['arguments'] = array (
);
$arguments118['quantity'] = NULL;
$arguments118['languageIdentifier'] = NULL;
$renderChildrenClosure119 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper120 = $self->getViewHelper('$viewHelper120', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper120->setArguments($arguments118);
$viewHelper120->setRenderingContext($renderingContext);
$viewHelper120->setRenderChildrenClosure($renderChildrenClosure119);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments117['value'] = $viewHelper120->initializeArgumentsAndRender();
$arguments117['keepQuotes'] = false;
$arguments117['encoding'] = 'UTF-8';
$arguments117['doubleEncode'] = true;
$renderChildrenClosure121 = function() use ($renderingContext, $self) {
return NULL;
};
$value122 = ($arguments117['value'] !== NULL ? $arguments117['value'] : $renderChildrenClosure121());

$output116 .= !is_string($value122) && !(is_object($value122) && method_exists($value122, '__toString')) ? $value122 : htmlspecialchars($value122, ($arguments117['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments117['encoding'], $arguments117['doubleEncode']);

$output116 .= '" data-neos-toggle="tooltip"><i class="icon-trash icon-white"></i></button>
								';
return $output116;
};
$arguments45['__elseClosure'] = function() use ($renderingContext, $self) {
$output123 = '';

$output123 .= '
									<button class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments124 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments125 = array();
$arguments125['id'] = 'users.tooltip.delete';
$arguments125['source'] = 'Modules';
$arguments125['package'] = 'TYPO3.Neos';
$arguments125['value'] = NULL;
$arguments125['arguments'] = array (
);
$arguments125['quantity'] = NULL;
$arguments125['languageIdentifier'] = NULL;
$renderChildrenClosure126 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper127 = $self->getViewHelper('$viewHelper127', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper127->setArguments($arguments125);
$viewHelper127->setRenderingContext($renderingContext);
$viewHelper127->setRenderChildrenClosure($renderChildrenClosure126);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments124['value'] = $viewHelper127->initializeArgumentsAndRender();
$arguments124['keepQuotes'] = false;
$arguments124['encoding'] = 'UTF-8';
$arguments124['doubleEncode'] = true;
$renderChildrenClosure128 = function() use ($renderingContext, $self) {
return NULL;
};
$value129 = ($arguments124['value'] !== NULL ? $arguments124['value'] : $renderChildrenClosure128());

$output123 .= !is_string($value129) && !(is_object($value129) && method_exists($value129, '__toString')) ? $value129 : htmlspecialchars($value129, ($arguments124['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments124['encoding'], $arguments124['doubleEncode']);

$output123 .= '" data-toggle="modal" href="#user-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments130 = array();
$arguments130['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'index', $renderingContext);
$arguments130['keepQuotes'] = false;
$arguments130['encoding'] = 'UTF-8';
$arguments130['doubleEncode'] = true;
$renderChildrenClosure131 = function() use ($renderingContext, $self) {
return NULL;
};
$value132 = ($arguments130['value'] !== NULL ? $arguments130['value'] : $renderChildrenClosure131());

$output123 .= !is_string($value132) && !(is_object($value132) && method_exists($value132, '__toString')) ? $value132 : htmlspecialchars($value132, ($arguments130['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments130['encoding'], $arguments130['doubleEncode']);

$output123 .= '" data-neos-toggle="tooltip">
										<i class="icon-trash icon-white"></i>
									</button>
									<div class="neos-hide" id="user-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments133 = array();
$arguments133['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'index', $renderingContext);
$arguments133['keepQuotes'] = false;
$arguments133['encoding'] = 'UTF-8';
$arguments133['doubleEncode'] = true;
$renderChildrenClosure134 = function() use ($renderingContext, $self) {
return NULL;
};
$value135 = ($arguments133['value'] !== NULL ? $arguments133['value'] : $renderChildrenClosure134());

$output123 .= !is_string($value135) && !(is_object($value135) && method_exists($value135, '__toString')) ? $value135 : htmlspecialchars($value135, ($arguments133['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments133['encoding'], $arguments133['doubleEncode']);

$output123 .= '">
										<div class="neos-modal-centered">
											<div class="neos-modal-content">
												<div class="neos-modal-header">
													<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
													<div class="neos-header">
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments136 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments137 = array();
$arguments137['id'] = 'users.message.reallyDelete';
// Rendering Array
$array138 = array();
$array138['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user.name', $renderingContext);
$arguments137['arguments'] = $array138;
$arguments137['source'] = 'Modules';
$arguments137['package'] = 'TYPO3.Neos';
$arguments137['value'] = NULL;
$arguments137['quantity'] = NULL;
$arguments137['languageIdentifier'] = NULL;
$renderChildrenClosure139 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper140 = $self->getViewHelper('$viewHelper140', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper140->setArguments($arguments137);
$viewHelper140->setRenderingContext($renderingContext);
$viewHelper140->setRenderChildrenClosure($renderChildrenClosure139);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments136['value'] = $viewHelper140->initializeArgumentsAndRender();
$arguments136['keepQuotes'] = false;
$arguments136['encoding'] = 'UTF-8';
$arguments136['doubleEncode'] = true;
$renderChildrenClosure141 = function() use ($renderingContext, $self) {
return NULL;
};
$value142 = ($arguments136['value'] !== NULL ? $arguments136['value'] : $renderChildrenClosure141());

$output123 .= !is_string($value142) && !(is_object($value142) && method_exists($value142, '__toString')) ? $value142 : htmlspecialchars($value142, ($arguments136['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments136['encoding'], $arguments136['doubleEncode']);

$output123 .= '
													</div>
													<div>
														<div class="neos-subheader">
															<p>
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments143 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments144 = array();
$arguments144['id'] = 'users.message.willBeDeleted';
$arguments144['source'] = 'Modules';
$arguments144['package'] = 'TYPO3.Neos';
$arguments144['value'] = NULL;
$arguments144['arguments'] = array (
);
$arguments144['quantity'] = NULL;
$arguments144['languageIdentifier'] = NULL;
$renderChildrenClosure145 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper146 = $self->getViewHelper('$viewHelper146', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper146->setArguments($arguments144);
$viewHelper146->setRenderingContext($renderingContext);
$viewHelper146->setRenderChildrenClosure($renderChildrenClosure145);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments143['value'] = $viewHelper146->initializeArgumentsAndRender();
$arguments143['keepQuotes'] = false;
$arguments143['encoding'] = 'UTF-8';
$arguments143['doubleEncode'] = true;
$renderChildrenClosure147 = function() use ($renderingContext, $self) {
return NULL;
};
$value148 = ($arguments143['value'] !== NULL ? $arguments143['value'] : $renderChildrenClosure147());

$output123 .= !is_string($value148) && !(is_object($value148) && method_exists($value148, '__toString')) ? $value148 : htmlspecialchars($value148, ($arguments143['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments143['encoding'], $arguments143['doubleEncode']);

$output123 .= '<br/>
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments149 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments150 = array();
$arguments150['id'] = 'operationCannotBeUndone';
$arguments150['package'] = 'TYPO3.Neos';
$arguments150['value'] = NULL;
$arguments150['arguments'] = array (
);
$arguments150['source'] = 'Main';
$arguments150['quantity'] = NULL;
$arguments150['languageIdentifier'] = NULL;
$renderChildrenClosure151 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper152 = $self->getViewHelper('$viewHelper152', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper152->setArguments($arguments150);
$viewHelper152->setRenderingContext($renderingContext);
$viewHelper152->setRenderChildrenClosure($renderChildrenClosure151);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments149['value'] = $viewHelper152->initializeArgumentsAndRender();
$arguments149['keepQuotes'] = false;
$arguments149['encoding'] = 'UTF-8';
$arguments149['doubleEncode'] = true;
$renderChildrenClosure153 = function() use ($renderingContext, $self) {
return NULL;
};
$value154 = ($arguments149['value'] !== NULL ? $arguments149['value'] : $renderChildrenClosure153());

$output123 .= !is_string($value154) && !(is_object($value154) && method_exists($value154, '__toString')) ? $value154 : htmlspecialchars($value154, ($arguments149['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments149['encoding'], $arguments149['doubleEncode']);

$output123 .= '
															</p>
														</div>
													</div>
												</div>
												<div class="neos-modal-footer">
													<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments155 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments156 = array();
$arguments156['id'] = 'cancel';
$arguments156['value'] = 'Cancel';
$arguments156['arguments'] = array (
);
$arguments156['source'] = 'Main';
$arguments156['package'] = NULL;
$arguments156['quantity'] = NULL;
$arguments156['languageIdentifier'] = NULL;
$renderChildrenClosure157 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper158 = $self->getViewHelper('$viewHelper158', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper158->setArguments($arguments156);
$viewHelper158->setRenderingContext($renderingContext);
$viewHelper158->setRenderChildrenClosure($renderChildrenClosure157);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments155['value'] = $viewHelper158->initializeArgumentsAndRender();
$arguments155['keepQuotes'] = false;
$arguments155['encoding'] = 'UTF-8';
$arguments155['doubleEncode'] = true;
$renderChildrenClosure159 = function() use ($renderingContext, $self) {
return NULL;
};
$value160 = ($arguments155['value'] !== NULL ? $arguments155['value'] : $renderChildrenClosure159());

$output123 .= !is_string($value160) && !(is_object($value160) && method_exists($value160, '__toString')) ? $value160 : htmlspecialchars($value160, ($arguments155['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments155['encoding'], $arguments155['doubleEncode']);

$output123 .= '</a>
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments161 = array();
$arguments161['action'] = 'delete';
// Rendering Array
$array162 = array();
$array162['user'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user', $renderingContext);
$arguments161['arguments'] = $array162;
$arguments161['class'] = 'neos-inline';
$arguments161['additionalAttributes'] = NULL;
$arguments161['data'] = NULL;
$arguments161['controller'] = NULL;
$arguments161['package'] = NULL;
$arguments161['subpackage'] = NULL;
$arguments161['object'] = NULL;
$arguments161['section'] = '';
$arguments161['format'] = '';
$arguments161['additionalParams'] = array (
);
$arguments161['absolute'] = false;
$arguments161['addQueryString'] = false;
$arguments161['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments161['fieldNamePrefix'] = NULL;
$arguments161['actionUri'] = NULL;
$arguments161['objectName'] = NULL;
$arguments161['useParentRequest'] = false;
$arguments161['enctype'] = NULL;
$arguments161['method'] = NULL;
$arguments161['name'] = NULL;
$arguments161['onreset'] = NULL;
$arguments161['onsubmit'] = NULL;
$arguments161['dir'] = NULL;
$arguments161['id'] = NULL;
$arguments161['lang'] = NULL;
$arguments161['style'] = NULL;
$arguments161['title'] = NULL;
$arguments161['accesskey'] = NULL;
$arguments161['tabindex'] = NULL;
$arguments161['onclick'] = NULL;
$renderChildrenClosure163 = function() use ($renderingContext, $self) {
$output164 = '';

$output164 .= '
														<button type="submit" class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments165 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments166 = array();
$arguments166['id'] = 'users.tooltip.delete';
$arguments166['source'] = 'Modules';
$arguments166['package'] = 'TYPO3.Neos';
$arguments166['value'] = NULL;
$arguments166['arguments'] = array (
);
$arguments166['quantity'] = NULL;
$arguments166['languageIdentifier'] = NULL;
$renderChildrenClosure167 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper168 = $self->getViewHelper('$viewHelper168', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper168->setArguments($arguments166);
$viewHelper168->setRenderingContext($renderingContext);
$viewHelper168->setRenderChildrenClosure($renderChildrenClosure167);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments165['value'] = $viewHelper168->initializeArgumentsAndRender();
$arguments165['keepQuotes'] = false;
$arguments165['encoding'] = 'UTF-8';
$arguments165['doubleEncode'] = true;
$renderChildrenClosure169 = function() use ($renderingContext, $self) {
return NULL;
};
$value170 = ($arguments165['value'] !== NULL ? $arguments165['value'] : $renderChildrenClosure169());

$output164 .= !is_string($value170) && !(is_object($value170) && method_exists($value170, '__toString')) ? $value170 : htmlspecialchars($value170, ($arguments165['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments165['encoding'], $arguments165['doubleEncode']);

$output164 .= '">
															';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments171 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments172 = array();
$arguments172['id'] = 'users.message.confirmDelete';
$arguments172['source'] = 'Modules';
$arguments172['package'] = 'TYPO3.Neos';
$arguments172['value'] = NULL;
$arguments172['arguments'] = array (
);
$arguments172['quantity'] = NULL;
$arguments172['languageIdentifier'] = NULL;
$renderChildrenClosure173 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper174 = $self->getViewHelper('$viewHelper174', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper174->setArguments($arguments172);
$viewHelper174->setRenderingContext($renderingContext);
$viewHelper174->setRenderChildrenClosure($renderChildrenClosure173);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments171['value'] = $viewHelper174->initializeArgumentsAndRender();
$arguments171['keepQuotes'] = false;
$arguments171['encoding'] = 'UTF-8';
$arguments171['doubleEncode'] = true;
$renderChildrenClosure175 = function() use ($renderingContext, $self) {
return NULL;
};
$value176 = ($arguments171['value'] !== NULL ? $arguments171['value'] : $renderChildrenClosure175());

$output164 .= !is_string($value176) && !(is_object($value176) && method_exists($value176, '__toString')) ? $value176 : htmlspecialchars($value176, ($arguments171['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments171['encoding'], $arguments171['doubleEncode']);

$output164 .= '
														</button>
													';
return $output164;
};
$viewHelper177 = $self->getViewHelper('$viewHelper177', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper177->setArguments($arguments161);
$viewHelper177->setRenderingContext($renderingContext);
$viewHelper177->setRenderChildrenClosure($renderChildrenClosure163);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output123 .= $viewHelper177->initializeArgumentsAndRender();

$output123 .= '
												</div>
											</div>
										</div>
										<div class="neos-modal-backdrop neos-in"></div>
									</div>
								';
return $output123;
};
$viewHelper178 = $self->getViewHelper('$viewHelper178', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper178->setArguments($arguments45);
$viewHelper178->setRenderingContext($renderingContext);
$viewHelper178->setRenderChildrenClosure($renderChildrenClosure46);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output15 .= $viewHelper178->initializeArgumentsAndRender();

$output15 .= '
						</div>
					</td>
				</tr>
			';
return $output15;
};

$output0 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments13, $renderChildrenClosure14, $renderingContext);

$output0 .= '
		</table>
	</div>
	<div class="neos-footer">
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments179 = array();
$arguments179['action'] = 'new';
$arguments179['class'] = 'neos-button neos-button-primary';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments180 = array();
$arguments180['id'] = 'users.actions.new.label';
$arguments180['source'] = 'Modules';
$arguments180['package'] = 'TYPO3.Neos';
$arguments180['value'] = NULL;
$arguments180['arguments'] = array (
);
$arguments180['quantity'] = NULL;
$arguments180['languageIdentifier'] = NULL;
$renderChildrenClosure181 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper182 = $self->getViewHelper('$viewHelper182', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper182->setArguments($arguments180);
$viewHelper182->setRenderingContext($renderingContext);
$viewHelper182->setRenderChildrenClosure($renderChildrenClosure181);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments179['title'] = $viewHelper182->initializeArgumentsAndRender();
$arguments179['additionalAttributes'] = NULL;
$arguments179['data'] = NULL;
$arguments179['arguments'] = array (
);
$arguments179['controller'] = NULL;
$arguments179['package'] = NULL;
$arguments179['subpackage'] = NULL;
$arguments179['section'] = '';
$arguments179['format'] = '';
$arguments179['additionalParams'] = array (
);
$arguments179['addQueryString'] = false;
$arguments179['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments179['useParentRequest'] = false;
$arguments179['absolute'] = true;
$arguments179['dir'] = NULL;
$arguments179['id'] = NULL;
$arguments179['lang'] = NULL;
$arguments179['style'] = NULL;
$arguments179['accesskey'] = NULL;
$arguments179['tabindex'] = NULL;
$arguments179['onclick'] = NULL;
$arguments179['name'] = NULL;
$arguments179['rel'] = NULL;
$arguments179['rev'] = NULL;
$arguments179['target'] = NULL;
$renderChildrenClosure183 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments184 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments185 = array();
$arguments185['id'] = 'users.actions.new.label';
$arguments185['source'] = 'Modules';
$arguments185['package'] = 'TYPO3.Neos';
$arguments185['value'] = NULL;
$arguments185['arguments'] = array (
);
$arguments185['quantity'] = NULL;
$arguments185['languageIdentifier'] = NULL;
$renderChildrenClosure186 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper187 = $self->getViewHelper('$viewHelper187', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper187->setArguments($arguments185);
$viewHelper187->setRenderingContext($renderingContext);
$viewHelper187->setRenderChildrenClosure($renderChildrenClosure186);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments184['value'] = $viewHelper187->initializeArgumentsAndRender();
$arguments184['keepQuotes'] = false;
$arguments184['encoding'] = 'UTF-8';
$arguments184['doubleEncode'] = true;
$renderChildrenClosure188 = function() use ($renderingContext, $self) {
return NULL;
};
$value189 = ($arguments184['value'] !== NULL ? $arguments184['value'] : $renderChildrenClosure188());
return !is_string($value189) && !(is_object($value189) && method_exists($value189, '__toString')) ? $value189 : htmlspecialchars($value189, ($arguments184['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments184['encoding'], $arguments184['doubleEncode']);
};
$viewHelper190 = $self->getViewHelper('$viewHelper190', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper190->setArguments($arguments179);
$viewHelper190->setRenderingContext($renderingContext);
$viewHelper190->setRenderChildrenClosure($renderChildrenClosure183);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output0 .= $viewHelper190->initializeArgumentsAndRender();

$output0 .= '
	</div>
';

return $output0;
}
/**
 * Main Render function
 */
public function render(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output191 = '';

$output191 .= '
';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments192 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\LayoutViewHelper
$arguments193 = array();
$arguments193['name'] = 'BackendSubModule';
$renderChildrenClosure194 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper195 = $self->getViewHelper('$viewHelper195', $renderingContext, 'TYPO3\Fluid\ViewHelpers\LayoutViewHelper');
$viewHelper195->setArguments($arguments193);
$viewHelper195->setRenderingContext($renderingContext);
$viewHelper195->setRenderChildrenClosure($renderChildrenClosure194);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\LayoutViewHelper
$arguments192['value'] = $viewHelper195->initializeArgumentsAndRender();
$arguments192['keepQuotes'] = false;
$arguments192['encoding'] = 'UTF-8';
$arguments192['doubleEncode'] = true;
$renderChildrenClosure196 = function() use ($renderingContext, $self) {
return NULL;
};
$value197 = ($arguments192['value'] !== NULL ? $arguments192['value'] : $renderChildrenClosure196());

$output191 .= !is_string($value197) && !(is_object($value197) && method_exists($value197, '__toString')) ? $value197 : htmlspecialchars($value197, ($arguments192['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments192['encoding'], $arguments192['doubleEncode']);

$output191 .= '

';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\SectionViewHelper
$arguments198 = array();
$arguments198['name'] = 'content';
$renderChildrenClosure199 = function() use ($renderingContext, $self) {
$output200 = '';

$output200 .= '
	<div class="neos-row-fluid">
		<table class="neos-table">
			<thead>
				<tr>
					<th>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments201 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments202 = array();
$arguments202['id'] = 'users.name';
$arguments202['source'] = 'Modules';
$arguments202['package'] = 'TYPO3.Neos';
$arguments202['value'] = NULL;
$arguments202['arguments'] = array (
);
$arguments202['quantity'] = NULL;
$arguments202['languageIdentifier'] = NULL;
$renderChildrenClosure203 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper204 = $self->getViewHelper('$viewHelper204', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper204->setArguments($arguments202);
$viewHelper204->setRenderingContext($renderingContext);
$viewHelper204->setRenderChildrenClosure($renderChildrenClosure203);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments201['value'] = $viewHelper204->initializeArgumentsAndRender();
$arguments201['keepQuotes'] = false;
$arguments201['encoding'] = 'UTF-8';
$arguments201['doubleEncode'] = true;
$renderChildrenClosure205 = function() use ($renderingContext, $self) {
return NULL;
};
$value206 = ($arguments201['value'] !== NULL ? $arguments201['value'] : $renderChildrenClosure205());

$output200 .= !is_string($value206) && !(is_object($value206) && method_exists($value206, '__toString')) ? $value206 : htmlspecialchars($value206, ($arguments201['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments201['encoding'], $arguments201['doubleEncode']);

$output200 .= '</th>
					<th>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments207 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments208 = array();
$arguments208['id'] = 'users.accountsAndRoles';
$arguments208['source'] = 'Modules';
$arguments208['package'] = 'TYPO3.Neos';
$arguments208['value'] = NULL;
$arguments208['arguments'] = array (
);
$arguments208['quantity'] = NULL;
$arguments208['languageIdentifier'] = NULL;
$renderChildrenClosure209 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper210 = $self->getViewHelper('$viewHelper210', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper210->setArguments($arguments208);
$viewHelper210->setRenderingContext($renderingContext);
$viewHelper210->setRenderChildrenClosure($renderChildrenClosure209);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments207['value'] = $viewHelper210->initializeArgumentsAndRender();
$arguments207['keepQuotes'] = false;
$arguments207['encoding'] = 'UTF-8';
$arguments207['doubleEncode'] = true;
$renderChildrenClosure211 = function() use ($renderingContext, $self) {
return NULL;
};
$value212 = ($arguments207['value'] !== NULL ? $arguments207['value'] : $renderChildrenClosure211());

$output200 .= !is_string($value212) && !(is_object($value212) && method_exists($value212, '__toString')) ? $value212 : htmlspecialchars($value212, ($arguments207['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments207['encoding'], $arguments207['doubleEncode']);

$output200 .= '</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments213 = array();
$arguments213['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'users', $renderingContext);
$arguments213['key'] = 'index';
$arguments213['as'] = 'user';
$arguments213['reverse'] = false;
$arguments213['iteration'] = NULL;
$renderChildrenClosure214 = function() use ($renderingContext, $self) {
$output215 = '';

$output215 .= '
				<tr>
					<td>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments216 = array();
$arguments216['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user.name.fullName', $renderingContext);
$arguments216['keepQuotes'] = false;
$arguments216['encoding'] = 'UTF-8';
$arguments216['doubleEncode'] = true;
$renderChildrenClosure217 = function() use ($renderingContext, $self) {
return NULL;
};
$value218 = ($arguments216['value'] !== NULL ? $arguments216['value'] : $renderChildrenClosure217());

$output215 .= !is_string($value218) && !(is_object($value218) && method_exists($value218, '__toString')) ? $value218 : htmlspecialchars($value218, ($arguments216['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments216['encoding'], $arguments216['doubleEncode']);

$output215 .= '</td>
					<td>
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments219 = array();
$arguments219['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user.accounts', $renderingContext);
$arguments219['as'] = 'account';
$arguments219['key'] = '';
$arguments219['reverse'] = false;
$arguments219['iteration'] = NULL;
$renderChildrenClosure220 = function() use ($renderingContext, $self) {
$output221 = '';

$output221 .= '
							<i class="icon-user icon-white"></i>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments222 = array();
$arguments222['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'account.accountIdentifier', $renderingContext);
$arguments222['keepQuotes'] = false;
$arguments222['encoding'] = 'UTF-8';
$arguments222['doubleEncode'] = true;
$renderChildrenClosure223 = function() use ($renderingContext, $self) {
return NULL;
};
$value224 = ($arguments222['value'] !== NULL ? $arguments222['value'] : $renderChildrenClosure223());

$output221 .= !is_string($value224) && !(is_object($value224) && method_exists($value224, '__toString')) ? $value224 : htmlspecialchars($value224, ($arguments222['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments222['encoding'], $arguments222['doubleEncode']);

$output221 .= ' ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper
$arguments225 = array();
$arguments225['partial'] = 'Module/Shared/Roles';
// Rendering Array
$array226 = array();
$array226['roles'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'account.roles', $renderingContext);
$arguments225['arguments'] = $array226;
$arguments225['section'] = NULL;
$arguments225['optional'] = false;
$renderChildrenClosure227 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper228 = $self->getViewHelper('$viewHelper228', $renderingContext, 'TYPO3\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper228->setArguments($arguments225);
$viewHelper228->setRenderingContext($renderingContext);
$viewHelper228->setRenderChildrenClosure($renderChildrenClosure227);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper

$output221 .= $viewHelper228->initializeArgumentsAndRender();

$output221 .= '
						';
return $output221;
};

$output215 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments219, $renderChildrenClosure220, $renderingContext);

$output215 .= '
					</td>
					<td class="neos-action">
						<div class="neos-pull-right">
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments229 = array();
$arguments229['action'] = 'show';
// Rendering Array
$array230 = array();
$array230['user'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user', $renderingContext);
$arguments229['arguments'] = $array230;
$arguments229['class'] = 'neos-button neos-button-primary';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments231 = array();
$arguments231['id'] = 'users.tooltip.view';
$arguments231['source'] = 'Modules';
$arguments231['package'] = 'TYPO3.Neos';
$arguments231['value'] = NULL;
$arguments231['arguments'] = array (
);
$arguments231['quantity'] = NULL;
$arguments231['languageIdentifier'] = NULL;
$renderChildrenClosure232 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper233 = $self->getViewHelper('$viewHelper233', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper233->setArguments($arguments231);
$viewHelper233->setRenderingContext($renderingContext);
$viewHelper233->setRenderChildrenClosure($renderChildrenClosure232);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments229['title'] = $viewHelper233->initializeArgumentsAndRender();
// Rendering Array
$array234 = array();
$array234['data-neos-toggle'] = 'tooltip';
$arguments229['additionalAttributes'] = $array234;
$arguments229['data'] = NULL;
$arguments229['controller'] = NULL;
$arguments229['package'] = NULL;
$arguments229['subpackage'] = NULL;
$arguments229['section'] = '';
$arguments229['format'] = '';
$arguments229['additionalParams'] = array (
);
$arguments229['addQueryString'] = false;
$arguments229['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments229['useParentRequest'] = false;
$arguments229['absolute'] = true;
$arguments229['dir'] = NULL;
$arguments229['id'] = NULL;
$arguments229['lang'] = NULL;
$arguments229['style'] = NULL;
$arguments229['accesskey'] = NULL;
$arguments229['tabindex'] = NULL;
$arguments229['onclick'] = NULL;
$arguments229['name'] = NULL;
$arguments229['rel'] = NULL;
$arguments229['rev'] = NULL;
$arguments229['target'] = NULL;
$renderChildrenClosure235 = function() use ($renderingContext, $self) {
return '
								<i class="icon-info-sign icon-white"></i>
							';
};
$viewHelper236 = $self->getViewHelper('$viewHelper236', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper236->setArguments($arguments229);
$viewHelper236->setRenderingContext($renderingContext);
$viewHelper236->setRenderChildrenClosure($renderChildrenClosure235);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output215 .= $viewHelper236->initializeArgumentsAndRender();

$output215 .= '
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments237 = array();
$arguments237['action'] = 'edit';
// Rendering Array
$array238 = array();
$array238['user'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user', $renderingContext);
$arguments237['arguments'] = $array238;
$arguments237['class'] = 'neos-button neos-button-primary';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments239 = array();
$arguments239['id'] = 'users.tooltip.edit';
$arguments239['source'] = 'Modules';
$arguments239['package'] = 'TYPO3.Neos';
$arguments239['value'] = NULL;
$arguments239['arguments'] = array (
);
$arguments239['quantity'] = NULL;
$arguments239['languageIdentifier'] = NULL;
$renderChildrenClosure240 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper241 = $self->getViewHelper('$viewHelper241', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper241->setArguments($arguments239);
$viewHelper241->setRenderingContext($renderingContext);
$viewHelper241->setRenderChildrenClosure($renderChildrenClosure240);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments237['title'] = $viewHelper241->initializeArgumentsAndRender();
// Rendering Array
$array242 = array();
$array242['data-neos-toggle'] = 'tooltip';
$arguments237['additionalAttributes'] = $array242;
$arguments237['data'] = NULL;
$arguments237['controller'] = NULL;
$arguments237['package'] = NULL;
$arguments237['subpackage'] = NULL;
$arguments237['section'] = '';
$arguments237['format'] = '';
$arguments237['additionalParams'] = array (
);
$arguments237['addQueryString'] = false;
$arguments237['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments237['useParentRequest'] = false;
$arguments237['absolute'] = true;
$arguments237['dir'] = NULL;
$arguments237['id'] = NULL;
$arguments237['lang'] = NULL;
$arguments237['style'] = NULL;
$arguments237['accesskey'] = NULL;
$arguments237['tabindex'] = NULL;
$arguments237['onclick'] = NULL;
$arguments237['name'] = NULL;
$arguments237['rel'] = NULL;
$arguments237['rev'] = NULL;
$arguments237['target'] = NULL;
$renderChildrenClosure243 = function() use ($renderingContext, $self) {
return '
								<i class="icon-pencil icon-white"></i>
							';
};
$viewHelper244 = $self->getViewHelper('$viewHelper244', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper244->setArguments($arguments237);
$viewHelper244->setRenderingContext($renderingContext);
$viewHelper244->setRenderChildrenClosure($renderChildrenClosure243);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output215 .= $viewHelper244->initializeArgumentsAndRender();

$output215 .= '

							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments245 = array();
// Rendering Boolean node
$arguments245['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'currentUser', $renderingContext), \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user', $renderingContext));
$arguments245['then'] = NULL;
$arguments245['else'] = NULL;
$renderChildrenClosure246 = function() use ($renderingContext, $self) {
$output247 = '';

$output247 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments248 = array();
$renderChildrenClosure249 = function() use ($renderingContext, $self) {
$output250 = '';

$output250 .= '
									<button class="neos-button neos-button-danger neos-disabled" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments251 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments252 = array();
$arguments252['id'] = 'users.tooltip.cannotDeleteYourself';
$arguments252['source'] = 'Modules';
$arguments252['package'] = 'TYPO3.Neos';
$arguments252['value'] = NULL;
$arguments252['arguments'] = array (
);
$arguments252['quantity'] = NULL;
$arguments252['languageIdentifier'] = NULL;
$renderChildrenClosure253 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper254 = $self->getViewHelper('$viewHelper254', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper254->setArguments($arguments252);
$viewHelper254->setRenderingContext($renderingContext);
$viewHelper254->setRenderChildrenClosure($renderChildrenClosure253);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments251['value'] = $viewHelper254->initializeArgumentsAndRender();
$arguments251['keepQuotes'] = false;
$arguments251['encoding'] = 'UTF-8';
$arguments251['doubleEncode'] = true;
$renderChildrenClosure255 = function() use ($renderingContext, $self) {
return NULL;
};
$value256 = ($arguments251['value'] !== NULL ? $arguments251['value'] : $renderChildrenClosure255());

$output250 .= !is_string($value256) && !(is_object($value256) && method_exists($value256, '__toString')) ? $value256 : htmlspecialchars($value256, ($arguments251['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments251['encoding'], $arguments251['doubleEncode']);

$output250 .= '" data-neos-toggle="tooltip"><i class="icon-trash icon-white"></i></button>
								';
return $output250;
};
$viewHelper257 = $self->getViewHelper('$viewHelper257', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper257->setArguments($arguments248);
$viewHelper257->setRenderingContext($renderingContext);
$viewHelper257->setRenderChildrenClosure($renderChildrenClosure249);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output247 .= $viewHelper257->initializeArgumentsAndRender();

$output247 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments258 = array();
$renderChildrenClosure259 = function() use ($renderingContext, $self) {
$output260 = '';

$output260 .= '
									<button class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments261 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments262 = array();
$arguments262['id'] = 'users.tooltip.delete';
$arguments262['source'] = 'Modules';
$arguments262['package'] = 'TYPO3.Neos';
$arguments262['value'] = NULL;
$arguments262['arguments'] = array (
);
$arguments262['quantity'] = NULL;
$arguments262['languageIdentifier'] = NULL;
$renderChildrenClosure263 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper264 = $self->getViewHelper('$viewHelper264', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper264->setArguments($arguments262);
$viewHelper264->setRenderingContext($renderingContext);
$viewHelper264->setRenderChildrenClosure($renderChildrenClosure263);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments261['value'] = $viewHelper264->initializeArgumentsAndRender();
$arguments261['keepQuotes'] = false;
$arguments261['encoding'] = 'UTF-8';
$arguments261['doubleEncode'] = true;
$renderChildrenClosure265 = function() use ($renderingContext, $self) {
return NULL;
};
$value266 = ($arguments261['value'] !== NULL ? $arguments261['value'] : $renderChildrenClosure265());

$output260 .= !is_string($value266) && !(is_object($value266) && method_exists($value266, '__toString')) ? $value266 : htmlspecialchars($value266, ($arguments261['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments261['encoding'], $arguments261['doubleEncode']);

$output260 .= '" data-toggle="modal" href="#user-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments267 = array();
$arguments267['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'index', $renderingContext);
$arguments267['keepQuotes'] = false;
$arguments267['encoding'] = 'UTF-8';
$arguments267['doubleEncode'] = true;
$renderChildrenClosure268 = function() use ($renderingContext, $self) {
return NULL;
};
$value269 = ($arguments267['value'] !== NULL ? $arguments267['value'] : $renderChildrenClosure268());

$output260 .= !is_string($value269) && !(is_object($value269) && method_exists($value269, '__toString')) ? $value269 : htmlspecialchars($value269, ($arguments267['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments267['encoding'], $arguments267['doubleEncode']);

$output260 .= '" data-neos-toggle="tooltip">
										<i class="icon-trash icon-white"></i>
									</button>
									<div class="neos-hide" id="user-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments270 = array();
$arguments270['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'index', $renderingContext);
$arguments270['keepQuotes'] = false;
$arguments270['encoding'] = 'UTF-8';
$arguments270['doubleEncode'] = true;
$renderChildrenClosure271 = function() use ($renderingContext, $self) {
return NULL;
};
$value272 = ($arguments270['value'] !== NULL ? $arguments270['value'] : $renderChildrenClosure271());

$output260 .= !is_string($value272) && !(is_object($value272) && method_exists($value272, '__toString')) ? $value272 : htmlspecialchars($value272, ($arguments270['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments270['encoding'], $arguments270['doubleEncode']);

$output260 .= '">
										<div class="neos-modal-centered">
											<div class="neos-modal-content">
												<div class="neos-modal-header">
													<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
													<div class="neos-header">
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments273 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments274 = array();
$arguments274['id'] = 'users.message.reallyDelete';
// Rendering Array
$array275 = array();
$array275['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user.name', $renderingContext);
$arguments274['arguments'] = $array275;
$arguments274['source'] = 'Modules';
$arguments274['package'] = 'TYPO3.Neos';
$arguments274['value'] = NULL;
$arguments274['quantity'] = NULL;
$arguments274['languageIdentifier'] = NULL;
$renderChildrenClosure276 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper277 = $self->getViewHelper('$viewHelper277', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper277->setArguments($arguments274);
$viewHelper277->setRenderingContext($renderingContext);
$viewHelper277->setRenderChildrenClosure($renderChildrenClosure276);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments273['value'] = $viewHelper277->initializeArgumentsAndRender();
$arguments273['keepQuotes'] = false;
$arguments273['encoding'] = 'UTF-8';
$arguments273['doubleEncode'] = true;
$renderChildrenClosure278 = function() use ($renderingContext, $self) {
return NULL;
};
$value279 = ($arguments273['value'] !== NULL ? $arguments273['value'] : $renderChildrenClosure278());

$output260 .= !is_string($value279) && !(is_object($value279) && method_exists($value279, '__toString')) ? $value279 : htmlspecialchars($value279, ($arguments273['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments273['encoding'], $arguments273['doubleEncode']);

$output260 .= '
													</div>
													<div>
														<div class="neos-subheader">
															<p>
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments280 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments281 = array();
$arguments281['id'] = 'users.message.willBeDeleted';
$arguments281['source'] = 'Modules';
$arguments281['package'] = 'TYPO3.Neos';
$arguments281['value'] = NULL;
$arguments281['arguments'] = array (
);
$arguments281['quantity'] = NULL;
$arguments281['languageIdentifier'] = NULL;
$renderChildrenClosure282 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper283 = $self->getViewHelper('$viewHelper283', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper283->setArguments($arguments281);
$viewHelper283->setRenderingContext($renderingContext);
$viewHelper283->setRenderChildrenClosure($renderChildrenClosure282);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments280['value'] = $viewHelper283->initializeArgumentsAndRender();
$arguments280['keepQuotes'] = false;
$arguments280['encoding'] = 'UTF-8';
$arguments280['doubleEncode'] = true;
$renderChildrenClosure284 = function() use ($renderingContext, $self) {
return NULL;
};
$value285 = ($arguments280['value'] !== NULL ? $arguments280['value'] : $renderChildrenClosure284());

$output260 .= !is_string($value285) && !(is_object($value285) && method_exists($value285, '__toString')) ? $value285 : htmlspecialchars($value285, ($arguments280['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments280['encoding'], $arguments280['doubleEncode']);

$output260 .= '<br/>
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments286 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments287 = array();
$arguments287['id'] = 'operationCannotBeUndone';
$arguments287['package'] = 'TYPO3.Neos';
$arguments287['value'] = NULL;
$arguments287['arguments'] = array (
);
$arguments287['source'] = 'Main';
$arguments287['quantity'] = NULL;
$arguments287['languageIdentifier'] = NULL;
$renderChildrenClosure288 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper289 = $self->getViewHelper('$viewHelper289', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper289->setArguments($arguments287);
$viewHelper289->setRenderingContext($renderingContext);
$viewHelper289->setRenderChildrenClosure($renderChildrenClosure288);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments286['value'] = $viewHelper289->initializeArgumentsAndRender();
$arguments286['keepQuotes'] = false;
$arguments286['encoding'] = 'UTF-8';
$arguments286['doubleEncode'] = true;
$renderChildrenClosure290 = function() use ($renderingContext, $self) {
return NULL;
};
$value291 = ($arguments286['value'] !== NULL ? $arguments286['value'] : $renderChildrenClosure290());

$output260 .= !is_string($value291) && !(is_object($value291) && method_exists($value291, '__toString')) ? $value291 : htmlspecialchars($value291, ($arguments286['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments286['encoding'], $arguments286['doubleEncode']);

$output260 .= '
															</p>
														</div>
													</div>
												</div>
												<div class="neos-modal-footer">
													<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments292 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments293 = array();
$arguments293['id'] = 'cancel';
$arguments293['value'] = 'Cancel';
$arguments293['arguments'] = array (
);
$arguments293['source'] = 'Main';
$arguments293['package'] = NULL;
$arguments293['quantity'] = NULL;
$arguments293['languageIdentifier'] = NULL;
$renderChildrenClosure294 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper295 = $self->getViewHelper('$viewHelper295', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper295->setArguments($arguments293);
$viewHelper295->setRenderingContext($renderingContext);
$viewHelper295->setRenderChildrenClosure($renderChildrenClosure294);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments292['value'] = $viewHelper295->initializeArgumentsAndRender();
$arguments292['keepQuotes'] = false;
$arguments292['encoding'] = 'UTF-8';
$arguments292['doubleEncode'] = true;
$renderChildrenClosure296 = function() use ($renderingContext, $self) {
return NULL;
};
$value297 = ($arguments292['value'] !== NULL ? $arguments292['value'] : $renderChildrenClosure296());

$output260 .= !is_string($value297) && !(is_object($value297) && method_exists($value297, '__toString')) ? $value297 : htmlspecialchars($value297, ($arguments292['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments292['encoding'], $arguments292['doubleEncode']);

$output260 .= '</a>
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments298 = array();
$arguments298['action'] = 'delete';
// Rendering Array
$array299 = array();
$array299['user'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user', $renderingContext);
$arguments298['arguments'] = $array299;
$arguments298['class'] = 'neos-inline';
$arguments298['additionalAttributes'] = NULL;
$arguments298['data'] = NULL;
$arguments298['controller'] = NULL;
$arguments298['package'] = NULL;
$arguments298['subpackage'] = NULL;
$arguments298['object'] = NULL;
$arguments298['section'] = '';
$arguments298['format'] = '';
$arguments298['additionalParams'] = array (
);
$arguments298['absolute'] = false;
$arguments298['addQueryString'] = false;
$arguments298['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments298['fieldNamePrefix'] = NULL;
$arguments298['actionUri'] = NULL;
$arguments298['objectName'] = NULL;
$arguments298['useParentRequest'] = false;
$arguments298['enctype'] = NULL;
$arguments298['method'] = NULL;
$arguments298['name'] = NULL;
$arguments298['onreset'] = NULL;
$arguments298['onsubmit'] = NULL;
$arguments298['dir'] = NULL;
$arguments298['id'] = NULL;
$arguments298['lang'] = NULL;
$arguments298['style'] = NULL;
$arguments298['title'] = NULL;
$arguments298['accesskey'] = NULL;
$arguments298['tabindex'] = NULL;
$arguments298['onclick'] = NULL;
$renderChildrenClosure300 = function() use ($renderingContext, $self) {
$output301 = '';

$output301 .= '
														<button type="submit" class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments302 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments303 = array();
$arguments303['id'] = 'users.tooltip.delete';
$arguments303['source'] = 'Modules';
$arguments303['package'] = 'TYPO3.Neos';
$arguments303['value'] = NULL;
$arguments303['arguments'] = array (
);
$arguments303['quantity'] = NULL;
$arguments303['languageIdentifier'] = NULL;
$renderChildrenClosure304 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper305 = $self->getViewHelper('$viewHelper305', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper305->setArguments($arguments303);
$viewHelper305->setRenderingContext($renderingContext);
$viewHelper305->setRenderChildrenClosure($renderChildrenClosure304);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments302['value'] = $viewHelper305->initializeArgumentsAndRender();
$arguments302['keepQuotes'] = false;
$arguments302['encoding'] = 'UTF-8';
$arguments302['doubleEncode'] = true;
$renderChildrenClosure306 = function() use ($renderingContext, $self) {
return NULL;
};
$value307 = ($arguments302['value'] !== NULL ? $arguments302['value'] : $renderChildrenClosure306());

$output301 .= !is_string($value307) && !(is_object($value307) && method_exists($value307, '__toString')) ? $value307 : htmlspecialchars($value307, ($arguments302['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments302['encoding'], $arguments302['doubleEncode']);

$output301 .= '">
															';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments308 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments309 = array();
$arguments309['id'] = 'users.message.confirmDelete';
$arguments309['source'] = 'Modules';
$arguments309['package'] = 'TYPO3.Neos';
$arguments309['value'] = NULL;
$arguments309['arguments'] = array (
);
$arguments309['quantity'] = NULL;
$arguments309['languageIdentifier'] = NULL;
$renderChildrenClosure310 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper311 = $self->getViewHelper('$viewHelper311', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper311->setArguments($arguments309);
$viewHelper311->setRenderingContext($renderingContext);
$viewHelper311->setRenderChildrenClosure($renderChildrenClosure310);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments308['value'] = $viewHelper311->initializeArgumentsAndRender();
$arguments308['keepQuotes'] = false;
$arguments308['encoding'] = 'UTF-8';
$arguments308['doubleEncode'] = true;
$renderChildrenClosure312 = function() use ($renderingContext, $self) {
return NULL;
};
$value313 = ($arguments308['value'] !== NULL ? $arguments308['value'] : $renderChildrenClosure312());

$output301 .= !is_string($value313) && !(is_object($value313) && method_exists($value313, '__toString')) ? $value313 : htmlspecialchars($value313, ($arguments308['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments308['encoding'], $arguments308['doubleEncode']);

$output301 .= '
														</button>
													';
return $output301;
};
$viewHelper314 = $self->getViewHelper('$viewHelper314', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper314->setArguments($arguments298);
$viewHelper314->setRenderingContext($renderingContext);
$viewHelper314->setRenderChildrenClosure($renderChildrenClosure300);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output260 .= $viewHelper314->initializeArgumentsAndRender();

$output260 .= '
												</div>
											</div>
										</div>
										<div class="neos-modal-backdrop neos-in"></div>
									</div>
								';
return $output260;
};
$viewHelper315 = $self->getViewHelper('$viewHelper315', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper315->setArguments($arguments258);
$viewHelper315->setRenderingContext($renderingContext);
$viewHelper315->setRenderChildrenClosure($renderChildrenClosure259);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output247 .= $viewHelper315->initializeArgumentsAndRender();

$output247 .= '
							';
return $output247;
};
$arguments245['__thenClosure'] = function() use ($renderingContext, $self) {
$output316 = '';

$output316 .= '
									<button class="neos-button neos-button-danger neos-disabled" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments317 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments318 = array();
$arguments318['id'] = 'users.tooltip.cannotDeleteYourself';
$arguments318['source'] = 'Modules';
$arguments318['package'] = 'TYPO3.Neos';
$arguments318['value'] = NULL;
$arguments318['arguments'] = array (
);
$arguments318['quantity'] = NULL;
$arguments318['languageIdentifier'] = NULL;
$renderChildrenClosure319 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper320 = $self->getViewHelper('$viewHelper320', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper320->setArguments($arguments318);
$viewHelper320->setRenderingContext($renderingContext);
$viewHelper320->setRenderChildrenClosure($renderChildrenClosure319);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments317['value'] = $viewHelper320->initializeArgumentsAndRender();
$arguments317['keepQuotes'] = false;
$arguments317['encoding'] = 'UTF-8';
$arguments317['doubleEncode'] = true;
$renderChildrenClosure321 = function() use ($renderingContext, $self) {
return NULL;
};
$value322 = ($arguments317['value'] !== NULL ? $arguments317['value'] : $renderChildrenClosure321());

$output316 .= !is_string($value322) && !(is_object($value322) && method_exists($value322, '__toString')) ? $value322 : htmlspecialchars($value322, ($arguments317['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments317['encoding'], $arguments317['doubleEncode']);

$output316 .= '" data-neos-toggle="tooltip"><i class="icon-trash icon-white"></i></button>
								';
return $output316;
};
$arguments245['__elseClosure'] = function() use ($renderingContext, $self) {
$output323 = '';

$output323 .= '
									<button class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments324 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments325 = array();
$arguments325['id'] = 'users.tooltip.delete';
$arguments325['source'] = 'Modules';
$arguments325['package'] = 'TYPO3.Neos';
$arguments325['value'] = NULL;
$arguments325['arguments'] = array (
);
$arguments325['quantity'] = NULL;
$arguments325['languageIdentifier'] = NULL;
$renderChildrenClosure326 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper327 = $self->getViewHelper('$viewHelper327', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper327->setArguments($arguments325);
$viewHelper327->setRenderingContext($renderingContext);
$viewHelper327->setRenderChildrenClosure($renderChildrenClosure326);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments324['value'] = $viewHelper327->initializeArgumentsAndRender();
$arguments324['keepQuotes'] = false;
$arguments324['encoding'] = 'UTF-8';
$arguments324['doubleEncode'] = true;
$renderChildrenClosure328 = function() use ($renderingContext, $self) {
return NULL;
};
$value329 = ($arguments324['value'] !== NULL ? $arguments324['value'] : $renderChildrenClosure328());

$output323 .= !is_string($value329) && !(is_object($value329) && method_exists($value329, '__toString')) ? $value329 : htmlspecialchars($value329, ($arguments324['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments324['encoding'], $arguments324['doubleEncode']);

$output323 .= '" data-toggle="modal" href="#user-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments330 = array();
$arguments330['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'index', $renderingContext);
$arguments330['keepQuotes'] = false;
$arguments330['encoding'] = 'UTF-8';
$arguments330['doubleEncode'] = true;
$renderChildrenClosure331 = function() use ($renderingContext, $self) {
return NULL;
};
$value332 = ($arguments330['value'] !== NULL ? $arguments330['value'] : $renderChildrenClosure331());

$output323 .= !is_string($value332) && !(is_object($value332) && method_exists($value332, '__toString')) ? $value332 : htmlspecialchars($value332, ($arguments330['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments330['encoding'], $arguments330['doubleEncode']);

$output323 .= '" data-neos-toggle="tooltip">
										<i class="icon-trash icon-white"></i>
									</button>
									<div class="neos-hide" id="user-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments333 = array();
$arguments333['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'index', $renderingContext);
$arguments333['keepQuotes'] = false;
$arguments333['encoding'] = 'UTF-8';
$arguments333['doubleEncode'] = true;
$renderChildrenClosure334 = function() use ($renderingContext, $self) {
return NULL;
};
$value335 = ($arguments333['value'] !== NULL ? $arguments333['value'] : $renderChildrenClosure334());

$output323 .= !is_string($value335) && !(is_object($value335) && method_exists($value335, '__toString')) ? $value335 : htmlspecialchars($value335, ($arguments333['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments333['encoding'], $arguments333['doubleEncode']);

$output323 .= '">
										<div class="neos-modal-centered">
											<div class="neos-modal-content">
												<div class="neos-modal-header">
													<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
													<div class="neos-header">
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments336 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments337 = array();
$arguments337['id'] = 'users.message.reallyDelete';
// Rendering Array
$array338 = array();
$array338['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user.name', $renderingContext);
$arguments337['arguments'] = $array338;
$arguments337['source'] = 'Modules';
$arguments337['package'] = 'TYPO3.Neos';
$arguments337['value'] = NULL;
$arguments337['quantity'] = NULL;
$arguments337['languageIdentifier'] = NULL;
$renderChildrenClosure339 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper340 = $self->getViewHelper('$viewHelper340', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper340->setArguments($arguments337);
$viewHelper340->setRenderingContext($renderingContext);
$viewHelper340->setRenderChildrenClosure($renderChildrenClosure339);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments336['value'] = $viewHelper340->initializeArgumentsAndRender();
$arguments336['keepQuotes'] = false;
$arguments336['encoding'] = 'UTF-8';
$arguments336['doubleEncode'] = true;
$renderChildrenClosure341 = function() use ($renderingContext, $self) {
return NULL;
};
$value342 = ($arguments336['value'] !== NULL ? $arguments336['value'] : $renderChildrenClosure341());

$output323 .= !is_string($value342) && !(is_object($value342) && method_exists($value342, '__toString')) ? $value342 : htmlspecialchars($value342, ($arguments336['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments336['encoding'], $arguments336['doubleEncode']);

$output323 .= '
													</div>
													<div>
														<div class="neos-subheader">
															<p>
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments343 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments344 = array();
$arguments344['id'] = 'users.message.willBeDeleted';
$arguments344['source'] = 'Modules';
$arguments344['package'] = 'TYPO3.Neos';
$arguments344['value'] = NULL;
$arguments344['arguments'] = array (
);
$arguments344['quantity'] = NULL;
$arguments344['languageIdentifier'] = NULL;
$renderChildrenClosure345 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper346 = $self->getViewHelper('$viewHelper346', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper346->setArguments($arguments344);
$viewHelper346->setRenderingContext($renderingContext);
$viewHelper346->setRenderChildrenClosure($renderChildrenClosure345);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments343['value'] = $viewHelper346->initializeArgumentsAndRender();
$arguments343['keepQuotes'] = false;
$arguments343['encoding'] = 'UTF-8';
$arguments343['doubleEncode'] = true;
$renderChildrenClosure347 = function() use ($renderingContext, $self) {
return NULL;
};
$value348 = ($arguments343['value'] !== NULL ? $arguments343['value'] : $renderChildrenClosure347());

$output323 .= !is_string($value348) && !(is_object($value348) && method_exists($value348, '__toString')) ? $value348 : htmlspecialchars($value348, ($arguments343['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments343['encoding'], $arguments343['doubleEncode']);

$output323 .= '<br/>
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments349 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments350 = array();
$arguments350['id'] = 'operationCannotBeUndone';
$arguments350['package'] = 'TYPO3.Neos';
$arguments350['value'] = NULL;
$arguments350['arguments'] = array (
);
$arguments350['source'] = 'Main';
$arguments350['quantity'] = NULL;
$arguments350['languageIdentifier'] = NULL;
$renderChildrenClosure351 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper352 = $self->getViewHelper('$viewHelper352', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper352->setArguments($arguments350);
$viewHelper352->setRenderingContext($renderingContext);
$viewHelper352->setRenderChildrenClosure($renderChildrenClosure351);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments349['value'] = $viewHelper352->initializeArgumentsAndRender();
$arguments349['keepQuotes'] = false;
$arguments349['encoding'] = 'UTF-8';
$arguments349['doubleEncode'] = true;
$renderChildrenClosure353 = function() use ($renderingContext, $self) {
return NULL;
};
$value354 = ($arguments349['value'] !== NULL ? $arguments349['value'] : $renderChildrenClosure353());

$output323 .= !is_string($value354) && !(is_object($value354) && method_exists($value354, '__toString')) ? $value354 : htmlspecialchars($value354, ($arguments349['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments349['encoding'], $arguments349['doubleEncode']);

$output323 .= '
															</p>
														</div>
													</div>
												</div>
												<div class="neos-modal-footer">
													<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments355 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments356 = array();
$arguments356['id'] = 'cancel';
$arguments356['value'] = 'Cancel';
$arguments356['arguments'] = array (
);
$arguments356['source'] = 'Main';
$arguments356['package'] = NULL;
$arguments356['quantity'] = NULL;
$arguments356['languageIdentifier'] = NULL;
$renderChildrenClosure357 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper358 = $self->getViewHelper('$viewHelper358', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper358->setArguments($arguments356);
$viewHelper358->setRenderingContext($renderingContext);
$viewHelper358->setRenderChildrenClosure($renderChildrenClosure357);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments355['value'] = $viewHelper358->initializeArgumentsAndRender();
$arguments355['keepQuotes'] = false;
$arguments355['encoding'] = 'UTF-8';
$arguments355['doubleEncode'] = true;
$renderChildrenClosure359 = function() use ($renderingContext, $self) {
return NULL;
};
$value360 = ($arguments355['value'] !== NULL ? $arguments355['value'] : $renderChildrenClosure359());

$output323 .= !is_string($value360) && !(is_object($value360) && method_exists($value360, '__toString')) ? $value360 : htmlspecialchars($value360, ($arguments355['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments355['encoding'], $arguments355['doubleEncode']);

$output323 .= '</a>
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments361 = array();
$arguments361['action'] = 'delete';
// Rendering Array
$array362 = array();
$array362['user'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user', $renderingContext);
$arguments361['arguments'] = $array362;
$arguments361['class'] = 'neos-inline';
$arguments361['additionalAttributes'] = NULL;
$arguments361['data'] = NULL;
$arguments361['controller'] = NULL;
$arguments361['package'] = NULL;
$arguments361['subpackage'] = NULL;
$arguments361['object'] = NULL;
$arguments361['section'] = '';
$arguments361['format'] = '';
$arguments361['additionalParams'] = array (
);
$arguments361['absolute'] = false;
$arguments361['addQueryString'] = false;
$arguments361['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments361['fieldNamePrefix'] = NULL;
$arguments361['actionUri'] = NULL;
$arguments361['objectName'] = NULL;
$arguments361['useParentRequest'] = false;
$arguments361['enctype'] = NULL;
$arguments361['method'] = NULL;
$arguments361['name'] = NULL;
$arguments361['onreset'] = NULL;
$arguments361['onsubmit'] = NULL;
$arguments361['dir'] = NULL;
$arguments361['id'] = NULL;
$arguments361['lang'] = NULL;
$arguments361['style'] = NULL;
$arguments361['title'] = NULL;
$arguments361['accesskey'] = NULL;
$arguments361['tabindex'] = NULL;
$arguments361['onclick'] = NULL;
$renderChildrenClosure363 = function() use ($renderingContext, $self) {
$output364 = '';

$output364 .= '
														<button type="submit" class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments365 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments366 = array();
$arguments366['id'] = 'users.tooltip.delete';
$arguments366['source'] = 'Modules';
$arguments366['package'] = 'TYPO3.Neos';
$arguments366['value'] = NULL;
$arguments366['arguments'] = array (
);
$arguments366['quantity'] = NULL;
$arguments366['languageIdentifier'] = NULL;
$renderChildrenClosure367 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper368 = $self->getViewHelper('$viewHelper368', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper368->setArguments($arguments366);
$viewHelper368->setRenderingContext($renderingContext);
$viewHelper368->setRenderChildrenClosure($renderChildrenClosure367);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments365['value'] = $viewHelper368->initializeArgumentsAndRender();
$arguments365['keepQuotes'] = false;
$arguments365['encoding'] = 'UTF-8';
$arguments365['doubleEncode'] = true;
$renderChildrenClosure369 = function() use ($renderingContext, $self) {
return NULL;
};
$value370 = ($arguments365['value'] !== NULL ? $arguments365['value'] : $renderChildrenClosure369());

$output364 .= !is_string($value370) && !(is_object($value370) && method_exists($value370, '__toString')) ? $value370 : htmlspecialchars($value370, ($arguments365['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments365['encoding'], $arguments365['doubleEncode']);

$output364 .= '">
															';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments371 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments372 = array();
$arguments372['id'] = 'users.message.confirmDelete';
$arguments372['source'] = 'Modules';
$arguments372['package'] = 'TYPO3.Neos';
$arguments372['value'] = NULL;
$arguments372['arguments'] = array (
);
$arguments372['quantity'] = NULL;
$arguments372['languageIdentifier'] = NULL;
$renderChildrenClosure373 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper374 = $self->getViewHelper('$viewHelper374', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper374->setArguments($arguments372);
$viewHelper374->setRenderingContext($renderingContext);
$viewHelper374->setRenderChildrenClosure($renderChildrenClosure373);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments371['value'] = $viewHelper374->initializeArgumentsAndRender();
$arguments371['keepQuotes'] = false;
$arguments371['encoding'] = 'UTF-8';
$arguments371['doubleEncode'] = true;
$renderChildrenClosure375 = function() use ($renderingContext, $self) {
return NULL;
};
$value376 = ($arguments371['value'] !== NULL ? $arguments371['value'] : $renderChildrenClosure375());

$output364 .= !is_string($value376) && !(is_object($value376) && method_exists($value376, '__toString')) ? $value376 : htmlspecialchars($value376, ($arguments371['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments371['encoding'], $arguments371['doubleEncode']);

$output364 .= '
														</button>
													';
return $output364;
};
$viewHelper377 = $self->getViewHelper('$viewHelper377', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper377->setArguments($arguments361);
$viewHelper377->setRenderingContext($renderingContext);
$viewHelper377->setRenderChildrenClosure($renderChildrenClosure363);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output323 .= $viewHelper377->initializeArgumentsAndRender();

$output323 .= '
												</div>
											</div>
										</div>
										<div class="neos-modal-backdrop neos-in"></div>
									</div>
								';
return $output323;
};
$viewHelper378 = $self->getViewHelper('$viewHelper378', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper378->setArguments($arguments245);
$viewHelper378->setRenderingContext($renderingContext);
$viewHelper378->setRenderChildrenClosure($renderChildrenClosure246);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output215 .= $viewHelper378->initializeArgumentsAndRender();

$output215 .= '
						</div>
					</td>
				</tr>
			';
return $output215;
};

$output200 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments213, $renderChildrenClosure214, $renderingContext);

$output200 .= '
		</table>
	</div>
	<div class="neos-footer">
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments379 = array();
$arguments379['action'] = 'new';
$arguments379['class'] = 'neos-button neos-button-primary';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments380 = array();
$arguments380['id'] = 'users.actions.new.label';
$arguments380['source'] = 'Modules';
$arguments380['package'] = 'TYPO3.Neos';
$arguments380['value'] = NULL;
$arguments380['arguments'] = array (
);
$arguments380['quantity'] = NULL;
$arguments380['languageIdentifier'] = NULL;
$renderChildrenClosure381 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper382 = $self->getViewHelper('$viewHelper382', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper382->setArguments($arguments380);
$viewHelper382->setRenderingContext($renderingContext);
$viewHelper382->setRenderChildrenClosure($renderChildrenClosure381);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments379['title'] = $viewHelper382->initializeArgumentsAndRender();
$arguments379['additionalAttributes'] = NULL;
$arguments379['data'] = NULL;
$arguments379['arguments'] = array (
);
$arguments379['controller'] = NULL;
$arguments379['package'] = NULL;
$arguments379['subpackage'] = NULL;
$arguments379['section'] = '';
$arguments379['format'] = '';
$arguments379['additionalParams'] = array (
);
$arguments379['addQueryString'] = false;
$arguments379['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments379['useParentRequest'] = false;
$arguments379['absolute'] = true;
$arguments379['dir'] = NULL;
$arguments379['id'] = NULL;
$arguments379['lang'] = NULL;
$arguments379['style'] = NULL;
$arguments379['accesskey'] = NULL;
$arguments379['tabindex'] = NULL;
$arguments379['onclick'] = NULL;
$arguments379['name'] = NULL;
$arguments379['rel'] = NULL;
$arguments379['rev'] = NULL;
$arguments379['target'] = NULL;
$renderChildrenClosure383 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments384 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments385 = array();
$arguments385['id'] = 'users.actions.new.label';
$arguments385['source'] = 'Modules';
$arguments385['package'] = 'TYPO3.Neos';
$arguments385['value'] = NULL;
$arguments385['arguments'] = array (
);
$arguments385['quantity'] = NULL;
$arguments385['languageIdentifier'] = NULL;
$renderChildrenClosure386 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper387 = $self->getViewHelper('$viewHelper387', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper387->setArguments($arguments385);
$viewHelper387->setRenderingContext($renderingContext);
$viewHelper387->setRenderChildrenClosure($renderChildrenClosure386);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments384['value'] = $viewHelper387->initializeArgumentsAndRender();
$arguments384['keepQuotes'] = false;
$arguments384['encoding'] = 'UTF-8';
$arguments384['doubleEncode'] = true;
$renderChildrenClosure388 = function() use ($renderingContext, $self) {
return NULL;
};
$value389 = ($arguments384['value'] !== NULL ? $arguments384['value'] : $renderChildrenClosure388());
return !is_string($value389) && !(is_object($value389) && method_exists($value389, '__toString')) ? $value389 : htmlspecialchars($value389, ($arguments384['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments384['encoding'], $arguments384['doubleEncode']);
};
$viewHelper390 = $self->getViewHelper('$viewHelper390', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper390->setArguments($arguments379);
$viewHelper390->setRenderingContext($renderingContext);
$viewHelper390->setRenderChildrenClosure($renderChildrenClosure383);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output200 .= $viewHelper390->initializeArgumentsAndRender();

$output200 .= '
	</div>
';
return $output200;
};

$output191 .= '';

$output191 .= '
';

return $output191;
}


}
#0             114616    