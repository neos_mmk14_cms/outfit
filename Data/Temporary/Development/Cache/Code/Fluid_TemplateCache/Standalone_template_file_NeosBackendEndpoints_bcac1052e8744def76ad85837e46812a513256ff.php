<?php class FluidCache_Standalone_template_file_NeosBackendEndpoints_bcac1052e8744def76ad85837e46812a513256ff extends \TYPO3\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// TODO
	return new \TYPO3\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;

return NULL;
}
public function hasLayout() {
return FALSE;
}

/**
 * Main Render function
 */
public function render(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output0 = '';

$output0 .= '

<meta name="neos-username" content="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1 = array();
$arguments1['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'account.accountIdentifier', $renderingContext);
$arguments1['keepQuotes'] = false;
$arguments1['encoding'] = 'UTF-8';
$arguments1['doubleEncode'] = true;
$renderChildrenClosure2 = function() use ($renderingContext, $self) {
return NULL;
};
$value3 = ($arguments1['value'] !== NULL ? $arguments1['value'] : $renderChildrenClosure2());

$output0 .= !is_string($value3) && !(is_object($value3) && method_exists($value3, '__toString')) ? $value3 : htmlspecialchars($value3, ($arguments1['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1['encoding'], $arguments1['doubleEncode']);

$output0 .= '" />
<meta name="neos-workspace" content="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments4 = array();
$arguments4['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'node.context.workspace.name', $renderingContext);
$arguments4['keepQuotes'] = false;
$arguments4['encoding'] = 'UTF-8';
$arguments4['doubleEncode'] = true;
$renderChildrenClosure5 = function() use ($renderingContext, $self) {
return NULL;
};
$value6 = ($arguments4['value'] !== NULL ? $arguments4['value'] : $renderChildrenClosure5());

$output0 .= !is_string($value6) && !(is_object($value6) && method_exists($value6, '__toString')) ? $value6 : htmlspecialchars($value6, ($arguments4['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments4['encoding'], $arguments4['doubleEncode']);

$output0 .= '" />
<meta name="neos-csrf-token" content="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments7 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Security\CsrfTokenViewHelper
$arguments8 = array();
$renderChildrenClosure9 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper10 = $self->getViewHelper('$viewHelper10', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Security\CsrfTokenViewHelper');
$viewHelper10->setArguments($arguments8);
$viewHelper10->setRenderingContext($renderingContext);
$viewHelper10->setRenderChildrenClosure($renderChildrenClosure9);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Security\CsrfTokenViewHelper
$arguments7['value'] = $viewHelper10->initializeArgumentsAndRender();
$arguments7['keepQuotes'] = false;
$arguments7['encoding'] = 'UTF-8';
$arguments7['doubleEncode'] = true;
$renderChildrenClosure11 = function() use ($renderingContext, $self) {
return NULL;
};
$value12 = ($arguments7['value'] !== NULL ? $arguments7['value'] : $renderChildrenClosure11());

$output0 .= !is_string($value12) && !(is_object($value12) && method_exists($value12, '__toString')) ? $value12 : htmlspecialchars($value12, ($arguments7['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments7['encoding'], $arguments7['doubleEncode']);

$output0 .= '" />

<link rel="neos-site" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments13 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments14 = array();
$arguments14['action'] = 'show';
$arguments14['controller'] = 'Frontend\\Node';
$arguments14['package'] = 'TYPO3.Neos';
$arguments14['format'] = 'html';
// Rendering Array
$array15 = array();
$array15['node'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'node.context.currentSiteNode', $renderingContext);
$arguments14['arguments'] = $array15;
// Rendering Boolean node
$arguments14['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments14['subpackage'] = NULL;
$arguments14['section'] = '';
$arguments14['additionalParams'] = array (
);
$arguments14['addQueryString'] = false;
$arguments14['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments14['useParentRequest'] = false;
$renderChildrenClosure16 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper17 = $self->getViewHelper('$viewHelper17', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper17->setArguments($arguments14);
$viewHelper17->setRenderingContext($renderingContext);
$viewHelper17->setRenderChildrenClosure($renderChildrenClosure16);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments13['value'] = $viewHelper17->initializeArgumentsAndRender();
$arguments13['keepQuotes'] = false;
$arguments13['encoding'] = 'UTF-8';
$arguments13['doubleEncode'] = true;
$renderChildrenClosure18 = function() use ($renderingContext, $self) {
return NULL;
};
$value19 = ($arguments13['value'] !== NULL ? $arguments13['value'] : $renderChildrenClosure18());

$output0 .= !is_string($value19) && !(is_object($value19) && method_exists($value19, '__toString')) ? $value19 : htmlspecialchars($value19, ($arguments13['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments13['encoding'], $arguments13['doubleEncode']);

$output0 .= '" data-node-name="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments20 = array();
$arguments20['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'node.context.currentSiteNode.name', $renderingContext);
$arguments20['keepQuotes'] = false;
$arguments20['encoding'] = 'UTF-8';
$arguments20['doubleEncode'] = true;
$renderChildrenClosure21 = function() use ($renderingContext, $self) {
return NULL;
};
$value22 = ($arguments20['value'] !== NULL ? $arguments20['value'] : $renderChildrenClosure21());

$output0 .= !is_string($value22) && !(is_object($value22) && method_exists($value22, '__toString')) ? $value22 : htmlspecialchars($value22, ($arguments20['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments20['encoding'], $arguments20['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-nodes" type="application/vnd.typo3.neos.nodes" data-current-workspace="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments23 = array();
$arguments23['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'node.context.workspace.name', $renderingContext);
$arguments23['keepQuotes'] = false;
$arguments23['encoding'] = 'UTF-8';
$arguments23['doubleEncode'] = true;
$renderChildrenClosure24 = function() use ($renderingContext, $self) {
return NULL;
};
$value25 = ($arguments23['value'] !== NULL ? $arguments23['value'] : $renderChildrenClosure24());

$output0 .= !is_string($value25) && !(is_object($value25) && method_exists($value25, '__toString')) ? $value25 : htmlspecialchars($value25, ($arguments23['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments23['encoding'], $arguments23['doubleEncode']);

$output0 .= '" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments26 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments27 = array();
$arguments27['action'] = 'index';
$arguments27['controller'] = 'Service\\Nodes';
$arguments27['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments27['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments27['arguments'] = array (
);
$arguments27['subpackage'] = NULL;
$arguments27['section'] = '';
$arguments27['format'] = '';
$arguments27['additionalParams'] = array (
);
$arguments27['addQueryString'] = false;
$arguments27['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments27['useParentRequest'] = false;
$renderChildrenClosure28 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper29 = $self->getViewHelper('$viewHelper29', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper29->setArguments($arguments27);
$viewHelper29->setRenderingContext($renderingContext);
$viewHelper29->setRenderChildrenClosure($renderChildrenClosure28);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments26['value'] = $viewHelper29->initializeArgumentsAndRender();
$arguments26['keepQuotes'] = false;
$arguments26['encoding'] = 'UTF-8';
$arguments26['doubleEncode'] = true;
$renderChildrenClosure30 = function() use ($renderingContext, $self) {
return NULL;
};
$value31 = ($arguments26['value'] !== NULL ? $arguments26['value'] : $renderChildrenClosure30());

$output0 .= !is_string($value31) && !(is_object($value31) && method_exists($value31, '__toString')) ? $value31 : htmlspecialchars($value31, ($arguments26['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments26['encoding'], $arguments26['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-assets" type="application/vnd.typo3.neos.assets" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments32 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments33 = array();
$arguments33['action'] = 'index';
$arguments33['controller'] = 'Service\\Assets';
$arguments33['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments33['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments33['arguments'] = array (
);
$arguments33['subpackage'] = NULL;
$arguments33['section'] = '';
$arguments33['format'] = '';
$arguments33['additionalParams'] = array (
);
$arguments33['addQueryString'] = false;
$arguments33['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments33['useParentRequest'] = false;
$renderChildrenClosure34 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper35 = $self->getViewHelper('$viewHelper35', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper35->setArguments($arguments33);
$viewHelper35->setRenderingContext($renderingContext);
$viewHelper35->setRenderChildrenClosure($renderChildrenClosure34);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments32['value'] = $viewHelper35->initializeArgumentsAndRender();
$arguments32['keepQuotes'] = false;
$arguments32['encoding'] = 'UTF-8';
$arguments32['doubleEncode'] = true;
$renderChildrenClosure36 = function() use ($renderingContext, $self) {
return NULL;
};
$value37 = ($arguments32['value'] !== NULL ? $arguments32['value'] : $renderChildrenClosure36());

$output0 .= !is_string($value37) && !(is_object($value37) && method_exists($value37, '__toString')) ? $value37 : htmlspecialchars($value37, ($arguments32['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments32['encoding'], $arguments32['doubleEncode']);

$output0 .= '" />

';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\AliasViewHelper
$arguments38 = array();
// Rendering Array
$array39 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper
$arguments40 = array();
$renderChildrenClosure41 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper42 = $self->getViewHelper('$viewHelper42', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper');
$viewHelper42->setArguments($arguments40);
$viewHelper42->setRenderingContext($renderingContext);
$viewHelper42->setRenderChildrenClosure($renderChildrenClosure41);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper
$array39['configurationCacheIdentifier'] = $viewHelper42->initializeArgumentsAndRender();
$arguments38['map'] = $array39;
$renderChildrenClosure43 = function() use ($renderingContext, $self) {
$output44 = '';

$output44 .= '
	<link rel="neos-vieschema" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments45 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments46 = array();
$arguments46['action'] = 'vieSchema';
$arguments46['controller'] = 'Backend\\Schema';
$arguments46['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments46['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
// Rendering Array
$array47 = array();
$array47['version'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configurationCacheIdentifier', $renderingContext);
$arguments46['arguments'] = $array47;
$arguments46['subpackage'] = NULL;
$arguments46['section'] = '';
$arguments46['format'] = '';
$arguments46['additionalParams'] = array (
);
$arguments46['addQueryString'] = false;
$arguments46['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments46['useParentRequest'] = false;
$renderChildrenClosure48 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper49 = $self->getViewHelper('$viewHelper49', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper49->setArguments($arguments46);
$viewHelper49->setRenderingContext($renderingContext);
$viewHelper49->setRenderChildrenClosure($renderChildrenClosure48);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments45['value'] = $viewHelper49->initializeArgumentsAndRender();
$arguments45['keepQuotes'] = false;
$arguments45['encoding'] = 'UTF-8';
$arguments45['doubleEncode'] = true;
$renderChildrenClosure50 = function() use ($renderingContext, $self) {
return NULL;
};
$value51 = ($arguments45['value'] !== NULL ? $arguments45['value'] : $renderChildrenClosure50());

$output44 .= !is_string($value51) && !(is_object($value51) && method_exists($value51, '__toString')) ? $value51 : htmlspecialchars($value51, ($arguments45['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments45['encoding'], $arguments45['doubleEncode']);

$output44 .= '" />
	<link rel="neos-nodetypeschema" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments52 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments53 = array();
$arguments53['action'] = 'nodeTypeSchema';
$arguments53['controller'] = 'Backend\\Schema';
$arguments53['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments53['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
// Rendering Array
$array54 = array();
$array54['version'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configurationCacheIdentifier', $renderingContext);
$arguments53['arguments'] = $array54;
$arguments53['subpackage'] = NULL;
$arguments53['section'] = '';
$arguments53['format'] = '';
$arguments53['additionalParams'] = array (
);
$arguments53['addQueryString'] = false;
$arguments53['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments53['useParentRequest'] = false;
$renderChildrenClosure55 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper56 = $self->getViewHelper('$viewHelper56', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper56->setArguments($arguments53);
$viewHelper56->setRenderingContext($renderingContext);
$viewHelper56->setRenderChildrenClosure($renderChildrenClosure55);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments52['value'] = $viewHelper56->initializeArgumentsAndRender();
$arguments52['keepQuotes'] = false;
$arguments52['encoding'] = 'UTF-8';
$arguments52['doubleEncode'] = true;
$renderChildrenClosure57 = function() use ($renderingContext, $self) {
return NULL;
};
$value58 = ($arguments52['value'] !== NULL ? $arguments52['value'] : $renderChildrenClosure57());

$output44 .= !is_string($value58) && !(is_object($value58) && method_exists($value58, '__toString')) ? $value58 : htmlspecialchars($value58, ($arguments52['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments52['encoding'], $arguments52['doubleEncode']);

$output44 .= '" />
	<link rel="neos-menudata" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments59 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments60 = array();
$arguments60['action'] = 'index';
$arguments60['controller'] = 'Backend\\Menu';
$arguments60['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments60['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
// Rendering Array
$array61 = array();
$array61['version'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configurationCacheIdentifier', $renderingContext);
$arguments60['arguments'] = $array61;
$arguments60['subpackage'] = NULL;
$arguments60['section'] = '';
$arguments60['format'] = '';
$arguments60['additionalParams'] = array (
);
$arguments60['addQueryString'] = false;
$arguments60['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments60['useParentRequest'] = false;
$renderChildrenClosure62 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper63 = $self->getViewHelper('$viewHelper63', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper63->setArguments($arguments60);
$viewHelper63->setRenderingContext($renderingContext);
$viewHelper63->setRenderChildrenClosure($renderChildrenClosure62);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments59['value'] = $viewHelper63->initializeArgumentsAndRender();
$arguments59['keepQuotes'] = false;
$arguments59['encoding'] = 'UTF-8';
$arguments59['doubleEncode'] = true;
$renderChildrenClosure64 = function() use ($renderingContext, $self) {
return NULL;
};
$value65 = ($arguments59['value'] !== NULL ? $arguments59['value'] : $renderChildrenClosure64());

$output44 .= !is_string($value65) && !(is_object($value65) && method_exists($value65, '__toString')) ? $value65 : htmlspecialchars($value65, ($arguments59['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments59['encoding'], $arguments59['doubleEncode']);

$output44 .= '" />
	<link rel="neos-editpreviewdata" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments66 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments67 = array();
$arguments67['action'] = 'editPreview';
$arguments67['controller'] = 'Backend\\Settings';
$arguments67['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments67['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
// Rendering Array
$array68 = array();
$array68['version'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configurationCacheIdentifier', $renderingContext);
$arguments67['arguments'] = $array68;
$arguments67['subpackage'] = NULL;
$arguments67['section'] = '';
$arguments67['format'] = '';
$arguments67['additionalParams'] = array (
);
$arguments67['addQueryString'] = false;
$arguments67['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments67['useParentRequest'] = false;
$renderChildrenClosure69 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper70 = $self->getViewHelper('$viewHelper70', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper70->setArguments($arguments67);
$viewHelper70->setRenderingContext($renderingContext);
$viewHelper70->setRenderChildrenClosure($renderChildrenClosure69);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments66['value'] = $viewHelper70->initializeArgumentsAndRender();
$arguments66['keepQuotes'] = false;
$arguments66['encoding'] = 'UTF-8';
$arguments66['doubleEncode'] = true;
$renderChildrenClosure71 = function() use ($renderingContext, $self) {
return NULL;
};
$value72 = ($arguments66['value'] !== NULL ? $arguments66['value'] : $renderChildrenClosure71());

$output44 .= !is_string($value72) && !(is_object($value72) && method_exists($value72, '__toString')) ? $value72 : htmlspecialchars($value72, ($arguments66['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments66['encoding'], $arguments66['doubleEncode']);

$output44 .= '" />
	<link rel="neos-service-contentdimensions" type="application/vnd.typo3.neos.contentdimensions" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments73 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments74 = array();
$arguments74['action'] = 'index';
$arguments74['controller'] = 'Service\\ContentDimensions';
$arguments74['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments74['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
// Rendering Array
$array75 = array();
$array75['version'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configurationCacheIdentifier', $renderingContext);
$arguments74['arguments'] = $array75;
$arguments74['subpackage'] = NULL;
$arguments74['section'] = '';
$arguments74['format'] = '';
$arguments74['additionalParams'] = array (
);
$arguments74['addQueryString'] = false;
$arguments74['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments74['useParentRequest'] = false;
$renderChildrenClosure76 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper77 = $self->getViewHelper('$viewHelper77', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper77->setArguments($arguments74);
$viewHelper77->setRenderingContext($renderingContext);
$viewHelper77->setRenderChildrenClosure($renderChildrenClosure76);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments73['value'] = $viewHelper77->initializeArgumentsAndRender();
$arguments73['keepQuotes'] = false;
$arguments73['encoding'] = 'UTF-8';
$arguments73['doubleEncode'] = true;
$renderChildrenClosure78 = function() use ($renderingContext, $self) {
return NULL;
};
$value79 = ($arguments73['value'] !== NULL ? $arguments73['value'] : $renderChildrenClosure78());

$output44 .= !is_string($value79) && !(is_object($value79) && method_exists($value79, '__toString')) ? $value79 : htmlspecialchars($value79, ($arguments73['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments73['encoding'], $arguments73['doubleEncode']);

$output44 .= '" />
';
return $output44;
};
$viewHelper80 = $self->getViewHelper('$viewHelper80', $renderingContext, 'TYPO3\Fluid\ViewHelpers\AliasViewHelper');
$viewHelper80->setArguments($arguments38);
$viewHelper80->setRenderingContext($renderingContext);
$viewHelper80->setRenderChildrenClosure($renderChildrenClosure43);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\AliasViewHelper

$output0 .= $viewHelper80->initializeArgumentsAndRender();

$output0 .= '
<link rel="neos-service-workspaces" type="application/vnd.typo3.neos.workspaces" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments81 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments82 = array();
$arguments82['action'] = 'index';
$arguments82['controller'] = 'Service\\Workspaces';
$arguments82['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments82['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments82['arguments'] = array (
);
$arguments82['subpackage'] = NULL;
$arguments82['section'] = '';
$arguments82['format'] = '';
$arguments82['additionalParams'] = array (
);
$arguments82['addQueryString'] = false;
$arguments82['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments82['useParentRequest'] = false;
$renderChildrenClosure83 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper84 = $self->getViewHelper('$viewHelper84', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper84->setArguments($arguments82);
$viewHelper84->setRenderingContext($renderingContext);
$viewHelper84->setRenderChildrenClosure($renderChildrenClosure83);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments81['value'] = $viewHelper84->initializeArgumentsAndRender();
$arguments81['keepQuotes'] = false;
$arguments81['encoding'] = 'UTF-8';
$arguments81['doubleEncode'] = true;
$renderChildrenClosure85 = function() use ($renderingContext, $self) {
return NULL;
};
$value86 = ($arguments81['value'] !== NULL ? $arguments81['value'] : $renderChildrenClosure85());

$output0 .= !is_string($value86) && !(is_object($value86) && method_exists($value86, '__toString')) ? $value86 : htmlspecialchars($value86, ($arguments81['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments81['encoding'], $arguments81['doubleEncode']);

$output0 .= '" />
<link rel="neos-xliff" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\RawViewHelper
$arguments87 = array();
$arguments87['value'] = NULL;
$renderChildrenClosure88 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments89 = array();
$arguments89['action'] = 'xliffAsJson';
// Rendering Array
$array90 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\InterfaceLanguageViewHelper
$arguments91 = array();
$renderChildrenClosure92 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper93 = $self->getViewHelper('$viewHelper93', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\InterfaceLanguageViewHelper');
$viewHelper93->setArguments($arguments91);
$viewHelper93->setRenderingContext($renderingContext);
$viewHelper93->setRenderChildrenClosure($renderChildrenClosure92);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\InterfaceLanguageViewHelper
$array90['locale'] = $viewHelper93->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\XliffCacheVersionViewHelper
$arguments94 = array();
$renderChildrenClosure95 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper96 = $self->getViewHelper('$viewHelper96', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\XliffCacheVersionViewHelper');
$viewHelper96->setArguments($arguments94);
$viewHelper96->setRenderingContext($renderingContext);
$viewHelper96->setRenderChildrenClosure($renderChildrenClosure95);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\XliffCacheVersionViewHelper
$array90['version'] = $viewHelper96->initializeArgumentsAndRender();
$arguments89['arguments'] = $array90;
$arguments89['controller'] = 'Backend\\Backend';
$arguments89['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments89['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments89['subpackage'] = NULL;
$arguments89['section'] = '';
$arguments89['format'] = '';
$arguments89['additionalParams'] = array (
);
$arguments89['addQueryString'] = false;
$arguments89['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments89['useParentRequest'] = false;
$renderChildrenClosure97 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper98 = $self->getViewHelper('$viewHelper98', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper98->setArguments($arguments89);
$viewHelper98->setRenderingContext($renderingContext);
$viewHelper98->setRenderChildrenClosure($renderChildrenClosure97);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
return $viewHelper98->initializeArgumentsAndRender();
};
$viewHelper99 = $self->getViewHelper('$viewHelper99', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\RawViewHelper');
$viewHelper99->setArguments($arguments87);
$viewHelper99->setRenderingContext($renderingContext);
$viewHelper99->setRenderChildrenClosure($renderChildrenClosure88);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\RawViewHelper

$output0 .= $viewHelper99->initializeArgumentsAndRender();

$output0 .= '" />

<!-- Temporary URL endpoints, will be removed / grouped when a REST interface is fully implemented -->
<link rel="neos-service-workspace-publishNode" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments100 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments101 = array();
$arguments101['action'] = 'publishNode';
$arguments101['controller'] = 'Workspace';
$arguments101['package'] = 'TYPO3.Neos';
$arguments101['subpackage'] = 'Service';
// Rendering Boolean node
$arguments101['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments101['format'] = 'json';
$arguments101['arguments'] = array (
);
$arguments101['section'] = '';
$arguments101['additionalParams'] = array (
);
$arguments101['addQueryString'] = false;
$arguments101['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments101['useParentRequest'] = false;
$renderChildrenClosure102 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper103 = $self->getViewHelper('$viewHelper103', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper103->setArguments($arguments101);
$viewHelper103->setRenderingContext($renderingContext);
$viewHelper103->setRenderChildrenClosure($renderChildrenClosure102);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments100['value'] = $viewHelper103->initializeArgumentsAndRender();
$arguments100['keepQuotes'] = false;
$arguments100['encoding'] = 'UTF-8';
$arguments100['doubleEncode'] = true;
$renderChildrenClosure104 = function() use ($renderingContext, $self) {
return NULL;
};
$value105 = ($arguments100['value'] !== NULL ? $arguments100['value'] : $renderChildrenClosure104());

$output0 .= !is_string($value105) && !(is_object($value105) && method_exists($value105, '__toString')) ? $value105 : htmlspecialchars($value105, ($arguments100['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments100['encoding'], $arguments100['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-workspace-publishNodes" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments106 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments107 = array();
$arguments107['action'] = 'publishNodes';
$arguments107['controller'] = 'Workspace';
$arguments107['package'] = 'TYPO3.Neos';
$arguments107['subpackage'] = 'Service';
// Rendering Boolean node
$arguments107['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments107['format'] = 'json';
$arguments107['arguments'] = array (
);
$arguments107['section'] = '';
$arguments107['additionalParams'] = array (
);
$arguments107['addQueryString'] = false;
$arguments107['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments107['useParentRequest'] = false;
$renderChildrenClosure108 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper109 = $self->getViewHelper('$viewHelper109', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper109->setArguments($arguments107);
$viewHelper109->setRenderingContext($renderingContext);
$viewHelper109->setRenderChildrenClosure($renderChildrenClosure108);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments106['value'] = $viewHelper109->initializeArgumentsAndRender();
$arguments106['keepQuotes'] = false;
$arguments106['encoding'] = 'UTF-8';
$arguments106['doubleEncode'] = true;
$renderChildrenClosure110 = function() use ($renderingContext, $self) {
return NULL;
};
$value111 = ($arguments106['value'] !== NULL ? $arguments106['value'] : $renderChildrenClosure110());

$output0 .= !is_string($value111) && !(is_object($value111) && method_exists($value111, '__toString')) ? $value111 : htmlspecialchars($value111, ($arguments106['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments106['encoding'], $arguments106['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-workspace-publishAll" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments112 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments113 = array();
$arguments113['action'] = 'publishAll';
$arguments113['controller'] = 'Workspace';
$arguments113['package'] = 'TYPO3.Neos';
$arguments113['subpackage'] = 'Service';
// Rendering Boolean node
$arguments113['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments113['format'] = 'json';
$arguments113['arguments'] = array (
);
$arguments113['section'] = '';
$arguments113['additionalParams'] = array (
);
$arguments113['addQueryString'] = false;
$arguments113['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments113['useParentRequest'] = false;
$renderChildrenClosure114 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper115 = $self->getViewHelper('$viewHelper115', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper115->setArguments($arguments113);
$viewHelper115->setRenderingContext($renderingContext);
$viewHelper115->setRenderChildrenClosure($renderChildrenClosure114);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments112['value'] = $viewHelper115->initializeArgumentsAndRender();
$arguments112['keepQuotes'] = false;
$arguments112['encoding'] = 'UTF-8';
$arguments112['doubleEncode'] = true;
$renderChildrenClosure116 = function() use ($renderingContext, $self) {
return NULL;
};
$value117 = ($arguments112['value'] !== NULL ? $arguments112['value'] : $renderChildrenClosure116());

$output0 .= !is_string($value117) && !(is_object($value117) && method_exists($value117, '__toString')) ? $value117 : htmlspecialchars($value117, ($arguments112['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments112['encoding'], $arguments112['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-workspace-discardNode" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments118 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments119 = array();
$arguments119['action'] = 'discardNode';
$arguments119['controller'] = 'Workspace';
$arguments119['package'] = 'TYPO3.Neos';
$arguments119['subpackage'] = 'Service';
// Rendering Boolean node
$arguments119['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments119['format'] = 'json';
$arguments119['arguments'] = array (
);
$arguments119['section'] = '';
$arguments119['additionalParams'] = array (
);
$arguments119['addQueryString'] = false;
$arguments119['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments119['useParentRequest'] = false;
$renderChildrenClosure120 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper121 = $self->getViewHelper('$viewHelper121', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper121->setArguments($arguments119);
$viewHelper121->setRenderingContext($renderingContext);
$viewHelper121->setRenderChildrenClosure($renderChildrenClosure120);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments118['value'] = $viewHelper121->initializeArgumentsAndRender();
$arguments118['keepQuotes'] = false;
$arguments118['encoding'] = 'UTF-8';
$arguments118['doubleEncode'] = true;
$renderChildrenClosure122 = function() use ($renderingContext, $self) {
return NULL;
};
$value123 = ($arguments118['value'] !== NULL ? $arguments118['value'] : $renderChildrenClosure122());

$output0 .= !is_string($value123) && !(is_object($value123) && method_exists($value123, '__toString')) ? $value123 : htmlspecialchars($value123, ($arguments118['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments118['encoding'], $arguments118['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-workspace-discardNodes" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments124 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments125 = array();
$arguments125['action'] = 'discardNodes';
$arguments125['controller'] = 'Workspace';
$arguments125['package'] = 'TYPO3.Neos';
$arguments125['subpackage'] = 'Service';
// Rendering Boolean node
$arguments125['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments125['format'] = 'json';
$arguments125['arguments'] = array (
);
$arguments125['section'] = '';
$arguments125['additionalParams'] = array (
);
$arguments125['addQueryString'] = false;
$arguments125['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments125['useParentRequest'] = false;
$renderChildrenClosure126 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper127 = $self->getViewHelper('$viewHelper127', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper127->setArguments($arguments125);
$viewHelper127->setRenderingContext($renderingContext);
$viewHelper127->setRenderChildrenClosure($renderChildrenClosure126);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments124['value'] = $viewHelper127->initializeArgumentsAndRender();
$arguments124['keepQuotes'] = false;
$arguments124['encoding'] = 'UTF-8';
$arguments124['doubleEncode'] = true;
$renderChildrenClosure128 = function() use ($renderingContext, $self) {
return NULL;
};
$value129 = ($arguments124['value'] !== NULL ? $arguments124['value'] : $renderChildrenClosure128());

$output0 .= !is_string($value129) && !(is_object($value129) && method_exists($value129, '__toString')) ? $value129 : htmlspecialchars($value129, ($arguments124['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments124['encoding'], $arguments124['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-workspace-discardAll" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments130 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments131 = array();
$arguments131['action'] = 'discardAll';
$arguments131['controller'] = 'Workspace';
$arguments131['package'] = 'TYPO3.Neos';
$arguments131['subpackage'] = 'Service';
// Rendering Boolean node
$arguments131['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments131['format'] = 'json';
$arguments131['arguments'] = array (
);
$arguments131['section'] = '';
$arguments131['additionalParams'] = array (
);
$arguments131['addQueryString'] = false;
$arguments131['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments131['useParentRequest'] = false;
$renderChildrenClosure132 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper133 = $self->getViewHelper('$viewHelper133', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper133->setArguments($arguments131);
$viewHelper133->setRenderingContext($renderingContext);
$viewHelper133->setRenderChildrenClosure($renderChildrenClosure132);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments130['value'] = $viewHelper133->initializeArgumentsAndRender();
$arguments130['keepQuotes'] = false;
$arguments130['encoding'] = 'UTF-8';
$arguments130['doubleEncode'] = true;
$renderChildrenClosure134 = function() use ($renderingContext, $self) {
return NULL;
};
$value135 = ($arguments130['value'] !== NULL ? $arguments130['value'] : $renderChildrenClosure134());

$output0 .= !is_string($value135) && !(is_object($value135) && method_exists($value135, '__toString')) ? $value135 : htmlspecialchars($value135, ($arguments130['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments130['encoding'], $arguments130['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-workspace-getWorkspaceWideUnpublishedNodes" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments136 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments137 = array();
$arguments137['action'] = 'getWorkspaceWideUnpublishedNodes';
$arguments137['controller'] = 'Workspace';
$arguments137['package'] = 'TYPO3.Neos';
$arguments137['subpackage'] = 'Service';
// Rendering Boolean node
$arguments137['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments137['format'] = 'json';
$arguments137['arguments'] = array (
);
$arguments137['section'] = '';
$arguments137['additionalParams'] = array (
);
$arguments137['addQueryString'] = false;
$arguments137['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments137['useParentRequest'] = false;
$renderChildrenClosure138 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper139 = $self->getViewHelper('$viewHelper139', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper139->setArguments($arguments137);
$viewHelper139->setRenderingContext($renderingContext);
$viewHelper139->setRenderChildrenClosure($renderChildrenClosure138);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments136['value'] = $viewHelper139->initializeArgumentsAndRender();
$arguments136['keepQuotes'] = false;
$arguments136['encoding'] = 'UTF-8';
$arguments136['doubleEncode'] = true;
$renderChildrenClosure140 = function() use ($renderingContext, $self) {
return NULL;
};
$value141 = ($arguments136['value'] !== NULL ? $arguments136['value'] : $renderChildrenClosure140());

$output0 .= !is_string($value141) && !(is_object($value141) && method_exists($value141, '__toString')) ? $value141 : htmlspecialchars($value141, ($arguments136['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments136['encoding'], $arguments136['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-node-getChildNodesForTree" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments142 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments143 = array();
$arguments143['action'] = 'getChildNodesForTree';
$arguments143['controller'] = 'Node';
$arguments143['package'] = 'TYPO3.Neos';
$arguments143['subpackage'] = 'Service';
// Rendering Boolean node
$arguments143['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments143['format'] = 'json';
$arguments143['arguments'] = array (
);
$arguments143['section'] = '';
$arguments143['additionalParams'] = array (
);
$arguments143['addQueryString'] = false;
$arguments143['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments143['useParentRequest'] = false;
$renderChildrenClosure144 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper145 = $self->getViewHelper('$viewHelper145', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper145->setArguments($arguments143);
$viewHelper145->setRenderingContext($renderingContext);
$viewHelper145->setRenderChildrenClosure($renderChildrenClosure144);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments142['value'] = $viewHelper145->initializeArgumentsAndRender();
$arguments142['keepQuotes'] = false;
$arguments142['encoding'] = 'UTF-8';
$arguments142['doubleEncode'] = true;
$renderChildrenClosure146 = function() use ($renderingContext, $self) {
return NULL;
};
$value147 = ($arguments142['value'] !== NULL ? $arguments142['value'] : $renderChildrenClosure146());

$output0 .= !is_string($value147) && !(is_object($value147) && method_exists($value147, '__toString')) ? $value147 : htmlspecialchars($value147, ($arguments142['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments142['encoding'], $arguments142['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-node-createNodeForTheTree" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments148 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments149 = array();
$arguments149['action'] = 'createNodeForTheTree';
$arguments149['controller'] = 'Node';
$arguments149['package'] = 'TYPO3.Neos';
$arguments149['subpackage'] = 'Service';
// Rendering Boolean node
$arguments149['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments149['format'] = 'json';
$arguments149['arguments'] = array (
);
$arguments149['section'] = '';
$arguments149['additionalParams'] = array (
);
$arguments149['addQueryString'] = false;
$arguments149['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments149['useParentRequest'] = false;
$renderChildrenClosure150 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper151 = $self->getViewHelper('$viewHelper151', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper151->setArguments($arguments149);
$viewHelper151->setRenderingContext($renderingContext);
$viewHelper151->setRenderChildrenClosure($renderChildrenClosure150);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments148['value'] = $viewHelper151->initializeArgumentsAndRender();
$arguments148['keepQuotes'] = false;
$arguments148['encoding'] = 'UTF-8';
$arguments148['doubleEncode'] = true;
$renderChildrenClosure152 = function() use ($renderingContext, $self) {
return NULL;
};
$value153 = ($arguments148['value'] !== NULL ? $arguments148['value'] : $renderChildrenClosure152());

$output0 .= !is_string($value153) && !(is_object($value153) && method_exists($value153, '__toString')) ? $value153 : htmlspecialchars($value153, ($arguments148['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments148['encoding'], $arguments148['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-node-filterChildNodesForTree" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments154 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments155 = array();
$arguments155['action'] = 'filterChildNodesForTree';
$arguments155['controller'] = 'Node';
$arguments155['package'] = 'TYPO3.Neos';
$arguments155['subpackage'] = 'Service';
// Rendering Boolean node
$arguments155['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments155['format'] = 'json';
$arguments155['arguments'] = array (
);
$arguments155['section'] = '';
$arguments155['additionalParams'] = array (
);
$arguments155['addQueryString'] = false;
$arguments155['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments155['useParentRequest'] = false;
$renderChildrenClosure156 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper157 = $self->getViewHelper('$viewHelper157', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper157->setArguments($arguments155);
$viewHelper157->setRenderingContext($renderingContext);
$viewHelper157->setRenderChildrenClosure($renderChildrenClosure156);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments154['value'] = $viewHelper157->initializeArgumentsAndRender();
$arguments154['keepQuotes'] = false;
$arguments154['encoding'] = 'UTF-8';
$arguments154['doubleEncode'] = true;
$renderChildrenClosure158 = function() use ($renderingContext, $self) {
return NULL;
};
$value159 = ($arguments154['value'] !== NULL ? $arguments154['value'] : $renderChildrenClosure158());

$output0 .= !is_string($value159) && !(is_object($value159) && method_exists($value159, '__toString')) ? $value159 : htmlspecialchars($value159, ($arguments154['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments154['encoding'], $arguments154['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-node-create" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments160 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments161 = array();
$arguments161['action'] = 'create';
$arguments161['controller'] = 'Node';
$arguments161['package'] = 'TYPO3.Neos';
$arguments161['subpackage'] = 'Service';
// Rendering Boolean node
$arguments161['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments161['format'] = 'json';
$arguments161['arguments'] = array (
);
$arguments161['section'] = '';
$arguments161['additionalParams'] = array (
);
$arguments161['addQueryString'] = false;
$arguments161['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments161['useParentRequest'] = false;
$renderChildrenClosure162 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper163 = $self->getViewHelper('$viewHelper163', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper163->setArguments($arguments161);
$viewHelper163->setRenderingContext($renderingContext);
$viewHelper163->setRenderChildrenClosure($renderChildrenClosure162);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments160['value'] = $viewHelper163->initializeArgumentsAndRender();
$arguments160['keepQuotes'] = false;
$arguments160['encoding'] = 'UTF-8';
$arguments160['doubleEncode'] = true;
$renderChildrenClosure164 = function() use ($renderingContext, $self) {
return NULL;
};
$value165 = ($arguments160['value'] !== NULL ? $arguments160['value'] : $renderChildrenClosure164());

$output0 .= !is_string($value165) && !(is_object($value165) && method_exists($value165, '__toString')) ? $value165 : htmlspecialchars($value165, ($arguments160['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments160['encoding'], $arguments160['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-node-createAndRender" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments166 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments167 = array();
$arguments167['action'] = 'createAndRender';
$arguments167['controller'] = 'Node';
$arguments167['package'] = 'TYPO3.Neos';
$arguments167['subpackage'] = 'Service';
// Rendering Boolean node
$arguments167['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments167['format'] = 'json';
$arguments167['arguments'] = array (
);
$arguments167['section'] = '';
$arguments167['additionalParams'] = array (
);
$arguments167['addQueryString'] = false;
$arguments167['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments167['useParentRequest'] = false;
$renderChildrenClosure168 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper169 = $self->getViewHelper('$viewHelper169', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper169->setArguments($arguments167);
$viewHelper169->setRenderingContext($renderingContext);
$viewHelper169->setRenderChildrenClosure($renderChildrenClosure168);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments166['value'] = $viewHelper169->initializeArgumentsAndRender();
$arguments166['keepQuotes'] = false;
$arguments166['encoding'] = 'UTF-8';
$arguments166['doubleEncode'] = true;
$renderChildrenClosure170 = function() use ($renderingContext, $self) {
return NULL;
};
$value171 = ($arguments166['value'] !== NULL ? $arguments166['value'] : $renderChildrenClosure170());

$output0 .= !is_string($value171) && !(is_object($value171) && method_exists($value171, '__toString')) ? $value171 : htmlspecialchars($value171, ($arguments166['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments166['encoding'], $arguments166['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-node-move" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments172 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments173 = array();
$arguments173['action'] = 'move';
$arguments173['controller'] = 'Node';
$arguments173['package'] = 'TYPO3.Neos';
$arguments173['subpackage'] = 'Service';
// Rendering Boolean node
$arguments173['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments173['format'] = 'json';
$arguments173['arguments'] = array (
);
$arguments173['section'] = '';
$arguments173['additionalParams'] = array (
);
$arguments173['addQueryString'] = false;
$arguments173['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments173['useParentRequest'] = false;
$renderChildrenClosure174 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper175 = $self->getViewHelper('$viewHelper175', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper175->setArguments($arguments173);
$viewHelper175->setRenderingContext($renderingContext);
$viewHelper175->setRenderChildrenClosure($renderChildrenClosure174);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments172['value'] = $viewHelper175->initializeArgumentsAndRender();
$arguments172['keepQuotes'] = false;
$arguments172['encoding'] = 'UTF-8';
$arguments172['doubleEncode'] = true;
$renderChildrenClosure176 = function() use ($renderingContext, $self) {
return NULL;
};
$value177 = ($arguments172['value'] !== NULL ? $arguments172['value'] : $renderChildrenClosure176());

$output0 .= !is_string($value177) && !(is_object($value177) && method_exists($value177, '__toString')) ? $value177 : htmlspecialchars($value177, ($arguments172['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments172['encoding'], $arguments172['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-node-moveAndRender" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments178 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments179 = array();
$arguments179['action'] = 'moveAndRender';
$arguments179['controller'] = 'Node';
$arguments179['package'] = 'TYPO3.Neos';
$arguments179['subpackage'] = 'Service';
// Rendering Boolean node
$arguments179['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments179['format'] = 'json';
$arguments179['arguments'] = array (
);
$arguments179['section'] = '';
$arguments179['additionalParams'] = array (
);
$arguments179['addQueryString'] = false;
$arguments179['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments179['useParentRequest'] = false;
$renderChildrenClosure180 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper181 = $self->getViewHelper('$viewHelper181', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper181->setArguments($arguments179);
$viewHelper181->setRenderingContext($renderingContext);
$viewHelper181->setRenderChildrenClosure($renderChildrenClosure180);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments178['value'] = $viewHelper181->initializeArgumentsAndRender();
$arguments178['keepQuotes'] = false;
$arguments178['encoding'] = 'UTF-8';
$arguments178['doubleEncode'] = true;
$renderChildrenClosure182 = function() use ($renderingContext, $self) {
return NULL;
};
$value183 = ($arguments178['value'] !== NULL ? $arguments178['value'] : $renderChildrenClosure182());

$output0 .= !is_string($value183) && !(is_object($value183) && method_exists($value183, '__toString')) ? $value183 : htmlspecialchars($value183, ($arguments178['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments178['encoding'], $arguments178['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-node-copy" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments184 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments185 = array();
$arguments185['action'] = 'copy';
$arguments185['controller'] = 'Node';
$arguments185['package'] = 'TYPO3.Neos';
$arguments185['subpackage'] = 'Service';
// Rendering Boolean node
$arguments185['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments185['format'] = 'json';
$arguments185['arguments'] = array (
);
$arguments185['section'] = '';
$arguments185['additionalParams'] = array (
);
$arguments185['addQueryString'] = false;
$arguments185['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments185['useParentRequest'] = false;
$renderChildrenClosure186 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper187 = $self->getViewHelper('$viewHelper187', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper187->setArguments($arguments185);
$viewHelper187->setRenderingContext($renderingContext);
$viewHelper187->setRenderChildrenClosure($renderChildrenClosure186);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments184['value'] = $viewHelper187->initializeArgumentsAndRender();
$arguments184['keepQuotes'] = false;
$arguments184['encoding'] = 'UTF-8';
$arguments184['doubleEncode'] = true;
$renderChildrenClosure188 = function() use ($renderingContext, $self) {
return NULL;
};
$value189 = ($arguments184['value'] !== NULL ? $arguments184['value'] : $renderChildrenClosure188());

$output0 .= !is_string($value189) && !(is_object($value189) && method_exists($value189, '__toString')) ? $value189 : htmlspecialchars($value189, ($arguments184['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments184['encoding'], $arguments184['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-node-copyAndRender" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments190 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments191 = array();
$arguments191['action'] = 'copyAndRender';
$arguments191['controller'] = 'Node';
$arguments191['package'] = 'TYPO3.Neos';
$arguments191['subpackage'] = 'Service';
// Rendering Boolean node
$arguments191['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments191['format'] = 'json';
$arguments191['arguments'] = array (
);
$arguments191['section'] = '';
$arguments191['additionalParams'] = array (
);
$arguments191['addQueryString'] = false;
$arguments191['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments191['useParentRequest'] = false;
$renderChildrenClosure192 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper193 = $self->getViewHelper('$viewHelper193', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper193->setArguments($arguments191);
$viewHelper193->setRenderingContext($renderingContext);
$viewHelper193->setRenderChildrenClosure($renderChildrenClosure192);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments190['value'] = $viewHelper193->initializeArgumentsAndRender();
$arguments190['keepQuotes'] = false;
$arguments190['encoding'] = 'UTF-8';
$arguments190['doubleEncode'] = true;
$renderChildrenClosure194 = function() use ($renderingContext, $self) {
return NULL;
};
$value195 = ($arguments190['value'] !== NULL ? $arguments190['value'] : $renderChildrenClosure194());

$output0 .= !is_string($value195) && !(is_object($value195) && method_exists($value195, '__toString')) ? $value195 : htmlspecialchars($value195, ($arguments190['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments190['encoding'], $arguments190['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-node-update" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments196 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments197 = array();
$arguments197['action'] = 'update';
$arguments197['controller'] = 'Node';
$arguments197['package'] = 'TYPO3.Neos';
$arguments197['subpackage'] = 'Service';
// Rendering Boolean node
$arguments197['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments197['format'] = 'json';
$arguments197['arguments'] = array (
);
$arguments197['section'] = '';
$arguments197['additionalParams'] = array (
);
$arguments197['addQueryString'] = false;
$arguments197['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments197['useParentRequest'] = false;
$renderChildrenClosure198 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper199 = $self->getViewHelper('$viewHelper199', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper199->setArguments($arguments197);
$viewHelper199->setRenderingContext($renderingContext);
$viewHelper199->setRenderChildrenClosure($renderChildrenClosure198);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments196['value'] = $viewHelper199->initializeArgumentsAndRender();
$arguments196['keepQuotes'] = false;
$arguments196['encoding'] = 'UTF-8';
$arguments196['doubleEncode'] = true;
$renderChildrenClosure200 = function() use ($renderingContext, $self) {
return NULL;
};
$value201 = ($arguments196['value'] !== NULL ? $arguments196['value'] : $renderChildrenClosure200());

$output0 .= !is_string($value201) && !(is_object($value201) && method_exists($value201, '__toString')) ? $value201 : htmlspecialchars($value201, ($arguments196['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments196['encoding'], $arguments196['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-node-updateAndRender" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments202 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments203 = array();
$arguments203['action'] = 'updateAndRender';
$arguments203['controller'] = 'Node';
$arguments203['package'] = 'TYPO3.Neos';
$arguments203['subpackage'] = 'Service';
// Rendering Boolean node
$arguments203['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments203['format'] = 'json';
$arguments203['arguments'] = array (
);
$arguments203['section'] = '';
$arguments203['additionalParams'] = array (
);
$arguments203['addQueryString'] = false;
$arguments203['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments203['useParentRequest'] = false;
$renderChildrenClosure204 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper205 = $self->getViewHelper('$viewHelper205', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper205->setArguments($arguments203);
$viewHelper205->setRenderingContext($renderingContext);
$viewHelper205->setRenderChildrenClosure($renderChildrenClosure204);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments202['value'] = $viewHelper205->initializeArgumentsAndRender();
$arguments202['keepQuotes'] = false;
$arguments202['encoding'] = 'UTF-8';
$arguments202['doubleEncode'] = true;
$renderChildrenClosure206 = function() use ($renderingContext, $self) {
return NULL;
};
$value207 = ($arguments202['value'] !== NULL ? $arguments202['value'] : $renderChildrenClosure206());

$output0 .= !is_string($value207) && !(is_object($value207) && method_exists($value207, '__toString')) ? $value207 : htmlspecialchars($value207, ($arguments202['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments202['encoding'], $arguments202['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-node-delete" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments208 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments209 = array();
$arguments209['action'] = 'delete';
$arguments209['controller'] = 'Node';
$arguments209['package'] = 'TYPO3.Neos';
$arguments209['subpackage'] = 'Service';
// Rendering Boolean node
$arguments209['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments209['format'] = 'json';
$arguments209['arguments'] = array (
);
$arguments209['section'] = '';
$arguments209['additionalParams'] = array (
);
$arguments209['addQueryString'] = false;
$arguments209['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments209['useParentRequest'] = false;
$renderChildrenClosure210 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper211 = $self->getViewHelper('$viewHelper211', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper211->setArguments($arguments209);
$viewHelper211->setRenderingContext($renderingContext);
$viewHelper211->setRenderChildrenClosure($renderChildrenClosure210);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments208['value'] = $viewHelper211->initializeArgumentsAndRender();
$arguments208['keepQuotes'] = false;
$arguments208['encoding'] = 'UTF-8';
$arguments208['doubleEncode'] = true;
$renderChildrenClosure212 = function() use ($renderingContext, $self) {
return NULL;
};
$value213 = ($arguments208['value'] !== NULL ? $arguments208['value'] : $renderChildrenClosure212());

$output0 .= !is_string($value213) && !(is_object($value213) && method_exists($value213, '__toString')) ? $value213 : htmlspecialchars($value213, ($arguments208['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments208['encoding'], $arguments208['doubleEncode']);

$output0 .= '" />
<link rel="neos-service-node-searchPage" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments214 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments215 = array();
$arguments215['action'] = 'searchPage';
$arguments215['controller'] = 'Node';
$arguments215['package'] = 'TYPO3.Neos';
$arguments215['subpackage'] = 'Service';
// Rendering Boolean node
$arguments215['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments215['format'] = 'json';
$arguments215['arguments'] = array (
);
$arguments215['section'] = '';
$arguments215['additionalParams'] = array (
);
$arguments215['addQueryString'] = false;
$arguments215['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments215['useParentRequest'] = false;
$renderChildrenClosure216 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper217 = $self->getViewHelper('$viewHelper217', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper217->setArguments($arguments215);
$viewHelper217->setRenderingContext($renderingContext);
$viewHelper217->setRenderChildrenClosure($renderChildrenClosure216);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments214['value'] = $viewHelper217->initializeArgumentsAndRender();
$arguments214['keepQuotes'] = false;
$arguments214['encoding'] = 'UTF-8';
$arguments214['doubleEncode'] = true;
$renderChildrenClosure218 = function() use ($renderingContext, $self) {
return NULL;
};
$value219 = ($arguments214['value'] !== NULL ? $arguments214['value'] : $renderChildrenClosure218());

$output0 .= !is_string($value219) && !(is_object($value219) && method_exists($value219, '__toString')) ? $value219 : htmlspecialchars($value219, ($arguments214['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments214['encoding'], $arguments214['doubleEncode']);

$output0 .= '" />
<link rel="neos-images" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments220 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments221 = array();
$arguments221['action'] = 'imageWithMetadata';
$arguments221['controller'] = 'Backend\\Content';
$arguments221['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments221['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments221['arguments'] = array (
);
$arguments221['subpackage'] = NULL;
$arguments221['section'] = '';
$arguments221['format'] = '';
$arguments221['additionalParams'] = array (
);
$arguments221['addQueryString'] = false;
$arguments221['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments221['useParentRequest'] = false;
$renderChildrenClosure222 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper223 = $self->getViewHelper('$viewHelper223', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper223->setArguments($arguments221);
$viewHelper223->setRenderingContext($renderingContext);
$viewHelper223->setRenderChildrenClosure($renderChildrenClosure222);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments220['value'] = $viewHelper223->initializeArgumentsAndRender();
$arguments220['keepQuotes'] = false;
$arguments220['encoding'] = 'UTF-8';
$arguments220['doubleEncode'] = true;
$renderChildrenClosure224 = function() use ($renderingContext, $self) {
return NULL;
};
$value225 = ($arguments220['value'] !== NULL ? $arguments220['value'] : $renderChildrenClosure224());

$output0 .= !is_string($value225) && !(is_object($value225) && method_exists($value225, '__toString')) ? $value225 : htmlspecialchars($value225, ($arguments220['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments220['encoding'], $arguments220['doubleEncode']);

$output0 .= '" />
<link rel="neos-asset-upload" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments226 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments227 = array();
$arguments227['action'] = 'uploadAsset';
$arguments227['controller'] = 'Backend\\Content';
$arguments227['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments227['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments227['arguments'] = array (
);
$arguments227['subpackage'] = NULL;
$arguments227['section'] = '';
$arguments227['format'] = '';
$arguments227['additionalParams'] = array (
);
$arguments227['addQueryString'] = false;
$arguments227['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments227['useParentRequest'] = false;
$renderChildrenClosure228 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper229 = $self->getViewHelper('$viewHelper229', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper229->setArguments($arguments227);
$viewHelper229->setRenderingContext($renderingContext);
$viewHelper229->setRenderChildrenClosure($renderChildrenClosure228);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments226['value'] = $viewHelper229->initializeArgumentsAndRender();
$arguments226['keepQuotes'] = false;
$arguments226['encoding'] = 'UTF-8';
$arguments226['doubleEncode'] = true;
$renderChildrenClosure230 = function() use ($renderingContext, $self) {
return NULL;
};
$value231 = ($arguments226['value'] !== NULL ? $arguments226['value'] : $renderChildrenClosure230());

$output0 .= !is_string($value231) && !(is_object($value231) && method_exists($value231, '__toString')) ? $value231 : htmlspecialchars($value231, ($arguments226['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments226['encoding'], $arguments226['doubleEncode']);

$output0 .= '" />
<link rel="neos-asset-metadata" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments232 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments233 = array();
$arguments233['action'] = 'assetsWithMetadata';
$arguments233['controller'] = 'Backend\\Content';
$arguments233['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments233['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments233['arguments'] = array (
);
$arguments233['subpackage'] = NULL;
$arguments233['section'] = '';
$arguments233['format'] = '';
$arguments233['additionalParams'] = array (
);
$arguments233['addQueryString'] = false;
$arguments233['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments233['useParentRequest'] = false;
$renderChildrenClosure234 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper235 = $self->getViewHelper('$viewHelper235', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper235->setArguments($arguments233);
$viewHelper235->setRenderingContext($renderingContext);
$viewHelper235->setRenderChildrenClosure($renderChildrenClosure234);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments232['value'] = $viewHelper235->initializeArgumentsAndRender();
$arguments232['keepQuotes'] = false;
$arguments232['encoding'] = 'UTF-8';
$arguments232['doubleEncode'] = true;
$renderChildrenClosure236 = function() use ($renderingContext, $self) {
return NULL;
};
$value237 = ($arguments232['value'] !== NULL ? $arguments232['value'] : $renderChildrenClosure236());

$output0 .= !is_string($value237) && !(is_object($value237) && method_exists($value237, '__toString')) ? $value237 : htmlspecialchars($value237, ($arguments232['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments232['encoding'], $arguments232['doubleEncode']);

$output0 .= '" />
<link rel="neos-imagevariant-create" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments238 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments239 = array();
$arguments239['action'] = 'createImageVariant';
$arguments239['controller'] = 'Backend\\Content';
$arguments239['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments239['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments239['arguments'] = array (
);
$arguments239['subpackage'] = NULL;
$arguments239['section'] = '';
$arguments239['format'] = '';
$arguments239['additionalParams'] = array (
);
$arguments239['addQueryString'] = false;
$arguments239['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments239['useParentRequest'] = false;
$renderChildrenClosure240 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper241 = $self->getViewHelper('$viewHelper241', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper241->setArguments($arguments239);
$viewHelper241->setRenderingContext($renderingContext);
$viewHelper241->setRenderChildrenClosure($renderChildrenClosure240);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments238['value'] = $viewHelper241->initializeArgumentsAndRender();
$arguments238['keepQuotes'] = false;
$arguments238['encoding'] = 'UTF-8';
$arguments238['doubleEncode'] = true;
$renderChildrenClosure242 = function() use ($renderingContext, $self) {
return NULL;
};
$value243 = ($arguments238['value'] !== NULL ? $arguments238['value'] : $renderChildrenClosure242());

$output0 .= !is_string($value243) && !(is_object($value243) && method_exists($value243, '__toString')) ? $value243 : htmlspecialchars($value243, ($arguments238['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments238['encoding'], $arguments238['doubleEncode']);

$output0 .= '" />
<link rel="neos-pluginviews" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments244 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments245 = array();
$arguments245['action'] = 'pluginViews';
$arguments245['controller'] = 'Backend\\Content';
$arguments245['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments245['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments245['arguments'] = array (
);
$arguments245['subpackage'] = NULL;
$arguments245['section'] = '';
$arguments245['format'] = '';
$arguments245['additionalParams'] = array (
);
$arguments245['addQueryString'] = false;
$arguments245['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments245['useParentRequest'] = false;
$renderChildrenClosure246 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper247 = $self->getViewHelper('$viewHelper247', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper247->setArguments($arguments245);
$viewHelper247->setRenderingContext($renderingContext);
$viewHelper247->setRenderChildrenClosure($renderChildrenClosure246);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments244['value'] = $viewHelper247->initializeArgumentsAndRender();
$arguments244['keepQuotes'] = false;
$arguments244['encoding'] = 'UTF-8';
$arguments244['doubleEncode'] = true;
$renderChildrenClosure248 = function() use ($renderingContext, $self) {
return NULL;
};
$value249 = ($arguments244['value'] !== NULL ? $arguments244['value'] : $renderChildrenClosure248());

$output0 .= !is_string($value249) && !(is_object($value249) && method_exists($value249, '__toString')) ? $value249 : htmlspecialchars($value249, ($arguments244['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments244['encoding'], $arguments244['doubleEncode']);

$output0 .= '" />
<link rel="neos-masterplugins" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments250 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments251 = array();
$arguments251['action'] = 'masterPlugins';
$arguments251['controller'] = 'Backend\\Content';
$arguments251['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments251['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments251['arguments'] = array (
);
$arguments251['subpackage'] = NULL;
$arguments251['section'] = '';
$arguments251['format'] = '';
$arguments251['additionalParams'] = array (
);
$arguments251['addQueryString'] = false;
$arguments251['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments251['useParentRequest'] = false;
$renderChildrenClosure252 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper253 = $self->getViewHelper('$viewHelper253', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper253->setArguments($arguments251);
$viewHelper253->setRenderingContext($renderingContext);
$viewHelper253->setRenderChildrenClosure($renderChildrenClosure252);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments250['value'] = $viewHelper253->initializeArgumentsAndRender();
$arguments250['keepQuotes'] = false;
$arguments250['encoding'] = 'UTF-8';
$arguments250['doubleEncode'] = true;
$renderChildrenClosure254 = function() use ($renderingContext, $self) {
return NULL;
};
$value255 = ($arguments250['value'] !== NULL ? $arguments250['value'] : $renderChildrenClosure254());

$output0 .= !is_string($value255) && !(is_object($value255) && method_exists($value255, '__toString')) ? $value255 : htmlspecialchars($value255, ($arguments250['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments250['encoding'], $arguments250['doubleEncode']);

$output0 .= '" />
<link rel="neos-user-preferences" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments256 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments257 = array();
$arguments257['action'] = 'index';
$arguments257['controller'] = 'UserPreference';
$arguments257['package'] = 'TYPO3.Neos';
$arguments257['subpackage'] = 'Service';
// Rendering Boolean node
$arguments257['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments257['format'] = 'json';
$arguments257['arguments'] = array (
);
$arguments257['section'] = '';
$arguments257['additionalParams'] = array (
);
$arguments257['addQueryString'] = false;
$arguments257['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments257['useParentRequest'] = false;
$renderChildrenClosure258 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper259 = $self->getViewHelper('$viewHelper259', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper259->setArguments($arguments257);
$viewHelper259->setRenderingContext($renderingContext);
$viewHelper259->setRenderChildrenClosure($renderChildrenClosure258);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments256['value'] = $viewHelper259->initializeArgumentsAndRender();
$arguments256['keepQuotes'] = false;
$arguments256['encoding'] = 'UTF-8';
$arguments256['doubleEncode'] = true;
$renderChildrenClosure260 = function() use ($renderingContext, $self) {
return NULL;
};
$value261 = ($arguments256['value'] !== NULL ? $arguments256['value'] : $renderChildrenClosure260());

$output0 .= !is_string($value261) && !(is_object($value261) && method_exists($value261, '__toString')) ? $value261 : htmlspecialchars($value261, ($arguments256['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments256['encoding'], $arguments256['doubleEncode']);

$output0 .= '" />
<link rel="neos-data-source" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments262 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments263 = array();
$arguments263['action'] = 'index';
$arguments263['controller'] = 'DataSource';
$arguments263['package'] = 'TYPO3.Neos';
$arguments263['subpackage'] = 'Service';
// Rendering Boolean node
$arguments263['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments263['format'] = 'json';
$arguments263['arguments'] = array (
);
$arguments263['section'] = '';
$arguments263['additionalParams'] = array (
);
$arguments263['addQueryString'] = false;
$arguments263['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments263['useParentRequest'] = false;
$renderChildrenClosure264 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper265 = $self->getViewHelper('$viewHelper265', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper265->setArguments($arguments263);
$viewHelper265->setRenderingContext($renderingContext);
$viewHelper265->setRenderChildrenClosure($renderChildrenClosure264);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments262['value'] = $viewHelper265->initializeArgumentsAndRender();
$arguments262['keepQuotes'] = false;
$arguments262['encoding'] = 'UTF-8';
$arguments262['doubleEncode'] = true;
$renderChildrenClosure266 = function() use ($renderingContext, $self) {
return NULL;
};
$value267 = ($arguments262['value'] !== NULL ? $arguments262['value'] : $renderChildrenClosure266());

$output0 .= !is_string($value267) && !(is_object($value267) && method_exists($value267, '__toString')) ? $value267 : htmlspecialchars($value267, ($arguments262['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments262['encoding'], $arguments262['doubleEncode']);

$output0 .= '" />
<link rel="neos-login" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments268 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments269 = array();
$arguments269['action'] = 'authenticate';
$arguments269['controller'] = 'Login';
$arguments269['package'] = 'TYPO3.Neos';
$arguments269['format'] = 'json';
// Rendering Boolean node
$arguments269['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments269['arguments'] = array (
);
$arguments269['subpackage'] = NULL;
$arguments269['section'] = '';
$arguments269['additionalParams'] = array (
);
$arguments269['addQueryString'] = false;
$arguments269['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments269['useParentRequest'] = false;
$renderChildrenClosure270 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper271 = $self->getViewHelper('$viewHelper271', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper271->setArguments($arguments269);
$viewHelper271->setRenderingContext($renderingContext);
$viewHelper271->setRenderChildrenClosure($renderChildrenClosure270);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments268['value'] = $viewHelper271->initializeArgumentsAndRender();
$arguments268['keepQuotes'] = false;
$arguments268['encoding'] = 'UTF-8';
$arguments268['doubleEncode'] = true;
$renderChildrenClosure272 = function() use ($renderingContext, $self) {
return NULL;
};
$value273 = ($arguments268['value'] !== NULL ? $arguments268['value'] : $renderChildrenClosure272());

$output0 .= !is_string($value273) && !(is_object($value273) && method_exists($value273, '__toString')) ? $value273 : htmlspecialchars($value273, ($arguments268['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments268['encoding'], $arguments268['doubleEncode']);

$output0 .= '" />
<!-- /Temporary URL endpoints -->

<link rel="neos-public-resources" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments274 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments275 = array();
$arguments275['path'] = '';
$arguments275['package'] = 'TYPO3.Neos';
$arguments275['resource'] = NULL;
$arguments275['localize'] = true;
$renderChildrenClosure276 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper277 = $self->getViewHelper('$viewHelper277', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper277->setArguments($arguments275);
$viewHelper277->setRenderingContext($renderingContext);
$viewHelper277->setRenderChildrenClosure($renderChildrenClosure276);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments274['value'] = $viewHelper277->initializeArgumentsAndRender();
$arguments274['keepQuotes'] = false;
$arguments274['encoding'] = 'UTF-8';
$arguments274['doubleEncode'] = true;
$renderChildrenClosure278 = function() use ($renderingContext, $self) {
return NULL;
};
$value279 = ($arguments274['value'] !== NULL ? $arguments274['value'] : $renderChildrenClosure278());

$output0 .= !is_string($value279) && !(is_object($value279) && method_exists($value279, '__toString')) ? $value279 : htmlspecialchars($value279, ($arguments274['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments274['encoding'], $arguments274['doubleEncode']);

$output0 .= '" />
<link rel="neos-module-workspacesmanagement" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments280 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Uri\ModuleViewHelper
$arguments281 = array();
$arguments281['path'] = 'management/workspaces';
$arguments281['action'] = NULL;
$arguments281['arguments'] = array (
);
$arguments281['section'] = '';
$arguments281['format'] = '';
$arguments281['additionalParams'] = array (
);
$arguments281['addQueryString'] = false;
$arguments281['argumentsToBeExcludedFromQueryString'] = array (
);
$renderChildrenClosure282 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper283 = $self->getViewHelper('$viewHelper283', $renderingContext, 'TYPO3\Neos\ViewHelpers\Uri\ModuleViewHelper');
$viewHelper283->setArguments($arguments281);
$viewHelper283->setRenderingContext($renderingContext);
$viewHelper283->setRenderChildrenClosure($renderChildrenClosure282);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Uri\ModuleViewHelper
$arguments280['value'] = $viewHelper283->initializeArgumentsAndRender();
$arguments280['keepQuotes'] = false;
$arguments280['encoding'] = 'UTF-8';
$arguments280['doubleEncode'] = true;
$renderChildrenClosure284 = function() use ($renderingContext, $self) {
return NULL;
};
$value285 = ($arguments280['value'] !== NULL ? $arguments280['value'] : $renderChildrenClosure284());

$output0 .= !is_string($value285) && !(is_object($value285) && method_exists($value285, '__toString')) ? $value285 : htmlspecialchars($value285, ($arguments280['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments280['encoding'], $arguments280['doubleEncode']);

$output0 .= '" />
<link rel="neos-module-workspacesmanagement-show" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments286 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Uri\ModuleViewHelper
$arguments287 = array();
$arguments287['path'] = 'management/workspaces';
$arguments287['action'] = 'show';
$arguments287['arguments'] = array (
);
$arguments287['section'] = '';
$arguments287['format'] = '';
$arguments287['additionalParams'] = array (
);
$arguments287['addQueryString'] = false;
$arguments287['argumentsToBeExcludedFromQueryString'] = array (
);
$renderChildrenClosure288 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper289 = $self->getViewHelper('$viewHelper289', $renderingContext, 'TYPO3\Neos\ViewHelpers\Uri\ModuleViewHelper');
$viewHelper289->setArguments($arguments287);
$viewHelper289->setRenderingContext($renderingContext);
$viewHelper289->setRenderChildrenClosure($renderChildrenClosure288);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Uri\ModuleViewHelper
$arguments286['value'] = $viewHelper289->initializeArgumentsAndRender();
$arguments286['keepQuotes'] = false;
$arguments286['encoding'] = 'UTF-8';
$arguments286['doubleEncode'] = true;
$renderChildrenClosure290 = function() use ($renderingContext, $self) {
return NULL;
};
$value291 = ($arguments286['value'] !== NULL ? $arguments286['value'] : $renderChildrenClosure290());

$output0 .= !is_string($value291) && !(is_object($value291) && method_exists($value291, '__toString')) ? $value291 : htmlspecialchars($value291, ($arguments286['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments286['encoding'], $arguments286['doubleEncode']);

$output0 .= '" />
<link rel="neos-media-browser" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments292 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments293 = array();
$arguments293['action'] = 'index';
$arguments293['controller'] = 'Backend\\MediaBrowser';
$arguments293['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments293['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
// Rendering Array
$array294 = array();
$array294['assetCollection'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'node.context.currentSite.assetCollection', $renderingContext);
$arguments293['arguments'] = $array294;
$arguments293['subpackage'] = NULL;
$arguments293['section'] = '';
$arguments293['format'] = '';
$arguments293['additionalParams'] = array (
);
$arguments293['addQueryString'] = false;
$arguments293['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments293['useParentRequest'] = false;
$renderChildrenClosure295 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper296 = $self->getViewHelper('$viewHelper296', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper296->setArguments($arguments293);
$viewHelper296->setRenderingContext($renderingContext);
$viewHelper296->setRenderChildrenClosure($renderChildrenClosure295);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments292['value'] = $viewHelper296->initializeArgumentsAndRender();
$arguments292['keepQuotes'] = false;
$arguments292['encoding'] = 'UTF-8';
$arguments292['doubleEncode'] = true;
$renderChildrenClosure297 = function() use ($renderingContext, $self) {
return NULL;
};
$value298 = ($arguments292['value'] !== NULL ? $arguments292['value'] : $renderChildrenClosure297());

$output0 .= !is_string($value298) && !(is_object($value298) && method_exists($value298, '__toString')) ? $value298 : htmlspecialchars($value298, ($arguments292['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments292['encoding'], $arguments292['doubleEncode']);

$output0 .= '" />
<link rel="neos-image-browser" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments299 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments300 = array();
$arguments300['action'] = 'index';
$arguments300['controller'] = 'Backend\\ImageBrowser';
$arguments300['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments300['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
// Rendering Array
$array301 = array();
$array301['assetCollection'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'node.context.currentSite.assetCollection', $renderingContext);
$arguments300['arguments'] = $array301;
$arguments300['subpackage'] = NULL;
$arguments300['section'] = '';
$arguments300['format'] = '';
$arguments300['additionalParams'] = array (
);
$arguments300['addQueryString'] = false;
$arguments300['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments300['useParentRequest'] = false;
$renderChildrenClosure302 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper303 = $self->getViewHelper('$viewHelper303', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper303->setArguments($arguments300);
$viewHelper303->setRenderingContext($renderingContext);
$viewHelper303->setRenderChildrenClosure($renderChildrenClosure302);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments299['value'] = $viewHelper303->initializeArgumentsAndRender();
$arguments299['keepQuotes'] = false;
$arguments299['encoding'] = 'UTF-8';
$arguments299['doubleEncode'] = true;
$renderChildrenClosure304 = function() use ($renderingContext, $self) {
return NULL;
};
$value305 = ($arguments299['value'] !== NULL ? $arguments299['value'] : $renderChildrenClosure304());

$output0 .= !is_string($value305) && !(is_object($value305) && method_exists($value305, '__toString')) ? $value305 : htmlspecialchars($value305, ($arguments299['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments299['encoding'], $arguments299['doubleEncode']);

$output0 .= '" />
<link rel="neos-image-browser-edit" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments306 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments307 = array();
$arguments307['action'] = 'edit';
$arguments307['controller'] = 'Backend\\ImageBrowser';
$arguments307['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments307['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments307['arguments'] = array (
);
$arguments307['subpackage'] = NULL;
$arguments307['section'] = '';
$arguments307['format'] = '';
$arguments307['additionalParams'] = array (
);
$arguments307['addQueryString'] = false;
$arguments307['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments307['useParentRequest'] = false;
$renderChildrenClosure308 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper309 = $self->getViewHelper('$viewHelper309', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper309->setArguments($arguments307);
$viewHelper309->setRenderingContext($renderingContext);
$viewHelper309->setRenderChildrenClosure($renderChildrenClosure308);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments306['value'] = $viewHelper309->initializeArgumentsAndRender();
$arguments306['keepQuotes'] = false;
$arguments306['encoding'] = 'UTF-8';
$arguments306['doubleEncode'] = true;
$renderChildrenClosure310 = function() use ($renderingContext, $self) {
return NULL;
};
$value311 = ($arguments306['value'] !== NULL ? $arguments306['value'] : $renderChildrenClosure310());

$output0 .= !is_string($value311) && !(is_object($value311) && method_exists($value311, '__toString')) ? $value311 : htmlspecialchars($value311, ($arguments306['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments306['encoding'], $arguments306['doubleEncode']);

$output0 .= '" />
';

return $output0;
}


}
#0             102876    