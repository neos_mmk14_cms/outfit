<?php class FluidCache_TYPO3_Neos__Module_Administration_Packages_action_index_3f54b3604c47fdf7c32ecc6833a235b66d648ced extends \TYPO3\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// TODO
	return new \TYPO3\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;

return 'BackendSubModule';
}
public function hasLayout() {
return TRUE;
}

/**
 * section content
 */
public function section_040f06fd774092478d450774f5ba30c5da78acc8(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output0 = '';

$output0 .= '
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments1 = array();
$arguments1['action'] = 'batch';
$arguments1['additionalAttributes'] = NULL;
$arguments1['data'] = NULL;
$arguments1['arguments'] = array (
);
$arguments1['controller'] = NULL;
$arguments1['package'] = NULL;
$arguments1['subpackage'] = NULL;
$arguments1['object'] = NULL;
$arguments1['section'] = '';
$arguments1['format'] = '';
$arguments1['additionalParams'] = array (
);
$arguments1['absolute'] = false;
$arguments1['addQueryString'] = false;
$arguments1['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1['fieldNamePrefix'] = NULL;
$arguments1['actionUri'] = NULL;
$arguments1['objectName'] = NULL;
$arguments1['useParentRequest'] = false;
$arguments1['enctype'] = NULL;
$arguments1['method'] = NULL;
$arguments1['name'] = NULL;
$arguments1['onreset'] = NULL;
$arguments1['onsubmit'] = NULL;
$arguments1['class'] = NULL;
$arguments1['dir'] = NULL;
$arguments1['id'] = NULL;
$arguments1['lang'] = NULL;
$arguments1['style'] = NULL;
$arguments1['title'] = NULL;
$arguments1['accesskey'] = NULL;
$arguments1['tabindex'] = NULL;
$arguments1['onclick'] = NULL;
$renderChildrenClosure2 = function() use ($renderingContext, $self) {
$output3 = '';

$output3 .= '
		<div class="neos-row-fluid">
			<legend>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments4 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments5 = array();
$arguments5['id'] = 'packages.index.legend';
$arguments5['source'] = 'Modules';
$arguments5['package'] = 'TYPO3.Neos';
$arguments5['value'] = NULL;
$arguments5['arguments'] = array (
);
$arguments5['quantity'] = NULL;
$arguments5['languageIdentifier'] = NULL;
$renderChildrenClosure6 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper7 = $self->getViewHelper('$viewHelper7', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper7->setArguments($arguments5);
$viewHelper7->setRenderingContext($renderingContext);
$viewHelper7->setRenderChildrenClosure($renderChildrenClosure6);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments4['value'] = $viewHelper7->initializeArgumentsAndRender();
$arguments4['keepQuotes'] = false;
$arguments4['encoding'] = 'UTF-8';
$arguments4['doubleEncode'] = true;
$renderChildrenClosure8 = function() use ($renderingContext, $self) {
return NULL;
};
$value9 = ($arguments4['value'] !== NULL ? $arguments4['value'] : $renderChildrenClosure8());

$output3 .= !is_string($value9) && !(is_object($value9) && method_exists($value9, '__toString')) ? $value9 : htmlspecialchars($value9, ($arguments4['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments4['encoding'], $arguments4['doubleEncode']);

$output3 .= '</legend>
			<br />
			<table class="neos-table">
				<thead>
					<th class="check">
						<label for="check-all" class="neos-checkbox">
							<input type="checkbox" id="check-all" /><span></span>
						</label>
					</th>
					<th>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments10 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments11 = array();
$arguments11['id'] = 'packages.name';
$arguments11['source'] = 'Modules';
$arguments11['package'] = 'TYPO3.Neos';
$arguments11['value'] = NULL;
$arguments11['arguments'] = array (
);
$arguments11['quantity'] = NULL;
$arguments11['languageIdentifier'] = NULL;
$renderChildrenClosure12 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper13 = $self->getViewHelper('$viewHelper13', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper13->setArguments($arguments11);
$viewHelper13->setRenderingContext($renderingContext);
$viewHelper13->setRenderChildrenClosure($renderChildrenClosure12);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments10['value'] = $viewHelper13->initializeArgumentsAndRender();
$arguments10['keepQuotes'] = false;
$arguments10['encoding'] = 'UTF-8';
$arguments10['doubleEncode'] = true;
$renderChildrenClosure14 = function() use ($renderingContext, $self) {
return NULL;
};
$value15 = ($arguments10['value'] !== NULL ? $arguments10['value'] : $renderChildrenClosure14());

$output3 .= !is_string($value15) && !(is_object($value15) && method_exists($value15, '__toString')) ? $value15 : htmlspecialchars($value15, ($arguments10['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments10['encoding'], $arguments10['doubleEncode']);

$output3 .= '</th>
					<th>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments16 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments17 = array();
$arguments17['id'] = 'version';
$arguments17['package'] = 'TYPO3.Neos';
$arguments17['value'] = NULL;
$arguments17['arguments'] = array (
);
$arguments17['source'] = 'Main';
$arguments17['quantity'] = NULL;
$arguments17['languageIdentifier'] = NULL;
$renderChildrenClosure18 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper19 = $self->getViewHelper('$viewHelper19', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper19->setArguments($arguments17);
$viewHelper19->setRenderingContext($renderingContext);
$viewHelper19->setRenderChildrenClosure($renderChildrenClosure18);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments16['value'] = $viewHelper19->initializeArgumentsAndRender();
$arguments16['keepQuotes'] = false;
$arguments16['encoding'] = 'UTF-8';
$arguments16['doubleEncode'] = true;
$renderChildrenClosure20 = function() use ($renderingContext, $self) {
return NULL;
};
$value21 = ($arguments16['value'] !== NULL ? $arguments16['value'] : $renderChildrenClosure20());

$output3 .= !is_string($value21) && !(is_object($value21) && method_exists($value21, '__toString')) ? $value21 : htmlspecialchars($value21, ($arguments16['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments16['encoding'], $arguments16['doubleEncode']);

$output3 .= '</th>
					<th>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments22 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments23 = array();
$arguments23['id'] = 'packages.key';
$arguments23['source'] = 'Modules';
$arguments23['package'] = 'TYPO3.Neos';
$arguments23['value'] = NULL;
$arguments23['arguments'] = array (
);
$arguments23['quantity'] = NULL;
$arguments23['languageIdentifier'] = NULL;
$renderChildrenClosure24 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper25 = $self->getViewHelper('$viewHelper25', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper25->setArguments($arguments23);
$viewHelper25->setRenderingContext($renderingContext);
$viewHelper25->setRenderChildrenClosure($renderChildrenClosure24);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments22['value'] = $viewHelper25->initializeArgumentsAndRender();
$arguments22['keepQuotes'] = false;
$arguments22['encoding'] = 'UTF-8';
$arguments22['doubleEncode'] = true;
$renderChildrenClosure26 = function() use ($renderingContext, $self) {
return NULL;
};
$value27 = ($arguments22['value'] !== NULL ? $arguments22['value'] : $renderChildrenClosure26());

$output3 .= !is_string($value27) && !(is_object($value27) && method_exists($value27, '__toString')) ? $value27 : htmlspecialchars($value27, ($arguments22['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments22['encoding'], $arguments22['doubleEncode']);

$output3 .= '</th>
					<th>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments28 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments29 = array();
$arguments29['id'] = 'packages.type';
$arguments29['source'] = 'Modules';
$arguments29['package'] = 'TYPO3.Neos';
$arguments29['value'] = NULL;
$arguments29['arguments'] = array (
);
$arguments29['quantity'] = NULL;
$arguments29['languageIdentifier'] = NULL;
$renderChildrenClosure30 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper31 = $self->getViewHelper('$viewHelper31', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper31->setArguments($arguments29);
$viewHelper31->setRenderingContext($renderingContext);
$viewHelper31->setRenderChildrenClosure($renderChildrenClosure30);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments28['value'] = $viewHelper31->initializeArgumentsAndRender();
$arguments28['keepQuotes'] = false;
$arguments28['encoding'] = 'UTF-8';
$arguments28['doubleEncode'] = true;
$renderChildrenClosure32 = function() use ($renderingContext, $self) {
return NULL;
};
$value33 = ($arguments28['value'] !== NULL ? $arguments28['value'] : $renderChildrenClosure32());

$output3 .= !is_string($value33) && !(is_object($value33) && method_exists($value33, '__toString')) ? $value33 : htmlspecialchars($value33, ($arguments28['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments28['encoding'], $arguments28['doubleEncode']);

$output3 .= '</th>
					<th>&nbsp;</th>
				</thead>
				<tbody>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments34 = array();
$arguments34['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageGroups', $renderingContext);
$arguments34['key'] = 'packageGroup';
$arguments34['as'] = 'packages';
$arguments34['reverse'] = false;
$arguments34['iteration'] = NULL;
$renderChildrenClosure35 = function() use ($renderingContext, $self) {
$output36 = '';

$output36 .= '
						<tr class="neos-folder" id="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments37 = array();
$arguments37['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageGroup', $renderingContext);
$arguments37['keepQuotes'] = false;
$arguments37['encoding'] = 'UTF-8';
$arguments37['doubleEncode'] = true;
$renderChildrenClosure38 = function() use ($renderingContext, $self) {
return NULL;
};
$value39 = ($arguments37['value'] !== NULL ? $arguments37['value'] : $renderChildrenClosure38());

$output36 .= !is_string($value39) && !(is_object($value39) && method_exists($value39, '__toString')) ? $value39 : htmlspecialchars($value39, ($arguments37['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments37['encoding'], $arguments37['doubleEncode']);

$output36 .= '">
							<td colspan="2" class="neos-priority1">
								<strong>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments40 = array();
$arguments40['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageGroup', $renderingContext);
$arguments40['keepQuotes'] = false;
$arguments40['encoding'] = 'UTF-8';
$arguments40['doubleEncode'] = true;
$renderChildrenClosure41 = function() use ($renderingContext, $self) {
return NULL;
};
$value42 = ($arguments40['value'] !== NULL ? $arguments40['value'] : $renderChildrenClosure41());

$output36 .= !is_string($value42) && !(is_object($value42) && method_exists($value42, '__toString')) ? $value42 : htmlspecialchars($value42, ($arguments40['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments40['encoding'], $arguments40['doubleEncode']);

$output36 .= '</strong>
							</td>
							<td class="neos-priority2">&nbsp;</td>
							<td class="neos-priority3">&nbsp;</td>
							<td class="neos-priority3">&nbsp;</td>
							<td class="neos-priority1 neos-aRight">
								<i class="fold-toggle icon-chevron-up icon-white" data-toggle="fold-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments43 = array();
$arguments43['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageGroup', $renderingContext);
$arguments43['keepQuotes'] = false;
$arguments43['encoding'] = 'UTF-8';
$arguments43['doubleEncode'] = true;
$renderChildrenClosure44 = function() use ($renderingContext, $self) {
return NULL;
};
$value45 = ($arguments43['value'] !== NULL ? $arguments43['value'] : $renderChildrenClosure44());

$output36 .= !is_string($value45) && !(is_object($value45) && method_exists($value45, '__toString')) ? $value45 : htmlspecialchars($value45, ($arguments43['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments43['encoding'], $arguments43['doubleEncode']);

$output36 .= '"></i>
							</td>
						</tr>
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments46 = array();
$arguments46['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packages', $renderingContext);
$arguments46['key'] = 'packageKey';
$arguments46['as'] = 'package';
$arguments46['reverse'] = false;
$arguments46['iteration'] = NULL;
$renderChildrenClosure47 = function() use ($renderingContext, $self) {
$output48 = '';

$output48 .= '
							<tr class="fold-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments49 = array();
$arguments49['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageGroup', $renderingContext);
$arguments49['keepQuotes'] = false;
$arguments49['encoding'] = 'UTF-8';
$arguments49['doubleEncode'] = true;
$renderChildrenClosure50 = function() use ($renderingContext, $self) {
return NULL;
};
$value51 = ($arguments49['value'] !== NULL ? $arguments49['value'] : $renderChildrenClosure50());

$output48 .= !is_string($value51) && !(is_object($value51) && method_exists($value51, '__toString')) ? $value51 : htmlspecialchars($value51, ($arguments49['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments49['encoding'], $arguments49['doubleEncode']);
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments52 = array();
// Rendering Boolean node
$arguments52['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.isActive', $renderingContext));
$arguments52['else'] = ' muted';
$arguments52['then'] = NULL;
$renderChildrenClosure53 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper54 = $self->getViewHelper('$viewHelper54', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper54->setArguments($arguments52);
$viewHelper54->setRenderingContext($renderingContext);
$viewHelper54->setRenderChildrenClosure($renderChildrenClosure53);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output48 .= $viewHelper54->initializeArgumentsAndRender();

$output48 .= '"';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments55 = array();
// Rendering Boolean node
$arguments55['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.description', $renderingContext));
$output56 = '';

$output56 .= ' title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments57 = array();
$arguments57['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.description', $renderingContext);
$arguments57['keepQuotes'] = false;
$arguments57['encoding'] = 'UTF-8';
$arguments57['doubleEncode'] = true;
$renderChildrenClosure58 = function() use ($renderingContext, $self) {
return NULL;
};
$value59 = ($arguments57['value'] !== NULL ? $arguments57['value'] : $renderChildrenClosure58());

$output56 .= !is_string($value59) && !(is_object($value59) && method_exists($value59, '__toString')) ? $value59 : htmlspecialchars($value59, ($arguments57['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments57['encoding'], $arguments57['doubleEncode']);

$output56 .= '"';
$arguments55['then'] = $output56;
$arguments55['else'] = NULL;
$renderChildrenClosure60 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper61 = $self->getViewHelper('$viewHelper61', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper61->setArguments($arguments55);
$viewHelper61->setRenderingContext($renderingContext);
$viewHelper61->setRenderChildrenClosure($renderChildrenClosure60);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output48 .= $viewHelper61->initializeArgumentsAndRender();

$output48 .= ' data-neos-toggle="tooltip">
								<td class="check neos-priority1">
									<label for="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments62 = array();
$arguments62['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments62['keepQuotes'] = false;
$arguments62['encoding'] = 'UTF-8';
$arguments62['doubleEncode'] = true;
$renderChildrenClosure63 = function() use ($renderingContext, $self) {
return NULL;
};
$value64 = ($arguments62['value'] !== NULL ? $arguments62['value'] : $renderChildrenClosure63());

$output48 .= !is_string($value64) && !(is_object($value64) && method_exists($value64, '__toString')) ? $value64 : htmlspecialchars($value64, ($arguments62['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments62['encoding'], $arguments62['doubleEncode']);

$output48 .= '" class="neos-checkbox">
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\CheckboxViewHelper
$arguments65 = array();
$arguments65['name'] = 'packageKeys[]';
$arguments65['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments65['id'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments65['additionalAttributes'] = NULL;
$arguments65['data'] = NULL;
$arguments65['checked'] = NULL;
$arguments65['multiple'] = NULL;
$arguments65['property'] = NULL;
$arguments65['disabled'] = NULL;
$arguments65['errorClass'] = 'f3-form-error';
$arguments65['class'] = NULL;
$arguments65['dir'] = NULL;
$arguments65['lang'] = NULL;
$arguments65['style'] = NULL;
$arguments65['title'] = NULL;
$arguments65['accesskey'] = NULL;
$arguments65['tabindex'] = NULL;
$arguments65['onclick'] = NULL;
$renderChildrenClosure66 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper67 = $self->getViewHelper('$viewHelper67', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\CheckboxViewHelper');
$viewHelper67->setArguments($arguments65);
$viewHelper67->setRenderingContext($renderingContext);
$viewHelper67->setRenderChildrenClosure($renderChildrenClosure66);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\CheckboxViewHelper

$output48 .= $viewHelper67->initializeArgumentsAndRender();

$output48 .= '<span></span>
									</label>
								</td>
								<td class="package-name neos-priority1">
									<label for="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments68 = array();
$arguments68['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments68['keepQuotes'] = false;
$arguments68['encoding'] = 'UTF-8';
$arguments68['doubleEncode'] = true;
$renderChildrenClosure69 = function() use ($renderingContext, $self) {
return NULL;
};
$value70 = ($arguments68['value'] !== NULL ? $arguments68['value'] : $renderChildrenClosure69());

$output48 .= !is_string($value70) && !(is_object($value70) && method_exists($value70, '__toString')) ? $value70 : htmlspecialchars($value70, ($arguments68['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments68['encoding'], $arguments68['doubleEncode']);

$output48 .= '">
										<span class="neos-label">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments71 = array();
$arguments71['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.name', $renderingContext);
$arguments71['keepQuotes'] = false;
$arguments71['encoding'] = 'UTF-8';
$arguments71['doubleEncode'] = true;
$renderChildrenClosure72 = function() use ($renderingContext, $self) {
return NULL;
};
$value73 = ($arguments71['value'] !== NULL ? $arguments71['value'] : $renderChildrenClosure72());

$output48 .= !is_string($value73) && !(is_object($value73) && method_exists($value73, '__toString')) ? $value73 : htmlspecialchars($value73, ($arguments71['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments71['encoding'], $arguments71['doubleEncode']);

$output48 .= '</span>
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments74 = array();
// Rendering Boolean node
$arguments74['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.isActive', $renderingContext));
$arguments74['then'] = NULL;
$arguments74['else'] = NULL;
$renderChildrenClosure75 = function() use ($renderingContext, $self) {
$output76 = '';

$output76 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments77 = array();
$renderChildrenClosure78 = function() use ($renderingContext, $self) {
$output79 = '';

$output79 .= '
												<span class="neos-badge neos-badge-warning">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments80 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments81 = array();
$arguments81['id'] = 'packages.deactivated';
$arguments81['source'] = 'Modules';
$arguments81['package'] = 'TYPO3.Neos';
$arguments81['value'] = NULL;
$arguments81['arguments'] = array (
);
$arguments81['quantity'] = NULL;
$arguments81['languageIdentifier'] = NULL;
$renderChildrenClosure82 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper83 = $self->getViewHelper('$viewHelper83', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper83->setArguments($arguments81);
$viewHelper83->setRenderingContext($renderingContext);
$viewHelper83->setRenderChildrenClosure($renderChildrenClosure82);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments80['value'] = $viewHelper83->initializeArgumentsAndRender();
$arguments80['keepQuotes'] = false;
$arguments80['encoding'] = 'UTF-8';
$arguments80['doubleEncode'] = true;
$renderChildrenClosure84 = function() use ($renderingContext, $self) {
return NULL;
};
$value85 = ($arguments80['value'] !== NULL ? $arguments80['value'] : $renderChildrenClosure84());

$output79 .= !is_string($value85) && !(is_object($value85) && method_exists($value85, '__toString')) ? $value85 : htmlspecialchars($value85, ($arguments80['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments80['encoding'], $arguments80['doubleEncode']);

$output79 .= '</span>
											';
return $output79;
};
$viewHelper86 = $self->getViewHelper('$viewHelper86', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper86->setArguments($arguments77);
$viewHelper86->setRenderingContext($renderingContext);
$viewHelper86->setRenderChildrenClosure($renderChildrenClosure78);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output76 .= $viewHelper86->initializeArgumentsAndRender();

$output76 .= '
										';
return $output76;
};
$arguments74['__elseClosure'] = function() use ($renderingContext, $self) {
$output87 = '';

$output87 .= '
												<span class="neos-badge neos-badge-warning">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments88 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments89 = array();
$arguments89['id'] = 'packages.deactivated';
$arguments89['source'] = 'Modules';
$arguments89['package'] = 'TYPO3.Neos';
$arguments89['value'] = NULL;
$arguments89['arguments'] = array (
);
$arguments89['quantity'] = NULL;
$arguments89['languageIdentifier'] = NULL;
$renderChildrenClosure90 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper91 = $self->getViewHelper('$viewHelper91', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper91->setArguments($arguments89);
$viewHelper91->setRenderingContext($renderingContext);
$viewHelper91->setRenderChildrenClosure($renderChildrenClosure90);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments88['value'] = $viewHelper91->initializeArgumentsAndRender();
$arguments88['keepQuotes'] = false;
$arguments88['encoding'] = 'UTF-8';
$arguments88['doubleEncode'] = true;
$renderChildrenClosure92 = function() use ($renderingContext, $self) {
return NULL;
};
$value93 = ($arguments88['value'] !== NULL ? $arguments88['value'] : $renderChildrenClosure92());

$output87 .= !is_string($value93) && !(is_object($value93) && method_exists($value93, '__toString')) ? $value93 : htmlspecialchars($value93, ($arguments88['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments88['encoding'], $arguments88['doubleEncode']);

$output87 .= '</span>
											';
return $output87;
};
$viewHelper94 = $self->getViewHelper('$viewHelper94', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper94->setArguments($arguments74);
$viewHelper94->setRenderingContext($renderingContext);
$viewHelper94->setRenderChildrenClosure($renderChildrenClosure75);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output48 .= $viewHelper94->initializeArgumentsAndRender();

$output48 .= '
									</label>
								</td>
								<td class="package-version neos-priority2">
									<label for="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments95 = array();
$arguments95['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments95['keepQuotes'] = false;
$arguments95['encoding'] = 'UTF-8';
$arguments95['doubleEncode'] = true;
$renderChildrenClosure96 = function() use ($renderingContext, $self) {
return NULL;
};
$value97 = ($arguments95['value'] !== NULL ? $arguments95['value'] : $renderChildrenClosure96());

$output48 .= !is_string($value97) && !(is_object($value97) && method_exists($value97, '__toString')) ? $value97 : htmlspecialchars($value97, ($arguments95['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments95['encoding'], $arguments95['doubleEncode']);

$output48 .= '">
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments98 = array();
// Rendering Boolean node
$arguments98['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.version', $renderingContext));
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments99 = array();
$arguments99['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.version', $renderingContext);
$arguments99['keepQuotes'] = false;
$arguments99['encoding'] = 'UTF-8';
$arguments99['doubleEncode'] = true;
$renderChildrenClosure100 = function() use ($renderingContext, $self) {
return NULL;
};
$value101 = ($arguments99['value'] !== NULL ? $arguments99['value'] : $renderChildrenClosure100());
$arguments98['then'] = !is_string($value101) && !(is_object($value101) && method_exists($value101, '__toString')) ? $value101 : htmlspecialchars($value101, ($arguments99['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments99['encoding'], $arguments99['doubleEncode']);
$arguments98['else'] = '&nbsp;';
$renderChildrenClosure102 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper103 = $self->getViewHelper('$viewHelper103', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper103->setArguments($arguments98);
$viewHelper103->setRenderingContext($renderingContext);
$viewHelper103->setRenderChildrenClosure($renderChildrenClosure102);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output48 .= $viewHelper103->initializeArgumentsAndRender();

$output48 .= '
									</label>
								</td>
								<td class="package-key neos-priority3">
									<label for="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments104 = array();
$arguments104['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments104['keepQuotes'] = false;
$arguments104['encoding'] = 'UTF-8';
$arguments104['doubleEncode'] = true;
$renderChildrenClosure105 = function() use ($renderingContext, $self) {
return NULL;
};
$value106 = ($arguments104['value'] !== NULL ? $arguments104['value'] : $renderChildrenClosure105());

$output48 .= !is_string($value106) && !(is_object($value106) && method_exists($value106, '__toString')) ? $value106 : htmlspecialchars($value106, ($arguments104['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments104['encoding'], $arguments104['doubleEncode']);

$output48 .= '"">
										<span class="neos-label">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments107 = array();
$arguments107['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments107['keepQuotes'] = false;
$arguments107['encoding'] = 'UTF-8';
$arguments107['doubleEncode'] = true;
$renderChildrenClosure108 = function() use ($renderingContext, $self) {
return NULL;
};
$value109 = ($arguments107['value'] !== NULL ? $arguments107['value'] : $renderChildrenClosure108());

$output48 .= !is_string($value109) && !(is_object($value109) && method_exists($value109, '__toString')) ? $value109 : htmlspecialchars($value109, ($arguments107['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments107['encoding'], $arguments107['doubleEncode']);

$output48 .= '</span>
									</label>
								</td>
								<td class="package-type neos-priority3">
									<label for="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments110 = array();
$arguments110['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments110['keepQuotes'] = false;
$arguments110['encoding'] = 'UTF-8';
$arguments110['doubleEncode'] = true;
$renderChildrenClosure111 = function() use ($renderingContext, $self) {
return NULL;
};
$value112 = ($arguments110['value'] !== NULL ? $arguments110['value'] : $renderChildrenClosure111());

$output48 .= !is_string($value112) && !(is_object($value112) && method_exists($value112, '__toString')) ? $value112 : htmlspecialchars($value112, ($arguments110['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments110['encoding'], $arguments110['doubleEncode']);

$output48 .= '">
										<span class="neos-label">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments113 = array();
$arguments113['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.type', $renderingContext);
$arguments113['keepQuotes'] = false;
$arguments113['encoding'] = 'UTF-8';
$arguments113['doubleEncode'] = true;
$renderChildrenClosure114 = function() use ($renderingContext, $self) {
return NULL;
};
$value115 = ($arguments113['value'] !== NULL ? $arguments113['value'] : $renderChildrenClosure114());

$output48 .= !is_string($value115) && !(is_object($value115) && method_exists($value115, '__toString')) ? $value115 : htmlspecialchars($value115, ($arguments113['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments113['encoding'], $arguments113['doubleEncode']);

$output48 .= '</span>
									</label>
								</td>
								<td class="neos-action neos-priority1">
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments116 = array();
// Rendering Boolean node
$arguments116['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.isActive', $renderingContext));
$arguments116['then'] = NULL;
$arguments116['else'] = NULL;
$renderChildrenClosure117 = function() use ($renderingContext, $self) {
$output118 = '';

$output118 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments119 = array();
$renderChildrenClosure120 = function() use ($renderingContext, $self) {
$output121 = '';

$output121 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments122 = array();
// Rendering Boolean node
$arguments122['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'isDevelopmentContext', $renderingContext));
$arguments122['then'] = NULL;
$arguments122['else'] = NULL;
$renderChildrenClosure123 = function() use ($renderingContext, $self) {
$output124 = '';

$output124 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments125 = array();
// Rendering Boolean node
$arguments125['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.isFrozen', $renderingContext));
$arguments125['then'] = NULL;
$arguments125['else'] = NULL;
$renderChildrenClosure126 = function() use ($renderingContext, $self) {
$output127 = '';

$output127 .= '
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments128 = array();
$renderChildrenClosure129 = function() use ($renderingContext, $self) {
$output130 = '';

$output130 .= '
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments131 = array();
$arguments131['action'] = 'unfreeze';
$arguments131['class'] = 'neos-button neos-button-freeze neos-active';
// Rendering Array
$array132 = array();
$array132['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments131['arguments'] = $array132;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments133 = array();
$arguments133['id'] = 'packages.tooltip.unfreeze';
$arguments133['source'] = 'Modules';
$arguments133['package'] = 'TYPO3.Neos';
$arguments133['value'] = NULL;
$arguments133['arguments'] = array (
);
$arguments133['quantity'] = NULL;
$arguments133['languageIdentifier'] = NULL;
$renderChildrenClosure134 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper135 = $self->getViewHelper('$viewHelper135', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper135->setArguments($arguments133);
$viewHelper135->setRenderingContext($renderingContext);
$viewHelper135->setRenderChildrenClosure($renderChildrenClosure134);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments131['title'] = $viewHelper135->initializeArgumentsAndRender();
// Rendering Array
$array136 = array();
$array136['data-neos-toggle'] = 'tooltip';
$arguments131['additionalAttributes'] = $array136;
$arguments131['data'] = NULL;
$arguments131['controller'] = NULL;
$arguments131['package'] = NULL;
$arguments131['subpackage'] = NULL;
$arguments131['section'] = '';
$arguments131['format'] = '';
$arguments131['additionalParams'] = array (
);
$arguments131['addQueryString'] = false;
$arguments131['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments131['useParentRequest'] = false;
$arguments131['absolute'] = true;
$arguments131['dir'] = NULL;
$arguments131['id'] = NULL;
$arguments131['lang'] = NULL;
$arguments131['style'] = NULL;
$arguments131['accesskey'] = NULL;
$arguments131['tabindex'] = NULL;
$arguments131['onclick'] = NULL;
$arguments131['name'] = NULL;
$arguments131['rel'] = NULL;
$arguments131['rev'] = NULL;
$arguments131['target'] = NULL;
$renderChildrenClosure137 = function() use ($renderingContext, $self) {
return '
															<i class="icon-asterisk icon-white"></i>
														';
};
$viewHelper138 = $self->getViewHelper('$viewHelper138', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper138->setArguments($arguments131);
$viewHelper138->setRenderingContext($renderingContext);
$viewHelper138->setRenderChildrenClosure($renderChildrenClosure137);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output130 .= $viewHelper138->initializeArgumentsAndRender();

$output130 .= '
													';
return $output130;
};
$viewHelper139 = $self->getViewHelper('$viewHelper139', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper139->setArguments($arguments128);
$viewHelper139->setRenderingContext($renderingContext);
$viewHelper139->setRenderChildrenClosure($renderChildrenClosure129);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output127 .= $viewHelper139->initializeArgumentsAndRender();

$output127 .= '
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments140 = array();
$renderChildrenClosure141 = function() use ($renderingContext, $self) {
$output142 = '';

$output142 .= '
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments143 = array();
$arguments143['action'] = 'freeze';
$arguments143['class'] = 'neos-button neos-button-freeze';
// Rendering Array
$array144 = array();
$array144['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments143['arguments'] = $array144;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments145 = array();
$arguments145['id'] = 'packages.tooltip.freeze';
$arguments145['source'] = 'Modules';
$arguments145['package'] = 'TYPO3.Neos';
$arguments145['value'] = NULL;
$arguments145['arguments'] = array (
);
$arguments145['quantity'] = NULL;
$arguments145['languageIdentifier'] = NULL;
$renderChildrenClosure146 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper147 = $self->getViewHelper('$viewHelper147', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper147->setArguments($arguments145);
$viewHelper147->setRenderingContext($renderingContext);
$viewHelper147->setRenderChildrenClosure($renderChildrenClosure146);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments143['title'] = $viewHelper147->initializeArgumentsAndRender();
// Rendering Array
$array148 = array();
$array148['data-neos-toggle'] = 'tooltip';
$arguments143['additionalAttributes'] = $array148;
$arguments143['data'] = NULL;
$arguments143['controller'] = NULL;
$arguments143['package'] = NULL;
$arguments143['subpackage'] = NULL;
$arguments143['section'] = '';
$arguments143['format'] = '';
$arguments143['additionalParams'] = array (
);
$arguments143['addQueryString'] = false;
$arguments143['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments143['useParentRequest'] = false;
$arguments143['absolute'] = true;
$arguments143['dir'] = NULL;
$arguments143['id'] = NULL;
$arguments143['lang'] = NULL;
$arguments143['style'] = NULL;
$arguments143['accesskey'] = NULL;
$arguments143['tabindex'] = NULL;
$arguments143['onclick'] = NULL;
$arguments143['name'] = NULL;
$arguments143['rel'] = NULL;
$arguments143['rev'] = NULL;
$arguments143['target'] = NULL;
$renderChildrenClosure149 = function() use ($renderingContext, $self) {
return '
															<i class="icon-asterisk icon-white"></i>
														';
};
$viewHelper150 = $self->getViewHelper('$viewHelper150', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper150->setArguments($arguments143);
$viewHelper150->setRenderingContext($renderingContext);
$viewHelper150->setRenderChildrenClosure($renderChildrenClosure149);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output142 .= $viewHelper150->initializeArgumentsAndRender();

$output142 .= '
													';
return $output142;
};
$viewHelper151 = $self->getViewHelper('$viewHelper151', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper151->setArguments($arguments140);
$viewHelper151->setRenderingContext($renderingContext);
$viewHelper151->setRenderChildrenClosure($renderChildrenClosure141);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output127 .= $viewHelper151->initializeArgumentsAndRender();

$output127 .= '
												';
return $output127;
};
$arguments125['__thenClosure'] = function() use ($renderingContext, $self) {
$output152 = '';

$output152 .= '
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments153 = array();
$arguments153['action'] = 'unfreeze';
$arguments153['class'] = 'neos-button neos-button-freeze neos-active';
// Rendering Array
$array154 = array();
$array154['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments153['arguments'] = $array154;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments155 = array();
$arguments155['id'] = 'packages.tooltip.unfreeze';
$arguments155['source'] = 'Modules';
$arguments155['package'] = 'TYPO3.Neos';
$arguments155['value'] = NULL;
$arguments155['arguments'] = array (
);
$arguments155['quantity'] = NULL;
$arguments155['languageIdentifier'] = NULL;
$renderChildrenClosure156 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper157 = $self->getViewHelper('$viewHelper157', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper157->setArguments($arguments155);
$viewHelper157->setRenderingContext($renderingContext);
$viewHelper157->setRenderChildrenClosure($renderChildrenClosure156);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments153['title'] = $viewHelper157->initializeArgumentsAndRender();
// Rendering Array
$array158 = array();
$array158['data-neos-toggle'] = 'tooltip';
$arguments153['additionalAttributes'] = $array158;
$arguments153['data'] = NULL;
$arguments153['controller'] = NULL;
$arguments153['package'] = NULL;
$arguments153['subpackage'] = NULL;
$arguments153['section'] = '';
$arguments153['format'] = '';
$arguments153['additionalParams'] = array (
);
$arguments153['addQueryString'] = false;
$arguments153['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments153['useParentRequest'] = false;
$arguments153['absolute'] = true;
$arguments153['dir'] = NULL;
$arguments153['id'] = NULL;
$arguments153['lang'] = NULL;
$arguments153['style'] = NULL;
$arguments153['accesskey'] = NULL;
$arguments153['tabindex'] = NULL;
$arguments153['onclick'] = NULL;
$arguments153['name'] = NULL;
$arguments153['rel'] = NULL;
$arguments153['rev'] = NULL;
$arguments153['target'] = NULL;
$renderChildrenClosure159 = function() use ($renderingContext, $self) {
return '
															<i class="icon-asterisk icon-white"></i>
														';
};
$viewHelper160 = $self->getViewHelper('$viewHelper160', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper160->setArguments($arguments153);
$viewHelper160->setRenderingContext($renderingContext);
$viewHelper160->setRenderChildrenClosure($renderChildrenClosure159);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output152 .= $viewHelper160->initializeArgumentsAndRender();

$output152 .= '
													';
return $output152;
};
$arguments125['__elseClosure'] = function() use ($renderingContext, $self) {
$output161 = '';

$output161 .= '
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments162 = array();
$arguments162['action'] = 'freeze';
$arguments162['class'] = 'neos-button neos-button-freeze';
// Rendering Array
$array163 = array();
$array163['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments162['arguments'] = $array163;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments164 = array();
$arguments164['id'] = 'packages.tooltip.freeze';
$arguments164['source'] = 'Modules';
$arguments164['package'] = 'TYPO3.Neos';
$arguments164['value'] = NULL;
$arguments164['arguments'] = array (
);
$arguments164['quantity'] = NULL;
$arguments164['languageIdentifier'] = NULL;
$renderChildrenClosure165 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper166 = $self->getViewHelper('$viewHelper166', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper166->setArguments($arguments164);
$viewHelper166->setRenderingContext($renderingContext);
$viewHelper166->setRenderChildrenClosure($renderChildrenClosure165);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments162['title'] = $viewHelper166->initializeArgumentsAndRender();
// Rendering Array
$array167 = array();
$array167['data-neos-toggle'] = 'tooltip';
$arguments162['additionalAttributes'] = $array167;
$arguments162['data'] = NULL;
$arguments162['controller'] = NULL;
$arguments162['package'] = NULL;
$arguments162['subpackage'] = NULL;
$arguments162['section'] = '';
$arguments162['format'] = '';
$arguments162['additionalParams'] = array (
);
$arguments162['addQueryString'] = false;
$arguments162['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments162['useParentRequest'] = false;
$arguments162['absolute'] = true;
$arguments162['dir'] = NULL;
$arguments162['id'] = NULL;
$arguments162['lang'] = NULL;
$arguments162['style'] = NULL;
$arguments162['accesskey'] = NULL;
$arguments162['tabindex'] = NULL;
$arguments162['onclick'] = NULL;
$arguments162['name'] = NULL;
$arguments162['rel'] = NULL;
$arguments162['rev'] = NULL;
$arguments162['target'] = NULL;
$renderChildrenClosure168 = function() use ($renderingContext, $self) {
return '
															<i class="icon-asterisk icon-white"></i>
														';
};
$viewHelper169 = $self->getViewHelper('$viewHelper169', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper169->setArguments($arguments162);
$viewHelper169->setRenderingContext($renderingContext);
$viewHelper169->setRenderChildrenClosure($renderChildrenClosure168);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output161 .= $viewHelper169->initializeArgumentsAndRender();

$output161 .= '
													';
return $output161;
};
$viewHelper170 = $self->getViewHelper('$viewHelper170', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper170->setArguments($arguments125);
$viewHelper170->setRenderingContext($renderingContext);
$viewHelper170->setRenderChildrenClosure($renderChildrenClosure126);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output124 .= $viewHelper170->initializeArgumentsAndRender();

$output124 .= '
											';
return $output124;
};
$viewHelper171 = $self->getViewHelper('$viewHelper171', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper171->setArguments($arguments122);
$viewHelper171->setRenderingContext($renderingContext);
$viewHelper171->setRenderChildrenClosure($renderChildrenClosure123);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output121 .= $viewHelper171->initializeArgumentsAndRender();

$output121 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments172 = array();
// Rendering Boolean node
$arguments172['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.isProtected', $renderingContext));
$arguments172['then'] = NULL;
$arguments172['else'] = NULL;
$renderChildrenClosure173 = function() use ($renderingContext, $self) {
$output174 = '';

$output174 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments175 = array();
$renderChildrenClosure176 = function() use ($renderingContext, $self) {
$output177 = '';

$output177 .= '
													<button class="neos-button neos-button-warning neos-disabled" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments178 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments179 = array();
$arguments179['id'] = 'packages.tooltip.noDeactivate';
$arguments179['source'] = 'Modules';
$arguments179['package'] = 'TYPO3.Neos';
$arguments179['value'] = NULL;
$arguments179['arguments'] = array (
);
$arguments179['quantity'] = NULL;
$arguments179['languageIdentifier'] = NULL;
$renderChildrenClosure180 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper181 = $self->getViewHelper('$viewHelper181', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper181->setArguments($arguments179);
$viewHelper181->setRenderingContext($renderingContext);
$viewHelper181->setRenderChildrenClosure($renderChildrenClosure180);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments178['value'] = $viewHelper181->initializeArgumentsAndRender();
$arguments178['keepQuotes'] = false;
$arguments178['encoding'] = 'UTF-8';
$arguments178['doubleEncode'] = true;
$renderChildrenClosure182 = function() use ($renderingContext, $self) {
return NULL;
};
$value183 = ($arguments178['value'] !== NULL ? $arguments178['value'] : $renderChildrenClosure182());

$output177 .= !is_string($value183) && !(is_object($value183) && method_exists($value183, '__toString')) ? $value183 : htmlspecialchars($value183, ($arguments178['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments178['encoding'], $arguments178['doubleEncode']);

$output177 .= '" disabled="disabled" data-neos-toggle="tooltip">
														<i class="icon-pause icon-white"></i>
													</button>
												';
return $output177;
};
$viewHelper184 = $self->getViewHelper('$viewHelper184', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper184->setArguments($arguments175);
$viewHelper184->setRenderingContext($renderingContext);
$viewHelper184->setRenderChildrenClosure($renderChildrenClosure176);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output174 .= $viewHelper184->initializeArgumentsAndRender();

$output174 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments185 = array();
$renderChildrenClosure186 = function() use ($renderingContext, $self) {
$output187 = '';

$output187 .= '
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments188 = array();
$arguments188['action'] = 'deactivate';
$arguments188['class'] = 'neos-button neos-button-warning';
// Rendering Array
$array189 = array();
$array189['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments188['arguments'] = $array189;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments190 = array();
$arguments190['id'] = 'packages.tooltip.deactivate';
$arguments190['source'] = 'Modules';
$arguments190['package'] = 'TYPO3.Neos';
$arguments190['value'] = NULL;
$arguments190['arguments'] = array (
);
$arguments190['quantity'] = NULL;
$arguments190['languageIdentifier'] = NULL;
$renderChildrenClosure191 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper192 = $self->getViewHelper('$viewHelper192', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper192->setArguments($arguments190);
$viewHelper192->setRenderingContext($renderingContext);
$viewHelper192->setRenderChildrenClosure($renderChildrenClosure191);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments188['title'] = $viewHelper192->initializeArgumentsAndRender();
// Rendering Array
$array193 = array();
$array193['data-neos-toggle'] = 'tooltip';
$arguments188['additionalAttributes'] = $array193;
$arguments188['data'] = NULL;
$arguments188['controller'] = NULL;
$arguments188['package'] = NULL;
$arguments188['subpackage'] = NULL;
$arguments188['section'] = '';
$arguments188['format'] = '';
$arguments188['additionalParams'] = array (
);
$arguments188['addQueryString'] = false;
$arguments188['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments188['useParentRequest'] = false;
$arguments188['absolute'] = true;
$arguments188['dir'] = NULL;
$arguments188['id'] = NULL;
$arguments188['lang'] = NULL;
$arguments188['style'] = NULL;
$arguments188['accesskey'] = NULL;
$arguments188['tabindex'] = NULL;
$arguments188['onclick'] = NULL;
$arguments188['name'] = NULL;
$arguments188['rel'] = NULL;
$arguments188['rev'] = NULL;
$arguments188['target'] = NULL;
$renderChildrenClosure194 = function() use ($renderingContext, $self) {
return '
														<i class="icon-pause icon-white"></i>
													';
};
$viewHelper195 = $self->getViewHelper('$viewHelper195', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper195->setArguments($arguments188);
$viewHelper195->setRenderingContext($renderingContext);
$viewHelper195->setRenderChildrenClosure($renderChildrenClosure194);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output187 .= $viewHelper195->initializeArgumentsAndRender();

$output187 .= '
												';
return $output187;
};
$viewHelper196 = $self->getViewHelper('$viewHelper196', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper196->setArguments($arguments185);
$viewHelper196->setRenderingContext($renderingContext);
$viewHelper196->setRenderChildrenClosure($renderChildrenClosure186);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output174 .= $viewHelper196->initializeArgumentsAndRender();

$output174 .= '
											';
return $output174;
};
$arguments172['__thenClosure'] = function() use ($renderingContext, $self) {
$output197 = '';

$output197 .= '
													<button class="neos-button neos-button-warning neos-disabled" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments198 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments199 = array();
$arguments199['id'] = 'packages.tooltip.noDeactivate';
$arguments199['source'] = 'Modules';
$arguments199['package'] = 'TYPO3.Neos';
$arguments199['value'] = NULL;
$arguments199['arguments'] = array (
);
$arguments199['quantity'] = NULL;
$arguments199['languageIdentifier'] = NULL;
$renderChildrenClosure200 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper201 = $self->getViewHelper('$viewHelper201', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper201->setArguments($arguments199);
$viewHelper201->setRenderingContext($renderingContext);
$viewHelper201->setRenderChildrenClosure($renderChildrenClosure200);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments198['value'] = $viewHelper201->initializeArgumentsAndRender();
$arguments198['keepQuotes'] = false;
$arguments198['encoding'] = 'UTF-8';
$arguments198['doubleEncode'] = true;
$renderChildrenClosure202 = function() use ($renderingContext, $self) {
return NULL;
};
$value203 = ($arguments198['value'] !== NULL ? $arguments198['value'] : $renderChildrenClosure202());

$output197 .= !is_string($value203) && !(is_object($value203) && method_exists($value203, '__toString')) ? $value203 : htmlspecialchars($value203, ($arguments198['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments198['encoding'], $arguments198['doubleEncode']);

$output197 .= '" disabled="disabled" data-neos-toggle="tooltip">
														<i class="icon-pause icon-white"></i>
													</button>
												';
return $output197;
};
$arguments172['__elseClosure'] = function() use ($renderingContext, $self) {
$output204 = '';

$output204 .= '
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments205 = array();
$arguments205['action'] = 'deactivate';
$arguments205['class'] = 'neos-button neos-button-warning';
// Rendering Array
$array206 = array();
$array206['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments205['arguments'] = $array206;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments207 = array();
$arguments207['id'] = 'packages.tooltip.deactivate';
$arguments207['source'] = 'Modules';
$arguments207['package'] = 'TYPO3.Neos';
$arguments207['value'] = NULL;
$arguments207['arguments'] = array (
);
$arguments207['quantity'] = NULL;
$arguments207['languageIdentifier'] = NULL;
$renderChildrenClosure208 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper209 = $self->getViewHelper('$viewHelper209', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper209->setArguments($arguments207);
$viewHelper209->setRenderingContext($renderingContext);
$viewHelper209->setRenderChildrenClosure($renderChildrenClosure208);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments205['title'] = $viewHelper209->initializeArgumentsAndRender();
// Rendering Array
$array210 = array();
$array210['data-neos-toggle'] = 'tooltip';
$arguments205['additionalAttributes'] = $array210;
$arguments205['data'] = NULL;
$arguments205['controller'] = NULL;
$arguments205['package'] = NULL;
$arguments205['subpackage'] = NULL;
$arguments205['section'] = '';
$arguments205['format'] = '';
$arguments205['additionalParams'] = array (
);
$arguments205['addQueryString'] = false;
$arguments205['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments205['useParentRequest'] = false;
$arguments205['absolute'] = true;
$arguments205['dir'] = NULL;
$arguments205['id'] = NULL;
$arguments205['lang'] = NULL;
$arguments205['style'] = NULL;
$arguments205['accesskey'] = NULL;
$arguments205['tabindex'] = NULL;
$arguments205['onclick'] = NULL;
$arguments205['name'] = NULL;
$arguments205['rel'] = NULL;
$arguments205['rev'] = NULL;
$arguments205['target'] = NULL;
$renderChildrenClosure211 = function() use ($renderingContext, $self) {
return '
														<i class="icon-pause icon-white"></i>
													';
};
$viewHelper212 = $self->getViewHelper('$viewHelper212', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper212->setArguments($arguments205);
$viewHelper212->setRenderingContext($renderingContext);
$viewHelper212->setRenderChildrenClosure($renderChildrenClosure211);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output204 .= $viewHelper212->initializeArgumentsAndRender();

$output204 .= '
												';
return $output204;
};
$viewHelper213 = $self->getViewHelper('$viewHelper213', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper213->setArguments($arguments172);
$viewHelper213->setRenderingContext($renderingContext);
$viewHelper213->setRenderChildrenClosure($renderChildrenClosure173);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output121 .= $viewHelper213->initializeArgumentsAndRender();

$output121 .= '
										';
return $output121;
};
$viewHelper214 = $self->getViewHelper('$viewHelper214', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper214->setArguments($arguments119);
$viewHelper214->setRenderingContext($renderingContext);
$viewHelper214->setRenderChildrenClosure($renderChildrenClosure120);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output118 .= $viewHelper214->initializeArgumentsAndRender();

$output118 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments215 = array();
$renderChildrenClosure216 = function() use ($renderingContext, $self) {
$output217 = '';

$output217 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments218 = array();
$arguments218['action'] = 'activate';
$arguments218['class'] = 'neos-button neos-button-success';
// Rendering Array
$array219 = array();
$array219['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments218['arguments'] = $array219;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments220 = array();
$arguments220['id'] = 'packages.tooltip.activate';
$arguments220['source'] = 'Modules';
$arguments220['package'] = 'TYPO3.Neos';
$arguments220['value'] = NULL;
$arguments220['arguments'] = array (
);
$arguments220['quantity'] = NULL;
$arguments220['languageIdentifier'] = NULL;
$renderChildrenClosure221 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper222 = $self->getViewHelper('$viewHelper222', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper222->setArguments($arguments220);
$viewHelper222->setRenderingContext($renderingContext);
$viewHelper222->setRenderChildrenClosure($renderChildrenClosure221);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments218['title'] = $viewHelper222->initializeArgumentsAndRender();
// Rendering Array
$array223 = array();
$array223['data-neos-toggle'] = 'tooltip';
$arguments218['additionalAttributes'] = $array223;
$arguments218['data'] = NULL;
$arguments218['controller'] = NULL;
$arguments218['package'] = NULL;
$arguments218['subpackage'] = NULL;
$arguments218['section'] = '';
$arguments218['format'] = '';
$arguments218['additionalParams'] = array (
);
$arguments218['addQueryString'] = false;
$arguments218['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments218['useParentRequest'] = false;
$arguments218['absolute'] = true;
$arguments218['dir'] = NULL;
$arguments218['id'] = NULL;
$arguments218['lang'] = NULL;
$arguments218['style'] = NULL;
$arguments218['accesskey'] = NULL;
$arguments218['tabindex'] = NULL;
$arguments218['onclick'] = NULL;
$arguments218['name'] = NULL;
$arguments218['rel'] = NULL;
$arguments218['rev'] = NULL;
$arguments218['target'] = NULL;
$renderChildrenClosure224 = function() use ($renderingContext, $self) {
return '
												<i class="icon-play icon-white"></i>
											';
};
$viewHelper225 = $self->getViewHelper('$viewHelper225', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper225->setArguments($arguments218);
$viewHelper225->setRenderingContext($renderingContext);
$viewHelper225->setRenderChildrenClosure($renderChildrenClosure224);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output217 .= $viewHelper225->initializeArgumentsAndRender();

$output217 .= '
										';
return $output217;
};
$viewHelper226 = $self->getViewHelper('$viewHelper226', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper226->setArguments($arguments215);
$viewHelper226->setRenderingContext($renderingContext);
$viewHelper226->setRenderChildrenClosure($renderChildrenClosure216);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output118 .= $viewHelper226->initializeArgumentsAndRender();

$output118 .= '
									';
return $output118;
};
$arguments116['__thenClosure'] = function() use ($renderingContext, $self) {
$output227 = '';

$output227 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments228 = array();
// Rendering Boolean node
$arguments228['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'isDevelopmentContext', $renderingContext));
$arguments228['then'] = NULL;
$arguments228['else'] = NULL;
$renderChildrenClosure229 = function() use ($renderingContext, $self) {
$output230 = '';

$output230 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments231 = array();
// Rendering Boolean node
$arguments231['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.isFrozen', $renderingContext));
$arguments231['then'] = NULL;
$arguments231['else'] = NULL;
$renderChildrenClosure232 = function() use ($renderingContext, $self) {
$output233 = '';

$output233 .= '
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments234 = array();
$renderChildrenClosure235 = function() use ($renderingContext, $self) {
$output236 = '';

$output236 .= '
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments237 = array();
$arguments237['action'] = 'unfreeze';
$arguments237['class'] = 'neos-button neos-button-freeze neos-active';
// Rendering Array
$array238 = array();
$array238['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments237['arguments'] = $array238;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments239 = array();
$arguments239['id'] = 'packages.tooltip.unfreeze';
$arguments239['source'] = 'Modules';
$arguments239['package'] = 'TYPO3.Neos';
$arguments239['value'] = NULL;
$arguments239['arguments'] = array (
);
$arguments239['quantity'] = NULL;
$arguments239['languageIdentifier'] = NULL;
$renderChildrenClosure240 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper241 = $self->getViewHelper('$viewHelper241', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper241->setArguments($arguments239);
$viewHelper241->setRenderingContext($renderingContext);
$viewHelper241->setRenderChildrenClosure($renderChildrenClosure240);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments237['title'] = $viewHelper241->initializeArgumentsAndRender();
// Rendering Array
$array242 = array();
$array242['data-neos-toggle'] = 'tooltip';
$arguments237['additionalAttributes'] = $array242;
$arguments237['data'] = NULL;
$arguments237['controller'] = NULL;
$arguments237['package'] = NULL;
$arguments237['subpackage'] = NULL;
$arguments237['section'] = '';
$arguments237['format'] = '';
$arguments237['additionalParams'] = array (
);
$arguments237['addQueryString'] = false;
$arguments237['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments237['useParentRequest'] = false;
$arguments237['absolute'] = true;
$arguments237['dir'] = NULL;
$arguments237['id'] = NULL;
$arguments237['lang'] = NULL;
$arguments237['style'] = NULL;
$arguments237['accesskey'] = NULL;
$arguments237['tabindex'] = NULL;
$arguments237['onclick'] = NULL;
$arguments237['name'] = NULL;
$arguments237['rel'] = NULL;
$arguments237['rev'] = NULL;
$arguments237['target'] = NULL;
$renderChildrenClosure243 = function() use ($renderingContext, $self) {
return '
															<i class="icon-asterisk icon-white"></i>
														';
};
$viewHelper244 = $self->getViewHelper('$viewHelper244', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper244->setArguments($arguments237);
$viewHelper244->setRenderingContext($renderingContext);
$viewHelper244->setRenderChildrenClosure($renderChildrenClosure243);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output236 .= $viewHelper244->initializeArgumentsAndRender();

$output236 .= '
													';
return $output236;
};
$viewHelper245 = $self->getViewHelper('$viewHelper245', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper245->setArguments($arguments234);
$viewHelper245->setRenderingContext($renderingContext);
$viewHelper245->setRenderChildrenClosure($renderChildrenClosure235);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output233 .= $viewHelper245->initializeArgumentsAndRender();

$output233 .= '
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments246 = array();
$renderChildrenClosure247 = function() use ($renderingContext, $self) {
$output248 = '';

$output248 .= '
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments249 = array();
$arguments249['action'] = 'freeze';
$arguments249['class'] = 'neos-button neos-button-freeze';
// Rendering Array
$array250 = array();
$array250['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments249['arguments'] = $array250;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments251 = array();
$arguments251['id'] = 'packages.tooltip.freeze';
$arguments251['source'] = 'Modules';
$arguments251['package'] = 'TYPO3.Neos';
$arguments251['value'] = NULL;
$arguments251['arguments'] = array (
);
$arguments251['quantity'] = NULL;
$arguments251['languageIdentifier'] = NULL;
$renderChildrenClosure252 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper253 = $self->getViewHelper('$viewHelper253', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper253->setArguments($arguments251);
$viewHelper253->setRenderingContext($renderingContext);
$viewHelper253->setRenderChildrenClosure($renderChildrenClosure252);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments249['title'] = $viewHelper253->initializeArgumentsAndRender();
// Rendering Array
$array254 = array();
$array254['data-neos-toggle'] = 'tooltip';
$arguments249['additionalAttributes'] = $array254;
$arguments249['data'] = NULL;
$arguments249['controller'] = NULL;
$arguments249['package'] = NULL;
$arguments249['subpackage'] = NULL;
$arguments249['section'] = '';
$arguments249['format'] = '';
$arguments249['additionalParams'] = array (
);
$arguments249['addQueryString'] = false;
$arguments249['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments249['useParentRequest'] = false;
$arguments249['absolute'] = true;
$arguments249['dir'] = NULL;
$arguments249['id'] = NULL;
$arguments249['lang'] = NULL;
$arguments249['style'] = NULL;
$arguments249['accesskey'] = NULL;
$arguments249['tabindex'] = NULL;
$arguments249['onclick'] = NULL;
$arguments249['name'] = NULL;
$arguments249['rel'] = NULL;
$arguments249['rev'] = NULL;
$arguments249['target'] = NULL;
$renderChildrenClosure255 = function() use ($renderingContext, $self) {
return '
															<i class="icon-asterisk icon-white"></i>
														';
};
$viewHelper256 = $self->getViewHelper('$viewHelper256', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper256->setArguments($arguments249);
$viewHelper256->setRenderingContext($renderingContext);
$viewHelper256->setRenderChildrenClosure($renderChildrenClosure255);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output248 .= $viewHelper256->initializeArgumentsAndRender();

$output248 .= '
													';
return $output248;
};
$viewHelper257 = $self->getViewHelper('$viewHelper257', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper257->setArguments($arguments246);
$viewHelper257->setRenderingContext($renderingContext);
$viewHelper257->setRenderChildrenClosure($renderChildrenClosure247);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output233 .= $viewHelper257->initializeArgumentsAndRender();

$output233 .= '
												';
return $output233;
};
$arguments231['__thenClosure'] = function() use ($renderingContext, $self) {
$output258 = '';

$output258 .= '
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments259 = array();
$arguments259['action'] = 'unfreeze';
$arguments259['class'] = 'neos-button neos-button-freeze neos-active';
// Rendering Array
$array260 = array();
$array260['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments259['arguments'] = $array260;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments261 = array();
$arguments261['id'] = 'packages.tooltip.unfreeze';
$arguments261['source'] = 'Modules';
$arguments261['package'] = 'TYPO3.Neos';
$arguments261['value'] = NULL;
$arguments261['arguments'] = array (
);
$arguments261['quantity'] = NULL;
$arguments261['languageIdentifier'] = NULL;
$renderChildrenClosure262 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper263 = $self->getViewHelper('$viewHelper263', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper263->setArguments($arguments261);
$viewHelper263->setRenderingContext($renderingContext);
$viewHelper263->setRenderChildrenClosure($renderChildrenClosure262);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments259['title'] = $viewHelper263->initializeArgumentsAndRender();
// Rendering Array
$array264 = array();
$array264['data-neos-toggle'] = 'tooltip';
$arguments259['additionalAttributes'] = $array264;
$arguments259['data'] = NULL;
$arguments259['controller'] = NULL;
$arguments259['package'] = NULL;
$arguments259['subpackage'] = NULL;
$arguments259['section'] = '';
$arguments259['format'] = '';
$arguments259['additionalParams'] = array (
);
$arguments259['addQueryString'] = false;
$arguments259['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments259['useParentRequest'] = false;
$arguments259['absolute'] = true;
$arguments259['dir'] = NULL;
$arguments259['id'] = NULL;
$arguments259['lang'] = NULL;
$arguments259['style'] = NULL;
$arguments259['accesskey'] = NULL;
$arguments259['tabindex'] = NULL;
$arguments259['onclick'] = NULL;
$arguments259['name'] = NULL;
$arguments259['rel'] = NULL;
$arguments259['rev'] = NULL;
$arguments259['target'] = NULL;
$renderChildrenClosure265 = function() use ($renderingContext, $self) {
return '
															<i class="icon-asterisk icon-white"></i>
														';
};
$viewHelper266 = $self->getViewHelper('$viewHelper266', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper266->setArguments($arguments259);
$viewHelper266->setRenderingContext($renderingContext);
$viewHelper266->setRenderChildrenClosure($renderChildrenClosure265);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output258 .= $viewHelper266->initializeArgumentsAndRender();

$output258 .= '
													';
return $output258;
};
$arguments231['__elseClosure'] = function() use ($renderingContext, $self) {
$output267 = '';

$output267 .= '
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments268 = array();
$arguments268['action'] = 'freeze';
$arguments268['class'] = 'neos-button neos-button-freeze';
// Rendering Array
$array269 = array();
$array269['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments268['arguments'] = $array269;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments270 = array();
$arguments270['id'] = 'packages.tooltip.freeze';
$arguments270['source'] = 'Modules';
$arguments270['package'] = 'TYPO3.Neos';
$arguments270['value'] = NULL;
$arguments270['arguments'] = array (
);
$arguments270['quantity'] = NULL;
$arguments270['languageIdentifier'] = NULL;
$renderChildrenClosure271 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper272 = $self->getViewHelper('$viewHelper272', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper272->setArguments($arguments270);
$viewHelper272->setRenderingContext($renderingContext);
$viewHelper272->setRenderChildrenClosure($renderChildrenClosure271);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments268['title'] = $viewHelper272->initializeArgumentsAndRender();
// Rendering Array
$array273 = array();
$array273['data-neos-toggle'] = 'tooltip';
$arguments268['additionalAttributes'] = $array273;
$arguments268['data'] = NULL;
$arguments268['controller'] = NULL;
$arguments268['package'] = NULL;
$arguments268['subpackage'] = NULL;
$arguments268['section'] = '';
$arguments268['format'] = '';
$arguments268['additionalParams'] = array (
);
$arguments268['addQueryString'] = false;
$arguments268['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments268['useParentRequest'] = false;
$arguments268['absolute'] = true;
$arguments268['dir'] = NULL;
$arguments268['id'] = NULL;
$arguments268['lang'] = NULL;
$arguments268['style'] = NULL;
$arguments268['accesskey'] = NULL;
$arguments268['tabindex'] = NULL;
$arguments268['onclick'] = NULL;
$arguments268['name'] = NULL;
$arguments268['rel'] = NULL;
$arguments268['rev'] = NULL;
$arguments268['target'] = NULL;
$renderChildrenClosure274 = function() use ($renderingContext, $self) {
return '
															<i class="icon-asterisk icon-white"></i>
														';
};
$viewHelper275 = $self->getViewHelper('$viewHelper275', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper275->setArguments($arguments268);
$viewHelper275->setRenderingContext($renderingContext);
$viewHelper275->setRenderChildrenClosure($renderChildrenClosure274);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output267 .= $viewHelper275->initializeArgumentsAndRender();

$output267 .= '
													';
return $output267;
};
$viewHelper276 = $self->getViewHelper('$viewHelper276', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper276->setArguments($arguments231);
$viewHelper276->setRenderingContext($renderingContext);
$viewHelper276->setRenderChildrenClosure($renderChildrenClosure232);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output230 .= $viewHelper276->initializeArgumentsAndRender();

$output230 .= '
											';
return $output230;
};
$viewHelper277 = $self->getViewHelper('$viewHelper277', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper277->setArguments($arguments228);
$viewHelper277->setRenderingContext($renderingContext);
$viewHelper277->setRenderChildrenClosure($renderChildrenClosure229);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output227 .= $viewHelper277->initializeArgumentsAndRender();

$output227 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments278 = array();
// Rendering Boolean node
$arguments278['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.isProtected', $renderingContext));
$arguments278['then'] = NULL;
$arguments278['else'] = NULL;
$renderChildrenClosure279 = function() use ($renderingContext, $self) {
$output280 = '';

$output280 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments281 = array();
$renderChildrenClosure282 = function() use ($renderingContext, $self) {
$output283 = '';

$output283 .= '
													<button class="neos-button neos-button-warning neos-disabled" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments284 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments285 = array();
$arguments285['id'] = 'packages.tooltip.noDeactivate';
$arguments285['source'] = 'Modules';
$arguments285['package'] = 'TYPO3.Neos';
$arguments285['value'] = NULL;
$arguments285['arguments'] = array (
);
$arguments285['quantity'] = NULL;
$arguments285['languageIdentifier'] = NULL;
$renderChildrenClosure286 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper287 = $self->getViewHelper('$viewHelper287', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper287->setArguments($arguments285);
$viewHelper287->setRenderingContext($renderingContext);
$viewHelper287->setRenderChildrenClosure($renderChildrenClosure286);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments284['value'] = $viewHelper287->initializeArgumentsAndRender();
$arguments284['keepQuotes'] = false;
$arguments284['encoding'] = 'UTF-8';
$arguments284['doubleEncode'] = true;
$renderChildrenClosure288 = function() use ($renderingContext, $self) {
return NULL;
};
$value289 = ($arguments284['value'] !== NULL ? $arguments284['value'] : $renderChildrenClosure288());

$output283 .= !is_string($value289) && !(is_object($value289) && method_exists($value289, '__toString')) ? $value289 : htmlspecialchars($value289, ($arguments284['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments284['encoding'], $arguments284['doubleEncode']);

$output283 .= '" disabled="disabled" data-neos-toggle="tooltip">
														<i class="icon-pause icon-white"></i>
													</button>
												';
return $output283;
};
$viewHelper290 = $self->getViewHelper('$viewHelper290', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper290->setArguments($arguments281);
$viewHelper290->setRenderingContext($renderingContext);
$viewHelper290->setRenderChildrenClosure($renderChildrenClosure282);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output280 .= $viewHelper290->initializeArgumentsAndRender();

$output280 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments291 = array();
$renderChildrenClosure292 = function() use ($renderingContext, $self) {
$output293 = '';

$output293 .= '
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments294 = array();
$arguments294['action'] = 'deactivate';
$arguments294['class'] = 'neos-button neos-button-warning';
// Rendering Array
$array295 = array();
$array295['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments294['arguments'] = $array295;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments296 = array();
$arguments296['id'] = 'packages.tooltip.deactivate';
$arguments296['source'] = 'Modules';
$arguments296['package'] = 'TYPO3.Neos';
$arguments296['value'] = NULL;
$arguments296['arguments'] = array (
);
$arguments296['quantity'] = NULL;
$arguments296['languageIdentifier'] = NULL;
$renderChildrenClosure297 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper298 = $self->getViewHelper('$viewHelper298', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper298->setArguments($arguments296);
$viewHelper298->setRenderingContext($renderingContext);
$viewHelper298->setRenderChildrenClosure($renderChildrenClosure297);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments294['title'] = $viewHelper298->initializeArgumentsAndRender();
// Rendering Array
$array299 = array();
$array299['data-neos-toggle'] = 'tooltip';
$arguments294['additionalAttributes'] = $array299;
$arguments294['data'] = NULL;
$arguments294['controller'] = NULL;
$arguments294['package'] = NULL;
$arguments294['subpackage'] = NULL;
$arguments294['section'] = '';
$arguments294['format'] = '';
$arguments294['additionalParams'] = array (
);
$arguments294['addQueryString'] = false;
$arguments294['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments294['useParentRequest'] = false;
$arguments294['absolute'] = true;
$arguments294['dir'] = NULL;
$arguments294['id'] = NULL;
$arguments294['lang'] = NULL;
$arguments294['style'] = NULL;
$arguments294['accesskey'] = NULL;
$arguments294['tabindex'] = NULL;
$arguments294['onclick'] = NULL;
$arguments294['name'] = NULL;
$arguments294['rel'] = NULL;
$arguments294['rev'] = NULL;
$arguments294['target'] = NULL;
$renderChildrenClosure300 = function() use ($renderingContext, $self) {
return '
														<i class="icon-pause icon-white"></i>
													';
};
$viewHelper301 = $self->getViewHelper('$viewHelper301', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper301->setArguments($arguments294);
$viewHelper301->setRenderingContext($renderingContext);
$viewHelper301->setRenderChildrenClosure($renderChildrenClosure300);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output293 .= $viewHelper301->initializeArgumentsAndRender();

$output293 .= '
												';
return $output293;
};
$viewHelper302 = $self->getViewHelper('$viewHelper302', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper302->setArguments($arguments291);
$viewHelper302->setRenderingContext($renderingContext);
$viewHelper302->setRenderChildrenClosure($renderChildrenClosure292);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output280 .= $viewHelper302->initializeArgumentsAndRender();

$output280 .= '
											';
return $output280;
};
$arguments278['__thenClosure'] = function() use ($renderingContext, $self) {
$output303 = '';

$output303 .= '
													<button class="neos-button neos-button-warning neos-disabled" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments304 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments305 = array();
$arguments305['id'] = 'packages.tooltip.noDeactivate';
$arguments305['source'] = 'Modules';
$arguments305['package'] = 'TYPO3.Neos';
$arguments305['value'] = NULL;
$arguments305['arguments'] = array (
);
$arguments305['quantity'] = NULL;
$arguments305['languageIdentifier'] = NULL;
$renderChildrenClosure306 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper307 = $self->getViewHelper('$viewHelper307', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper307->setArguments($arguments305);
$viewHelper307->setRenderingContext($renderingContext);
$viewHelper307->setRenderChildrenClosure($renderChildrenClosure306);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments304['value'] = $viewHelper307->initializeArgumentsAndRender();
$arguments304['keepQuotes'] = false;
$arguments304['encoding'] = 'UTF-8';
$arguments304['doubleEncode'] = true;
$renderChildrenClosure308 = function() use ($renderingContext, $self) {
return NULL;
};
$value309 = ($arguments304['value'] !== NULL ? $arguments304['value'] : $renderChildrenClosure308());

$output303 .= !is_string($value309) && !(is_object($value309) && method_exists($value309, '__toString')) ? $value309 : htmlspecialchars($value309, ($arguments304['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments304['encoding'], $arguments304['doubleEncode']);

$output303 .= '" disabled="disabled" data-neos-toggle="tooltip">
														<i class="icon-pause icon-white"></i>
													</button>
												';
return $output303;
};
$arguments278['__elseClosure'] = function() use ($renderingContext, $self) {
$output310 = '';

$output310 .= '
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments311 = array();
$arguments311['action'] = 'deactivate';
$arguments311['class'] = 'neos-button neos-button-warning';
// Rendering Array
$array312 = array();
$array312['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments311['arguments'] = $array312;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments313 = array();
$arguments313['id'] = 'packages.tooltip.deactivate';
$arguments313['source'] = 'Modules';
$arguments313['package'] = 'TYPO3.Neos';
$arguments313['value'] = NULL;
$arguments313['arguments'] = array (
);
$arguments313['quantity'] = NULL;
$arguments313['languageIdentifier'] = NULL;
$renderChildrenClosure314 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper315 = $self->getViewHelper('$viewHelper315', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper315->setArguments($arguments313);
$viewHelper315->setRenderingContext($renderingContext);
$viewHelper315->setRenderChildrenClosure($renderChildrenClosure314);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments311['title'] = $viewHelper315->initializeArgumentsAndRender();
// Rendering Array
$array316 = array();
$array316['data-neos-toggle'] = 'tooltip';
$arguments311['additionalAttributes'] = $array316;
$arguments311['data'] = NULL;
$arguments311['controller'] = NULL;
$arguments311['package'] = NULL;
$arguments311['subpackage'] = NULL;
$arguments311['section'] = '';
$arguments311['format'] = '';
$arguments311['additionalParams'] = array (
);
$arguments311['addQueryString'] = false;
$arguments311['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments311['useParentRequest'] = false;
$arguments311['absolute'] = true;
$arguments311['dir'] = NULL;
$arguments311['id'] = NULL;
$arguments311['lang'] = NULL;
$arguments311['style'] = NULL;
$arguments311['accesskey'] = NULL;
$arguments311['tabindex'] = NULL;
$arguments311['onclick'] = NULL;
$arguments311['name'] = NULL;
$arguments311['rel'] = NULL;
$arguments311['rev'] = NULL;
$arguments311['target'] = NULL;
$renderChildrenClosure317 = function() use ($renderingContext, $self) {
return '
														<i class="icon-pause icon-white"></i>
													';
};
$viewHelper318 = $self->getViewHelper('$viewHelper318', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper318->setArguments($arguments311);
$viewHelper318->setRenderingContext($renderingContext);
$viewHelper318->setRenderChildrenClosure($renderChildrenClosure317);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output310 .= $viewHelper318->initializeArgumentsAndRender();

$output310 .= '
												';
return $output310;
};
$viewHelper319 = $self->getViewHelper('$viewHelper319', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper319->setArguments($arguments278);
$viewHelper319->setRenderingContext($renderingContext);
$viewHelper319->setRenderChildrenClosure($renderChildrenClosure279);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output227 .= $viewHelper319->initializeArgumentsAndRender();

$output227 .= '
										';
return $output227;
};
$arguments116['__elseClosure'] = function() use ($renderingContext, $self) {
$output320 = '';

$output320 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments321 = array();
$arguments321['action'] = 'activate';
$arguments321['class'] = 'neos-button neos-button-success';
// Rendering Array
$array322 = array();
$array322['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments321['arguments'] = $array322;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments323 = array();
$arguments323['id'] = 'packages.tooltip.activate';
$arguments323['source'] = 'Modules';
$arguments323['package'] = 'TYPO3.Neos';
$arguments323['value'] = NULL;
$arguments323['arguments'] = array (
);
$arguments323['quantity'] = NULL;
$arguments323['languageIdentifier'] = NULL;
$renderChildrenClosure324 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper325 = $self->getViewHelper('$viewHelper325', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper325->setArguments($arguments323);
$viewHelper325->setRenderingContext($renderingContext);
$viewHelper325->setRenderChildrenClosure($renderChildrenClosure324);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments321['title'] = $viewHelper325->initializeArgumentsAndRender();
// Rendering Array
$array326 = array();
$array326['data-neos-toggle'] = 'tooltip';
$arguments321['additionalAttributes'] = $array326;
$arguments321['data'] = NULL;
$arguments321['controller'] = NULL;
$arguments321['package'] = NULL;
$arguments321['subpackage'] = NULL;
$arguments321['section'] = '';
$arguments321['format'] = '';
$arguments321['additionalParams'] = array (
);
$arguments321['addQueryString'] = false;
$arguments321['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments321['useParentRequest'] = false;
$arguments321['absolute'] = true;
$arguments321['dir'] = NULL;
$arguments321['id'] = NULL;
$arguments321['lang'] = NULL;
$arguments321['style'] = NULL;
$arguments321['accesskey'] = NULL;
$arguments321['tabindex'] = NULL;
$arguments321['onclick'] = NULL;
$arguments321['name'] = NULL;
$arguments321['rel'] = NULL;
$arguments321['rev'] = NULL;
$arguments321['target'] = NULL;
$renderChildrenClosure327 = function() use ($renderingContext, $self) {
return '
												<i class="icon-play icon-white"></i>
											';
};
$viewHelper328 = $self->getViewHelper('$viewHelper328', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper328->setArguments($arguments321);
$viewHelper328->setRenderingContext($renderingContext);
$viewHelper328->setRenderChildrenClosure($renderChildrenClosure327);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output320 .= $viewHelper328->initializeArgumentsAndRender();

$output320 .= '
										';
return $output320;
};
$viewHelper329 = $self->getViewHelper('$viewHelper329', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper329->setArguments($arguments116);
$viewHelper329->setRenderingContext($renderingContext);
$viewHelper329->setRenderChildrenClosure($renderChildrenClosure117);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output48 .= $viewHelper329->initializeArgumentsAndRender();

$output48 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments330 = array();
// Rendering Boolean node
$arguments330['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.isProtected', $renderingContext));
$arguments330['then'] = NULL;
$arguments330['else'] = NULL;
$renderChildrenClosure331 = function() use ($renderingContext, $self) {
$output332 = '';

$output332 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments333 = array();
$renderChildrenClosure334 = function() use ($renderingContext, $self) {
$output335 = '';

$output335 .= '
											<button class="neos-button neos-button-danger neos-disabled" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments336 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments337 = array();
$arguments337['id'] = 'packages.tooltip.noDelete';
$arguments337['source'] = 'Modules';
$arguments337['package'] = 'TYPO3.Neos';
$arguments337['value'] = NULL;
$arguments337['arguments'] = array (
);
$arguments337['quantity'] = NULL;
$arguments337['languageIdentifier'] = NULL;
$renderChildrenClosure338 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper339 = $self->getViewHelper('$viewHelper339', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper339->setArguments($arguments337);
$viewHelper339->setRenderingContext($renderingContext);
$viewHelper339->setRenderChildrenClosure($renderChildrenClosure338);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments336['value'] = $viewHelper339->initializeArgumentsAndRender();
$arguments336['keepQuotes'] = false;
$arguments336['encoding'] = 'UTF-8';
$arguments336['doubleEncode'] = true;
$renderChildrenClosure340 = function() use ($renderingContext, $self) {
return NULL;
};
$value341 = ($arguments336['value'] !== NULL ? $arguments336['value'] : $renderChildrenClosure340());

$output335 .= !is_string($value341) && !(is_object($value341) && method_exists($value341, '__toString')) ? $value341 : htmlspecialchars($value341, ($arguments336['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments336['encoding'], $arguments336['doubleEncode']);

$output335 .= '" disabled="disabled" data-neos-toggle="tooltip"><i class="icon-trash icon-white"></i></button>
										';
return $output335;
};
$viewHelper342 = $self->getViewHelper('$viewHelper342', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper342->setArguments($arguments333);
$viewHelper342->setRenderingContext($renderingContext);
$viewHelper342->setRenderChildrenClosure($renderChildrenClosure334);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output332 .= $viewHelper342->initializeArgumentsAndRender();

$output332 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments343 = array();
$renderChildrenClosure344 = function() use ($renderingContext, $self) {
$output345 = '';

$output345 .= '
											<button class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments346 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments347 = array();
$arguments347['id'] = 'packages.tooltip.delete';
$arguments347['source'] = 'Modules';
$arguments347['package'] = 'TYPO3.Neos';
$arguments347['value'] = NULL;
$arguments347['arguments'] = array (
);
$arguments347['quantity'] = NULL;
$arguments347['languageIdentifier'] = NULL;
$renderChildrenClosure348 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper349 = $self->getViewHelper('$viewHelper349', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper349->setArguments($arguments347);
$viewHelper349->setRenderingContext($renderingContext);
$viewHelper349->setRenderChildrenClosure($renderChildrenClosure348);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments346['value'] = $viewHelper349->initializeArgumentsAndRender();
$arguments346['keepQuotes'] = false;
$arguments346['encoding'] = 'UTF-8';
$arguments346['doubleEncode'] = true;
$renderChildrenClosure350 = function() use ($renderingContext, $self) {
return NULL;
};
$value351 = ($arguments346['value'] !== NULL ? $arguments346['value'] : $renderChildrenClosure350());

$output345 .= !is_string($value351) && !(is_object($value351) && method_exists($value351, '__toString')) ? $value351 : htmlspecialchars($value351, ($arguments346['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments346['encoding'], $arguments346['doubleEncode']);

$output345 .= '" data-toggle="modal" href="#';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments352 = array();
$arguments352['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.sanitizedPackageKey', $renderingContext);
$arguments352['keepQuotes'] = false;
$arguments352['encoding'] = 'UTF-8';
$arguments352['doubleEncode'] = true;
$renderChildrenClosure353 = function() use ($renderingContext, $self) {
return NULL;
};
$value354 = ($arguments352['value'] !== NULL ? $arguments352['value'] : $renderChildrenClosure353());

$output345 .= !is_string($value354) && !(is_object($value354) && method_exists($value354, '__toString')) ? $value354 : htmlspecialchars($value354, ($arguments352['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments352['encoding'], $arguments352['doubleEncode']);

$output345 .= '" data-neos-toggle="tooltip"><i class="icon-trash icon-white"></i></button>
											<div class="neos-hide" id="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments355 = array();
$arguments355['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.sanitizedPackageKey', $renderingContext);
$arguments355['keepQuotes'] = false;
$arguments355['encoding'] = 'UTF-8';
$arguments355['doubleEncode'] = true;
$renderChildrenClosure356 = function() use ($renderingContext, $self) {
return NULL;
};
$value357 = ($arguments355['value'] !== NULL ? $arguments355['value'] : $renderChildrenClosure356());

$output345 .= !is_string($value357) && !(is_object($value357) && method_exists($value357, '__toString')) ? $value357 : htmlspecialchars($value357, ($arguments355['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments355['encoding'], $arguments355['doubleEncode']);

$output345 .= '">
												<div class="neos-modal-centered">
													<div class="neos-modal-content">
														<div class="neos-modal-header">
															<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
															<div class="neos-header">
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments358 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments359 = array();
$arguments359['id'] = 'packages.message.reallyDelete';
// Rendering Array
$array360 = array();
$array360['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments359['arguments'] = $array360;
$arguments359['source'] = 'Modules';
$arguments359['package'] = 'TYPO3.Neos';
$arguments359['value'] = NULL;
$arguments359['quantity'] = NULL;
$arguments359['languageIdentifier'] = NULL;
$renderChildrenClosure361 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper362 = $self->getViewHelper('$viewHelper362', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper362->setArguments($arguments359);
$viewHelper362->setRenderingContext($renderingContext);
$viewHelper362->setRenderChildrenClosure($renderChildrenClosure361);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments358['value'] = $viewHelper362->initializeArgumentsAndRender();
$arguments358['keepQuotes'] = false;
$arguments358['encoding'] = 'UTF-8';
$arguments358['doubleEncode'] = true;
$renderChildrenClosure363 = function() use ($renderingContext, $self) {
return NULL;
};
$value364 = ($arguments358['value'] !== NULL ? $arguments358['value'] : $renderChildrenClosure363());

$output345 .= !is_string($value364) && !(is_object($value364) && method_exists($value364, '__toString')) ? $value364 : htmlspecialchars($value364, ($arguments358['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments358['encoding'], $arguments358['doubleEncode']);

$output345 .= '
															</div>
															<div class="neos-subheader">
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments365 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments366 = array();
$arguments366['id'] = 'operationCannotBeUndone';
$arguments366['package'] = 'TYPO3.Neos';
$arguments366['value'] = NULL;
$arguments366['arguments'] = array (
);
$arguments366['source'] = 'Main';
$arguments366['quantity'] = NULL;
$arguments366['languageIdentifier'] = NULL;
$renderChildrenClosure367 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper368 = $self->getViewHelper('$viewHelper368', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper368->setArguments($arguments366);
$viewHelper368->setRenderingContext($renderingContext);
$viewHelper368->setRenderChildrenClosure($renderChildrenClosure367);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments365['value'] = $viewHelper368->initializeArgumentsAndRender();
$arguments365['keepQuotes'] = false;
$arguments365['encoding'] = 'UTF-8';
$arguments365['doubleEncode'] = true;
$renderChildrenClosure369 = function() use ($renderingContext, $self) {
return NULL;
};
$value370 = ($arguments365['value'] !== NULL ? $arguments365['value'] : $renderChildrenClosure369());

$output345 .= !is_string($value370) && !(is_object($value370) && method_exists($value370, '__toString')) ? $value370 : htmlspecialchars($value370, ($arguments365['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments365['encoding'], $arguments365['doubleEncode']);

$output345 .= '
															</div>
														</div>
														<div class="neos-modal-footer">
															<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments371 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments372 = array();
$arguments372['id'] = 'cancel';
$arguments372['package'] = 'TYPO3.Neos';
$arguments372['value'] = NULL;
$arguments372['arguments'] = array (
);
$arguments372['source'] = 'Main';
$arguments372['quantity'] = NULL;
$arguments372['languageIdentifier'] = NULL;
$renderChildrenClosure373 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper374 = $self->getViewHelper('$viewHelper374', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper374->setArguments($arguments372);
$viewHelper374->setRenderingContext($renderingContext);
$viewHelper374->setRenderChildrenClosure($renderChildrenClosure373);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments371['value'] = $viewHelper374->initializeArgumentsAndRender();
$arguments371['keepQuotes'] = false;
$arguments371['encoding'] = 'UTF-8';
$arguments371['doubleEncode'] = true;
$renderChildrenClosure375 = function() use ($renderingContext, $self) {
return NULL;
};
$value376 = ($arguments371['value'] !== NULL ? $arguments371['value'] : $renderChildrenClosure375());

$output345 .= !is_string($value376) && !(is_object($value376) && method_exists($value376, '__toString')) ? $value376 : htmlspecialchars($value376, ($arguments371['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments371['encoding'], $arguments371['doubleEncode']);

$output345 .= '</a>
															';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments377 = array();
$arguments377['action'] = 'delete';
$arguments377['class'] = 'neos-button neos-button-danger';
// Rendering Array
$array378 = array();
$array378['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments377['arguments'] = $array378;
$arguments377['title'] = 'Delete';
// Rendering Array
$array379 = array();
$array379['data-neos-toggle'] = 'tooltip';
$arguments377['additionalAttributes'] = $array379;
$arguments377['data'] = NULL;
$arguments377['controller'] = NULL;
$arguments377['package'] = NULL;
$arguments377['subpackage'] = NULL;
$arguments377['section'] = '';
$arguments377['format'] = '';
$arguments377['additionalParams'] = array (
);
$arguments377['addQueryString'] = false;
$arguments377['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments377['useParentRequest'] = false;
$arguments377['absolute'] = true;
$arguments377['dir'] = NULL;
$arguments377['id'] = NULL;
$arguments377['lang'] = NULL;
$arguments377['style'] = NULL;
$arguments377['accesskey'] = NULL;
$arguments377['tabindex'] = NULL;
$arguments377['onclick'] = NULL;
$arguments377['name'] = NULL;
$arguments377['rel'] = NULL;
$arguments377['rev'] = NULL;
$arguments377['target'] = NULL;
$renderChildrenClosure380 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments381 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments382 = array();
$arguments382['id'] = 'packages.message.confirmDelete';
$arguments382['source'] = 'Modules';
$arguments382['package'] = 'TYPO3.Neos';
$arguments382['value'] = NULL;
$arguments382['arguments'] = array (
);
$arguments382['quantity'] = NULL;
$arguments382['languageIdentifier'] = NULL;
$renderChildrenClosure383 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper384 = $self->getViewHelper('$viewHelper384', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper384->setArguments($arguments382);
$viewHelper384->setRenderingContext($renderingContext);
$viewHelper384->setRenderChildrenClosure($renderChildrenClosure383);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments381['value'] = $viewHelper384->initializeArgumentsAndRender();
$arguments381['keepQuotes'] = false;
$arguments381['encoding'] = 'UTF-8';
$arguments381['doubleEncode'] = true;
$renderChildrenClosure385 = function() use ($renderingContext, $self) {
return NULL;
};
$value386 = ($arguments381['value'] !== NULL ? $arguments381['value'] : $renderChildrenClosure385());
return !is_string($value386) && !(is_object($value386) && method_exists($value386, '__toString')) ? $value386 : htmlspecialchars($value386, ($arguments381['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments381['encoding'], $arguments381['doubleEncode']);
};
$viewHelper387 = $self->getViewHelper('$viewHelper387', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper387->setArguments($arguments377);
$viewHelper387->setRenderingContext($renderingContext);
$viewHelper387->setRenderChildrenClosure($renderChildrenClosure380);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output345 .= $viewHelper387->initializeArgumentsAndRender();

$output345 .= '
														</div>
													</div>
												</div>
												<div class="neos-modal-backdrop neos-in"></div>
											</div>
										';
return $output345;
};
$viewHelper388 = $self->getViewHelper('$viewHelper388', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper388->setArguments($arguments343);
$viewHelper388->setRenderingContext($renderingContext);
$viewHelper388->setRenderChildrenClosure($renderChildrenClosure344);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output332 .= $viewHelper388->initializeArgumentsAndRender();

$output332 .= '
									';
return $output332;
};
$arguments330['__thenClosure'] = function() use ($renderingContext, $self) {
$output389 = '';

$output389 .= '
											<button class="neos-button neos-button-danger neos-disabled" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments390 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments391 = array();
$arguments391['id'] = 'packages.tooltip.noDelete';
$arguments391['source'] = 'Modules';
$arguments391['package'] = 'TYPO3.Neos';
$arguments391['value'] = NULL;
$arguments391['arguments'] = array (
);
$arguments391['quantity'] = NULL;
$arguments391['languageIdentifier'] = NULL;
$renderChildrenClosure392 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper393 = $self->getViewHelper('$viewHelper393', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper393->setArguments($arguments391);
$viewHelper393->setRenderingContext($renderingContext);
$viewHelper393->setRenderChildrenClosure($renderChildrenClosure392);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments390['value'] = $viewHelper393->initializeArgumentsAndRender();
$arguments390['keepQuotes'] = false;
$arguments390['encoding'] = 'UTF-8';
$arguments390['doubleEncode'] = true;
$renderChildrenClosure394 = function() use ($renderingContext, $self) {
return NULL;
};
$value395 = ($arguments390['value'] !== NULL ? $arguments390['value'] : $renderChildrenClosure394());

$output389 .= !is_string($value395) && !(is_object($value395) && method_exists($value395, '__toString')) ? $value395 : htmlspecialchars($value395, ($arguments390['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments390['encoding'], $arguments390['doubleEncode']);

$output389 .= '" disabled="disabled" data-neos-toggle="tooltip"><i class="icon-trash icon-white"></i></button>
										';
return $output389;
};
$arguments330['__elseClosure'] = function() use ($renderingContext, $self) {
$output396 = '';

$output396 .= '
											<button class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments397 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments398 = array();
$arguments398['id'] = 'packages.tooltip.delete';
$arguments398['source'] = 'Modules';
$arguments398['package'] = 'TYPO3.Neos';
$arguments398['value'] = NULL;
$arguments398['arguments'] = array (
);
$arguments398['quantity'] = NULL;
$arguments398['languageIdentifier'] = NULL;
$renderChildrenClosure399 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper400 = $self->getViewHelper('$viewHelper400', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper400->setArguments($arguments398);
$viewHelper400->setRenderingContext($renderingContext);
$viewHelper400->setRenderChildrenClosure($renderChildrenClosure399);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments397['value'] = $viewHelper400->initializeArgumentsAndRender();
$arguments397['keepQuotes'] = false;
$arguments397['encoding'] = 'UTF-8';
$arguments397['doubleEncode'] = true;
$renderChildrenClosure401 = function() use ($renderingContext, $self) {
return NULL;
};
$value402 = ($arguments397['value'] !== NULL ? $arguments397['value'] : $renderChildrenClosure401());

$output396 .= !is_string($value402) && !(is_object($value402) && method_exists($value402, '__toString')) ? $value402 : htmlspecialchars($value402, ($arguments397['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments397['encoding'], $arguments397['doubleEncode']);

$output396 .= '" data-toggle="modal" href="#';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments403 = array();
$arguments403['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.sanitizedPackageKey', $renderingContext);
$arguments403['keepQuotes'] = false;
$arguments403['encoding'] = 'UTF-8';
$arguments403['doubleEncode'] = true;
$renderChildrenClosure404 = function() use ($renderingContext, $self) {
return NULL;
};
$value405 = ($arguments403['value'] !== NULL ? $arguments403['value'] : $renderChildrenClosure404());

$output396 .= !is_string($value405) && !(is_object($value405) && method_exists($value405, '__toString')) ? $value405 : htmlspecialchars($value405, ($arguments403['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments403['encoding'], $arguments403['doubleEncode']);

$output396 .= '" data-neos-toggle="tooltip"><i class="icon-trash icon-white"></i></button>
											<div class="neos-hide" id="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments406 = array();
$arguments406['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.sanitizedPackageKey', $renderingContext);
$arguments406['keepQuotes'] = false;
$arguments406['encoding'] = 'UTF-8';
$arguments406['doubleEncode'] = true;
$renderChildrenClosure407 = function() use ($renderingContext, $self) {
return NULL;
};
$value408 = ($arguments406['value'] !== NULL ? $arguments406['value'] : $renderChildrenClosure407());

$output396 .= !is_string($value408) && !(is_object($value408) && method_exists($value408, '__toString')) ? $value408 : htmlspecialchars($value408, ($arguments406['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments406['encoding'], $arguments406['doubleEncode']);

$output396 .= '">
												<div class="neos-modal-centered">
													<div class="neos-modal-content">
														<div class="neos-modal-header">
															<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
															<div class="neos-header">
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments409 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments410 = array();
$arguments410['id'] = 'packages.message.reallyDelete';
// Rendering Array
$array411 = array();
$array411['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments410['arguments'] = $array411;
$arguments410['source'] = 'Modules';
$arguments410['package'] = 'TYPO3.Neos';
$arguments410['value'] = NULL;
$arguments410['quantity'] = NULL;
$arguments410['languageIdentifier'] = NULL;
$renderChildrenClosure412 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper413 = $self->getViewHelper('$viewHelper413', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper413->setArguments($arguments410);
$viewHelper413->setRenderingContext($renderingContext);
$viewHelper413->setRenderChildrenClosure($renderChildrenClosure412);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments409['value'] = $viewHelper413->initializeArgumentsAndRender();
$arguments409['keepQuotes'] = false;
$arguments409['encoding'] = 'UTF-8';
$arguments409['doubleEncode'] = true;
$renderChildrenClosure414 = function() use ($renderingContext, $self) {
return NULL;
};
$value415 = ($arguments409['value'] !== NULL ? $arguments409['value'] : $renderChildrenClosure414());

$output396 .= !is_string($value415) && !(is_object($value415) && method_exists($value415, '__toString')) ? $value415 : htmlspecialchars($value415, ($arguments409['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments409['encoding'], $arguments409['doubleEncode']);

$output396 .= '
															</div>
															<div class="neos-subheader">
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments416 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments417 = array();
$arguments417['id'] = 'operationCannotBeUndone';
$arguments417['package'] = 'TYPO3.Neos';
$arguments417['value'] = NULL;
$arguments417['arguments'] = array (
);
$arguments417['source'] = 'Main';
$arguments417['quantity'] = NULL;
$arguments417['languageIdentifier'] = NULL;
$renderChildrenClosure418 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper419 = $self->getViewHelper('$viewHelper419', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper419->setArguments($arguments417);
$viewHelper419->setRenderingContext($renderingContext);
$viewHelper419->setRenderChildrenClosure($renderChildrenClosure418);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments416['value'] = $viewHelper419->initializeArgumentsAndRender();
$arguments416['keepQuotes'] = false;
$arguments416['encoding'] = 'UTF-8';
$arguments416['doubleEncode'] = true;
$renderChildrenClosure420 = function() use ($renderingContext, $self) {
return NULL;
};
$value421 = ($arguments416['value'] !== NULL ? $arguments416['value'] : $renderChildrenClosure420());

$output396 .= !is_string($value421) && !(is_object($value421) && method_exists($value421, '__toString')) ? $value421 : htmlspecialchars($value421, ($arguments416['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments416['encoding'], $arguments416['doubleEncode']);

$output396 .= '
															</div>
														</div>
														<div class="neos-modal-footer">
															<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments422 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments423 = array();
$arguments423['id'] = 'cancel';
$arguments423['package'] = 'TYPO3.Neos';
$arguments423['value'] = NULL;
$arguments423['arguments'] = array (
);
$arguments423['source'] = 'Main';
$arguments423['quantity'] = NULL;
$arguments423['languageIdentifier'] = NULL;
$renderChildrenClosure424 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper425 = $self->getViewHelper('$viewHelper425', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper425->setArguments($arguments423);
$viewHelper425->setRenderingContext($renderingContext);
$viewHelper425->setRenderChildrenClosure($renderChildrenClosure424);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments422['value'] = $viewHelper425->initializeArgumentsAndRender();
$arguments422['keepQuotes'] = false;
$arguments422['encoding'] = 'UTF-8';
$arguments422['doubleEncode'] = true;
$renderChildrenClosure426 = function() use ($renderingContext, $self) {
return NULL;
};
$value427 = ($arguments422['value'] !== NULL ? $arguments422['value'] : $renderChildrenClosure426());

$output396 .= !is_string($value427) && !(is_object($value427) && method_exists($value427, '__toString')) ? $value427 : htmlspecialchars($value427, ($arguments422['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments422['encoding'], $arguments422['doubleEncode']);

$output396 .= '</a>
															';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments428 = array();
$arguments428['action'] = 'delete';
$arguments428['class'] = 'neos-button neos-button-danger';
// Rendering Array
$array429 = array();
$array429['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments428['arguments'] = $array429;
$arguments428['title'] = 'Delete';
// Rendering Array
$array430 = array();
$array430['data-neos-toggle'] = 'tooltip';
$arguments428['additionalAttributes'] = $array430;
$arguments428['data'] = NULL;
$arguments428['controller'] = NULL;
$arguments428['package'] = NULL;
$arguments428['subpackage'] = NULL;
$arguments428['section'] = '';
$arguments428['format'] = '';
$arguments428['additionalParams'] = array (
);
$arguments428['addQueryString'] = false;
$arguments428['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments428['useParentRequest'] = false;
$arguments428['absolute'] = true;
$arguments428['dir'] = NULL;
$arguments428['id'] = NULL;
$arguments428['lang'] = NULL;
$arguments428['style'] = NULL;
$arguments428['accesskey'] = NULL;
$arguments428['tabindex'] = NULL;
$arguments428['onclick'] = NULL;
$arguments428['name'] = NULL;
$arguments428['rel'] = NULL;
$arguments428['rev'] = NULL;
$arguments428['target'] = NULL;
$renderChildrenClosure431 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments432 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments433 = array();
$arguments433['id'] = 'packages.message.confirmDelete';
$arguments433['source'] = 'Modules';
$arguments433['package'] = 'TYPO3.Neos';
$arguments433['value'] = NULL;
$arguments433['arguments'] = array (
);
$arguments433['quantity'] = NULL;
$arguments433['languageIdentifier'] = NULL;
$renderChildrenClosure434 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper435 = $self->getViewHelper('$viewHelper435', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper435->setArguments($arguments433);
$viewHelper435->setRenderingContext($renderingContext);
$viewHelper435->setRenderChildrenClosure($renderChildrenClosure434);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments432['value'] = $viewHelper435->initializeArgumentsAndRender();
$arguments432['keepQuotes'] = false;
$arguments432['encoding'] = 'UTF-8';
$arguments432['doubleEncode'] = true;
$renderChildrenClosure436 = function() use ($renderingContext, $self) {
return NULL;
};
$value437 = ($arguments432['value'] !== NULL ? $arguments432['value'] : $renderChildrenClosure436());
return !is_string($value437) && !(is_object($value437) && method_exists($value437, '__toString')) ? $value437 : htmlspecialchars($value437, ($arguments432['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments432['encoding'], $arguments432['doubleEncode']);
};
$viewHelper438 = $self->getViewHelper('$viewHelper438', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper438->setArguments($arguments428);
$viewHelper438->setRenderingContext($renderingContext);
$viewHelper438->setRenderChildrenClosure($renderChildrenClosure431);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output396 .= $viewHelper438->initializeArgumentsAndRender();

$output396 .= '
														</div>
													</div>
												</div>
												<div class="neos-modal-backdrop neos-in"></div>
											</div>
										';
return $output396;
};
$viewHelper439 = $self->getViewHelper('$viewHelper439', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper439->setArguments($arguments330);
$viewHelper439->setRenderingContext($renderingContext);
$viewHelper439->setRenderChildrenClosure($renderChildrenClosure331);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output48 .= $viewHelper439->initializeArgumentsAndRender();

$output48 .= '
								</td>
							</tr>
						';
return $output48;
};

$output36 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments46, $renderChildrenClosure47, $renderingContext);

$output36 .= '
					';
return $output36;
};

$output3 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments34, $renderChildrenClosure35, $renderingContext);

$output3 .= '
				</tbody>
			</table>
		</div>
		<div class="neos-footer">
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments440 = array();
// Rendering Boolean node
$arguments440['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'isDevelopmentContext', $renderingContext));
$arguments440['then'] = NULL;
$arguments440['else'] = NULL;
$renderChildrenClosure441 = function() use ($renderingContext, $self) {
$output442 = '';

$output442 .= '
				<button type="submit" name="moduleArguments[action]" value="freeze" class="neos-button batch-action neos-disabled" disabled="disabled">
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments443 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments444 = array();
$arguments444['id'] = 'packages.selected.freeze';
$arguments444['source'] = 'Modules';
$arguments444['package'] = 'TYPO3.Neos';
$arguments444['value'] = NULL;
$arguments444['arguments'] = array (
);
$arguments444['quantity'] = NULL;
$arguments444['languageIdentifier'] = NULL;
$renderChildrenClosure445 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper446 = $self->getViewHelper('$viewHelper446', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper446->setArguments($arguments444);
$viewHelper446->setRenderingContext($renderingContext);
$viewHelper446->setRenderChildrenClosure($renderChildrenClosure445);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments443['value'] = $viewHelper446->initializeArgumentsAndRender();
$arguments443['keepQuotes'] = false;
$arguments443['encoding'] = 'UTF-8';
$arguments443['doubleEncode'] = true;
$renderChildrenClosure447 = function() use ($renderingContext, $self) {
return NULL;
};
$value448 = ($arguments443['value'] !== NULL ? $arguments443['value'] : $renderChildrenClosure447());

$output442 .= !is_string($value448) && !(is_object($value448) && method_exists($value448, '__toString')) ? $value448 : htmlspecialchars($value448, ($arguments443['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments443['encoding'], $arguments443['doubleEncode']);

$output442 .= '
				</button>
				<button type="submit" name="moduleArguments[action]" value="unfreeze" class="neos-button batch-action neos-disabled" disabled="disabled">
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments449 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments450 = array();
$arguments450['id'] = 'packages.selected.unfreeze';
$arguments450['source'] = 'Modules';
$arguments450['package'] = 'TYPO3.Neos';
$arguments450['value'] = NULL;
$arguments450['arguments'] = array (
);
$arguments450['quantity'] = NULL;
$arguments450['languageIdentifier'] = NULL;
$renderChildrenClosure451 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper452 = $self->getViewHelper('$viewHelper452', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper452->setArguments($arguments450);
$viewHelper452->setRenderingContext($renderingContext);
$viewHelper452->setRenderChildrenClosure($renderChildrenClosure451);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments449['value'] = $viewHelper452->initializeArgumentsAndRender();
$arguments449['keepQuotes'] = false;
$arguments449['encoding'] = 'UTF-8';
$arguments449['doubleEncode'] = true;
$renderChildrenClosure453 = function() use ($renderingContext, $self) {
return NULL;
};
$value454 = ($arguments449['value'] !== NULL ? $arguments449['value'] : $renderChildrenClosure453());

$output442 .= !is_string($value454) && !(is_object($value454) && method_exists($value454, '__toString')) ? $value454 : htmlspecialchars($value454, ($arguments449['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments449['encoding'], $arguments449['doubleEncode']);

$output442 .= '
				</button>
			';
return $output442;
};
$viewHelper455 = $self->getViewHelper('$viewHelper455', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper455->setArguments($arguments440);
$viewHelper455->setRenderingContext($renderingContext);
$viewHelper455->setRenderChildrenClosure($renderChildrenClosure441);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output3 .= $viewHelper455->initializeArgumentsAndRender();

$output3 .= '
			<button class="neos-button neos-button-danger batch-action neos-disabled" data-toggle="modal" href="#delete" disabled="disabled">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments456 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments457 = array();
$arguments457['id'] = 'packages.selected.delete';
$arguments457['source'] = 'Modules';
$arguments457['package'] = 'TYPO3.Neos';
$arguments457['value'] = NULL;
$arguments457['arguments'] = array (
);
$arguments457['quantity'] = NULL;
$arguments457['languageIdentifier'] = NULL;
$renderChildrenClosure458 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper459 = $self->getViewHelper('$viewHelper459', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper459->setArguments($arguments457);
$viewHelper459->setRenderingContext($renderingContext);
$viewHelper459->setRenderChildrenClosure($renderChildrenClosure458);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments456['value'] = $viewHelper459->initializeArgumentsAndRender();
$arguments456['keepQuotes'] = false;
$arguments456['encoding'] = 'UTF-8';
$arguments456['doubleEncode'] = true;
$renderChildrenClosure460 = function() use ($renderingContext, $self) {
return NULL;
};
$value461 = ($arguments456['value'] !== NULL ? $arguments456['value'] : $renderChildrenClosure460());

$output3 .= !is_string($value461) && !(is_object($value461) && method_exists($value461, '__toString')) ? $value461 : htmlspecialchars($value461, ($arguments456['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments456['encoding'], $arguments456['doubleEncode']);

$output3 .= '
			</button>
			<button type="submit" name="moduleArguments[action]" value="deactivate" class="neos-button neos-button-warning batch-action neos-disabled" disabled="disabled">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments462 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments463 = array();
$arguments463['id'] = 'packages.selected.deactivate';
$arguments463['source'] = 'Modules';
$arguments463['package'] = 'TYPO3.Neos';
$arguments463['value'] = NULL;
$arguments463['arguments'] = array (
);
$arguments463['quantity'] = NULL;
$arguments463['languageIdentifier'] = NULL;
$renderChildrenClosure464 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper465 = $self->getViewHelper('$viewHelper465', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper465->setArguments($arguments463);
$viewHelper465->setRenderingContext($renderingContext);
$viewHelper465->setRenderChildrenClosure($renderChildrenClosure464);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments462['value'] = $viewHelper465->initializeArgumentsAndRender();
$arguments462['keepQuotes'] = false;
$arguments462['encoding'] = 'UTF-8';
$arguments462['doubleEncode'] = true;
$renderChildrenClosure466 = function() use ($renderingContext, $self) {
return NULL;
};
$value467 = ($arguments462['value'] !== NULL ? $arguments462['value'] : $renderChildrenClosure466());

$output3 .= !is_string($value467) && !(is_object($value467) && method_exists($value467, '__toString')) ? $value467 : htmlspecialchars($value467, ($arguments462['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments462['encoding'], $arguments462['doubleEncode']);

$output3 .= '
			</button>
			<button type="submit" name="moduleArguments[action]" value="activate" class="neos-button neos-button-success batch-action neos-disabled" disabled="disabled">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments468 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments469 = array();
$arguments469['id'] = 'packages.selected.activate';
$arguments469['source'] = 'Modules';
$arguments469['package'] = 'TYPO3.Neos';
$arguments469['value'] = NULL;
$arguments469['arguments'] = array (
);
$arguments469['quantity'] = NULL;
$arguments469['languageIdentifier'] = NULL;
$renderChildrenClosure470 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper471 = $self->getViewHelper('$viewHelper471', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper471->setArguments($arguments469);
$viewHelper471->setRenderingContext($renderingContext);
$viewHelper471->setRenderChildrenClosure($renderChildrenClosure470);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments468['value'] = $viewHelper471->initializeArgumentsAndRender();
$arguments468['keepQuotes'] = false;
$arguments468['encoding'] = 'UTF-8';
$arguments468['doubleEncode'] = true;
$renderChildrenClosure472 = function() use ($renderingContext, $self) {
return NULL;
};
$value473 = ($arguments468['value'] !== NULL ? $arguments468['value'] : $renderChildrenClosure472());

$output3 .= !is_string($value473) && !(is_object($value473) && method_exists($value473, '__toString')) ? $value473 : htmlspecialchars($value473, ($arguments468['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments468['encoding'], $arguments468['doubleEncode']);

$output3 .= '
			</button>
		</div>
		<div class="neos-hide" id="delete">
			<div class="neos-modal-centered">
				<div class="neos-modal-content">
					<div class="neos-modal-header">
						<button type="button" class="neos-close" data-dismiss="modal"></button>
						<div class="neos-header">
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments474 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments475 = array();
$arguments475['id'] = 'packages.message.selected.reallyDelete';
$arguments475['source'] = 'Modules';
$arguments475['package'] = 'TYPO3.Neos';
$arguments475['value'] = NULL;
$arguments475['arguments'] = array (
);
$arguments475['quantity'] = NULL;
$arguments475['languageIdentifier'] = NULL;
$renderChildrenClosure476 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper477 = $self->getViewHelper('$viewHelper477', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper477->setArguments($arguments475);
$viewHelper477->setRenderingContext($renderingContext);
$viewHelper477->setRenderChildrenClosure($renderChildrenClosure476);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments474['value'] = $viewHelper477->initializeArgumentsAndRender();
$arguments474['keepQuotes'] = false;
$arguments474['encoding'] = 'UTF-8';
$arguments474['doubleEncode'] = true;
$renderChildrenClosure478 = function() use ($renderingContext, $self) {
return NULL;
};
$value479 = ($arguments474['value'] !== NULL ? $arguments474['value'] : $renderChildrenClosure478());

$output3 .= !is_string($value479) && !(is_object($value479) && method_exists($value479, '__toString')) ? $value479 : htmlspecialchars($value479, ($arguments474['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments474['encoding'], $arguments474['doubleEncode']);

$output3 .= '
						</div>
						<div class="neos-subheader">
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments480 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments481 = array();
$arguments481['id'] = 'operationCannotBeUndone';
$arguments481['package'] = 'TYPO3.Neos';
$arguments481['value'] = NULL;
$arguments481['arguments'] = array (
);
$arguments481['source'] = 'Main';
$arguments481['quantity'] = NULL;
$arguments481['languageIdentifier'] = NULL;
$renderChildrenClosure482 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper483 = $self->getViewHelper('$viewHelper483', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper483->setArguments($arguments481);
$viewHelper483->setRenderingContext($renderingContext);
$viewHelper483->setRenderChildrenClosure($renderChildrenClosure482);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments480['value'] = $viewHelper483->initializeArgumentsAndRender();
$arguments480['keepQuotes'] = false;
$arguments480['encoding'] = 'UTF-8';
$arguments480['doubleEncode'] = true;
$renderChildrenClosure484 = function() use ($renderingContext, $self) {
return NULL;
};
$value485 = ($arguments480['value'] !== NULL ? $arguments480['value'] : $renderChildrenClosure484());

$output3 .= !is_string($value485) && !(is_object($value485) && method_exists($value485, '__toString')) ? $value485 : htmlspecialchars($value485, ($arguments480['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments480['encoding'], $arguments480['doubleEncode']);

$output3 .= '
						</div>
					</div>
					<div class="neos-modal-footer">
						<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments486 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments487 = array();
$arguments487['id'] = 'cancel';
$arguments487['package'] = 'TYPO3.Neos';
$arguments487['value'] = NULL;
$arguments487['arguments'] = array (
);
$arguments487['source'] = 'Main';
$arguments487['quantity'] = NULL;
$arguments487['languageIdentifier'] = NULL;
$renderChildrenClosure488 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper489 = $self->getViewHelper('$viewHelper489', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper489->setArguments($arguments487);
$viewHelper489->setRenderingContext($renderingContext);
$viewHelper489->setRenderChildrenClosure($renderChildrenClosure488);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments486['value'] = $viewHelper489->initializeArgumentsAndRender();
$arguments486['keepQuotes'] = false;
$arguments486['encoding'] = 'UTF-8';
$arguments486['doubleEncode'] = true;
$renderChildrenClosure490 = function() use ($renderingContext, $self) {
return NULL;
};
$value491 = ($arguments486['value'] !== NULL ? $arguments486['value'] : $renderChildrenClosure490());

$output3 .= !is_string($value491) && !(is_object($value491) && method_exists($value491, '__toString')) ? $value491 : htmlspecialchars($value491, ($arguments486['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments486['encoding'], $arguments486['doubleEncode']);

$output3 .= '</a>
						<button type="submit" name="moduleArguments[action]" value="delete" class="neos-button neos-button-danger">
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments492 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments493 = array();
$arguments493['id'] = 'packages.message.confirmDeletes';
$arguments493['source'] = 'Modules';
$arguments493['package'] = 'TYPO3.Neos';
$arguments493['value'] = NULL;
$arguments493['arguments'] = array (
);
$arguments493['quantity'] = NULL;
$arguments493['languageIdentifier'] = NULL;
$renderChildrenClosure494 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper495 = $self->getViewHelper('$viewHelper495', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper495->setArguments($arguments493);
$viewHelper495->setRenderingContext($renderingContext);
$viewHelper495->setRenderChildrenClosure($renderChildrenClosure494);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments492['value'] = $viewHelper495->initializeArgumentsAndRender();
$arguments492['keepQuotes'] = false;
$arguments492['encoding'] = 'UTF-8';
$arguments492['doubleEncode'] = true;
$renderChildrenClosure496 = function() use ($renderingContext, $self) {
return NULL;
};
$value497 = ($arguments492['value'] !== NULL ? $arguments492['value'] : $renderChildrenClosure496());

$output3 .= !is_string($value497) && !(is_object($value497) && method_exists($value497, '__toString')) ? $value497 : htmlspecialchars($value497, ($arguments492['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments492['encoding'], $arguments492['doubleEncode']);

$output3 .= '
						</button>
					</div>
				</div>
			</div>
			<div class="neos-modal-backdrop neos-in"></div>
		</div>
	';
return $output3;
};
$viewHelper498 = $self->getViewHelper('$viewHelper498', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper498->setArguments($arguments1);
$viewHelper498->setRenderingContext($renderingContext);
$viewHelper498->setRenderChildrenClosure($renderChildrenClosure2);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output0 .= $viewHelper498->initializeArgumentsAndRender();

$output0 .= '

	<script>
		(function($) {
			$(\'#check-all\').change(function() {
				var value = false;
				if ($(this).is(\':checked\')) {
					value = true;
					$(\'.batch-action\').removeClass(\'neos-disabled\').removeAttr(\'disabled\');
				} else {
					$(\'.batch-action\').addClass(\'neos-disabled\').attr(\'disabled\', \'disabled\');
				}
				$(\'tbody input[type="checkbox"]\').prop(\'checked\', value);
			});
			$(\'tbody input[type="checkbox"]\').change(function() {
				if ($(\'tbody input[type="checkbox"]:checked\').length > 0) {
					$(\'.batch-action\').removeClass(\'neos-disabled\').removeAttr(\'disabled\')
				} else {
					$(\'.batch-action\').addClass(\'neos-disabled\').attr(\'disabled\', \'disabled\');
				}
			});
			$(\'.fold-toggle\').click(function() {
				$(this).toggleClass(\'icon-chevron-down icon-chevron-up\');
				$(\'tr.\' + $(this).data(\'toggle\')).toggle();
			});
		})(jQuery);
	</script>
';

return $output0;
}
/**
 * Main Render function
 */
public function render(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output499 = '';

$output499 .= '
';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments500 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\LayoutViewHelper
$arguments501 = array();
$arguments501['name'] = 'BackendSubModule';
$renderChildrenClosure502 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper503 = $self->getViewHelper('$viewHelper503', $renderingContext, 'TYPO3\Fluid\ViewHelpers\LayoutViewHelper');
$viewHelper503->setArguments($arguments501);
$viewHelper503->setRenderingContext($renderingContext);
$viewHelper503->setRenderChildrenClosure($renderChildrenClosure502);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\LayoutViewHelper
$arguments500['value'] = $viewHelper503->initializeArgumentsAndRender();
$arguments500['keepQuotes'] = false;
$arguments500['encoding'] = 'UTF-8';
$arguments500['doubleEncode'] = true;
$renderChildrenClosure504 = function() use ($renderingContext, $self) {
return NULL;
};
$value505 = ($arguments500['value'] !== NULL ? $arguments500['value'] : $renderChildrenClosure504());

$output499 .= !is_string($value505) && !(is_object($value505) && method_exists($value505, '__toString')) ? $value505 : htmlspecialchars($value505, ($arguments500['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments500['encoding'], $arguments500['doubleEncode']);

$output499 .= '

';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\SectionViewHelper
$arguments506 = array();
$arguments506['name'] = 'content';
$renderChildrenClosure507 = function() use ($renderingContext, $self) {
$output508 = '';

$output508 .= '
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments509 = array();
$arguments509['action'] = 'batch';
$arguments509['additionalAttributes'] = NULL;
$arguments509['data'] = NULL;
$arguments509['arguments'] = array (
);
$arguments509['controller'] = NULL;
$arguments509['package'] = NULL;
$arguments509['subpackage'] = NULL;
$arguments509['object'] = NULL;
$arguments509['section'] = '';
$arguments509['format'] = '';
$arguments509['additionalParams'] = array (
);
$arguments509['absolute'] = false;
$arguments509['addQueryString'] = false;
$arguments509['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments509['fieldNamePrefix'] = NULL;
$arguments509['actionUri'] = NULL;
$arguments509['objectName'] = NULL;
$arguments509['useParentRequest'] = false;
$arguments509['enctype'] = NULL;
$arguments509['method'] = NULL;
$arguments509['name'] = NULL;
$arguments509['onreset'] = NULL;
$arguments509['onsubmit'] = NULL;
$arguments509['class'] = NULL;
$arguments509['dir'] = NULL;
$arguments509['id'] = NULL;
$arguments509['lang'] = NULL;
$arguments509['style'] = NULL;
$arguments509['title'] = NULL;
$arguments509['accesskey'] = NULL;
$arguments509['tabindex'] = NULL;
$arguments509['onclick'] = NULL;
$renderChildrenClosure510 = function() use ($renderingContext, $self) {
$output511 = '';

$output511 .= '
		<div class="neos-row-fluid">
			<legend>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments512 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments513 = array();
$arguments513['id'] = 'packages.index.legend';
$arguments513['source'] = 'Modules';
$arguments513['package'] = 'TYPO3.Neos';
$arguments513['value'] = NULL;
$arguments513['arguments'] = array (
);
$arguments513['quantity'] = NULL;
$arguments513['languageIdentifier'] = NULL;
$renderChildrenClosure514 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper515 = $self->getViewHelper('$viewHelper515', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper515->setArguments($arguments513);
$viewHelper515->setRenderingContext($renderingContext);
$viewHelper515->setRenderChildrenClosure($renderChildrenClosure514);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments512['value'] = $viewHelper515->initializeArgumentsAndRender();
$arguments512['keepQuotes'] = false;
$arguments512['encoding'] = 'UTF-8';
$arguments512['doubleEncode'] = true;
$renderChildrenClosure516 = function() use ($renderingContext, $self) {
return NULL;
};
$value517 = ($arguments512['value'] !== NULL ? $arguments512['value'] : $renderChildrenClosure516());

$output511 .= !is_string($value517) && !(is_object($value517) && method_exists($value517, '__toString')) ? $value517 : htmlspecialchars($value517, ($arguments512['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments512['encoding'], $arguments512['doubleEncode']);

$output511 .= '</legend>
			<br />
			<table class="neos-table">
				<thead>
					<th class="check">
						<label for="check-all" class="neos-checkbox">
							<input type="checkbox" id="check-all" /><span></span>
						</label>
					</th>
					<th>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments518 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments519 = array();
$arguments519['id'] = 'packages.name';
$arguments519['source'] = 'Modules';
$arguments519['package'] = 'TYPO3.Neos';
$arguments519['value'] = NULL;
$arguments519['arguments'] = array (
);
$arguments519['quantity'] = NULL;
$arguments519['languageIdentifier'] = NULL;
$renderChildrenClosure520 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper521 = $self->getViewHelper('$viewHelper521', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper521->setArguments($arguments519);
$viewHelper521->setRenderingContext($renderingContext);
$viewHelper521->setRenderChildrenClosure($renderChildrenClosure520);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments518['value'] = $viewHelper521->initializeArgumentsAndRender();
$arguments518['keepQuotes'] = false;
$arguments518['encoding'] = 'UTF-8';
$arguments518['doubleEncode'] = true;
$renderChildrenClosure522 = function() use ($renderingContext, $self) {
return NULL;
};
$value523 = ($arguments518['value'] !== NULL ? $arguments518['value'] : $renderChildrenClosure522());

$output511 .= !is_string($value523) && !(is_object($value523) && method_exists($value523, '__toString')) ? $value523 : htmlspecialchars($value523, ($arguments518['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments518['encoding'], $arguments518['doubleEncode']);

$output511 .= '</th>
					<th>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments524 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments525 = array();
$arguments525['id'] = 'version';
$arguments525['package'] = 'TYPO3.Neos';
$arguments525['value'] = NULL;
$arguments525['arguments'] = array (
);
$arguments525['source'] = 'Main';
$arguments525['quantity'] = NULL;
$arguments525['languageIdentifier'] = NULL;
$renderChildrenClosure526 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper527 = $self->getViewHelper('$viewHelper527', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper527->setArguments($arguments525);
$viewHelper527->setRenderingContext($renderingContext);
$viewHelper527->setRenderChildrenClosure($renderChildrenClosure526);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments524['value'] = $viewHelper527->initializeArgumentsAndRender();
$arguments524['keepQuotes'] = false;
$arguments524['encoding'] = 'UTF-8';
$arguments524['doubleEncode'] = true;
$renderChildrenClosure528 = function() use ($renderingContext, $self) {
return NULL;
};
$value529 = ($arguments524['value'] !== NULL ? $arguments524['value'] : $renderChildrenClosure528());

$output511 .= !is_string($value529) && !(is_object($value529) && method_exists($value529, '__toString')) ? $value529 : htmlspecialchars($value529, ($arguments524['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments524['encoding'], $arguments524['doubleEncode']);

$output511 .= '</th>
					<th>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments530 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments531 = array();
$arguments531['id'] = 'packages.key';
$arguments531['source'] = 'Modules';
$arguments531['package'] = 'TYPO3.Neos';
$arguments531['value'] = NULL;
$arguments531['arguments'] = array (
);
$arguments531['quantity'] = NULL;
$arguments531['languageIdentifier'] = NULL;
$renderChildrenClosure532 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper533 = $self->getViewHelper('$viewHelper533', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper533->setArguments($arguments531);
$viewHelper533->setRenderingContext($renderingContext);
$viewHelper533->setRenderChildrenClosure($renderChildrenClosure532);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments530['value'] = $viewHelper533->initializeArgumentsAndRender();
$arguments530['keepQuotes'] = false;
$arguments530['encoding'] = 'UTF-8';
$arguments530['doubleEncode'] = true;
$renderChildrenClosure534 = function() use ($renderingContext, $self) {
return NULL;
};
$value535 = ($arguments530['value'] !== NULL ? $arguments530['value'] : $renderChildrenClosure534());

$output511 .= !is_string($value535) && !(is_object($value535) && method_exists($value535, '__toString')) ? $value535 : htmlspecialchars($value535, ($arguments530['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments530['encoding'], $arguments530['doubleEncode']);

$output511 .= '</th>
					<th>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments536 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments537 = array();
$arguments537['id'] = 'packages.type';
$arguments537['source'] = 'Modules';
$arguments537['package'] = 'TYPO3.Neos';
$arguments537['value'] = NULL;
$arguments537['arguments'] = array (
);
$arguments537['quantity'] = NULL;
$arguments537['languageIdentifier'] = NULL;
$renderChildrenClosure538 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper539 = $self->getViewHelper('$viewHelper539', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper539->setArguments($arguments537);
$viewHelper539->setRenderingContext($renderingContext);
$viewHelper539->setRenderChildrenClosure($renderChildrenClosure538);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments536['value'] = $viewHelper539->initializeArgumentsAndRender();
$arguments536['keepQuotes'] = false;
$arguments536['encoding'] = 'UTF-8';
$arguments536['doubleEncode'] = true;
$renderChildrenClosure540 = function() use ($renderingContext, $self) {
return NULL;
};
$value541 = ($arguments536['value'] !== NULL ? $arguments536['value'] : $renderChildrenClosure540());

$output511 .= !is_string($value541) && !(is_object($value541) && method_exists($value541, '__toString')) ? $value541 : htmlspecialchars($value541, ($arguments536['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments536['encoding'], $arguments536['doubleEncode']);

$output511 .= '</th>
					<th>&nbsp;</th>
				</thead>
				<tbody>
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments542 = array();
$arguments542['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageGroups', $renderingContext);
$arguments542['key'] = 'packageGroup';
$arguments542['as'] = 'packages';
$arguments542['reverse'] = false;
$arguments542['iteration'] = NULL;
$renderChildrenClosure543 = function() use ($renderingContext, $self) {
$output544 = '';

$output544 .= '
						<tr class="neos-folder" id="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments545 = array();
$arguments545['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageGroup', $renderingContext);
$arguments545['keepQuotes'] = false;
$arguments545['encoding'] = 'UTF-8';
$arguments545['doubleEncode'] = true;
$renderChildrenClosure546 = function() use ($renderingContext, $self) {
return NULL;
};
$value547 = ($arguments545['value'] !== NULL ? $arguments545['value'] : $renderChildrenClosure546());

$output544 .= !is_string($value547) && !(is_object($value547) && method_exists($value547, '__toString')) ? $value547 : htmlspecialchars($value547, ($arguments545['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments545['encoding'], $arguments545['doubleEncode']);

$output544 .= '">
							<td colspan="2" class="neos-priority1">
								<strong>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments548 = array();
$arguments548['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageGroup', $renderingContext);
$arguments548['keepQuotes'] = false;
$arguments548['encoding'] = 'UTF-8';
$arguments548['doubleEncode'] = true;
$renderChildrenClosure549 = function() use ($renderingContext, $self) {
return NULL;
};
$value550 = ($arguments548['value'] !== NULL ? $arguments548['value'] : $renderChildrenClosure549());

$output544 .= !is_string($value550) && !(is_object($value550) && method_exists($value550, '__toString')) ? $value550 : htmlspecialchars($value550, ($arguments548['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments548['encoding'], $arguments548['doubleEncode']);

$output544 .= '</strong>
							</td>
							<td class="neos-priority2">&nbsp;</td>
							<td class="neos-priority3">&nbsp;</td>
							<td class="neos-priority3">&nbsp;</td>
							<td class="neos-priority1 neos-aRight">
								<i class="fold-toggle icon-chevron-up icon-white" data-toggle="fold-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments551 = array();
$arguments551['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageGroup', $renderingContext);
$arguments551['keepQuotes'] = false;
$arguments551['encoding'] = 'UTF-8';
$arguments551['doubleEncode'] = true;
$renderChildrenClosure552 = function() use ($renderingContext, $self) {
return NULL;
};
$value553 = ($arguments551['value'] !== NULL ? $arguments551['value'] : $renderChildrenClosure552());

$output544 .= !is_string($value553) && !(is_object($value553) && method_exists($value553, '__toString')) ? $value553 : htmlspecialchars($value553, ($arguments551['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments551['encoding'], $arguments551['doubleEncode']);

$output544 .= '"></i>
							</td>
						</tr>
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments554 = array();
$arguments554['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packages', $renderingContext);
$arguments554['key'] = 'packageKey';
$arguments554['as'] = 'package';
$arguments554['reverse'] = false;
$arguments554['iteration'] = NULL;
$renderChildrenClosure555 = function() use ($renderingContext, $self) {
$output556 = '';

$output556 .= '
							<tr class="fold-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments557 = array();
$arguments557['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageGroup', $renderingContext);
$arguments557['keepQuotes'] = false;
$arguments557['encoding'] = 'UTF-8';
$arguments557['doubleEncode'] = true;
$renderChildrenClosure558 = function() use ($renderingContext, $self) {
return NULL;
};
$value559 = ($arguments557['value'] !== NULL ? $arguments557['value'] : $renderChildrenClosure558());

$output556 .= !is_string($value559) && !(is_object($value559) && method_exists($value559, '__toString')) ? $value559 : htmlspecialchars($value559, ($arguments557['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments557['encoding'], $arguments557['doubleEncode']);
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments560 = array();
// Rendering Boolean node
$arguments560['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.isActive', $renderingContext));
$arguments560['else'] = ' muted';
$arguments560['then'] = NULL;
$renderChildrenClosure561 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper562 = $self->getViewHelper('$viewHelper562', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper562->setArguments($arguments560);
$viewHelper562->setRenderingContext($renderingContext);
$viewHelper562->setRenderChildrenClosure($renderChildrenClosure561);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output556 .= $viewHelper562->initializeArgumentsAndRender();

$output556 .= '"';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments563 = array();
// Rendering Boolean node
$arguments563['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.description', $renderingContext));
$output564 = '';

$output564 .= ' title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments565 = array();
$arguments565['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.description', $renderingContext);
$arguments565['keepQuotes'] = false;
$arguments565['encoding'] = 'UTF-8';
$arguments565['doubleEncode'] = true;
$renderChildrenClosure566 = function() use ($renderingContext, $self) {
return NULL;
};
$value567 = ($arguments565['value'] !== NULL ? $arguments565['value'] : $renderChildrenClosure566());

$output564 .= !is_string($value567) && !(is_object($value567) && method_exists($value567, '__toString')) ? $value567 : htmlspecialchars($value567, ($arguments565['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments565['encoding'], $arguments565['doubleEncode']);

$output564 .= '"';
$arguments563['then'] = $output564;
$arguments563['else'] = NULL;
$renderChildrenClosure568 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper569 = $self->getViewHelper('$viewHelper569', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper569->setArguments($arguments563);
$viewHelper569->setRenderingContext($renderingContext);
$viewHelper569->setRenderChildrenClosure($renderChildrenClosure568);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output556 .= $viewHelper569->initializeArgumentsAndRender();

$output556 .= ' data-neos-toggle="tooltip">
								<td class="check neos-priority1">
									<label for="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments570 = array();
$arguments570['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments570['keepQuotes'] = false;
$arguments570['encoding'] = 'UTF-8';
$arguments570['doubleEncode'] = true;
$renderChildrenClosure571 = function() use ($renderingContext, $self) {
return NULL;
};
$value572 = ($arguments570['value'] !== NULL ? $arguments570['value'] : $renderChildrenClosure571());

$output556 .= !is_string($value572) && !(is_object($value572) && method_exists($value572, '__toString')) ? $value572 : htmlspecialchars($value572, ($arguments570['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments570['encoding'], $arguments570['doubleEncode']);

$output556 .= '" class="neos-checkbox">
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\CheckboxViewHelper
$arguments573 = array();
$arguments573['name'] = 'packageKeys[]';
$arguments573['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments573['id'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments573['additionalAttributes'] = NULL;
$arguments573['data'] = NULL;
$arguments573['checked'] = NULL;
$arguments573['multiple'] = NULL;
$arguments573['property'] = NULL;
$arguments573['disabled'] = NULL;
$arguments573['errorClass'] = 'f3-form-error';
$arguments573['class'] = NULL;
$arguments573['dir'] = NULL;
$arguments573['lang'] = NULL;
$arguments573['style'] = NULL;
$arguments573['title'] = NULL;
$arguments573['accesskey'] = NULL;
$arguments573['tabindex'] = NULL;
$arguments573['onclick'] = NULL;
$renderChildrenClosure574 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper575 = $self->getViewHelper('$viewHelper575', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\CheckboxViewHelper');
$viewHelper575->setArguments($arguments573);
$viewHelper575->setRenderingContext($renderingContext);
$viewHelper575->setRenderChildrenClosure($renderChildrenClosure574);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\CheckboxViewHelper

$output556 .= $viewHelper575->initializeArgumentsAndRender();

$output556 .= '<span></span>
									</label>
								</td>
								<td class="package-name neos-priority1">
									<label for="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments576 = array();
$arguments576['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments576['keepQuotes'] = false;
$arguments576['encoding'] = 'UTF-8';
$arguments576['doubleEncode'] = true;
$renderChildrenClosure577 = function() use ($renderingContext, $self) {
return NULL;
};
$value578 = ($arguments576['value'] !== NULL ? $arguments576['value'] : $renderChildrenClosure577());

$output556 .= !is_string($value578) && !(is_object($value578) && method_exists($value578, '__toString')) ? $value578 : htmlspecialchars($value578, ($arguments576['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments576['encoding'], $arguments576['doubleEncode']);

$output556 .= '">
										<span class="neos-label">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments579 = array();
$arguments579['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.name', $renderingContext);
$arguments579['keepQuotes'] = false;
$arguments579['encoding'] = 'UTF-8';
$arguments579['doubleEncode'] = true;
$renderChildrenClosure580 = function() use ($renderingContext, $self) {
return NULL;
};
$value581 = ($arguments579['value'] !== NULL ? $arguments579['value'] : $renderChildrenClosure580());

$output556 .= !is_string($value581) && !(is_object($value581) && method_exists($value581, '__toString')) ? $value581 : htmlspecialchars($value581, ($arguments579['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments579['encoding'], $arguments579['doubleEncode']);

$output556 .= '</span>
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments582 = array();
// Rendering Boolean node
$arguments582['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.isActive', $renderingContext));
$arguments582['then'] = NULL;
$arguments582['else'] = NULL;
$renderChildrenClosure583 = function() use ($renderingContext, $self) {
$output584 = '';

$output584 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments585 = array();
$renderChildrenClosure586 = function() use ($renderingContext, $self) {
$output587 = '';

$output587 .= '
												<span class="neos-badge neos-badge-warning">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments588 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments589 = array();
$arguments589['id'] = 'packages.deactivated';
$arguments589['source'] = 'Modules';
$arguments589['package'] = 'TYPO3.Neos';
$arguments589['value'] = NULL;
$arguments589['arguments'] = array (
);
$arguments589['quantity'] = NULL;
$arguments589['languageIdentifier'] = NULL;
$renderChildrenClosure590 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper591 = $self->getViewHelper('$viewHelper591', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper591->setArguments($arguments589);
$viewHelper591->setRenderingContext($renderingContext);
$viewHelper591->setRenderChildrenClosure($renderChildrenClosure590);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments588['value'] = $viewHelper591->initializeArgumentsAndRender();
$arguments588['keepQuotes'] = false;
$arguments588['encoding'] = 'UTF-8';
$arguments588['doubleEncode'] = true;
$renderChildrenClosure592 = function() use ($renderingContext, $self) {
return NULL;
};
$value593 = ($arguments588['value'] !== NULL ? $arguments588['value'] : $renderChildrenClosure592());

$output587 .= !is_string($value593) && !(is_object($value593) && method_exists($value593, '__toString')) ? $value593 : htmlspecialchars($value593, ($arguments588['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments588['encoding'], $arguments588['doubleEncode']);

$output587 .= '</span>
											';
return $output587;
};
$viewHelper594 = $self->getViewHelper('$viewHelper594', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper594->setArguments($arguments585);
$viewHelper594->setRenderingContext($renderingContext);
$viewHelper594->setRenderChildrenClosure($renderChildrenClosure586);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output584 .= $viewHelper594->initializeArgumentsAndRender();

$output584 .= '
										';
return $output584;
};
$arguments582['__elseClosure'] = function() use ($renderingContext, $self) {
$output595 = '';

$output595 .= '
												<span class="neos-badge neos-badge-warning">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments596 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments597 = array();
$arguments597['id'] = 'packages.deactivated';
$arguments597['source'] = 'Modules';
$arguments597['package'] = 'TYPO3.Neos';
$arguments597['value'] = NULL;
$arguments597['arguments'] = array (
);
$arguments597['quantity'] = NULL;
$arguments597['languageIdentifier'] = NULL;
$renderChildrenClosure598 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper599 = $self->getViewHelper('$viewHelper599', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper599->setArguments($arguments597);
$viewHelper599->setRenderingContext($renderingContext);
$viewHelper599->setRenderChildrenClosure($renderChildrenClosure598);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments596['value'] = $viewHelper599->initializeArgumentsAndRender();
$arguments596['keepQuotes'] = false;
$arguments596['encoding'] = 'UTF-8';
$arguments596['doubleEncode'] = true;
$renderChildrenClosure600 = function() use ($renderingContext, $self) {
return NULL;
};
$value601 = ($arguments596['value'] !== NULL ? $arguments596['value'] : $renderChildrenClosure600());

$output595 .= !is_string($value601) && !(is_object($value601) && method_exists($value601, '__toString')) ? $value601 : htmlspecialchars($value601, ($arguments596['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments596['encoding'], $arguments596['doubleEncode']);

$output595 .= '</span>
											';
return $output595;
};
$viewHelper602 = $self->getViewHelper('$viewHelper602', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper602->setArguments($arguments582);
$viewHelper602->setRenderingContext($renderingContext);
$viewHelper602->setRenderChildrenClosure($renderChildrenClosure583);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output556 .= $viewHelper602->initializeArgumentsAndRender();

$output556 .= '
									</label>
								</td>
								<td class="package-version neos-priority2">
									<label for="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments603 = array();
$arguments603['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments603['keepQuotes'] = false;
$arguments603['encoding'] = 'UTF-8';
$arguments603['doubleEncode'] = true;
$renderChildrenClosure604 = function() use ($renderingContext, $self) {
return NULL;
};
$value605 = ($arguments603['value'] !== NULL ? $arguments603['value'] : $renderChildrenClosure604());

$output556 .= !is_string($value605) && !(is_object($value605) && method_exists($value605, '__toString')) ? $value605 : htmlspecialchars($value605, ($arguments603['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments603['encoding'], $arguments603['doubleEncode']);

$output556 .= '">
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments606 = array();
// Rendering Boolean node
$arguments606['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.version', $renderingContext));
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments607 = array();
$arguments607['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.version', $renderingContext);
$arguments607['keepQuotes'] = false;
$arguments607['encoding'] = 'UTF-8';
$arguments607['doubleEncode'] = true;
$renderChildrenClosure608 = function() use ($renderingContext, $self) {
return NULL;
};
$value609 = ($arguments607['value'] !== NULL ? $arguments607['value'] : $renderChildrenClosure608());
$arguments606['then'] = !is_string($value609) && !(is_object($value609) && method_exists($value609, '__toString')) ? $value609 : htmlspecialchars($value609, ($arguments607['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments607['encoding'], $arguments607['doubleEncode']);
$arguments606['else'] = '&nbsp;';
$renderChildrenClosure610 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper611 = $self->getViewHelper('$viewHelper611', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper611->setArguments($arguments606);
$viewHelper611->setRenderingContext($renderingContext);
$viewHelper611->setRenderChildrenClosure($renderChildrenClosure610);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output556 .= $viewHelper611->initializeArgumentsAndRender();

$output556 .= '
									</label>
								</td>
								<td class="package-key neos-priority3">
									<label for="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments612 = array();
$arguments612['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments612['keepQuotes'] = false;
$arguments612['encoding'] = 'UTF-8';
$arguments612['doubleEncode'] = true;
$renderChildrenClosure613 = function() use ($renderingContext, $self) {
return NULL;
};
$value614 = ($arguments612['value'] !== NULL ? $arguments612['value'] : $renderChildrenClosure613());

$output556 .= !is_string($value614) && !(is_object($value614) && method_exists($value614, '__toString')) ? $value614 : htmlspecialchars($value614, ($arguments612['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments612['encoding'], $arguments612['doubleEncode']);

$output556 .= '"">
										<span class="neos-label">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments615 = array();
$arguments615['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments615['keepQuotes'] = false;
$arguments615['encoding'] = 'UTF-8';
$arguments615['doubleEncode'] = true;
$renderChildrenClosure616 = function() use ($renderingContext, $self) {
return NULL;
};
$value617 = ($arguments615['value'] !== NULL ? $arguments615['value'] : $renderChildrenClosure616());

$output556 .= !is_string($value617) && !(is_object($value617) && method_exists($value617, '__toString')) ? $value617 : htmlspecialchars($value617, ($arguments615['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments615['encoding'], $arguments615['doubleEncode']);

$output556 .= '</span>
									</label>
								</td>
								<td class="package-type neos-priority3">
									<label for="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments618 = array();
$arguments618['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments618['keepQuotes'] = false;
$arguments618['encoding'] = 'UTF-8';
$arguments618['doubleEncode'] = true;
$renderChildrenClosure619 = function() use ($renderingContext, $self) {
return NULL;
};
$value620 = ($arguments618['value'] !== NULL ? $arguments618['value'] : $renderChildrenClosure619());

$output556 .= !is_string($value620) && !(is_object($value620) && method_exists($value620, '__toString')) ? $value620 : htmlspecialchars($value620, ($arguments618['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments618['encoding'], $arguments618['doubleEncode']);

$output556 .= '">
										<span class="neos-label">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments621 = array();
$arguments621['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.type', $renderingContext);
$arguments621['keepQuotes'] = false;
$arguments621['encoding'] = 'UTF-8';
$arguments621['doubleEncode'] = true;
$renderChildrenClosure622 = function() use ($renderingContext, $self) {
return NULL;
};
$value623 = ($arguments621['value'] !== NULL ? $arguments621['value'] : $renderChildrenClosure622());

$output556 .= !is_string($value623) && !(is_object($value623) && method_exists($value623, '__toString')) ? $value623 : htmlspecialchars($value623, ($arguments621['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments621['encoding'], $arguments621['doubleEncode']);

$output556 .= '</span>
									</label>
								</td>
								<td class="neos-action neos-priority1">
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments624 = array();
// Rendering Boolean node
$arguments624['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.isActive', $renderingContext));
$arguments624['then'] = NULL;
$arguments624['else'] = NULL;
$renderChildrenClosure625 = function() use ($renderingContext, $self) {
$output626 = '';

$output626 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments627 = array();
$renderChildrenClosure628 = function() use ($renderingContext, $self) {
$output629 = '';

$output629 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments630 = array();
// Rendering Boolean node
$arguments630['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'isDevelopmentContext', $renderingContext));
$arguments630['then'] = NULL;
$arguments630['else'] = NULL;
$renderChildrenClosure631 = function() use ($renderingContext, $self) {
$output632 = '';

$output632 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments633 = array();
// Rendering Boolean node
$arguments633['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.isFrozen', $renderingContext));
$arguments633['then'] = NULL;
$arguments633['else'] = NULL;
$renderChildrenClosure634 = function() use ($renderingContext, $self) {
$output635 = '';

$output635 .= '
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments636 = array();
$renderChildrenClosure637 = function() use ($renderingContext, $self) {
$output638 = '';

$output638 .= '
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments639 = array();
$arguments639['action'] = 'unfreeze';
$arguments639['class'] = 'neos-button neos-button-freeze neos-active';
// Rendering Array
$array640 = array();
$array640['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments639['arguments'] = $array640;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments641 = array();
$arguments641['id'] = 'packages.tooltip.unfreeze';
$arguments641['source'] = 'Modules';
$arguments641['package'] = 'TYPO3.Neos';
$arguments641['value'] = NULL;
$arguments641['arguments'] = array (
);
$arguments641['quantity'] = NULL;
$arguments641['languageIdentifier'] = NULL;
$renderChildrenClosure642 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper643 = $self->getViewHelper('$viewHelper643', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper643->setArguments($arguments641);
$viewHelper643->setRenderingContext($renderingContext);
$viewHelper643->setRenderChildrenClosure($renderChildrenClosure642);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments639['title'] = $viewHelper643->initializeArgumentsAndRender();
// Rendering Array
$array644 = array();
$array644['data-neos-toggle'] = 'tooltip';
$arguments639['additionalAttributes'] = $array644;
$arguments639['data'] = NULL;
$arguments639['controller'] = NULL;
$arguments639['package'] = NULL;
$arguments639['subpackage'] = NULL;
$arguments639['section'] = '';
$arguments639['format'] = '';
$arguments639['additionalParams'] = array (
);
$arguments639['addQueryString'] = false;
$arguments639['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments639['useParentRequest'] = false;
$arguments639['absolute'] = true;
$arguments639['dir'] = NULL;
$arguments639['id'] = NULL;
$arguments639['lang'] = NULL;
$arguments639['style'] = NULL;
$arguments639['accesskey'] = NULL;
$arguments639['tabindex'] = NULL;
$arguments639['onclick'] = NULL;
$arguments639['name'] = NULL;
$arguments639['rel'] = NULL;
$arguments639['rev'] = NULL;
$arguments639['target'] = NULL;
$renderChildrenClosure645 = function() use ($renderingContext, $self) {
return '
															<i class="icon-asterisk icon-white"></i>
														';
};
$viewHelper646 = $self->getViewHelper('$viewHelper646', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper646->setArguments($arguments639);
$viewHelper646->setRenderingContext($renderingContext);
$viewHelper646->setRenderChildrenClosure($renderChildrenClosure645);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output638 .= $viewHelper646->initializeArgumentsAndRender();

$output638 .= '
													';
return $output638;
};
$viewHelper647 = $self->getViewHelper('$viewHelper647', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper647->setArguments($arguments636);
$viewHelper647->setRenderingContext($renderingContext);
$viewHelper647->setRenderChildrenClosure($renderChildrenClosure637);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output635 .= $viewHelper647->initializeArgumentsAndRender();

$output635 .= '
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments648 = array();
$renderChildrenClosure649 = function() use ($renderingContext, $self) {
$output650 = '';

$output650 .= '
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments651 = array();
$arguments651['action'] = 'freeze';
$arguments651['class'] = 'neos-button neos-button-freeze';
// Rendering Array
$array652 = array();
$array652['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments651['arguments'] = $array652;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments653 = array();
$arguments653['id'] = 'packages.tooltip.freeze';
$arguments653['source'] = 'Modules';
$arguments653['package'] = 'TYPO3.Neos';
$arguments653['value'] = NULL;
$arguments653['arguments'] = array (
);
$arguments653['quantity'] = NULL;
$arguments653['languageIdentifier'] = NULL;
$renderChildrenClosure654 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper655 = $self->getViewHelper('$viewHelper655', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper655->setArguments($arguments653);
$viewHelper655->setRenderingContext($renderingContext);
$viewHelper655->setRenderChildrenClosure($renderChildrenClosure654);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments651['title'] = $viewHelper655->initializeArgumentsAndRender();
// Rendering Array
$array656 = array();
$array656['data-neos-toggle'] = 'tooltip';
$arguments651['additionalAttributes'] = $array656;
$arguments651['data'] = NULL;
$arguments651['controller'] = NULL;
$arguments651['package'] = NULL;
$arguments651['subpackage'] = NULL;
$arguments651['section'] = '';
$arguments651['format'] = '';
$arguments651['additionalParams'] = array (
);
$arguments651['addQueryString'] = false;
$arguments651['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments651['useParentRequest'] = false;
$arguments651['absolute'] = true;
$arguments651['dir'] = NULL;
$arguments651['id'] = NULL;
$arguments651['lang'] = NULL;
$arguments651['style'] = NULL;
$arguments651['accesskey'] = NULL;
$arguments651['tabindex'] = NULL;
$arguments651['onclick'] = NULL;
$arguments651['name'] = NULL;
$arguments651['rel'] = NULL;
$arguments651['rev'] = NULL;
$arguments651['target'] = NULL;
$renderChildrenClosure657 = function() use ($renderingContext, $self) {
return '
															<i class="icon-asterisk icon-white"></i>
														';
};
$viewHelper658 = $self->getViewHelper('$viewHelper658', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper658->setArguments($arguments651);
$viewHelper658->setRenderingContext($renderingContext);
$viewHelper658->setRenderChildrenClosure($renderChildrenClosure657);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output650 .= $viewHelper658->initializeArgumentsAndRender();

$output650 .= '
													';
return $output650;
};
$viewHelper659 = $self->getViewHelper('$viewHelper659', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper659->setArguments($arguments648);
$viewHelper659->setRenderingContext($renderingContext);
$viewHelper659->setRenderChildrenClosure($renderChildrenClosure649);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output635 .= $viewHelper659->initializeArgumentsAndRender();

$output635 .= '
												';
return $output635;
};
$arguments633['__thenClosure'] = function() use ($renderingContext, $self) {
$output660 = '';

$output660 .= '
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments661 = array();
$arguments661['action'] = 'unfreeze';
$arguments661['class'] = 'neos-button neos-button-freeze neos-active';
// Rendering Array
$array662 = array();
$array662['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments661['arguments'] = $array662;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments663 = array();
$arguments663['id'] = 'packages.tooltip.unfreeze';
$arguments663['source'] = 'Modules';
$arguments663['package'] = 'TYPO3.Neos';
$arguments663['value'] = NULL;
$arguments663['arguments'] = array (
);
$arguments663['quantity'] = NULL;
$arguments663['languageIdentifier'] = NULL;
$renderChildrenClosure664 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper665 = $self->getViewHelper('$viewHelper665', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper665->setArguments($arguments663);
$viewHelper665->setRenderingContext($renderingContext);
$viewHelper665->setRenderChildrenClosure($renderChildrenClosure664);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments661['title'] = $viewHelper665->initializeArgumentsAndRender();
// Rendering Array
$array666 = array();
$array666['data-neos-toggle'] = 'tooltip';
$arguments661['additionalAttributes'] = $array666;
$arguments661['data'] = NULL;
$arguments661['controller'] = NULL;
$arguments661['package'] = NULL;
$arguments661['subpackage'] = NULL;
$arguments661['section'] = '';
$arguments661['format'] = '';
$arguments661['additionalParams'] = array (
);
$arguments661['addQueryString'] = false;
$arguments661['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments661['useParentRequest'] = false;
$arguments661['absolute'] = true;
$arguments661['dir'] = NULL;
$arguments661['id'] = NULL;
$arguments661['lang'] = NULL;
$arguments661['style'] = NULL;
$arguments661['accesskey'] = NULL;
$arguments661['tabindex'] = NULL;
$arguments661['onclick'] = NULL;
$arguments661['name'] = NULL;
$arguments661['rel'] = NULL;
$arguments661['rev'] = NULL;
$arguments661['target'] = NULL;
$renderChildrenClosure667 = function() use ($renderingContext, $self) {
return '
															<i class="icon-asterisk icon-white"></i>
														';
};
$viewHelper668 = $self->getViewHelper('$viewHelper668', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper668->setArguments($arguments661);
$viewHelper668->setRenderingContext($renderingContext);
$viewHelper668->setRenderChildrenClosure($renderChildrenClosure667);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output660 .= $viewHelper668->initializeArgumentsAndRender();

$output660 .= '
													';
return $output660;
};
$arguments633['__elseClosure'] = function() use ($renderingContext, $self) {
$output669 = '';

$output669 .= '
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments670 = array();
$arguments670['action'] = 'freeze';
$arguments670['class'] = 'neos-button neos-button-freeze';
// Rendering Array
$array671 = array();
$array671['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments670['arguments'] = $array671;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments672 = array();
$arguments672['id'] = 'packages.tooltip.freeze';
$arguments672['source'] = 'Modules';
$arguments672['package'] = 'TYPO3.Neos';
$arguments672['value'] = NULL;
$arguments672['arguments'] = array (
);
$arguments672['quantity'] = NULL;
$arguments672['languageIdentifier'] = NULL;
$renderChildrenClosure673 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper674 = $self->getViewHelper('$viewHelper674', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper674->setArguments($arguments672);
$viewHelper674->setRenderingContext($renderingContext);
$viewHelper674->setRenderChildrenClosure($renderChildrenClosure673);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments670['title'] = $viewHelper674->initializeArgumentsAndRender();
// Rendering Array
$array675 = array();
$array675['data-neos-toggle'] = 'tooltip';
$arguments670['additionalAttributes'] = $array675;
$arguments670['data'] = NULL;
$arguments670['controller'] = NULL;
$arguments670['package'] = NULL;
$arguments670['subpackage'] = NULL;
$arguments670['section'] = '';
$arguments670['format'] = '';
$arguments670['additionalParams'] = array (
);
$arguments670['addQueryString'] = false;
$arguments670['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments670['useParentRequest'] = false;
$arguments670['absolute'] = true;
$arguments670['dir'] = NULL;
$arguments670['id'] = NULL;
$arguments670['lang'] = NULL;
$arguments670['style'] = NULL;
$arguments670['accesskey'] = NULL;
$arguments670['tabindex'] = NULL;
$arguments670['onclick'] = NULL;
$arguments670['name'] = NULL;
$arguments670['rel'] = NULL;
$arguments670['rev'] = NULL;
$arguments670['target'] = NULL;
$renderChildrenClosure676 = function() use ($renderingContext, $self) {
return '
															<i class="icon-asterisk icon-white"></i>
														';
};
$viewHelper677 = $self->getViewHelper('$viewHelper677', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper677->setArguments($arguments670);
$viewHelper677->setRenderingContext($renderingContext);
$viewHelper677->setRenderChildrenClosure($renderChildrenClosure676);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output669 .= $viewHelper677->initializeArgumentsAndRender();

$output669 .= '
													';
return $output669;
};
$viewHelper678 = $self->getViewHelper('$viewHelper678', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper678->setArguments($arguments633);
$viewHelper678->setRenderingContext($renderingContext);
$viewHelper678->setRenderChildrenClosure($renderChildrenClosure634);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output632 .= $viewHelper678->initializeArgumentsAndRender();

$output632 .= '
											';
return $output632;
};
$viewHelper679 = $self->getViewHelper('$viewHelper679', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper679->setArguments($arguments630);
$viewHelper679->setRenderingContext($renderingContext);
$viewHelper679->setRenderChildrenClosure($renderChildrenClosure631);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output629 .= $viewHelper679->initializeArgumentsAndRender();

$output629 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments680 = array();
// Rendering Boolean node
$arguments680['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.isProtected', $renderingContext));
$arguments680['then'] = NULL;
$arguments680['else'] = NULL;
$renderChildrenClosure681 = function() use ($renderingContext, $self) {
$output682 = '';

$output682 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments683 = array();
$renderChildrenClosure684 = function() use ($renderingContext, $self) {
$output685 = '';

$output685 .= '
													<button class="neos-button neos-button-warning neos-disabled" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments686 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments687 = array();
$arguments687['id'] = 'packages.tooltip.noDeactivate';
$arguments687['source'] = 'Modules';
$arguments687['package'] = 'TYPO3.Neos';
$arguments687['value'] = NULL;
$arguments687['arguments'] = array (
);
$arguments687['quantity'] = NULL;
$arguments687['languageIdentifier'] = NULL;
$renderChildrenClosure688 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper689 = $self->getViewHelper('$viewHelper689', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper689->setArguments($arguments687);
$viewHelper689->setRenderingContext($renderingContext);
$viewHelper689->setRenderChildrenClosure($renderChildrenClosure688);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments686['value'] = $viewHelper689->initializeArgumentsAndRender();
$arguments686['keepQuotes'] = false;
$arguments686['encoding'] = 'UTF-8';
$arguments686['doubleEncode'] = true;
$renderChildrenClosure690 = function() use ($renderingContext, $self) {
return NULL;
};
$value691 = ($arguments686['value'] !== NULL ? $arguments686['value'] : $renderChildrenClosure690());

$output685 .= !is_string($value691) && !(is_object($value691) && method_exists($value691, '__toString')) ? $value691 : htmlspecialchars($value691, ($arguments686['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments686['encoding'], $arguments686['doubleEncode']);

$output685 .= '" disabled="disabled" data-neos-toggle="tooltip">
														<i class="icon-pause icon-white"></i>
													</button>
												';
return $output685;
};
$viewHelper692 = $self->getViewHelper('$viewHelper692', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper692->setArguments($arguments683);
$viewHelper692->setRenderingContext($renderingContext);
$viewHelper692->setRenderChildrenClosure($renderChildrenClosure684);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output682 .= $viewHelper692->initializeArgumentsAndRender();

$output682 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments693 = array();
$renderChildrenClosure694 = function() use ($renderingContext, $self) {
$output695 = '';

$output695 .= '
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments696 = array();
$arguments696['action'] = 'deactivate';
$arguments696['class'] = 'neos-button neos-button-warning';
// Rendering Array
$array697 = array();
$array697['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments696['arguments'] = $array697;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments698 = array();
$arguments698['id'] = 'packages.tooltip.deactivate';
$arguments698['source'] = 'Modules';
$arguments698['package'] = 'TYPO3.Neos';
$arguments698['value'] = NULL;
$arguments698['arguments'] = array (
);
$arguments698['quantity'] = NULL;
$arguments698['languageIdentifier'] = NULL;
$renderChildrenClosure699 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper700 = $self->getViewHelper('$viewHelper700', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper700->setArguments($arguments698);
$viewHelper700->setRenderingContext($renderingContext);
$viewHelper700->setRenderChildrenClosure($renderChildrenClosure699);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments696['title'] = $viewHelper700->initializeArgumentsAndRender();
// Rendering Array
$array701 = array();
$array701['data-neos-toggle'] = 'tooltip';
$arguments696['additionalAttributes'] = $array701;
$arguments696['data'] = NULL;
$arguments696['controller'] = NULL;
$arguments696['package'] = NULL;
$arguments696['subpackage'] = NULL;
$arguments696['section'] = '';
$arguments696['format'] = '';
$arguments696['additionalParams'] = array (
);
$arguments696['addQueryString'] = false;
$arguments696['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments696['useParentRequest'] = false;
$arguments696['absolute'] = true;
$arguments696['dir'] = NULL;
$arguments696['id'] = NULL;
$arguments696['lang'] = NULL;
$arguments696['style'] = NULL;
$arguments696['accesskey'] = NULL;
$arguments696['tabindex'] = NULL;
$arguments696['onclick'] = NULL;
$arguments696['name'] = NULL;
$arguments696['rel'] = NULL;
$arguments696['rev'] = NULL;
$arguments696['target'] = NULL;
$renderChildrenClosure702 = function() use ($renderingContext, $self) {
return '
														<i class="icon-pause icon-white"></i>
													';
};
$viewHelper703 = $self->getViewHelper('$viewHelper703', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper703->setArguments($arguments696);
$viewHelper703->setRenderingContext($renderingContext);
$viewHelper703->setRenderChildrenClosure($renderChildrenClosure702);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output695 .= $viewHelper703->initializeArgumentsAndRender();

$output695 .= '
												';
return $output695;
};
$viewHelper704 = $self->getViewHelper('$viewHelper704', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper704->setArguments($arguments693);
$viewHelper704->setRenderingContext($renderingContext);
$viewHelper704->setRenderChildrenClosure($renderChildrenClosure694);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output682 .= $viewHelper704->initializeArgumentsAndRender();

$output682 .= '
											';
return $output682;
};
$arguments680['__thenClosure'] = function() use ($renderingContext, $self) {
$output705 = '';

$output705 .= '
													<button class="neos-button neos-button-warning neos-disabled" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments706 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments707 = array();
$arguments707['id'] = 'packages.tooltip.noDeactivate';
$arguments707['source'] = 'Modules';
$arguments707['package'] = 'TYPO3.Neos';
$arguments707['value'] = NULL;
$arguments707['arguments'] = array (
);
$arguments707['quantity'] = NULL;
$arguments707['languageIdentifier'] = NULL;
$renderChildrenClosure708 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper709 = $self->getViewHelper('$viewHelper709', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper709->setArguments($arguments707);
$viewHelper709->setRenderingContext($renderingContext);
$viewHelper709->setRenderChildrenClosure($renderChildrenClosure708);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments706['value'] = $viewHelper709->initializeArgumentsAndRender();
$arguments706['keepQuotes'] = false;
$arguments706['encoding'] = 'UTF-8';
$arguments706['doubleEncode'] = true;
$renderChildrenClosure710 = function() use ($renderingContext, $self) {
return NULL;
};
$value711 = ($arguments706['value'] !== NULL ? $arguments706['value'] : $renderChildrenClosure710());

$output705 .= !is_string($value711) && !(is_object($value711) && method_exists($value711, '__toString')) ? $value711 : htmlspecialchars($value711, ($arguments706['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments706['encoding'], $arguments706['doubleEncode']);

$output705 .= '" disabled="disabled" data-neos-toggle="tooltip">
														<i class="icon-pause icon-white"></i>
													</button>
												';
return $output705;
};
$arguments680['__elseClosure'] = function() use ($renderingContext, $self) {
$output712 = '';

$output712 .= '
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments713 = array();
$arguments713['action'] = 'deactivate';
$arguments713['class'] = 'neos-button neos-button-warning';
// Rendering Array
$array714 = array();
$array714['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments713['arguments'] = $array714;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments715 = array();
$arguments715['id'] = 'packages.tooltip.deactivate';
$arguments715['source'] = 'Modules';
$arguments715['package'] = 'TYPO3.Neos';
$arguments715['value'] = NULL;
$arguments715['arguments'] = array (
);
$arguments715['quantity'] = NULL;
$arguments715['languageIdentifier'] = NULL;
$renderChildrenClosure716 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper717 = $self->getViewHelper('$viewHelper717', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper717->setArguments($arguments715);
$viewHelper717->setRenderingContext($renderingContext);
$viewHelper717->setRenderChildrenClosure($renderChildrenClosure716);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments713['title'] = $viewHelper717->initializeArgumentsAndRender();
// Rendering Array
$array718 = array();
$array718['data-neos-toggle'] = 'tooltip';
$arguments713['additionalAttributes'] = $array718;
$arguments713['data'] = NULL;
$arguments713['controller'] = NULL;
$arguments713['package'] = NULL;
$arguments713['subpackage'] = NULL;
$arguments713['section'] = '';
$arguments713['format'] = '';
$arguments713['additionalParams'] = array (
);
$arguments713['addQueryString'] = false;
$arguments713['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments713['useParentRequest'] = false;
$arguments713['absolute'] = true;
$arguments713['dir'] = NULL;
$arguments713['id'] = NULL;
$arguments713['lang'] = NULL;
$arguments713['style'] = NULL;
$arguments713['accesskey'] = NULL;
$arguments713['tabindex'] = NULL;
$arguments713['onclick'] = NULL;
$arguments713['name'] = NULL;
$arguments713['rel'] = NULL;
$arguments713['rev'] = NULL;
$arguments713['target'] = NULL;
$renderChildrenClosure719 = function() use ($renderingContext, $self) {
return '
														<i class="icon-pause icon-white"></i>
													';
};
$viewHelper720 = $self->getViewHelper('$viewHelper720', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper720->setArguments($arguments713);
$viewHelper720->setRenderingContext($renderingContext);
$viewHelper720->setRenderChildrenClosure($renderChildrenClosure719);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output712 .= $viewHelper720->initializeArgumentsAndRender();

$output712 .= '
												';
return $output712;
};
$viewHelper721 = $self->getViewHelper('$viewHelper721', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper721->setArguments($arguments680);
$viewHelper721->setRenderingContext($renderingContext);
$viewHelper721->setRenderChildrenClosure($renderChildrenClosure681);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output629 .= $viewHelper721->initializeArgumentsAndRender();

$output629 .= '
										';
return $output629;
};
$viewHelper722 = $self->getViewHelper('$viewHelper722', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper722->setArguments($arguments627);
$viewHelper722->setRenderingContext($renderingContext);
$viewHelper722->setRenderChildrenClosure($renderChildrenClosure628);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output626 .= $viewHelper722->initializeArgumentsAndRender();

$output626 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments723 = array();
$renderChildrenClosure724 = function() use ($renderingContext, $self) {
$output725 = '';

$output725 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments726 = array();
$arguments726['action'] = 'activate';
$arguments726['class'] = 'neos-button neos-button-success';
// Rendering Array
$array727 = array();
$array727['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments726['arguments'] = $array727;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments728 = array();
$arguments728['id'] = 'packages.tooltip.activate';
$arguments728['source'] = 'Modules';
$arguments728['package'] = 'TYPO3.Neos';
$arguments728['value'] = NULL;
$arguments728['arguments'] = array (
);
$arguments728['quantity'] = NULL;
$arguments728['languageIdentifier'] = NULL;
$renderChildrenClosure729 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper730 = $self->getViewHelper('$viewHelper730', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper730->setArguments($arguments728);
$viewHelper730->setRenderingContext($renderingContext);
$viewHelper730->setRenderChildrenClosure($renderChildrenClosure729);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments726['title'] = $viewHelper730->initializeArgumentsAndRender();
// Rendering Array
$array731 = array();
$array731['data-neos-toggle'] = 'tooltip';
$arguments726['additionalAttributes'] = $array731;
$arguments726['data'] = NULL;
$arguments726['controller'] = NULL;
$arguments726['package'] = NULL;
$arguments726['subpackage'] = NULL;
$arguments726['section'] = '';
$arguments726['format'] = '';
$arguments726['additionalParams'] = array (
);
$arguments726['addQueryString'] = false;
$arguments726['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments726['useParentRequest'] = false;
$arguments726['absolute'] = true;
$arguments726['dir'] = NULL;
$arguments726['id'] = NULL;
$arguments726['lang'] = NULL;
$arguments726['style'] = NULL;
$arguments726['accesskey'] = NULL;
$arguments726['tabindex'] = NULL;
$arguments726['onclick'] = NULL;
$arguments726['name'] = NULL;
$arguments726['rel'] = NULL;
$arguments726['rev'] = NULL;
$arguments726['target'] = NULL;
$renderChildrenClosure732 = function() use ($renderingContext, $self) {
return '
												<i class="icon-play icon-white"></i>
											';
};
$viewHelper733 = $self->getViewHelper('$viewHelper733', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper733->setArguments($arguments726);
$viewHelper733->setRenderingContext($renderingContext);
$viewHelper733->setRenderChildrenClosure($renderChildrenClosure732);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output725 .= $viewHelper733->initializeArgumentsAndRender();

$output725 .= '
										';
return $output725;
};
$viewHelper734 = $self->getViewHelper('$viewHelper734', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper734->setArguments($arguments723);
$viewHelper734->setRenderingContext($renderingContext);
$viewHelper734->setRenderChildrenClosure($renderChildrenClosure724);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output626 .= $viewHelper734->initializeArgumentsAndRender();

$output626 .= '
									';
return $output626;
};
$arguments624['__thenClosure'] = function() use ($renderingContext, $self) {
$output735 = '';

$output735 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments736 = array();
// Rendering Boolean node
$arguments736['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'isDevelopmentContext', $renderingContext));
$arguments736['then'] = NULL;
$arguments736['else'] = NULL;
$renderChildrenClosure737 = function() use ($renderingContext, $self) {
$output738 = '';

$output738 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments739 = array();
// Rendering Boolean node
$arguments739['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.isFrozen', $renderingContext));
$arguments739['then'] = NULL;
$arguments739['else'] = NULL;
$renderChildrenClosure740 = function() use ($renderingContext, $self) {
$output741 = '';

$output741 .= '
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments742 = array();
$renderChildrenClosure743 = function() use ($renderingContext, $self) {
$output744 = '';

$output744 .= '
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments745 = array();
$arguments745['action'] = 'unfreeze';
$arguments745['class'] = 'neos-button neos-button-freeze neos-active';
// Rendering Array
$array746 = array();
$array746['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments745['arguments'] = $array746;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments747 = array();
$arguments747['id'] = 'packages.tooltip.unfreeze';
$arguments747['source'] = 'Modules';
$arguments747['package'] = 'TYPO3.Neos';
$arguments747['value'] = NULL;
$arguments747['arguments'] = array (
);
$arguments747['quantity'] = NULL;
$arguments747['languageIdentifier'] = NULL;
$renderChildrenClosure748 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper749 = $self->getViewHelper('$viewHelper749', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper749->setArguments($arguments747);
$viewHelper749->setRenderingContext($renderingContext);
$viewHelper749->setRenderChildrenClosure($renderChildrenClosure748);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments745['title'] = $viewHelper749->initializeArgumentsAndRender();
// Rendering Array
$array750 = array();
$array750['data-neos-toggle'] = 'tooltip';
$arguments745['additionalAttributes'] = $array750;
$arguments745['data'] = NULL;
$arguments745['controller'] = NULL;
$arguments745['package'] = NULL;
$arguments745['subpackage'] = NULL;
$arguments745['section'] = '';
$arguments745['format'] = '';
$arguments745['additionalParams'] = array (
);
$arguments745['addQueryString'] = false;
$arguments745['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments745['useParentRequest'] = false;
$arguments745['absolute'] = true;
$arguments745['dir'] = NULL;
$arguments745['id'] = NULL;
$arguments745['lang'] = NULL;
$arguments745['style'] = NULL;
$arguments745['accesskey'] = NULL;
$arguments745['tabindex'] = NULL;
$arguments745['onclick'] = NULL;
$arguments745['name'] = NULL;
$arguments745['rel'] = NULL;
$arguments745['rev'] = NULL;
$arguments745['target'] = NULL;
$renderChildrenClosure751 = function() use ($renderingContext, $self) {
return '
															<i class="icon-asterisk icon-white"></i>
														';
};
$viewHelper752 = $self->getViewHelper('$viewHelper752', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper752->setArguments($arguments745);
$viewHelper752->setRenderingContext($renderingContext);
$viewHelper752->setRenderChildrenClosure($renderChildrenClosure751);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output744 .= $viewHelper752->initializeArgumentsAndRender();

$output744 .= '
													';
return $output744;
};
$viewHelper753 = $self->getViewHelper('$viewHelper753', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper753->setArguments($arguments742);
$viewHelper753->setRenderingContext($renderingContext);
$viewHelper753->setRenderChildrenClosure($renderChildrenClosure743);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output741 .= $viewHelper753->initializeArgumentsAndRender();

$output741 .= '
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments754 = array();
$renderChildrenClosure755 = function() use ($renderingContext, $self) {
$output756 = '';

$output756 .= '
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments757 = array();
$arguments757['action'] = 'freeze';
$arguments757['class'] = 'neos-button neos-button-freeze';
// Rendering Array
$array758 = array();
$array758['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments757['arguments'] = $array758;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments759 = array();
$arguments759['id'] = 'packages.tooltip.freeze';
$arguments759['source'] = 'Modules';
$arguments759['package'] = 'TYPO3.Neos';
$arguments759['value'] = NULL;
$arguments759['arguments'] = array (
);
$arguments759['quantity'] = NULL;
$arguments759['languageIdentifier'] = NULL;
$renderChildrenClosure760 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper761 = $self->getViewHelper('$viewHelper761', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper761->setArguments($arguments759);
$viewHelper761->setRenderingContext($renderingContext);
$viewHelper761->setRenderChildrenClosure($renderChildrenClosure760);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments757['title'] = $viewHelper761->initializeArgumentsAndRender();
// Rendering Array
$array762 = array();
$array762['data-neos-toggle'] = 'tooltip';
$arguments757['additionalAttributes'] = $array762;
$arguments757['data'] = NULL;
$arguments757['controller'] = NULL;
$arguments757['package'] = NULL;
$arguments757['subpackage'] = NULL;
$arguments757['section'] = '';
$arguments757['format'] = '';
$arguments757['additionalParams'] = array (
);
$arguments757['addQueryString'] = false;
$arguments757['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments757['useParentRequest'] = false;
$arguments757['absolute'] = true;
$arguments757['dir'] = NULL;
$arguments757['id'] = NULL;
$arguments757['lang'] = NULL;
$arguments757['style'] = NULL;
$arguments757['accesskey'] = NULL;
$arguments757['tabindex'] = NULL;
$arguments757['onclick'] = NULL;
$arguments757['name'] = NULL;
$arguments757['rel'] = NULL;
$arguments757['rev'] = NULL;
$arguments757['target'] = NULL;
$renderChildrenClosure763 = function() use ($renderingContext, $self) {
return '
															<i class="icon-asterisk icon-white"></i>
														';
};
$viewHelper764 = $self->getViewHelper('$viewHelper764', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper764->setArguments($arguments757);
$viewHelper764->setRenderingContext($renderingContext);
$viewHelper764->setRenderChildrenClosure($renderChildrenClosure763);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output756 .= $viewHelper764->initializeArgumentsAndRender();

$output756 .= '
													';
return $output756;
};
$viewHelper765 = $self->getViewHelper('$viewHelper765', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper765->setArguments($arguments754);
$viewHelper765->setRenderingContext($renderingContext);
$viewHelper765->setRenderChildrenClosure($renderChildrenClosure755);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output741 .= $viewHelper765->initializeArgumentsAndRender();

$output741 .= '
												';
return $output741;
};
$arguments739['__thenClosure'] = function() use ($renderingContext, $self) {
$output766 = '';

$output766 .= '
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments767 = array();
$arguments767['action'] = 'unfreeze';
$arguments767['class'] = 'neos-button neos-button-freeze neos-active';
// Rendering Array
$array768 = array();
$array768['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments767['arguments'] = $array768;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments769 = array();
$arguments769['id'] = 'packages.tooltip.unfreeze';
$arguments769['source'] = 'Modules';
$arguments769['package'] = 'TYPO3.Neos';
$arguments769['value'] = NULL;
$arguments769['arguments'] = array (
);
$arguments769['quantity'] = NULL;
$arguments769['languageIdentifier'] = NULL;
$renderChildrenClosure770 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper771 = $self->getViewHelper('$viewHelper771', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper771->setArguments($arguments769);
$viewHelper771->setRenderingContext($renderingContext);
$viewHelper771->setRenderChildrenClosure($renderChildrenClosure770);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments767['title'] = $viewHelper771->initializeArgumentsAndRender();
// Rendering Array
$array772 = array();
$array772['data-neos-toggle'] = 'tooltip';
$arguments767['additionalAttributes'] = $array772;
$arguments767['data'] = NULL;
$arguments767['controller'] = NULL;
$arguments767['package'] = NULL;
$arguments767['subpackage'] = NULL;
$arguments767['section'] = '';
$arguments767['format'] = '';
$arguments767['additionalParams'] = array (
);
$arguments767['addQueryString'] = false;
$arguments767['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments767['useParentRequest'] = false;
$arguments767['absolute'] = true;
$arguments767['dir'] = NULL;
$arguments767['id'] = NULL;
$arguments767['lang'] = NULL;
$arguments767['style'] = NULL;
$arguments767['accesskey'] = NULL;
$arguments767['tabindex'] = NULL;
$arguments767['onclick'] = NULL;
$arguments767['name'] = NULL;
$arguments767['rel'] = NULL;
$arguments767['rev'] = NULL;
$arguments767['target'] = NULL;
$renderChildrenClosure773 = function() use ($renderingContext, $self) {
return '
															<i class="icon-asterisk icon-white"></i>
														';
};
$viewHelper774 = $self->getViewHelper('$viewHelper774', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper774->setArguments($arguments767);
$viewHelper774->setRenderingContext($renderingContext);
$viewHelper774->setRenderChildrenClosure($renderChildrenClosure773);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output766 .= $viewHelper774->initializeArgumentsAndRender();

$output766 .= '
													';
return $output766;
};
$arguments739['__elseClosure'] = function() use ($renderingContext, $self) {
$output775 = '';

$output775 .= '
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments776 = array();
$arguments776['action'] = 'freeze';
$arguments776['class'] = 'neos-button neos-button-freeze';
// Rendering Array
$array777 = array();
$array777['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments776['arguments'] = $array777;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments778 = array();
$arguments778['id'] = 'packages.tooltip.freeze';
$arguments778['source'] = 'Modules';
$arguments778['package'] = 'TYPO3.Neos';
$arguments778['value'] = NULL;
$arguments778['arguments'] = array (
);
$arguments778['quantity'] = NULL;
$arguments778['languageIdentifier'] = NULL;
$renderChildrenClosure779 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper780 = $self->getViewHelper('$viewHelper780', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper780->setArguments($arguments778);
$viewHelper780->setRenderingContext($renderingContext);
$viewHelper780->setRenderChildrenClosure($renderChildrenClosure779);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments776['title'] = $viewHelper780->initializeArgumentsAndRender();
// Rendering Array
$array781 = array();
$array781['data-neos-toggle'] = 'tooltip';
$arguments776['additionalAttributes'] = $array781;
$arguments776['data'] = NULL;
$arguments776['controller'] = NULL;
$arguments776['package'] = NULL;
$arguments776['subpackage'] = NULL;
$arguments776['section'] = '';
$arguments776['format'] = '';
$arguments776['additionalParams'] = array (
);
$arguments776['addQueryString'] = false;
$arguments776['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments776['useParentRequest'] = false;
$arguments776['absolute'] = true;
$arguments776['dir'] = NULL;
$arguments776['id'] = NULL;
$arguments776['lang'] = NULL;
$arguments776['style'] = NULL;
$arguments776['accesskey'] = NULL;
$arguments776['tabindex'] = NULL;
$arguments776['onclick'] = NULL;
$arguments776['name'] = NULL;
$arguments776['rel'] = NULL;
$arguments776['rev'] = NULL;
$arguments776['target'] = NULL;
$renderChildrenClosure782 = function() use ($renderingContext, $self) {
return '
															<i class="icon-asterisk icon-white"></i>
														';
};
$viewHelper783 = $self->getViewHelper('$viewHelper783', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper783->setArguments($arguments776);
$viewHelper783->setRenderingContext($renderingContext);
$viewHelper783->setRenderChildrenClosure($renderChildrenClosure782);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output775 .= $viewHelper783->initializeArgumentsAndRender();

$output775 .= '
													';
return $output775;
};
$viewHelper784 = $self->getViewHelper('$viewHelper784', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper784->setArguments($arguments739);
$viewHelper784->setRenderingContext($renderingContext);
$viewHelper784->setRenderChildrenClosure($renderChildrenClosure740);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output738 .= $viewHelper784->initializeArgumentsAndRender();

$output738 .= '
											';
return $output738;
};
$viewHelper785 = $self->getViewHelper('$viewHelper785', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper785->setArguments($arguments736);
$viewHelper785->setRenderingContext($renderingContext);
$viewHelper785->setRenderChildrenClosure($renderChildrenClosure737);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output735 .= $viewHelper785->initializeArgumentsAndRender();

$output735 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments786 = array();
// Rendering Boolean node
$arguments786['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.isProtected', $renderingContext));
$arguments786['then'] = NULL;
$arguments786['else'] = NULL;
$renderChildrenClosure787 = function() use ($renderingContext, $self) {
$output788 = '';

$output788 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments789 = array();
$renderChildrenClosure790 = function() use ($renderingContext, $self) {
$output791 = '';

$output791 .= '
													<button class="neos-button neos-button-warning neos-disabled" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments792 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments793 = array();
$arguments793['id'] = 'packages.tooltip.noDeactivate';
$arguments793['source'] = 'Modules';
$arguments793['package'] = 'TYPO3.Neos';
$arguments793['value'] = NULL;
$arguments793['arguments'] = array (
);
$arguments793['quantity'] = NULL;
$arguments793['languageIdentifier'] = NULL;
$renderChildrenClosure794 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper795 = $self->getViewHelper('$viewHelper795', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper795->setArguments($arguments793);
$viewHelper795->setRenderingContext($renderingContext);
$viewHelper795->setRenderChildrenClosure($renderChildrenClosure794);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments792['value'] = $viewHelper795->initializeArgumentsAndRender();
$arguments792['keepQuotes'] = false;
$arguments792['encoding'] = 'UTF-8';
$arguments792['doubleEncode'] = true;
$renderChildrenClosure796 = function() use ($renderingContext, $self) {
return NULL;
};
$value797 = ($arguments792['value'] !== NULL ? $arguments792['value'] : $renderChildrenClosure796());

$output791 .= !is_string($value797) && !(is_object($value797) && method_exists($value797, '__toString')) ? $value797 : htmlspecialchars($value797, ($arguments792['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments792['encoding'], $arguments792['doubleEncode']);

$output791 .= '" disabled="disabled" data-neos-toggle="tooltip">
														<i class="icon-pause icon-white"></i>
													</button>
												';
return $output791;
};
$viewHelper798 = $self->getViewHelper('$viewHelper798', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper798->setArguments($arguments789);
$viewHelper798->setRenderingContext($renderingContext);
$viewHelper798->setRenderChildrenClosure($renderChildrenClosure790);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output788 .= $viewHelper798->initializeArgumentsAndRender();

$output788 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments799 = array();
$renderChildrenClosure800 = function() use ($renderingContext, $self) {
$output801 = '';

$output801 .= '
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments802 = array();
$arguments802['action'] = 'deactivate';
$arguments802['class'] = 'neos-button neos-button-warning';
// Rendering Array
$array803 = array();
$array803['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments802['arguments'] = $array803;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments804 = array();
$arguments804['id'] = 'packages.tooltip.deactivate';
$arguments804['source'] = 'Modules';
$arguments804['package'] = 'TYPO3.Neos';
$arguments804['value'] = NULL;
$arguments804['arguments'] = array (
);
$arguments804['quantity'] = NULL;
$arguments804['languageIdentifier'] = NULL;
$renderChildrenClosure805 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper806 = $self->getViewHelper('$viewHelper806', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper806->setArguments($arguments804);
$viewHelper806->setRenderingContext($renderingContext);
$viewHelper806->setRenderChildrenClosure($renderChildrenClosure805);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments802['title'] = $viewHelper806->initializeArgumentsAndRender();
// Rendering Array
$array807 = array();
$array807['data-neos-toggle'] = 'tooltip';
$arguments802['additionalAttributes'] = $array807;
$arguments802['data'] = NULL;
$arguments802['controller'] = NULL;
$arguments802['package'] = NULL;
$arguments802['subpackage'] = NULL;
$arguments802['section'] = '';
$arguments802['format'] = '';
$arguments802['additionalParams'] = array (
);
$arguments802['addQueryString'] = false;
$arguments802['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments802['useParentRequest'] = false;
$arguments802['absolute'] = true;
$arguments802['dir'] = NULL;
$arguments802['id'] = NULL;
$arguments802['lang'] = NULL;
$arguments802['style'] = NULL;
$arguments802['accesskey'] = NULL;
$arguments802['tabindex'] = NULL;
$arguments802['onclick'] = NULL;
$arguments802['name'] = NULL;
$arguments802['rel'] = NULL;
$arguments802['rev'] = NULL;
$arguments802['target'] = NULL;
$renderChildrenClosure808 = function() use ($renderingContext, $self) {
return '
														<i class="icon-pause icon-white"></i>
													';
};
$viewHelper809 = $self->getViewHelper('$viewHelper809', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper809->setArguments($arguments802);
$viewHelper809->setRenderingContext($renderingContext);
$viewHelper809->setRenderChildrenClosure($renderChildrenClosure808);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output801 .= $viewHelper809->initializeArgumentsAndRender();

$output801 .= '
												';
return $output801;
};
$viewHelper810 = $self->getViewHelper('$viewHelper810', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper810->setArguments($arguments799);
$viewHelper810->setRenderingContext($renderingContext);
$viewHelper810->setRenderChildrenClosure($renderChildrenClosure800);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output788 .= $viewHelper810->initializeArgumentsAndRender();

$output788 .= '
											';
return $output788;
};
$arguments786['__thenClosure'] = function() use ($renderingContext, $self) {
$output811 = '';

$output811 .= '
													<button class="neos-button neos-button-warning neos-disabled" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments812 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments813 = array();
$arguments813['id'] = 'packages.tooltip.noDeactivate';
$arguments813['source'] = 'Modules';
$arguments813['package'] = 'TYPO3.Neos';
$arguments813['value'] = NULL;
$arguments813['arguments'] = array (
);
$arguments813['quantity'] = NULL;
$arguments813['languageIdentifier'] = NULL;
$renderChildrenClosure814 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper815 = $self->getViewHelper('$viewHelper815', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper815->setArguments($arguments813);
$viewHelper815->setRenderingContext($renderingContext);
$viewHelper815->setRenderChildrenClosure($renderChildrenClosure814);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments812['value'] = $viewHelper815->initializeArgumentsAndRender();
$arguments812['keepQuotes'] = false;
$arguments812['encoding'] = 'UTF-8';
$arguments812['doubleEncode'] = true;
$renderChildrenClosure816 = function() use ($renderingContext, $self) {
return NULL;
};
$value817 = ($arguments812['value'] !== NULL ? $arguments812['value'] : $renderChildrenClosure816());

$output811 .= !is_string($value817) && !(is_object($value817) && method_exists($value817, '__toString')) ? $value817 : htmlspecialchars($value817, ($arguments812['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments812['encoding'], $arguments812['doubleEncode']);

$output811 .= '" disabled="disabled" data-neos-toggle="tooltip">
														<i class="icon-pause icon-white"></i>
													</button>
												';
return $output811;
};
$arguments786['__elseClosure'] = function() use ($renderingContext, $self) {
$output818 = '';

$output818 .= '
													';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments819 = array();
$arguments819['action'] = 'deactivate';
$arguments819['class'] = 'neos-button neos-button-warning';
// Rendering Array
$array820 = array();
$array820['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments819['arguments'] = $array820;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments821 = array();
$arguments821['id'] = 'packages.tooltip.deactivate';
$arguments821['source'] = 'Modules';
$arguments821['package'] = 'TYPO3.Neos';
$arguments821['value'] = NULL;
$arguments821['arguments'] = array (
);
$arguments821['quantity'] = NULL;
$arguments821['languageIdentifier'] = NULL;
$renderChildrenClosure822 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper823 = $self->getViewHelper('$viewHelper823', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper823->setArguments($arguments821);
$viewHelper823->setRenderingContext($renderingContext);
$viewHelper823->setRenderChildrenClosure($renderChildrenClosure822);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments819['title'] = $viewHelper823->initializeArgumentsAndRender();
// Rendering Array
$array824 = array();
$array824['data-neos-toggle'] = 'tooltip';
$arguments819['additionalAttributes'] = $array824;
$arguments819['data'] = NULL;
$arguments819['controller'] = NULL;
$arguments819['package'] = NULL;
$arguments819['subpackage'] = NULL;
$arguments819['section'] = '';
$arguments819['format'] = '';
$arguments819['additionalParams'] = array (
);
$arguments819['addQueryString'] = false;
$arguments819['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments819['useParentRequest'] = false;
$arguments819['absolute'] = true;
$arguments819['dir'] = NULL;
$arguments819['id'] = NULL;
$arguments819['lang'] = NULL;
$arguments819['style'] = NULL;
$arguments819['accesskey'] = NULL;
$arguments819['tabindex'] = NULL;
$arguments819['onclick'] = NULL;
$arguments819['name'] = NULL;
$arguments819['rel'] = NULL;
$arguments819['rev'] = NULL;
$arguments819['target'] = NULL;
$renderChildrenClosure825 = function() use ($renderingContext, $self) {
return '
														<i class="icon-pause icon-white"></i>
													';
};
$viewHelper826 = $self->getViewHelper('$viewHelper826', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper826->setArguments($arguments819);
$viewHelper826->setRenderingContext($renderingContext);
$viewHelper826->setRenderChildrenClosure($renderChildrenClosure825);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output818 .= $viewHelper826->initializeArgumentsAndRender();

$output818 .= '
												';
return $output818;
};
$viewHelper827 = $self->getViewHelper('$viewHelper827', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper827->setArguments($arguments786);
$viewHelper827->setRenderingContext($renderingContext);
$viewHelper827->setRenderChildrenClosure($renderChildrenClosure787);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output735 .= $viewHelper827->initializeArgumentsAndRender();

$output735 .= '
										';
return $output735;
};
$arguments624['__elseClosure'] = function() use ($renderingContext, $self) {
$output828 = '';

$output828 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments829 = array();
$arguments829['action'] = 'activate';
$arguments829['class'] = 'neos-button neos-button-success';
// Rendering Array
$array830 = array();
$array830['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments829['arguments'] = $array830;
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments831 = array();
$arguments831['id'] = 'packages.tooltip.activate';
$arguments831['source'] = 'Modules';
$arguments831['package'] = 'TYPO3.Neos';
$arguments831['value'] = NULL;
$arguments831['arguments'] = array (
);
$arguments831['quantity'] = NULL;
$arguments831['languageIdentifier'] = NULL;
$renderChildrenClosure832 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper833 = $self->getViewHelper('$viewHelper833', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper833->setArguments($arguments831);
$viewHelper833->setRenderingContext($renderingContext);
$viewHelper833->setRenderChildrenClosure($renderChildrenClosure832);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments829['title'] = $viewHelper833->initializeArgumentsAndRender();
// Rendering Array
$array834 = array();
$array834['data-neos-toggle'] = 'tooltip';
$arguments829['additionalAttributes'] = $array834;
$arguments829['data'] = NULL;
$arguments829['controller'] = NULL;
$arguments829['package'] = NULL;
$arguments829['subpackage'] = NULL;
$arguments829['section'] = '';
$arguments829['format'] = '';
$arguments829['additionalParams'] = array (
);
$arguments829['addQueryString'] = false;
$arguments829['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments829['useParentRequest'] = false;
$arguments829['absolute'] = true;
$arguments829['dir'] = NULL;
$arguments829['id'] = NULL;
$arguments829['lang'] = NULL;
$arguments829['style'] = NULL;
$arguments829['accesskey'] = NULL;
$arguments829['tabindex'] = NULL;
$arguments829['onclick'] = NULL;
$arguments829['name'] = NULL;
$arguments829['rel'] = NULL;
$arguments829['rev'] = NULL;
$arguments829['target'] = NULL;
$renderChildrenClosure835 = function() use ($renderingContext, $self) {
return '
												<i class="icon-play icon-white"></i>
											';
};
$viewHelper836 = $self->getViewHelper('$viewHelper836', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper836->setArguments($arguments829);
$viewHelper836->setRenderingContext($renderingContext);
$viewHelper836->setRenderChildrenClosure($renderChildrenClosure835);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output828 .= $viewHelper836->initializeArgumentsAndRender();

$output828 .= '
										';
return $output828;
};
$viewHelper837 = $self->getViewHelper('$viewHelper837', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper837->setArguments($arguments624);
$viewHelper837->setRenderingContext($renderingContext);
$viewHelper837->setRenderChildrenClosure($renderChildrenClosure625);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output556 .= $viewHelper837->initializeArgumentsAndRender();

$output556 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments838 = array();
// Rendering Boolean node
$arguments838['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.isProtected', $renderingContext));
$arguments838['then'] = NULL;
$arguments838['else'] = NULL;
$renderChildrenClosure839 = function() use ($renderingContext, $self) {
$output840 = '';

$output840 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments841 = array();
$renderChildrenClosure842 = function() use ($renderingContext, $self) {
$output843 = '';

$output843 .= '
											<button class="neos-button neos-button-danger neos-disabled" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments844 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments845 = array();
$arguments845['id'] = 'packages.tooltip.noDelete';
$arguments845['source'] = 'Modules';
$arguments845['package'] = 'TYPO3.Neos';
$arguments845['value'] = NULL;
$arguments845['arguments'] = array (
);
$arguments845['quantity'] = NULL;
$arguments845['languageIdentifier'] = NULL;
$renderChildrenClosure846 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper847 = $self->getViewHelper('$viewHelper847', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper847->setArguments($arguments845);
$viewHelper847->setRenderingContext($renderingContext);
$viewHelper847->setRenderChildrenClosure($renderChildrenClosure846);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments844['value'] = $viewHelper847->initializeArgumentsAndRender();
$arguments844['keepQuotes'] = false;
$arguments844['encoding'] = 'UTF-8';
$arguments844['doubleEncode'] = true;
$renderChildrenClosure848 = function() use ($renderingContext, $self) {
return NULL;
};
$value849 = ($arguments844['value'] !== NULL ? $arguments844['value'] : $renderChildrenClosure848());

$output843 .= !is_string($value849) && !(is_object($value849) && method_exists($value849, '__toString')) ? $value849 : htmlspecialchars($value849, ($arguments844['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments844['encoding'], $arguments844['doubleEncode']);

$output843 .= '" disabled="disabled" data-neos-toggle="tooltip"><i class="icon-trash icon-white"></i></button>
										';
return $output843;
};
$viewHelper850 = $self->getViewHelper('$viewHelper850', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper850->setArguments($arguments841);
$viewHelper850->setRenderingContext($renderingContext);
$viewHelper850->setRenderChildrenClosure($renderChildrenClosure842);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output840 .= $viewHelper850->initializeArgumentsAndRender();

$output840 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments851 = array();
$renderChildrenClosure852 = function() use ($renderingContext, $self) {
$output853 = '';

$output853 .= '
											<button class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments854 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments855 = array();
$arguments855['id'] = 'packages.tooltip.delete';
$arguments855['source'] = 'Modules';
$arguments855['package'] = 'TYPO3.Neos';
$arguments855['value'] = NULL;
$arguments855['arguments'] = array (
);
$arguments855['quantity'] = NULL;
$arguments855['languageIdentifier'] = NULL;
$renderChildrenClosure856 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper857 = $self->getViewHelper('$viewHelper857', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper857->setArguments($arguments855);
$viewHelper857->setRenderingContext($renderingContext);
$viewHelper857->setRenderChildrenClosure($renderChildrenClosure856);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments854['value'] = $viewHelper857->initializeArgumentsAndRender();
$arguments854['keepQuotes'] = false;
$arguments854['encoding'] = 'UTF-8';
$arguments854['doubleEncode'] = true;
$renderChildrenClosure858 = function() use ($renderingContext, $self) {
return NULL;
};
$value859 = ($arguments854['value'] !== NULL ? $arguments854['value'] : $renderChildrenClosure858());

$output853 .= !is_string($value859) && !(is_object($value859) && method_exists($value859, '__toString')) ? $value859 : htmlspecialchars($value859, ($arguments854['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments854['encoding'], $arguments854['doubleEncode']);

$output853 .= '" data-toggle="modal" href="#';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments860 = array();
$arguments860['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.sanitizedPackageKey', $renderingContext);
$arguments860['keepQuotes'] = false;
$arguments860['encoding'] = 'UTF-8';
$arguments860['doubleEncode'] = true;
$renderChildrenClosure861 = function() use ($renderingContext, $self) {
return NULL;
};
$value862 = ($arguments860['value'] !== NULL ? $arguments860['value'] : $renderChildrenClosure861());

$output853 .= !is_string($value862) && !(is_object($value862) && method_exists($value862, '__toString')) ? $value862 : htmlspecialchars($value862, ($arguments860['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments860['encoding'], $arguments860['doubleEncode']);

$output853 .= '" data-neos-toggle="tooltip"><i class="icon-trash icon-white"></i></button>
											<div class="neos-hide" id="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments863 = array();
$arguments863['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.sanitizedPackageKey', $renderingContext);
$arguments863['keepQuotes'] = false;
$arguments863['encoding'] = 'UTF-8';
$arguments863['doubleEncode'] = true;
$renderChildrenClosure864 = function() use ($renderingContext, $self) {
return NULL;
};
$value865 = ($arguments863['value'] !== NULL ? $arguments863['value'] : $renderChildrenClosure864());

$output853 .= !is_string($value865) && !(is_object($value865) && method_exists($value865, '__toString')) ? $value865 : htmlspecialchars($value865, ($arguments863['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments863['encoding'], $arguments863['doubleEncode']);

$output853 .= '">
												<div class="neos-modal-centered">
													<div class="neos-modal-content">
														<div class="neos-modal-header">
															<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
															<div class="neos-header">
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments866 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments867 = array();
$arguments867['id'] = 'packages.message.reallyDelete';
// Rendering Array
$array868 = array();
$array868['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments867['arguments'] = $array868;
$arguments867['source'] = 'Modules';
$arguments867['package'] = 'TYPO3.Neos';
$arguments867['value'] = NULL;
$arguments867['quantity'] = NULL;
$arguments867['languageIdentifier'] = NULL;
$renderChildrenClosure869 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper870 = $self->getViewHelper('$viewHelper870', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper870->setArguments($arguments867);
$viewHelper870->setRenderingContext($renderingContext);
$viewHelper870->setRenderChildrenClosure($renderChildrenClosure869);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments866['value'] = $viewHelper870->initializeArgumentsAndRender();
$arguments866['keepQuotes'] = false;
$arguments866['encoding'] = 'UTF-8';
$arguments866['doubleEncode'] = true;
$renderChildrenClosure871 = function() use ($renderingContext, $self) {
return NULL;
};
$value872 = ($arguments866['value'] !== NULL ? $arguments866['value'] : $renderChildrenClosure871());

$output853 .= !is_string($value872) && !(is_object($value872) && method_exists($value872, '__toString')) ? $value872 : htmlspecialchars($value872, ($arguments866['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments866['encoding'], $arguments866['doubleEncode']);

$output853 .= '
															</div>
															<div class="neos-subheader">
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments873 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments874 = array();
$arguments874['id'] = 'operationCannotBeUndone';
$arguments874['package'] = 'TYPO3.Neos';
$arguments874['value'] = NULL;
$arguments874['arguments'] = array (
);
$arguments874['source'] = 'Main';
$arguments874['quantity'] = NULL;
$arguments874['languageIdentifier'] = NULL;
$renderChildrenClosure875 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper876 = $self->getViewHelper('$viewHelper876', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper876->setArguments($arguments874);
$viewHelper876->setRenderingContext($renderingContext);
$viewHelper876->setRenderChildrenClosure($renderChildrenClosure875);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments873['value'] = $viewHelper876->initializeArgumentsAndRender();
$arguments873['keepQuotes'] = false;
$arguments873['encoding'] = 'UTF-8';
$arguments873['doubleEncode'] = true;
$renderChildrenClosure877 = function() use ($renderingContext, $self) {
return NULL;
};
$value878 = ($arguments873['value'] !== NULL ? $arguments873['value'] : $renderChildrenClosure877());

$output853 .= !is_string($value878) && !(is_object($value878) && method_exists($value878, '__toString')) ? $value878 : htmlspecialchars($value878, ($arguments873['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments873['encoding'], $arguments873['doubleEncode']);

$output853 .= '
															</div>
														</div>
														<div class="neos-modal-footer">
															<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments879 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments880 = array();
$arguments880['id'] = 'cancel';
$arguments880['package'] = 'TYPO3.Neos';
$arguments880['value'] = NULL;
$arguments880['arguments'] = array (
);
$arguments880['source'] = 'Main';
$arguments880['quantity'] = NULL;
$arguments880['languageIdentifier'] = NULL;
$renderChildrenClosure881 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper882 = $self->getViewHelper('$viewHelper882', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper882->setArguments($arguments880);
$viewHelper882->setRenderingContext($renderingContext);
$viewHelper882->setRenderChildrenClosure($renderChildrenClosure881);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments879['value'] = $viewHelper882->initializeArgumentsAndRender();
$arguments879['keepQuotes'] = false;
$arguments879['encoding'] = 'UTF-8';
$arguments879['doubleEncode'] = true;
$renderChildrenClosure883 = function() use ($renderingContext, $self) {
return NULL;
};
$value884 = ($arguments879['value'] !== NULL ? $arguments879['value'] : $renderChildrenClosure883());

$output853 .= !is_string($value884) && !(is_object($value884) && method_exists($value884, '__toString')) ? $value884 : htmlspecialchars($value884, ($arguments879['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments879['encoding'], $arguments879['doubleEncode']);

$output853 .= '</a>
															';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments885 = array();
$arguments885['action'] = 'delete';
$arguments885['class'] = 'neos-button neos-button-danger';
// Rendering Array
$array886 = array();
$array886['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments885['arguments'] = $array886;
$arguments885['title'] = 'Delete';
// Rendering Array
$array887 = array();
$array887['data-neos-toggle'] = 'tooltip';
$arguments885['additionalAttributes'] = $array887;
$arguments885['data'] = NULL;
$arguments885['controller'] = NULL;
$arguments885['package'] = NULL;
$arguments885['subpackage'] = NULL;
$arguments885['section'] = '';
$arguments885['format'] = '';
$arguments885['additionalParams'] = array (
);
$arguments885['addQueryString'] = false;
$arguments885['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments885['useParentRequest'] = false;
$arguments885['absolute'] = true;
$arguments885['dir'] = NULL;
$arguments885['id'] = NULL;
$arguments885['lang'] = NULL;
$arguments885['style'] = NULL;
$arguments885['accesskey'] = NULL;
$arguments885['tabindex'] = NULL;
$arguments885['onclick'] = NULL;
$arguments885['name'] = NULL;
$arguments885['rel'] = NULL;
$arguments885['rev'] = NULL;
$arguments885['target'] = NULL;
$renderChildrenClosure888 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments889 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments890 = array();
$arguments890['id'] = 'packages.message.confirmDelete';
$arguments890['source'] = 'Modules';
$arguments890['package'] = 'TYPO3.Neos';
$arguments890['value'] = NULL;
$arguments890['arguments'] = array (
);
$arguments890['quantity'] = NULL;
$arguments890['languageIdentifier'] = NULL;
$renderChildrenClosure891 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper892 = $self->getViewHelper('$viewHelper892', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper892->setArguments($arguments890);
$viewHelper892->setRenderingContext($renderingContext);
$viewHelper892->setRenderChildrenClosure($renderChildrenClosure891);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments889['value'] = $viewHelper892->initializeArgumentsAndRender();
$arguments889['keepQuotes'] = false;
$arguments889['encoding'] = 'UTF-8';
$arguments889['doubleEncode'] = true;
$renderChildrenClosure893 = function() use ($renderingContext, $self) {
return NULL;
};
$value894 = ($arguments889['value'] !== NULL ? $arguments889['value'] : $renderChildrenClosure893());
return !is_string($value894) && !(is_object($value894) && method_exists($value894, '__toString')) ? $value894 : htmlspecialchars($value894, ($arguments889['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments889['encoding'], $arguments889['doubleEncode']);
};
$viewHelper895 = $self->getViewHelper('$viewHelper895', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper895->setArguments($arguments885);
$viewHelper895->setRenderingContext($renderingContext);
$viewHelper895->setRenderChildrenClosure($renderChildrenClosure888);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output853 .= $viewHelper895->initializeArgumentsAndRender();

$output853 .= '
														</div>
													</div>
												</div>
												<div class="neos-modal-backdrop neos-in"></div>
											</div>
										';
return $output853;
};
$viewHelper896 = $self->getViewHelper('$viewHelper896', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper896->setArguments($arguments851);
$viewHelper896->setRenderingContext($renderingContext);
$viewHelper896->setRenderChildrenClosure($renderChildrenClosure852);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output840 .= $viewHelper896->initializeArgumentsAndRender();

$output840 .= '
									';
return $output840;
};
$arguments838['__thenClosure'] = function() use ($renderingContext, $self) {
$output897 = '';

$output897 .= '
											<button class="neos-button neos-button-danger neos-disabled" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments898 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments899 = array();
$arguments899['id'] = 'packages.tooltip.noDelete';
$arguments899['source'] = 'Modules';
$arguments899['package'] = 'TYPO3.Neos';
$arguments899['value'] = NULL;
$arguments899['arguments'] = array (
);
$arguments899['quantity'] = NULL;
$arguments899['languageIdentifier'] = NULL;
$renderChildrenClosure900 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper901 = $self->getViewHelper('$viewHelper901', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper901->setArguments($arguments899);
$viewHelper901->setRenderingContext($renderingContext);
$viewHelper901->setRenderChildrenClosure($renderChildrenClosure900);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments898['value'] = $viewHelper901->initializeArgumentsAndRender();
$arguments898['keepQuotes'] = false;
$arguments898['encoding'] = 'UTF-8';
$arguments898['doubleEncode'] = true;
$renderChildrenClosure902 = function() use ($renderingContext, $self) {
return NULL;
};
$value903 = ($arguments898['value'] !== NULL ? $arguments898['value'] : $renderChildrenClosure902());

$output897 .= !is_string($value903) && !(is_object($value903) && method_exists($value903, '__toString')) ? $value903 : htmlspecialchars($value903, ($arguments898['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments898['encoding'], $arguments898['doubleEncode']);

$output897 .= '" disabled="disabled" data-neos-toggle="tooltip"><i class="icon-trash icon-white"></i></button>
										';
return $output897;
};
$arguments838['__elseClosure'] = function() use ($renderingContext, $self) {
$output904 = '';

$output904 .= '
											<button class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments905 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments906 = array();
$arguments906['id'] = 'packages.tooltip.delete';
$arguments906['source'] = 'Modules';
$arguments906['package'] = 'TYPO3.Neos';
$arguments906['value'] = NULL;
$arguments906['arguments'] = array (
);
$arguments906['quantity'] = NULL;
$arguments906['languageIdentifier'] = NULL;
$renderChildrenClosure907 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper908 = $self->getViewHelper('$viewHelper908', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper908->setArguments($arguments906);
$viewHelper908->setRenderingContext($renderingContext);
$viewHelper908->setRenderChildrenClosure($renderChildrenClosure907);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments905['value'] = $viewHelper908->initializeArgumentsAndRender();
$arguments905['keepQuotes'] = false;
$arguments905['encoding'] = 'UTF-8';
$arguments905['doubleEncode'] = true;
$renderChildrenClosure909 = function() use ($renderingContext, $self) {
return NULL;
};
$value910 = ($arguments905['value'] !== NULL ? $arguments905['value'] : $renderChildrenClosure909());

$output904 .= !is_string($value910) && !(is_object($value910) && method_exists($value910, '__toString')) ? $value910 : htmlspecialchars($value910, ($arguments905['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments905['encoding'], $arguments905['doubleEncode']);

$output904 .= '" data-toggle="modal" href="#';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments911 = array();
$arguments911['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.sanitizedPackageKey', $renderingContext);
$arguments911['keepQuotes'] = false;
$arguments911['encoding'] = 'UTF-8';
$arguments911['doubleEncode'] = true;
$renderChildrenClosure912 = function() use ($renderingContext, $self) {
return NULL;
};
$value913 = ($arguments911['value'] !== NULL ? $arguments911['value'] : $renderChildrenClosure912());

$output904 .= !is_string($value913) && !(is_object($value913) && method_exists($value913, '__toString')) ? $value913 : htmlspecialchars($value913, ($arguments911['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments911['encoding'], $arguments911['doubleEncode']);

$output904 .= '" data-neos-toggle="tooltip"><i class="icon-trash icon-white"></i></button>
											<div class="neos-hide" id="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments914 = array();
$arguments914['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'package.sanitizedPackageKey', $renderingContext);
$arguments914['keepQuotes'] = false;
$arguments914['encoding'] = 'UTF-8';
$arguments914['doubleEncode'] = true;
$renderChildrenClosure915 = function() use ($renderingContext, $self) {
return NULL;
};
$value916 = ($arguments914['value'] !== NULL ? $arguments914['value'] : $renderChildrenClosure915());

$output904 .= !is_string($value916) && !(is_object($value916) && method_exists($value916, '__toString')) ? $value916 : htmlspecialchars($value916, ($arguments914['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments914['encoding'], $arguments914['doubleEncode']);

$output904 .= '">
												<div class="neos-modal-centered">
													<div class="neos-modal-content">
														<div class="neos-modal-header">
															<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
															<div class="neos-header">
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments917 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments918 = array();
$arguments918['id'] = 'packages.message.reallyDelete';
// Rendering Array
$array919 = array();
$array919['0'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments918['arguments'] = $array919;
$arguments918['source'] = 'Modules';
$arguments918['package'] = 'TYPO3.Neos';
$arguments918['value'] = NULL;
$arguments918['quantity'] = NULL;
$arguments918['languageIdentifier'] = NULL;
$renderChildrenClosure920 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper921 = $self->getViewHelper('$viewHelper921', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper921->setArguments($arguments918);
$viewHelper921->setRenderingContext($renderingContext);
$viewHelper921->setRenderChildrenClosure($renderChildrenClosure920);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments917['value'] = $viewHelper921->initializeArgumentsAndRender();
$arguments917['keepQuotes'] = false;
$arguments917['encoding'] = 'UTF-8';
$arguments917['doubleEncode'] = true;
$renderChildrenClosure922 = function() use ($renderingContext, $self) {
return NULL;
};
$value923 = ($arguments917['value'] !== NULL ? $arguments917['value'] : $renderChildrenClosure922());

$output904 .= !is_string($value923) && !(is_object($value923) && method_exists($value923, '__toString')) ? $value923 : htmlspecialchars($value923, ($arguments917['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments917['encoding'], $arguments917['doubleEncode']);

$output904 .= '
															</div>
															<div class="neos-subheader">
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments924 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments925 = array();
$arguments925['id'] = 'operationCannotBeUndone';
$arguments925['package'] = 'TYPO3.Neos';
$arguments925['value'] = NULL;
$arguments925['arguments'] = array (
);
$arguments925['source'] = 'Main';
$arguments925['quantity'] = NULL;
$arguments925['languageIdentifier'] = NULL;
$renderChildrenClosure926 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper927 = $self->getViewHelper('$viewHelper927', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper927->setArguments($arguments925);
$viewHelper927->setRenderingContext($renderingContext);
$viewHelper927->setRenderChildrenClosure($renderChildrenClosure926);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments924['value'] = $viewHelper927->initializeArgumentsAndRender();
$arguments924['keepQuotes'] = false;
$arguments924['encoding'] = 'UTF-8';
$arguments924['doubleEncode'] = true;
$renderChildrenClosure928 = function() use ($renderingContext, $self) {
return NULL;
};
$value929 = ($arguments924['value'] !== NULL ? $arguments924['value'] : $renderChildrenClosure928());

$output904 .= !is_string($value929) && !(is_object($value929) && method_exists($value929, '__toString')) ? $value929 : htmlspecialchars($value929, ($arguments924['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments924['encoding'], $arguments924['doubleEncode']);

$output904 .= '
															</div>
														</div>
														<div class="neos-modal-footer">
															<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments930 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments931 = array();
$arguments931['id'] = 'cancel';
$arguments931['package'] = 'TYPO3.Neos';
$arguments931['value'] = NULL;
$arguments931['arguments'] = array (
);
$arguments931['source'] = 'Main';
$arguments931['quantity'] = NULL;
$arguments931['languageIdentifier'] = NULL;
$renderChildrenClosure932 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper933 = $self->getViewHelper('$viewHelper933', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper933->setArguments($arguments931);
$viewHelper933->setRenderingContext($renderingContext);
$viewHelper933->setRenderChildrenClosure($renderChildrenClosure932);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments930['value'] = $viewHelper933->initializeArgumentsAndRender();
$arguments930['keepQuotes'] = false;
$arguments930['encoding'] = 'UTF-8';
$arguments930['doubleEncode'] = true;
$renderChildrenClosure934 = function() use ($renderingContext, $self) {
return NULL;
};
$value935 = ($arguments930['value'] !== NULL ? $arguments930['value'] : $renderChildrenClosure934());

$output904 .= !is_string($value935) && !(is_object($value935) && method_exists($value935, '__toString')) ? $value935 : htmlspecialchars($value935, ($arguments930['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments930['encoding'], $arguments930['doubleEncode']);

$output904 .= '</a>
															';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments936 = array();
$arguments936['action'] = 'delete';
$arguments936['class'] = 'neos-button neos-button-danger';
// Rendering Array
$array937 = array();
$array937['packageKey'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'packageKey', $renderingContext);
$arguments936['arguments'] = $array937;
$arguments936['title'] = 'Delete';
// Rendering Array
$array938 = array();
$array938['data-neos-toggle'] = 'tooltip';
$arguments936['additionalAttributes'] = $array938;
$arguments936['data'] = NULL;
$arguments936['controller'] = NULL;
$arguments936['package'] = NULL;
$arguments936['subpackage'] = NULL;
$arguments936['section'] = '';
$arguments936['format'] = '';
$arguments936['additionalParams'] = array (
);
$arguments936['addQueryString'] = false;
$arguments936['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments936['useParentRequest'] = false;
$arguments936['absolute'] = true;
$arguments936['dir'] = NULL;
$arguments936['id'] = NULL;
$arguments936['lang'] = NULL;
$arguments936['style'] = NULL;
$arguments936['accesskey'] = NULL;
$arguments936['tabindex'] = NULL;
$arguments936['onclick'] = NULL;
$arguments936['name'] = NULL;
$arguments936['rel'] = NULL;
$arguments936['rev'] = NULL;
$arguments936['target'] = NULL;
$renderChildrenClosure939 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments940 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments941 = array();
$arguments941['id'] = 'packages.message.confirmDelete';
$arguments941['source'] = 'Modules';
$arguments941['package'] = 'TYPO3.Neos';
$arguments941['value'] = NULL;
$arguments941['arguments'] = array (
);
$arguments941['quantity'] = NULL;
$arguments941['languageIdentifier'] = NULL;
$renderChildrenClosure942 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper943 = $self->getViewHelper('$viewHelper943', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper943->setArguments($arguments941);
$viewHelper943->setRenderingContext($renderingContext);
$viewHelper943->setRenderChildrenClosure($renderChildrenClosure942);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments940['value'] = $viewHelper943->initializeArgumentsAndRender();
$arguments940['keepQuotes'] = false;
$arguments940['encoding'] = 'UTF-8';
$arguments940['doubleEncode'] = true;
$renderChildrenClosure944 = function() use ($renderingContext, $self) {
return NULL;
};
$value945 = ($arguments940['value'] !== NULL ? $arguments940['value'] : $renderChildrenClosure944());
return !is_string($value945) && !(is_object($value945) && method_exists($value945, '__toString')) ? $value945 : htmlspecialchars($value945, ($arguments940['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments940['encoding'], $arguments940['doubleEncode']);
};
$viewHelper946 = $self->getViewHelper('$viewHelper946', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper946->setArguments($arguments936);
$viewHelper946->setRenderingContext($renderingContext);
$viewHelper946->setRenderChildrenClosure($renderChildrenClosure939);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output904 .= $viewHelper946->initializeArgumentsAndRender();

$output904 .= '
														</div>
													</div>
												</div>
												<div class="neos-modal-backdrop neos-in"></div>
											</div>
										';
return $output904;
};
$viewHelper947 = $self->getViewHelper('$viewHelper947', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper947->setArguments($arguments838);
$viewHelper947->setRenderingContext($renderingContext);
$viewHelper947->setRenderChildrenClosure($renderChildrenClosure839);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output556 .= $viewHelper947->initializeArgumentsAndRender();

$output556 .= '
								</td>
							</tr>
						';
return $output556;
};

$output544 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments554, $renderChildrenClosure555, $renderingContext);

$output544 .= '
					';
return $output544;
};

$output511 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments542, $renderChildrenClosure543, $renderingContext);

$output511 .= '
				</tbody>
			</table>
		</div>
		<div class="neos-footer">
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments948 = array();
// Rendering Boolean node
$arguments948['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'isDevelopmentContext', $renderingContext));
$arguments948['then'] = NULL;
$arguments948['else'] = NULL;
$renderChildrenClosure949 = function() use ($renderingContext, $self) {
$output950 = '';

$output950 .= '
				<button type="submit" name="moduleArguments[action]" value="freeze" class="neos-button batch-action neos-disabled" disabled="disabled">
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments951 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments952 = array();
$arguments952['id'] = 'packages.selected.freeze';
$arguments952['source'] = 'Modules';
$arguments952['package'] = 'TYPO3.Neos';
$arguments952['value'] = NULL;
$arguments952['arguments'] = array (
);
$arguments952['quantity'] = NULL;
$arguments952['languageIdentifier'] = NULL;
$renderChildrenClosure953 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper954 = $self->getViewHelper('$viewHelper954', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper954->setArguments($arguments952);
$viewHelper954->setRenderingContext($renderingContext);
$viewHelper954->setRenderChildrenClosure($renderChildrenClosure953);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments951['value'] = $viewHelper954->initializeArgumentsAndRender();
$arguments951['keepQuotes'] = false;
$arguments951['encoding'] = 'UTF-8';
$arguments951['doubleEncode'] = true;
$renderChildrenClosure955 = function() use ($renderingContext, $self) {
return NULL;
};
$value956 = ($arguments951['value'] !== NULL ? $arguments951['value'] : $renderChildrenClosure955());

$output950 .= !is_string($value956) && !(is_object($value956) && method_exists($value956, '__toString')) ? $value956 : htmlspecialchars($value956, ($arguments951['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments951['encoding'], $arguments951['doubleEncode']);

$output950 .= '
				</button>
				<button type="submit" name="moduleArguments[action]" value="unfreeze" class="neos-button batch-action neos-disabled" disabled="disabled">
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments957 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments958 = array();
$arguments958['id'] = 'packages.selected.unfreeze';
$arguments958['source'] = 'Modules';
$arguments958['package'] = 'TYPO3.Neos';
$arguments958['value'] = NULL;
$arguments958['arguments'] = array (
);
$arguments958['quantity'] = NULL;
$arguments958['languageIdentifier'] = NULL;
$renderChildrenClosure959 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper960 = $self->getViewHelper('$viewHelper960', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper960->setArguments($arguments958);
$viewHelper960->setRenderingContext($renderingContext);
$viewHelper960->setRenderChildrenClosure($renderChildrenClosure959);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments957['value'] = $viewHelper960->initializeArgumentsAndRender();
$arguments957['keepQuotes'] = false;
$arguments957['encoding'] = 'UTF-8';
$arguments957['doubleEncode'] = true;
$renderChildrenClosure961 = function() use ($renderingContext, $self) {
return NULL;
};
$value962 = ($arguments957['value'] !== NULL ? $arguments957['value'] : $renderChildrenClosure961());

$output950 .= !is_string($value962) && !(is_object($value962) && method_exists($value962, '__toString')) ? $value962 : htmlspecialchars($value962, ($arguments957['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments957['encoding'], $arguments957['doubleEncode']);

$output950 .= '
				</button>
			';
return $output950;
};
$viewHelper963 = $self->getViewHelper('$viewHelper963', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper963->setArguments($arguments948);
$viewHelper963->setRenderingContext($renderingContext);
$viewHelper963->setRenderChildrenClosure($renderChildrenClosure949);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output511 .= $viewHelper963->initializeArgumentsAndRender();

$output511 .= '
			<button class="neos-button neos-button-danger batch-action neos-disabled" data-toggle="modal" href="#delete" disabled="disabled">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments964 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments965 = array();
$arguments965['id'] = 'packages.selected.delete';
$arguments965['source'] = 'Modules';
$arguments965['package'] = 'TYPO3.Neos';
$arguments965['value'] = NULL;
$arguments965['arguments'] = array (
);
$arguments965['quantity'] = NULL;
$arguments965['languageIdentifier'] = NULL;
$renderChildrenClosure966 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper967 = $self->getViewHelper('$viewHelper967', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper967->setArguments($arguments965);
$viewHelper967->setRenderingContext($renderingContext);
$viewHelper967->setRenderChildrenClosure($renderChildrenClosure966);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments964['value'] = $viewHelper967->initializeArgumentsAndRender();
$arguments964['keepQuotes'] = false;
$arguments964['encoding'] = 'UTF-8';
$arguments964['doubleEncode'] = true;
$renderChildrenClosure968 = function() use ($renderingContext, $self) {
return NULL;
};
$value969 = ($arguments964['value'] !== NULL ? $arguments964['value'] : $renderChildrenClosure968());

$output511 .= !is_string($value969) && !(is_object($value969) && method_exists($value969, '__toString')) ? $value969 : htmlspecialchars($value969, ($arguments964['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments964['encoding'], $arguments964['doubleEncode']);

$output511 .= '
			</button>
			<button type="submit" name="moduleArguments[action]" value="deactivate" class="neos-button neos-button-warning batch-action neos-disabled" disabled="disabled">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments970 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments971 = array();
$arguments971['id'] = 'packages.selected.deactivate';
$arguments971['source'] = 'Modules';
$arguments971['package'] = 'TYPO3.Neos';
$arguments971['value'] = NULL;
$arguments971['arguments'] = array (
);
$arguments971['quantity'] = NULL;
$arguments971['languageIdentifier'] = NULL;
$renderChildrenClosure972 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper973 = $self->getViewHelper('$viewHelper973', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper973->setArguments($arguments971);
$viewHelper973->setRenderingContext($renderingContext);
$viewHelper973->setRenderChildrenClosure($renderChildrenClosure972);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments970['value'] = $viewHelper973->initializeArgumentsAndRender();
$arguments970['keepQuotes'] = false;
$arguments970['encoding'] = 'UTF-8';
$arguments970['doubleEncode'] = true;
$renderChildrenClosure974 = function() use ($renderingContext, $self) {
return NULL;
};
$value975 = ($arguments970['value'] !== NULL ? $arguments970['value'] : $renderChildrenClosure974());

$output511 .= !is_string($value975) && !(is_object($value975) && method_exists($value975, '__toString')) ? $value975 : htmlspecialchars($value975, ($arguments970['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments970['encoding'], $arguments970['doubleEncode']);

$output511 .= '
			</button>
			<button type="submit" name="moduleArguments[action]" value="activate" class="neos-button neos-button-success batch-action neos-disabled" disabled="disabled">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments976 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments977 = array();
$arguments977['id'] = 'packages.selected.activate';
$arguments977['source'] = 'Modules';
$arguments977['package'] = 'TYPO3.Neos';
$arguments977['value'] = NULL;
$arguments977['arguments'] = array (
);
$arguments977['quantity'] = NULL;
$arguments977['languageIdentifier'] = NULL;
$renderChildrenClosure978 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper979 = $self->getViewHelper('$viewHelper979', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper979->setArguments($arguments977);
$viewHelper979->setRenderingContext($renderingContext);
$viewHelper979->setRenderChildrenClosure($renderChildrenClosure978);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments976['value'] = $viewHelper979->initializeArgumentsAndRender();
$arguments976['keepQuotes'] = false;
$arguments976['encoding'] = 'UTF-8';
$arguments976['doubleEncode'] = true;
$renderChildrenClosure980 = function() use ($renderingContext, $self) {
return NULL;
};
$value981 = ($arguments976['value'] !== NULL ? $arguments976['value'] : $renderChildrenClosure980());

$output511 .= !is_string($value981) && !(is_object($value981) && method_exists($value981, '__toString')) ? $value981 : htmlspecialchars($value981, ($arguments976['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments976['encoding'], $arguments976['doubleEncode']);

$output511 .= '
			</button>
		</div>
		<div class="neos-hide" id="delete">
			<div class="neos-modal-centered">
				<div class="neos-modal-content">
					<div class="neos-modal-header">
						<button type="button" class="neos-close" data-dismiss="modal"></button>
						<div class="neos-header">
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments982 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments983 = array();
$arguments983['id'] = 'packages.message.selected.reallyDelete';
$arguments983['source'] = 'Modules';
$arguments983['package'] = 'TYPO3.Neos';
$arguments983['value'] = NULL;
$arguments983['arguments'] = array (
);
$arguments983['quantity'] = NULL;
$arguments983['languageIdentifier'] = NULL;
$renderChildrenClosure984 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper985 = $self->getViewHelper('$viewHelper985', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper985->setArguments($arguments983);
$viewHelper985->setRenderingContext($renderingContext);
$viewHelper985->setRenderChildrenClosure($renderChildrenClosure984);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments982['value'] = $viewHelper985->initializeArgumentsAndRender();
$arguments982['keepQuotes'] = false;
$arguments982['encoding'] = 'UTF-8';
$arguments982['doubleEncode'] = true;
$renderChildrenClosure986 = function() use ($renderingContext, $self) {
return NULL;
};
$value987 = ($arguments982['value'] !== NULL ? $arguments982['value'] : $renderChildrenClosure986());

$output511 .= !is_string($value987) && !(is_object($value987) && method_exists($value987, '__toString')) ? $value987 : htmlspecialchars($value987, ($arguments982['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments982['encoding'], $arguments982['doubleEncode']);

$output511 .= '
						</div>
						<div class="neos-subheader">
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments988 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments989 = array();
$arguments989['id'] = 'operationCannotBeUndone';
$arguments989['package'] = 'TYPO3.Neos';
$arguments989['value'] = NULL;
$arguments989['arguments'] = array (
);
$arguments989['source'] = 'Main';
$arguments989['quantity'] = NULL;
$arguments989['languageIdentifier'] = NULL;
$renderChildrenClosure990 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper991 = $self->getViewHelper('$viewHelper991', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper991->setArguments($arguments989);
$viewHelper991->setRenderingContext($renderingContext);
$viewHelper991->setRenderChildrenClosure($renderChildrenClosure990);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments988['value'] = $viewHelper991->initializeArgumentsAndRender();
$arguments988['keepQuotes'] = false;
$arguments988['encoding'] = 'UTF-8';
$arguments988['doubleEncode'] = true;
$renderChildrenClosure992 = function() use ($renderingContext, $self) {
return NULL;
};
$value993 = ($arguments988['value'] !== NULL ? $arguments988['value'] : $renderChildrenClosure992());

$output511 .= !is_string($value993) && !(is_object($value993) && method_exists($value993, '__toString')) ? $value993 : htmlspecialchars($value993, ($arguments988['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments988['encoding'], $arguments988['doubleEncode']);

$output511 .= '
						</div>
					</div>
					<div class="neos-modal-footer">
						<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments994 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments995 = array();
$arguments995['id'] = 'cancel';
$arguments995['package'] = 'TYPO3.Neos';
$arguments995['value'] = NULL;
$arguments995['arguments'] = array (
);
$arguments995['source'] = 'Main';
$arguments995['quantity'] = NULL;
$arguments995['languageIdentifier'] = NULL;
$renderChildrenClosure996 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper997 = $self->getViewHelper('$viewHelper997', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper997->setArguments($arguments995);
$viewHelper997->setRenderingContext($renderingContext);
$viewHelper997->setRenderChildrenClosure($renderChildrenClosure996);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments994['value'] = $viewHelper997->initializeArgumentsAndRender();
$arguments994['keepQuotes'] = false;
$arguments994['encoding'] = 'UTF-8';
$arguments994['doubleEncode'] = true;
$renderChildrenClosure998 = function() use ($renderingContext, $self) {
return NULL;
};
$value999 = ($arguments994['value'] !== NULL ? $arguments994['value'] : $renderChildrenClosure998());

$output511 .= !is_string($value999) && !(is_object($value999) && method_exists($value999, '__toString')) ? $value999 : htmlspecialchars($value999, ($arguments994['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments994['encoding'], $arguments994['doubleEncode']);

$output511 .= '</a>
						<button type="submit" name="moduleArguments[action]" value="delete" class="neos-button neos-button-danger">
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1000 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1001 = array();
$arguments1001['id'] = 'packages.message.confirmDeletes';
$arguments1001['source'] = 'Modules';
$arguments1001['package'] = 'TYPO3.Neos';
$arguments1001['value'] = NULL;
$arguments1001['arguments'] = array (
);
$arguments1001['quantity'] = NULL;
$arguments1001['languageIdentifier'] = NULL;
$renderChildrenClosure1002 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper1003 = $self->getViewHelper('$viewHelper1003', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper1003->setArguments($arguments1001);
$viewHelper1003->setRenderingContext($renderingContext);
$viewHelper1003->setRenderChildrenClosure($renderChildrenClosure1002);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1000['value'] = $viewHelper1003->initializeArgumentsAndRender();
$arguments1000['keepQuotes'] = false;
$arguments1000['encoding'] = 'UTF-8';
$arguments1000['doubleEncode'] = true;
$renderChildrenClosure1004 = function() use ($renderingContext, $self) {
return NULL;
};
$value1005 = ($arguments1000['value'] !== NULL ? $arguments1000['value'] : $renderChildrenClosure1004());

$output511 .= !is_string($value1005) && !(is_object($value1005) && method_exists($value1005, '__toString')) ? $value1005 : htmlspecialchars($value1005, ($arguments1000['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1000['encoding'], $arguments1000['doubleEncode']);

$output511 .= '
						</button>
					</div>
				</div>
			</div>
			<div class="neos-modal-backdrop neos-in"></div>
		</div>
	';
return $output511;
};
$viewHelper1006 = $self->getViewHelper('$viewHelper1006', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper1006->setArguments($arguments509);
$viewHelper1006->setRenderingContext($renderingContext);
$viewHelper1006->setRenderChildrenClosure($renderChildrenClosure510);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output508 .= $viewHelper1006->initializeArgumentsAndRender();

$output508 .= '

	<script>
		(function($) {
			$(\'#check-all\').change(function() {
				var value = false;
				if ($(this).is(\':checked\')) {
					value = true;
					$(\'.batch-action\').removeClass(\'neos-disabled\').removeAttr(\'disabled\');
				} else {
					$(\'.batch-action\').addClass(\'neos-disabled\').attr(\'disabled\', \'disabled\');
				}
				$(\'tbody input[type="checkbox"]\').prop(\'checked\', value);
			});
			$(\'tbody input[type="checkbox"]\').change(function() {
				if ($(\'tbody input[type="checkbox"]:checked\').length > 0) {
					$(\'.batch-action\').removeClass(\'neos-disabled\').removeAttr(\'disabled\')
				} else {
					$(\'.batch-action\').addClass(\'neos-disabled\').attr(\'disabled\', \'disabled\');
				}
			});
			$(\'.fold-toggle\').click(function() {
				$(this).toggleClass(\'icon-chevron-down icon-chevron-up\');
				$(\'tr.\' + $(this).data(\'toggle\')).toggle();
			});
		})(jQuery);
	</script>
';
return $output508;
};

$output499 .= '';

$output499 .= '
';

return $output499;
}


}
#0             296453    