<?php class FluidCache_TYPO3_Neos__Module_Administration_Sites_action_index_ef6a36fdb9de0995e99d799a4149d1b394a5da0c extends \TYPO3\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// TODO
	return new \TYPO3\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;

return 'BackendSubModule';
}
public function hasLayout() {
return TRUE;
}

/**
 * section content
 */
public function section_040f06fd774092478d450774f5ba30c5da78acc8(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output0 = '';

$output0 .= '
	<table class="neos-table">
		<thead>
			<tr>
				<th>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments2 = array();
$arguments2['id'] = 'name';
$arguments2['value'] = 'Name';
$arguments2['arguments'] = array (
);
$arguments2['source'] = 'Main';
$arguments2['package'] = NULL;
$arguments2['quantity'] = NULL;
$arguments2['languageIdentifier'] = NULL;
$renderChildrenClosure3 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper4 = $self->getViewHelper('$viewHelper4', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper4->setArguments($arguments2);
$viewHelper4->setRenderingContext($renderingContext);
$viewHelper4->setRenderChildrenClosure($renderChildrenClosure3);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1['value'] = $viewHelper4->initializeArgumentsAndRender();
$arguments1['keepQuotes'] = false;
$arguments1['encoding'] = 'UTF-8';
$arguments1['doubleEncode'] = true;
$renderChildrenClosure5 = function() use ($renderingContext, $self) {
return NULL;
};
$value6 = ($arguments1['value'] !== NULL ? $arguments1['value'] : $renderChildrenClosure5());

$output0 .= !is_string($value6) && !(is_object($value6) && method_exists($value6, '__toString')) ? $value6 : htmlspecialchars($value6, ($arguments1['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1['encoding'], $arguments1['doubleEncode']);

$output0 .= '</th>
				<th>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments7 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments8 = array();
$arguments8['id'] = 'sites.rootNodeName';
$arguments8['value'] = 'Rootnode name';
$arguments8['arguments'] = array (
);
$arguments8['source'] = 'Main';
$arguments8['package'] = NULL;
$arguments8['quantity'] = NULL;
$arguments8['languageIdentifier'] = NULL;
$renderChildrenClosure9 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper10 = $self->getViewHelper('$viewHelper10', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper10->setArguments($arguments8);
$viewHelper10->setRenderingContext($renderingContext);
$viewHelper10->setRenderChildrenClosure($renderChildrenClosure9);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments7['value'] = $viewHelper10->initializeArgumentsAndRender();
$arguments7['keepQuotes'] = false;
$arguments7['encoding'] = 'UTF-8';
$arguments7['doubleEncode'] = true;
$renderChildrenClosure11 = function() use ($renderingContext, $self) {
return NULL;
};
$value12 = ($arguments7['value'] !== NULL ? $arguments7['value'] : $renderChildrenClosure11());

$output0 .= !is_string($value12) && !(is_object($value12) && method_exists($value12, '__toString')) ? $value12 : htmlspecialchars($value12, ($arguments7['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments7['encoding'], $arguments7['doubleEncode']);

$output0 .= '</th>
				<th>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments13 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments14 = array();
$arguments14['id'] = 'sites.domains';
$arguments14['value'] = 'Domains';
$arguments14['arguments'] = array (
);
$arguments14['source'] = 'Main';
$arguments14['package'] = NULL;
$arguments14['quantity'] = NULL;
$arguments14['languageIdentifier'] = NULL;
$renderChildrenClosure15 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper16 = $self->getViewHelper('$viewHelper16', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper16->setArguments($arguments14);
$viewHelper16->setRenderingContext($renderingContext);
$viewHelper16->setRenderChildrenClosure($renderChildrenClosure15);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments13['value'] = $viewHelper16->initializeArgumentsAndRender();
$arguments13['keepQuotes'] = false;
$arguments13['encoding'] = 'UTF-8';
$arguments13['doubleEncode'] = true;
$renderChildrenClosure17 = function() use ($renderingContext, $self) {
return NULL;
};
$value18 = ($arguments13['value'] !== NULL ? $arguments13['value'] : $renderChildrenClosure17());

$output0 .= !is_string($value18) && !(is_object($value18) && method_exists($value18, '__toString')) ? $value18 : htmlspecialchars($value18, ($arguments13['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments13['encoding'], $arguments13['doubleEncode']);

$output0 .= '</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments19 = array();
$arguments19['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackagesAndSites', $renderingContext);
$arguments19['as'] = 'sitePackageAndSite';
$arguments19['key'] = 'sitePackageKey';
$arguments19['reverse'] = false;
$arguments19['iteration'] = NULL;
$renderChildrenClosure20 = function() use ($renderingContext, $self) {
$output21 = '';

$output21 .= '
				<tr class="neos-folder">
					<td class="neos-priority1" colspan="3">
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments22 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments23 = array();
$arguments23['id'] = 'package';
$arguments23['value'] = 'Package';
$arguments23['arguments'] = array (
);
$arguments23['source'] = 'Main';
$arguments23['package'] = NULL;
$arguments23['quantity'] = NULL;
$arguments23['languageIdentifier'] = NULL;
$renderChildrenClosure24 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper25 = $self->getViewHelper('$viewHelper25', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper25->setArguments($arguments23);
$viewHelper25->setRenderingContext($renderingContext);
$viewHelper25->setRenderChildrenClosure($renderChildrenClosure24);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments22['value'] = $viewHelper25->initializeArgumentsAndRender();
$arguments22['keepQuotes'] = false;
$arguments22['encoding'] = 'UTF-8';
$arguments22['doubleEncode'] = true;
$renderChildrenClosure26 = function() use ($renderingContext, $self) {
return NULL;
};
$value27 = ($arguments22['value'] !== NULL ? $arguments22['value'] : $renderChildrenClosure26());

$output21 .= !is_string($value27) && !(is_object($value27) && method_exists($value27, '__toString')) ? $value27 : htmlspecialchars($value27, ($arguments22['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments22['encoding'], $arguments22['doubleEncode']);

$output21 .= ':
						';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper
$arguments28 = array();
$arguments28['path'] = 'administration/packages';
$arguments28['section'] = 'Sites';
$arguments28['class'] = 'neos-label';
$arguments28['title'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.package.packageMetaData.description', $renderingContext);
// Rendering Array
$array29 = array();
$array29['data-neos-toggle'] = 'tooltip';
$arguments28['additionalAttributes'] = $array29;
$arguments28['data'] = NULL;
$arguments28['action'] = NULL;
$arguments28['arguments'] = array (
);
$arguments28['format'] = '';
$arguments28['additionalParams'] = array (
);
$arguments28['addQueryString'] = false;
$arguments28['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments28['dir'] = NULL;
$arguments28['id'] = NULL;
$arguments28['lang'] = NULL;
$arguments28['style'] = NULL;
$arguments28['accesskey'] = NULL;
$arguments28['tabindex'] = NULL;
$arguments28['onclick'] = NULL;
$arguments28['name'] = NULL;
$arguments28['rel'] = NULL;
$arguments28['rev'] = NULL;
$arguments28['target'] = NULL;
$renderChildrenClosure30 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments31 = array();
$arguments31['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.packageKey', $renderingContext);
$arguments31['keepQuotes'] = false;
$arguments31['encoding'] = 'UTF-8';
$arguments31['doubleEncode'] = true;
$renderChildrenClosure32 = function() use ($renderingContext, $self) {
return NULL;
};
$value33 = ($arguments31['value'] !== NULL ? $arguments31['value'] : $renderChildrenClosure32());
return !is_string($value33) && !(is_object($value33) && method_exists($value33, '__toString')) ? $value33 : htmlspecialchars($value33, ($arguments31['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments31['encoding'], $arguments31['doubleEncode']);
};
$viewHelper34 = $self->getViewHelper('$viewHelper34', $renderingContext, 'TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper');
$viewHelper34->setArguments($arguments28);
$viewHelper34->setRenderingContext($renderingContext);
$viewHelper34->setRenderChildrenClosure($renderChildrenClosure30);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper

$output21 .= $viewHelper34->initializeArgumentsAndRender();

$output21 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments35 = array();
// Rendering Boolean node
$arguments35['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.package', $renderingContext));
$arguments35['then'] = NULL;
$arguments35['else'] = NULL;
$renderChildrenClosure36 = function() use ($renderingContext, $self) {
$output37 = '';

$output37 .= '
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments38 = array();
$renderChildrenClosure39 = function() use ($renderingContext, $self) {
$output40 = '';

$output40 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments41 = array();
// Rendering Boolean node
$arguments41['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.packageIsActive', $renderingContext));
$arguments41['then'] = NULL;
$arguments41['else'] = NULL;
$renderChildrenClosure42 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments43 = array();
$renderChildrenClosure44 = function() use ($renderingContext, $self) {
$output45 = '';

$output45 .= '<span class="neos-badge neos-badge-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments46 = array();
// Rendering Boolean node
$arguments46['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.sites', $renderingContext));
$arguments46['then'] = 'important';
$arguments46['else'] = 'warning';
$renderChildrenClosure47 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper48 = $self->getViewHelper('$viewHelper48', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper48->setArguments($arguments46);
$viewHelper48->setRenderingContext($renderingContext);
$viewHelper48->setRenderChildrenClosure($renderChildrenClosure47);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output45 .= $viewHelper48->initializeArgumentsAndRender();

$output45 .= '">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments49 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments50 = array();
$arguments50['id'] = 'deactivated';
$arguments50['value'] = 'Deactivated';
$arguments50['arguments'] = array (
);
$arguments50['source'] = 'Main';
$arguments50['package'] = NULL;
$arguments50['quantity'] = NULL;
$arguments50['languageIdentifier'] = NULL;
$renderChildrenClosure51 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper52 = $self->getViewHelper('$viewHelper52', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper52->setArguments($arguments50);
$viewHelper52->setRenderingContext($renderingContext);
$viewHelper52->setRenderChildrenClosure($renderChildrenClosure51);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments49['value'] = $viewHelper52->initializeArgumentsAndRender();
$arguments49['keepQuotes'] = false;
$arguments49['encoding'] = 'UTF-8';
$arguments49['doubleEncode'] = true;
$renderChildrenClosure53 = function() use ($renderingContext, $self) {
return NULL;
};
$value54 = ($arguments49['value'] !== NULL ? $arguments49['value'] : $renderChildrenClosure53());

$output45 .= !is_string($value54) && !(is_object($value54) && method_exists($value54, '__toString')) ? $value54 : htmlspecialchars($value54, ($arguments49['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments49['encoding'], $arguments49['doubleEncode']);

$output45 .= '</span>';
return $output45;
};
$viewHelper55 = $self->getViewHelper('$viewHelper55', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper55->setArguments($arguments43);
$viewHelper55->setRenderingContext($renderingContext);
$viewHelper55->setRenderChildrenClosure($renderChildrenClosure44);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
return $viewHelper55->initializeArgumentsAndRender();
};
$arguments41['__elseClosure'] = function() use ($renderingContext, $self) {
$output56 = '';

$output56 .= '<span class="neos-badge neos-badge-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments57 = array();
// Rendering Boolean node
$arguments57['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.sites', $renderingContext));
$arguments57['then'] = 'important';
$arguments57['else'] = 'warning';
$renderChildrenClosure58 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper59 = $self->getViewHelper('$viewHelper59', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper59->setArguments($arguments57);
$viewHelper59->setRenderingContext($renderingContext);
$viewHelper59->setRenderChildrenClosure($renderChildrenClosure58);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output56 .= $viewHelper59->initializeArgumentsAndRender();

$output56 .= '">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments60 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments61 = array();
$arguments61['id'] = 'deactivated';
$arguments61['value'] = 'Deactivated';
$arguments61['arguments'] = array (
);
$arguments61['source'] = 'Main';
$arguments61['package'] = NULL;
$arguments61['quantity'] = NULL;
$arguments61['languageIdentifier'] = NULL;
$renderChildrenClosure62 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper63 = $self->getViewHelper('$viewHelper63', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper63->setArguments($arguments61);
$viewHelper63->setRenderingContext($renderingContext);
$viewHelper63->setRenderChildrenClosure($renderChildrenClosure62);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments60['value'] = $viewHelper63->initializeArgumentsAndRender();
$arguments60['keepQuotes'] = false;
$arguments60['encoding'] = 'UTF-8';
$arguments60['doubleEncode'] = true;
$renderChildrenClosure64 = function() use ($renderingContext, $self) {
return NULL;
};
$value65 = ($arguments60['value'] !== NULL ? $arguments60['value'] : $renderChildrenClosure64());

$output56 .= !is_string($value65) && !(is_object($value65) && method_exists($value65, '__toString')) ? $value65 : htmlspecialchars($value65, ($arguments60['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments60['encoding'], $arguments60['doubleEncode']);

$output56 .= '</span>';
return $output56;
};
$viewHelper66 = $self->getViewHelper('$viewHelper66', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper66->setArguments($arguments41);
$viewHelper66->setRenderingContext($renderingContext);
$viewHelper66->setRenderChildrenClosure($renderChildrenClosure42);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output40 .= $viewHelper66->initializeArgumentsAndRender();

$output40 .= '
							';
return $output40;
};
$viewHelper67 = $self->getViewHelper('$viewHelper67', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper67->setArguments($arguments38);
$viewHelper67->setRenderingContext($renderingContext);
$viewHelper67->setRenderChildrenClosure($renderChildrenClosure39);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output37 .= $viewHelper67->initializeArgumentsAndRender();

$output37 .= '
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments68 = array();
$renderChildrenClosure69 = function() use ($renderingContext, $self) {
$output70 = '';

$output70 .= '<span class="neos-badge neos-badge-important">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments71 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments72 = array();
$arguments72['id'] = 'unavailable';
$arguments72['value'] = 'Unavailable';
$arguments72['arguments'] = array (
);
$arguments72['source'] = 'Main';
$arguments72['package'] = NULL;
$arguments72['quantity'] = NULL;
$arguments72['languageIdentifier'] = NULL;
$renderChildrenClosure73 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper74 = $self->getViewHelper('$viewHelper74', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper74->setArguments($arguments72);
$viewHelper74->setRenderingContext($renderingContext);
$viewHelper74->setRenderChildrenClosure($renderChildrenClosure73);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments71['value'] = $viewHelper74->initializeArgumentsAndRender();
$arguments71['keepQuotes'] = false;
$arguments71['encoding'] = 'UTF-8';
$arguments71['doubleEncode'] = true;
$renderChildrenClosure75 = function() use ($renderingContext, $self) {
return NULL;
};
$value76 = ($arguments71['value'] !== NULL ? $arguments71['value'] : $renderChildrenClosure75());

$output70 .= !is_string($value76) && !(is_object($value76) && method_exists($value76, '__toString')) ? $value76 : htmlspecialchars($value76, ($arguments71['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments71['encoding'], $arguments71['doubleEncode']);

$output70 .= '</span>';
return $output70;
};
$viewHelper77 = $self->getViewHelper('$viewHelper77', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper77->setArguments($arguments68);
$viewHelper77->setRenderingContext($renderingContext);
$viewHelper77->setRenderChildrenClosure($renderChildrenClosure69);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output37 .= $viewHelper77->initializeArgumentsAndRender();

$output37 .= '
						';
return $output37;
};
$arguments35['__thenClosure'] = function() use ($renderingContext, $self) {
$output78 = '';

$output78 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments79 = array();
// Rendering Boolean node
$arguments79['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.packageIsActive', $renderingContext));
$arguments79['then'] = NULL;
$arguments79['else'] = NULL;
$renderChildrenClosure80 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments81 = array();
$renderChildrenClosure82 = function() use ($renderingContext, $self) {
$output83 = '';

$output83 .= '<span class="neos-badge neos-badge-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments84 = array();
// Rendering Boolean node
$arguments84['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.sites', $renderingContext));
$arguments84['then'] = 'important';
$arguments84['else'] = 'warning';
$renderChildrenClosure85 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper86 = $self->getViewHelper('$viewHelper86', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper86->setArguments($arguments84);
$viewHelper86->setRenderingContext($renderingContext);
$viewHelper86->setRenderChildrenClosure($renderChildrenClosure85);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output83 .= $viewHelper86->initializeArgumentsAndRender();

$output83 .= '">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments87 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments88 = array();
$arguments88['id'] = 'deactivated';
$arguments88['value'] = 'Deactivated';
$arguments88['arguments'] = array (
);
$arguments88['source'] = 'Main';
$arguments88['package'] = NULL;
$arguments88['quantity'] = NULL;
$arguments88['languageIdentifier'] = NULL;
$renderChildrenClosure89 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper90 = $self->getViewHelper('$viewHelper90', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper90->setArguments($arguments88);
$viewHelper90->setRenderingContext($renderingContext);
$viewHelper90->setRenderChildrenClosure($renderChildrenClosure89);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments87['value'] = $viewHelper90->initializeArgumentsAndRender();
$arguments87['keepQuotes'] = false;
$arguments87['encoding'] = 'UTF-8';
$arguments87['doubleEncode'] = true;
$renderChildrenClosure91 = function() use ($renderingContext, $self) {
return NULL;
};
$value92 = ($arguments87['value'] !== NULL ? $arguments87['value'] : $renderChildrenClosure91());

$output83 .= !is_string($value92) && !(is_object($value92) && method_exists($value92, '__toString')) ? $value92 : htmlspecialchars($value92, ($arguments87['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments87['encoding'], $arguments87['doubleEncode']);

$output83 .= '</span>';
return $output83;
};
$viewHelper93 = $self->getViewHelper('$viewHelper93', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper93->setArguments($arguments81);
$viewHelper93->setRenderingContext($renderingContext);
$viewHelper93->setRenderChildrenClosure($renderChildrenClosure82);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
return $viewHelper93->initializeArgumentsAndRender();
};
$arguments79['__elseClosure'] = function() use ($renderingContext, $self) {
$output94 = '';

$output94 .= '<span class="neos-badge neos-badge-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments95 = array();
// Rendering Boolean node
$arguments95['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.sites', $renderingContext));
$arguments95['then'] = 'important';
$arguments95['else'] = 'warning';
$renderChildrenClosure96 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper97 = $self->getViewHelper('$viewHelper97', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper97->setArguments($arguments95);
$viewHelper97->setRenderingContext($renderingContext);
$viewHelper97->setRenderChildrenClosure($renderChildrenClosure96);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output94 .= $viewHelper97->initializeArgumentsAndRender();

$output94 .= '">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments98 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments99 = array();
$arguments99['id'] = 'deactivated';
$arguments99['value'] = 'Deactivated';
$arguments99['arguments'] = array (
);
$arguments99['source'] = 'Main';
$arguments99['package'] = NULL;
$arguments99['quantity'] = NULL;
$arguments99['languageIdentifier'] = NULL;
$renderChildrenClosure100 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper101 = $self->getViewHelper('$viewHelper101', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper101->setArguments($arguments99);
$viewHelper101->setRenderingContext($renderingContext);
$viewHelper101->setRenderChildrenClosure($renderChildrenClosure100);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments98['value'] = $viewHelper101->initializeArgumentsAndRender();
$arguments98['keepQuotes'] = false;
$arguments98['encoding'] = 'UTF-8';
$arguments98['doubleEncode'] = true;
$renderChildrenClosure102 = function() use ($renderingContext, $self) {
return NULL;
};
$value103 = ($arguments98['value'] !== NULL ? $arguments98['value'] : $renderChildrenClosure102());

$output94 .= !is_string($value103) && !(is_object($value103) && method_exists($value103, '__toString')) ? $value103 : htmlspecialchars($value103, ($arguments98['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments98['encoding'], $arguments98['doubleEncode']);

$output94 .= '</span>';
return $output94;
};
$viewHelper104 = $self->getViewHelper('$viewHelper104', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper104->setArguments($arguments79);
$viewHelper104->setRenderingContext($renderingContext);
$viewHelper104->setRenderChildrenClosure($renderChildrenClosure80);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output78 .= $viewHelper104->initializeArgumentsAndRender();

$output78 .= '
							';
return $output78;
};
$arguments35['__elseClosure'] = function() use ($renderingContext, $self) {
$output105 = '';

$output105 .= '<span class="neos-badge neos-badge-important">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments106 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments107 = array();
$arguments107['id'] = 'unavailable';
$arguments107['value'] = 'Unavailable';
$arguments107['arguments'] = array (
);
$arguments107['source'] = 'Main';
$arguments107['package'] = NULL;
$arguments107['quantity'] = NULL;
$arguments107['languageIdentifier'] = NULL;
$renderChildrenClosure108 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper109 = $self->getViewHelper('$viewHelper109', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper109->setArguments($arguments107);
$viewHelper109->setRenderingContext($renderingContext);
$viewHelper109->setRenderChildrenClosure($renderChildrenClosure108);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments106['value'] = $viewHelper109->initializeArgumentsAndRender();
$arguments106['keepQuotes'] = false;
$arguments106['encoding'] = 'UTF-8';
$arguments106['doubleEncode'] = true;
$renderChildrenClosure110 = function() use ($renderingContext, $self) {
return NULL;
};
$value111 = ($arguments106['value'] !== NULL ? $arguments106['value'] : $renderChildrenClosure110());

$output105 .= !is_string($value111) && !(is_object($value111) && method_exists($value111, '__toString')) ? $value111 : htmlspecialchars($value111, ($arguments106['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments106['encoding'], $arguments106['doubleEncode']);

$output105 .= '</span>';
return $output105;
};
$viewHelper112 = $self->getViewHelper('$viewHelper112', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper112->setArguments($arguments35);
$viewHelper112->setRenderingContext($renderingContext);
$viewHelper112->setRenderChildrenClosure($renderChildrenClosure36);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output21 .= $viewHelper112->initializeArgumentsAndRender();

$output21 .= '
					</td>
					<td class="neos-priority1 neos-aRight">
						<i class="fold-toggle icon-chevron-up icon-white" data-toggle="fold-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments113 = array();
$arguments113['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageKey', $renderingContext);
$arguments113['keepQuotes'] = false;
$arguments113['encoding'] = 'UTF-8';
$arguments113['doubleEncode'] = true;
$renderChildrenClosure114 = function() use ($renderingContext, $self) {
return NULL;
};
$value115 = ($arguments113['value'] !== NULL ? $arguments113['value'] : $renderChildrenClosure114());

$output21 .= !is_string($value115) && !(is_object($value115) && method_exists($value115, '__toString')) ? $value115 : htmlspecialchars($value115, ($arguments113['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments113['encoding'], $arguments113['doubleEncode']);

$output21 .= '"></i>
					</td>
				</tr>
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments116 = array();
// Rendering Boolean node
$arguments116['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.sites', $renderingContext));
$arguments116['then'] = NULL;
$arguments116['else'] = NULL;
$renderChildrenClosure117 = function() use ($renderingContext, $self) {
$output118 = '';

$output118 .= '
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments119 = array();
$renderChildrenClosure120 = function() use ($renderingContext, $self) {
$output121 = '';

$output121 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments122 = array();
$arguments122['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.sites', $renderingContext);
$arguments122['as'] = 'site';
$arguments122['key'] = '';
$arguments122['reverse'] = false;
$arguments122['iteration'] = NULL;
$renderChildrenClosure123 = function() use ($renderingContext, $self) {
$output124 = '';

$output124 .= '
							<tr class="fold-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments125 = array();
$arguments125['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageKey', $renderingContext);
$arguments125['keepQuotes'] = false;
$arguments125['encoding'] = 'UTF-8';
$arguments125['doubleEncode'] = true;
$renderChildrenClosure126 = function() use ($renderingContext, $self) {
return NULL;
};
$value127 = ($arguments125['value'] !== NULL ? $arguments125['value'] : $renderChildrenClosure126());

$output124 .= !is_string($value127) && !(is_object($value127) && method_exists($value127, '__toString')) ? $value127 : htmlspecialchars($value127, ($arguments125['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments125['encoding'], $arguments125['doubleEncode']);

$output124 .= '">
								<td>
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments128 = array();
$arguments128['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments128['keepQuotes'] = false;
$arguments128['encoding'] = 'UTF-8';
$arguments128['doubleEncode'] = true;
$renderChildrenClosure129 = function() use ($renderingContext, $self) {
return NULL;
};
$value130 = ($arguments128['value'] !== NULL ? $arguments128['value'] : $renderChildrenClosure129());

$output124 .= !is_string($value130) && !(is_object($value130) && method_exists($value130, '__toString')) ? $value130 : htmlspecialchars($value130, ($arguments128['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments128['encoding'], $arguments128['doubleEncode']);

$output124 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments131 = array();
// Rendering Boolean node
$arguments131['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.online', $renderingContext));
$arguments131['then'] = NULL;
$arguments131['else'] = NULL;
$renderChildrenClosure132 = function() use ($renderingContext, $self) {
$output133 = '';

$output133 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments134 = array();
$renderChildrenClosure135 = function() use ($renderingContext, $self) {
$output136 = '';

$output136 .= '
											<span class="neos-badge neos-badge-warning">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments137 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments138 = array();
$arguments138['id'] = 'inactive';
$arguments138['value'] = 'Inactive';
$arguments138['arguments'] = array (
);
$arguments138['source'] = 'Main';
$arguments138['package'] = NULL;
$arguments138['quantity'] = NULL;
$arguments138['languageIdentifier'] = NULL;
$renderChildrenClosure139 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper140 = $self->getViewHelper('$viewHelper140', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper140->setArguments($arguments138);
$viewHelper140->setRenderingContext($renderingContext);
$viewHelper140->setRenderChildrenClosure($renderChildrenClosure139);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments137['value'] = $viewHelper140->initializeArgumentsAndRender();
$arguments137['keepQuotes'] = false;
$arguments137['encoding'] = 'UTF-8';
$arguments137['doubleEncode'] = true;
$renderChildrenClosure141 = function() use ($renderingContext, $self) {
return NULL;
};
$value142 = ($arguments137['value'] !== NULL ? $arguments137['value'] : $renderChildrenClosure141());

$output136 .= !is_string($value142) && !(is_object($value142) && method_exists($value142, '__toString')) ? $value142 : htmlspecialchars($value142, ($arguments137['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments137['encoding'], $arguments137['doubleEncode']);

$output136 .= '</span>
										';
return $output136;
};
$viewHelper143 = $self->getViewHelper('$viewHelper143', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper143->setArguments($arguments134);
$viewHelper143->setRenderingContext($renderingContext);
$viewHelper143->setRenderChildrenClosure($renderChildrenClosure135);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output133 .= $viewHelper143->initializeArgumentsAndRender();

$output133 .= '
									';
return $output133;
};
$arguments131['__elseClosure'] = function() use ($renderingContext, $self) {
$output144 = '';

$output144 .= '
											<span class="neos-badge neos-badge-warning">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments145 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments146 = array();
$arguments146['id'] = 'inactive';
$arguments146['value'] = 'Inactive';
$arguments146['arguments'] = array (
);
$arguments146['source'] = 'Main';
$arguments146['package'] = NULL;
$arguments146['quantity'] = NULL;
$arguments146['languageIdentifier'] = NULL;
$renderChildrenClosure147 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper148 = $self->getViewHelper('$viewHelper148', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper148->setArguments($arguments146);
$viewHelper148->setRenderingContext($renderingContext);
$viewHelper148->setRenderChildrenClosure($renderChildrenClosure147);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments145['value'] = $viewHelper148->initializeArgumentsAndRender();
$arguments145['keepQuotes'] = false;
$arguments145['encoding'] = 'UTF-8';
$arguments145['doubleEncode'] = true;
$renderChildrenClosure149 = function() use ($renderingContext, $self) {
return NULL;
};
$value150 = ($arguments145['value'] !== NULL ? $arguments145['value'] : $renderChildrenClosure149());

$output144 .= !is_string($value150) && !(is_object($value150) && method_exists($value150, '__toString')) ? $value150 : htmlspecialchars($value150, ($arguments145['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments145['encoding'], $arguments145['doubleEncode']);

$output144 .= '</span>
										';
return $output144;
};
$viewHelper151 = $self->getViewHelper('$viewHelper151', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper151->setArguments($arguments131);
$viewHelper151->setRenderingContext($renderingContext);
$viewHelper151->setRenderChildrenClosure($renderChildrenClosure132);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output124 .= $viewHelper151->initializeArgumentsAndRender();

$output124 .= '
								</td>
								<td><span class="neos-label">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments152 = array();
$arguments152['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments152['keepQuotes'] = false;
$arguments152['encoding'] = 'UTF-8';
$arguments152['doubleEncode'] = true;
$renderChildrenClosure153 = function() use ($renderingContext, $self) {
return NULL;
};
$value154 = ($arguments152['value'] !== NULL ? $arguments152['value'] : $renderChildrenClosure153());

$output124 .= !is_string($value154) && !(is_object($value154) && method_exists($value154, '__toString')) ? $value154 : htmlspecialchars($value154, ($arguments152['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments152['encoding'], $arguments152['doubleEncode']);

$output124 .= '</span></td>
								<td><span class="neos-badge neos-badge-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments155 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments156 = array();
$arguments156['subject'] = NULL;
$renderChildrenClosure157 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.activeDomains', $renderingContext);
};
$viewHelper158 = $self->getViewHelper('$viewHelper158', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper158->setArguments($arguments156);
$viewHelper158->setRenderingContext($renderingContext);
$viewHelper158->setRenderChildrenClosure($renderChildrenClosure157);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments155['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', $viewHelper158->initializeArgumentsAndRender(), 0);
$arguments155['then'] = 'info';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments159 = array();
// Rendering Boolean node
$arguments159['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'multipleSites', $renderingContext));
$arguments159['then'] = 'important';
$arguments159['else'] = 'inverse';
$renderChildrenClosure160 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper161 = $self->getViewHelper('$viewHelper161', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper161->setArguments($arguments159);
$viewHelper161->setRenderingContext($renderingContext);
$viewHelper161->setRenderChildrenClosure($renderChildrenClosure160);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments155['else'] = $viewHelper161->initializeArgumentsAndRender();
$renderChildrenClosure162 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper163 = $self->getViewHelper('$viewHelper163', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper163->setArguments($arguments155);
$viewHelper163->setRenderingContext($renderingContext);
$viewHelper163->setRenderChildrenClosure($renderChildrenClosure162);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output124 .= $viewHelper163->initializeArgumentsAndRender();

$output124 .= '">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments164 = array();
$arguments164['subject'] = NULL;
$renderChildrenClosure165 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.domains', $renderingContext);
};
$viewHelper166 = $self->getViewHelper('$viewHelper166', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper166->setArguments($arguments164);
$viewHelper166->setRenderingContext($renderingContext);
$viewHelper166->setRenderChildrenClosure($renderChildrenClosure165);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper

$output124 .= $viewHelper166->initializeArgumentsAndRender();

$output124 .= '</span>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments167 = array();
// Rendering Boolean node
$arguments167['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.activeDomains', $renderingContext));
$arguments167['then'] = NULL;
$arguments167['else'] = NULL;
$renderChildrenClosure168 = function() use ($renderingContext, $self) {
$output169 = '';

$output169 .= ' <span class="neos-label neos-label-inverse">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments170 = array();
$arguments170['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.primaryDomain', $renderingContext);
$arguments170['keepQuotes'] = false;
$arguments170['encoding'] = 'UTF-8';
$arguments170['doubleEncode'] = true;
$renderChildrenClosure171 = function() use ($renderingContext, $self) {
return NULL;
};
$value172 = ($arguments170['value'] !== NULL ? $arguments170['value'] : $renderChildrenClosure171());

$output169 .= !is_string($value172) && !(is_object($value172) && method_exists($value172, '__toString')) ? $value172 : htmlspecialchars($value172, ($arguments170['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments170['encoding'], $arguments170['doubleEncode']);

$output169 .= '</span>';
return $output169;
};
$viewHelper173 = $self->getViewHelper('$viewHelper173', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper173->setArguments($arguments167);
$viewHelper173->setRenderingContext($renderingContext);
$viewHelper173->setRenderChildrenClosure($renderChildrenClosure168);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output124 .= $viewHelper173->initializeArgumentsAndRender();

$output124 .= '</td>
								<td class="neos-action">
									<div class="neos-pull-right">
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments174 = array();
$arguments174['action'] = 'edit';
// Rendering Array
$array175 = array();
$array175['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments174['arguments'] = $array175;
$arguments174['class'] = 'neos-button neos-button-primary';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments176 = array();
$arguments176['id'] = 'clickToEdit';
$arguments176['value'] = 'Click to edit';
$arguments176['arguments'] = array (
);
$arguments176['source'] = 'Main';
$arguments176['package'] = NULL;
$arguments176['quantity'] = NULL;
$arguments176['languageIdentifier'] = NULL;
$renderChildrenClosure177 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper178 = $self->getViewHelper('$viewHelper178', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper178->setArguments($arguments176);
$viewHelper178->setRenderingContext($renderingContext);
$viewHelper178->setRenderChildrenClosure($renderChildrenClosure177);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments174['title'] = $viewHelper178->initializeArgumentsAndRender();
// Rendering Array
$array179 = array();
$array179['data-neos-toggle'] = 'tooltip';
$arguments174['additionalAttributes'] = $array179;
$arguments174['data'] = NULL;
$arguments174['controller'] = NULL;
$arguments174['package'] = NULL;
$arguments174['subpackage'] = NULL;
$arguments174['section'] = '';
$arguments174['format'] = '';
$arguments174['additionalParams'] = array (
);
$arguments174['addQueryString'] = false;
$arguments174['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments174['useParentRequest'] = false;
$arguments174['absolute'] = true;
$arguments174['dir'] = NULL;
$arguments174['id'] = NULL;
$arguments174['lang'] = NULL;
$arguments174['style'] = NULL;
$arguments174['accesskey'] = NULL;
$arguments174['tabindex'] = NULL;
$arguments174['onclick'] = NULL;
$arguments174['name'] = NULL;
$arguments174['rel'] = NULL;
$arguments174['rev'] = NULL;
$arguments174['target'] = NULL;
$renderChildrenClosure180 = function() use ($renderingContext, $self) {
return '
											<i class="icon-pencil icon-white"></i>
										';
};
$viewHelper181 = $self->getViewHelper('$viewHelper181', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper181->setArguments($arguments174);
$viewHelper181->setRenderingContext($renderingContext);
$viewHelper181->setRenderChildrenClosure($renderChildrenClosure180);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output124 .= $viewHelper181->initializeArgumentsAndRender();

$output124 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments182 = array();
// Rendering Boolean node
$arguments182['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.state', $renderingContext), 1);
$arguments182['then'] = NULL;
$arguments182['else'] = NULL;
$renderChildrenClosure183 = function() use ($renderingContext, $self) {
$output184 = '';

$output184 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments185 = array();
$renderChildrenClosure186 = function() use ($renderingContext, $self) {
$output187 = '';

$output187 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments188 = array();
$arguments188['action'] = 'deactivateSite';
// Rendering Array
$array189 = array();
$array189['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments188['arguments'] = $array189;
$arguments188['class'] = 'neos-inline';
$arguments188['additionalAttributes'] = NULL;
$arguments188['data'] = NULL;
$arguments188['controller'] = NULL;
$arguments188['package'] = NULL;
$arguments188['subpackage'] = NULL;
$arguments188['object'] = NULL;
$arguments188['section'] = '';
$arguments188['format'] = '';
$arguments188['additionalParams'] = array (
);
$arguments188['absolute'] = false;
$arguments188['addQueryString'] = false;
$arguments188['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments188['fieldNamePrefix'] = NULL;
$arguments188['actionUri'] = NULL;
$arguments188['objectName'] = NULL;
$arguments188['useParentRequest'] = false;
$arguments188['enctype'] = NULL;
$arguments188['method'] = NULL;
$arguments188['name'] = NULL;
$arguments188['onreset'] = NULL;
$arguments188['onsubmit'] = NULL;
$arguments188['dir'] = NULL;
$arguments188['id'] = NULL;
$arguments188['lang'] = NULL;
$arguments188['style'] = NULL;
$arguments188['title'] = NULL;
$arguments188['accesskey'] = NULL;
$arguments188['tabindex'] = NULL;
$arguments188['onclick'] = NULL;
$renderChildrenClosure190 = function() use ($renderingContext, $self) {
$output191 = '';

$output191 .= '
													<button class="neos-button neos-button-warning" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments192 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments193 = array();
$arguments193['id'] = 'clickToDeactivate';
$arguments193['value'] = 'Click to deactivate';
$arguments193['arguments'] = array (
);
$arguments193['source'] = 'Main';
$arguments193['package'] = NULL;
$arguments193['quantity'] = NULL;
$arguments193['languageIdentifier'] = NULL;
$renderChildrenClosure194 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper195 = $self->getViewHelper('$viewHelper195', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper195->setArguments($arguments193);
$viewHelper195->setRenderingContext($renderingContext);
$viewHelper195->setRenderChildrenClosure($renderChildrenClosure194);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments192['value'] = $viewHelper195->initializeArgumentsAndRender();
$arguments192['keepQuotes'] = false;
$arguments192['encoding'] = 'UTF-8';
$arguments192['doubleEncode'] = true;
$renderChildrenClosure196 = function() use ($renderingContext, $self) {
return NULL;
};
$value197 = ($arguments192['value'] !== NULL ? $arguments192['value'] : $renderChildrenClosure196());

$output191 .= !is_string($value197) && !(is_object($value197) && method_exists($value197, '__toString')) ? $value197 : htmlspecialchars($value197, ($arguments192['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments192['encoding'], $arguments192['doubleEncode']);

$output191 .= '" data-neos-toggle="tooltip">
														<i class="icon-minus-sign icon-white"></i>
													</button>
												';
return $output191;
};
$viewHelper198 = $self->getViewHelper('$viewHelper198', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper198->setArguments($arguments188);
$viewHelper198->setRenderingContext($renderingContext);
$viewHelper198->setRenderChildrenClosure($renderChildrenClosure190);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output187 .= $viewHelper198->initializeArgumentsAndRender();

$output187 .= '
											';
return $output187;
};
$viewHelper199 = $self->getViewHelper('$viewHelper199', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper199->setArguments($arguments185);
$viewHelper199->setRenderingContext($renderingContext);
$viewHelper199->setRenderChildrenClosure($renderChildrenClosure186);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output184 .= $viewHelper199->initializeArgumentsAndRender();

$output184 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments200 = array();
$renderChildrenClosure201 = function() use ($renderingContext, $self) {
$output202 = '';

$output202 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments203 = array();
$arguments203['action'] = 'activateSite';
// Rendering Array
$array204 = array();
$array204['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments203['arguments'] = $array204;
$arguments203['class'] = 'neos-inline';
$arguments203['additionalAttributes'] = NULL;
$arguments203['data'] = NULL;
$arguments203['controller'] = NULL;
$arguments203['package'] = NULL;
$arguments203['subpackage'] = NULL;
$arguments203['object'] = NULL;
$arguments203['section'] = '';
$arguments203['format'] = '';
$arguments203['additionalParams'] = array (
);
$arguments203['absolute'] = false;
$arguments203['addQueryString'] = false;
$arguments203['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments203['fieldNamePrefix'] = NULL;
$arguments203['actionUri'] = NULL;
$arguments203['objectName'] = NULL;
$arguments203['useParentRequest'] = false;
$arguments203['enctype'] = NULL;
$arguments203['method'] = NULL;
$arguments203['name'] = NULL;
$arguments203['onreset'] = NULL;
$arguments203['onsubmit'] = NULL;
$arguments203['dir'] = NULL;
$arguments203['id'] = NULL;
$arguments203['lang'] = NULL;
$arguments203['style'] = NULL;
$arguments203['title'] = NULL;
$arguments203['accesskey'] = NULL;
$arguments203['tabindex'] = NULL;
$arguments203['onclick'] = NULL;
$renderChildrenClosure205 = function() use ($renderingContext, $self) {
$output206 = '';

$output206 .= '
													<button class="neos-button neos-button-success" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments207 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments208 = array();
$arguments208['id'] = 'clickToActivate';
$arguments208['value'] = 'Click to activate';
$arguments208['arguments'] = array (
);
$arguments208['source'] = 'Main';
$arguments208['package'] = NULL;
$arguments208['quantity'] = NULL;
$arguments208['languageIdentifier'] = NULL;
$renderChildrenClosure209 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper210 = $self->getViewHelper('$viewHelper210', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper210->setArguments($arguments208);
$viewHelper210->setRenderingContext($renderingContext);
$viewHelper210->setRenderChildrenClosure($renderChildrenClosure209);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments207['value'] = $viewHelper210->initializeArgumentsAndRender();
$arguments207['keepQuotes'] = false;
$arguments207['encoding'] = 'UTF-8';
$arguments207['doubleEncode'] = true;
$renderChildrenClosure211 = function() use ($renderingContext, $self) {
return NULL;
};
$value212 = ($arguments207['value'] !== NULL ? $arguments207['value'] : $renderChildrenClosure211());

$output206 .= !is_string($value212) && !(is_object($value212) && method_exists($value212, '__toString')) ? $value212 : htmlspecialchars($value212, ($arguments207['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments207['encoding'], $arguments207['doubleEncode']);

$output206 .= '" data-neos-toggle="tooltip">
														<i class="icon-plus-sign icon-white"></i>
													</button>
												';
return $output206;
};
$viewHelper213 = $self->getViewHelper('$viewHelper213', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper213->setArguments($arguments203);
$viewHelper213->setRenderingContext($renderingContext);
$viewHelper213->setRenderChildrenClosure($renderChildrenClosure205);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output202 .= $viewHelper213->initializeArgumentsAndRender();

$output202 .= '
											';
return $output202;
};
$viewHelper214 = $self->getViewHelper('$viewHelper214', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper214->setArguments($arguments200);
$viewHelper214->setRenderingContext($renderingContext);
$viewHelper214->setRenderChildrenClosure($renderChildrenClosure201);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output184 .= $viewHelper214->initializeArgumentsAndRender();

$output184 .= '
										';
return $output184;
};
$arguments182['__thenClosure'] = function() use ($renderingContext, $self) {
$output215 = '';

$output215 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments216 = array();
$arguments216['action'] = 'deactivateSite';
// Rendering Array
$array217 = array();
$array217['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments216['arguments'] = $array217;
$arguments216['class'] = 'neos-inline';
$arguments216['additionalAttributes'] = NULL;
$arguments216['data'] = NULL;
$arguments216['controller'] = NULL;
$arguments216['package'] = NULL;
$arguments216['subpackage'] = NULL;
$arguments216['object'] = NULL;
$arguments216['section'] = '';
$arguments216['format'] = '';
$arguments216['additionalParams'] = array (
);
$arguments216['absolute'] = false;
$arguments216['addQueryString'] = false;
$arguments216['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments216['fieldNamePrefix'] = NULL;
$arguments216['actionUri'] = NULL;
$arguments216['objectName'] = NULL;
$arguments216['useParentRequest'] = false;
$arguments216['enctype'] = NULL;
$arguments216['method'] = NULL;
$arguments216['name'] = NULL;
$arguments216['onreset'] = NULL;
$arguments216['onsubmit'] = NULL;
$arguments216['dir'] = NULL;
$arguments216['id'] = NULL;
$arguments216['lang'] = NULL;
$arguments216['style'] = NULL;
$arguments216['title'] = NULL;
$arguments216['accesskey'] = NULL;
$arguments216['tabindex'] = NULL;
$arguments216['onclick'] = NULL;
$renderChildrenClosure218 = function() use ($renderingContext, $self) {
$output219 = '';

$output219 .= '
													<button class="neos-button neos-button-warning" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments220 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments221 = array();
$arguments221['id'] = 'clickToDeactivate';
$arguments221['value'] = 'Click to deactivate';
$arguments221['arguments'] = array (
);
$arguments221['source'] = 'Main';
$arguments221['package'] = NULL;
$arguments221['quantity'] = NULL;
$arguments221['languageIdentifier'] = NULL;
$renderChildrenClosure222 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper223 = $self->getViewHelper('$viewHelper223', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper223->setArguments($arguments221);
$viewHelper223->setRenderingContext($renderingContext);
$viewHelper223->setRenderChildrenClosure($renderChildrenClosure222);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments220['value'] = $viewHelper223->initializeArgumentsAndRender();
$arguments220['keepQuotes'] = false;
$arguments220['encoding'] = 'UTF-8';
$arguments220['doubleEncode'] = true;
$renderChildrenClosure224 = function() use ($renderingContext, $self) {
return NULL;
};
$value225 = ($arguments220['value'] !== NULL ? $arguments220['value'] : $renderChildrenClosure224());

$output219 .= !is_string($value225) && !(is_object($value225) && method_exists($value225, '__toString')) ? $value225 : htmlspecialchars($value225, ($arguments220['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments220['encoding'], $arguments220['doubleEncode']);

$output219 .= '" data-neos-toggle="tooltip">
														<i class="icon-minus-sign icon-white"></i>
													</button>
												';
return $output219;
};
$viewHelper226 = $self->getViewHelper('$viewHelper226', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper226->setArguments($arguments216);
$viewHelper226->setRenderingContext($renderingContext);
$viewHelper226->setRenderChildrenClosure($renderChildrenClosure218);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output215 .= $viewHelper226->initializeArgumentsAndRender();

$output215 .= '
											';
return $output215;
};
$arguments182['__elseClosure'] = function() use ($renderingContext, $self) {
$output227 = '';

$output227 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments228 = array();
$arguments228['action'] = 'activateSite';
// Rendering Array
$array229 = array();
$array229['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments228['arguments'] = $array229;
$arguments228['class'] = 'neos-inline';
$arguments228['additionalAttributes'] = NULL;
$arguments228['data'] = NULL;
$arguments228['controller'] = NULL;
$arguments228['package'] = NULL;
$arguments228['subpackage'] = NULL;
$arguments228['object'] = NULL;
$arguments228['section'] = '';
$arguments228['format'] = '';
$arguments228['additionalParams'] = array (
);
$arguments228['absolute'] = false;
$arguments228['addQueryString'] = false;
$arguments228['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments228['fieldNamePrefix'] = NULL;
$arguments228['actionUri'] = NULL;
$arguments228['objectName'] = NULL;
$arguments228['useParentRequest'] = false;
$arguments228['enctype'] = NULL;
$arguments228['method'] = NULL;
$arguments228['name'] = NULL;
$arguments228['onreset'] = NULL;
$arguments228['onsubmit'] = NULL;
$arguments228['dir'] = NULL;
$arguments228['id'] = NULL;
$arguments228['lang'] = NULL;
$arguments228['style'] = NULL;
$arguments228['title'] = NULL;
$arguments228['accesskey'] = NULL;
$arguments228['tabindex'] = NULL;
$arguments228['onclick'] = NULL;
$renderChildrenClosure230 = function() use ($renderingContext, $self) {
$output231 = '';

$output231 .= '
													<button class="neos-button neos-button-success" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments232 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments233 = array();
$arguments233['id'] = 'clickToActivate';
$arguments233['value'] = 'Click to activate';
$arguments233['arguments'] = array (
);
$arguments233['source'] = 'Main';
$arguments233['package'] = NULL;
$arguments233['quantity'] = NULL;
$arguments233['languageIdentifier'] = NULL;
$renderChildrenClosure234 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper235 = $self->getViewHelper('$viewHelper235', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper235->setArguments($arguments233);
$viewHelper235->setRenderingContext($renderingContext);
$viewHelper235->setRenderChildrenClosure($renderChildrenClosure234);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments232['value'] = $viewHelper235->initializeArgumentsAndRender();
$arguments232['keepQuotes'] = false;
$arguments232['encoding'] = 'UTF-8';
$arguments232['doubleEncode'] = true;
$renderChildrenClosure236 = function() use ($renderingContext, $self) {
return NULL;
};
$value237 = ($arguments232['value'] !== NULL ? $arguments232['value'] : $renderChildrenClosure236());

$output231 .= !is_string($value237) && !(is_object($value237) && method_exists($value237, '__toString')) ? $value237 : htmlspecialchars($value237, ($arguments232['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments232['encoding'], $arguments232['doubleEncode']);

$output231 .= '" data-neos-toggle="tooltip">
														<i class="icon-plus-sign icon-white"></i>
													</button>
												';
return $output231;
};
$viewHelper238 = $self->getViewHelper('$viewHelper238', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper238->setArguments($arguments228);
$viewHelper238->setRenderingContext($renderingContext);
$viewHelper238->setRenderChildrenClosure($renderChildrenClosure230);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output227 .= $viewHelper238->initializeArgumentsAndRender();

$output227 .= '
											';
return $output227;
};
$viewHelper239 = $self->getViewHelper('$viewHelper239', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper239->setArguments($arguments182);
$viewHelper239->setRenderingContext($renderingContext);
$viewHelper239->setRenderChildrenClosure($renderChildrenClosure183);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output124 .= $viewHelper239->initializeArgumentsAndRender();

$output124 .= '
										<button class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments240 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments241 = array();
$arguments241['id'] = 'clickToDelete';
$arguments241['value'] = 'Click to delete';
$arguments241['arguments'] = array (
);
$arguments241['source'] = 'Main';
$arguments241['package'] = NULL;
$arguments241['quantity'] = NULL;
$arguments241['languageIdentifier'] = NULL;
$renderChildrenClosure242 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper243 = $self->getViewHelper('$viewHelper243', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper243->setArguments($arguments241);
$viewHelper243->setRenderingContext($renderingContext);
$viewHelper243->setRenderChildrenClosure($renderChildrenClosure242);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments240['value'] = $viewHelper243->initializeArgumentsAndRender();
$arguments240['keepQuotes'] = false;
$arguments240['encoding'] = 'UTF-8';
$arguments240['doubleEncode'] = true;
$renderChildrenClosure244 = function() use ($renderingContext, $self) {
return NULL;
};
$value245 = ($arguments240['value'] !== NULL ? $arguments240['value'] : $renderChildrenClosure244());

$output124 .= !is_string($value245) && !(is_object($value245) && method_exists($value245, '__toString')) ? $value245 : htmlspecialchars($value245, ($arguments240['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments240['encoding'], $arguments240['doubleEncode']);

$output124 .= '" data-toggle="modal" href="#';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments246 = array();
$arguments246['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments246['keepQuotes'] = false;
$arguments246['encoding'] = 'UTF-8';
$arguments246['doubleEncode'] = true;
$renderChildrenClosure247 = function() use ($renderingContext, $self) {
return NULL;
};
$value248 = ($arguments246['value'] !== NULL ? $arguments246['value'] : $renderChildrenClosure247());

$output124 .= !is_string($value248) && !(is_object($value248) && method_exists($value248, '__toString')) ? $value248 : htmlspecialchars($value248, ($arguments246['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments246['encoding'], $arguments246['doubleEncode']);

$output124 .= '" data-neos-toggle="tooltip">
											<i class="icon-trash icon-white"></i>
										</button>
										<div class="neos-hide" id="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments249 = array();
$arguments249['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments249['keepQuotes'] = false;
$arguments249['encoding'] = 'UTF-8';
$arguments249['doubleEncode'] = true;
$renderChildrenClosure250 = function() use ($renderingContext, $self) {
return NULL;
};
$value251 = ($arguments249['value'] !== NULL ? $arguments249['value'] : $renderChildrenClosure250());

$output124 .= !is_string($value251) && !(is_object($value251) && method_exists($value251, '__toString')) ? $value251 : htmlspecialchars($value251, ($arguments249['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments249['encoding'], $arguments249['doubleEncode']);

$output124 .= '">
											<div class="neos-modal-centered">
												<div class="neos-modal-content">
													<div class="neos-modal-header">
														<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
														<div class="neos-header">Do you really want to delete "';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments252 = array();
$arguments252['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments252['keepQuotes'] = false;
$arguments252['encoding'] = 'UTF-8';
$arguments252['doubleEncode'] = true;
$renderChildrenClosure253 = function() use ($renderingContext, $self) {
return NULL;
};
$value254 = ($arguments252['value'] !== NULL ? $arguments252['value'] : $renderChildrenClosure253());

$output124 .= !is_string($value254) && !(is_object($value254) && method_exists($value254, '__toString')) ? $value254 : htmlspecialchars($value254, ($arguments252['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments252['encoding'], $arguments252['doubleEncode']);

$output124 .= '"? This action cannot be undone.</div>
													</div>
													<div class="neos-modal-footer">
														<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments255 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments256 = array();
$arguments256['id'] = 'cancel';
$arguments256['value'] = 'Cancel';
$arguments256['arguments'] = array (
);
$arguments256['source'] = 'Main';
$arguments256['package'] = NULL;
$arguments256['quantity'] = NULL;
$arguments256['languageIdentifier'] = NULL;
$renderChildrenClosure257 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper258 = $self->getViewHelper('$viewHelper258', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper258->setArguments($arguments256);
$viewHelper258->setRenderingContext($renderingContext);
$viewHelper258->setRenderChildrenClosure($renderChildrenClosure257);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments255['value'] = $viewHelper258->initializeArgumentsAndRender();
$arguments255['keepQuotes'] = false;
$arguments255['encoding'] = 'UTF-8';
$arguments255['doubleEncode'] = true;
$renderChildrenClosure259 = function() use ($renderingContext, $self) {
return NULL;
};
$value260 = ($arguments255['value'] !== NULL ? $arguments255['value'] : $renderChildrenClosure259());

$output124 .= !is_string($value260) && !(is_object($value260) && method_exists($value260, '__toString')) ? $value260 : htmlspecialchars($value260, ($arguments255['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments255['encoding'], $arguments255['doubleEncode']);

$output124 .= '</a>
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments261 = array();
$arguments261['action'] = 'deleteSite';
// Rendering Array
$array262 = array();
$array262['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments261['arguments'] = $array262;
$arguments261['class'] = 'neos-inline';
$arguments261['additionalAttributes'] = NULL;
$arguments261['data'] = NULL;
$arguments261['controller'] = NULL;
$arguments261['package'] = NULL;
$arguments261['subpackage'] = NULL;
$arguments261['object'] = NULL;
$arguments261['section'] = '';
$arguments261['format'] = '';
$arguments261['additionalParams'] = array (
);
$arguments261['absolute'] = false;
$arguments261['addQueryString'] = false;
$arguments261['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments261['fieldNamePrefix'] = NULL;
$arguments261['actionUri'] = NULL;
$arguments261['objectName'] = NULL;
$arguments261['useParentRequest'] = false;
$arguments261['enctype'] = NULL;
$arguments261['method'] = NULL;
$arguments261['name'] = NULL;
$arguments261['onreset'] = NULL;
$arguments261['onsubmit'] = NULL;
$arguments261['dir'] = NULL;
$arguments261['id'] = NULL;
$arguments261['lang'] = NULL;
$arguments261['style'] = NULL;
$arguments261['title'] = NULL;
$arguments261['accesskey'] = NULL;
$arguments261['tabindex'] = NULL;
$arguments261['onclick'] = NULL;
$renderChildrenClosure263 = function() use ($renderingContext, $self) {
$output264 = '';

$output264 .= '
															<button class="neos-button neos-button-danger" title="Delete this site">
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments265 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments266 = array();
$arguments266['id'] = 'sites.confirmDelete';
$arguments266['value'] = 'Yes, delete this site';
$arguments266['arguments'] = array (
);
$arguments266['source'] = 'Main';
$arguments266['package'] = NULL;
$arguments266['quantity'] = NULL;
$arguments266['languageIdentifier'] = NULL;
$renderChildrenClosure267 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper268 = $self->getViewHelper('$viewHelper268', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper268->setArguments($arguments266);
$viewHelper268->setRenderingContext($renderingContext);
$viewHelper268->setRenderChildrenClosure($renderChildrenClosure267);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments265['value'] = $viewHelper268->initializeArgumentsAndRender();
$arguments265['keepQuotes'] = false;
$arguments265['encoding'] = 'UTF-8';
$arguments265['doubleEncode'] = true;
$renderChildrenClosure269 = function() use ($renderingContext, $self) {
return NULL;
};
$value270 = ($arguments265['value'] !== NULL ? $arguments265['value'] : $renderChildrenClosure269());

$output264 .= !is_string($value270) && !(is_object($value270) && method_exists($value270, '__toString')) ? $value270 : htmlspecialchars($value270, ($arguments265['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments265['encoding'], $arguments265['doubleEncode']);

$output264 .= '
															</button>
														';
return $output264;
};
$viewHelper271 = $self->getViewHelper('$viewHelper271', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper271->setArguments($arguments261);
$viewHelper271->setRenderingContext($renderingContext);
$viewHelper271->setRenderChildrenClosure($renderChildrenClosure263);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output124 .= $viewHelper271->initializeArgumentsAndRender();

$output124 .= '
													</div>
												</div>
											</div>
											<div class="neos-modal-backdrop neos-in"></div>
										</div>
									</div>
								</td>
							</tr>
						';
return $output124;
};

$output121 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments122, $renderChildrenClosure123, $renderingContext);

$output121 .= '
					';
return $output121;
};
$viewHelper272 = $self->getViewHelper('$viewHelper272', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper272->setArguments($arguments119);
$viewHelper272->setRenderingContext($renderingContext);
$viewHelper272->setRenderChildrenClosure($renderChildrenClosure120);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output118 .= $viewHelper272->initializeArgumentsAndRender();

$output118 .= '
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments273 = array();
$renderChildrenClosure274 = function() use ($renderingContext, $self) {
$output275 = '';

$output275 .= '
						<tr class="fold-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments276 = array();
$arguments276['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageKey', $renderingContext);
$arguments276['keepQuotes'] = false;
$arguments276['encoding'] = 'UTF-8';
$arguments276['doubleEncode'] = true;
$renderChildrenClosure277 = function() use ($renderingContext, $self) {
return NULL;
};
$value278 = ($arguments276['value'] !== NULL ? $arguments276['value'] : $renderChildrenClosure277());

$output275 .= !is_string($value278) && !(is_object($value278) && method_exists($value278, '__toString')) ? $value278 : htmlspecialchars($value278, ($arguments276['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments276['encoding'], $arguments276['doubleEncode']);

$output275 .= '">
							<td colspan="4"><i>No sites available</i></td>
						</tr>
					';
return $output275;
};
$viewHelper279 = $self->getViewHelper('$viewHelper279', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper279->setArguments($arguments273);
$viewHelper279->setRenderingContext($renderingContext);
$viewHelper279->setRenderChildrenClosure($renderChildrenClosure274);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output118 .= $viewHelper279->initializeArgumentsAndRender();

$output118 .= '
				';
return $output118;
};
$arguments116['__thenClosure'] = function() use ($renderingContext, $self) {
$output280 = '';

$output280 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments281 = array();
$arguments281['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.sites', $renderingContext);
$arguments281['as'] = 'site';
$arguments281['key'] = '';
$arguments281['reverse'] = false;
$arguments281['iteration'] = NULL;
$renderChildrenClosure282 = function() use ($renderingContext, $self) {
$output283 = '';

$output283 .= '
							<tr class="fold-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments284 = array();
$arguments284['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageKey', $renderingContext);
$arguments284['keepQuotes'] = false;
$arguments284['encoding'] = 'UTF-8';
$arguments284['doubleEncode'] = true;
$renderChildrenClosure285 = function() use ($renderingContext, $self) {
return NULL;
};
$value286 = ($arguments284['value'] !== NULL ? $arguments284['value'] : $renderChildrenClosure285());

$output283 .= !is_string($value286) && !(is_object($value286) && method_exists($value286, '__toString')) ? $value286 : htmlspecialchars($value286, ($arguments284['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments284['encoding'], $arguments284['doubleEncode']);

$output283 .= '">
								<td>
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments287 = array();
$arguments287['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments287['keepQuotes'] = false;
$arguments287['encoding'] = 'UTF-8';
$arguments287['doubleEncode'] = true;
$renderChildrenClosure288 = function() use ($renderingContext, $self) {
return NULL;
};
$value289 = ($arguments287['value'] !== NULL ? $arguments287['value'] : $renderChildrenClosure288());

$output283 .= !is_string($value289) && !(is_object($value289) && method_exists($value289, '__toString')) ? $value289 : htmlspecialchars($value289, ($arguments287['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments287['encoding'], $arguments287['doubleEncode']);

$output283 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments290 = array();
// Rendering Boolean node
$arguments290['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.online', $renderingContext));
$arguments290['then'] = NULL;
$arguments290['else'] = NULL;
$renderChildrenClosure291 = function() use ($renderingContext, $self) {
$output292 = '';

$output292 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments293 = array();
$renderChildrenClosure294 = function() use ($renderingContext, $self) {
$output295 = '';

$output295 .= '
											<span class="neos-badge neos-badge-warning">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments296 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments297 = array();
$arguments297['id'] = 'inactive';
$arguments297['value'] = 'Inactive';
$arguments297['arguments'] = array (
);
$arguments297['source'] = 'Main';
$arguments297['package'] = NULL;
$arguments297['quantity'] = NULL;
$arguments297['languageIdentifier'] = NULL;
$renderChildrenClosure298 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper299 = $self->getViewHelper('$viewHelper299', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper299->setArguments($arguments297);
$viewHelper299->setRenderingContext($renderingContext);
$viewHelper299->setRenderChildrenClosure($renderChildrenClosure298);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments296['value'] = $viewHelper299->initializeArgumentsAndRender();
$arguments296['keepQuotes'] = false;
$arguments296['encoding'] = 'UTF-8';
$arguments296['doubleEncode'] = true;
$renderChildrenClosure300 = function() use ($renderingContext, $self) {
return NULL;
};
$value301 = ($arguments296['value'] !== NULL ? $arguments296['value'] : $renderChildrenClosure300());

$output295 .= !is_string($value301) && !(is_object($value301) && method_exists($value301, '__toString')) ? $value301 : htmlspecialchars($value301, ($arguments296['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments296['encoding'], $arguments296['doubleEncode']);

$output295 .= '</span>
										';
return $output295;
};
$viewHelper302 = $self->getViewHelper('$viewHelper302', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper302->setArguments($arguments293);
$viewHelper302->setRenderingContext($renderingContext);
$viewHelper302->setRenderChildrenClosure($renderChildrenClosure294);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output292 .= $viewHelper302->initializeArgumentsAndRender();

$output292 .= '
									';
return $output292;
};
$arguments290['__elseClosure'] = function() use ($renderingContext, $self) {
$output303 = '';

$output303 .= '
											<span class="neos-badge neos-badge-warning">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments304 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments305 = array();
$arguments305['id'] = 'inactive';
$arguments305['value'] = 'Inactive';
$arguments305['arguments'] = array (
);
$arguments305['source'] = 'Main';
$arguments305['package'] = NULL;
$arguments305['quantity'] = NULL;
$arguments305['languageIdentifier'] = NULL;
$renderChildrenClosure306 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper307 = $self->getViewHelper('$viewHelper307', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper307->setArguments($arguments305);
$viewHelper307->setRenderingContext($renderingContext);
$viewHelper307->setRenderChildrenClosure($renderChildrenClosure306);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments304['value'] = $viewHelper307->initializeArgumentsAndRender();
$arguments304['keepQuotes'] = false;
$arguments304['encoding'] = 'UTF-8';
$arguments304['doubleEncode'] = true;
$renderChildrenClosure308 = function() use ($renderingContext, $self) {
return NULL;
};
$value309 = ($arguments304['value'] !== NULL ? $arguments304['value'] : $renderChildrenClosure308());

$output303 .= !is_string($value309) && !(is_object($value309) && method_exists($value309, '__toString')) ? $value309 : htmlspecialchars($value309, ($arguments304['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments304['encoding'], $arguments304['doubleEncode']);

$output303 .= '</span>
										';
return $output303;
};
$viewHelper310 = $self->getViewHelper('$viewHelper310', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper310->setArguments($arguments290);
$viewHelper310->setRenderingContext($renderingContext);
$viewHelper310->setRenderChildrenClosure($renderChildrenClosure291);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output283 .= $viewHelper310->initializeArgumentsAndRender();

$output283 .= '
								</td>
								<td><span class="neos-label">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments311 = array();
$arguments311['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments311['keepQuotes'] = false;
$arguments311['encoding'] = 'UTF-8';
$arguments311['doubleEncode'] = true;
$renderChildrenClosure312 = function() use ($renderingContext, $self) {
return NULL;
};
$value313 = ($arguments311['value'] !== NULL ? $arguments311['value'] : $renderChildrenClosure312());

$output283 .= !is_string($value313) && !(is_object($value313) && method_exists($value313, '__toString')) ? $value313 : htmlspecialchars($value313, ($arguments311['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments311['encoding'], $arguments311['doubleEncode']);

$output283 .= '</span></td>
								<td><span class="neos-badge neos-badge-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments314 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments315 = array();
$arguments315['subject'] = NULL;
$renderChildrenClosure316 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.activeDomains', $renderingContext);
};
$viewHelper317 = $self->getViewHelper('$viewHelper317', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper317->setArguments($arguments315);
$viewHelper317->setRenderingContext($renderingContext);
$viewHelper317->setRenderChildrenClosure($renderChildrenClosure316);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments314['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', $viewHelper317->initializeArgumentsAndRender(), 0);
$arguments314['then'] = 'info';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments318 = array();
// Rendering Boolean node
$arguments318['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'multipleSites', $renderingContext));
$arguments318['then'] = 'important';
$arguments318['else'] = 'inverse';
$renderChildrenClosure319 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper320 = $self->getViewHelper('$viewHelper320', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper320->setArguments($arguments318);
$viewHelper320->setRenderingContext($renderingContext);
$viewHelper320->setRenderChildrenClosure($renderChildrenClosure319);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments314['else'] = $viewHelper320->initializeArgumentsAndRender();
$renderChildrenClosure321 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper322 = $self->getViewHelper('$viewHelper322', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper322->setArguments($arguments314);
$viewHelper322->setRenderingContext($renderingContext);
$viewHelper322->setRenderChildrenClosure($renderChildrenClosure321);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output283 .= $viewHelper322->initializeArgumentsAndRender();

$output283 .= '">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments323 = array();
$arguments323['subject'] = NULL;
$renderChildrenClosure324 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.domains', $renderingContext);
};
$viewHelper325 = $self->getViewHelper('$viewHelper325', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper325->setArguments($arguments323);
$viewHelper325->setRenderingContext($renderingContext);
$viewHelper325->setRenderChildrenClosure($renderChildrenClosure324);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper

$output283 .= $viewHelper325->initializeArgumentsAndRender();

$output283 .= '</span>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments326 = array();
// Rendering Boolean node
$arguments326['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.activeDomains', $renderingContext));
$arguments326['then'] = NULL;
$arguments326['else'] = NULL;
$renderChildrenClosure327 = function() use ($renderingContext, $self) {
$output328 = '';

$output328 .= ' <span class="neos-label neos-label-inverse">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments329 = array();
$arguments329['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.primaryDomain', $renderingContext);
$arguments329['keepQuotes'] = false;
$arguments329['encoding'] = 'UTF-8';
$arguments329['doubleEncode'] = true;
$renderChildrenClosure330 = function() use ($renderingContext, $self) {
return NULL;
};
$value331 = ($arguments329['value'] !== NULL ? $arguments329['value'] : $renderChildrenClosure330());

$output328 .= !is_string($value331) && !(is_object($value331) && method_exists($value331, '__toString')) ? $value331 : htmlspecialchars($value331, ($arguments329['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments329['encoding'], $arguments329['doubleEncode']);

$output328 .= '</span>';
return $output328;
};
$viewHelper332 = $self->getViewHelper('$viewHelper332', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper332->setArguments($arguments326);
$viewHelper332->setRenderingContext($renderingContext);
$viewHelper332->setRenderChildrenClosure($renderChildrenClosure327);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output283 .= $viewHelper332->initializeArgumentsAndRender();

$output283 .= '</td>
								<td class="neos-action">
									<div class="neos-pull-right">
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments333 = array();
$arguments333['action'] = 'edit';
// Rendering Array
$array334 = array();
$array334['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments333['arguments'] = $array334;
$arguments333['class'] = 'neos-button neos-button-primary';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments335 = array();
$arguments335['id'] = 'clickToEdit';
$arguments335['value'] = 'Click to edit';
$arguments335['arguments'] = array (
);
$arguments335['source'] = 'Main';
$arguments335['package'] = NULL;
$arguments335['quantity'] = NULL;
$arguments335['languageIdentifier'] = NULL;
$renderChildrenClosure336 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper337 = $self->getViewHelper('$viewHelper337', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper337->setArguments($arguments335);
$viewHelper337->setRenderingContext($renderingContext);
$viewHelper337->setRenderChildrenClosure($renderChildrenClosure336);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments333['title'] = $viewHelper337->initializeArgumentsAndRender();
// Rendering Array
$array338 = array();
$array338['data-neos-toggle'] = 'tooltip';
$arguments333['additionalAttributes'] = $array338;
$arguments333['data'] = NULL;
$arguments333['controller'] = NULL;
$arguments333['package'] = NULL;
$arguments333['subpackage'] = NULL;
$arguments333['section'] = '';
$arguments333['format'] = '';
$arguments333['additionalParams'] = array (
);
$arguments333['addQueryString'] = false;
$arguments333['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments333['useParentRequest'] = false;
$arguments333['absolute'] = true;
$arguments333['dir'] = NULL;
$arguments333['id'] = NULL;
$arguments333['lang'] = NULL;
$arguments333['style'] = NULL;
$arguments333['accesskey'] = NULL;
$arguments333['tabindex'] = NULL;
$arguments333['onclick'] = NULL;
$arguments333['name'] = NULL;
$arguments333['rel'] = NULL;
$arguments333['rev'] = NULL;
$arguments333['target'] = NULL;
$renderChildrenClosure339 = function() use ($renderingContext, $self) {
return '
											<i class="icon-pencil icon-white"></i>
										';
};
$viewHelper340 = $self->getViewHelper('$viewHelper340', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper340->setArguments($arguments333);
$viewHelper340->setRenderingContext($renderingContext);
$viewHelper340->setRenderChildrenClosure($renderChildrenClosure339);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output283 .= $viewHelper340->initializeArgumentsAndRender();

$output283 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments341 = array();
// Rendering Boolean node
$arguments341['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.state', $renderingContext), 1);
$arguments341['then'] = NULL;
$arguments341['else'] = NULL;
$renderChildrenClosure342 = function() use ($renderingContext, $self) {
$output343 = '';

$output343 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments344 = array();
$renderChildrenClosure345 = function() use ($renderingContext, $self) {
$output346 = '';

$output346 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments347 = array();
$arguments347['action'] = 'deactivateSite';
// Rendering Array
$array348 = array();
$array348['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments347['arguments'] = $array348;
$arguments347['class'] = 'neos-inline';
$arguments347['additionalAttributes'] = NULL;
$arguments347['data'] = NULL;
$arguments347['controller'] = NULL;
$arguments347['package'] = NULL;
$arguments347['subpackage'] = NULL;
$arguments347['object'] = NULL;
$arguments347['section'] = '';
$arguments347['format'] = '';
$arguments347['additionalParams'] = array (
);
$arguments347['absolute'] = false;
$arguments347['addQueryString'] = false;
$arguments347['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments347['fieldNamePrefix'] = NULL;
$arguments347['actionUri'] = NULL;
$arguments347['objectName'] = NULL;
$arguments347['useParentRequest'] = false;
$arguments347['enctype'] = NULL;
$arguments347['method'] = NULL;
$arguments347['name'] = NULL;
$arguments347['onreset'] = NULL;
$arguments347['onsubmit'] = NULL;
$arguments347['dir'] = NULL;
$arguments347['id'] = NULL;
$arguments347['lang'] = NULL;
$arguments347['style'] = NULL;
$arguments347['title'] = NULL;
$arguments347['accesskey'] = NULL;
$arguments347['tabindex'] = NULL;
$arguments347['onclick'] = NULL;
$renderChildrenClosure349 = function() use ($renderingContext, $self) {
$output350 = '';

$output350 .= '
													<button class="neos-button neos-button-warning" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments351 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments352 = array();
$arguments352['id'] = 'clickToDeactivate';
$arguments352['value'] = 'Click to deactivate';
$arguments352['arguments'] = array (
);
$arguments352['source'] = 'Main';
$arguments352['package'] = NULL;
$arguments352['quantity'] = NULL;
$arguments352['languageIdentifier'] = NULL;
$renderChildrenClosure353 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper354 = $self->getViewHelper('$viewHelper354', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper354->setArguments($arguments352);
$viewHelper354->setRenderingContext($renderingContext);
$viewHelper354->setRenderChildrenClosure($renderChildrenClosure353);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments351['value'] = $viewHelper354->initializeArgumentsAndRender();
$arguments351['keepQuotes'] = false;
$arguments351['encoding'] = 'UTF-8';
$arguments351['doubleEncode'] = true;
$renderChildrenClosure355 = function() use ($renderingContext, $self) {
return NULL;
};
$value356 = ($arguments351['value'] !== NULL ? $arguments351['value'] : $renderChildrenClosure355());

$output350 .= !is_string($value356) && !(is_object($value356) && method_exists($value356, '__toString')) ? $value356 : htmlspecialchars($value356, ($arguments351['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments351['encoding'], $arguments351['doubleEncode']);

$output350 .= '" data-neos-toggle="tooltip">
														<i class="icon-minus-sign icon-white"></i>
													</button>
												';
return $output350;
};
$viewHelper357 = $self->getViewHelper('$viewHelper357', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper357->setArguments($arguments347);
$viewHelper357->setRenderingContext($renderingContext);
$viewHelper357->setRenderChildrenClosure($renderChildrenClosure349);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output346 .= $viewHelper357->initializeArgumentsAndRender();

$output346 .= '
											';
return $output346;
};
$viewHelper358 = $self->getViewHelper('$viewHelper358', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper358->setArguments($arguments344);
$viewHelper358->setRenderingContext($renderingContext);
$viewHelper358->setRenderChildrenClosure($renderChildrenClosure345);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output343 .= $viewHelper358->initializeArgumentsAndRender();

$output343 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments359 = array();
$renderChildrenClosure360 = function() use ($renderingContext, $self) {
$output361 = '';

$output361 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments362 = array();
$arguments362['action'] = 'activateSite';
// Rendering Array
$array363 = array();
$array363['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments362['arguments'] = $array363;
$arguments362['class'] = 'neos-inline';
$arguments362['additionalAttributes'] = NULL;
$arguments362['data'] = NULL;
$arguments362['controller'] = NULL;
$arguments362['package'] = NULL;
$arguments362['subpackage'] = NULL;
$arguments362['object'] = NULL;
$arguments362['section'] = '';
$arguments362['format'] = '';
$arguments362['additionalParams'] = array (
);
$arguments362['absolute'] = false;
$arguments362['addQueryString'] = false;
$arguments362['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments362['fieldNamePrefix'] = NULL;
$arguments362['actionUri'] = NULL;
$arguments362['objectName'] = NULL;
$arguments362['useParentRequest'] = false;
$arguments362['enctype'] = NULL;
$arguments362['method'] = NULL;
$arguments362['name'] = NULL;
$arguments362['onreset'] = NULL;
$arguments362['onsubmit'] = NULL;
$arguments362['dir'] = NULL;
$arguments362['id'] = NULL;
$arguments362['lang'] = NULL;
$arguments362['style'] = NULL;
$arguments362['title'] = NULL;
$arguments362['accesskey'] = NULL;
$arguments362['tabindex'] = NULL;
$arguments362['onclick'] = NULL;
$renderChildrenClosure364 = function() use ($renderingContext, $self) {
$output365 = '';

$output365 .= '
													<button class="neos-button neos-button-success" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments366 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments367 = array();
$arguments367['id'] = 'clickToActivate';
$arguments367['value'] = 'Click to activate';
$arguments367['arguments'] = array (
);
$arguments367['source'] = 'Main';
$arguments367['package'] = NULL;
$arguments367['quantity'] = NULL;
$arguments367['languageIdentifier'] = NULL;
$renderChildrenClosure368 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper369 = $self->getViewHelper('$viewHelper369', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper369->setArguments($arguments367);
$viewHelper369->setRenderingContext($renderingContext);
$viewHelper369->setRenderChildrenClosure($renderChildrenClosure368);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments366['value'] = $viewHelper369->initializeArgumentsAndRender();
$arguments366['keepQuotes'] = false;
$arguments366['encoding'] = 'UTF-8';
$arguments366['doubleEncode'] = true;
$renderChildrenClosure370 = function() use ($renderingContext, $self) {
return NULL;
};
$value371 = ($arguments366['value'] !== NULL ? $arguments366['value'] : $renderChildrenClosure370());

$output365 .= !is_string($value371) && !(is_object($value371) && method_exists($value371, '__toString')) ? $value371 : htmlspecialchars($value371, ($arguments366['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments366['encoding'], $arguments366['doubleEncode']);

$output365 .= '" data-neos-toggle="tooltip">
														<i class="icon-plus-sign icon-white"></i>
													</button>
												';
return $output365;
};
$viewHelper372 = $self->getViewHelper('$viewHelper372', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper372->setArguments($arguments362);
$viewHelper372->setRenderingContext($renderingContext);
$viewHelper372->setRenderChildrenClosure($renderChildrenClosure364);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output361 .= $viewHelper372->initializeArgumentsAndRender();

$output361 .= '
											';
return $output361;
};
$viewHelper373 = $self->getViewHelper('$viewHelper373', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper373->setArguments($arguments359);
$viewHelper373->setRenderingContext($renderingContext);
$viewHelper373->setRenderChildrenClosure($renderChildrenClosure360);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output343 .= $viewHelper373->initializeArgumentsAndRender();

$output343 .= '
										';
return $output343;
};
$arguments341['__thenClosure'] = function() use ($renderingContext, $self) {
$output374 = '';

$output374 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments375 = array();
$arguments375['action'] = 'deactivateSite';
// Rendering Array
$array376 = array();
$array376['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments375['arguments'] = $array376;
$arguments375['class'] = 'neos-inline';
$arguments375['additionalAttributes'] = NULL;
$arguments375['data'] = NULL;
$arguments375['controller'] = NULL;
$arguments375['package'] = NULL;
$arguments375['subpackage'] = NULL;
$arguments375['object'] = NULL;
$arguments375['section'] = '';
$arguments375['format'] = '';
$arguments375['additionalParams'] = array (
);
$arguments375['absolute'] = false;
$arguments375['addQueryString'] = false;
$arguments375['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments375['fieldNamePrefix'] = NULL;
$arguments375['actionUri'] = NULL;
$arguments375['objectName'] = NULL;
$arguments375['useParentRequest'] = false;
$arguments375['enctype'] = NULL;
$arguments375['method'] = NULL;
$arguments375['name'] = NULL;
$arguments375['onreset'] = NULL;
$arguments375['onsubmit'] = NULL;
$arguments375['dir'] = NULL;
$arguments375['id'] = NULL;
$arguments375['lang'] = NULL;
$arguments375['style'] = NULL;
$arguments375['title'] = NULL;
$arguments375['accesskey'] = NULL;
$arguments375['tabindex'] = NULL;
$arguments375['onclick'] = NULL;
$renderChildrenClosure377 = function() use ($renderingContext, $self) {
$output378 = '';

$output378 .= '
													<button class="neos-button neos-button-warning" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments379 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments380 = array();
$arguments380['id'] = 'clickToDeactivate';
$arguments380['value'] = 'Click to deactivate';
$arguments380['arguments'] = array (
);
$arguments380['source'] = 'Main';
$arguments380['package'] = NULL;
$arguments380['quantity'] = NULL;
$arguments380['languageIdentifier'] = NULL;
$renderChildrenClosure381 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper382 = $self->getViewHelper('$viewHelper382', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper382->setArguments($arguments380);
$viewHelper382->setRenderingContext($renderingContext);
$viewHelper382->setRenderChildrenClosure($renderChildrenClosure381);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments379['value'] = $viewHelper382->initializeArgumentsAndRender();
$arguments379['keepQuotes'] = false;
$arguments379['encoding'] = 'UTF-8';
$arguments379['doubleEncode'] = true;
$renderChildrenClosure383 = function() use ($renderingContext, $self) {
return NULL;
};
$value384 = ($arguments379['value'] !== NULL ? $arguments379['value'] : $renderChildrenClosure383());

$output378 .= !is_string($value384) && !(is_object($value384) && method_exists($value384, '__toString')) ? $value384 : htmlspecialchars($value384, ($arguments379['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments379['encoding'], $arguments379['doubleEncode']);

$output378 .= '" data-neos-toggle="tooltip">
														<i class="icon-minus-sign icon-white"></i>
													</button>
												';
return $output378;
};
$viewHelper385 = $self->getViewHelper('$viewHelper385', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper385->setArguments($arguments375);
$viewHelper385->setRenderingContext($renderingContext);
$viewHelper385->setRenderChildrenClosure($renderChildrenClosure377);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output374 .= $viewHelper385->initializeArgumentsAndRender();

$output374 .= '
											';
return $output374;
};
$arguments341['__elseClosure'] = function() use ($renderingContext, $self) {
$output386 = '';

$output386 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments387 = array();
$arguments387['action'] = 'activateSite';
// Rendering Array
$array388 = array();
$array388['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments387['arguments'] = $array388;
$arguments387['class'] = 'neos-inline';
$arguments387['additionalAttributes'] = NULL;
$arguments387['data'] = NULL;
$arguments387['controller'] = NULL;
$arguments387['package'] = NULL;
$arguments387['subpackage'] = NULL;
$arguments387['object'] = NULL;
$arguments387['section'] = '';
$arguments387['format'] = '';
$arguments387['additionalParams'] = array (
);
$arguments387['absolute'] = false;
$arguments387['addQueryString'] = false;
$arguments387['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments387['fieldNamePrefix'] = NULL;
$arguments387['actionUri'] = NULL;
$arguments387['objectName'] = NULL;
$arguments387['useParentRequest'] = false;
$arguments387['enctype'] = NULL;
$arguments387['method'] = NULL;
$arguments387['name'] = NULL;
$arguments387['onreset'] = NULL;
$arguments387['onsubmit'] = NULL;
$arguments387['dir'] = NULL;
$arguments387['id'] = NULL;
$arguments387['lang'] = NULL;
$arguments387['style'] = NULL;
$arguments387['title'] = NULL;
$arguments387['accesskey'] = NULL;
$arguments387['tabindex'] = NULL;
$arguments387['onclick'] = NULL;
$renderChildrenClosure389 = function() use ($renderingContext, $self) {
$output390 = '';

$output390 .= '
													<button class="neos-button neos-button-success" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments391 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments392 = array();
$arguments392['id'] = 'clickToActivate';
$arguments392['value'] = 'Click to activate';
$arguments392['arguments'] = array (
);
$arguments392['source'] = 'Main';
$arguments392['package'] = NULL;
$arguments392['quantity'] = NULL;
$arguments392['languageIdentifier'] = NULL;
$renderChildrenClosure393 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper394 = $self->getViewHelper('$viewHelper394', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper394->setArguments($arguments392);
$viewHelper394->setRenderingContext($renderingContext);
$viewHelper394->setRenderChildrenClosure($renderChildrenClosure393);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments391['value'] = $viewHelper394->initializeArgumentsAndRender();
$arguments391['keepQuotes'] = false;
$arguments391['encoding'] = 'UTF-8';
$arguments391['doubleEncode'] = true;
$renderChildrenClosure395 = function() use ($renderingContext, $self) {
return NULL;
};
$value396 = ($arguments391['value'] !== NULL ? $arguments391['value'] : $renderChildrenClosure395());

$output390 .= !is_string($value396) && !(is_object($value396) && method_exists($value396, '__toString')) ? $value396 : htmlspecialchars($value396, ($arguments391['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments391['encoding'], $arguments391['doubleEncode']);

$output390 .= '" data-neos-toggle="tooltip">
														<i class="icon-plus-sign icon-white"></i>
													</button>
												';
return $output390;
};
$viewHelper397 = $self->getViewHelper('$viewHelper397', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper397->setArguments($arguments387);
$viewHelper397->setRenderingContext($renderingContext);
$viewHelper397->setRenderChildrenClosure($renderChildrenClosure389);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output386 .= $viewHelper397->initializeArgumentsAndRender();

$output386 .= '
											';
return $output386;
};
$viewHelper398 = $self->getViewHelper('$viewHelper398', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper398->setArguments($arguments341);
$viewHelper398->setRenderingContext($renderingContext);
$viewHelper398->setRenderChildrenClosure($renderChildrenClosure342);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output283 .= $viewHelper398->initializeArgumentsAndRender();

$output283 .= '
										<button class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments399 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments400 = array();
$arguments400['id'] = 'clickToDelete';
$arguments400['value'] = 'Click to delete';
$arguments400['arguments'] = array (
);
$arguments400['source'] = 'Main';
$arguments400['package'] = NULL;
$arguments400['quantity'] = NULL;
$arguments400['languageIdentifier'] = NULL;
$renderChildrenClosure401 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper402 = $self->getViewHelper('$viewHelper402', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper402->setArguments($arguments400);
$viewHelper402->setRenderingContext($renderingContext);
$viewHelper402->setRenderChildrenClosure($renderChildrenClosure401);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments399['value'] = $viewHelper402->initializeArgumentsAndRender();
$arguments399['keepQuotes'] = false;
$arguments399['encoding'] = 'UTF-8';
$arguments399['doubleEncode'] = true;
$renderChildrenClosure403 = function() use ($renderingContext, $self) {
return NULL;
};
$value404 = ($arguments399['value'] !== NULL ? $arguments399['value'] : $renderChildrenClosure403());

$output283 .= !is_string($value404) && !(is_object($value404) && method_exists($value404, '__toString')) ? $value404 : htmlspecialchars($value404, ($arguments399['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments399['encoding'], $arguments399['doubleEncode']);

$output283 .= '" data-toggle="modal" href="#';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments405 = array();
$arguments405['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments405['keepQuotes'] = false;
$arguments405['encoding'] = 'UTF-8';
$arguments405['doubleEncode'] = true;
$renderChildrenClosure406 = function() use ($renderingContext, $self) {
return NULL;
};
$value407 = ($arguments405['value'] !== NULL ? $arguments405['value'] : $renderChildrenClosure406());

$output283 .= !is_string($value407) && !(is_object($value407) && method_exists($value407, '__toString')) ? $value407 : htmlspecialchars($value407, ($arguments405['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments405['encoding'], $arguments405['doubleEncode']);

$output283 .= '" data-neos-toggle="tooltip">
											<i class="icon-trash icon-white"></i>
										</button>
										<div class="neos-hide" id="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments408 = array();
$arguments408['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments408['keepQuotes'] = false;
$arguments408['encoding'] = 'UTF-8';
$arguments408['doubleEncode'] = true;
$renderChildrenClosure409 = function() use ($renderingContext, $self) {
return NULL;
};
$value410 = ($arguments408['value'] !== NULL ? $arguments408['value'] : $renderChildrenClosure409());

$output283 .= !is_string($value410) && !(is_object($value410) && method_exists($value410, '__toString')) ? $value410 : htmlspecialchars($value410, ($arguments408['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments408['encoding'], $arguments408['doubleEncode']);

$output283 .= '">
											<div class="neos-modal-centered">
												<div class="neos-modal-content">
													<div class="neos-modal-header">
														<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
														<div class="neos-header">Do you really want to delete "';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments411 = array();
$arguments411['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments411['keepQuotes'] = false;
$arguments411['encoding'] = 'UTF-8';
$arguments411['doubleEncode'] = true;
$renderChildrenClosure412 = function() use ($renderingContext, $self) {
return NULL;
};
$value413 = ($arguments411['value'] !== NULL ? $arguments411['value'] : $renderChildrenClosure412());

$output283 .= !is_string($value413) && !(is_object($value413) && method_exists($value413, '__toString')) ? $value413 : htmlspecialchars($value413, ($arguments411['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments411['encoding'], $arguments411['doubleEncode']);

$output283 .= '"? This action cannot be undone.</div>
													</div>
													<div class="neos-modal-footer">
														<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments414 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments415 = array();
$arguments415['id'] = 'cancel';
$arguments415['value'] = 'Cancel';
$arguments415['arguments'] = array (
);
$arguments415['source'] = 'Main';
$arguments415['package'] = NULL;
$arguments415['quantity'] = NULL;
$arguments415['languageIdentifier'] = NULL;
$renderChildrenClosure416 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper417 = $self->getViewHelper('$viewHelper417', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper417->setArguments($arguments415);
$viewHelper417->setRenderingContext($renderingContext);
$viewHelper417->setRenderChildrenClosure($renderChildrenClosure416);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments414['value'] = $viewHelper417->initializeArgumentsAndRender();
$arguments414['keepQuotes'] = false;
$arguments414['encoding'] = 'UTF-8';
$arguments414['doubleEncode'] = true;
$renderChildrenClosure418 = function() use ($renderingContext, $self) {
return NULL;
};
$value419 = ($arguments414['value'] !== NULL ? $arguments414['value'] : $renderChildrenClosure418());

$output283 .= !is_string($value419) && !(is_object($value419) && method_exists($value419, '__toString')) ? $value419 : htmlspecialchars($value419, ($arguments414['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments414['encoding'], $arguments414['doubleEncode']);

$output283 .= '</a>
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments420 = array();
$arguments420['action'] = 'deleteSite';
// Rendering Array
$array421 = array();
$array421['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments420['arguments'] = $array421;
$arguments420['class'] = 'neos-inline';
$arguments420['additionalAttributes'] = NULL;
$arguments420['data'] = NULL;
$arguments420['controller'] = NULL;
$arguments420['package'] = NULL;
$arguments420['subpackage'] = NULL;
$arguments420['object'] = NULL;
$arguments420['section'] = '';
$arguments420['format'] = '';
$arguments420['additionalParams'] = array (
);
$arguments420['absolute'] = false;
$arguments420['addQueryString'] = false;
$arguments420['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments420['fieldNamePrefix'] = NULL;
$arguments420['actionUri'] = NULL;
$arguments420['objectName'] = NULL;
$arguments420['useParentRequest'] = false;
$arguments420['enctype'] = NULL;
$arguments420['method'] = NULL;
$arguments420['name'] = NULL;
$arguments420['onreset'] = NULL;
$arguments420['onsubmit'] = NULL;
$arguments420['dir'] = NULL;
$arguments420['id'] = NULL;
$arguments420['lang'] = NULL;
$arguments420['style'] = NULL;
$arguments420['title'] = NULL;
$arguments420['accesskey'] = NULL;
$arguments420['tabindex'] = NULL;
$arguments420['onclick'] = NULL;
$renderChildrenClosure422 = function() use ($renderingContext, $self) {
$output423 = '';

$output423 .= '
															<button class="neos-button neos-button-danger" title="Delete this site">
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments424 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments425 = array();
$arguments425['id'] = 'sites.confirmDelete';
$arguments425['value'] = 'Yes, delete this site';
$arguments425['arguments'] = array (
);
$arguments425['source'] = 'Main';
$arguments425['package'] = NULL;
$arguments425['quantity'] = NULL;
$arguments425['languageIdentifier'] = NULL;
$renderChildrenClosure426 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper427 = $self->getViewHelper('$viewHelper427', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper427->setArguments($arguments425);
$viewHelper427->setRenderingContext($renderingContext);
$viewHelper427->setRenderChildrenClosure($renderChildrenClosure426);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments424['value'] = $viewHelper427->initializeArgumentsAndRender();
$arguments424['keepQuotes'] = false;
$arguments424['encoding'] = 'UTF-8';
$arguments424['doubleEncode'] = true;
$renderChildrenClosure428 = function() use ($renderingContext, $self) {
return NULL;
};
$value429 = ($arguments424['value'] !== NULL ? $arguments424['value'] : $renderChildrenClosure428());

$output423 .= !is_string($value429) && !(is_object($value429) && method_exists($value429, '__toString')) ? $value429 : htmlspecialchars($value429, ($arguments424['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments424['encoding'], $arguments424['doubleEncode']);

$output423 .= '
															</button>
														';
return $output423;
};
$viewHelper430 = $self->getViewHelper('$viewHelper430', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper430->setArguments($arguments420);
$viewHelper430->setRenderingContext($renderingContext);
$viewHelper430->setRenderChildrenClosure($renderChildrenClosure422);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output283 .= $viewHelper430->initializeArgumentsAndRender();

$output283 .= '
													</div>
												</div>
											</div>
											<div class="neos-modal-backdrop neos-in"></div>
										</div>
									</div>
								</td>
							</tr>
						';
return $output283;
};

$output280 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments281, $renderChildrenClosure282, $renderingContext);

$output280 .= '
					';
return $output280;
};
$arguments116['__elseClosure'] = function() use ($renderingContext, $self) {
$output431 = '';

$output431 .= '
						<tr class="fold-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments432 = array();
$arguments432['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageKey', $renderingContext);
$arguments432['keepQuotes'] = false;
$arguments432['encoding'] = 'UTF-8';
$arguments432['doubleEncode'] = true;
$renderChildrenClosure433 = function() use ($renderingContext, $self) {
return NULL;
};
$value434 = ($arguments432['value'] !== NULL ? $arguments432['value'] : $renderChildrenClosure433());

$output431 .= !is_string($value434) && !(is_object($value434) && method_exists($value434, '__toString')) ? $value434 : htmlspecialchars($value434, ($arguments432['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments432['encoding'], $arguments432['doubleEncode']);

$output431 .= '">
							<td colspan="4"><i>No sites available</i></td>
						</tr>
					';
return $output431;
};
$viewHelper435 = $self->getViewHelper('$viewHelper435', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper435->setArguments($arguments116);
$viewHelper435->setRenderingContext($renderingContext);
$viewHelper435->setRenderChildrenClosure($renderChildrenClosure117);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output21 .= $viewHelper435->initializeArgumentsAndRender();

$output21 .= '
			';
return $output21;
};

$output0 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments19, $renderChildrenClosure20, $renderingContext);

$output0 .= '
		</tbody>
	</table>
	<div class="neos-footer">
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments436 = array();
$arguments436['action'] = 'newSite';
$arguments436['class'] = 'neos-button neos-button-primary';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments437 = array();
$arguments437['id'] = 'clickToCreate';
$arguments437['value'] = 'Click to create new';
$arguments437['arguments'] = array (
);
$arguments437['source'] = 'Main';
$arguments437['package'] = NULL;
$arguments437['quantity'] = NULL;
$arguments437['languageIdentifier'] = NULL;
$renderChildrenClosure438 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper439 = $self->getViewHelper('$viewHelper439', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper439->setArguments($arguments437);
$viewHelper439->setRenderingContext($renderingContext);
$viewHelper439->setRenderChildrenClosure($renderChildrenClosure438);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments436['title'] = $viewHelper439->initializeArgumentsAndRender();
$arguments436['additionalAttributes'] = NULL;
$arguments436['data'] = NULL;
$arguments436['arguments'] = array (
);
$arguments436['controller'] = NULL;
$arguments436['package'] = NULL;
$arguments436['subpackage'] = NULL;
$arguments436['section'] = '';
$arguments436['format'] = '';
$arguments436['additionalParams'] = array (
);
$arguments436['addQueryString'] = false;
$arguments436['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments436['useParentRequest'] = false;
$arguments436['absolute'] = true;
$arguments436['dir'] = NULL;
$arguments436['id'] = NULL;
$arguments436['lang'] = NULL;
$arguments436['style'] = NULL;
$arguments436['accesskey'] = NULL;
$arguments436['tabindex'] = NULL;
$arguments436['onclick'] = NULL;
$arguments436['name'] = NULL;
$arguments436['rel'] = NULL;
$arguments436['rev'] = NULL;
$arguments436['target'] = NULL;
$renderChildrenClosure440 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments441 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments442 = array();
$arguments442['id'] = 'sites.add';
$arguments442['value'] = 'Add new site';
$arguments442['arguments'] = array (
);
$arguments442['source'] = 'Main';
$arguments442['package'] = NULL;
$arguments442['quantity'] = NULL;
$arguments442['languageIdentifier'] = NULL;
$renderChildrenClosure443 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper444 = $self->getViewHelper('$viewHelper444', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper444->setArguments($arguments442);
$viewHelper444->setRenderingContext($renderingContext);
$viewHelper444->setRenderChildrenClosure($renderChildrenClosure443);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments441['value'] = $viewHelper444->initializeArgumentsAndRender();
$arguments441['keepQuotes'] = false;
$arguments441['encoding'] = 'UTF-8';
$arguments441['doubleEncode'] = true;
$renderChildrenClosure445 = function() use ($renderingContext, $self) {
return NULL;
};
$value446 = ($arguments441['value'] !== NULL ? $arguments441['value'] : $renderChildrenClosure445());
return !is_string($value446) && !(is_object($value446) && method_exists($value446, '__toString')) ? $value446 : htmlspecialchars($value446, ($arguments441['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments441['encoding'], $arguments441['doubleEncode']);
};
$viewHelper447 = $self->getViewHelper('$viewHelper447', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper447->setArguments($arguments436);
$viewHelper447->setRenderingContext($renderingContext);
$viewHelper447->setRenderChildrenClosure($renderChildrenClosure440);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output0 .= $viewHelper447->initializeArgumentsAndRender();

$output0 .= '
	</div>

	<script>
		(function($) {
			$(\'.fold-toggle\').click(function() {
				$(this).toggleClass(\'icon-chevron-down icon-chevron-up\');
				$(\'tr.\' + $(this).data(\'toggle\')).toggle();
			});
		})(jQuery);
	</script>
';

return $output0;
}
/**
 * Main Render function
 */
public function render(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output448 = '';

$output448 .= '
';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments449 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\LayoutViewHelper
$arguments450 = array();
$arguments450['name'] = 'BackendSubModule';
$renderChildrenClosure451 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper452 = $self->getViewHelper('$viewHelper452', $renderingContext, 'TYPO3\Fluid\ViewHelpers\LayoutViewHelper');
$viewHelper452->setArguments($arguments450);
$viewHelper452->setRenderingContext($renderingContext);
$viewHelper452->setRenderChildrenClosure($renderChildrenClosure451);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\LayoutViewHelper
$arguments449['value'] = $viewHelper452->initializeArgumentsAndRender();
$arguments449['keepQuotes'] = false;
$arguments449['encoding'] = 'UTF-8';
$arguments449['doubleEncode'] = true;
$renderChildrenClosure453 = function() use ($renderingContext, $self) {
return NULL;
};
$value454 = ($arguments449['value'] !== NULL ? $arguments449['value'] : $renderChildrenClosure453());

$output448 .= !is_string($value454) && !(is_object($value454) && method_exists($value454, '__toString')) ? $value454 : htmlspecialchars($value454, ($arguments449['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments449['encoding'], $arguments449['doubleEncode']);

$output448 .= '

';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\SectionViewHelper
$arguments455 = array();
$arguments455['name'] = 'content';
$renderChildrenClosure456 = function() use ($renderingContext, $self) {
$output457 = '';

$output457 .= '
	<table class="neos-table">
		<thead>
			<tr>
				<th>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments458 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments459 = array();
$arguments459['id'] = 'name';
$arguments459['value'] = 'Name';
$arguments459['arguments'] = array (
);
$arguments459['source'] = 'Main';
$arguments459['package'] = NULL;
$arguments459['quantity'] = NULL;
$arguments459['languageIdentifier'] = NULL;
$renderChildrenClosure460 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper461 = $self->getViewHelper('$viewHelper461', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper461->setArguments($arguments459);
$viewHelper461->setRenderingContext($renderingContext);
$viewHelper461->setRenderChildrenClosure($renderChildrenClosure460);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments458['value'] = $viewHelper461->initializeArgumentsAndRender();
$arguments458['keepQuotes'] = false;
$arguments458['encoding'] = 'UTF-8';
$arguments458['doubleEncode'] = true;
$renderChildrenClosure462 = function() use ($renderingContext, $self) {
return NULL;
};
$value463 = ($arguments458['value'] !== NULL ? $arguments458['value'] : $renderChildrenClosure462());

$output457 .= !is_string($value463) && !(is_object($value463) && method_exists($value463, '__toString')) ? $value463 : htmlspecialchars($value463, ($arguments458['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments458['encoding'], $arguments458['doubleEncode']);

$output457 .= '</th>
				<th>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments464 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments465 = array();
$arguments465['id'] = 'sites.rootNodeName';
$arguments465['value'] = 'Rootnode name';
$arguments465['arguments'] = array (
);
$arguments465['source'] = 'Main';
$arguments465['package'] = NULL;
$arguments465['quantity'] = NULL;
$arguments465['languageIdentifier'] = NULL;
$renderChildrenClosure466 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper467 = $self->getViewHelper('$viewHelper467', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper467->setArguments($arguments465);
$viewHelper467->setRenderingContext($renderingContext);
$viewHelper467->setRenderChildrenClosure($renderChildrenClosure466);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments464['value'] = $viewHelper467->initializeArgumentsAndRender();
$arguments464['keepQuotes'] = false;
$arguments464['encoding'] = 'UTF-8';
$arguments464['doubleEncode'] = true;
$renderChildrenClosure468 = function() use ($renderingContext, $self) {
return NULL;
};
$value469 = ($arguments464['value'] !== NULL ? $arguments464['value'] : $renderChildrenClosure468());

$output457 .= !is_string($value469) && !(is_object($value469) && method_exists($value469, '__toString')) ? $value469 : htmlspecialchars($value469, ($arguments464['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments464['encoding'], $arguments464['doubleEncode']);

$output457 .= '</th>
				<th>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments470 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments471 = array();
$arguments471['id'] = 'sites.domains';
$arguments471['value'] = 'Domains';
$arguments471['arguments'] = array (
);
$arguments471['source'] = 'Main';
$arguments471['package'] = NULL;
$arguments471['quantity'] = NULL;
$arguments471['languageIdentifier'] = NULL;
$renderChildrenClosure472 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper473 = $self->getViewHelper('$viewHelper473', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper473->setArguments($arguments471);
$viewHelper473->setRenderingContext($renderingContext);
$viewHelper473->setRenderChildrenClosure($renderChildrenClosure472);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments470['value'] = $viewHelper473->initializeArgumentsAndRender();
$arguments470['keepQuotes'] = false;
$arguments470['encoding'] = 'UTF-8';
$arguments470['doubleEncode'] = true;
$renderChildrenClosure474 = function() use ($renderingContext, $self) {
return NULL;
};
$value475 = ($arguments470['value'] !== NULL ? $arguments470['value'] : $renderChildrenClosure474());

$output457 .= !is_string($value475) && !(is_object($value475) && method_exists($value475, '__toString')) ? $value475 : htmlspecialchars($value475, ($arguments470['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments470['encoding'], $arguments470['doubleEncode']);

$output457 .= '</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments476 = array();
$arguments476['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackagesAndSites', $renderingContext);
$arguments476['as'] = 'sitePackageAndSite';
$arguments476['key'] = 'sitePackageKey';
$arguments476['reverse'] = false;
$arguments476['iteration'] = NULL;
$renderChildrenClosure477 = function() use ($renderingContext, $self) {
$output478 = '';

$output478 .= '
				<tr class="neos-folder">
					<td class="neos-priority1" colspan="3">
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments479 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments480 = array();
$arguments480['id'] = 'package';
$arguments480['value'] = 'Package';
$arguments480['arguments'] = array (
);
$arguments480['source'] = 'Main';
$arguments480['package'] = NULL;
$arguments480['quantity'] = NULL;
$arguments480['languageIdentifier'] = NULL;
$renderChildrenClosure481 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper482 = $self->getViewHelper('$viewHelper482', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper482->setArguments($arguments480);
$viewHelper482->setRenderingContext($renderingContext);
$viewHelper482->setRenderChildrenClosure($renderChildrenClosure481);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments479['value'] = $viewHelper482->initializeArgumentsAndRender();
$arguments479['keepQuotes'] = false;
$arguments479['encoding'] = 'UTF-8';
$arguments479['doubleEncode'] = true;
$renderChildrenClosure483 = function() use ($renderingContext, $self) {
return NULL;
};
$value484 = ($arguments479['value'] !== NULL ? $arguments479['value'] : $renderChildrenClosure483());

$output478 .= !is_string($value484) && !(is_object($value484) && method_exists($value484, '__toString')) ? $value484 : htmlspecialchars($value484, ($arguments479['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments479['encoding'], $arguments479['doubleEncode']);

$output478 .= ':
						';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper
$arguments485 = array();
$arguments485['path'] = 'administration/packages';
$arguments485['section'] = 'Sites';
$arguments485['class'] = 'neos-label';
$arguments485['title'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.package.packageMetaData.description', $renderingContext);
// Rendering Array
$array486 = array();
$array486['data-neos-toggle'] = 'tooltip';
$arguments485['additionalAttributes'] = $array486;
$arguments485['data'] = NULL;
$arguments485['action'] = NULL;
$arguments485['arguments'] = array (
);
$arguments485['format'] = '';
$arguments485['additionalParams'] = array (
);
$arguments485['addQueryString'] = false;
$arguments485['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments485['dir'] = NULL;
$arguments485['id'] = NULL;
$arguments485['lang'] = NULL;
$arguments485['style'] = NULL;
$arguments485['accesskey'] = NULL;
$arguments485['tabindex'] = NULL;
$arguments485['onclick'] = NULL;
$arguments485['name'] = NULL;
$arguments485['rel'] = NULL;
$arguments485['rev'] = NULL;
$arguments485['target'] = NULL;
$renderChildrenClosure487 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments488 = array();
$arguments488['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.packageKey', $renderingContext);
$arguments488['keepQuotes'] = false;
$arguments488['encoding'] = 'UTF-8';
$arguments488['doubleEncode'] = true;
$renderChildrenClosure489 = function() use ($renderingContext, $self) {
return NULL;
};
$value490 = ($arguments488['value'] !== NULL ? $arguments488['value'] : $renderChildrenClosure489());
return !is_string($value490) && !(is_object($value490) && method_exists($value490, '__toString')) ? $value490 : htmlspecialchars($value490, ($arguments488['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments488['encoding'], $arguments488['doubleEncode']);
};
$viewHelper491 = $self->getViewHelper('$viewHelper491', $renderingContext, 'TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper');
$viewHelper491->setArguments($arguments485);
$viewHelper491->setRenderingContext($renderingContext);
$viewHelper491->setRenderChildrenClosure($renderChildrenClosure487);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper

$output478 .= $viewHelper491->initializeArgumentsAndRender();

$output478 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments492 = array();
// Rendering Boolean node
$arguments492['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.package', $renderingContext));
$arguments492['then'] = NULL;
$arguments492['else'] = NULL;
$renderChildrenClosure493 = function() use ($renderingContext, $self) {
$output494 = '';

$output494 .= '
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments495 = array();
$renderChildrenClosure496 = function() use ($renderingContext, $self) {
$output497 = '';

$output497 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments498 = array();
// Rendering Boolean node
$arguments498['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.packageIsActive', $renderingContext));
$arguments498['then'] = NULL;
$arguments498['else'] = NULL;
$renderChildrenClosure499 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments500 = array();
$renderChildrenClosure501 = function() use ($renderingContext, $self) {
$output502 = '';

$output502 .= '<span class="neos-badge neos-badge-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments503 = array();
// Rendering Boolean node
$arguments503['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.sites', $renderingContext));
$arguments503['then'] = 'important';
$arguments503['else'] = 'warning';
$renderChildrenClosure504 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper505 = $self->getViewHelper('$viewHelper505', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper505->setArguments($arguments503);
$viewHelper505->setRenderingContext($renderingContext);
$viewHelper505->setRenderChildrenClosure($renderChildrenClosure504);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output502 .= $viewHelper505->initializeArgumentsAndRender();

$output502 .= '">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments506 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments507 = array();
$arguments507['id'] = 'deactivated';
$arguments507['value'] = 'Deactivated';
$arguments507['arguments'] = array (
);
$arguments507['source'] = 'Main';
$arguments507['package'] = NULL;
$arguments507['quantity'] = NULL;
$arguments507['languageIdentifier'] = NULL;
$renderChildrenClosure508 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper509 = $self->getViewHelper('$viewHelper509', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper509->setArguments($arguments507);
$viewHelper509->setRenderingContext($renderingContext);
$viewHelper509->setRenderChildrenClosure($renderChildrenClosure508);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments506['value'] = $viewHelper509->initializeArgumentsAndRender();
$arguments506['keepQuotes'] = false;
$arguments506['encoding'] = 'UTF-8';
$arguments506['doubleEncode'] = true;
$renderChildrenClosure510 = function() use ($renderingContext, $self) {
return NULL;
};
$value511 = ($arguments506['value'] !== NULL ? $arguments506['value'] : $renderChildrenClosure510());

$output502 .= !is_string($value511) && !(is_object($value511) && method_exists($value511, '__toString')) ? $value511 : htmlspecialchars($value511, ($arguments506['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments506['encoding'], $arguments506['doubleEncode']);

$output502 .= '</span>';
return $output502;
};
$viewHelper512 = $self->getViewHelper('$viewHelper512', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper512->setArguments($arguments500);
$viewHelper512->setRenderingContext($renderingContext);
$viewHelper512->setRenderChildrenClosure($renderChildrenClosure501);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
return $viewHelper512->initializeArgumentsAndRender();
};
$arguments498['__elseClosure'] = function() use ($renderingContext, $self) {
$output513 = '';

$output513 .= '<span class="neos-badge neos-badge-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments514 = array();
// Rendering Boolean node
$arguments514['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.sites', $renderingContext));
$arguments514['then'] = 'important';
$arguments514['else'] = 'warning';
$renderChildrenClosure515 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper516 = $self->getViewHelper('$viewHelper516', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper516->setArguments($arguments514);
$viewHelper516->setRenderingContext($renderingContext);
$viewHelper516->setRenderChildrenClosure($renderChildrenClosure515);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output513 .= $viewHelper516->initializeArgumentsAndRender();

$output513 .= '">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments517 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments518 = array();
$arguments518['id'] = 'deactivated';
$arguments518['value'] = 'Deactivated';
$arguments518['arguments'] = array (
);
$arguments518['source'] = 'Main';
$arguments518['package'] = NULL;
$arguments518['quantity'] = NULL;
$arguments518['languageIdentifier'] = NULL;
$renderChildrenClosure519 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper520 = $self->getViewHelper('$viewHelper520', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper520->setArguments($arguments518);
$viewHelper520->setRenderingContext($renderingContext);
$viewHelper520->setRenderChildrenClosure($renderChildrenClosure519);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments517['value'] = $viewHelper520->initializeArgumentsAndRender();
$arguments517['keepQuotes'] = false;
$arguments517['encoding'] = 'UTF-8';
$arguments517['doubleEncode'] = true;
$renderChildrenClosure521 = function() use ($renderingContext, $self) {
return NULL;
};
$value522 = ($arguments517['value'] !== NULL ? $arguments517['value'] : $renderChildrenClosure521());

$output513 .= !is_string($value522) && !(is_object($value522) && method_exists($value522, '__toString')) ? $value522 : htmlspecialchars($value522, ($arguments517['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments517['encoding'], $arguments517['doubleEncode']);

$output513 .= '</span>';
return $output513;
};
$viewHelper523 = $self->getViewHelper('$viewHelper523', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper523->setArguments($arguments498);
$viewHelper523->setRenderingContext($renderingContext);
$viewHelper523->setRenderChildrenClosure($renderChildrenClosure499);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output497 .= $viewHelper523->initializeArgumentsAndRender();

$output497 .= '
							';
return $output497;
};
$viewHelper524 = $self->getViewHelper('$viewHelper524', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper524->setArguments($arguments495);
$viewHelper524->setRenderingContext($renderingContext);
$viewHelper524->setRenderChildrenClosure($renderChildrenClosure496);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output494 .= $viewHelper524->initializeArgumentsAndRender();

$output494 .= '
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments525 = array();
$renderChildrenClosure526 = function() use ($renderingContext, $self) {
$output527 = '';

$output527 .= '<span class="neos-badge neos-badge-important">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments528 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments529 = array();
$arguments529['id'] = 'unavailable';
$arguments529['value'] = 'Unavailable';
$arguments529['arguments'] = array (
);
$arguments529['source'] = 'Main';
$arguments529['package'] = NULL;
$arguments529['quantity'] = NULL;
$arguments529['languageIdentifier'] = NULL;
$renderChildrenClosure530 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper531 = $self->getViewHelper('$viewHelper531', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper531->setArguments($arguments529);
$viewHelper531->setRenderingContext($renderingContext);
$viewHelper531->setRenderChildrenClosure($renderChildrenClosure530);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments528['value'] = $viewHelper531->initializeArgumentsAndRender();
$arguments528['keepQuotes'] = false;
$arguments528['encoding'] = 'UTF-8';
$arguments528['doubleEncode'] = true;
$renderChildrenClosure532 = function() use ($renderingContext, $self) {
return NULL;
};
$value533 = ($arguments528['value'] !== NULL ? $arguments528['value'] : $renderChildrenClosure532());

$output527 .= !is_string($value533) && !(is_object($value533) && method_exists($value533, '__toString')) ? $value533 : htmlspecialchars($value533, ($arguments528['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments528['encoding'], $arguments528['doubleEncode']);

$output527 .= '</span>';
return $output527;
};
$viewHelper534 = $self->getViewHelper('$viewHelper534', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper534->setArguments($arguments525);
$viewHelper534->setRenderingContext($renderingContext);
$viewHelper534->setRenderChildrenClosure($renderChildrenClosure526);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output494 .= $viewHelper534->initializeArgumentsAndRender();

$output494 .= '
						';
return $output494;
};
$arguments492['__thenClosure'] = function() use ($renderingContext, $self) {
$output535 = '';

$output535 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments536 = array();
// Rendering Boolean node
$arguments536['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.packageIsActive', $renderingContext));
$arguments536['then'] = NULL;
$arguments536['else'] = NULL;
$renderChildrenClosure537 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments538 = array();
$renderChildrenClosure539 = function() use ($renderingContext, $self) {
$output540 = '';

$output540 .= '<span class="neos-badge neos-badge-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments541 = array();
// Rendering Boolean node
$arguments541['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.sites', $renderingContext));
$arguments541['then'] = 'important';
$arguments541['else'] = 'warning';
$renderChildrenClosure542 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper543 = $self->getViewHelper('$viewHelper543', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper543->setArguments($arguments541);
$viewHelper543->setRenderingContext($renderingContext);
$viewHelper543->setRenderChildrenClosure($renderChildrenClosure542);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output540 .= $viewHelper543->initializeArgumentsAndRender();

$output540 .= '">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments544 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments545 = array();
$arguments545['id'] = 'deactivated';
$arguments545['value'] = 'Deactivated';
$arguments545['arguments'] = array (
);
$arguments545['source'] = 'Main';
$arguments545['package'] = NULL;
$arguments545['quantity'] = NULL;
$arguments545['languageIdentifier'] = NULL;
$renderChildrenClosure546 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper547 = $self->getViewHelper('$viewHelper547', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper547->setArguments($arguments545);
$viewHelper547->setRenderingContext($renderingContext);
$viewHelper547->setRenderChildrenClosure($renderChildrenClosure546);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments544['value'] = $viewHelper547->initializeArgumentsAndRender();
$arguments544['keepQuotes'] = false;
$arguments544['encoding'] = 'UTF-8';
$arguments544['doubleEncode'] = true;
$renderChildrenClosure548 = function() use ($renderingContext, $self) {
return NULL;
};
$value549 = ($arguments544['value'] !== NULL ? $arguments544['value'] : $renderChildrenClosure548());

$output540 .= !is_string($value549) && !(is_object($value549) && method_exists($value549, '__toString')) ? $value549 : htmlspecialchars($value549, ($arguments544['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments544['encoding'], $arguments544['doubleEncode']);

$output540 .= '</span>';
return $output540;
};
$viewHelper550 = $self->getViewHelper('$viewHelper550', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper550->setArguments($arguments538);
$viewHelper550->setRenderingContext($renderingContext);
$viewHelper550->setRenderChildrenClosure($renderChildrenClosure539);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
return $viewHelper550->initializeArgumentsAndRender();
};
$arguments536['__elseClosure'] = function() use ($renderingContext, $self) {
$output551 = '';

$output551 .= '<span class="neos-badge neos-badge-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments552 = array();
// Rendering Boolean node
$arguments552['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.sites', $renderingContext));
$arguments552['then'] = 'important';
$arguments552['else'] = 'warning';
$renderChildrenClosure553 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper554 = $self->getViewHelper('$viewHelper554', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper554->setArguments($arguments552);
$viewHelper554->setRenderingContext($renderingContext);
$viewHelper554->setRenderChildrenClosure($renderChildrenClosure553);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output551 .= $viewHelper554->initializeArgumentsAndRender();

$output551 .= '">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments555 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments556 = array();
$arguments556['id'] = 'deactivated';
$arguments556['value'] = 'Deactivated';
$arguments556['arguments'] = array (
);
$arguments556['source'] = 'Main';
$arguments556['package'] = NULL;
$arguments556['quantity'] = NULL;
$arguments556['languageIdentifier'] = NULL;
$renderChildrenClosure557 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper558 = $self->getViewHelper('$viewHelper558', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper558->setArguments($arguments556);
$viewHelper558->setRenderingContext($renderingContext);
$viewHelper558->setRenderChildrenClosure($renderChildrenClosure557);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments555['value'] = $viewHelper558->initializeArgumentsAndRender();
$arguments555['keepQuotes'] = false;
$arguments555['encoding'] = 'UTF-8';
$arguments555['doubleEncode'] = true;
$renderChildrenClosure559 = function() use ($renderingContext, $self) {
return NULL;
};
$value560 = ($arguments555['value'] !== NULL ? $arguments555['value'] : $renderChildrenClosure559());

$output551 .= !is_string($value560) && !(is_object($value560) && method_exists($value560, '__toString')) ? $value560 : htmlspecialchars($value560, ($arguments555['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments555['encoding'], $arguments555['doubleEncode']);

$output551 .= '</span>';
return $output551;
};
$viewHelper561 = $self->getViewHelper('$viewHelper561', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper561->setArguments($arguments536);
$viewHelper561->setRenderingContext($renderingContext);
$viewHelper561->setRenderChildrenClosure($renderChildrenClosure537);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output535 .= $viewHelper561->initializeArgumentsAndRender();

$output535 .= '
							';
return $output535;
};
$arguments492['__elseClosure'] = function() use ($renderingContext, $self) {
$output562 = '';

$output562 .= '<span class="neos-badge neos-badge-important">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments563 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments564 = array();
$arguments564['id'] = 'unavailable';
$arguments564['value'] = 'Unavailable';
$arguments564['arguments'] = array (
);
$arguments564['source'] = 'Main';
$arguments564['package'] = NULL;
$arguments564['quantity'] = NULL;
$arguments564['languageIdentifier'] = NULL;
$renderChildrenClosure565 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper566 = $self->getViewHelper('$viewHelper566', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper566->setArguments($arguments564);
$viewHelper566->setRenderingContext($renderingContext);
$viewHelper566->setRenderChildrenClosure($renderChildrenClosure565);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments563['value'] = $viewHelper566->initializeArgumentsAndRender();
$arguments563['keepQuotes'] = false;
$arguments563['encoding'] = 'UTF-8';
$arguments563['doubleEncode'] = true;
$renderChildrenClosure567 = function() use ($renderingContext, $self) {
return NULL;
};
$value568 = ($arguments563['value'] !== NULL ? $arguments563['value'] : $renderChildrenClosure567());

$output562 .= !is_string($value568) && !(is_object($value568) && method_exists($value568, '__toString')) ? $value568 : htmlspecialchars($value568, ($arguments563['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments563['encoding'], $arguments563['doubleEncode']);

$output562 .= '</span>';
return $output562;
};
$viewHelper569 = $self->getViewHelper('$viewHelper569', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper569->setArguments($arguments492);
$viewHelper569->setRenderingContext($renderingContext);
$viewHelper569->setRenderChildrenClosure($renderChildrenClosure493);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output478 .= $viewHelper569->initializeArgumentsAndRender();

$output478 .= '
					</td>
					<td class="neos-priority1 neos-aRight">
						<i class="fold-toggle icon-chevron-up icon-white" data-toggle="fold-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments570 = array();
$arguments570['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageKey', $renderingContext);
$arguments570['keepQuotes'] = false;
$arguments570['encoding'] = 'UTF-8';
$arguments570['doubleEncode'] = true;
$renderChildrenClosure571 = function() use ($renderingContext, $self) {
return NULL;
};
$value572 = ($arguments570['value'] !== NULL ? $arguments570['value'] : $renderChildrenClosure571());

$output478 .= !is_string($value572) && !(is_object($value572) && method_exists($value572, '__toString')) ? $value572 : htmlspecialchars($value572, ($arguments570['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments570['encoding'], $arguments570['doubleEncode']);

$output478 .= '"></i>
					</td>
				</tr>
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments573 = array();
// Rendering Boolean node
$arguments573['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.sites', $renderingContext));
$arguments573['then'] = NULL;
$arguments573['else'] = NULL;
$renderChildrenClosure574 = function() use ($renderingContext, $self) {
$output575 = '';

$output575 .= '
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments576 = array();
$renderChildrenClosure577 = function() use ($renderingContext, $self) {
$output578 = '';

$output578 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments579 = array();
$arguments579['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.sites', $renderingContext);
$arguments579['as'] = 'site';
$arguments579['key'] = '';
$arguments579['reverse'] = false;
$arguments579['iteration'] = NULL;
$renderChildrenClosure580 = function() use ($renderingContext, $self) {
$output581 = '';

$output581 .= '
							<tr class="fold-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments582 = array();
$arguments582['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageKey', $renderingContext);
$arguments582['keepQuotes'] = false;
$arguments582['encoding'] = 'UTF-8';
$arguments582['doubleEncode'] = true;
$renderChildrenClosure583 = function() use ($renderingContext, $self) {
return NULL;
};
$value584 = ($arguments582['value'] !== NULL ? $arguments582['value'] : $renderChildrenClosure583());

$output581 .= !is_string($value584) && !(is_object($value584) && method_exists($value584, '__toString')) ? $value584 : htmlspecialchars($value584, ($arguments582['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments582['encoding'], $arguments582['doubleEncode']);

$output581 .= '">
								<td>
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments585 = array();
$arguments585['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments585['keepQuotes'] = false;
$arguments585['encoding'] = 'UTF-8';
$arguments585['doubleEncode'] = true;
$renderChildrenClosure586 = function() use ($renderingContext, $self) {
return NULL;
};
$value587 = ($arguments585['value'] !== NULL ? $arguments585['value'] : $renderChildrenClosure586());

$output581 .= !is_string($value587) && !(is_object($value587) && method_exists($value587, '__toString')) ? $value587 : htmlspecialchars($value587, ($arguments585['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments585['encoding'], $arguments585['doubleEncode']);

$output581 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments588 = array();
// Rendering Boolean node
$arguments588['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.online', $renderingContext));
$arguments588['then'] = NULL;
$arguments588['else'] = NULL;
$renderChildrenClosure589 = function() use ($renderingContext, $self) {
$output590 = '';

$output590 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments591 = array();
$renderChildrenClosure592 = function() use ($renderingContext, $self) {
$output593 = '';

$output593 .= '
											<span class="neos-badge neos-badge-warning">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments594 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments595 = array();
$arguments595['id'] = 'inactive';
$arguments595['value'] = 'Inactive';
$arguments595['arguments'] = array (
);
$arguments595['source'] = 'Main';
$arguments595['package'] = NULL;
$arguments595['quantity'] = NULL;
$arguments595['languageIdentifier'] = NULL;
$renderChildrenClosure596 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper597 = $self->getViewHelper('$viewHelper597', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper597->setArguments($arguments595);
$viewHelper597->setRenderingContext($renderingContext);
$viewHelper597->setRenderChildrenClosure($renderChildrenClosure596);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments594['value'] = $viewHelper597->initializeArgumentsAndRender();
$arguments594['keepQuotes'] = false;
$arguments594['encoding'] = 'UTF-8';
$arguments594['doubleEncode'] = true;
$renderChildrenClosure598 = function() use ($renderingContext, $self) {
return NULL;
};
$value599 = ($arguments594['value'] !== NULL ? $arguments594['value'] : $renderChildrenClosure598());

$output593 .= !is_string($value599) && !(is_object($value599) && method_exists($value599, '__toString')) ? $value599 : htmlspecialchars($value599, ($arguments594['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments594['encoding'], $arguments594['doubleEncode']);

$output593 .= '</span>
										';
return $output593;
};
$viewHelper600 = $self->getViewHelper('$viewHelper600', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper600->setArguments($arguments591);
$viewHelper600->setRenderingContext($renderingContext);
$viewHelper600->setRenderChildrenClosure($renderChildrenClosure592);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output590 .= $viewHelper600->initializeArgumentsAndRender();

$output590 .= '
									';
return $output590;
};
$arguments588['__elseClosure'] = function() use ($renderingContext, $self) {
$output601 = '';

$output601 .= '
											<span class="neos-badge neos-badge-warning">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments602 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments603 = array();
$arguments603['id'] = 'inactive';
$arguments603['value'] = 'Inactive';
$arguments603['arguments'] = array (
);
$arguments603['source'] = 'Main';
$arguments603['package'] = NULL;
$arguments603['quantity'] = NULL;
$arguments603['languageIdentifier'] = NULL;
$renderChildrenClosure604 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper605 = $self->getViewHelper('$viewHelper605', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper605->setArguments($arguments603);
$viewHelper605->setRenderingContext($renderingContext);
$viewHelper605->setRenderChildrenClosure($renderChildrenClosure604);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments602['value'] = $viewHelper605->initializeArgumentsAndRender();
$arguments602['keepQuotes'] = false;
$arguments602['encoding'] = 'UTF-8';
$arguments602['doubleEncode'] = true;
$renderChildrenClosure606 = function() use ($renderingContext, $self) {
return NULL;
};
$value607 = ($arguments602['value'] !== NULL ? $arguments602['value'] : $renderChildrenClosure606());

$output601 .= !is_string($value607) && !(is_object($value607) && method_exists($value607, '__toString')) ? $value607 : htmlspecialchars($value607, ($arguments602['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments602['encoding'], $arguments602['doubleEncode']);

$output601 .= '</span>
										';
return $output601;
};
$viewHelper608 = $self->getViewHelper('$viewHelper608', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper608->setArguments($arguments588);
$viewHelper608->setRenderingContext($renderingContext);
$viewHelper608->setRenderChildrenClosure($renderChildrenClosure589);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output581 .= $viewHelper608->initializeArgumentsAndRender();

$output581 .= '
								</td>
								<td><span class="neos-label">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments609 = array();
$arguments609['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments609['keepQuotes'] = false;
$arguments609['encoding'] = 'UTF-8';
$arguments609['doubleEncode'] = true;
$renderChildrenClosure610 = function() use ($renderingContext, $self) {
return NULL;
};
$value611 = ($arguments609['value'] !== NULL ? $arguments609['value'] : $renderChildrenClosure610());

$output581 .= !is_string($value611) && !(is_object($value611) && method_exists($value611, '__toString')) ? $value611 : htmlspecialchars($value611, ($arguments609['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments609['encoding'], $arguments609['doubleEncode']);

$output581 .= '</span></td>
								<td><span class="neos-badge neos-badge-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments612 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments613 = array();
$arguments613['subject'] = NULL;
$renderChildrenClosure614 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.activeDomains', $renderingContext);
};
$viewHelper615 = $self->getViewHelper('$viewHelper615', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper615->setArguments($arguments613);
$viewHelper615->setRenderingContext($renderingContext);
$viewHelper615->setRenderChildrenClosure($renderChildrenClosure614);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments612['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', $viewHelper615->initializeArgumentsAndRender(), 0);
$arguments612['then'] = 'info';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments616 = array();
// Rendering Boolean node
$arguments616['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'multipleSites', $renderingContext));
$arguments616['then'] = 'important';
$arguments616['else'] = 'inverse';
$renderChildrenClosure617 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper618 = $self->getViewHelper('$viewHelper618', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper618->setArguments($arguments616);
$viewHelper618->setRenderingContext($renderingContext);
$viewHelper618->setRenderChildrenClosure($renderChildrenClosure617);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments612['else'] = $viewHelper618->initializeArgumentsAndRender();
$renderChildrenClosure619 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper620 = $self->getViewHelper('$viewHelper620', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper620->setArguments($arguments612);
$viewHelper620->setRenderingContext($renderingContext);
$viewHelper620->setRenderChildrenClosure($renderChildrenClosure619);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output581 .= $viewHelper620->initializeArgumentsAndRender();

$output581 .= '">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments621 = array();
$arguments621['subject'] = NULL;
$renderChildrenClosure622 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.domains', $renderingContext);
};
$viewHelper623 = $self->getViewHelper('$viewHelper623', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper623->setArguments($arguments621);
$viewHelper623->setRenderingContext($renderingContext);
$viewHelper623->setRenderChildrenClosure($renderChildrenClosure622);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper

$output581 .= $viewHelper623->initializeArgumentsAndRender();

$output581 .= '</span>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments624 = array();
// Rendering Boolean node
$arguments624['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.activeDomains', $renderingContext));
$arguments624['then'] = NULL;
$arguments624['else'] = NULL;
$renderChildrenClosure625 = function() use ($renderingContext, $self) {
$output626 = '';

$output626 .= ' <span class="neos-label neos-label-inverse">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments627 = array();
$arguments627['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.primaryDomain', $renderingContext);
$arguments627['keepQuotes'] = false;
$arguments627['encoding'] = 'UTF-8';
$arguments627['doubleEncode'] = true;
$renderChildrenClosure628 = function() use ($renderingContext, $self) {
return NULL;
};
$value629 = ($arguments627['value'] !== NULL ? $arguments627['value'] : $renderChildrenClosure628());

$output626 .= !is_string($value629) && !(is_object($value629) && method_exists($value629, '__toString')) ? $value629 : htmlspecialchars($value629, ($arguments627['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments627['encoding'], $arguments627['doubleEncode']);

$output626 .= '</span>';
return $output626;
};
$viewHelper630 = $self->getViewHelper('$viewHelper630', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper630->setArguments($arguments624);
$viewHelper630->setRenderingContext($renderingContext);
$viewHelper630->setRenderChildrenClosure($renderChildrenClosure625);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output581 .= $viewHelper630->initializeArgumentsAndRender();

$output581 .= '</td>
								<td class="neos-action">
									<div class="neos-pull-right">
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments631 = array();
$arguments631['action'] = 'edit';
// Rendering Array
$array632 = array();
$array632['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments631['arguments'] = $array632;
$arguments631['class'] = 'neos-button neos-button-primary';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments633 = array();
$arguments633['id'] = 'clickToEdit';
$arguments633['value'] = 'Click to edit';
$arguments633['arguments'] = array (
);
$arguments633['source'] = 'Main';
$arguments633['package'] = NULL;
$arguments633['quantity'] = NULL;
$arguments633['languageIdentifier'] = NULL;
$renderChildrenClosure634 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper635 = $self->getViewHelper('$viewHelper635', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper635->setArguments($arguments633);
$viewHelper635->setRenderingContext($renderingContext);
$viewHelper635->setRenderChildrenClosure($renderChildrenClosure634);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments631['title'] = $viewHelper635->initializeArgumentsAndRender();
// Rendering Array
$array636 = array();
$array636['data-neos-toggle'] = 'tooltip';
$arguments631['additionalAttributes'] = $array636;
$arguments631['data'] = NULL;
$arguments631['controller'] = NULL;
$arguments631['package'] = NULL;
$arguments631['subpackage'] = NULL;
$arguments631['section'] = '';
$arguments631['format'] = '';
$arguments631['additionalParams'] = array (
);
$arguments631['addQueryString'] = false;
$arguments631['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments631['useParentRequest'] = false;
$arguments631['absolute'] = true;
$arguments631['dir'] = NULL;
$arguments631['id'] = NULL;
$arguments631['lang'] = NULL;
$arguments631['style'] = NULL;
$arguments631['accesskey'] = NULL;
$arguments631['tabindex'] = NULL;
$arguments631['onclick'] = NULL;
$arguments631['name'] = NULL;
$arguments631['rel'] = NULL;
$arguments631['rev'] = NULL;
$arguments631['target'] = NULL;
$renderChildrenClosure637 = function() use ($renderingContext, $self) {
return '
											<i class="icon-pencil icon-white"></i>
										';
};
$viewHelper638 = $self->getViewHelper('$viewHelper638', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper638->setArguments($arguments631);
$viewHelper638->setRenderingContext($renderingContext);
$viewHelper638->setRenderChildrenClosure($renderChildrenClosure637);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output581 .= $viewHelper638->initializeArgumentsAndRender();

$output581 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments639 = array();
// Rendering Boolean node
$arguments639['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.state', $renderingContext), 1);
$arguments639['then'] = NULL;
$arguments639['else'] = NULL;
$renderChildrenClosure640 = function() use ($renderingContext, $self) {
$output641 = '';

$output641 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments642 = array();
$renderChildrenClosure643 = function() use ($renderingContext, $self) {
$output644 = '';

$output644 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments645 = array();
$arguments645['action'] = 'deactivateSite';
// Rendering Array
$array646 = array();
$array646['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments645['arguments'] = $array646;
$arguments645['class'] = 'neos-inline';
$arguments645['additionalAttributes'] = NULL;
$arguments645['data'] = NULL;
$arguments645['controller'] = NULL;
$arguments645['package'] = NULL;
$arguments645['subpackage'] = NULL;
$arguments645['object'] = NULL;
$arguments645['section'] = '';
$arguments645['format'] = '';
$arguments645['additionalParams'] = array (
);
$arguments645['absolute'] = false;
$arguments645['addQueryString'] = false;
$arguments645['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments645['fieldNamePrefix'] = NULL;
$arguments645['actionUri'] = NULL;
$arguments645['objectName'] = NULL;
$arguments645['useParentRequest'] = false;
$arguments645['enctype'] = NULL;
$arguments645['method'] = NULL;
$arguments645['name'] = NULL;
$arguments645['onreset'] = NULL;
$arguments645['onsubmit'] = NULL;
$arguments645['dir'] = NULL;
$arguments645['id'] = NULL;
$arguments645['lang'] = NULL;
$arguments645['style'] = NULL;
$arguments645['title'] = NULL;
$arguments645['accesskey'] = NULL;
$arguments645['tabindex'] = NULL;
$arguments645['onclick'] = NULL;
$renderChildrenClosure647 = function() use ($renderingContext, $self) {
$output648 = '';

$output648 .= '
													<button class="neos-button neos-button-warning" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments649 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments650 = array();
$arguments650['id'] = 'clickToDeactivate';
$arguments650['value'] = 'Click to deactivate';
$arguments650['arguments'] = array (
);
$arguments650['source'] = 'Main';
$arguments650['package'] = NULL;
$arguments650['quantity'] = NULL;
$arguments650['languageIdentifier'] = NULL;
$renderChildrenClosure651 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper652 = $self->getViewHelper('$viewHelper652', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper652->setArguments($arguments650);
$viewHelper652->setRenderingContext($renderingContext);
$viewHelper652->setRenderChildrenClosure($renderChildrenClosure651);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments649['value'] = $viewHelper652->initializeArgumentsAndRender();
$arguments649['keepQuotes'] = false;
$arguments649['encoding'] = 'UTF-8';
$arguments649['doubleEncode'] = true;
$renderChildrenClosure653 = function() use ($renderingContext, $self) {
return NULL;
};
$value654 = ($arguments649['value'] !== NULL ? $arguments649['value'] : $renderChildrenClosure653());

$output648 .= !is_string($value654) && !(is_object($value654) && method_exists($value654, '__toString')) ? $value654 : htmlspecialchars($value654, ($arguments649['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments649['encoding'], $arguments649['doubleEncode']);

$output648 .= '" data-neos-toggle="tooltip">
														<i class="icon-minus-sign icon-white"></i>
													</button>
												';
return $output648;
};
$viewHelper655 = $self->getViewHelper('$viewHelper655', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper655->setArguments($arguments645);
$viewHelper655->setRenderingContext($renderingContext);
$viewHelper655->setRenderChildrenClosure($renderChildrenClosure647);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output644 .= $viewHelper655->initializeArgumentsAndRender();

$output644 .= '
											';
return $output644;
};
$viewHelper656 = $self->getViewHelper('$viewHelper656', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper656->setArguments($arguments642);
$viewHelper656->setRenderingContext($renderingContext);
$viewHelper656->setRenderChildrenClosure($renderChildrenClosure643);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output641 .= $viewHelper656->initializeArgumentsAndRender();

$output641 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments657 = array();
$renderChildrenClosure658 = function() use ($renderingContext, $self) {
$output659 = '';

$output659 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments660 = array();
$arguments660['action'] = 'activateSite';
// Rendering Array
$array661 = array();
$array661['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments660['arguments'] = $array661;
$arguments660['class'] = 'neos-inline';
$arguments660['additionalAttributes'] = NULL;
$arguments660['data'] = NULL;
$arguments660['controller'] = NULL;
$arguments660['package'] = NULL;
$arguments660['subpackage'] = NULL;
$arguments660['object'] = NULL;
$arguments660['section'] = '';
$arguments660['format'] = '';
$arguments660['additionalParams'] = array (
);
$arguments660['absolute'] = false;
$arguments660['addQueryString'] = false;
$arguments660['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments660['fieldNamePrefix'] = NULL;
$arguments660['actionUri'] = NULL;
$arguments660['objectName'] = NULL;
$arguments660['useParentRequest'] = false;
$arguments660['enctype'] = NULL;
$arguments660['method'] = NULL;
$arguments660['name'] = NULL;
$arguments660['onreset'] = NULL;
$arguments660['onsubmit'] = NULL;
$arguments660['dir'] = NULL;
$arguments660['id'] = NULL;
$arguments660['lang'] = NULL;
$arguments660['style'] = NULL;
$arguments660['title'] = NULL;
$arguments660['accesskey'] = NULL;
$arguments660['tabindex'] = NULL;
$arguments660['onclick'] = NULL;
$renderChildrenClosure662 = function() use ($renderingContext, $self) {
$output663 = '';

$output663 .= '
													<button class="neos-button neos-button-success" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments664 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments665 = array();
$arguments665['id'] = 'clickToActivate';
$arguments665['value'] = 'Click to activate';
$arguments665['arguments'] = array (
);
$arguments665['source'] = 'Main';
$arguments665['package'] = NULL;
$arguments665['quantity'] = NULL;
$arguments665['languageIdentifier'] = NULL;
$renderChildrenClosure666 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper667 = $self->getViewHelper('$viewHelper667', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper667->setArguments($arguments665);
$viewHelper667->setRenderingContext($renderingContext);
$viewHelper667->setRenderChildrenClosure($renderChildrenClosure666);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments664['value'] = $viewHelper667->initializeArgumentsAndRender();
$arguments664['keepQuotes'] = false;
$arguments664['encoding'] = 'UTF-8';
$arguments664['doubleEncode'] = true;
$renderChildrenClosure668 = function() use ($renderingContext, $self) {
return NULL;
};
$value669 = ($arguments664['value'] !== NULL ? $arguments664['value'] : $renderChildrenClosure668());

$output663 .= !is_string($value669) && !(is_object($value669) && method_exists($value669, '__toString')) ? $value669 : htmlspecialchars($value669, ($arguments664['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments664['encoding'], $arguments664['doubleEncode']);

$output663 .= '" data-neos-toggle="tooltip">
														<i class="icon-plus-sign icon-white"></i>
													</button>
												';
return $output663;
};
$viewHelper670 = $self->getViewHelper('$viewHelper670', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper670->setArguments($arguments660);
$viewHelper670->setRenderingContext($renderingContext);
$viewHelper670->setRenderChildrenClosure($renderChildrenClosure662);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output659 .= $viewHelper670->initializeArgumentsAndRender();

$output659 .= '
											';
return $output659;
};
$viewHelper671 = $self->getViewHelper('$viewHelper671', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper671->setArguments($arguments657);
$viewHelper671->setRenderingContext($renderingContext);
$viewHelper671->setRenderChildrenClosure($renderChildrenClosure658);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output641 .= $viewHelper671->initializeArgumentsAndRender();

$output641 .= '
										';
return $output641;
};
$arguments639['__thenClosure'] = function() use ($renderingContext, $self) {
$output672 = '';

$output672 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments673 = array();
$arguments673['action'] = 'deactivateSite';
// Rendering Array
$array674 = array();
$array674['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments673['arguments'] = $array674;
$arguments673['class'] = 'neos-inline';
$arguments673['additionalAttributes'] = NULL;
$arguments673['data'] = NULL;
$arguments673['controller'] = NULL;
$arguments673['package'] = NULL;
$arguments673['subpackage'] = NULL;
$arguments673['object'] = NULL;
$arguments673['section'] = '';
$arguments673['format'] = '';
$arguments673['additionalParams'] = array (
);
$arguments673['absolute'] = false;
$arguments673['addQueryString'] = false;
$arguments673['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments673['fieldNamePrefix'] = NULL;
$arguments673['actionUri'] = NULL;
$arguments673['objectName'] = NULL;
$arguments673['useParentRequest'] = false;
$arguments673['enctype'] = NULL;
$arguments673['method'] = NULL;
$arguments673['name'] = NULL;
$arguments673['onreset'] = NULL;
$arguments673['onsubmit'] = NULL;
$arguments673['dir'] = NULL;
$arguments673['id'] = NULL;
$arguments673['lang'] = NULL;
$arguments673['style'] = NULL;
$arguments673['title'] = NULL;
$arguments673['accesskey'] = NULL;
$arguments673['tabindex'] = NULL;
$arguments673['onclick'] = NULL;
$renderChildrenClosure675 = function() use ($renderingContext, $self) {
$output676 = '';

$output676 .= '
													<button class="neos-button neos-button-warning" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments677 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments678 = array();
$arguments678['id'] = 'clickToDeactivate';
$arguments678['value'] = 'Click to deactivate';
$arguments678['arguments'] = array (
);
$arguments678['source'] = 'Main';
$arguments678['package'] = NULL;
$arguments678['quantity'] = NULL;
$arguments678['languageIdentifier'] = NULL;
$renderChildrenClosure679 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper680 = $self->getViewHelper('$viewHelper680', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper680->setArguments($arguments678);
$viewHelper680->setRenderingContext($renderingContext);
$viewHelper680->setRenderChildrenClosure($renderChildrenClosure679);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments677['value'] = $viewHelper680->initializeArgumentsAndRender();
$arguments677['keepQuotes'] = false;
$arguments677['encoding'] = 'UTF-8';
$arguments677['doubleEncode'] = true;
$renderChildrenClosure681 = function() use ($renderingContext, $self) {
return NULL;
};
$value682 = ($arguments677['value'] !== NULL ? $arguments677['value'] : $renderChildrenClosure681());

$output676 .= !is_string($value682) && !(is_object($value682) && method_exists($value682, '__toString')) ? $value682 : htmlspecialchars($value682, ($arguments677['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments677['encoding'], $arguments677['doubleEncode']);

$output676 .= '" data-neos-toggle="tooltip">
														<i class="icon-minus-sign icon-white"></i>
													</button>
												';
return $output676;
};
$viewHelper683 = $self->getViewHelper('$viewHelper683', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper683->setArguments($arguments673);
$viewHelper683->setRenderingContext($renderingContext);
$viewHelper683->setRenderChildrenClosure($renderChildrenClosure675);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output672 .= $viewHelper683->initializeArgumentsAndRender();

$output672 .= '
											';
return $output672;
};
$arguments639['__elseClosure'] = function() use ($renderingContext, $self) {
$output684 = '';

$output684 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments685 = array();
$arguments685['action'] = 'activateSite';
// Rendering Array
$array686 = array();
$array686['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments685['arguments'] = $array686;
$arguments685['class'] = 'neos-inline';
$arguments685['additionalAttributes'] = NULL;
$arguments685['data'] = NULL;
$arguments685['controller'] = NULL;
$arguments685['package'] = NULL;
$arguments685['subpackage'] = NULL;
$arguments685['object'] = NULL;
$arguments685['section'] = '';
$arguments685['format'] = '';
$arguments685['additionalParams'] = array (
);
$arguments685['absolute'] = false;
$arguments685['addQueryString'] = false;
$arguments685['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments685['fieldNamePrefix'] = NULL;
$arguments685['actionUri'] = NULL;
$arguments685['objectName'] = NULL;
$arguments685['useParentRequest'] = false;
$arguments685['enctype'] = NULL;
$arguments685['method'] = NULL;
$arguments685['name'] = NULL;
$arguments685['onreset'] = NULL;
$arguments685['onsubmit'] = NULL;
$arguments685['dir'] = NULL;
$arguments685['id'] = NULL;
$arguments685['lang'] = NULL;
$arguments685['style'] = NULL;
$arguments685['title'] = NULL;
$arguments685['accesskey'] = NULL;
$arguments685['tabindex'] = NULL;
$arguments685['onclick'] = NULL;
$renderChildrenClosure687 = function() use ($renderingContext, $self) {
$output688 = '';

$output688 .= '
													<button class="neos-button neos-button-success" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments689 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments690 = array();
$arguments690['id'] = 'clickToActivate';
$arguments690['value'] = 'Click to activate';
$arguments690['arguments'] = array (
);
$arguments690['source'] = 'Main';
$arguments690['package'] = NULL;
$arguments690['quantity'] = NULL;
$arguments690['languageIdentifier'] = NULL;
$renderChildrenClosure691 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper692 = $self->getViewHelper('$viewHelper692', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper692->setArguments($arguments690);
$viewHelper692->setRenderingContext($renderingContext);
$viewHelper692->setRenderChildrenClosure($renderChildrenClosure691);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments689['value'] = $viewHelper692->initializeArgumentsAndRender();
$arguments689['keepQuotes'] = false;
$arguments689['encoding'] = 'UTF-8';
$arguments689['doubleEncode'] = true;
$renderChildrenClosure693 = function() use ($renderingContext, $self) {
return NULL;
};
$value694 = ($arguments689['value'] !== NULL ? $arguments689['value'] : $renderChildrenClosure693());

$output688 .= !is_string($value694) && !(is_object($value694) && method_exists($value694, '__toString')) ? $value694 : htmlspecialchars($value694, ($arguments689['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments689['encoding'], $arguments689['doubleEncode']);

$output688 .= '" data-neos-toggle="tooltip">
														<i class="icon-plus-sign icon-white"></i>
													</button>
												';
return $output688;
};
$viewHelper695 = $self->getViewHelper('$viewHelper695', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper695->setArguments($arguments685);
$viewHelper695->setRenderingContext($renderingContext);
$viewHelper695->setRenderChildrenClosure($renderChildrenClosure687);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output684 .= $viewHelper695->initializeArgumentsAndRender();

$output684 .= '
											';
return $output684;
};
$viewHelper696 = $self->getViewHelper('$viewHelper696', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper696->setArguments($arguments639);
$viewHelper696->setRenderingContext($renderingContext);
$viewHelper696->setRenderChildrenClosure($renderChildrenClosure640);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output581 .= $viewHelper696->initializeArgumentsAndRender();

$output581 .= '
										<button class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments697 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments698 = array();
$arguments698['id'] = 'clickToDelete';
$arguments698['value'] = 'Click to delete';
$arguments698['arguments'] = array (
);
$arguments698['source'] = 'Main';
$arguments698['package'] = NULL;
$arguments698['quantity'] = NULL;
$arguments698['languageIdentifier'] = NULL;
$renderChildrenClosure699 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper700 = $self->getViewHelper('$viewHelper700', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper700->setArguments($arguments698);
$viewHelper700->setRenderingContext($renderingContext);
$viewHelper700->setRenderChildrenClosure($renderChildrenClosure699);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments697['value'] = $viewHelper700->initializeArgumentsAndRender();
$arguments697['keepQuotes'] = false;
$arguments697['encoding'] = 'UTF-8';
$arguments697['doubleEncode'] = true;
$renderChildrenClosure701 = function() use ($renderingContext, $self) {
return NULL;
};
$value702 = ($arguments697['value'] !== NULL ? $arguments697['value'] : $renderChildrenClosure701());

$output581 .= !is_string($value702) && !(is_object($value702) && method_exists($value702, '__toString')) ? $value702 : htmlspecialchars($value702, ($arguments697['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments697['encoding'], $arguments697['doubleEncode']);

$output581 .= '" data-toggle="modal" href="#';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments703 = array();
$arguments703['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments703['keepQuotes'] = false;
$arguments703['encoding'] = 'UTF-8';
$arguments703['doubleEncode'] = true;
$renderChildrenClosure704 = function() use ($renderingContext, $self) {
return NULL;
};
$value705 = ($arguments703['value'] !== NULL ? $arguments703['value'] : $renderChildrenClosure704());

$output581 .= !is_string($value705) && !(is_object($value705) && method_exists($value705, '__toString')) ? $value705 : htmlspecialchars($value705, ($arguments703['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments703['encoding'], $arguments703['doubleEncode']);

$output581 .= '" data-neos-toggle="tooltip">
											<i class="icon-trash icon-white"></i>
										</button>
										<div class="neos-hide" id="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments706 = array();
$arguments706['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments706['keepQuotes'] = false;
$arguments706['encoding'] = 'UTF-8';
$arguments706['doubleEncode'] = true;
$renderChildrenClosure707 = function() use ($renderingContext, $self) {
return NULL;
};
$value708 = ($arguments706['value'] !== NULL ? $arguments706['value'] : $renderChildrenClosure707());

$output581 .= !is_string($value708) && !(is_object($value708) && method_exists($value708, '__toString')) ? $value708 : htmlspecialchars($value708, ($arguments706['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments706['encoding'], $arguments706['doubleEncode']);

$output581 .= '">
											<div class="neos-modal-centered">
												<div class="neos-modal-content">
													<div class="neos-modal-header">
														<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
														<div class="neos-header">Do you really want to delete "';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments709 = array();
$arguments709['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments709['keepQuotes'] = false;
$arguments709['encoding'] = 'UTF-8';
$arguments709['doubleEncode'] = true;
$renderChildrenClosure710 = function() use ($renderingContext, $self) {
return NULL;
};
$value711 = ($arguments709['value'] !== NULL ? $arguments709['value'] : $renderChildrenClosure710());

$output581 .= !is_string($value711) && !(is_object($value711) && method_exists($value711, '__toString')) ? $value711 : htmlspecialchars($value711, ($arguments709['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments709['encoding'], $arguments709['doubleEncode']);

$output581 .= '"? This action cannot be undone.</div>
													</div>
													<div class="neos-modal-footer">
														<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments712 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments713 = array();
$arguments713['id'] = 'cancel';
$arguments713['value'] = 'Cancel';
$arguments713['arguments'] = array (
);
$arguments713['source'] = 'Main';
$arguments713['package'] = NULL;
$arguments713['quantity'] = NULL;
$arguments713['languageIdentifier'] = NULL;
$renderChildrenClosure714 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper715 = $self->getViewHelper('$viewHelper715', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper715->setArguments($arguments713);
$viewHelper715->setRenderingContext($renderingContext);
$viewHelper715->setRenderChildrenClosure($renderChildrenClosure714);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments712['value'] = $viewHelper715->initializeArgumentsAndRender();
$arguments712['keepQuotes'] = false;
$arguments712['encoding'] = 'UTF-8';
$arguments712['doubleEncode'] = true;
$renderChildrenClosure716 = function() use ($renderingContext, $self) {
return NULL;
};
$value717 = ($arguments712['value'] !== NULL ? $arguments712['value'] : $renderChildrenClosure716());

$output581 .= !is_string($value717) && !(is_object($value717) && method_exists($value717, '__toString')) ? $value717 : htmlspecialchars($value717, ($arguments712['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments712['encoding'], $arguments712['doubleEncode']);

$output581 .= '</a>
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments718 = array();
$arguments718['action'] = 'deleteSite';
// Rendering Array
$array719 = array();
$array719['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments718['arguments'] = $array719;
$arguments718['class'] = 'neos-inline';
$arguments718['additionalAttributes'] = NULL;
$arguments718['data'] = NULL;
$arguments718['controller'] = NULL;
$arguments718['package'] = NULL;
$arguments718['subpackage'] = NULL;
$arguments718['object'] = NULL;
$arguments718['section'] = '';
$arguments718['format'] = '';
$arguments718['additionalParams'] = array (
);
$arguments718['absolute'] = false;
$arguments718['addQueryString'] = false;
$arguments718['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments718['fieldNamePrefix'] = NULL;
$arguments718['actionUri'] = NULL;
$arguments718['objectName'] = NULL;
$arguments718['useParentRequest'] = false;
$arguments718['enctype'] = NULL;
$arguments718['method'] = NULL;
$arguments718['name'] = NULL;
$arguments718['onreset'] = NULL;
$arguments718['onsubmit'] = NULL;
$arguments718['dir'] = NULL;
$arguments718['id'] = NULL;
$arguments718['lang'] = NULL;
$arguments718['style'] = NULL;
$arguments718['title'] = NULL;
$arguments718['accesskey'] = NULL;
$arguments718['tabindex'] = NULL;
$arguments718['onclick'] = NULL;
$renderChildrenClosure720 = function() use ($renderingContext, $self) {
$output721 = '';

$output721 .= '
															<button class="neos-button neos-button-danger" title="Delete this site">
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments722 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments723 = array();
$arguments723['id'] = 'sites.confirmDelete';
$arguments723['value'] = 'Yes, delete this site';
$arguments723['arguments'] = array (
);
$arguments723['source'] = 'Main';
$arguments723['package'] = NULL;
$arguments723['quantity'] = NULL;
$arguments723['languageIdentifier'] = NULL;
$renderChildrenClosure724 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper725 = $self->getViewHelper('$viewHelper725', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper725->setArguments($arguments723);
$viewHelper725->setRenderingContext($renderingContext);
$viewHelper725->setRenderChildrenClosure($renderChildrenClosure724);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments722['value'] = $viewHelper725->initializeArgumentsAndRender();
$arguments722['keepQuotes'] = false;
$arguments722['encoding'] = 'UTF-8';
$arguments722['doubleEncode'] = true;
$renderChildrenClosure726 = function() use ($renderingContext, $self) {
return NULL;
};
$value727 = ($arguments722['value'] !== NULL ? $arguments722['value'] : $renderChildrenClosure726());

$output721 .= !is_string($value727) && !(is_object($value727) && method_exists($value727, '__toString')) ? $value727 : htmlspecialchars($value727, ($arguments722['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments722['encoding'], $arguments722['doubleEncode']);

$output721 .= '
															</button>
														';
return $output721;
};
$viewHelper728 = $self->getViewHelper('$viewHelper728', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper728->setArguments($arguments718);
$viewHelper728->setRenderingContext($renderingContext);
$viewHelper728->setRenderChildrenClosure($renderChildrenClosure720);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output581 .= $viewHelper728->initializeArgumentsAndRender();

$output581 .= '
													</div>
												</div>
											</div>
											<div class="neos-modal-backdrop neos-in"></div>
										</div>
									</div>
								</td>
							</tr>
						';
return $output581;
};

$output578 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments579, $renderChildrenClosure580, $renderingContext);

$output578 .= '
					';
return $output578;
};
$viewHelper729 = $self->getViewHelper('$viewHelper729', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper729->setArguments($arguments576);
$viewHelper729->setRenderingContext($renderingContext);
$viewHelper729->setRenderChildrenClosure($renderChildrenClosure577);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output575 .= $viewHelper729->initializeArgumentsAndRender();

$output575 .= '
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments730 = array();
$renderChildrenClosure731 = function() use ($renderingContext, $self) {
$output732 = '';

$output732 .= '
						<tr class="fold-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments733 = array();
$arguments733['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageKey', $renderingContext);
$arguments733['keepQuotes'] = false;
$arguments733['encoding'] = 'UTF-8';
$arguments733['doubleEncode'] = true;
$renderChildrenClosure734 = function() use ($renderingContext, $self) {
return NULL;
};
$value735 = ($arguments733['value'] !== NULL ? $arguments733['value'] : $renderChildrenClosure734());

$output732 .= !is_string($value735) && !(is_object($value735) && method_exists($value735, '__toString')) ? $value735 : htmlspecialchars($value735, ($arguments733['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments733['encoding'], $arguments733['doubleEncode']);

$output732 .= '">
							<td colspan="4"><i>No sites available</i></td>
						</tr>
					';
return $output732;
};
$viewHelper736 = $self->getViewHelper('$viewHelper736', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper736->setArguments($arguments730);
$viewHelper736->setRenderingContext($renderingContext);
$viewHelper736->setRenderChildrenClosure($renderChildrenClosure731);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output575 .= $viewHelper736->initializeArgumentsAndRender();

$output575 .= '
				';
return $output575;
};
$arguments573['__thenClosure'] = function() use ($renderingContext, $self) {
$output737 = '';

$output737 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments738 = array();
$arguments738['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageAndSite.sites', $renderingContext);
$arguments738['as'] = 'site';
$arguments738['key'] = '';
$arguments738['reverse'] = false;
$arguments738['iteration'] = NULL;
$renderChildrenClosure739 = function() use ($renderingContext, $self) {
$output740 = '';

$output740 .= '
							<tr class="fold-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments741 = array();
$arguments741['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageKey', $renderingContext);
$arguments741['keepQuotes'] = false;
$arguments741['encoding'] = 'UTF-8';
$arguments741['doubleEncode'] = true;
$renderChildrenClosure742 = function() use ($renderingContext, $self) {
return NULL;
};
$value743 = ($arguments741['value'] !== NULL ? $arguments741['value'] : $renderChildrenClosure742());

$output740 .= !is_string($value743) && !(is_object($value743) && method_exists($value743, '__toString')) ? $value743 : htmlspecialchars($value743, ($arguments741['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments741['encoding'], $arguments741['doubleEncode']);

$output740 .= '">
								<td>
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments744 = array();
$arguments744['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments744['keepQuotes'] = false;
$arguments744['encoding'] = 'UTF-8';
$arguments744['doubleEncode'] = true;
$renderChildrenClosure745 = function() use ($renderingContext, $self) {
return NULL;
};
$value746 = ($arguments744['value'] !== NULL ? $arguments744['value'] : $renderChildrenClosure745());

$output740 .= !is_string($value746) && !(is_object($value746) && method_exists($value746, '__toString')) ? $value746 : htmlspecialchars($value746, ($arguments744['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments744['encoding'], $arguments744['doubleEncode']);

$output740 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments747 = array();
// Rendering Boolean node
$arguments747['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.online', $renderingContext));
$arguments747['then'] = NULL;
$arguments747['else'] = NULL;
$renderChildrenClosure748 = function() use ($renderingContext, $self) {
$output749 = '';

$output749 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments750 = array();
$renderChildrenClosure751 = function() use ($renderingContext, $self) {
$output752 = '';

$output752 .= '
											<span class="neos-badge neos-badge-warning">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments753 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments754 = array();
$arguments754['id'] = 'inactive';
$arguments754['value'] = 'Inactive';
$arguments754['arguments'] = array (
);
$arguments754['source'] = 'Main';
$arguments754['package'] = NULL;
$arguments754['quantity'] = NULL;
$arguments754['languageIdentifier'] = NULL;
$renderChildrenClosure755 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper756 = $self->getViewHelper('$viewHelper756', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper756->setArguments($arguments754);
$viewHelper756->setRenderingContext($renderingContext);
$viewHelper756->setRenderChildrenClosure($renderChildrenClosure755);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments753['value'] = $viewHelper756->initializeArgumentsAndRender();
$arguments753['keepQuotes'] = false;
$arguments753['encoding'] = 'UTF-8';
$arguments753['doubleEncode'] = true;
$renderChildrenClosure757 = function() use ($renderingContext, $self) {
return NULL;
};
$value758 = ($arguments753['value'] !== NULL ? $arguments753['value'] : $renderChildrenClosure757());

$output752 .= !is_string($value758) && !(is_object($value758) && method_exists($value758, '__toString')) ? $value758 : htmlspecialchars($value758, ($arguments753['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments753['encoding'], $arguments753['doubleEncode']);

$output752 .= '</span>
										';
return $output752;
};
$viewHelper759 = $self->getViewHelper('$viewHelper759', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper759->setArguments($arguments750);
$viewHelper759->setRenderingContext($renderingContext);
$viewHelper759->setRenderChildrenClosure($renderChildrenClosure751);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output749 .= $viewHelper759->initializeArgumentsAndRender();

$output749 .= '
									';
return $output749;
};
$arguments747['__elseClosure'] = function() use ($renderingContext, $self) {
$output760 = '';

$output760 .= '
											<span class="neos-badge neos-badge-warning">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments761 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments762 = array();
$arguments762['id'] = 'inactive';
$arguments762['value'] = 'Inactive';
$arguments762['arguments'] = array (
);
$arguments762['source'] = 'Main';
$arguments762['package'] = NULL;
$arguments762['quantity'] = NULL;
$arguments762['languageIdentifier'] = NULL;
$renderChildrenClosure763 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper764 = $self->getViewHelper('$viewHelper764', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper764->setArguments($arguments762);
$viewHelper764->setRenderingContext($renderingContext);
$viewHelper764->setRenderChildrenClosure($renderChildrenClosure763);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments761['value'] = $viewHelper764->initializeArgumentsAndRender();
$arguments761['keepQuotes'] = false;
$arguments761['encoding'] = 'UTF-8';
$arguments761['doubleEncode'] = true;
$renderChildrenClosure765 = function() use ($renderingContext, $self) {
return NULL;
};
$value766 = ($arguments761['value'] !== NULL ? $arguments761['value'] : $renderChildrenClosure765());

$output760 .= !is_string($value766) && !(is_object($value766) && method_exists($value766, '__toString')) ? $value766 : htmlspecialchars($value766, ($arguments761['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments761['encoding'], $arguments761['doubleEncode']);

$output760 .= '</span>
										';
return $output760;
};
$viewHelper767 = $self->getViewHelper('$viewHelper767', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper767->setArguments($arguments747);
$viewHelper767->setRenderingContext($renderingContext);
$viewHelper767->setRenderChildrenClosure($renderChildrenClosure748);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output740 .= $viewHelper767->initializeArgumentsAndRender();

$output740 .= '
								</td>
								<td><span class="neos-label">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments768 = array();
$arguments768['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments768['keepQuotes'] = false;
$arguments768['encoding'] = 'UTF-8';
$arguments768['doubleEncode'] = true;
$renderChildrenClosure769 = function() use ($renderingContext, $self) {
return NULL;
};
$value770 = ($arguments768['value'] !== NULL ? $arguments768['value'] : $renderChildrenClosure769());

$output740 .= !is_string($value770) && !(is_object($value770) && method_exists($value770, '__toString')) ? $value770 : htmlspecialchars($value770, ($arguments768['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments768['encoding'], $arguments768['doubleEncode']);

$output740 .= '</span></td>
								<td><span class="neos-badge neos-badge-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments771 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments772 = array();
$arguments772['subject'] = NULL;
$renderChildrenClosure773 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.activeDomains', $renderingContext);
};
$viewHelper774 = $self->getViewHelper('$viewHelper774', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper774->setArguments($arguments772);
$viewHelper774->setRenderingContext($renderingContext);
$viewHelper774->setRenderChildrenClosure($renderChildrenClosure773);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments771['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', $viewHelper774->initializeArgumentsAndRender(), 0);
$arguments771['then'] = 'info';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments775 = array();
// Rendering Boolean node
$arguments775['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'multipleSites', $renderingContext));
$arguments775['then'] = 'important';
$arguments775['else'] = 'inverse';
$renderChildrenClosure776 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper777 = $self->getViewHelper('$viewHelper777', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper777->setArguments($arguments775);
$viewHelper777->setRenderingContext($renderingContext);
$viewHelper777->setRenderChildrenClosure($renderChildrenClosure776);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments771['else'] = $viewHelper777->initializeArgumentsAndRender();
$renderChildrenClosure778 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper779 = $self->getViewHelper('$viewHelper779', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper779->setArguments($arguments771);
$viewHelper779->setRenderingContext($renderingContext);
$viewHelper779->setRenderChildrenClosure($renderChildrenClosure778);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output740 .= $viewHelper779->initializeArgumentsAndRender();

$output740 .= '">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments780 = array();
$arguments780['subject'] = NULL;
$renderChildrenClosure781 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.domains', $renderingContext);
};
$viewHelper782 = $self->getViewHelper('$viewHelper782', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper782->setArguments($arguments780);
$viewHelper782->setRenderingContext($renderingContext);
$viewHelper782->setRenderChildrenClosure($renderChildrenClosure781);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper

$output740 .= $viewHelper782->initializeArgumentsAndRender();

$output740 .= '</span>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments783 = array();
// Rendering Boolean node
$arguments783['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.activeDomains', $renderingContext));
$arguments783['then'] = NULL;
$arguments783['else'] = NULL;
$renderChildrenClosure784 = function() use ($renderingContext, $self) {
$output785 = '';

$output785 .= ' <span class="neos-label neos-label-inverse">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments786 = array();
$arguments786['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.primaryDomain', $renderingContext);
$arguments786['keepQuotes'] = false;
$arguments786['encoding'] = 'UTF-8';
$arguments786['doubleEncode'] = true;
$renderChildrenClosure787 = function() use ($renderingContext, $self) {
return NULL;
};
$value788 = ($arguments786['value'] !== NULL ? $arguments786['value'] : $renderChildrenClosure787());

$output785 .= !is_string($value788) && !(is_object($value788) && method_exists($value788, '__toString')) ? $value788 : htmlspecialchars($value788, ($arguments786['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments786['encoding'], $arguments786['doubleEncode']);

$output785 .= '</span>';
return $output785;
};
$viewHelper789 = $self->getViewHelper('$viewHelper789', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper789->setArguments($arguments783);
$viewHelper789->setRenderingContext($renderingContext);
$viewHelper789->setRenderChildrenClosure($renderChildrenClosure784);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output740 .= $viewHelper789->initializeArgumentsAndRender();

$output740 .= '</td>
								<td class="neos-action">
									<div class="neos-pull-right">
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments790 = array();
$arguments790['action'] = 'edit';
// Rendering Array
$array791 = array();
$array791['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments790['arguments'] = $array791;
$arguments790['class'] = 'neos-button neos-button-primary';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments792 = array();
$arguments792['id'] = 'clickToEdit';
$arguments792['value'] = 'Click to edit';
$arguments792['arguments'] = array (
);
$arguments792['source'] = 'Main';
$arguments792['package'] = NULL;
$arguments792['quantity'] = NULL;
$arguments792['languageIdentifier'] = NULL;
$renderChildrenClosure793 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper794 = $self->getViewHelper('$viewHelper794', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper794->setArguments($arguments792);
$viewHelper794->setRenderingContext($renderingContext);
$viewHelper794->setRenderChildrenClosure($renderChildrenClosure793);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments790['title'] = $viewHelper794->initializeArgumentsAndRender();
// Rendering Array
$array795 = array();
$array795['data-neos-toggle'] = 'tooltip';
$arguments790['additionalAttributes'] = $array795;
$arguments790['data'] = NULL;
$arguments790['controller'] = NULL;
$arguments790['package'] = NULL;
$arguments790['subpackage'] = NULL;
$arguments790['section'] = '';
$arguments790['format'] = '';
$arguments790['additionalParams'] = array (
);
$arguments790['addQueryString'] = false;
$arguments790['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments790['useParentRequest'] = false;
$arguments790['absolute'] = true;
$arguments790['dir'] = NULL;
$arguments790['id'] = NULL;
$arguments790['lang'] = NULL;
$arguments790['style'] = NULL;
$arguments790['accesskey'] = NULL;
$arguments790['tabindex'] = NULL;
$arguments790['onclick'] = NULL;
$arguments790['name'] = NULL;
$arguments790['rel'] = NULL;
$arguments790['rev'] = NULL;
$arguments790['target'] = NULL;
$renderChildrenClosure796 = function() use ($renderingContext, $self) {
return '
											<i class="icon-pencil icon-white"></i>
										';
};
$viewHelper797 = $self->getViewHelper('$viewHelper797', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper797->setArguments($arguments790);
$viewHelper797->setRenderingContext($renderingContext);
$viewHelper797->setRenderChildrenClosure($renderChildrenClosure796);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output740 .= $viewHelper797->initializeArgumentsAndRender();

$output740 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments798 = array();
// Rendering Boolean node
$arguments798['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.state', $renderingContext), 1);
$arguments798['then'] = NULL;
$arguments798['else'] = NULL;
$renderChildrenClosure799 = function() use ($renderingContext, $self) {
$output800 = '';

$output800 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments801 = array();
$renderChildrenClosure802 = function() use ($renderingContext, $self) {
$output803 = '';

$output803 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments804 = array();
$arguments804['action'] = 'deactivateSite';
// Rendering Array
$array805 = array();
$array805['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments804['arguments'] = $array805;
$arguments804['class'] = 'neos-inline';
$arguments804['additionalAttributes'] = NULL;
$arguments804['data'] = NULL;
$arguments804['controller'] = NULL;
$arguments804['package'] = NULL;
$arguments804['subpackage'] = NULL;
$arguments804['object'] = NULL;
$arguments804['section'] = '';
$arguments804['format'] = '';
$arguments804['additionalParams'] = array (
);
$arguments804['absolute'] = false;
$arguments804['addQueryString'] = false;
$arguments804['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments804['fieldNamePrefix'] = NULL;
$arguments804['actionUri'] = NULL;
$arguments804['objectName'] = NULL;
$arguments804['useParentRequest'] = false;
$arguments804['enctype'] = NULL;
$arguments804['method'] = NULL;
$arguments804['name'] = NULL;
$arguments804['onreset'] = NULL;
$arguments804['onsubmit'] = NULL;
$arguments804['dir'] = NULL;
$arguments804['id'] = NULL;
$arguments804['lang'] = NULL;
$arguments804['style'] = NULL;
$arguments804['title'] = NULL;
$arguments804['accesskey'] = NULL;
$arguments804['tabindex'] = NULL;
$arguments804['onclick'] = NULL;
$renderChildrenClosure806 = function() use ($renderingContext, $self) {
$output807 = '';

$output807 .= '
													<button class="neos-button neos-button-warning" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments808 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments809 = array();
$arguments809['id'] = 'clickToDeactivate';
$arguments809['value'] = 'Click to deactivate';
$arguments809['arguments'] = array (
);
$arguments809['source'] = 'Main';
$arguments809['package'] = NULL;
$arguments809['quantity'] = NULL;
$arguments809['languageIdentifier'] = NULL;
$renderChildrenClosure810 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper811 = $self->getViewHelper('$viewHelper811', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper811->setArguments($arguments809);
$viewHelper811->setRenderingContext($renderingContext);
$viewHelper811->setRenderChildrenClosure($renderChildrenClosure810);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments808['value'] = $viewHelper811->initializeArgumentsAndRender();
$arguments808['keepQuotes'] = false;
$arguments808['encoding'] = 'UTF-8';
$arguments808['doubleEncode'] = true;
$renderChildrenClosure812 = function() use ($renderingContext, $self) {
return NULL;
};
$value813 = ($arguments808['value'] !== NULL ? $arguments808['value'] : $renderChildrenClosure812());

$output807 .= !is_string($value813) && !(is_object($value813) && method_exists($value813, '__toString')) ? $value813 : htmlspecialchars($value813, ($arguments808['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments808['encoding'], $arguments808['doubleEncode']);

$output807 .= '" data-neos-toggle="tooltip">
														<i class="icon-minus-sign icon-white"></i>
													</button>
												';
return $output807;
};
$viewHelper814 = $self->getViewHelper('$viewHelper814', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper814->setArguments($arguments804);
$viewHelper814->setRenderingContext($renderingContext);
$viewHelper814->setRenderChildrenClosure($renderChildrenClosure806);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output803 .= $viewHelper814->initializeArgumentsAndRender();

$output803 .= '
											';
return $output803;
};
$viewHelper815 = $self->getViewHelper('$viewHelper815', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper815->setArguments($arguments801);
$viewHelper815->setRenderingContext($renderingContext);
$viewHelper815->setRenderChildrenClosure($renderChildrenClosure802);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output800 .= $viewHelper815->initializeArgumentsAndRender();

$output800 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments816 = array();
$renderChildrenClosure817 = function() use ($renderingContext, $self) {
$output818 = '';

$output818 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments819 = array();
$arguments819['action'] = 'activateSite';
// Rendering Array
$array820 = array();
$array820['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments819['arguments'] = $array820;
$arguments819['class'] = 'neos-inline';
$arguments819['additionalAttributes'] = NULL;
$arguments819['data'] = NULL;
$arguments819['controller'] = NULL;
$arguments819['package'] = NULL;
$arguments819['subpackage'] = NULL;
$arguments819['object'] = NULL;
$arguments819['section'] = '';
$arguments819['format'] = '';
$arguments819['additionalParams'] = array (
);
$arguments819['absolute'] = false;
$arguments819['addQueryString'] = false;
$arguments819['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments819['fieldNamePrefix'] = NULL;
$arguments819['actionUri'] = NULL;
$arguments819['objectName'] = NULL;
$arguments819['useParentRequest'] = false;
$arguments819['enctype'] = NULL;
$arguments819['method'] = NULL;
$arguments819['name'] = NULL;
$arguments819['onreset'] = NULL;
$arguments819['onsubmit'] = NULL;
$arguments819['dir'] = NULL;
$arguments819['id'] = NULL;
$arguments819['lang'] = NULL;
$arguments819['style'] = NULL;
$arguments819['title'] = NULL;
$arguments819['accesskey'] = NULL;
$arguments819['tabindex'] = NULL;
$arguments819['onclick'] = NULL;
$renderChildrenClosure821 = function() use ($renderingContext, $self) {
$output822 = '';

$output822 .= '
													<button class="neos-button neos-button-success" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments823 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments824 = array();
$arguments824['id'] = 'clickToActivate';
$arguments824['value'] = 'Click to activate';
$arguments824['arguments'] = array (
);
$arguments824['source'] = 'Main';
$arguments824['package'] = NULL;
$arguments824['quantity'] = NULL;
$arguments824['languageIdentifier'] = NULL;
$renderChildrenClosure825 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper826 = $self->getViewHelper('$viewHelper826', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper826->setArguments($arguments824);
$viewHelper826->setRenderingContext($renderingContext);
$viewHelper826->setRenderChildrenClosure($renderChildrenClosure825);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments823['value'] = $viewHelper826->initializeArgumentsAndRender();
$arguments823['keepQuotes'] = false;
$arguments823['encoding'] = 'UTF-8';
$arguments823['doubleEncode'] = true;
$renderChildrenClosure827 = function() use ($renderingContext, $self) {
return NULL;
};
$value828 = ($arguments823['value'] !== NULL ? $arguments823['value'] : $renderChildrenClosure827());

$output822 .= !is_string($value828) && !(is_object($value828) && method_exists($value828, '__toString')) ? $value828 : htmlspecialchars($value828, ($arguments823['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments823['encoding'], $arguments823['doubleEncode']);

$output822 .= '" data-neos-toggle="tooltip">
														<i class="icon-plus-sign icon-white"></i>
													</button>
												';
return $output822;
};
$viewHelper829 = $self->getViewHelper('$viewHelper829', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper829->setArguments($arguments819);
$viewHelper829->setRenderingContext($renderingContext);
$viewHelper829->setRenderChildrenClosure($renderChildrenClosure821);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output818 .= $viewHelper829->initializeArgumentsAndRender();

$output818 .= '
											';
return $output818;
};
$viewHelper830 = $self->getViewHelper('$viewHelper830', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper830->setArguments($arguments816);
$viewHelper830->setRenderingContext($renderingContext);
$viewHelper830->setRenderChildrenClosure($renderChildrenClosure817);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output800 .= $viewHelper830->initializeArgumentsAndRender();

$output800 .= '
										';
return $output800;
};
$arguments798['__thenClosure'] = function() use ($renderingContext, $self) {
$output831 = '';

$output831 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments832 = array();
$arguments832['action'] = 'deactivateSite';
// Rendering Array
$array833 = array();
$array833['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments832['arguments'] = $array833;
$arguments832['class'] = 'neos-inline';
$arguments832['additionalAttributes'] = NULL;
$arguments832['data'] = NULL;
$arguments832['controller'] = NULL;
$arguments832['package'] = NULL;
$arguments832['subpackage'] = NULL;
$arguments832['object'] = NULL;
$arguments832['section'] = '';
$arguments832['format'] = '';
$arguments832['additionalParams'] = array (
);
$arguments832['absolute'] = false;
$arguments832['addQueryString'] = false;
$arguments832['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments832['fieldNamePrefix'] = NULL;
$arguments832['actionUri'] = NULL;
$arguments832['objectName'] = NULL;
$arguments832['useParentRequest'] = false;
$arguments832['enctype'] = NULL;
$arguments832['method'] = NULL;
$arguments832['name'] = NULL;
$arguments832['onreset'] = NULL;
$arguments832['onsubmit'] = NULL;
$arguments832['dir'] = NULL;
$arguments832['id'] = NULL;
$arguments832['lang'] = NULL;
$arguments832['style'] = NULL;
$arguments832['title'] = NULL;
$arguments832['accesskey'] = NULL;
$arguments832['tabindex'] = NULL;
$arguments832['onclick'] = NULL;
$renderChildrenClosure834 = function() use ($renderingContext, $self) {
$output835 = '';

$output835 .= '
													<button class="neos-button neos-button-warning" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments836 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments837 = array();
$arguments837['id'] = 'clickToDeactivate';
$arguments837['value'] = 'Click to deactivate';
$arguments837['arguments'] = array (
);
$arguments837['source'] = 'Main';
$arguments837['package'] = NULL;
$arguments837['quantity'] = NULL;
$arguments837['languageIdentifier'] = NULL;
$renderChildrenClosure838 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper839 = $self->getViewHelper('$viewHelper839', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper839->setArguments($arguments837);
$viewHelper839->setRenderingContext($renderingContext);
$viewHelper839->setRenderChildrenClosure($renderChildrenClosure838);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments836['value'] = $viewHelper839->initializeArgumentsAndRender();
$arguments836['keepQuotes'] = false;
$arguments836['encoding'] = 'UTF-8';
$arguments836['doubleEncode'] = true;
$renderChildrenClosure840 = function() use ($renderingContext, $self) {
return NULL;
};
$value841 = ($arguments836['value'] !== NULL ? $arguments836['value'] : $renderChildrenClosure840());

$output835 .= !is_string($value841) && !(is_object($value841) && method_exists($value841, '__toString')) ? $value841 : htmlspecialchars($value841, ($arguments836['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments836['encoding'], $arguments836['doubleEncode']);

$output835 .= '" data-neos-toggle="tooltip">
														<i class="icon-minus-sign icon-white"></i>
													</button>
												';
return $output835;
};
$viewHelper842 = $self->getViewHelper('$viewHelper842', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper842->setArguments($arguments832);
$viewHelper842->setRenderingContext($renderingContext);
$viewHelper842->setRenderChildrenClosure($renderChildrenClosure834);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output831 .= $viewHelper842->initializeArgumentsAndRender();

$output831 .= '
											';
return $output831;
};
$arguments798['__elseClosure'] = function() use ($renderingContext, $self) {
$output843 = '';

$output843 .= '
												';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments844 = array();
$arguments844['action'] = 'activateSite';
// Rendering Array
$array845 = array();
$array845['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments844['arguments'] = $array845;
$arguments844['class'] = 'neos-inline';
$arguments844['additionalAttributes'] = NULL;
$arguments844['data'] = NULL;
$arguments844['controller'] = NULL;
$arguments844['package'] = NULL;
$arguments844['subpackage'] = NULL;
$arguments844['object'] = NULL;
$arguments844['section'] = '';
$arguments844['format'] = '';
$arguments844['additionalParams'] = array (
);
$arguments844['absolute'] = false;
$arguments844['addQueryString'] = false;
$arguments844['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments844['fieldNamePrefix'] = NULL;
$arguments844['actionUri'] = NULL;
$arguments844['objectName'] = NULL;
$arguments844['useParentRequest'] = false;
$arguments844['enctype'] = NULL;
$arguments844['method'] = NULL;
$arguments844['name'] = NULL;
$arguments844['onreset'] = NULL;
$arguments844['onsubmit'] = NULL;
$arguments844['dir'] = NULL;
$arguments844['id'] = NULL;
$arguments844['lang'] = NULL;
$arguments844['style'] = NULL;
$arguments844['title'] = NULL;
$arguments844['accesskey'] = NULL;
$arguments844['tabindex'] = NULL;
$arguments844['onclick'] = NULL;
$renderChildrenClosure846 = function() use ($renderingContext, $self) {
$output847 = '';

$output847 .= '
													<button class="neos-button neos-button-success" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments848 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments849 = array();
$arguments849['id'] = 'clickToActivate';
$arguments849['value'] = 'Click to activate';
$arguments849['arguments'] = array (
);
$arguments849['source'] = 'Main';
$arguments849['package'] = NULL;
$arguments849['quantity'] = NULL;
$arguments849['languageIdentifier'] = NULL;
$renderChildrenClosure850 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper851 = $self->getViewHelper('$viewHelper851', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper851->setArguments($arguments849);
$viewHelper851->setRenderingContext($renderingContext);
$viewHelper851->setRenderChildrenClosure($renderChildrenClosure850);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments848['value'] = $viewHelper851->initializeArgumentsAndRender();
$arguments848['keepQuotes'] = false;
$arguments848['encoding'] = 'UTF-8';
$arguments848['doubleEncode'] = true;
$renderChildrenClosure852 = function() use ($renderingContext, $self) {
return NULL;
};
$value853 = ($arguments848['value'] !== NULL ? $arguments848['value'] : $renderChildrenClosure852());

$output847 .= !is_string($value853) && !(is_object($value853) && method_exists($value853, '__toString')) ? $value853 : htmlspecialchars($value853, ($arguments848['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments848['encoding'], $arguments848['doubleEncode']);

$output847 .= '" data-neos-toggle="tooltip">
														<i class="icon-plus-sign icon-white"></i>
													</button>
												';
return $output847;
};
$viewHelper854 = $self->getViewHelper('$viewHelper854', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper854->setArguments($arguments844);
$viewHelper854->setRenderingContext($renderingContext);
$viewHelper854->setRenderChildrenClosure($renderChildrenClosure846);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output843 .= $viewHelper854->initializeArgumentsAndRender();

$output843 .= '
											';
return $output843;
};
$viewHelper855 = $self->getViewHelper('$viewHelper855', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper855->setArguments($arguments798);
$viewHelper855->setRenderingContext($renderingContext);
$viewHelper855->setRenderChildrenClosure($renderChildrenClosure799);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output740 .= $viewHelper855->initializeArgumentsAndRender();

$output740 .= '
										<button class="neos-button neos-button-danger" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments856 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments857 = array();
$arguments857['id'] = 'clickToDelete';
$arguments857['value'] = 'Click to delete';
$arguments857['arguments'] = array (
);
$arguments857['source'] = 'Main';
$arguments857['package'] = NULL;
$arguments857['quantity'] = NULL;
$arguments857['languageIdentifier'] = NULL;
$renderChildrenClosure858 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper859 = $self->getViewHelper('$viewHelper859', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper859->setArguments($arguments857);
$viewHelper859->setRenderingContext($renderingContext);
$viewHelper859->setRenderChildrenClosure($renderChildrenClosure858);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments856['value'] = $viewHelper859->initializeArgumentsAndRender();
$arguments856['keepQuotes'] = false;
$arguments856['encoding'] = 'UTF-8';
$arguments856['doubleEncode'] = true;
$renderChildrenClosure860 = function() use ($renderingContext, $self) {
return NULL;
};
$value861 = ($arguments856['value'] !== NULL ? $arguments856['value'] : $renderChildrenClosure860());

$output740 .= !is_string($value861) && !(is_object($value861) && method_exists($value861, '__toString')) ? $value861 : htmlspecialchars($value861, ($arguments856['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments856['encoding'], $arguments856['doubleEncode']);

$output740 .= '" data-toggle="modal" href="#';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments862 = array();
$arguments862['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments862['keepQuotes'] = false;
$arguments862['encoding'] = 'UTF-8';
$arguments862['doubleEncode'] = true;
$renderChildrenClosure863 = function() use ($renderingContext, $self) {
return NULL;
};
$value864 = ($arguments862['value'] !== NULL ? $arguments862['value'] : $renderChildrenClosure863());

$output740 .= !is_string($value864) && !(is_object($value864) && method_exists($value864, '__toString')) ? $value864 : htmlspecialchars($value864, ($arguments862['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments862['encoding'], $arguments862['doubleEncode']);

$output740 .= '" data-neos-toggle="tooltip">
											<i class="icon-trash icon-white"></i>
										</button>
										<div class="neos-hide" id="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments865 = array();
$arguments865['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments865['keepQuotes'] = false;
$arguments865['encoding'] = 'UTF-8';
$arguments865['doubleEncode'] = true;
$renderChildrenClosure866 = function() use ($renderingContext, $self) {
return NULL;
};
$value867 = ($arguments865['value'] !== NULL ? $arguments865['value'] : $renderChildrenClosure866());

$output740 .= !is_string($value867) && !(is_object($value867) && method_exists($value867, '__toString')) ? $value867 : htmlspecialchars($value867, ($arguments865['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments865['encoding'], $arguments865['doubleEncode']);

$output740 .= '">
											<div class="neos-modal-centered">
												<div class="neos-modal-content">
													<div class="neos-modal-header">
														<button type="button" class="neos-close neos-button" data-dismiss="modal"></button>
														<div class="neos-header">Do you really want to delete "';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments868 = array();
$arguments868['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments868['keepQuotes'] = false;
$arguments868['encoding'] = 'UTF-8';
$arguments868['doubleEncode'] = true;
$renderChildrenClosure869 = function() use ($renderingContext, $self) {
return NULL;
};
$value870 = ($arguments868['value'] !== NULL ? $arguments868['value'] : $renderChildrenClosure869());

$output740 .= !is_string($value870) && !(is_object($value870) && method_exists($value870, '__toString')) ? $value870 : htmlspecialchars($value870, ($arguments868['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments868['encoding'], $arguments868['doubleEncode']);

$output740 .= '"? This action cannot be undone.</div>
													</div>
													<div class="neos-modal-footer">
														<a href="#" class="neos-button" data-dismiss="modal">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments871 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments872 = array();
$arguments872['id'] = 'cancel';
$arguments872['value'] = 'Cancel';
$arguments872['arguments'] = array (
);
$arguments872['source'] = 'Main';
$arguments872['package'] = NULL;
$arguments872['quantity'] = NULL;
$arguments872['languageIdentifier'] = NULL;
$renderChildrenClosure873 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper874 = $self->getViewHelper('$viewHelper874', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper874->setArguments($arguments872);
$viewHelper874->setRenderingContext($renderingContext);
$viewHelper874->setRenderChildrenClosure($renderChildrenClosure873);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments871['value'] = $viewHelper874->initializeArgumentsAndRender();
$arguments871['keepQuotes'] = false;
$arguments871['encoding'] = 'UTF-8';
$arguments871['doubleEncode'] = true;
$renderChildrenClosure875 = function() use ($renderingContext, $self) {
return NULL;
};
$value876 = ($arguments871['value'] !== NULL ? $arguments871['value'] : $renderChildrenClosure875());

$output740 .= !is_string($value876) && !(is_object($value876) && method_exists($value876, '__toString')) ? $value876 : htmlspecialchars($value876, ($arguments871['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments871['encoding'], $arguments871['doubleEncode']);

$output740 .= '</a>
														';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments877 = array();
$arguments877['action'] = 'deleteSite';
// Rendering Array
$array878 = array();
$array878['site'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site', $renderingContext);
$arguments877['arguments'] = $array878;
$arguments877['class'] = 'neos-inline';
$arguments877['additionalAttributes'] = NULL;
$arguments877['data'] = NULL;
$arguments877['controller'] = NULL;
$arguments877['package'] = NULL;
$arguments877['subpackage'] = NULL;
$arguments877['object'] = NULL;
$arguments877['section'] = '';
$arguments877['format'] = '';
$arguments877['additionalParams'] = array (
);
$arguments877['absolute'] = false;
$arguments877['addQueryString'] = false;
$arguments877['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments877['fieldNamePrefix'] = NULL;
$arguments877['actionUri'] = NULL;
$arguments877['objectName'] = NULL;
$arguments877['useParentRequest'] = false;
$arguments877['enctype'] = NULL;
$arguments877['method'] = NULL;
$arguments877['name'] = NULL;
$arguments877['onreset'] = NULL;
$arguments877['onsubmit'] = NULL;
$arguments877['dir'] = NULL;
$arguments877['id'] = NULL;
$arguments877['lang'] = NULL;
$arguments877['style'] = NULL;
$arguments877['title'] = NULL;
$arguments877['accesskey'] = NULL;
$arguments877['tabindex'] = NULL;
$arguments877['onclick'] = NULL;
$renderChildrenClosure879 = function() use ($renderingContext, $self) {
$output880 = '';

$output880 .= '
															<button class="neos-button neos-button-danger" title="Delete this site">
																';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments881 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments882 = array();
$arguments882['id'] = 'sites.confirmDelete';
$arguments882['value'] = 'Yes, delete this site';
$arguments882['arguments'] = array (
);
$arguments882['source'] = 'Main';
$arguments882['package'] = NULL;
$arguments882['quantity'] = NULL;
$arguments882['languageIdentifier'] = NULL;
$renderChildrenClosure883 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper884 = $self->getViewHelper('$viewHelper884', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper884->setArguments($arguments882);
$viewHelper884->setRenderingContext($renderingContext);
$viewHelper884->setRenderChildrenClosure($renderChildrenClosure883);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments881['value'] = $viewHelper884->initializeArgumentsAndRender();
$arguments881['keepQuotes'] = false;
$arguments881['encoding'] = 'UTF-8';
$arguments881['doubleEncode'] = true;
$renderChildrenClosure885 = function() use ($renderingContext, $self) {
return NULL;
};
$value886 = ($arguments881['value'] !== NULL ? $arguments881['value'] : $renderChildrenClosure885());

$output880 .= !is_string($value886) && !(is_object($value886) && method_exists($value886, '__toString')) ? $value886 : htmlspecialchars($value886, ($arguments881['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments881['encoding'], $arguments881['doubleEncode']);

$output880 .= '
															</button>
														';
return $output880;
};
$viewHelper887 = $self->getViewHelper('$viewHelper887', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper887->setArguments($arguments877);
$viewHelper887->setRenderingContext($renderingContext);
$viewHelper887->setRenderChildrenClosure($renderChildrenClosure879);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output740 .= $viewHelper887->initializeArgumentsAndRender();

$output740 .= '
													</div>
												</div>
											</div>
											<div class="neos-modal-backdrop neos-in"></div>
										</div>
									</div>
								</td>
							</tr>
						';
return $output740;
};

$output737 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments738, $renderChildrenClosure739, $renderingContext);

$output737 .= '
					';
return $output737;
};
$arguments573['__elseClosure'] = function() use ($renderingContext, $self) {
$output888 = '';

$output888 .= '
						<tr class="fold-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments889 = array();
$arguments889['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sitePackageKey', $renderingContext);
$arguments889['keepQuotes'] = false;
$arguments889['encoding'] = 'UTF-8';
$arguments889['doubleEncode'] = true;
$renderChildrenClosure890 = function() use ($renderingContext, $self) {
return NULL;
};
$value891 = ($arguments889['value'] !== NULL ? $arguments889['value'] : $renderChildrenClosure890());

$output888 .= !is_string($value891) && !(is_object($value891) && method_exists($value891, '__toString')) ? $value891 : htmlspecialchars($value891, ($arguments889['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments889['encoding'], $arguments889['doubleEncode']);

$output888 .= '">
							<td colspan="4"><i>No sites available</i></td>
						</tr>
					';
return $output888;
};
$viewHelper892 = $self->getViewHelper('$viewHelper892', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper892->setArguments($arguments573);
$viewHelper892->setRenderingContext($renderingContext);
$viewHelper892->setRenderChildrenClosure($renderChildrenClosure574);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output478 .= $viewHelper892->initializeArgumentsAndRender();

$output478 .= '
			';
return $output478;
};

$output457 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments476, $renderChildrenClosure477, $renderingContext);

$output457 .= '
		</tbody>
	</table>
	<div class="neos-footer">
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments893 = array();
$arguments893['action'] = 'newSite';
$arguments893['class'] = 'neos-button neos-button-primary';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments894 = array();
$arguments894['id'] = 'clickToCreate';
$arguments894['value'] = 'Click to create new';
$arguments894['arguments'] = array (
);
$arguments894['source'] = 'Main';
$arguments894['package'] = NULL;
$arguments894['quantity'] = NULL;
$arguments894['languageIdentifier'] = NULL;
$renderChildrenClosure895 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper896 = $self->getViewHelper('$viewHelper896', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper896->setArguments($arguments894);
$viewHelper896->setRenderingContext($renderingContext);
$viewHelper896->setRenderChildrenClosure($renderChildrenClosure895);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments893['title'] = $viewHelper896->initializeArgumentsAndRender();
$arguments893['additionalAttributes'] = NULL;
$arguments893['data'] = NULL;
$arguments893['arguments'] = array (
);
$arguments893['controller'] = NULL;
$arguments893['package'] = NULL;
$arguments893['subpackage'] = NULL;
$arguments893['section'] = '';
$arguments893['format'] = '';
$arguments893['additionalParams'] = array (
);
$arguments893['addQueryString'] = false;
$arguments893['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments893['useParentRequest'] = false;
$arguments893['absolute'] = true;
$arguments893['dir'] = NULL;
$arguments893['id'] = NULL;
$arguments893['lang'] = NULL;
$arguments893['style'] = NULL;
$arguments893['accesskey'] = NULL;
$arguments893['tabindex'] = NULL;
$arguments893['onclick'] = NULL;
$arguments893['name'] = NULL;
$arguments893['rel'] = NULL;
$arguments893['rev'] = NULL;
$arguments893['target'] = NULL;
$renderChildrenClosure897 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments898 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments899 = array();
$arguments899['id'] = 'sites.add';
$arguments899['value'] = 'Add new site';
$arguments899['arguments'] = array (
);
$arguments899['source'] = 'Main';
$arguments899['package'] = NULL;
$arguments899['quantity'] = NULL;
$arguments899['languageIdentifier'] = NULL;
$renderChildrenClosure900 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper901 = $self->getViewHelper('$viewHelper901', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper901->setArguments($arguments899);
$viewHelper901->setRenderingContext($renderingContext);
$viewHelper901->setRenderChildrenClosure($renderChildrenClosure900);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments898['value'] = $viewHelper901->initializeArgumentsAndRender();
$arguments898['keepQuotes'] = false;
$arguments898['encoding'] = 'UTF-8';
$arguments898['doubleEncode'] = true;
$renderChildrenClosure902 = function() use ($renderingContext, $self) {
return NULL;
};
$value903 = ($arguments898['value'] !== NULL ? $arguments898['value'] : $renderChildrenClosure902());
return !is_string($value903) && !(is_object($value903) && method_exists($value903, '__toString')) ? $value903 : htmlspecialchars($value903, ($arguments898['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments898['encoding'], $arguments898['doubleEncode']);
};
$viewHelper904 = $self->getViewHelper('$viewHelper904', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper904->setArguments($arguments893);
$viewHelper904->setRenderingContext($renderingContext);
$viewHelper904->setRenderChildrenClosure($renderChildrenClosure897);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output457 .= $viewHelper904->initializeArgumentsAndRender();

$output457 .= '
	</div>

	<script>
		(function($) {
			$(\'.fold-toggle\').click(function() {
				$(this).toggleClass(\'icon-chevron-down icon-chevron-up\');
				$(\'tr.\' + $(this).data(\'toggle\')).toggle();
			});
		})(jQuery);
	</script>
';
return $output457;
};

$output448 .= '';

$output448 .= '
';

return $output448;
}


}
#0             261417    