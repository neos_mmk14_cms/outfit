<?php class FluidCache_TYPO3_Neos_partial_Backend_UserMenu_c57fc750a838fb0406fad607845a646e233ef912 extends \TYPO3\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// TODO
	return new \TYPO3\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;

return NULL;
}
public function hasLayout() {
return FALSE;
}

/**
 * Main Render function
 */
public function render(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output0 = '';

$output0 .= '
<div class="neos-button-group neos-user-menu">
	<button class="neos-dropdown-toggle neos-button" data-neos-toggle="dropdown" href="#" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments2 = array();
$arguments2['id'] = 'userSettings.usermenu';
$arguments2['source'] = 'Modules';
$arguments2['value'] = 'User Menu';
$arguments2['arguments'] = array (
);
$arguments2['package'] = NULL;
$arguments2['quantity'] = NULL;
$arguments2['languageIdentifier'] = NULL;
$renderChildrenClosure3 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper4 = $self->getViewHelper('$viewHelper4', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper4->setArguments($arguments2);
$viewHelper4->setRenderingContext($renderingContext);
$viewHelper4->setRenderChildrenClosure($renderChildrenClosure3);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1['value'] = $viewHelper4->initializeArgumentsAndRender();
$arguments1['keepQuotes'] = false;
$arguments1['encoding'] = 'UTF-8';
$arguments1['doubleEncode'] = true;
$renderChildrenClosure5 = function() use ($renderingContext, $self) {
return NULL;
};
$value6 = ($arguments1['value'] !== NULL ? $arguments1['value'] : $renderChildrenClosure5());

$output0 .= !is_string($value6) && !(is_object($value6) && method_exists($value6, '__toString')) ? $value6 : htmlspecialchars($value6, ($arguments1['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1['encoding'], $arguments1['doubleEncode']);

$output0 .= '" data-neos-tooltip data-placement="left">
		<i class="icon-user"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments7 = array();
$arguments7['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'user.name.fullName', $renderingContext);
$arguments7['keepQuotes'] = false;
$arguments7['encoding'] = 'UTF-8';
$arguments7['doubleEncode'] = true;
$renderChildrenClosure8 = function() use ($renderingContext, $self) {
return NULL;
};
$value9 = ($arguments7['value'] !== NULL ? $arguments7['value'] : $renderChildrenClosure8());

$output0 .= !is_string($value9) && !(is_object($value9) && method_exists($value9, '__toString')) ? $value9 : htmlspecialchars($value9, ($arguments7['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments7['encoding'], $arguments7['doubleEncode']);

$output0 .= ' <i class="icon-caret-down"></i>
	</button>
	<ul class="neos-dropdown-menu" role="menu" aria-labelledby="dLabel">
		<li>
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments10 = array();
$arguments10['controller'] = 'Login';
$arguments10['action'] = 'logout';
$arguments10['additionalAttributes'] = NULL;
$arguments10['data'] = NULL;
$arguments10['arguments'] = array (
);
$arguments10['package'] = NULL;
$arguments10['subpackage'] = NULL;
$arguments10['object'] = NULL;
$arguments10['section'] = '';
$arguments10['format'] = '';
$arguments10['additionalParams'] = array (
);
$arguments10['absolute'] = false;
$arguments10['addQueryString'] = false;
$arguments10['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments10['fieldNamePrefix'] = NULL;
$arguments10['actionUri'] = NULL;
$arguments10['objectName'] = NULL;
$arguments10['useParentRequest'] = false;
$arguments10['enctype'] = NULL;
$arguments10['method'] = NULL;
$arguments10['name'] = NULL;
$arguments10['onreset'] = NULL;
$arguments10['onsubmit'] = NULL;
$arguments10['class'] = NULL;
$arguments10['dir'] = NULL;
$arguments10['id'] = NULL;
$arguments10['lang'] = NULL;
$arguments10['style'] = NULL;
$arguments10['title'] = NULL;
$arguments10['accesskey'] = NULL;
$arguments10['tabindex'] = NULL;
$arguments10['onclick'] = NULL;
$renderChildrenClosure11 = function() use ($renderingContext, $self) {
$output12 = '';

$output12 .= '
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\ButtonViewHelper
$arguments13 = array();
$arguments13['value'] = 'logout';
$arguments13['additionalAttributes'] = NULL;
$arguments13['data'] = NULL;
$arguments13['type'] = 'submit';
$arguments13['name'] = NULL;
$arguments13['property'] = NULL;
$arguments13['autofocus'] = NULL;
$arguments13['disabled'] = NULL;
$arguments13['form'] = NULL;
$arguments13['formaction'] = NULL;
$arguments13['formenctype'] = NULL;
$arguments13['formmethod'] = NULL;
$arguments13['formnovalidate'] = NULL;
$arguments13['formtarget'] = NULL;
$arguments13['class'] = NULL;
$arguments13['dir'] = NULL;
$arguments13['id'] = NULL;
$arguments13['lang'] = NULL;
$arguments13['style'] = NULL;
$arguments13['title'] = NULL;
$arguments13['accesskey'] = NULL;
$arguments13['tabindex'] = NULL;
$arguments13['onclick'] = NULL;
$renderChildrenClosure14 = function() use ($renderingContext, $self) {
$output15 = '';

$output15 .= '<i class="icon-sign-out"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments16 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments17 = array();
$arguments17['id'] = 'logout';
$arguments17['value'] = 'Logout';
$arguments17['arguments'] = array (
);
$arguments17['source'] = 'Main';
$arguments17['package'] = NULL;
$arguments17['quantity'] = NULL;
$arguments17['languageIdentifier'] = NULL;
$renderChildrenClosure18 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper19 = $self->getViewHelper('$viewHelper19', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper19->setArguments($arguments17);
$viewHelper19->setRenderingContext($renderingContext);
$viewHelper19->setRenderChildrenClosure($renderChildrenClosure18);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments16['value'] = $viewHelper19->initializeArgumentsAndRender();
$arguments16['keepQuotes'] = false;
$arguments16['encoding'] = 'UTF-8';
$arguments16['doubleEncode'] = true;
$renderChildrenClosure20 = function() use ($renderingContext, $self) {
return NULL;
};
$value21 = ($arguments16['value'] !== NULL ? $arguments16['value'] : $renderChildrenClosure20());

$output15 .= !is_string($value21) && !(is_object($value21) && method_exists($value21, '__toString')) ? $value21 : htmlspecialchars($value21, ($arguments16['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments16['encoding'], $arguments16['doubleEncode']);
return $output15;
};
$viewHelper22 = $self->getViewHelper('$viewHelper22', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\ButtonViewHelper');
$viewHelper22->setArguments($arguments13);
$viewHelper22->setRenderingContext($renderingContext);
$viewHelper22->setRenderChildrenClosure($renderChildrenClosure14);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\ButtonViewHelper

$output12 .= $viewHelper22->initializeArgumentsAndRender();

$output12 .= '
			';
return $output12;
};
$viewHelper23 = $self->getViewHelper('$viewHelper23', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper23->setArguments($arguments10);
$viewHelper23->setRenderingContext($renderingContext);
$viewHelper23->setRenderChildrenClosure($renderChildrenClosure11);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output0 .= $viewHelper23->initializeArgumentsAndRender();

$output0 .= '
		</li>
		<li>
			';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper
$arguments24 = array();
$arguments24['path'] = 'user/usersettings';
$arguments24['additionalAttributes'] = NULL;
$arguments24['data'] = NULL;
$arguments24['action'] = NULL;
$arguments24['arguments'] = array (
);
$arguments24['section'] = '';
$arguments24['format'] = '';
$arguments24['additionalParams'] = array (
);
$arguments24['addQueryString'] = false;
$arguments24['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments24['class'] = NULL;
$arguments24['dir'] = NULL;
$arguments24['id'] = NULL;
$arguments24['lang'] = NULL;
$arguments24['style'] = NULL;
$arguments24['title'] = NULL;
$arguments24['accesskey'] = NULL;
$arguments24['tabindex'] = NULL;
$arguments24['onclick'] = NULL;
$arguments24['name'] = NULL;
$arguments24['rel'] = NULL;
$arguments24['rev'] = NULL;
$arguments24['target'] = NULL;
$renderChildrenClosure25 = function() use ($renderingContext, $self) {
$output26 = '';

$output26 .= '
				<i class="icon-sliders"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments27 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments28 = array();
$arguments28['id'] = 'userSettings.label';
$arguments28['source'] = 'Modules';
$arguments28['value'] = 'User Settings';
$arguments28['arguments'] = array (
);
$arguments28['package'] = NULL;
$arguments28['quantity'] = NULL;
$arguments28['languageIdentifier'] = NULL;
$renderChildrenClosure29 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper30 = $self->getViewHelper('$viewHelper30', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper30->setArguments($arguments28);
$viewHelper30->setRenderingContext($renderingContext);
$viewHelper30->setRenderChildrenClosure($renderChildrenClosure29);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments27['value'] = $viewHelper30->initializeArgumentsAndRender();
$arguments27['keepQuotes'] = false;
$arguments27['encoding'] = 'UTF-8';
$arguments27['doubleEncode'] = true;
$renderChildrenClosure31 = function() use ($renderingContext, $self) {
return NULL;
};
$value32 = ($arguments27['value'] !== NULL ? $arguments27['value'] : $renderChildrenClosure31());

$output26 .= !is_string($value32) && !(is_object($value32) && method_exists($value32, '__toString')) ? $value32 : htmlspecialchars($value32, ($arguments27['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments27['encoding'], $arguments27['doubleEncode']);

$output26 .= '
			';
return $output26;
};
$viewHelper33 = $self->getViewHelper('$viewHelper33', $renderingContext, 'TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper');
$viewHelper33->setArguments($arguments24);
$viewHelper33->setRenderingContext($renderingContext);
$viewHelper33->setRenderChildrenClosure($renderChildrenClosure25);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper

$output0 .= $viewHelper33->initializeArgumentsAndRender();

$output0 .= '
		</li>
	</ul>
</div>
';

return $output0;
}


}
#0             11168     