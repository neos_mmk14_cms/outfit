<?php class FluidCache_TYPO3_Neos__Module_Administration_Configuration_action_index_2918f47472dc5be463c1cb671b94e47a611568b5 extends \TYPO3\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// TODO
	return new \TYPO3\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;

return 'BackendSubModule';
}
public function hasLayout() {
return TRUE;
}

/**
 * section content
 */
public function section_040f06fd774092478d450774f5ba30c5da78acc8(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output0 = '';

$output0 .= '
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments1 = array();
$arguments1['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'availableConfigurationTypes', $renderingContext);
$arguments1['as'] = 'availableConfigurationType';
$arguments1['key'] = '';
$arguments1['reverse'] = false;
$arguments1['iteration'] = NULL;
$renderChildrenClosure2 = function() use ($renderingContext, $self) {
$output3 = '';

$output3 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments4 = array();
$arguments4['action'] = 'index';
// Rendering Array
$array5 = array();
$array5['type'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'availableConfigurationType', $renderingContext);
$arguments4['arguments'] = $array5;
$output6 = '';

$output6 .= 'neos-button';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments7 = array();
// Rendering Boolean node
$arguments7['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'availableConfigurationType', $renderingContext), \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'type', $renderingContext));
$arguments7['then'] = ' neos-active';
$arguments7['else'] = NULL;
$renderChildrenClosure8 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper9 = $self->getViewHelper('$viewHelper9', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper9->setArguments($arguments7);
$viewHelper9->setRenderingContext($renderingContext);
$viewHelper9->setRenderChildrenClosure($renderChildrenClosure8);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output6 .= $viewHelper9->initializeArgumentsAndRender();
$arguments4['class'] = $output6;
$arguments4['additionalAttributes'] = NULL;
$arguments4['data'] = NULL;
$arguments4['controller'] = NULL;
$arguments4['package'] = NULL;
$arguments4['subpackage'] = NULL;
$arguments4['section'] = '';
$arguments4['format'] = '';
$arguments4['additionalParams'] = array (
);
$arguments4['addQueryString'] = false;
$arguments4['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments4['useParentRequest'] = false;
$arguments4['absolute'] = true;
$arguments4['dir'] = NULL;
$arguments4['id'] = NULL;
$arguments4['lang'] = NULL;
$arguments4['style'] = NULL;
$arguments4['title'] = NULL;
$arguments4['accesskey'] = NULL;
$arguments4['tabindex'] = NULL;
$arguments4['onclick'] = NULL;
$arguments4['name'] = NULL;
$arguments4['rel'] = NULL;
$arguments4['rev'] = NULL;
$arguments4['target'] = NULL;
$renderChildrenClosure10 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments11 = array();
$arguments11['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'availableConfigurationType', $renderingContext);
$arguments11['keepQuotes'] = false;
$arguments11['encoding'] = 'UTF-8';
$arguments11['doubleEncode'] = true;
$renderChildrenClosure12 = function() use ($renderingContext, $self) {
return NULL;
};
$value13 = ($arguments11['value'] !== NULL ? $arguments11['value'] : $renderChildrenClosure12());
return !is_string($value13) && !(is_object($value13) && method_exists($value13, '__toString')) ? $value13 : htmlspecialchars($value13, ($arguments11['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments11['encoding'], $arguments11['doubleEncode']);
};
$viewHelper14 = $self->getViewHelper('$viewHelper14', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper14->setArguments($arguments4);
$viewHelper14->setRenderingContext($renderingContext);
$viewHelper14->setRenderChildrenClosure($renderChildrenClosure10);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output3 .= $viewHelper14->initializeArgumentsAndRender();

$output3 .= '
	';
return $output3;
};

$output0 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments1, $renderChildrenClosure2, $renderingContext);

$output0 .= '
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments15 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments16 = array();
$arguments16['subject'] = NULL;
$renderChildrenClosure17 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'validationResult.flattenedErrors', $renderingContext);
};
$viewHelper18 = $self->getViewHelper('$viewHelper18', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper18->setArguments($arguments16);
$viewHelper18->setRenderingContext($renderingContext);
$viewHelper18->setRenderChildrenClosure($renderChildrenClosure17);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments15['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', $viewHelper18->initializeArgumentsAndRender(), 0);
$arguments15['then'] = NULL;
$arguments15['else'] = NULL;
$renderChildrenClosure19 = function() use ($renderingContext, $self) {
$output20 = '';

$output20 .= '
		<ul id="neos-notifications-inline">
			<li data-type="warning" data-title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments21 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments22 = array();
$arguments22['id'] = 'numberValidationErrors';
$arguments22['source'] = 'ValidationErrors';
// Rendering Array
$array23 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments24 = array();
$arguments24['subject'] = NULL;
$renderChildrenClosure25 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'validationResult.flattenedErrors', $renderingContext);
};
$viewHelper26 = $self->getViewHelper('$viewHelper26', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper26->setArguments($arguments24);
$viewHelper26->setRenderingContext($renderingContext);
$viewHelper26->setRenderChildrenClosure($renderChildrenClosure25);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$array23['0'] = $viewHelper26->initializeArgumentsAndRender();
$arguments22['arguments'] = $array23;
$output27 = '';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments28 = array();
$arguments28['subject'] = NULL;
$renderChildrenClosure29 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'validationResult.flattenedErrors', $renderingContext);
};
$viewHelper30 = $self->getViewHelper('$viewHelper30', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper30->setArguments($arguments28);
$viewHelper30->setRenderingContext($renderingContext);
$viewHelper30->setRenderChildrenClosure($renderChildrenClosure29);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper

$output27 .= $viewHelper30->initializeArgumentsAndRender();

$output27 .= ' errors were found';
$arguments22['value'] = $output27;
$arguments22['package'] = NULL;
$arguments22['quantity'] = NULL;
$arguments22['languageIdentifier'] = NULL;
$renderChildrenClosure31 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper32 = $self->getViewHelper('$viewHelper32', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper32->setArguments($arguments22);
$viewHelper32->setRenderingContext($renderingContext);
$viewHelper32->setRenderChildrenClosure($renderChildrenClosure31);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments21['value'] = $viewHelper32->initializeArgumentsAndRender();
$arguments21['keepQuotes'] = false;
$arguments21['encoding'] = 'UTF-8';
$arguments21['doubleEncode'] = true;
$renderChildrenClosure33 = function() use ($renderingContext, $self) {
return NULL;
};
$value34 = ($arguments21['value'] !== NULL ? $arguments21['value'] : $renderChildrenClosure33());

$output20 .= !is_string($value34) && !(is_object($value34) && method_exists($value34, '__toString')) ? $value34 : htmlspecialchars($value34, ($arguments21['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments21['encoding'], $arguments21['doubleEncode']);

$output20 .= '">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments35 = array();
$arguments35['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'validationResult.flattenedErrors', $renderingContext);
$arguments35['key'] = 'path';
$arguments35['as'] = 'errors';
$arguments35['reverse'] = false;
$arguments35['iteration'] = NULL;
$renderChildrenClosure36 = function() use ($renderingContext, $self) {
$output37 = '';

$output37 .= '
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments38 = array();
$arguments38['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'errors', $renderingContext);
$arguments38['as'] = 'error';
$arguments38['key'] = '';
$arguments38['reverse'] = false;
$arguments38['iteration'] = NULL;
$renderChildrenClosure39 = function() use ($renderingContext, $self) {
$output40 = '';

$output40 .= '
						<pre>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments41 = array();
$arguments41['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'path', $renderingContext);
$arguments41['keepQuotes'] = false;
$arguments41['encoding'] = 'UTF-8';
$arguments41['doubleEncode'] = true;
$renderChildrenClosure42 = function() use ($renderingContext, $self) {
return NULL;
};
$value43 = ($arguments41['value'] !== NULL ? $arguments41['value'] : $renderChildrenClosure42());

$output40 .= !is_string($value43) && !(is_object($value43) && method_exists($value43, '__toString')) ? $value43 : htmlspecialchars($value43, ($arguments41['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments41['encoding'], $arguments41['doubleEncode']);

$output40 .= ' -> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments44 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments45 = array();
$arguments45['id'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'error.code', $renderingContext);
$arguments45['source'] = 'ValidationErrors';
$arguments45['package'] = 'TYPO3.Flow';
$arguments45['arguments'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'error.arguments', $renderingContext);
$arguments45['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'error', $renderingContext);
$arguments45['quantity'] = NULL;
$arguments45['languageIdentifier'] = NULL;
$renderChildrenClosure46 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper47 = $self->getViewHelper('$viewHelper47', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper47->setArguments($arguments45);
$viewHelper47->setRenderingContext($renderingContext);
$viewHelper47->setRenderChildrenClosure($renderChildrenClosure46);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments44['value'] = $viewHelper47->initializeArgumentsAndRender();
$arguments44['keepQuotes'] = false;
$arguments44['encoding'] = 'UTF-8';
$arguments44['doubleEncode'] = true;
$renderChildrenClosure48 = function() use ($renderingContext, $self) {
return NULL;
};
$value49 = ($arguments44['value'] !== NULL ? $arguments44['value'] : $renderChildrenClosure48());

$output40 .= !is_string($value49) && !(is_object($value49) && method_exists($value49, '__toString')) ? $value49 : htmlspecialchars($value49, ($arguments44['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments44['encoding'], $arguments44['doubleEncode']);

$output40 .= '</pre>
					';
return $output40;
};

$output37 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments38, $renderChildrenClosure39, $renderingContext);

$output37 .= '
				';
return $output37;
};

$output20 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments35, $renderChildrenClosure36, $renderingContext);

$output20 .= '
			</li>
		</ul>
	';
return $output20;
};
$viewHelper50 = $self->getViewHelper('$viewHelper50', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper50->setArguments($arguments15);
$viewHelper50->setRenderingContext($renderingContext);
$viewHelper50->setRenderChildrenClosure($renderChildrenClosure19);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output0 .= $viewHelper50->initializeArgumentsAndRender();

$output0 .= '
	<br /><br />
	<div id="configuration">
		<ul>
			<li class="folder">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments51 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments52 = array();
$arguments52['id'] = 'configuration';
$arguments52['value'] = 'Configuration';
$arguments52['arguments'] = array (
);
$arguments52['source'] = 'Main';
$arguments52['package'] = NULL;
$arguments52['quantity'] = NULL;
$arguments52['languageIdentifier'] = NULL;
$renderChildrenClosure53 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper54 = $self->getViewHelper('$viewHelper54', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper54->setArguments($arguments52);
$viewHelper54->setRenderingContext($renderingContext);
$viewHelper54->setRenderChildrenClosure($renderChildrenClosure53);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments51['value'] = $viewHelper54->initializeArgumentsAndRender();
$arguments51['keepQuotes'] = false;
$arguments51['encoding'] = 'UTF-8';
$arguments51['doubleEncode'] = true;
$renderChildrenClosure55 = function() use ($renderingContext, $self) {
return NULL;
};
$value56 = ($arguments51['value'] !== NULL ? $arguments51['value'] : $renderChildrenClosure55());

$output0 .= !is_string($value56) && !(is_object($value56) && method_exists($value56, '__toString')) ? $value56 : htmlspecialchars($value56, ($arguments51['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments51['encoding'], $arguments51['doubleEncode']);

$output0 .= '
				';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationTreeViewHelper
$arguments57 = array();
$arguments57['configuration'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration', $renderingContext);
$renderChildrenClosure58 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper59 = $self->getViewHelper('$viewHelper59', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\ConfigurationTreeViewHelper');
$viewHelper59->setArguments($arguments57);
$viewHelper59->setRenderingContext($renderingContext);
$viewHelper59->setRenderChildrenClosure($renderChildrenClosure58);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationTreeViewHelper

$output0 .= $viewHelper59->initializeArgumentsAndRender();

$output0 .= '
			</li>
		</ul>
	</div>
	<script>
		$(function() {
			var cookieId = "typo3-neos-module-configuration-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments60 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\CaseViewHelper
$arguments61 = array();
$arguments61['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'type', $renderingContext);
$arguments61['mode'] = 'lower';
$renderChildrenClosure62 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper63 = $self->getViewHelper('$viewHelper63', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\CaseViewHelper');
$viewHelper63->setArguments($arguments61);
$viewHelper63->setRenderingContext($renderingContext);
$viewHelper63->setRenderChildrenClosure($renderChildrenClosure62);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\CaseViewHelper
$arguments60['value'] = $viewHelper63->initializeArgumentsAndRender();
$arguments60['keepQuotes'] = false;
$arguments60['encoding'] = 'UTF-8';
$arguments60['doubleEncode'] = true;
$renderChildrenClosure64 = function() use ($renderingContext, $self) {
return NULL;
};
$value65 = ($arguments60['value'] !== NULL ? $arguments60['value'] : $renderChildrenClosure64());

$output0 .= !is_string($value65) && !(is_object($value65) && method_exists($value65, '__toString')) ? $value65 : htmlspecialchars($value65, ($arguments60['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments60['encoding'], $arguments60['doubleEncode']);

$output0 .= '";
			require(
				';
$output66 = '';

$output66 .= '{
					baseUrl: \'';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments67 = array();
$arguments67['path'] = 'JavaScript';
$arguments67['package'] = NULL;
$arguments67['resource'] = NULL;
$arguments67['localize'] = true;
$renderChildrenClosure68 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper69 = $self->getViewHelper('$viewHelper69', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper69->setArguments($arguments67);
$viewHelper69->setRenderingContext($renderingContext);
$viewHelper69->setRenderChildrenClosure($renderChildrenClosure68);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper

$output66 .= $viewHelper69->initializeArgumentsAndRender();

$output66 .= '\',
					paths: requirePaths,
					context: \'neos\',
					locale: \'en\'
				}';

$output0 .= $output66;

$output0 .= ',
				[
					\'Library/jquery-with-dependencies\'
				],
				function($) {
					$(\'#configuration\').dynatree({
						keyboard: true,
						selectMode: 1,
						minExpandLevel: 2,
						classNames: {
							container: \'neos-dynatree-container\',
							node: \'neos-dynatree-node\',
							folder: \'neos-dynatree-folder\',

							empty: \'neos-dynatree-empty\',
							vline: \'neos-dynatree-vline\',
							expander: \'neos-dynatree-expander\',
							connector: \'neos-dynatree-connector\',
							checkbox: \'neos-dynatree-checkbox\',
							nodeIcon: \'neos-dynatree-icon\',
							title: \'neos-dynatree-title\',
							noConnector: \'neos-dynatree-no-connector\',

							nodeError: \'neos-dynatree-statusnode-error\',
							nodeWait: \'neos-dynatree-statusnode-wait\',
							hidden: \'neos-dynatree-hidden\',
							combinedExpanderPrefix: \'neos-dynatree-exp-\',
							combinedIconPrefix: \'neos-dynatree-ico-\',
							nodeLoading: \'neos-dynatree-loading\',
							hasChildren: \'neos-dynatree-has-children\',
							active: \'neos-dynatree-active\',
							selected: \'neos-dynatree-selected\',
							expanded: \'neos-dynatree-expanded\',
							lazy: \'neos-dynatree-lazy\',
							focused: \'neos-dynatree-focused\',
							partsel: \'neos-dynatree-partsel\',
							lastsib: \'neos-dynatree-lastsib\'
						},
						noLink: true,
						autoFocus: false,
						clickFolderMode: 3,
						debugLevel: 0, // 0: quiet, 1: normal, 2: debug
						persist: true,
						cookieId: cookieId,
						cookie: {
							expires: 365
						}
					});
				}
			);
		});
	</script>
';

return $output0;
}
/**
 * Main Render function
 */
public function render(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output70 = '';

$output70 .= '
';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments71 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\LayoutViewHelper
$arguments72 = array();
$arguments72['name'] = 'BackendSubModule';
$renderChildrenClosure73 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper74 = $self->getViewHelper('$viewHelper74', $renderingContext, 'TYPO3\Fluid\ViewHelpers\LayoutViewHelper');
$viewHelper74->setArguments($arguments72);
$viewHelper74->setRenderingContext($renderingContext);
$viewHelper74->setRenderChildrenClosure($renderChildrenClosure73);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\LayoutViewHelper
$arguments71['value'] = $viewHelper74->initializeArgumentsAndRender();
$arguments71['keepQuotes'] = false;
$arguments71['encoding'] = 'UTF-8';
$arguments71['doubleEncode'] = true;
$renderChildrenClosure75 = function() use ($renderingContext, $self) {
return NULL;
};
$value76 = ($arguments71['value'] !== NULL ? $arguments71['value'] : $renderChildrenClosure75());

$output70 .= !is_string($value76) && !(is_object($value76) && method_exists($value76, '__toString')) ? $value76 : htmlspecialchars($value76, ($arguments71['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments71['encoding'], $arguments71['doubleEncode']);

$output70 .= '

';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\SectionViewHelper
$arguments77 = array();
$arguments77['name'] = 'content';
$renderChildrenClosure78 = function() use ($renderingContext, $self) {
$output79 = '';

$output79 .= '
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments80 = array();
$arguments80['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'availableConfigurationTypes', $renderingContext);
$arguments80['as'] = 'availableConfigurationType';
$arguments80['key'] = '';
$arguments80['reverse'] = false;
$arguments80['iteration'] = NULL;
$renderChildrenClosure81 = function() use ($renderingContext, $self) {
$output82 = '';

$output82 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments83 = array();
$arguments83['action'] = 'index';
// Rendering Array
$array84 = array();
$array84['type'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'availableConfigurationType', $renderingContext);
$arguments83['arguments'] = $array84;
$output85 = '';

$output85 .= 'neos-button';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments86 = array();
// Rendering Boolean node
$arguments86['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'availableConfigurationType', $renderingContext), \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'type', $renderingContext));
$arguments86['then'] = ' neos-active';
$arguments86['else'] = NULL;
$renderChildrenClosure87 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper88 = $self->getViewHelper('$viewHelper88', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper88->setArguments($arguments86);
$viewHelper88->setRenderingContext($renderingContext);
$viewHelper88->setRenderChildrenClosure($renderChildrenClosure87);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output85 .= $viewHelper88->initializeArgumentsAndRender();
$arguments83['class'] = $output85;
$arguments83['additionalAttributes'] = NULL;
$arguments83['data'] = NULL;
$arguments83['controller'] = NULL;
$arguments83['package'] = NULL;
$arguments83['subpackage'] = NULL;
$arguments83['section'] = '';
$arguments83['format'] = '';
$arguments83['additionalParams'] = array (
);
$arguments83['addQueryString'] = false;
$arguments83['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments83['useParentRequest'] = false;
$arguments83['absolute'] = true;
$arguments83['dir'] = NULL;
$arguments83['id'] = NULL;
$arguments83['lang'] = NULL;
$arguments83['style'] = NULL;
$arguments83['title'] = NULL;
$arguments83['accesskey'] = NULL;
$arguments83['tabindex'] = NULL;
$arguments83['onclick'] = NULL;
$arguments83['name'] = NULL;
$arguments83['rel'] = NULL;
$arguments83['rev'] = NULL;
$arguments83['target'] = NULL;
$renderChildrenClosure89 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments90 = array();
$arguments90['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'availableConfigurationType', $renderingContext);
$arguments90['keepQuotes'] = false;
$arguments90['encoding'] = 'UTF-8';
$arguments90['doubleEncode'] = true;
$renderChildrenClosure91 = function() use ($renderingContext, $self) {
return NULL;
};
$value92 = ($arguments90['value'] !== NULL ? $arguments90['value'] : $renderChildrenClosure91());
return !is_string($value92) && !(is_object($value92) && method_exists($value92, '__toString')) ? $value92 : htmlspecialchars($value92, ($arguments90['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments90['encoding'], $arguments90['doubleEncode']);
};
$viewHelper93 = $self->getViewHelper('$viewHelper93', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper93->setArguments($arguments83);
$viewHelper93->setRenderingContext($renderingContext);
$viewHelper93->setRenderChildrenClosure($renderChildrenClosure89);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output82 .= $viewHelper93->initializeArgumentsAndRender();

$output82 .= '
	';
return $output82;
};

$output79 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments80, $renderChildrenClosure81, $renderingContext);

$output79 .= '
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments94 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments95 = array();
$arguments95['subject'] = NULL;
$renderChildrenClosure96 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'validationResult.flattenedErrors', $renderingContext);
};
$viewHelper97 = $self->getViewHelper('$viewHelper97', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper97->setArguments($arguments95);
$viewHelper97->setRenderingContext($renderingContext);
$viewHelper97->setRenderChildrenClosure($renderChildrenClosure96);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments94['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', $viewHelper97->initializeArgumentsAndRender(), 0);
$arguments94['then'] = NULL;
$arguments94['else'] = NULL;
$renderChildrenClosure98 = function() use ($renderingContext, $self) {
$output99 = '';

$output99 .= '
		<ul id="neos-notifications-inline">
			<li data-type="warning" data-title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments100 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments101 = array();
$arguments101['id'] = 'numberValidationErrors';
$arguments101['source'] = 'ValidationErrors';
// Rendering Array
$array102 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments103 = array();
$arguments103['subject'] = NULL;
$renderChildrenClosure104 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'validationResult.flattenedErrors', $renderingContext);
};
$viewHelper105 = $self->getViewHelper('$viewHelper105', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper105->setArguments($arguments103);
$viewHelper105->setRenderingContext($renderingContext);
$viewHelper105->setRenderChildrenClosure($renderChildrenClosure104);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$array102['0'] = $viewHelper105->initializeArgumentsAndRender();
$arguments101['arguments'] = $array102;
$output106 = '';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments107 = array();
$arguments107['subject'] = NULL;
$renderChildrenClosure108 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'validationResult.flattenedErrors', $renderingContext);
};
$viewHelper109 = $self->getViewHelper('$viewHelper109', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper109->setArguments($arguments107);
$viewHelper109->setRenderingContext($renderingContext);
$viewHelper109->setRenderChildrenClosure($renderChildrenClosure108);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper

$output106 .= $viewHelper109->initializeArgumentsAndRender();

$output106 .= ' errors were found';
$arguments101['value'] = $output106;
$arguments101['package'] = NULL;
$arguments101['quantity'] = NULL;
$arguments101['languageIdentifier'] = NULL;
$renderChildrenClosure110 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper111 = $self->getViewHelper('$viewHelper111', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper111->setArguments($arguments101);
$viewHelper111->setRenderingContext($renderingContext);
$viewHelper111->setRenderChildrenClosure($renderChildrenClosure110);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments100['value'] = $viewHelper111->initializeArgumentsAndRender();
$arguments100['keepQuotes'] = false;
$arguments100['encoding'] = 'UTF-8';
$arguments100['doubleEncode'] = true;
$renderChildrenClosure112 = function() use ($renderingContext, $self) {
return NULL;
};
$value113 = ($arguments100['value'] !== NULL ? $arguments100['value'] : $renderChildrenClosure112());

$output99 .= !is_string($value113) && !(is_object($value113) && method_exists($value113, '__toString')) ? $value113 : htmlspecialchars($value113, ($arguments100['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments100['encoding'], $arguments100['doubleEncode']);

$output99 .= '">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments114 = array();
$arguments114['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'validationResult.flattenedErrors', $renderingContext);
$arguments114['key'] = 'path';
$arguments114['as'] = 'errors';
$arguments114['reverse'] = false;
$arguments114['iteration'] = NULL;
$renderChildrenClosure115 = function() use ($renderingContext, $self) {
$output116 = '';

$output116 .= '
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments117 = array();
$arguments117['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'errors', $renderingContext);
$arguments117['as'] = 'error';
$arguments117['key'] = '';
$arguments117['reverse'] = false;
$arguments117['iteration'] = NULL;
$renderChildrenClosure118 = function() use ($renderingContext, $self) {
$output119 = '';

$output119 .= '
						<pre>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments120 = array();
$arguments120['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'path', $renderingContext);
$arguments120['keepQuotes'] = false;
$arguments120['encoding'] = 'UTF-8';
$arguments120['doubleEncode'] = true;
$renderChildrenClosure121 = function() use ($renderingContext, $self) {
return NULL;
};
$value122 = ($arguments120['value'] !== NULL ? $arguments120['value'] : $renderChildrenClosure121());

$output119 .= !is_string($value122) && !(is_object($value122) && method_exists($value122, '__toString')) ? $value122 : htmlspecialchars($value122, ($arguments120['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments120['encoding'], $arguments120['doubleEncode']);

$output119 .= ' -> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments123 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments124 = array();
$arguments124['id'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'error.code', $renderingContext);
$arguments124['source'] = 'ValidationErrors';
$arguments124['package'] = 'TYPO3.Flow';
$arguments124['arguments'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'error.arguments', $renderingContext);
$arguments124['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'error', $renderingContext);
$arguments124['quantity'] = NULL;
$arguments124['languageIdentifier'] = NULL;
$renderChildrenClosure125 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper126 = $self->getViewHelper('$viewHelper126', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper126->setArguments($arguments124);
$viewHelper126->setRenderingContext($renderingContext);
$viewHelper126->setRenderChildrenClosure($renderChildrenClosure125);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments123['value'] = $viewHelper126->initializeArgumentsAndRender();
$arguments123['keepQuotes'] = false;
$arguments123['encoding'] = 'UTF-8';
$arguments123['doubleEncode'] = true;
$renderChildrenClosure127 = function() use ($renderingContext, $self) {
return NULL;
};
$value128 = ($arguments123['value'] !== NULL ? $arguments123['value'] : $renderChildrenClosure127());

$output119 .= !is_string($value128) && !(is_object($value128) && method_exists($value128, '__toString')) ? $value128 : htmlspecialchars($value128, ($arguments123['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments123['encoding'], $arguments123['doubleEncode']);

$output119 .= '</pre>
					';
return $output119;
};

$output116 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments117, $renderChildrenClosure118, $renderingContext);

$output116 .= '
				';
return $output116;
};

$output99 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments114, $renderChildrenClosure115, $renderingContext);

$output99 .= '
			</li>
		</ul>
	';
return $output99;
};
$viewHelper129 = $self->getViewHelper('$viewHelper129', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper129->setArguments($arguments94);
$viewHelper129->setRenderingContext($renderingContext);
$viewHelper129->setRenderChildrenClosure($renderChildrenClosure98);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output79 .= $viewHelper129->initializeArgumentsAndRender();

$output79 .= '
	<br /><br />
	<div id="configuration">
		<ul>
			<li class="folder">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments130 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments131 = array();
$arguments131['id'] = 'configuration';
$arguments131['value'] = 'Configuration';
$arguments131['arguments'] = array (
);
$arguments131['source'] = 'Main';
$arguments131['package'] = NULL;
$arguments131['quantity'] = NULL;
$arguments131['languageIdentifier'] = NULL;
$renderChildrenClosure132 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper133 = $self->getViewHelper('$viewHelper133', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper133->setArguments($arguments131);
$viewHelper133->setRenderingContext($renderingContext);
$viewHelper133->setRenderChildrenClosure($renderChildrenClosure132);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments130['value'] = $viewHelper133->initializeArgumentsAndRender();
$arguments130['keepQuotes'] = false;
$arguments130['encoding'] = 'UTF-8';
$arguments130['doubleEncode'] = true;
$renderChildrenClosure134 = function() use ($renderingContext, $self) {
return NULL;
};
$value135 = ($arguments130['value'] !== NULL ? $arguments130['value'] : $renderChildrenClosure134());

$output79 .= !is_string($value135) && !(is_object($value135) && method_exists($value135, '__toString')) ? $value135 : htmlspecialchars($value135, ($arguments130['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments130['encoding'], $arguments130['doubleEncode']);

$output79 .= '
				';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationTreeViewHelper
$arguments136 = array();
$arguments136['configuration'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration', $renderingContext);
$renderChildrenClosure137 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper138 = $self->getViewHelper('$viewHelper138', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\ConfigurationTreeViewHelper');
$viewHelper138->setArguments($arguments136);
$viewHelper138->setRenderingContext($renderingContext);
$viewHelper138->setRenderChildrenClosure($renderChildrenClosure137);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationTreeViewHelper

$output79 .= $viewHelper138->initializeArgumentsAndRender();

$output79 .= '
			</li>
		</ul>
	</div>
	<script>
		$(function() {
			var cookieId = "typo3-neos-module-configuration-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments139 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\CaseViewHelper
$arguments140 = array();
$arguments140['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'type', $renderingContext);
$arguments140['mode'] = 'lower';
$renderChildrenClosure141 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper142 = $self->getViewHelper('$viewHelper142', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\CaseViewHelper');
$viewHelper142->setArguments($arguments140);
$viewHelper142->setRenderingContext($renderingContext);
$viewHelper142->setRenderChildrenClosure($renderChildrenClosure141);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\CaseViewHelper
$arguments139['value'] = $viewHelper142->initializeArgumentsAndRender();
$arguments139['keepQuotes'] = false;
$arguments139['encoding'] = 'UTF-8';
$arguments139['doubleEncode'] = true;
$renderChildrenClosure143 = function() use ($renderingContext, $self) {
return NULL;
};
$value144 = ($arguments139['value'] !== NULL ? $arguments139['value'] : $renderChildrenClosure143());

$output79 .= !is_string($value144) && !(is_object($value144) && method_exists($value144, '__toString')) ? $value144 : htmlspecialchars($value144, ($arguments139['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments139['encoding'], $arguments139['doubleEncode']);

$output79 .= '";
			require(
				';
$output145 = '';

$output145 .= '{
					baseUrl: \'';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments146 = array();
$arguments146['path'] = 'JavaScript';
$arguments146['package'] = NULL;
$arguments146['resource'] = NULL;
$arguments146['localize'] = true;
$renderChildrenClosure147 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper148 = $self->getViewHelper('$viewHelper148', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper148->setArguments($arguments146);
$viewHelper148->setRenderingContext($renderingContext);
$viewHelper148->setRenderChildrenClosure($renderChildrenClosure147);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper

$output145 .= $viewHelper148->initializeArgumentsAndRender();

$output145 .= '\',
					paths: requirePaths,
					context: \'neos\',
					locale: \'en\'
				}';

$output79 .= $output145;

$output79 .= ',
				[
					\'Library/jquery-with-dependencies\'
				],
				function($) {
					$(\'#configuration\').dynatree({
						keyboard: true,
						selectMode: 1,
						minExpandLevel: 2,
						classNames: {
							container: \'neos-dynatree-container\',
							node: \'neos-dynatree-node\',
							folder: \'neos-dynatree-folder\',

							empty: \'neos-dynatree-empty\',
							vline: \'neos-dynatree-vline\',
							expander: \'neos-dynatree-expander\',
							connector: \'neos-dynatree-connector\',
							checkbox: \'neos-dynatree-checkbox\',
							nodeIcon: \'neos-dynatree-icon\',
							title: \'neos-dynatree-title\',
							noConnector: \'neos-dynatree-no-connector\',

							nodeError: \'neos-dynatree-statusnode-error\',
							nodeWait: \'neos-dynatree-statusnode-wait\',
							hidden: \'neos-dynatree-hidden\',
							combinedExpanderPrefix: \'neos-dynatree-exp-\',
							combinedIconPrefix: \'neos-dynatree-ico-\',
							nodeLoading: \'neos-dynatree-loading\',
							hasChildren: \'neos-dynatree-has-children\',
							active: \'neos-dynatree-active\',
							selected: \'neos-dynatree-selected\',
							expanded: \'neos-dynatree-expanded\',
							lazy: \'neos-dynatree-lazy\',
							focused: \'neos-dynatree-focused\',
							partsel: \'neos-dynatree-partsel\',
							lastsib: \'neos-dynatree-lastsib\'
						},
						noLink: true,
						autoFocus: false,
						clickFolderMode: 3,
						debugLevel: 0, // 0: quiet, 1: normal, 2: debug
						persist: true,
						cookieId: cookieId,
						cookie: {
							expires: 365
						}
					});
				}
			);
		});
	</script>
';
return $output79;
};

$output70 .= '';

$output70 .= '
';

return $output70;
}


}
#0             43136     