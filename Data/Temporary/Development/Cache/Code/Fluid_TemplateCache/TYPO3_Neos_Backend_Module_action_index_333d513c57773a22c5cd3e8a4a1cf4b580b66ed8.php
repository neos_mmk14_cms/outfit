<?php class FluidCache_TYPO3_Neos_Backend_Module_action_index_333d513c57773a22c5cd3e8a4a1cf4b580b66ed8 extends \TYPO3\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// TODO
	return new \TYPO3\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;

return 'Default';
}
public function hasLayout() {
return TRUE;
}

/**
 * section head
 */
public function section_1a954628a960aaef81d7b2d4521929579f3541e6(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output0 = '';

$output0 .= '
	<title>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments2 = array();
$arguments2['id'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'title', $renderingContext);
$arguments2['value'] = NULL;
$arguments2['arguments'] = array (
);
$arguments2['source'] = 'Main';
$arguments2['package'] = NULL;
$arguments2['quantity'] = NULL;
$arguments2['languageIdentifier'] = NULL;
$renderChildrenClosure3 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper4 = $self->getViewHelper('$viewHelper4', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper4->setArguments($arguments2);
$viewHelper4->setRenderingContext($renderingContext);
$viewHelper4->setRenderChildrenClosure($renderChildrenClosure3);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1['value'] = $viewHelper4->initializeArgumentsAndRender();
$arguments1['keepQuotes'] = false;
$arguments1['encoding'] = 'UTF-8';
$arguments1['doubleEncode'] = true;
$renderChildrenClosure5 = function() use ($renderingContext, $self) {
return NULL;
};
$value6 = ($arguments1['value'] !== NULL ? $arguments1['value'] : $renderChildrenClosure5());

$output0 .= !is_string($value6) && !(is_object($value6) && method_exists($value6, '__toString')) ? $value6 : htmlspecialchars($value6, ($arguments1['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1['encoding'], $arguments1['doubleEncode']);

$output0 .= '</title>

	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments7 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\ShouldLoadMinifiedJavascriptViewHelper
$arguments8 = array();
$renderChildrenClosure9 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper10 = $self->getViewHelper('$viewHelper10', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\ShouldLoadMinifiedJavascriptViewHelper');
$viewHelper10->setArguments($arguments8);
$viewHelper10->setRenderingContext($renderingContext);
$viewHelper10->setRenderChildrenClosure($renderChildrenClosure9);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\ShouldLoadMinifiedJavascriptViewHelper
$arguments7['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean($viewHelper10->initializeArgumentsAndRender());
$arguments7['then'] = NULL;
$arguments7['else'] = NULL;
$renderChildrenClosure11 = function() use ($renderingContext, $self) {
$output12 = '';

$output12 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments13 = array();
$renderChildrenClosure14 = function() use ($renderingContext, $self) {
$output15 = '';

$output15 .= '
			<link rel="stylesheet" type="text/css"
				  href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments16 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments17 = array();
$arguments17['path'] = 'Styles/Includes-built.css';
$arguments17['package'] = 'TYPO3.Neos';
$arguments17['resource'] = NULL;
$arguments17['localize'] = true;
$renderChildrenClosure18 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper19 = $self->getViewHelper('$viewHelper19', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper19->setArguments($arguments17);
$viewHelper19->setRenderingContext($renderingContext);
$viewHelper19->setRenderChildrenClosure($renderChildrenClosure18);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments16['value'] = $viewHelper19->initializeArgumentsAndRender();
$arguments16['keepQuotes'] = false;
$arguments16['encoding'] = 'UTF-8';
$arguments16['doubleEncode'] = true;
$renderChildrenClosure20 = function() use ($renderingContext, $self) {
return NULL;
};
$value21 = ($arguments16['value'] !== NULL ? $arguments16['value'] : $renderChildrenClosure20());

$output15 .= !is_string($value21) && !(is_object($value21) && method_exists($value21, '__toString')) ? $value21 : htmlspecialchars($value21, ($arguments16['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments16['encoding'], $arguments16['doubleEncode']);

$output15 .= '?bust=';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments22 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\CssBuiltVersionViewHelper
$arguments23 = array();
$renderChildrenClosure24 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper25 = $self->getViewHelper('$viewHelper25', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\CssBuiltVersionViewHelper');
$viewHelper25->setArguments($arguments23);
$viewHelper25->setRenderingContext($renderingContext);
$viewHelper25->setRenderChildrenClosure($renderChildrenClosure24);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\CssBuiltVersionViewHelper
$arguments22['value'] = $viewHelper25->initializeArgumentsAndRender();
$arguments22['keepQuotes'] = false;
$arguments22['encoding'] = 'UTF-8';
$arguments22['doubleEncode'] = true;
$renderChildrenClosure26 = function() use ($renderingContext, $self) {
return NULL;
};
$value27 = ($arguments22['value'] !== NULL ? $arguments22['value'] : $renderChildrenClosure26());

$output15 .= !is_string($value27) && !(is_object($value27) && method_exists($value27, '__toString')) ? $value27 : htmlspecialchars($value27, ($arguments22['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments22['encoding'], $arguments22['doubleEncode']);

$output15 .= '"/>
		';
return $output15;
};
$viewHelper28 = $self->getViewHelper('$viewHelper28', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper28->setArguments($arguments13);
$viewHelper28->setRenderingContext($renderingContext);
$viewHelper28->setRenderChildrenClosure($renderChildrenClosure14);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output12 .= $viewHelper28->initializeArgumentsAndRender();

$output12 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments29 = array();
$renderChildrenClosure30 = function() use ($renderingContext, $self) {
$output31 = '';

$output31 .= '
			<link rel="stylesheet" type="text/css"
				  href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments32 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments33 = array();
$arguments33['path'] = 'Styles/Includes.css';
$arguments33['package'] = 'TYPO3.Neos';
$arguments33['resource'] = NULL;
$arguments33['localize'] = true;
$renderChildrenClosure34 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper35 = $self->getViewHelper('$viewHelper35', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper35->setArguments($arguments33);
$viewHelper35->setRenderingContext($renderingContext);
$viewHelper35->setRenderChildrenClosure($renderChildrenClosure34);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments32['value'] = $viewHelper35->initializeArgumentsAndRender();
$arguments32['keepQuotes'] = false;
$arguments32['encoding'] = 'UTF-8';
$arguments32['doubleEncode'] = true;
$renderChildrenClosure36 = function() use ($renderingContext, $self) {
return NULL;
};
$value37 = ($arguments32['value'] !== NULL ? $arguments32['value'] : $renderChildrenClosure36());

$output31 .= !is_string($value37) && !(is_object($value37) && method_exists($value37, '__toString')) ? $value37 : htmlspecialchars($value37, ($arguments32['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments32['encoding'], $arguments32['doubleEncode']);

$output31 .= '"/>
		';
return $output31;
};
$viewHelper38 = $self->getViewHelper('$viewHelper38', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper38->setArguments($arguments29);
$viewHelper38->setRenderingContext($renderingContext);
$viewHelper38->setRenderChildrenClosure($renderChildrenClosure30);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output12 .= $viewHelper38->initializeArgumentsAndRender();

$output12 .= '
	';
return $output12;
};
$arguments7['__thenClosure'] = function() use ($renderingContext, $self) {
$output39 = '';

$output39 .= '
			<link rel="stylesheet" type="text/css"
				  href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments40 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments41 = array();
$arguments41['path'] = 'Styles/Includes-built.css';
$arguments41['package'] = 'TYPO3.Neos';
$arguments41['resource'] = NULL;
$arguments41['localize'] = true;
$renderChildrenClosure42 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper43 = $self->getViewHelper('$viewHelper43', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper43->setArguments($arguments41);
$viewHelper43->setRenderingContext($renderingContext);
$viewHelper43->setRenderChildrenClosure($renderChildrenClosure42);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments40['value'] = $viewHelper43->initializeArgumentsAndRender();
$arguments40['keepQuotes'] = false;
$arguments40['encoding'] = 'UTF-8';
$arguments40['doubleEncode'] = true;
$renderChildrenClosure44 = function() use ($renderingContext, $self) {
return NULL;
};
$value45 = ($arguments40['value'] !== NULL ? $arguments40['value'] : $renderChildrenClosure44());

$output39 .= !is_string($value45) && !(is_object($value45) && method_exists($value45, '__toString')) ? $value45 : htmlspecialchars($value45, ($arguments40['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments40['encoding'], $arguments40['doubleEncode']);

$output39 .= '?bust=';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments46 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\CssBuiltVersionViewHelper
$arguments47 = array();
$renderChildrenClosure48 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper49 = $self->getViewHelper('$viewHelper49', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\CssBuiltVersionViewHelper');
$viewHelper49->setArguments($arguments47);
$viewHelper49->setRenderingContext($renderingContext);
$viewHelper49->setRenderChildrenClosure($renderChildrenClosure48);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\CssBuiltVersionViewHelper
$arguments46['value'] = $viewHelper49->initializeArgumentsAndRender();
$arguments46['keepQuotes'] = false;
$arguments46['encoding'] = 'UTF-8';
$arguments46['doubleEncode'] = true;
$renderChildrenClosure50 = function() use ($renderingContext, $self) {
return NULL;
};
$value51 = ($arguments46['value'] !== NULL ? $arguments46['value'] : $renderChildrenClosure50());

$output39 .= !is_string($value51) && !(is_object($value51) && method_exists($value51, '__toString')) ? $value51 : htmlspecialchars($value51, ($arguments46['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments46['encoding'], $arguments46['doubleEncode']);

$output39 .= '"/>
		';
return $output39;
};
$arguments7['__elseClosure'] = function() use ($renderingContext, $self) {
$output52 = '';

$output52 .= '
			<link rel="stylesheet" type="text/css"
				  href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments53 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments54 = array();
$arguments54['path'] = 'Styles/Includes.css';
$arguments54['package'] = 'TYPO3.Neos';
$arguments54['resource'] = NULL;
$arguments54['localize'] = true;
$renderChildrenClosure55 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper56 = $self->getViewHelper('$viewHelper56', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper56->setArguments($arguments54);
$viewHelper56->setRenderingContext($renderingContext);
$viewHelper56->setRenderChildrenClosure($renderChildrenClosure55);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments53['value'] = $viewHelper56->initializeArgumentsAndRender();
$arguments53['keepQuotes'] = false;
$arguments53['encoding'] = 'UTF-8';
$arguments53['doubleEncode'] = true;
$renderChildrenClosure57 = function() use ($renderingContext, $self) {
return NULL;
};
$value58 = ($arguments53['value'] !== NULL ? $arguments53['value'] : $renderChildrenClosure57());

$output52 .= !is_string($value58) && !(is_object($value58) && method_exists($value58, '__toString')) ? $value58 : htmlspecialchars($value58, ($arguments53['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments53['encoding'], $arguments53['doubleEncode']);

$output52 .= '"/>
		';
return $output52;
};
$viewHelper59 = $self->getViewHelper('$viewHelper59', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper59->setArguments($arguments7);
$viewHelper59->setRenderingContext($renderingContext);
$viewHelper59->setRenderChildrenClosure($renderChildrenClosure11);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output0 .= $viewHelper59->initializeArgumentsAndRender();

$output0 .= '

	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments60 = array();
// Rendering Boolean node
$arguments60['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'moduleConfiguration.additionalResources.styleSheets', $renderingContext));
$arguments60['then'] = NULL;
$arguments60['else'] = NULL;
$renderChildrenClosure61 = function() use ($renderingContext, $self) {
$output62 = '';

$output62 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments63 = array();
$arguments63['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'moduleConfiguration.additionalResources.styleSheets', $renderingContext);
$arguments63['as'] = 'additionalResource';
$arguments63['key'] = '';
$arguments63['reverse'] = false;
$arguments63['iteration'] = NULL;
$renderChildrenClosure64 = function() use ($renderingContext, $self) {
$output65 = '';

$output65 .= '
			<link rel="stylesheet" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments66 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments67 = array();
$arguments67['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'additionalResource', $renderingContext);
$arguments67['package'] = NULL;
$arguments67['resource'] = NULL;
$arguments67['localize'] = true;
$renderChildrenClosure68 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper69 = $self->getViewHelper('$viewHelper69', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper69->setArguments($arguments67);
$viewHelper69->setRenderingContext($renderingContext);
$viewHelper69->setRenderChildrenClosure($renderChildrenClosure68);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments66['value'] = $viewHelper69->initializeArgumentsAndRender();
$arguments66['keepQuotes'] = false;
$arguments66['encoding'] = 'UTF-8';
$arguments66['doubleEncode'] = true;
$renderChildrenClosure70 = function() use ($renderingContext, $self) {
return NULL;
};
$value71 = ($arguments66['value'] !== NULL ? $arguments66['value'] : $renderChildrenClosure70());

$output65 .= !is_string($value71) && !(is_object($value71) && method_exists($value71, '__toString')) ? $value71 : htmlspecialchars($value71, ($arguments66['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments66['encoding'], $arguments66['doubleEncode']);

$output65 .= '" />
		';
return $output65;
};

$output62 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments63, $renderChildrenClosure64, $renderingContext);

$output62 .= '
	';
return $output62;
};
$viewHelper72 = $self->getViewHelper('$viewHelper72', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper72->setArguments($arguments60);
$viewHelper72->setRenderingContext($renderingContext);
$viewHelper72->setRenderChildrenClosure($renderChildrenClosure61);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output0 .= $viewHelper72->initializeArgumentsAndRender();

$output0 .= '

	<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments73 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments74 = array();
$arguments74['path'] = 'Library/jquery/jquery-2.0.3.js';
$arguments74['package'] = NULL;
$arguments74['resource'] = NULL;
$arguments74['localize'] = true;
$renderChildrenClosure75 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper76 = $self->getViewHelper('$viewHelper76', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper76->setArguments($arguments74);
$viewHelper76->setRenderingContext($renderingContext);
$viewHelper76->setRenderChildrenClosure($renderChildrenClosure75);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments73['value'] = $viewHelper76->initializeArgumentsAndRender();
$arguments73['keepQuotes'] = false;
$arguments73['encoding'] = 'UTF-8';
$arguments73['doubleEncode'] = true;
$renderChildrenClosure77 = function() use ($renderingContext, $self) {
return NULL;
};
$value78 = ($arguments73['value'] !== NULL ? $arguments73['value'] : $renderChildrenClosure77());

$output0 .= !is_string($value78) && !(is_object($value78) && method_exists($value78, '__toString')) ? $value78 : htmlspecialchars($value78, ($arguments73['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments73['encoding'], $arguments73['doubleEncode']);

$output0 .= '"></script>
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments79 = array();
// Rendering Boolean node
$arguments79['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'moduleConfiguration.additionalResources.javaScripts', $renderingContext));
$arguments79['then'] = NULL;
$arguments79['else'] = NULL;
$renderChildrenClosure80 = function() use ($renderingContext, $self) {
$output81 = '';

$output81 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments82 = array();
$arguments82['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'moduleConfiguration.additionalResources.javaScripts', $renderingContext);
$arguments82['as'] = 'additionalResource';
$arguments82['key'] = '';
$arguments82['reverse'] = false;
$arguments82['iteration'] = NULL;
$renderChildrenClosure83 = function() use ($renderingContext, $self) {
$output84 = '';

$output84 .= '
			<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments85 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments86 = array();
$arguments86['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'additionalResource', $renderingContext);
$arguments86['package'] = NULL;
$arguments86['resource'] = NULL;
$arguments86['localize'] = true;
$renderChildrenClosure87 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper88 = $self->getViewHelper('$viewHelper88', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper88->setArguments($arguments86);
$viewHelper88->setRenderingContext($renderingContext);
$viewHelper88->setRenderChildrenClosure($renderChildrenClosure87);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments85['value'] = $viewHelper88->initializeArgumentsAndRender();
$arguments85['keepQuotes'] = false;
$arguments85['encoding'] = 'UTF-8';
$arguments85['doubleEncode'] = true;
$renderChildrenClosure89 = function() use ($renderingContext, $self) {
return NULL;
};
$value90 = ($arguments85['value'] !== NULL ? $arguments85['value'] : $renderChildrenClosure89());

$output84 .= !is_string($value90) && !(is_object($value90) && method_exists($value90, '__toString')) ? $value90 : htmlspecialchars($value90, ($arguments85['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments85['encoding'], $arguments85['doubleEncode']);

$output84 .= '"></script>
		';
return $output84;
};

$output81 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments82, $renderChildrenClosure83, $renderingContext);

$output81 .= '
	';
return $output81;
};
$viewHelper91 = $self->getViewHelper('$viewHelper91', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper91->setArguments($arguments79);
$viewHelper91->setRenderingContext($renderingContext);
$viewHelper91->setRenderChildrenClosure($renderChildrenClosure80);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output0 .= $viewHelper91->initializeArgumentsAndRender();

$output0 .= '

	<script type="text/javascript">
		// TODO: Get rid of those global variables
		';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\JavascriptConfigurationViewHelper
$arguments92 = array();
$renderChildrenClosure93 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper94 = $self->getViewHelper('$viewHelper94', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\JavascriptConfigurationViewHelper');
$viewHelper94->setArguments($arguments92);
$viewHelper94->setRenderingContext($renderingContext);
$viewHelper94->setRenderChildrenClosure($renderChildrenClosure93);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\JavascriptConfigurationViewHelper

$output0 .= $viewHelper94->initializeArgumentsAndRender();

$output0 .= '
	</script>

	<link rel="neos-menudata" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments95 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments96 = array();
$arguments96['action'] = 'index';
$arguments96['controller'] = 'Backend\\Menu';
$arguments96['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments96['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
// Rendering Array
$array97 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments98 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper
$arguments99 = array();
$renderChildrenClosure100 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper101 = $self->getViewHelper('$viewHelper101', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper');
$viewHelper101->setArguments($arguments99);
$viewHelper101->setRenderingContext($renderingContext);
$viewHelper101->setRenderChildrenClosure($renderChildrenClosure100);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper
$arguments98['value'] = $viewHelper101->initializeArgumentsAndRender();
$arguments98['keepQuotes'] = false;
$arguments98['encoding'] = 'UTF-8';
$arguments98['doubleEncode'] = true;
$renderChildrenClosure102 = function() use ($renderingContext, $self) {
return NULL;
};
$value103 = ($arguments98['value'] !== NULL ? $arguments98['value'] : $renderChildrenClosure102());
$array97['version'] = !is_string($value103) && !(is_object($value103) && method_exists($value103, '__toString')) ? $value103 : htmlspecialchars($value103, ($arguments98['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments98['encoding'], $arguments98['doubleEncode']);
$arguments96['arguments'] = $array97;
$arguments96['subpackage'] = NULL;
$arguments96['section'] = '';
$arguments96['format'] = '';
$arguments96['additionalParams'] = array (
);
$arguments96['addQueryString'] = false;
$arguments96['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments96['useParentRequest'] = false;
$renderChildrenClosure104 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper105 = $self->getViewHelper('$viewHelper105', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper105->setArguments($arguments96);
$viewHelper105->setRenderingContext($renderingContext);
$viewHelper105->setRenderChildrenClosure($renderChildrenClosure104);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments95['value'] = $viewHelper105->initializeArgumentsAndRender();
$arguments95['keepQuotes'] = false;
$arguments95['encoding'] = 'UTF-8';
$arguments95['doubleEncode'] = true;
$renderChildrenClosure106 = function() use ($renderingContext, $self) {
return NULL;
};
$value107 = ($arguments95['value'] !== NULL ? $arguments95['value'] : $renderChildrenClosure106());

$output0 .= !is_string($value107) && !(is_object($value107) && method_exists($value107, '__toString')) ? $value107 : htmlspecialchars($value107, ($arguments95['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments95['encoding'], $arguments95['doubleEncode']);

$output0 .= '" />
	<!-- TODO: Make sure the schema information and edit / preview panel data isn\'t necessary in backend modules -->
	<link rel="neos-vieschema" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments108 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments109 = array();
$arguments109['action'] = 'vieSchema';
$arguments109['controller'] = 'Backend\\Schema';
$arguments109['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments109['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
// Rendering Array
$array110 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments111 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper
$arguments112 = array();
$renderChildrenClosure113 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper114 = $self->getViewHelper('$viewHelper114', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper');
$viewHelper114->setArguments($arguments112);
$viewHelper114->setRenderingContext($renderingContext);
$viewHelper114->setRenderChildrenClosure($renderChildrenClosure113);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper
$arguments111['value'] = $viewHelper114->initializeArgumentsAndRender();
$arguments111['keepQuotes'] = false;
$arguments111['encoding'] = 'UTF-8';
$arguments111['doubleEncode'] = true;
$renderChildrenClosure115 = function() use ($renderingContext, $self) {
return NULL;
};
$value116 = ($arguments111['value'] !== NULL ? $arguments111['value'] : $renderChildrenClosure115());
$array110['version'] = !is_string($value116) && !(is_object($value116) && method_exists($value116, '__toString')) ? $value116 : htmlspecialchars($value116, ($arguments111['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments111['encoding'], $arguments111['doubleEncode']);
$arguments109['arguments'] = $array110;
$arguments109['subpackage'] = NULL;
$arguments109['section'] = '';
$arguments109['format'] = '';
$arguments109['additionalParams'] = array (
);
$arguments109['addQueryString'] = false;
$arguments109['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments109['useParentRequest'] = false;
$renderChildrenClosure117 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper118 = $self->getViewHelper('$viewHelper118', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper118->setArguments($arguments109);
$viewHelper118->setRenderingContext($renderingContext);
$viewHelper118->setRenderChildrenClosure($renderChildrenClosure117);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments108['value'] = $viewHelper118->initializeArgumentsAndRender();
$arguments108['keepQuotes'] = false;
$arguments108['encoding'] = 'UTF-8';
$arguments108['doubleEncode'] = true;
$renderChildrenClosure119 = function() use ($renderingContext, $self) {
return NULL;
};
$value120 = ($arguments108['value'] !== NULL ? $arguments108['value'] : $renderChildrenClosure119());

$output0 .= !is_string($value120) && !(is_object($value120) && method_exists($value120, '__toString')) ? $value120 : htmlspecialchars($value120, ($arguments108['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments108['encoding'], $arguments108['doubleEncode']);

$output0 .= '" />
	<link rel="neos-nodetypeschema" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments121 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments122 = array();
$arguments122['action'] = 'nodeTypeSchema';
$arguments122['controller'] = 'Backend\\Schema';
$arguments122['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments122['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
// Rendering Array
$array123 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments124 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper
$arguments125 = array();
$renderChildrenClosure126 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper127 = $self->getViewHelper('$viewHelper127', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper');
$viewHelper127->setArguments($arguments125);
$viewHelper127->setRenderingContext($renderingContext);
$viewHelper127->setRenderChildrenClosure($renderChildrenClosure126);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper
$arguments124['value'] = $viewHelper127->initializeArgumentsAndRender();
$arguments124['keepQuotes'] = false;
$arguments124['encoding'] = 'UTF-8';
$arguments124['doubleEncode'] = true;
$renderChildrenClosure128 = function() use ($renderingContext, $self) {
return NULL;
};
$value129 = ($arguments124['value'] !== NULL ? $arguments124['value'] : $renderChildrenClosure128());
$array123['version'] = !is_string($value129) && !(is_object($value129) && method_exists($value129, '__toString')) ? $value129 : htmlspecialchars($value129, ($arguments124['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments124['encoding'], $arguments124['doubleEncode']);
$arguments122['arguments'] = $array123;
$arguments122['subpackage'] = NULL;
$arguments122['section'] = '';
$arguments122['format'] = '';
$arguments122['additionalParams'] = array (
);
$arguments122['addQueryString'] = false;
$arguments122['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments122['useParentRequest'] = false;
$renderChildrenClosure130 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper131 = $self->getViewHelper('$viewHelper131', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper131->setArguments($arguments122);
$viewHelper131->setRenderingContext($renderingContext);
$viewHelper131->setRenderChildrenClosure($renderChildrenClosure130);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments121['value'] = $viewHelper131->initializeArgumentsAndRender();
$arguments121['keepQuotes'] = false;
$arguments121['encoding'] = 'UTF-8';
$arguments121['doubleEncode'] = true;
$renderChildrenClosure132 = function() use ($renderingContext, $self) {
return NULL;
};
$value133 = ($arguments121['value'] !== NULL ? $arguments121['value'] : $renderChildrenClosure132());

$output0 .= !is_string($value133) && !(is_object($value133) && method_exists($value133, '__toString')) ? $value133 : htmlspecialchars($value133, ($arguments121['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments121['encoding'], $arguments121['doubleEncode']);

$output0 .= '" />
	<link rel="neos-editpreviewdata" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments134 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments135 = array();
$arguments135['action'] = 'editPreview';
$arguments135['controller'] = 'Backend\\Settings';
$arguments135['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments135['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
// Rendering Array
$array136 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments137 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper
$arguments138 = array();
$renderChildrenClosure139 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper140 = $self->getViewHelper('$viewHelper140', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper');
$viewHelper140->setArguments($arguments138);
$viewHelper140->setRenderingContext($renderingContext);
$viewHelper140->setRenderChildrenClosure($renderChildrenClosure139);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper
$arguments137['value'] = $viewHelper140->initializeArgumentsAndRender();
$arguments137['keepQuotes'] = false;
$arguments137['encoding'] = 'UTF-8';
$arguments137['doubleEncode'] = true;
$renderChildrenClosure141 = function() use ($renderingContext, $self) {
return NULL;
};
$value142 = ($arguments137['value'] !== NULL ? $arguments137['value'] : $renderChildrenClosure141());
$array136['version'] = !is_string($value142) && !(is_object($value142) && method_exists($value142, '__toString')) ? $value142 : htmlspecialchars($value142, ($arguments137['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments137['encoding'], $arguments137['doubleEncode']);
$arguments135['arguments'] = $array136;
$arguments135['subpackage'] = NULL;
$arguments135['section'] = '';
$arguments135['format'] = '';
$arguments135['additionalParams'] = array (
);
$arguments135['addQueryString'] = false;
$arguments135['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments135['useParentRequest'] = false;
$renderChildrenClosure143 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper144 = $self->getViewHelper('$viewHelper144', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper144->setArguments($arguments135);
$viewHelper144->setRenderingContext($renderingContext);
$viewHelper144->setRenderChildrenClosure($renderChildrenClosure143);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments134['value'] = $viewHelper144->initializeArgumentsAndRender();
$arguments134['keepQuotes'] = false;
$arguments134['encoding'] = 'UTF-8';
$arguments134['doubleEncode'] = true;
$renderChildrenClosure145 = function() use ($renderingContext, $self) {
return NULL;
};
$value146 = ($arguments134['value'] !== NULL ? $arguments134['value'] : $renderChildrenClosure145());

$output0 .= !is_string($value146) && !(is_object($value146) && method_exists($value146, '__toString')) ? $value146 : htmlspecialchars($value146, ($arguments134['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments134['encoding'], $arguments134['doubleEncode']);

$output0 .= '" />
	<link rel="neos-xliff" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\RawViewHelper
$arguments147 = array();
$arguments147['value'] = NULL;
$renderChildrenClosure148 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments149 = array();
$arguments149['action'] = 'xliffAsJson';
// Rendering Array
$array150 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\InterfaceLanguageViewHelper
$arguments151 = array();
$renderChildrenClosure152 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper153 = $self->getViewHelper('$viewHelper153', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\InterfaceLanguageViewHelper');
$viewHelper153->setArguments($arguments151);
$viewHelper153->setRenderingContext($renderingContext);
$viewHelper153->setRenderChildrenClosure($renderChildrenClosure152);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\InterfaceLanguageViewHelper
$array150['locale'] = $viewHelper153->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\XliffCacheVersionViewHelper
$arguments154 = array();
$renderChildrenClosure155 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper156 = $self->getViewHelper('$viewHelper156', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\XliffCacheVersionViewHelper');
$viewHelper156->setArguments($arguments154);
$viewHelper156->setRenderingContext($renderingContext);
$viewHelper156->setRenderChildrenClosure($renderChildrenClosure155);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\XliffCacheVersionViewHelper
$array150['version'] = $viewHelper156->initializeArgumentsAndRender();
$arguments149['arguments'] = $array150;
$arguments149['controller'] = 'Backend\\Backend';
$arguments149['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments149['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments149['subpackage'] = NULL;
$arguments149['section'] = '';
$arguments149['format'] = '';
$arguments149['additionalParams'] = array (
);
$arguments149['addQueryString'] = false;
$arguments149['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments149['useParentRequest'] = false;
$renderChildrenClosure157 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper158 = $self->getViewHelper('$viewHelper158', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper158->setArguments($arguments149);
$viewHelper158->setRenderingContext($renderingContext);
$viewHelper158->setRenderChildrenClosure($renderChildrenClosure157);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
return $viewHelper158->initializeArgumentsAndRender();
};
$viewHelper159 = $self->getViewHelper('$viewHelper159', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\RawViewHelper');
$viewHelper159->setArguments($arguments147);
$viewHelper159->setRenderingContext($renderingContext);
$viewHelper159->setRenderChildrenClosure($renderChildrenClosure148);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\RawViewHelper

$output0 .= $viewHelper159->initializeArgumentsAndRender();

$output0 .= '" />
';

return $output0;
}
/**
 * section body
 */
public function section_02083f4579e08a612425c0c1a17ee47add783b94(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output160 = '';

$output160 .= '
	<body class="neos neos-module neos-controls neos-module-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments161 = array();
$arguments161['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'moduleClass', $renderingContext);
$arguments161['keepQuotes'] = false;
$arguments161['encoding'] = 'UTF-8';
$arguments161['doubleEncode'] = true;
$renderChildrenClosure162 = function() use ($renderingContext, $self) {
return NULL;
};
$value163 = ($arguments161['value'] !== NULL ? $arguments161['value'] : $renderChildrenClosure162());

$output160 .= !is_string($value163) && !(is_object($value163) && method_exists($value163, '__toString')) ? $value163 : htmlspecialchars($value163, ($arguments161['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments161['encoding'], $arguments161['doubleEncode']);

$output160 .= '">
		<div class="neos-module-wrap">
			<ul class="neos-breadcrumb">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments164 = array();
$arguments164['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'moduleBreadcrumb', $renderingContext);
$arguments164['key'] = 'path';
$arguments164['as'] = 'configuration';
$arguments164['iteration'] = 'iterator';
$arguments164['reverse'] = false;
$renderChildrenClosure165 = function() use ($renderingContext, $self) {
$output166 = '';

$output166 .= '
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments167 = array();
// Rendering Boolean node
$arguments167['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.hideInMenu', $renderingContext));
$arguments167['then'] = NULL;
$arguments167['else'] = NULL;
$renderChildrenClosure168 = function() use ($renderingContext, $self) {
$output169 = '';

$output169 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments170 = array();
$renderChildrenClosure171 = function() use ($renderingContext, $self) {
$output172 = '';

$output172 .= '
							<li>
								';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper
$arguments173 = array();
$arguments173['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'path', $renderingContext);
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments174 = array();
// Rendering Boolean node
$arguments174['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'iterator.isLast', $renderingContext));
$arguments174['then'] = 'active';
$arguments174['else'] = NULL;
$renderChildrenClosure175 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper176 = $self->getViewHelper('$viewHelper176', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper176->setArguments($arguments174);
$viewHelper176->setRenderingContext($renderingContext);
$viewHelper176->setRenderChildrenClosure($renderChildrenClosure175);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments173['class'] = $viewHelper176->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments177 = array();
// Rendering Boolean node
$arguments177['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.description', $renderingContext));
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments178 = array();
$arguments178['id'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.description', $renderingContext);
$arguments178['value'] = NULL;
$arguments178['arguments'] = array (
);
$arguments178['source'] = 'Main';
$arguments178['package'] = NULL;
$arguments178['quantity'] = NULL;
$arguments178['languageIdentifier'] = NULL;
$renderChildrenClosure179 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper180 = $self->getViewHelper('$viewHelper180', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper180->setArguments($arguments178);
$viewHelper180->setRenderingContext($renderingContext);
$viewHelper180->setRenderChildrenClosure($renderChildrenClosure179);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments177['then'] = $viewHelper180->initializeArgumentsAndRender();
$arguments177['else'] = NULL;
$renderChildrenClosure181 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper182 = $self->getViewHelper('$viewHelper182', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper182->setArguments($arguments177);
$viewHelper182->setRenderingContext($renderingContext);
$viewHelper182->setRenderChildrenClosure($renderChildrenClosure181);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments173['title'] = $viewHelper182->initializeArgumentsAndRender();
// Rendering Array
$array183 = array();
$array183['data-neos-toggle'] = 'tooltip';
$array183['data-placement'] = 'bottom';
$arguments173['additionalAttributes'] = $array183;
$arguments173['data'] = NULL;
$arguments173['action'] = NULL;
$arguments173['arguments'] = array (
);
$arguments173['section'] = '';
$arguments173['format'] = '';
$arguments173['additionalParams'] = array (
);
$arguments173['addQueryString'] = false;
$arguments173['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments173['dir'] = NULL;
$arguments173['id'] = NULL;
$arguments173['lang'] = NULL;
$arguments173['style'] = NULL;
$arguments173['accesskey'] = NULL;
$arguments173['tabindex'] = NULL;
$arguments173['onclick'] = NULL;
$arguments173['name'] = NULL;
$arguments173['rel'] = NULL;
$arguments173['rev'] = NULL;
$arguments173['target'] = NULL;
$renderChildrenClosure184 = function() use ($renderingContext, $self) {
$output185 = '';

$output185 .= '<i class="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments186 = array();
$arguments186['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.icon', $renderingContext);
$arguments186['keepQuotes'] = false;
$arguments186['encoding'] = 'UTF-8';
$arguments186['doubleEncode'] = true;
$renderChildrenClosure187 = function() use ($renderingContext, $self) {
return NULL;
};
$value188 = ($arguments186['value'] !== NULL ? $arguments186['value'] : $renderChildrenClosure187());

$output185 .= !is_string($value188) && !(is_object($value188) && method_exists($value188, '__toString')) ? $value188 : htmlspecialchars($value188, ($arguments186['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments186['encoding'], $arguments186['doubleEncode']);

$output185 .= '"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments189 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments190 = array();
$arguments190['source'] = 'Modules';
$arguments190['id'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.label', $renderingContext);
$arguments190['value'] = NULL;
$arguments190['arguments'] = array (
);
$arguments190['package'] = NULL;
$arguments190['quantity'] = NULL;
$arguments190['languageIdentifier'] = NULL;
$renderChildrenClosure191 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper192 = $self->getViewHelper('$viewHelper192', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper192->setArguments($arguments190);
$viewHelper192->setRenderingContext($renderingContext);
$viewHelper192->setRenderChildrenClosure($renderChildrenClosure191);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments189['value'] = $viewHelper192->initializeArgumentsAndRender();
$arguments189['keepQuotes'] = false;
$arguments189['encoding'] = 'UTF-8';
$arguments189['doubleEncode'] = true;
$renderChildrenClosure193 = function() use ($renderingContext, $self) {
return NULL;
};
$value194 = ($arguments189['value'] !== NULL ? $arguments189['value'] : $renderChildrenClosure193());

$output185 .= !is_string($value194) && !(is_object($value194) && method_exists($value194, '__toString')) ? $value194 : htmlspecialchars($value194, ($arguments189['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments189['encoding'], $arguments189['doubleEncode']);
return $output185;
};
$viewHelper195 = $self->getViewHelper('$viewHelper195', $renderingContext, 'TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper');
$viewHelper195->setArguments($arguments173);
$viewHelper195->setRenderingContext($renderingContext);
$viewHelper195->setRenderChildrenClosure($renderChildrenClosure184);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper

$output172 .= $viewHelper195->initializeArgumentsAndRender();

$output172 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments196 = array();
// Rendering Boolean node
$arguments196['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'iterator.isLast', $renderingContext));
$arguments196['then'] = NULL;
$arguments196['else'] = NULL;
$renderChildrenClosure197 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments198 = array();
$renderChildrenClosure199 = function() use ($renderingContext, $self) {
return '<span class="neos-divider">/</span>';
};
$viewHelper200 = $self->getViewHelper('$viewHelper200', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper200->setArguments($arguments198);
$viewHelper200->setRenderingContext($renderingContext);
$viewHelper200->setRenderChildrenClosure($renderChildrenClosure199);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
return $viewHelper200->initializeArgumentsAndRender();
};
$arguments196['__elseClosure'] = function() use ($renderingContext, $self) {
return '<span class="neos-divider">/</span>';
};
$viewHelper201 = $self->getViewHelper('$viewHelper201', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper201->setArguments($arguments196);
$viewHelper201->setRenderingContext($renderingContext);
$viewHelper201->setRenderChildrenClosure($renderChildrenClosure197);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output172 .= $viewHelper201->initializeArgumentsAndRender();

$output172 .= '
							</li>
						';
return $output172;
};
$viewHelper202 = $self->getViewHelper('$viewHelper202', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper202->setArguments($arguments170);
$viewHelper202->setRenderingContext($renderingContext);
$viewHelper202->setRenderChildrenClosure($renderChildrenClosure171);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output169 .= $viewHelper202->initializeArgumentsAndRender();

$output169 .= '
					';
return $output169;
};
$arguments167['__elseClosure'] = function() use ($renderingContext, $self) {
$output203 = '';

$output203 .= '
							<li>
								';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper
$arguments204 = array();
$arguments204['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'path', $renderingContext);
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments205 = array();
// Rendering Boolean node
$arguments205['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'iterator.isLast', $renderingContext));
$arguments205['then'] = 'active';
$arguments205['else'] = NULL;
$renderChildrenClosure206 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper207 = $self->getViewHelper('$viewHelper207', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper207->setArguments($arguments205);
$viewHelper207->setRenderingContext($renderingContext);
$viewHelper207->setRenderChildrenClosure($renderChildrenClosure206);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments204['class'] = $viewHelper207->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments208 = array();
// Rendering Boolean node
$arguments208['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.description', $renderingContext));
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments209 = array();
$arguments209['id'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.description', $renderingContext);
$arguments209['value'] = NULL;
$arguments209['arguments'] = array (
);
$arguments209['source'] = 'Main';
$arguments209['package'] = NULL;
$arguments209['quantity'] = NULL;
$arguments209['languageIdentifier'] = NULL;
$renderChildrenClosure210 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper211 = $self->getViewHelper('$viewHelper211', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper211->setArguments($arguments209);
$viewHelper211->setRenderingContext($renderingContext);
$viewHelper211->setRenderChildrenClosure($renderChildrenClosure210);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments208['then'] = $viewHelper211->initializeArgumentsAndRender();
$arguments208['else'] = NULL;
$renderChildrenClosure212 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper213 = $self->getViewHelper('$viewHelper213', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper213->setArguments($arguments208);
$viewHelper213->setRenderingContext($renderingContext);
$viewHelper213->setRenderChildrenClosure($renderChildrenClosure212);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments204['title'] = $viewHelper213->initializeArgumentsAndRender();
// Rendering Array
$array214 = array();
$array214['data-neos-toggle'] = 'tooltip';
$array214['data-placement'] = 'bottom';
$arguments204['additionalAttributes'] = $array214;
$arguments204['data'] = NULL;
$arguments204['action'] = NULL;
$arguments204['arguments'] = array (
);
$arguments204['section'] = '';
$arguments204['format'] = '';
$arguments204['additionalParams'] = array (
);
$arguments204['addQueryString'] = false;
$arguments204['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments204['dir'] = NULL;
$arguments204['id'] = NULL;
$arguments204['lang'] = NULL;
$arguments204['style'] = NULL;
$arguments204['accesskey'] = NULL;
$arguments204['tabindex'] = NULL;
$arguments204['onclick'] = NULL;
$arguments204['name'] = NULL;
$arguments204['rel'] = NULL;
$arguments204['rev'] = NULL;
$arguments204['target'] = NULL;
$renderChildrenClosure215 = function() use ($renderingContext, $self) {
$output216 = '';

$output216 .= '<i class="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments217 = array();
$arguments217['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.icon', $renderingContext);
$arguments217['keepQuotes'] = false;
$arguments217['encoding'] = 'UTF-8';
$arguments217['doubleEncode'] = true;
$renderChildrenClosure218 = function() use ($renderingContext, $self) {
return NULL;
};
$value219 = ($arguments217['value'] !== NULL ? $arguments217['value'] : $renderChildrenClosure218());

$output216 .= !is_string($value219) && !(is_object($value219) && method_exists($value219, '__toString')) ? $value219 : htmlspecialchars($value219, ($arguments217['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments217['encoding'], $arguments217['doubleEncode']);

$output216 .= '"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments220 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments221 = array();
$arguments221['source'] = 'Modules';
$arguments221['id'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.label', $renderingContext);
$arguments221['value'] = NULL;
$arguments221['arguments'] = array (
);
$arguments221['package'] = NULL;
$arguments221['quantity'] = NULL;
$arguments221['languageIdentifier'] = NULL;
$renderChildrenClosure222 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper223 = $self->getViewHelper('$viewHelper223', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper223->setArguments($arguments221);
$viewHelper223->setRenderingContext($renderingContext);
$viewHelper223->setRenderChildrenClosure($renderChildrenClosure222);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments220['value'] = $viewHelper223->initializeArgumentsAndRender();
$arguments220['keepQuotes'] = false;
$arguments220['encoding'] = 'UTF-8';
$arguments220['doubleEncode'] = true;
$renderChildrenClosure224 = function() use ($renderingContext, $self) {
return NULL;
};
$value225 = ($arguments220['value'] !== NULL ? $arguments220['value'] : $renderChildrenClosure224());

$output216 .= !is_string($value225) && !(is_object($value225) && method_exists($value225, '__toString')) ? $value225 : htmlspecialchars($value225, ($arguments220['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments220['encoding'], $arguments220['doubleEncode']);
return $output216;
};
$viewHelper226 = $self->getViewHelper('$viewHelper226', $renderingContext, 'TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper');
$viewHelper226->setArguments($arguments204);
$viewHelper226->setRenderingContext($renderingContext);
$viewHelper226->setRenderChildrenClosure($renderChildrenClosure215);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper

$output203 .= $viewHelper226->initializeArgumentsAndRender();

$output203 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments227 = array();
// Rendering Boolean node
$arguments227['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'iterator.isLast', $renderingContext));
$arguments227['then'] = NULL;
$arguments227['else'] = NULL;
$renderChildrenClosure228 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments229 = array();
$renderChildrenClosure230 = function() use ($renderingContext, $self) {
return '<span class="neos-divider">/</span>';
};
$viewHelper231 = $self->getViewHelper('$viewHelper231', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper231->setArguments($arguments229);
$viewHelper231->setRenderingContext($renderingContext);
$viewHelper231->setRenderChildrenClosure($renderChildrenClosure230);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
return $viewHelper231->initializeArgumentsAndRender();
};
$arguments227['__elseClosure'] = function() use ($renderingContext, $self) {
return '<span class="neos-divider">/</span>';
};
$viewHelper232 = $self->getViewHelper('$viewHelper232', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper232->setArguments($arguments227);
$viewHelper232->setRenderingContext($renderingContext);
$viewHelper232->setRenderChildrenClosure($renderChildrenClosure228);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output203 .= $viewHelper232->initializeArgumentsAndRender();

$output203 .= '
							</li>
						';
return $output203;
};
$viewHelper233 = $self->getViewHelper('$viewHelper233', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper233->setArguments($arguments167);
$viewHelper233->setRenderingContext($renderingContext);
$viewHelper233->setRenderChildrenClosure($renderChildrenClosure168);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output166 .= $viewHelper233->initializeArgumentsAndRender();

$output166 .= '
				';
return $output166;
};

$output160 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments164, $renderChildrenClosure165, $renderingContext);

$output160 .= '
			</ul>
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments234 = array();
// Rendering Boolean node
$arguments234['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'moduleContents', $renderingContext));
$arguments234['then'] = NULL;
$arguments234['else'] = NULL;
$renderChildrenClosure235 = function() use ($renderingContext, $self) {
$output236 = '';

$output236 .= '
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\RawViewHelper
$arguments237 = array();
$arguments237['value'] = NULL;
$renderChildrenClosure238 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'moduleContents', $renderingContext);
};
$viewHelper239 = $self->getViewHelper('$viewHelper239', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\RawViewHelper');
$viewHelper239->setArguments($arguments237);
$viewHelper239->setRenderingContext($renderingContext);
$viewHelper239->setRenderChildrenClosure($renderChildrenClosure238);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\RawViewHelper

$output236 .= $viewHelper239->initializeArgumentsAndRender();

$output236 .= '
			';
return $output236;
};
$viewHelper240 = $self->getViewHelper('$viewHelper240', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper240->setArguments($arguments234);
$viewHelper240->setRenderingContext($renderingContext);
$viewHelper240->setRenderChildrenClosure($renderChildrenClosure235);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output160 .= $viewHelper240->initializeArgumentsAndRender();

$output160 .= '
			<div id="neos-application" class="neos">
				<div id="neos-top-bar">
					<div id="neos-top-bar-right">
						<div id="neos-user-actions">
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper
$arguments241 = array();
$arguments241['partial'] = 'Backend/UserMenu';
$arguments241['arguments'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), '_all', $renderingContext);
$arguments241['section'] = NULL;
$arguments241['optional'] = false;
$renderChildrenClosure242 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper243 = $self->getViewHelper('$viewHelper243', $renderingContext, 'TYPO3\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper243->setArguments($arguments241);
$viewHelper243->setRenderingContext($renderingContext);
$viewHelper243->setRenderChildrenClosure($renderChildrenClosure242);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper

$output160 .= $viewHelper243->initializeArgumentsAndRender();

$output160 .= '
						</div>
					</div>
				</div>
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper
$arguments244 = array();
$arguments244['partial'] = 'Backend/Menu';
// Rendering Array
$array245 = array();
$array245['sites'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sites', $renderingContext);
$array245['modules'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'modules', $renderingContext);
$array245['modulePath'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'moduleConfiguration.path', $renderingContext);
$arguments244['arguments'] = $array245;
$arguments244['section'] = NULL;
$arguments244['optional'] = false;
$renderChildrenClosure246 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper247 = $self->getViewHelper('$viewHelper247', $renderingContext, 'TYPO3\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper247->setArguments($arguments244);
$viewHelper247->setRenderingContext($renderingContext);
$viewHelper247->setRenderChildrenClosure($renderChildrenClosure246);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper

$output160 .= $viewHelper247->initializeArgumentsAndRender();

$output160 .= '
			</div>
		</div>
		<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments248 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments249 = array();
$arguments249['path'] = '2/js/bootstrap.min.js';
$arguments249['package'] = 'TYPO3.Twitter.Bootstrap';
$arguments249['resource'] = NULL;
$arguments249['localize'] = true;
$renderChildrenClosure250 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper251 = $self->getViewHelper('$viewHelper251', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper251->setArguments($arguments249);
$viewHelper251->setRenderingContext($renderingContext);
$viewHelper251->setRenderChildrenClosure($renderChildrenClosure250);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments248['value'] = $viewHelper251->initializeArgumentsAndRender();
$arguments248['keepQuotes'] = false;
$arguments248['encoding'] = 'UTF-8';
$arguments248['doubleEncode'] = true;
$renderChildrenClosure252 = function() use ($renderingContext, $self) {
return NULL;
};
$value253 = ($arguments248['value'] !== NULL ? $arguments248['value'] : $renderChildrenClosure252());

$output160 .= !is_string($value253) && !(is_object($value253) && method_exists($value253, '__toString')) ? $value253 : htmlspecialchars($value253, ($arguments248['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments248['encoding'], $arguments248['doubleEncode']);

$output160 .= '"></script>
		<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments254 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments255 = array();
$arguments255['path'] = 'Library/fixedsticky/fixedsticky.js';
$arguments255['package'] = 'TYPO3.Neos';
$arguments255['resource'] = NULL;
$arguments255['localize'] = true;
$renderChildrenClosure256 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper257 = $self->getViewHelper('$viewHelper257', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper257->setArguments($arguments255);
$viewHelper257->setRenderingContext($renderingContext);
$viewHelper257->setRenderChildrenClosure($renderChildrenClosure256);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments254['value'] = $viewHelper257->initializeArgumentsAndRender();
$arguments254['keepQuotes'] = false;
$arguments254['encoding'] = 'UTF-8';
$arguments254['doubleEncode'] = true;
$renderChildrenClosure258 = function() use ($renderingContext, $self) {
return NULL;
};
$value259 = ($arguments254['value'] !== NULL ? $arguments254['value'] : $renderChildrenClosure258());

$output160 .= !is_string($value259) && !(is_object($value259) && method_exists($value259, '__toString')) ? $value259 : htmlspecialchars($value259, ($arguments254['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments254['encoding'], $arguments254['doubleEncode']);

$output160 .= '"></script>

		<script type="text/javascript">
			(function($) {
				$(function() {
					$(\'.neos-footer\').fixedsticky();
				});
			})(jQuery);
		</script>

		<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments260 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments261 = array();
$arguments261['path'] = 'Library/requirejs/require.js';
$arguments261['package'] = 'TYPO3.Neos';
$arguments261['resource'] = NULL;
$arguments261['localize'] = true;
$renderChildrenClosure262 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper263 = $self->getViewHelper('$viewHelper263', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper263->setArguments($arguments261);
$viewHelper263->setRenderingContext($renderingContext);
$viewHelper263->setRenderChildrenClosure($renderChildrenClosure262);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments260['value'] = $viewHelper263->initializeArgumentsAndRender();
$arguments260['keepQuotes'] = false;
$arguments260['encoding'] = 'UTF-8';
$arguments260['doubleEncode'] = true;
$renderChildrenClosure264 = function() use ($renderingContext, $self) {
return NULL;
};
$value265 = ($arguments260['value'] !== NULL ? $arguments260['value'] : $renderChildrenClosure264());

$output160 .= !is_string($value265) && !(is_object($value265) && method_exists($value265, '__toString')) ? $value265 : htmlspecialchars($value265, ($arguments260['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments260['encoding'], $arguments260['doubleEncode']);

$output160 .= '"></script>
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments266 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\ShouldLoadMinifiedJavascriptViewHelper
$arguments267 = array();
$renderChildrenClosure268 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper269 = $self->getViewHelper('$viewHelper269', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\ShouldLoadMinifiedJavascriptViewHelper');
$viewHelper269->setArguments($arguments267);
$viewHelper269->setRenderingContext($renderingContext);
$viewHelper269->setRenderChildrenClosure($renderChildrenClosure268);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\ShouldLoadMinifiedJavascriptViewHelper
$arguments266['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean($viewHelper269->initializeArgumentsAndRender());
$arguments266['then'] = NULL;
$arguments266['else'] = NULL;
$renderChildrenClosure270 = function() use ($renderingContext, $self) {
$output271 = '';

$output271 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments272 = array();
$renderChildrenClosure273 = function() use ($renderingContext, $self) {
$output274 = '';

$output274 .= '
				<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments275 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments276 = array();
$arguments276['path'] = 'JavaScript/ContentModule-built.js';
$arguments276['package'] = 'TYPO3.Neos';
$arguments276['resource'] = NULL;
$arguments276['localize'] = true;
$renderChildrenClosure277 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper278 = $self->getViewHelper('$viewHelper278', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper278->setArguments($arguments276);
$viewHelper278->setRenderingContext($renderingContext);
$viewHelper278->setRenderChildrenClosure($renderChildrenClosure277);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments275['value'] = $viewHelper278->initializeArgumentsAndRender();
$arguments275['keepQuotes'] = false;
$arguments275['encoding'] = 'UTF-8';
$arguments275['doubleEncode'] = true;
$renderChildrenClosure279 = function() use ($renderingContext, $self) {
return NULL;
};
$value280 = ($arguments275['value'] !== NULL ? $arguments275['value'] : $renderChildrenClosure279());

$output274 .= !is_string($value280) && !(is_object($value280) && method_exists($value280, '__toString')) ? $value280 : htmlspecialchars($value280, ($arguments275['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments275['encoding'], $arguments275['doubleEncode']);

$output274 .= '?bust=';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments281 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\JavascriptBuiltVersionViewHelper
$arguments282 = array();
$renderChildrenClosure283 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper284 = $self->getViewHelper('$viewHelper284', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\JavascriptBuiltVersionViewHelper');
$viewHelper284->setArguments($arguments282);
$viewHelper284->setRenderingContext($renderingContext);
$viewHelper284->setRenderChildrenClosure($renderChildrenClosure283);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\JavascriptBuiltVersionViewHelper
$arguments281['value'] = $viewHelper284->initializeArgumentsAndRender();
$arguments281['keepQuotes'] = false;
$arguments281['encoding'] = 'UTF-8';
$arguments281['doubleEncode'] = true;
$renderChildrenClosure285 = function() use ($renderingContext, $self) {
return NULL;
};
$value286 = ($arguments281['value'] !== NULL ? $arguments281['value'] : $renderChildrenClosure285());

$output274 .= !is_string($value286) && !(is_object($value286) && method_exists($value286, '__toString')) ? $value286 : htmlspecialchars($value286, ($arguments281['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments281['encoding'], $arguments281['doubleEncode']);

$output274 .= '"></script>
			';
return $output274;
};
$viewHelper287 = $self->getViewHelper('$viewHelper287', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper287->setArguments($arguments272);
$viewHelper287->setRenderingContext($renderingContext);
$viewHelper287->setRenderChildrenClosure($renderChildrenClosure273);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output271 .= $viewHelper287->initializeArgumentsAndRender();

$output271 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments288 = array();
$renderChildrenClosure289 = function() use ($renderingContext, $self) {
$output290 = '';

$output290 .= '
				<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments291 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments292 = array();
$arguments292['path'] = 'JavaScript/ContentModuleBootstrap.js';
$arguments292['package'] = 'TYPO3.Neos';
$arguments292['resource'] = NULL;
$arguments292['localize'] = true;
$renderChildrenClosure293 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper294 = $self->getViewHelper('$viewHelper294', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper294->setArguments($arguments292);
$viewHelper294->setRenderingContext($renderingContext);
$viewHelper294->setRenderChildrenClosure($renderChildrenClosure293);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments291['value'] = $viewHelper294->initializeArgumentsAndRender();
$arguments291['keepQuotes'] = false;
$arguments291['encoding'] = 'UTF-8';
$arguments291['doubleEncode'] = true;
$renderChildrenClosure295 = function() use ($renderingContext, $self) {
return NULL;
};
$value296 = ($arguments291['value'] !== NULL ? $arguments291['value'] : $renderChildrenClosure295());

$output290 .= !is_string($value296) && !(is_object($value296) && method_exists($value296, '__toString')) ? $value296 : htmlspecialchars($value296, ($arguments291['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments291['encoding'], $arguments291['doubleEncode']);

$output290 .= '"></script>
			';
return $output290;
};
$viewHelper297 = $self->getViewHelper('$viewHelper297', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper297->setArguments($arguments288);
$viewHelper297->setRenderingContext($renderingContext);
$viewHelper297->setRenderChildrenClosure($renderChildrenClosure289);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output271 .= $viewHelper297->initializeArgumentsAndRender();

$output271 .= '
		';
return $output271;
};
$arguments266['__thenClosure'] = function() use ($renderingContext, $self) {
$output298 = '';

$output298 .= '
				<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments299 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments300 = array();
$arguments300['path'] = 'JavaScript/ContentModule-built.js';
$arguments300['package'] = 'TYPO3.Neos';
$arguments300['resource'] = NULL;
$arguments300['localize'] = true;
$renderChildrenClosure301 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper302 = $self->getViewHelper('$viewHelper302', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper302->setArguments($arguments300);
$viewHelper302->setRenderingContext($renderingContext);
$viewHelper302->setRenderChildrenClosure($renderChildrenClosure301);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments299['value'] = $viewHelper302->initializeArgumentsAndRender();
$arguments299['keepQuotes'] = false;
$arguments299['encoding'] = 'UTF-8';
$arguments299['doubleEncode'] = true;
$renderChildrenClosure303 = function() use ($renderingContext, $self) {
return NULL;
};
$value304 = ($arguments299['value'] !== NULL ? $arguments299['value'] : $renderChildrenClosure303());

$output298 .= !is_string($value304) && !(is_object($value304) && method_exists($value304, '__toString')) ? $value304 : htmlspecialchars($value304, ($arguments299['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments299['encoding'], $arguments299['doubleEncode']);

$output298 .= '?bust=';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments305 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\JavascriptBuiltVersionViewHelper
$arguments306 = array();
$renderChildrenClosure307 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper308 = $self->getViewHelper('$viewHelper308', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\JavascriptBuiltVersionViewHelper');
$viewHelper308->setArguments($arguments306);
$viewHelper308->setRenderingContext($renderingContext);
$viewHelper308->setRenderChildrenClosure($renderChildrenClosure307);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\JavascriptBuiltVersionViewHelper
$arguments305['value'] = $viewHelper308->initializeArgumentsAndRender();
$arguments305['keepQuotes'] = false;
$arguments305['encoding'] = 'UTF-8';
$arguments305['doubleEncode'] = true;
$renderChildrenClosure309 = function() use ($renderingContext, $self) {
return NULL;
};
$value310 = ($arguments305['value'] !== NULL ? $arguments305['value'] : $renderChildrenClosure309());

$output298 .= !is_string($value310) && !(is_object($value310) && method_exists($value310, '__toString')) ? $value310 : htmlspecialchars($value310, ($arguments305['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments305['encoding'], $arguments305['doubleEncode']);

$output298 .= '"></script>
			';
return $output298;
};
$arguments266['__elseClosure'] = function() use ($renderingContext, $self) {
$output311 = '';

$output311 .= '
				<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments312 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments313 = array();
$arguments313['path'] = 'JavaScript/ContentModuleBootstrap.js';
$arguments313['package'] = 'TYPO3.Neos';
$arguments313['resource'] = NULL;
$arguments313['localize'] = true;
$renderChildrenClosure314 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper315 = $self->getViewHelper('$viewHelper315', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper315->setArguments($arguments313);
$viewHelper315->setRenderingContext($renderingContext);
$viewHelper315->setRenderChildrenClosure($renderChildrenClosure314);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments312['value'] = $viewHelper315->initializeArgumentsAndRender();
$arguments312['keepQuotes'] = false;
$arguments312['encoding'] = 'UTF-8';
$arguments312['doubleEncode'] = true;
$renderChildrenClosure316 = function() use ($renderingContext, $self) {
return NULL;
};
$value317 = ($arguments312['value'] !== NULL ? $arguments312['value'] : $renderChildrenClosure316());

$output311 .= !is_string($value317) && !(is_object($value317) && method_exists($value317, '__toString')) ? $value317 : htmlspecialchars($value317, ($arguments312['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments312['encoding'], $arguments312['doubleEncode']);

$output311 .= '"></script>
			';
return $output311;
};
$viewHelper318 = $self->getViewHelper('$viewHelper318', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper318->setArguments($arguments266);
$viewHelper318->setRenderingContext($renderingContext);
$viewHelper318->setRenderChildrenClosure($renderChildrenClosure270);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output160 .= $viewHelper318->initializeArgumentsAndRender();

$output160 .= '

		<script>
			$(function() {
				require(
					';
$output319 = '';

$output319 .= '{
						baseUrl: \'';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments320 = array();
$arguments320['path'] = 'JavaScript';
$arguments320['package'] = NULL;
$arguments320['resource'] = NULL;
$arguments320['localize'] = true;
$renderChildrenClosure321 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper322 = $self->getViewHelper('$viewHelper322', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper322->setArguments($arguments320);
$viewHelper322->setRenderingContext($renderingContext);
$viewHelper322->setRenderChildrenClosure($renderChildrenClosure321);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper

$output319 .= $viewHelper322->initializeArgumentsAndRender();

$output319 .= '\',
						paths: requirePaths,
						context: \'neos\',
						locale: \'en\'
					}';

$output160 .= $output319;

$output160 .= ',
					[
						\'Library/jquery-with-dependencies\'
					],
					function($) {
						$(\'[data-neos-toggle="popover"]\').popover();
						$(\'[data-neos-toggle="tooltip"]\').tooltip();
					}
				)
			});
		</script>
	</body>
';

return $output160;
}
/**
 * Main Render function
 */
public function render(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output323 = '';

$output323 .= '
';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments324 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\LayoutViewHelper
$arguments325 = array();
$arguments325['name'] = 'Default';
$renderChildrenClosure326 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper327 = $self->getViewHelper('$viewHelper327', $renderingContext, 'TYPO3\Fluid\ViewHelpers\LayoutViewHelper');
$viewHelper327->setArguments($arguments325);
$viewHelper327->setRenderingContext($renderingContext);
$viewHelper327->setRenderChildrenClosure($renderChildrenClosure326);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\LayoutViewHelper
$arguments324['value'] = $viewHelper327->initializeArgumentsAndRender();
$arguments324['keepQuotes'] = false;
$arguments324['encoding'] = 'UTF-8';
$arguments324['doubleEncode'] = true;
$renderChildrenClosure328 = function() use ($renderingContext, $self) {
return NULL;
};
$value329 = ($arguments324['value'] !== NULL ? $arguments324['value'] : $renderChildrenClosure328());

$output323 .= !is_string($value329) && !(is_object($value329) && method_exists($value329, '__toString')) ? $value329 : htmlspecialchars($value329, ($arguments324['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments324['encoding'], $arguments324['doubleEncode']);

$output323 .= '

';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\SectionViewHelper
$arguments330 = array();
$arguments330['name'] = 'head';
$renderChildrenClosure331 = function() use ($renderingContext, $self) {
$output332 = '';

$output332 .= '
	<title>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments333 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments334 = array();
$arguments334['id'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'title', $renderingContext);
$arguments334['value'] = NULL;
$arguments334['arguments'] = array (
);
$arguments334['source'] = 'Main';
$arguments334['package'] = NULL;
$arguments334['quantity'] = NULL;
$arguments334['languageIdentifier'] = NULL;
$renderChildrenClosure335 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper336 = $self->getViewHelper('$viewHelper336', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper336->setArguments($arguments334);
$viewHelper336->setRenderingContext($renderingContext);
$viewHelper336->setRenderChildrenClosure($renderChildrenClosure335);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments333['value'] = $viewHelper336->initializeArgumentsAndRender();
$arguments333['keepQuotes'] = false;
$arguments333['encoding'] = 'UTF-8';
$arguments333['doubleEncode'] = true;
$renderChildrenClosure337 = function() use ($renderingContext, $self) {
return NULL;
};
$value338 = ($arguments333['value'] !== NULL ? $arguments333['value'] : $renderChildrenClosure337());

$output332 .= !is_string($value338) && !(is_object($value338) && method_exists($value338, '__toString')) ? $value338 : htmlspecialchars($value338, ($arguments333['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments333['encoding'], $arguments333['doubleEncode']);

$output332 .= '</title>

	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments339 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\ShouldLoadMinifiedJavascriptViewHelper
$arguments340 = array();
$renderChildrenClosure341 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper342 = $self->getViewHelper('$viewHelper342', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\ShouldLoadMinifiedJavascriptViewHelper');
$viewHelper342->setArguments($arguments340);
$viewHelper342->setRenderingContext($renderingContext);
$viewHelper342->setRenderChildrenClosure($renderChildrenClosure341);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\ShouldLoadMinifiedJavascriptViewHelper
$arguments339['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean($viewHelper342->initializeArgumentsAndRender());
$arguments339['then'] = NULL;
$arguments339['else'] = NULL;
$renderChildrenClosure343 = function() use ($renderingContext, $self) {
$output344 = '';

$output344 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments345 = array();
$renderChildrenClosure346 = function() use ($renderingContext, $self) {
$output347 = '';

$output347 .= '
			<link rel="stylesheet" type="text/css"
				  href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments348 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments349 = array();
$arguments349['path'] = 'Styles/Includes-built.css';
$arguments349['package'] = 'TYPO3.Neos';
$arguments349['resource'] = NULL;
$arguments349['localize'] = true;
$renderChildrenClosure350 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper351 = $self->getViewHelper('$viewHelper351', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper351->setArguments($arguments349);
$viewHelper351->setRenderingContext($renderingContext);
$viewHelper351->setRenderChildrenClosure($renderChildrenClosure350);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments348['value'] = $viewHelper351->initializeArgumentsAndRender();
$arguments348['keepQuotes'] = false;
$arguments348['encoding'] = 'UTF-8';
$arguments348['doubleEncode'] = true;
$renderChildrenClosure352 = function() use ($renderingContext, $self) {
return NULL;
};
$value353 = ($arguments348['value'] !== NULL ? $arguments348['value'] : $renderChildrenClosure352());

$output347 .= !is_string($value353) && !(is_object($value353) && method_exists($value353, '__toString')) ? $value353 : htmlspecialchars($value353, ($arguments348['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments348['encoding'], $arguments348['doubleEncode']);

$output347 .= '?bust=';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments354 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\CssBuiltVersionViewHelper
$arguments355 = array();
$renderChildrenClosure356 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper357 = $self->getViewHelper('$viewHelper357', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\CssBuiltVersionViewHelper');
$viewHelper357->setArguments($arguments355);
$viewHelper357->setRenderingContext($renderingContext);
$viewHelper357->setRenderChildrenClosure($renderChildrenClosure356);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\CssBuiltVersionViewHelper
$arguments354['value'] = $viewHelper357->initializeArgumentsAndRender();
$arguments354['keepQuotes'] = false;
$arguments354['encoding'] = 'UTF-8';
$arguments354['doubleEncode'] = true;
$renderChildrenClosure358 = function() use ($renderingContext, $self) {
return NULL;
};
$value359 = ($arguments354['value'] !== NULL ? $arguments354['value'] : $renderChildrenClosure358());

$output347 .= !is_string($value359) && !(is_object($value359) && method_exists($value359, '__toString')) ? $value359 : htmlspecialchars($value359, ($arguments354['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments354['encoding'], $arguments354['doubleEncode']);

$output347 .= '"/>
		';
return $output347;
};
$viewHelper360 = $self->getViewHelper('$viewHelper360', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper360->setArguments($arguments345);
$viewHelper360->setRenderingContext($renderingContext);
$viewHelper360->setRenderChildrenClosure($renderChildrenClosure346);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output344 .= $viewHelper360->initializeArgumentsAndRender();

$output344 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments361 = array();
$renderChildrenClosure362 = function() use ($renderingContext, $self) {
$output363 = '';

$output363 .= '
			<link rel="stylesheet" type="text/css"
				  href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments364 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments365 = array();
$arguments365['path'] = 'Styles/Includes.css';
$arguments365['package'] = 'TYPO3.Neos';
$arguments365['resource'] = NULL;
$arguments365['localize'] = true;
$renderChildrenClosure366 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper367 = $self->getViewHelper('$viewHelper367', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper367->setArguments($arguments365);
$viewHelper367->setRenderingContext($renderingContext);
$viewHelper367->setRenderChildrenClosure($renderChildrenClosure366);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments364['value'] = $viewHelper367->initializeArgumentsAndRender();
$arguments364['keepQuotes'] = false;
$arguments364['encoding'] = 'UTF-8';
$arguments364['doubleEncode'] = true;
$renderChildrenClosure368 = function() use ($renderingContext, $self) {
return NULL;
};
$value369 = ($arguments364['value'] !== NULL ? $arguments364['value'] : $renderChildrenClosure368());

$output363 .= !is_string($value369) && !(is_object($value369) && method_exists($value369, '__toString')) ? $value369 : htmlspecialchars($value369, ($arguments364['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments364['encoding'], $arguments364['doubleEncode']);

$output363 .= '"/>
		';
return $output363;
};
$viewHelper370 = $self->getViewHelper('$viewHelper370', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper370->setArguments($arguments361);
$viewHelper370->setRenderingContext($renderingContext);
$viewHelper370->setRenderChildrenClosure($renderChildrenClosure362);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output344 .= $viewHelper370->initializeArgumentsAndRender();

$output344 .= '
	';
return $output344;
};
$arguments339['__thenClosure'] = function() use ($renderingContext, $self) {
$output371 = '';

$output371 .= '
			<link rel="stylesheet" type="text/css"
				  href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments372 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments373 = array();
$arguments373['path'] = 'Styles/Includes-built.css';
$arguments373['package'] = 'TYPO3.Neos';
$arguments373['resource'] = NULL;
$arguments373['localize'] = true;
$renderChildrenClosure374 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper375 = $self->getViewHelper('$viewHelper375', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper375->setArguments($arguments373);
$viewHelper375->setRenderingContext($renderingContext);
$viewHelper375->setRenderChildrenClosure($renderChildrenClosure374);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments372['value'] = $viewHelper375->initializeArgumentsAndRender();
$arguments372['keepQuotes'] = false;
$arguments372['encoding'] = 'UTF-8';
$arguments372['doubleEncode'] = true;
$renderChildrenClosure376 = function() use ($renderingContext, $self) {
return NULL;
};
$value377 = ($arguments372['value'] !== NULL ? $arguments372['value'] : $renderChildrenClosure376());

$output371 .= !is_string($value377) && !(is_object($value377) && method_exists($value377, '__toString')) ? $value377 : htmlspecialchars($value377, ($arguments372['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments372['encoding'], $arguments372['doubleEncode']);

$output371 .= '?bust=';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments378 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\CssBuiltVersionViewHelper
$arguments379 = array();
$renderChildrenClosure380 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper381 = $self->getViewHelper('$viewHelper381', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\CssBuiltVersionViewHelper');
$viewHelper381->setArguments($arguments379);
$viewHelper381->setRenderingContext($renderingContext);
$viewHelper381->setRenderChildrenClosure($renderChildrenClosure380);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\CssBuiltVersionViewHelper
$arguments378['value'] = $viewHelper381->initializeArgumentsAndRender();
$arguments378['keepQuotes'] = false;
$arguments378['encoding'] = 'UTF-8';
$arguments378['doubleEncode'] = true;
$renderChildrenClosure382 = function() use ($renderingContext, $self) {
return NULL;
};
$value383 = ($arguments378['value'] !== NULL ? $arguments378['value'] : $renderChildrenClosure382());

$output371 .= !is_string($value383) && !(is_object($value383) && method_exists($value383, '__toString')) ? $value383 : htmlspecialchars($value383, ($arguments378['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments378['encoding'], $arguments378['doubleEncode']);

$output371 .= '"/>
		';
return $output371;
};
$arguments339['__elseClosure'] = function() use ($renderingContext, $self) {
$output384 = '';

$output384 .= '
			<link rel="stylesheet" type="text/css"
				  href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments385 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments386 = array();
$arguments386['path'] = 'Styles/Includes.css';
$arguments386['package'] = 'TYPO3.Neos';
$arguments386['resource'] = NULL;
$arguments386['localize'] = true;
$renderChildrenClosure387 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper388 = $self->getViewHelper('$viewHelper388', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper388->setArguments($arguments386);
$viewHelper388->setRenderingContext($renderingContext);
$viewHelper388->setRenderChildrenClosure($renderChildrenClosure387);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments385['value'] = $viewHelper388->initializeArgumentsAndRender();
$arguments385['keepQuotes'] = false;
$arguments385['encoding'] = 'UTF-8';
$arguments385['doubleEncode'] = true;
$renderChildrenClosure389 = function() use ($renderingContext, $self) {
return NULL;
};
$value390 = ($arguments385['value'] !== NULL ? $arguments385['value'] : $renderChildrenClosure389());

$output384 .= !is_string($value390) && !(is_object($value390) && method_exists($value390, '__toString')) ? $value390 : htmlspecialchars($value390, ($arguments385['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments385['encoding'], $arguments385['doubleEncode']);

$output384 .= '"/>
		';
return $output384;
};
$viewHelper391 = $self->getViewHelper('$viewHelper391', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper391->setArguments($arguments339);
$viewHelper391->setRenderingContext($renderingContext);
$viewHelper391->setRenderChildrenClosure($renderChildrenClosure343);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output332 .= $viewHelper391->initializeArgumentsAndRender();

$output332 .= '

	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments392 = array();
// Rendering Boolean node
$arguments392['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'moduleConfiguration.additionalResources.styleSheets', $renderingContext));
$arguments392['then'] = NULL;
$arguments392['else'] = NULL;
$renderChildrenClosure393 = function() use ($renderingContext, $self) {
$output394 = '';

$output394 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments395 = array();
$arguments395['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'moduleConfiguration.additionalResources.styleSheets', $renderingContext);
$arguments395['as'] = 'additionalResource';
$arguments395['key'] = '';
$arguments395['reverse'] = false;
$arguments395['iteration'] = NULL;
$renderChildrenClosure396 = function() use ($renderingContext, $self) {
$output397 = '';

$output397 .= '
			<link rel="stylesheet" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments398 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments399 = array();
$arguments399['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'additionalResource', $renderingContext);
$arguments399['package'] = NULL;
$arguments399['resource'] = NULL;
$arguments399['localize'] = true;
$renderChildrenClosure400 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper401 = $self->getViewHelper('$viewHelper401', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper401->setArguments($arguments399);
$viewHelper401->setRenderingContext($renderingContext);
$viewHelper401->setRenderChildrenClosure($renderChildrenClosure400);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments398['value'] = $viewHelper401->initializeArgumentsAndRender();
$arguments398['keepQuotes'] = false;
$arguments398['encoding'] = 'UTF-8';
$arguments398['doubleEncode'] = true;
$renderChildrenClosure402 = function() use ($renderingContext, $self) {
return NULL;
};
$value403 = ($arguments398['value'] !== NULL ? $arguments398['value'] : $renderChildrenClosure402());

$output397 .= !is_string($value403) && !(is_object($value403) && method_exists($value403, '__toString')) ? $value403 : htmlspecialchars($value403, ($arguments398['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments398['encoding'], $arguments398['doubleEncode']);

$output397 .= '" />
		';
return $output397;
};

$output394 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments395, $renderChildrenClosure396, $renderingContext);

$output394 .= '
	';
return $output394;
};
$viewHelper404 = $self->getViewHelper('$viewHelper404', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper404->setArguments($arguments392);
$viewHelper404->setRenderingContext($renderingContext);
$viewHelper404->setRenderChildrenClosure($renderChildrenClosure393);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output332 .= $viewHelper404->initializeArgumentsAndRender();

$output332 .= '

	<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments405 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments406 = array();
$arguments406['path'] = 'Library/jquery/jquery-2.0.3.js';
$arguments406['package'] = NULL;
$arguments406['resource'] = NULL;
$arguments406['localize'] = true;
$renderChildrenClosure407 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper408 = $self->getViewHelper('$viewHelper408', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper408->setArguments($arguments406);
$viewHelper408->setRenderingContext($renderingContext);
$viewHelper408->setRenderChildrenClosure($renderChildrenClosure407);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments405['value'] = $viewHelper408->initializeArgumentsAndRender();
$arguments405['keepQuotes'] = false;
$arguments405['encoding'] = 'UTF-8';
$arguments405['doubleEncode'] = true;
$renderChildrenClosure409 = function() use ($renderingContext, $self) {
return NULL;
};
$value410 = ($arguments405['value'] !== NULL ? $arguments405['value'] : $renderChildrenClosure409());

$output332 .= !is_string($value410) && !(is_object($value410) && method_exists($value410, '__toString')) ? $value410 : htmlspecialchars($value410, ($arguments405['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments405['encoding'], $arguments405['doubleEncode']);

$output332 .= '"></script>
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments411 = array();
// Rendering Boolean node
$arguments411['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'moduleConfiguration.additionalResources.javaScripts', $renderingContext));
$arguments411['then'] = NULL;
$arguments411['else'] = NULL;
$renderChildrenClosure412 = function() use ($renderingContext, $self) {
$output413 = '';

$output413 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments414 = array();
$arguments414['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'moduleConfiguration.additionalResources.javaScripts', $renderingContext);
$arguments414['as'] = 'additionalResource';
$arguments414['key'] = '';
$arguments414['reverse'] = false;
$arguments414['iteration'] = NULL;
$renderChildrenClosure415 = function() use ($renderingContext, $self) {
$output416 = '';

$output416 .= '
			<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments417 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments418 = array();
$arguments418['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'additionalResource', $renderingContext);
$arguments418['package'] = NULL;
$arguments418['resource'] = NULL;
$arguments418['localize'] = true;
$renderChildrenClosure419 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper420 = $self->getViewHelper('$viewHelper420', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper420->setArguments($arguments418);
$viewHelper420->setRenderingContext($renderingContext);
$viewHelper420->setRenderChildrenClosure($renderChildrenClosure419);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments417['value'] = $viewHelper420->initializeArgumentsAndRender();
$arguments417['keepQuotes'] = false;
$arguments417['encoding'] = 'UTF-8';
$arguments417['doubleEncode'] = true;
$renderChildrenClosure421 = function() use ($renderingContext, $self) {
return NULL;
};
$value422 = ($arguments417['value'] !== NULL ? $arguments417['value'] : $renderChildrenClosure421());

$output416 .= !is_string($value422) && !(is_object($value422) && method_exists($value422, '__toString')) ? $value422 : htmlspecialchars($value422, ($arguments417['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments417['encoding'], $arguments417['doubleEncode']);

$output416 .= '"></script>
		';
return $output416;
};

$output413 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments414, $renderChildrenClosure415, $renderingContext);

$output413 .= '
	';
return $output413;
};
$viewHelper423 = $self->getViewHelper('$viewHelper423', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper423->setArguments($arguments411);
$viewHelper423->setRenderingContext($renderingContext);
$viewHelper423->setRenderChildrenClosure($renderChildrenClosure412);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output332 .= $viewHelper423->initializeArgumentsAndRender();

$output332 .= '

	<script type="text/javascript">
		// TODO: Get rid of those global variables
		';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\JavascriptConfigurationViewHelper
$arguments424 = array();
$renderChildrenClosure425 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper426 = $self->getViewHelper('$viewHelper426', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\JavascriptConfigurationViewHelper');
$viewHelper426->setArguments($arguments424);
$viewHelper426->setRenderingContext($renderingContext);
$viewHelper426->setRenderChildrenClosure($renderChildrenClosure425);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\JavascriptConfigurationViewHelper

$output332 .= $viewHelper426->initializeArgumentsAndRender();

$output332 .= '
	</script>

	<link rel="neos-menudata" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments427 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments428 = array();
$arguments428['action'] = 'index';
$arguments428['controller'] = 'Backend\\Menu';
$arguments428['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments428['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
// Rendering Array
$array429 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments430 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper
$arguments431 = array();
$renderChildrenClosure432 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper433 = $self->getViewHelper('$viewHelper433', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper');
$viewHelper433->setArguments($arguments431);
$viewHelper433->setRenderingContext($renderingContext);
$viewHelper433->setRenderChildrenClosure($renderChildrenClosure432);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper
$arguments430['value'] = $viewHelper433->initializeArgumentsAndRender();
$arguments430['keepQuotes'] = false;
$arguments430['encoding'] = 'UTF-8';
$arguments430['doubleEncode'] = true;
$renderChildrenClosure434 = function() use ($renderingContext, $self) {
return NULL;
};
$value435 = ($arguments430['value'] !== NULL ? $arguments430['value'] : $renderChildrenClosure434());
$array429['version'] = !is_string($value435) && !(is_object($value435) && method_exists($value435, '__toString')) ? $value435 : htmlspecialchars($value435, ($arguments430['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments430['encoding'], $arguments430['doubleEncode']);
$arguments428['arguments'] = $array429;
$arguments428['subpackage'] = NULL;
$arguments428['section'] = '';
$arguments428['format'] = '';
$arguments428['additionalParams'] = array (
);
$arguments428['addQueryString'] = false;
$arguments428['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments428['useParentRequest'] = false;
$renderChildrenClosure436 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper437 = $self->getViewHelper('$viewHelper437', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper437->setArguments($arguments428);
$viewHelper437->setRenderingContext($renderingContext);
$viewHelper437->setRenderChildrenClosure($renderChildrenClosure436);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments427['value'] = $viewHelper437->initializeArgumentsAndRender();
$arguments427['keepQuotes'] = false;
$arguments427['encoding'] = 'UTF-8';
$arguments427['doubleEncode'] = true;
$renderChildrenClosure438 = function() use ($renderingContext, $self) {
return NULL;
};
$value439 = ($arguments427['value'] !== NULL ? $arguments427['value'] : $renderChildrenClosure438());

$output332 .= !is_string($value439) && !(is_object($value439) && method_exists($value439, '__toString')) ? $value439 : htmlspecialchars($value439, ($arguments427['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments427['encoding'], $arguments427['doubleEncode']);

$output332 .= '" />
	<!-- TODO: Make sure the schema information and edit / preview panel data isn\'t necessary in backend modules -->
	<link rel="neos-vieschema" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments440 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments441 = array();
$arguments441['action'] = 'vieSchema';
$arguments441['controller'] = 'Backend\\Schema';
$arguments441['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments441['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
// Rendering Array
$array442 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments443 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper
$arguments444 = array();
$renderChildrenClosure445 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper446 = $self->getViewHelper('$viewHelper446', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper');
$viewHelper446->setArguments($arguments444);
$viewHelper446->setRenderingContext($renderingContext);
$viewHelper446->setRenderChildrenClosure($renderChildrenClosure445);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper
$arguments443['value'] = $viewHelper446->initializeArgumentsAndRender();
$arguments443['keepQuotes'] = false;
$arguments443['encoding'] = 'UTF-8';
$arguments443['doubleEncode'] = true;
$renderChildrenClosure447 = function() use ($renderingContext, $self) {
return NULL;
};
$value448 = ($arguments443['value'] !== NULL ? $arguments443['value'] : $renderChildrenClosure447());
$array442['version'] = !is_string($value448) && !(is_object($value448) && method_exists($value448, '__toString')) ? $value448 : htmlspecialchars($value448, ($arguments443['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments443['encoding'], $arguments443['doubleEncode']);
$arguments441['arguments'] = $array442;
$arguments441['subpackage'] = NULL;
$arguments441['section'] = '';
$arguments441['format'] = '';
$arguments441['additionalParams'] = array (
);
$arguments441['addQueryString'] = false;
$arguments441['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments441['useParentRequest'] = false;
$renderChildrenClosure449 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper450 = $self->getViewHelper('$viewHelper450', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper450->setArguments($arguments441);
$viewHelper450->setRenderingContext($renderingContext);
$viewHelper450->setRenderChildrenClosure($renderChildrenClosure449);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments440['value'] = $viewHelper450->initializeArgumentsAndRender();
$arguments440['keepQuotes'] = false;
$arguments440['encoding'] = 'UTF-8';
$arguments440['doubleEncode'] = true;
$renderChildrenClosure451 = function() use ($renderingContext, $self) {
return NULL;
};
$value452 = ($arguments440['value'] !== NULL ? $arguments440['value'] : $renderChildrenClosure451());

$output332 .= !is_string($value452) && !(is_object($value452) && method_exists($value452, '__toString')) ? $value452 : htmlspecialchars($value452, ($arguments440['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments440['encoding'], $arguments440['doubleEncode']);

$output332 .= '" />
	<link rel="neos-nodetypeschema" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments453 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments454 = array();
$arguments454['action'] = 'nodeTypeSchema';
$arguments454['controller'] = 'Backend\\Schema';
$arguments454['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments454['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
// Rendering Array
$array455 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments456 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper
$arguments457 = array();
$renderChildrenClosure458 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper459 = $self->getViewHelper('$viewHelper459', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper');
$viewHelper459->setArguments($arguments457);
$viewHelper459->setRenderingContext($renderingContext);
$viewHelper459->setRenderChildrenClosure($renderChildrenClosure458);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper
$arguments456['value'] = $viewHelper459->initializeArgumentsAndRender();
$arguments456['keepQuotes'] = false;
$arguments456['encoding'] = 'UTF-8';
$arguments456['doubleEncode'] = true;
$renderChildrenClosure460 = function() use ($renderingContext, $self) {
return NULL;
};
$value461 = ($arguments456['value'] !== NULL ? $arguments456['value'] : $renderChildrenClosure460());
$array455['version'] = !is_string($value461) && !(is_object($value461) && method_exists($value461, '__toString')) ? $value461 : htmlspecialchars($value461, ($arguments456['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments456['encoding'], $arguments456['doubleEncode']);
$arguments454['arguments'] = $array455;
$arguments454['subpackage'] = NULL;
$arguments454['section'] = '';
$arguments454['format'] = '';
$arguments454['additionalParams'] = array (
);
$arguments454['addQueryString'] = false;
$arguments454['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments454['useParentRequest'] = false;
$renderChildrenClosure462 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper463 = $self->getViewHelper('$viewHelper463', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper463->setArguments($arguments454);
$viewHelper463->setRenderingContext($renderingContext);
$viewHelper463->setRenderChildrenClosure($renderChildrenClosure462);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments453['value'] = $viewHelper463->initializeArgumentsAndRender();
$arguments453['keepQuotes'] = false;
$arguments453['encoding'] = 'UTF-8';
$arguments453['doubleEncode'] = true;
$renderChildrenClosure464 = function() use ($renderingContext, $self) {
return NULL;
};
$value465 = ($arguments453['value'] !== NULL ? $arguments453['value'] : $renderChildrenClosure464());

$output332 .= !is_string($value465) && !(is_object($value465) && method_exists($value465, '__toString')) ? $value465 : htmlspecialchars($value465, ($arguments453['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments453['encoding'], $arguments453['doubleEncode']);

$output332 .= '" />
	<link rel="neos-editpreviewdata" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments466 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments467 = array();
$arguments467['action'] = 'editPreview';
$arguments467['controller'] = 'Backend\\Settings';
$arguments467['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments467['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
// Rendering Array
$array468 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments469 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper
$arguments470 = array();
$renderChildrenClosure471 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper472 = $self->getViewHelper('$viewHelper472', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper');
$viewHelper472->setArguments($arguments470);
$viewHelper472->setRenderingContext($renderingContext);
$viewHelper472->setRenderChildrenClosure($renderChildrenClosure471);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\ConfigurationCacheVersionViewHelper
$arguments469['value'] = $viewHelper472->initializeArgumentsAndRender();
$arguments469['keepQuotes'] = false;
$arguments469['encoding'] = 'UTF-8';
$arguments469['doubleEncode'] = true;
$renderChildrenClosure473 = function() use ($renderingContext, $self) {
return NULL;
};
$value474 = ($arguments469['value'] !== NULL ? $arguments469['value'] : $renderChildrenClosure473());
$array468['version'] = !is_string($value474) && !(is_object($value474) && method_exists($value474, '__toString')) ? $value474 : htmlspecialchars($value474, ($arguments469['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments469['encoding'], $arguments469['doubleEncode']);
$arguments467['arguments'] = $array468;
$arguments467['subpackage'] = NULL;
$arguments467['section'] = '';
$arguments467['format'] = '';
$arguments467['additionalParams'] = array (
);
$arguments467['addQueryString'] = false;
$arguments467['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments467['useParentRequest'] = false;
$renderChildrenClosure475 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper476 = $self->getViewHelper('$viewHelper476', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper476->setArguments($arguments467);
$viewHelper476->setRenderingContext($renderingContext);
$viewHelper476->setRenderChildrenClosure($renderChildrenClosure475);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments466['value'] = $viewHelper476->initializeArgumentsAndRender();
$arguments466['keepQuotes'] = false;
$arguments466['encoding'] = 'UTF-8';
$arguments466['doubleEncode'] = true;
$renderChildrenClosure477 = function() use ($renderingContext, $self) {
return NULL;
};
$value478 = ($arguments466['value'] !== NULL ? $arguments466['value'] : $renderChildrenClosure477());

$output332 .= !is_string($value478) && !(is_object($value478) && method_exists($value478, '__toString')) ? $value478 : htmlspecialchars($value478, ($arguments466['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments466['encoding'], $arguments466['doubleEncode']);

$output332 .= '" />
	<link rel="neos-xliff" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\RawViewHelper
$arguments479 = array();
$arguments479['value'] = NULL;
$renderChildrenClosure480 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments481 = array();
$arguments481['action'] = 'xliffAsJson';
// Rendering Array
$array482 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\InterfaceLanguageViewHelper
$arguments483 = array();
$renderChildrenClosure484 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper485 = $self->getViewHelper('$viewHelper485', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\InterfaceLanguageViewHelper');
$viewHelper485->setArguments($arguments483);
$viewHelper485->setRenderingContext($renderingContext);
$viewHelper485->setRenderChildrenClosure($renderChildrenClosure484);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\InterfaceLanguageViewHelper
$array482['locale'] = $viewHelper485->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\XliffCacheVersionViewHelper
$arguments486 = array();
$renderChildrenClosure487 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper488 = $self->getViewHelper('$viewHelper488', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\XliffCacheVersionViewHelper');
$viewHelper488->setArguments($arguments486);
$viewHelper488->setRenderingContext($renderingContext);
$viewHelper488->setRenderChildrenClosure($renderChildrenClosure487);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\XliffCacheVersionViewHelper
$array482['version'] = $viewHelper488->initializeArgumentsAndRender();
$arguments481['arguments'] = $array482;
$arguments481['controller'] = 'Backend\\Backend';
$arguments481['package'] = 'TYPO3.Neos';
// Rendering Boolean node
$arguments481['absolute'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'true', $renderingContext));
$arguments481['subpackage'] = NULL;
$arguments481['section'] = '';
$arguments481['format'] = '';
$arguments481['additionalParams'] = array (
);
$arguments481['addQueryString'] = false;
$arguments481['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments481['useParentRequest'] = false;
$renderChildrenClosure489 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper490 = $self->getViewHelper('$viewHelper490', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper490->setArguments($arguments481);
$viewHelper490->setRenderingContext($renderingContext);
$viewHelper490->setRenderChildrenClosure($renderChildrenClosure489);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ActionViewHelper
return $viewHelper490->initializeArgumentsAndRender();
};
$viewHelper491 = $self->getViewHelper('$viewHelper491', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\RawViewHelper');
$viewHelper491->setArguments($arguments479);
$viewHelper491->setRenderingContext($renderingContext);
$viewHelper491->setRenderChildrenClosure($renderChildrenClosure480);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\RawViewHelper

$output332 .= $viewHelper491->initializeArgumentsAndRender();

$output332 .= '" />
';
return $output332;
};

$output323 .= '';

$output323 .= '

';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\SectionViewHelper
$arguments492 = array();
$arguments492['name'] = 'body';
$renderChildrenClosure493 = function() use ($renderingContext, $self) {
$output494 = '';

$output494 .= '
	<body class="neos neos-module neos-controls neos-module-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments495 = array();
$arguments495['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'moduleClass', $renderingContext);
$arguments495['keepQuotes'] = false;
$arguments495['encoding'] = 'UTF-8';
$arguments495['doubleEncode'] = true;
$renderChildrenClosure496 = function() use ($renderingContext, $self) {
return NULL;
};
$value497 = ($arguments495['value'] !== NULL ? $arguments495['value'] : $renderChildrenClosure496());

$output494 .= !is_string($value497) && !(is_object($value497) && method_exists($value497, '__toString')) ? $value497 : htmlspecialchars($value497, ($arguments495['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments495['encoding'], $arguments495['doubleEncode']);

$output494 .= '">
		<div class="neos-module-wrap">
			<ul class="neos-breadcrumb">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments498 = array();
$arguments498['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'moduleBreadcrumb', $renderingContext);
$arguments498['key'] = 'path';
$arguments498['as'] = 'configuration';
$arguments498['iteration'] = 'iterator';
$arguments498['reverse'] = false;
$renderChildrenClosure499 = function() use ($renderingContext, $self) {
$output500 = '';

$output500 .= '
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments501 = array();
// Rendering Boolean node
$arguments501['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.hideInMenu', $renderingContext));
$arguments501['then'] = NULL;
$arguments501['else'] = NULL;
$renderChildrenClosure502 = function() use ($renderingContext, $self) {
$output503 = '';

$output503 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments504 = array();
$renderChildrenClosure505 = function() use ($renderingContext, $self) {
$output506 = '';

$output506 .= '
							<li>
								';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper
$arguments507 = array();
$arguments507['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'path', $renderingContext);
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments508 = array();
// Rendering Boolean node
$arguments508['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'iterator.isLast', $renderingContext));
$arguments508['then'] = 'active';
$arguments508['else'] = NULL;
$renderChildrenClosure509 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper510 = $self->getViewHelper('$viewHelper510', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper510->setArguments($arguments508);
$viewHelper510->setRenderingContext($renderingContext);
$viewHelper510->setRenderChildrenClosure($renderChildrenClosure509);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments507['class'] = $viewHelper510->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments511 = array();
// Rendering Boolean node
$arguments511['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.description', $renderingContext));
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments512 = array();
$arguments512['id'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.description', $renderingContext);
$arguments512['value'] = NULL;
$arguments512['arguments'] = array (
);
$arguments512['source'] = 'Main';
$arguments512['package'] = NULL;
$arguments512['quantity'] = NULL;
$arguments512['languageIdentifier'] = NULL;
$renderChildrenClosure513 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper514 = $self->getViewHelper('$viewHelper514', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper514->setArguments($arguments512);
$viewHelper514->setRenderingContext($renderingContext);
$viewHelper514->setRenderChildrenClosure($renderChildrenClosure513);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments511['then'] = $viewHelper514->initializeArgumentsAndRender();
$arguments511['else'] = NULL;
$renderChildrenClosure515 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper516 = $self->getViewHelper('$viewHelper516', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper516->setArguments($arguments511);
$viewHelper516->setRenderingContext($renderingContext);
$viewHelper516->setRenderChildrenClosure($renderChildrenClosure515);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments507['title'] = $viewHelper516->initializeArgumentsAndRender();
// Rendering Array
$array517 = array();
$array517['data-neos-toggle'] = 'tooltip';
$array517['data-placement'] = 'bottom';
$arguments507['additionalAttributes'] = $array517;
$arguments507['data'] = NULL;
$arguments507['action'] = NULL;
$arguments507['arguments'] = array (
);
$arguments507['section'] = '';
$arguments507['format'] = '';
$arguments507['additionalParams'] = array (
);
$arguments507['addQueryString'] = false;
$arguments507['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments507['dir'] = NULL;
$arguments507['id'] = NULL;
$arguments507['lang'] = NULL;
$arguments507['style'] = NULL;
$arguments507['accesskey'] = NULL;
$arguments507['tabindex'] = NULL;
$arguments507['onclick'] = NULL;
$arguments507['name'] = NULL;
$arguments507['rel'] = NULL;
$arguments507['rev'] = NULL;
$arguments507['target'] = NULL;
$renderChildrenClosure518 = function() use ($renderingContext, $self) {
$output519 = '';

$output519 .= '<i class="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments520 = array();
$arguments520['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.icon', $renderingContext);
$arguments520['keepQuotes'] = false;
$arguments520['encoding'] = 'UTF-8';
$arguments520['doubleEncode'] = true;
$renderChildrenClosure521 = function() use ($renderingContext, $self) {
return NULL;
};
$value522 = ($arguments520['value'] !== NULL ? $arguments520['value'] : $renderChildrenClosure521());

$output519 .= !is_string($value522) && !(is_object($value522) && method_exists($value522, '__toString')) ? $value522 : htmlspecialchars($value522, ($arguments520['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments520['encoding'], $arguments520['doubleEncode']);

$output519 .= '"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments523 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments524 = array();
$arguments524['source'] = 'Modules';
$arguments524['id'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.label', $renderingContext);
$arguments524['value'] = NULL;
$arguments524['arguments'] = array (
);
$arguments524['package'] = NULL;
$arguments524['quantity'] = NULL;
$arguments524['languageIdentifier'] = NULL;
$renderChildrenClosure525 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper526 = $self->getViewHelper('$viewHelper526', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper526->setArguments($arguments524);
$viewHelper526->setRenderingContext($renderingContext);
$viewHelper526->setRenderChildrenClosure($renderChildrenClosure525);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments523['value'] = $viewHelper526->initializeArgumentsAndRender();
$arguments523['keepQuotes'] = false;
$arguments523['encoding'] = 'UTF-8';
$arguments523['doubleEncode'] = true;
$renderChildrenClosure527 = function() use ($renderingContext, $self) {
return NULL;
};
$value528 = ($arguments523['value'] !== NULL ? $arguments523['value'] : $renderChildrenClosure527());

$output519 .= !is_string($value528) && !(is_object($value528) && method_exists($value528, '__toString')) ? $value528 : htmlspecialchars($value528, ($arguments523['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments523['encoding'], $arguments523['doubleEncode']);
return $output519;
};
$viewHelper529 = $self->getViewHelper('$viewHelper529', $renderingContext, 'TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper');
$viewHelper529->setArguments($arguments507);
$viewHelper529->setRenderingContext($renderingContext);
$viewHelper529->setRenderChildrenClosure($renderChildrenClosure518);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper

$output506 .= $viewHelper529->initializeArgumentsAndRender();

$output506 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments530 = array();
// Rendering Boolean node
$arguments530['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'iterator.isLast', $renderingContext));
$arguments530['then'] = NULL;
$arguments530['else'] = NULL;
$renderChildrenClosure531 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments532 = array();
$renderChildrenClosure533 = function() use ($renderingContext, $self) {
return '<span class="neos-divider">/</span>';
};
$viewHelper534 = $self->getViewHelper('$viewHelper534', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper534->setArguments($arguments532);
$viewHelper534->setRenderingContext($renderingContext);
$viewHelper534->setRenderChildrenClosure($renderChildrenClosure533);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
return $viewHelper534->initializeArgumentsAndRender();
};
$arguments530['__elseClosure'] = function() use ($renderingContext, $self) {
return '<span class="neos-divider">/</span>';
};
$viewHelper535 = $self->getViewHelper('$viewHelper535', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper535->setArguments($arguments530);
$viewHelper535->setRenderingContext($renderingContext);
$viewHelper535->setRenderChildrenClosure($renderChildrenClosure531);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output506 .= $viewHelper535->initializeArgumentsAndRender();

$output506 .= '
							</li>
						';
return $output506;
};
$viewHelper536 = $self->getViewHelper('$viewHelper536', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper536->setArguments($arguments504);
$viewHelper536->setRenderingContext($renderingContext);
$viewHelper536->setRenderChildrenClosure($renderChildrenClosure505);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output503 .= $viewHelper536->initializeArgumentsAndRender();

$output503 .= '
					';
return $output503;
};
$arguments501['__elseClosure'] = function() use ($renderingContext, $self) {
$output537 = '';

$output537 .= '
							<li>
								';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper
$arguments538 = array();
$arguments538['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'path', $renderingContext);
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments539 = array();
// Rendering Boolean node
$arguments539['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'iterator.isLast', $renderingContext));
$arguments539['then'] = 'active';
$arguments539['else'] = NULL;
$renderChildrenClosure540 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper541 = $self->getViewHelper('$viewHelper541', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper541->setArguments($arguments539);
$viewHelper541->setRenderingContext($renderingContext);
$viewHelper541->setRenderChildrenClosure($renderChildrenClosure540);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments538['class'] = $viewHelper541->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments542 = array();
// Rendering Boolean node
$arguments542['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.description', $renderingContext));
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments543 = array();
$arguments543['id'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.description', $renderingContext);
$arguments543['value'] = NULL;
$arguments543['arguments'] = array (
);
$arguments543['source'] = 'Main';
$arguments543['package'] = NULL;
$arguments543['quantity'] = NULL;
$arguments543['languageIdentifier'] = NULL;
$renderChildrenClosure544 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper545 = $self->getViewHelper('$viewHelper545', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper545->setArguments($arguments543);
$viewHelper545->setRenderingContext($renderingContext);
$viewHelper545->setRenderChildrenClosure($renderChildrenClosure544);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments542['then'] = $viewHelper545->initializeArgumentsAndRender();
$arguments542['else'] = NULL;
$renderChildrenClosure546 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper547 = $self->getViewHelper('$viewHelper547', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper547->setArguments($arguments542);
$viewHelper547->setRenderingContext($renderingContext);
$viewHelper547->setRenderChildrenClosure($renderChildrenClosure546);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments538['title'] = $viewHelper547->initializeArgumentsAndRender();
// Rendering Array
$array548 = array();
$array548['data-neos-toggle'] = 'tooltip';
$array548['data-placement'] = 'bottom';
$arguments538['additionalAttributes'] = $array548;
$arguments538['data'] = NULL;
$arguments538['action'] = NULL;
$arguments538['arguments'] = array (
);
$arguments538['section'] = '';
$arguments538['format'] = '';
$arguments538['additionalParams'] = array (
);
$arguments538['addQueryString'] = false;
$arguments538['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments538['dir'] = NULL;
$arguments538['id'] = NULL;
$arguments538['lang'] = NULL;
$arguments538['style'] = NULL;
$arguments538['accesskey'] = NULL;
$arguments538['tabindex'] = NULL;
$arguments538['onclick'] = NULL;
$arguments538['name'] = NULL;
$arguments538['rel'] = NULL;
$arguments538['rev'] = NULL;
$arguments538['target'] = NULL;
$renderChildrenClosure549 = function() use ($renderingContext, $self) {
$output550 = '';

$output550 .= '<i class="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments551 = array();
$arguments551['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.icon', $renderingContext);
$arguments551['keepQuotes'] = false;
$arguments551['encoding'] = 'UTF-8';
$arguments551['doubleEncode'] = true;
$renderChildrenClosure552 = function() use ($renderingContext, $self) {
return NULL;
};
$value553 = ($arguments551['value'] !== NULL ? $arguments551['value'] : $renderChildrenClosure552());

$output550 .= !is_string($value553) && !(is_object($value553) && method_exists($value553, '__toString')) ? $value553 : htmlspecialchars($value553, ($arguments551['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments551['encoding'], $arguments551['doubleEncode']);

$output550 .= '"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments554 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments555 = array();
$arguments555['source'] = 'Modules';
$arguments555['id'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.label', $renderingContext);
$arguments555['value'] = NULL;
$arguments555['arguments'] = array (
);
$arguments555['package'] = NULL;
$arguments555['quantity'] = NULL;
$arguments555['languageIdentifier'] = NULL;
$renderChildrenClosure556 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper557 = $self->getViewHelper('$viewHelper557', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper557->setArguments($arguments555);
$viewHelper557->setRenderingContext($renderingContext);
$viewHelper557->setRenderChildrenClosure($renderChildrenClosure556);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments554['value'] = $viewHelper557->initializeArgumentsAndRender();
$arguments554['keepQuotes'] = false;
$arguments554['encoding'] = 'UTF-8';
$arguments554['doubleEncode'] = true;
$renderChildrenClosure558 = function() use ($renderingContext, $self) {
return NULL;
};
$value559 = ($arguments554['value'] !== NULL ? $arguments554['value'] : $renderChildrenClosure558());

$output550 .= !is_string($value559) && !(is_object($value559) && method_exists($value559, '__toString')) ? $value559 : htmlspecialchars($value559, ($arguments554['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments554['encoding'], $arguments554['doubleEncode']);
return $output550;
};
$viewHelper560 = $self->getViewHelper('$viewHelper560', $renderingContext, 'TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper');
$viewHelper560->setArguments($arguments538);
$viewHelper560->setRenderingContext($renderingContext);
$viewHelper560->setRenderChildrenClosure($renderChildrenClosure549);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper

$output537 .= $viewHelper560->initializeArgumentsAndRender();

$output537 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments561 = array();
// Rendering Boolean node
$arguments561['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'iterator.isLast', $renderingContext));
$arguments561['then'] = NULL;
$arguments561['else'] = NULL;
$renderChildrenClosure562 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments563 = array();
$renderChildrenClosure564 = function() use ($renderingContext, $self) {
return '<span class="neos-divider">/</span>';
};
$viewHelper565 = $self->getViewHelper('$viewHelper565', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper565->setArguments($arguments563);
$viewHelper565->setRenderingContext($renderingContext);
$viewHelper565->setRenderChildrenClosure($renderChildrenClosure564);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
return $viewHelper565->initializeArgumentsAndRender();
};
$arguments561['__elseClosure'] = function() use ($renderingContext, $self) {
return '<span class="neos-divider">/</span>';
};
$viewHelper566 = $self->getViewHelper('$viewHelper566', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper566->setArguments($arguments561);
$viewHelper566->setRenderingContext($renderingContext);
$viewHelper566->setRenderChildrenClosure($renderChildrenClosure562);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output537 .= $viewHelper566->initializeArgumentsAndRender();

$output537 .= '
							</li>
						';
return $output537;
};
$viewHelper567 = $self->getViewHelper('$viewHelper567', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper567->setArguments($arguments501);
$viewHelper567->setRenderingContext($renderingContext);
$viewHelper567->setRenderChildrenClosure($renderChildrenClosure502);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output500 .= $viewHelper567->initializeArgumentsAndRender();

$output500 .= '
				';
return $output500;
};

$output494 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments498, $renderChildrenClosure499, $renderingContext);

$output494 .= '
			</ul>
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments568 = array();
// Rendering Boolean node
$arguments568['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'moduleContents', $renderingContext));
$arguments568['then'] = NULL;
$arguments568['else'] = NULL;
$renderChildrenClosure569 = function() use ($renderingContext, $self) {
$output570 = '';

$output570 .= '
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\RawViewHelper
$arguments571 = array();
$arguments571['value'] = NULL;
$renderChildrenClosure572 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'moduleContents', $renderingContext);
};
$viewHelper573 = $self->getViewHelper('$viewHelper573', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\RawViewHelper');
$viewHelper573->setArguments($arguments571);
$viewHelper573->setRenderingContext($renderingContext);
$viewHelper573->setRenderChildrenClosure($renderChildrenClosure572);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\RawViewHelper

$output570 .= $viewHelper573->initializeArgumentsAndRender();

$output570 .= '
			';
return $output570;
};
$viewHelper574 = $self->getViewHelper('$viewHelper574', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper574->setArguments($arguments568);
$viewHelper574->setRenderingContext($renderingContext);
$viewHelper574->setRenderChildrenClosure($renderChildrenClosure569);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output494 .= $viewHelper574->initializeArgumentsAndRender();

$output494 .= '
			<div id="neos-application" class="neos">
				<div id="neos-top-bar">
					<div id="neos-top-bar-right">
						<div id="neos-user-actions">
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper
$arguments575 = array();
$arguments575['partial'] = 'Backend/UserMenu';
$arguments575['arguments'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), '_all', $renderingContext);
$arguments575['section'] = NULL;
$arguments575['optional'] = false;
$renderChildrenClosure576 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper577 = $self->getViewHelper('$viewHelper577', $renderingContext, 'TYPO3\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper577->setArguments($arguments575);
$viewHelper577->setRenderingContext($renderingContext);
$viewHelper577->setRenderChildrenClosure($renderChildrenClosure576);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper

$output494 .= $viewHelper577->initializeArgumentsAndRender();

$output494 .= '
						</div>
					</div>
				</div>
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper
$arguments578 = array();
$arguments578['partial'] = 'Backend/Menu';
// Rendering Array
$array579 = array();
$array579['sites'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sites', $renderingContext);
$array579['modules'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'modules', $renderingContext);
$array579['modulePath'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'moduleConfiguration.path', $renderingContext);
$arguments578['arguments'] = $array579;
$arguments578['section'] = NULL;
$arguments578['optional'] = false;
$renderChildrenClosure580 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper581 = $self->getViewHelper('$viewHelper581', $renderingContext, 'TYPO3\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper581->setArguments($arguments578);
$viewHelper581->setRenderingContext($renderingContext);
$viewHelper581->setRenderChildrenClosure($renderChildrenClosure580);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper

$output494 .= $viewHelper581->initializeArgumentsAndRender();

$output494 .= '
			</div>
		</div>
		<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments582 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments583 = array();
$arguments583['path'] = '2/js/bootstrap.min.js';
$arguments583['package'] = 'TYPO3.Twitter.Bootstrap';
$arguments583['resource'] = NULL;
$arguments583['localize'] = true;
$renderChildrenClosure584 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper585 = $self->getViewHelper('$viewHelper585', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper585->setArguments($arguments583);
$viewHelper585->setRenderingContext($renderingContext);
$viewHelper585->setRenderChildrenClosure($renderChildrenClosure584);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments582['value'] = $viewHelper585->initializeArgumentsAndRender();
$arguments582['keepQuotes'] = false;
$arguments582['encoding'] = 'UTF-8';
$arguments582['doubleEncode'] = true;
$renderChildrenClosure586 = function() use ($renderingContext, $self) {
return NULL;
};
$value587 = ($arguments582['value'] !== NULL ? $arguments582['value'] : $renderChildrenClosure586());

$output494 .= !is_string($value587) && !(is_object($value587) && method_exists($value587, '__toString')) ? $value587 : htmlspecialchars($value587, ($arguments582['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments582['encoding'], $arguments582['doubleEncode']);

$output494 .= '"></script>
		<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments588 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments589 = array();
$arguments589['path'] = 'Library/fixedsticky/fixedsticky.js';
$arguments589['package'] = 'TYPO3.Neos';
$arguments589['resource'] = NULL;
$arguments589['localize'] = true;
$renderChildrenClosure590 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper591 = $self->getViewHelper('$viewHelper591', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper591->setArguments($arguments589);
$viewHelper591->setRenderingContext($renderingContext);
$viewHelper591->setRenderChildrenClosure($renderChildrenClosure590);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments588['value'] = $viewHelper591->initializeArgumentsAndRender();
$arguments588['keepQuotes'] = false;
$arguments588['encoding'] = 'UTF-8';
$arguments588['doubleEncode'] = true;
$renderChildrenClosure592 = function() use ($renderingContext, $self) {
return NULL;
};
$value593 = ($arguments588['value'] !== NULL ? $arguments588['value'] : $renderChildrenClosure592());

$output494 .= !is_string($value593) && !(is_object($value593) && method_exists($value593, '__toString')) ? $value593 : htmlspecialchars($value593, ($arguments588['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments588['encoding'], $arguments588['doubleEncode']);

$output494 .= '"></script>

		<script type="text/javascript">
			(function($) {
				$(function() {
					$(\'.neos-footer\').fixedsticky();
				});
			})(jQuery);
		</script>

		<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments594 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments595 = array();
$arguments595['path'] = 'Library/requirejs/require.js';
$arguments595['package'] = 'TYPO3.Neos';
$arguments595['resource'] = NULL;
$arguments595['localize'] = true;
$renderChildrenClosure596 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper597 = $self->getViewHelper('$viewHelper597', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper597->setArguments($arguments595);
$viewHelper597->setRenderingContext($renderingContext);
$viewHelper597->setRenderChildrenClosure($renderChildrenClosure596);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments594['value'] = $viewHelper597->initializeArgumentsAndRender();
$arguments594['keepQuotes'] = false;
$arguments594['encoding'] = 'UTF-8';
$arguments594['doubleEncode'] = true;
$renderChildrenClosure598 = function() use ($renderingContext, $self) {
return NULL;
};
$value599 = ($arguments594['value'] !== NULL ? $arguments594['value'] : $renderChildrenClosure598());

$output494 .= !is_string($value599) && !(is_object($value599) && method_exists($value599, '__toString')) ? $value599 : htmlspecialchars($value599, ($arguments594['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments594['encoding'], $arguments594['doubleEncode']);

$output494 .= '"></script>
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments600 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\ShouldLoadMinifiedJavascriptViewHelper
$arguments601 = array();
$renderChildrenClosure602 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper603 = $self->getViewHelper('$viewHelper603', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\ShouldLoadMinifiedJavascriptViewHelper');
$viewHelper603->setArguments($arguments601);
$viewHelper603->setRenderingContext($renderingContext);
$viewHelper603->setRenderChildrenClosure($renderChildrenClosure602);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\ShouldLoadMinifiedJavascriptViewHelper
$arguments600['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean($viewHelper603->initializeArgumentsAndRender());
$arguments600['then'] = NULL;
$arguments600['else'] = NULL;
$renderChildrenClosure604 = function() use ($renderingContext, $self) {
$output605 = '';

$output605 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments606 = array();
$renderChildrenClosure607 = function() use ($renderingContext, $self) {
$output608 = '';

$output608 .= '
				<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments609 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments610 = array();
$arguments610['path'] = 'JavaScript/ContentModule-built.js';
$arguments610['package'] = 'TYPO3.Neos';
$arguments610['resource'] = NULL;
$arguments610['localize'] = true;
$renderChildrenClosure611 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper612 = $self->getViewHelper('$viewHelper612', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper612->setArguments($arguments610);
$viewHelper612->setRenderingContext($renderingContext);
$viewHelper612->setRenderChildrenClosure($renderChildrenClosure611);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments609['value'] = $viewHelper612->initializeArgumentsAndRender();
$arguments609['keepQuotes'] = false;
$arguments609['encoding'] = 'UTF-8';
$arguments609['doubleEncode'] = true;
$renderChildrenClosure613 = function() use ($renderingContext, $self) {
return NULL;
};
$value614 = ($arguments609['value'] !== NULL ? $arguments609['value'] : $renderChildrenClosure613());

$output608 .= !is_string($value614) && !(is_object($value614) && method_exists($value614, '__toString')) ? $value614 : htmlspecialchars($value614, ($arguments609['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments609['encoding'], $arguments609['doubleEncode']);

$output608 .= '?bust=';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments615 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\JavascriptBuiltVersionViewHelper
$arguments616 = array();
$renderChildrenClosure617 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper618 = $self->getViewHelper('$viewHelper618', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\JavascriptBuiltVersionViewHelper');
$viewHelper618->setArguments($arguments616);
$viewHelper618->setRenderingContext($renderingContext);
$viewHelper618->setRenderChildrenClosure($renderChildrenClosure617);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\JavascriptBuiltVersionViewHelper
$arguments615['value'] = $viewHelper618->initializeArgumentsAndRender();
$arguments615['keepQuotes'] = false;
$arguments615['encoding'] = 'UTF-8';
$arguments615['doubleEncode'] = true;
$renderChildrenClosure619 = function() use ($renderingContext, $self) {
return NULL;
};
$value620 = ($arguments615['value'] !== NULL ? $arguments615['value'] : $renderChildrenClosure619());

$output608 .= !is_string($value620) && !(is_object($value620) && method_exists($value620, '__toString')) ? $value620 : htmlspecialchars($value620, ($arguments615['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments615['encoding'], $arguments615['doubleEncode']);

$output608 .= '"></script>
			';
return $output608;
};
$viewHelper621 = $self->getViewHelper('$viewHelper621', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper621->setArguments($arguments606);
$viewHelper621->setRenderingContext($renderingContext);
$viewHelper621->setRenderChildrenClosure($renderChildrenClosure607);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output605 .= $viewHelper621->initializeArgumentsAndRender();

$output605 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments622 = array();
$renderChildrenClosure623 = function() use ($renderingContext, $self) {
$output624 = '';

$output624 .= '
				<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments625 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments626 = array();
$arguments626['path'] = 'JavaScript/ContentModuleBootstrap.js';
$arguments626['package'] = 'TYPO3.Neos';
$arguments626['resource'] = NULL;
$arguments626['localize'] = true;
$renderChildrenClosure627 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper628 = $self->getViewHelper('$viewHelper628', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper628->setArguments($arguments626);
$viewHelper628->setRenderingContext($renderingContext);
$viewHelper628->setRenderChildrenClosure($renderChildrenClosure627);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments625['value'] = $viewHelper628->initializeArgumentsAndRender();
$arguments625['keepQuotes'] = false;
$arguments625['encoding'] = 'UTF-8';
$arguments625['doubleEncode'] = true;
$renderChildrenClosure629 = function() use ($renderingContext, $self) {
return NULL;
};
$value630 = ($arguments625['value'] !== NULL ? $arguments625['value'] : $renderChildrenClosure629());

$output624 .= !is_string($value630) && !(is_object($value630) && method_exists($value630, '__toString')) ? $value630 : htmlspecialchars($value630, ($arguments625['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments625['encoding'], $arguments625['doubleEncode']);

$output624 .= '"></script>
			';
return $output624;
};
$viewHelper631 = $self->getViewHelper('$viewHelper631', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper631->setArguments($arguments622);
$viewHelper631->setRenderingContext($renderingContext);
$viewHelper631->setRenderChildrenClosure($renderChildrenClosure623);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output605 .= $viewHelper631->initializeArgumentsAndRender();

$output605 .= '
		';
return $output605;
};
$arguments600['__thenClosure'] = function() use ($renderingContext, $self) {
$output632 = '';

$output632 .= '
				<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments633 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments634 = array();
$arguments634['path'] = 'JavaScript/ContentModule-built.js';
$arguments634['package'] = 'TYPO3.Neos';
$arguments634['resource'] = NULL;
$arguments634['localize'] = true;
$renderChildrenClosure635 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper636 = $self->getViewHelper('$viewHelper636', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper636->setArguments($arguments634);
$viewHelper636->setRenderingContext($renderingContext);
$viewHelper636->setRenderChildrenClosure($renderChildrenClosure635);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments633['value'] = $viewHelper636->initializeArgumentsAndRender();
$arguments633['keepQuotes'] = false;
$arguments633['encoding'] = 'UTF-8';
$arguments633['doubleEncode'] = true;
$renderChildrenClosure637 = function() use ($renderingContext, $self) {
return NULL;
};
$value638 = ($arguments633['value'] !== NULL ? $arguments633['value'] : $renderChildrenClosure637());

$output632 .= !is_string($value638) && !(is_object($value638) && method_exists($value638, '__toString')) ? $value638 : htmlspecialchars($value638, ($arguments633['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments633['encoding'], $arguments633['doubleEncode']);

$output632 .= '?bust=';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments639 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\JavascriptBuiltVersionViewHelper
$arguments640 = array();
$renderChildrenClosure641 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper642 = $self->getViewHelper('$viewHelper642', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\JavascriptBuiltVersionViewHelper');
$viewHelper642->setArguments($arguments640);
$viewHelper642->setRenderingContext($renderingContext);
$viewHelper642->setRenderChildrenClosure($renderChildrenClosure641);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\JavascriptBuiltVersionViewHelper
$arguments639['value'] = $viewHelper642->initializeArgumentsAndRender();
$arguments639['keepQuotes'] = false;
$arguments639['encoding'] = 'UTF-8';
$arguments639['doubleEncode'] = true;
$renderChildrenClosure643 = function() use ($renderingContext, $self) {
return NULL;
};
$value644 = ($arguments639['value'] !== NULL ? $arguments639['value'] : $renderChildrenClosure643());

$output632 .= !is_string($value644) && !(is_object($value644) && method_exists($value644, '__toString')) ? $value644 : htmlspecialchars($value644, ($arguments639['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments639['encoding'], $arguments639['doubleEncode']);

$output632 .= '"></script>
			';
return $output632;
};
$arguments600['__elseClosure'] = function() use ($renderingContext, $self) {
$output645 = '';

$output645 .= '
				<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments646 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments647 = array();
$arguments647['path'] = 'JavaScript/ContentModuleBootstrap.js';
$arguments647['package'] = 'TYPO3.Neos';
$arguments647['resource'] = NULL;
$arguments647['localize'] = true;
$renderChildrenClosure648 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper649 = $self->getViewHelper('$viewHelper649', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper649->setArguments($arguments647);
$viewHelper649->setRenderingContext($renderingContext);
$viewHelper649->setRenderChildrenClosure($renderChildrenClosure648);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments646['value'] = $viewHelper649->initializeArgumentsAndRender();
$arguments646['keepQuotes'] = false;
$arguments646['encoding'] = 'UTF-8';
$arguments646['doubleEncode'] = true;
$renderChildrenClosure650 = function() use ($renderingContext, $self) {
return NULL;
};
$value651 = ($arguments646['value'] !== NULL ? $arguments646['value'] : $renderChildrenClosure650());

$output645 .= !is_string($value651) && !(is_object($value651) && method_exists($value651, '__toString')) ? $value651 : htmlspecialchars($value651, ($arguments646['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments646['encoding'], $arguments646['doubleEncode']);

$output645 .= '"></script>
			';
return $output645;
};
$viewHelper652 = $self->getViewHelper('$viewHelper652', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper652->setArguments($arguments600);
$viewHelper652->setRenderingContext($renderingContext);
$viewHelper652->setRenderChildrenClosure($renderChildrenClosure604);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output494 .= $viewHelper652->initializeArgumentsAndRender();

$output494 .= '

		<script>
			$(function() {
				require(
					';
$output653 = '';

$output653 .= '{
						baseUrl: \'';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments654 = array();
$arguments654['path'] = 'JavaScript';
$arguments654['package'] = NULL;
$arguments654['resource'] = NULL;
$arguments654['localize'] = true;
$renderChildrenClosure655 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper656 = $self->getViewHelper('$viewHelper656', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper656->setArguments($arguments654);
$viewHelper656->setRenderingContext($renderingContext);
$viewHelper656->setRenderChildrenClosure($renderChildrenClosure655);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper

$output653 .= $viewHelper656->initializeArgumentsAndRender();

$output653 .= '\',
						paths: requirePaths,
						context: \'neos\',
						locale: \'en\'
					}';

$output494 .= $output653;

$output494 .= ',
					[
						\'Library/jquery-with-dependencies\'
					],
					function($) {
						$(\'[data-neos-toggle="popover"]\').popover();
						$(\'[data-neos-toggle="tooltip"]\').tooltip();
					}
				)
			});
		</script>
	</body>
';
return $output494;
};

$output323 .= '';

$output323 .= '
';

return $output323;
}


}
#0             169589    