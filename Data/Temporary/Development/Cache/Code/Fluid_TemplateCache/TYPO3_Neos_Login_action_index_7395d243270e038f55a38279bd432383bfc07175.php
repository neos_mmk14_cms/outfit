<?php class FluidCache_TYPO3_Neos_Login_action_index_7395d243270e038f55a38279bd432383bfc07175 extends \TYPO3\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// TODO
	return new \TYPO3\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;

return 'Default';
}
public function hasLayout() {
return TRUE;
}

/**
 * section head
 */
public function section_1a954628a960aaef81d7b2d4521929579f3541e6(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output0 = '';

$output0 .= '
	<title>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments2 = array();
$arguments2['id'] = 'login.index.title';
$arguments2['value'] = NULL;
$arguments2['arguments'] = array (
);
$arguments2['source'] = 'Main';
$arguments2['package'] = NULL;
$arguments2['quantity'] = NULL;
$arguments2['languageIdentifier'] = NULL;
$renderChildrenClosure3 = function() use ($renderingContext, $self) {
return 'Login to';
};
$viewHelper4 = $self->getViewHelper('$viewHelper4', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper4->setArguments($arguments2);
$viewHelper4->setRenderingContext($renderingContext);
$viewHelper4->setRenderChildrenClosure($renderChildrenClosure3);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments1['value'] = $viewHelper4->initializeArgumentsAndRender();
$arguments1['keepQuotes'] = false;
$arguments1['encoding'] = 'UTF-8';
$arguments1['doubleEncode'] = true;
$renderChildrenClosure5 = function() use ($renderingContext, $self) {
return NULL;
};
$value6 = ($arguments1['value'] !== NULL ? $arguments1['value'] : $renderChildrenClosure5());

$output0 .= !is_string($value6) && !(is_object($value6) && method_exists($value6, '__toString')) ? $value6 : htmlspecialchars($value6, ($arguments1['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments1['encoding'], $arguments1['doubleEncode']);

$output0 .= ' ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments7 = array();
$arguments7['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments7['keepQuotes'] = false;
$arguments7['encoding'] = 'UTF-8';
$arguments7['doubleEncode'] = true;
$renderChildrenClosure8 = function() use ($renderingContext, $self) {
return NULL;
};
$value9 = ($arguments7['value'] !== NULL ? $arguments7['value'] : $renderChildrenClosure8());

$output0 .= !is_string($value9) && !(is_object($value9) && method_exists($value9, '__toString')) ? $value9 : htmlspecialchars($value9, ($arguments7['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments7['encoding'], $arguments7['doubleEncode']);

$output0 .= '</title>
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments10 = array();
$arguments10['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'styles', $renderingContext);
$arguments10['as'] = 'style';
$arguments10['key'] = '';
$arguments10['reverse'] = false;
$arguments10['iteration'] = NULL;
$renderChildrenClosure11 = function() use ($renderingContext, $self) {
$output12 = '';

$output12 .= '
		<link rel="stylesheet" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments13 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments14 = array();
$arguments14['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'style', $renderingContext);
$arguments14['package'] = NULL;
$arguments14['resource'] = NULL;
$arguments14['localize'] = true;
$renderChildrenClosure15 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper16 = $self->getViewHelper('$viewHelper16', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper16->setArguments($arguments14);
$viewHelper16->setRenderingContext($renderingContext);
$viewHelper16->setRenderChildrenClosure($renderChildrenClosure15);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments13['value'] = $viewHelper16->initializeArgumentsAndRender();
$arguments13['keepQuotes'] = false;
$arguments13['encoding'] = 'UTF-8';
$arguments13['doubleEncode'] = true;
$renderChildrenClosure17 = function() use ($renderingContext, $self) {
return NULL;
};
$value18 = ($arguments13['value'] !== NULL ? $arguments13['value'] : $renderChildrenClosure17());

$output12 .= !is_string($value18) && !(is_object($value18) && method_exists($value18, '__toString')) ? $value18 : htmlspecialchars($value18, ($arguments13['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments13['encoding'], $arguments13['doubleEncode']);

$output12 .= '" />
	';
return $output12;
};

$output0 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments10, $renderChildrenClosure11, $renderingContext);

$output0 .= '
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments19 = array();
// Rendering Boolean node
$arguments19['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.userInterface.backendLoginForm.backgroundImage', $renderingContext));
$arguments19['then'] = NULL;
$arguments19['else'] = NULL;
$renderChildrenClosure20 = function() use ($renderingContext, $self) {
$output21 = '';

$output21 .= '
		<style type="text/css">
			.neos-login-box:before {
				background-image: url(';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments22 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments23 = array();
$arguments23['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.userInterface.backendLoginForm.backgroundImage', $renderingContext);
$arguments23['package'] = NULL;
$arguments23['resource'] = NULL;
$arguments23['localize'] = true;
$renderChildrenClosure24 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper25 = $self->getViewHelper('$viewHelper25', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper25->setArguments($arguments23);
$viewHelper25->setRenderingContext($renderingContext);
$viewHelper25->setRenderChildrenClosure($renderChildrenClosure24);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments22['value'] = $viewHelper25->initializeArgumentsAndRender();
$arguments22['keepQuotes'] = false;
$arguments22['encoding'] = 'UTF-8';
$arguments22['doubleEncode'] = true;
$renderChildrenClosure26 = function() use ($renderingContext, $self) {
return NULL;
};
$value27 = ($arguments22['value'] !== NULL ? $arguments22['value'] : $renderChildrenClosure26());

$output21 .= !is_string($value27) && !(is_object($value27) && method_exists($value27, '__toString')) ? $value27 : htmlspecialchars($value27, ($arguments22['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments22['encoding'], $arguments22['doubleEncode']);

$output21 .= ');
			}
		</style>
	';
return $output21;
};
$viewHelper28 = $self->getViewHelper('$viewHelper28', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper28->setArguments($arguments19);
$viewHelper28->setRenderingContext($renderingContext);
$viewHelper28->setRenderChildrenClosure($renderChildrenClosure20);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output0 .= $viewHelper28->initializeArgumentsAndRender();

$output0 .= '
	<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments29 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments30 = array();
$arguments30['path'] = 'Library/jquery/jquery-2.0.3.js';
$arguments30['package'] = NULL;
$arguments30['resource'] = NULL;
$arguments30['localize'] = true;
$renderChildrenClosure31 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper32 = $self->getViewHelper('$viewHelper32', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper32->setArguments($arguments30);
$viewHelper32->setRenderingContext($renderingContext);
$viewHelper32->setRenderChildrenClosure($renderChildrenClosure31);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments29['value'] = $viewHelper32->initializeArgumentsAndRender();
$arguments29['keepQuotes'] = false;
$arguments29['encoding'] = 'UTF-8';
$arguments29['doubleEncode'] = true;
$renderChildrenClosure33 = function() use ($renderingContext, $self) {
return NULL;
};
$value34 = ($arguments29['value'] !== NULL ? $arguments29['value'] : $renderChildrenClosure33());

$output0 .= !is_string($value34) && !(is_object($value34) && method_exists($value34, '__toString')) ? $value34 : htmlspecialchars($value34, ($arguments29['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments29['encoding'], $arguments29['doubleEncode']);

$output0 .= '"></script>
	<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments35 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments36 = array();
$arguments36['path'] = 'Library/jquery-ui/js/jquery-ui-1.10.4.custom.js';
$arguments36['package'] = NULL;
$arguments36['resource'] = NULL;
$arguments36['localize'] = true;
$renderChildrenClosure37 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper38 = $self->getViewHelper('$viewHelper38', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper38->setArguments($arguments36);
$viewHelper38->setRenderingContext($renderingContext);
$viewHelper38->setRenderChildrenClosure($renderChildrenClosure37);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments35['value'] = $viewHelper38->initializeArgumentsAndRender();
$arguments35['keepQuotes'] = false;
$arguments35['encoding'] = 'UTF-8';
$arguments35['doubleEncode'] = true;
$renderChildrenClosure39 = function() use ($renderingContext, $self) {
return NULL;
};
$value40 = ($arguments35['value'] !== NULL ? $arguments35['value'] : $renderChildrenClosure39());

$output0 .= !is_string($value40) && !(is_object($value40) && method_exists($value40, '__toString')) ? $value40 : htmlspecialchars($value40, ($arguments35['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments35['encoding'], $arguments35['doubleEncode']);

$output0 .= '"></script>
';

return $output0;
}
/**
 * section body
 */
public function section_02083f4579e08a612425c0c1a17ee47add783b94(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output41 = '';

$output41 .= '
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments42 = array();
// Rendering Boolean node
$arguments42['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.userInterface.backendLoginForm.backgroundImage', $renderingContext));
$arguments42['then'] = NULL;
$arguments42['else'] = NULL;
$renderChildrenClosure43 = function() use ($renderingContext, $self) {
$output44 = '';

$output44 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments45 = array();
$renderChildrenClosure46 = function() use ($renderingContext, $self) {
$output47 = '';

$output47 .= '
			<body class="neos" style="background-image:url(';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments48 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments49 = array();
$arguments49['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.userInterface.backendLoginForm.backgroundImage', $renderingContext);
$arguments49['package'] = NULL;
$arguments49['resource'] = NULL;
$arguments49['localize'] = true;
$renderChildrenClosure50 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper51 = $self->getViewHelper('$viewHelper51', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper51->setArguments($arguments49);
$viewHelper51->setRenderingContext($renderingContext);
$viewHelper51->setRenderChildrenClosure($renderChildrenClosure50);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments48['value'] = $viewHelper51->initializeArgumentsAndRender();
$arguments48['keepQuotes'] = false;
$arguments48['encoding'] = 'UTF-8';
$arguments48['doubleEncode'] = true;
$renderChildrenClosure52 = function() use ($renderingContext, $self) {
return NULL;
};
$value53 = ($arguments48['value'] !== NULL ? $arguments48['value'] : $renderChildrenClosure52());

$output47 .= !is_string($value53) && !(is_object($value53) && method_exists($value53, '__toString')) ? $value53 : htmlspecialchars($value53, ($arguments48['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments48['encoding'], $arguments48['doubleEncode']);

$output47 .= ');">
		';
return $output47;
};
$viewHelper54 = $self->getViewHelper('$viewHelper54', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper54->setArguments($arguments45);
$viewHelper54->setRenderingContext($renderingContext);
$viewHelper54->setRenderChildrenClosure($renderChildrenClosure46);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output44 .= $viewHelper54->initializeArgumentsAndRender();

$output44 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments55 = array();
$renderChildrenClosure56 = function() use ($renderingContext, $self) {
return '
			<body class="neos">
		';
};
$viewHelper57 = $self->getViewHelper('$viewHelper57', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper57->setArguments($arguments55);
$viewHelper57->setRenderingContext($renderingContext);
$viewHelper57->setRenderChildrenClosure($renderChildrenClosure56);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output44 .= $viewHelper57->initializeArgumentsAndRender();

$output44 .= '
	';
return $output44;
};
$arguments42['__thenClosure'] = function() use ($renderingContext, $self) {
$output58 = '';

$output58 .= '
			<body class="neos" style="background-image:url(';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments59 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments60 = array();
$arguments60['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.userInterface.backendLoginForm.backgroundImage', $renderingContext);
$arguments60['package'] = NULL;
$arguments60['resource'] = NULL;
$arguments60['localize'] = true;
$renderChildrenClosure61 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper62 = $self->getViewHelper('$viewHelper62', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper62->setArguments($arguments60);
$viewHelper62->setRenderingContext($renderingContext);
$viewHelper62->setRenderChildrenClosure($renderChildrenClosure61);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments59['value'] = $viewHelper62->initializeArgumentsAndRender();
$arguments59['keepQuotes'] = false;
$arguments59['encoding'] = 'UTF-8';
$arguments59['doubleEncode'] = true;
$renderChildrenClosure63 = function() use ($renderingContext, $self) {
return NULL;
};
$value64 = ($arguments59['value'] !== NULL ? $arguments59['value'] : $renderChildrenClosure63());

$output58 .= !is_string($value64) && !(is_object($value64) && method_exists($value64, '__toString')) ? $value64 : htmlspecialchars($value64, ($arguments59['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments59['encoding'], $arguments59['doubleEncode']);

$output58 .= ');">
		';
return $output58;
};
$arguments42['__elseClosure'] = function() use ($renderingContext, $self) {
return '
			<body class="neos">
		';
};
$viewHelper65 = $self->getViewHelper('$viewHelper65', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper65->setArguments($arguments42);
$viewHelper65->setRenderingContext($renderingContext);
$viewHelper65->setRenderChildrenClosure($renderChildrenClosure43);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output41 .= $viewHelper65->initializeArgumentsAndRender();

$output41 .= '
		<div class="neos-modal-centered">
			<main class="neos-login-main">
				<div class="neos-login-box ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments66 = array();
// Rendering Boolean node
$arguments66['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.userInterface.backendLoginForm.backgroundImage', $renderingContext));
$arguments66['then'] = 'background-image-active';
$arguments66['else'] = NULL;
$renderChildrenClosure67 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper68 = $self->getViewHelper('$viewHelper68', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper68->setArguments($arguments66);
$viewHelper68->setRenderingContext($renderingContext);
$viewHelper68->setRenderChildrenClosure($renderChildrenClosure67);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output41 .= $viewHelper68->initializeArgumentsAndRender();

$output41 .= '">
					<figure class="neos-login-box-logo">
						<img class="neos-login-box-logo-resource" src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments69 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments70 = array();
$arguments70['path'] = 'Images/Login/Logo.svg';
$arguments70['package'] = NULL;
$arguments70['resource'] = NULL;
$arguments70['localize'] = true;
$renderChildrenClosure71 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper72 = $self->getViewHelper('$viewHelper72', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper72->setArguments($arguments70);
$viewHelper72->setRenderingContext($renderingContext);
$viewHelper72->setRenderChildrenClosure($renderChildrenClosure71);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments69['value'] = $viewHelper72->initializeArgumentsAndRender();
$arguments69['keepQuotes'] = false;
$arguments69['encoding'] = 'UTF-8';
$arguments69['doubleEncode'] = true;
$renderChildrenClosure73 = function() use ($renderingContext, $self) {
return NULL;
};
$value74 = ($arguments69['value'] !== NULL ? $arguments69['value'] : $renderChildrenClosure73());

$output41 .= !is_string($value74) && !(is_object($value74) && method_exists($value74, '__toString')) ? $value74 : htmlspecialchars($value74, ($arguments69['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments69['encoding'], $arguments69['doubleEncode']);

$output41 .= '" width="200px" height="200px" />
					</figure>

					<header>
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments75 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments76 = array();
$arguments76['id'] = 'login.index.title';
$arguments76['value'] = NULL;
$arguments76['arguments'] = array (
);
$arguments76['source'] = 'Main';
$arguments76['package'] = NULL;
$arguments76['quantity'] = NULL;
$arguments76['languageIdentifier'] = NULL;
$renderChildrenClosure77 = function() use ($renderingContext, $self) {
return 'Login to';
};
$viewHelper78 = $self->getViewHelper('$viewHelper78', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper78->setArguments($arguments76);
$viewHelper78->setRenderingContext($renderingContext);
$viewHelper78->setRenderChildrenClosure($renderChildrenClosure77);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments75['value'] = $viewHelper78->initializeArgumentsAndRender();
$arguments75['keepQuotes'] = false;
$arguments75['encoding'] = 'UTF-8';
$arguments75['doubleEncode'] = true;
$renderChildrenClosure79 = function() use ($renderingContext, $self) {
return NULL;
};
$value80 = ($arguments75['value'] !== NULL ? $arguments75['value'] : $renderChildrenClosure79());

$output41 .= !is_string($value80) && !(is_object($value80) && method_exists($value80, '__toString')) ? $value80 : htmlspecialchars($value80, ($arguments75['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments75['encoding'], $arguments75['doubleEncode']);

$output41 .= '
						<strong>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments81 = array();
$arguments81['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments81['keepQuotes'] = false;
$arguments81['encoding'] = 'UTF-8';
$arguments81['doubleEncode'] = true;
$renderChildrenClosure82 = function() use ($renderingContext, $self) {
return NULL;
};
$value83 = ($arguments81['value'] !== NULL ? $arguments81['value'] : $renderChildrenClosure82());

$output41 .= !is_string($value83) && !(is_object($value83) && method_exists($value83, '__toString')) ? $value83 : htmlspecialchars($value83, ($arguments81['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments81['encoding'], $arguments81['doubleEncode']);

$output41 .= '</strong>
					</header>
					<div class="neos-login-body neos">
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments84 = array();
$arguments84['name'] = 'login';
$arguments84['action'] = 'authenticate';
$arguments84['additionalAttributes'] = NULL;
$arguments84['data'] = NULL;
$arguments84['arguments'] = array (
);
$arguments84['controller'] = NULL;
$arguments84['package'] = NULL;
$arguments84['subpackage'] = NULL;
$arguments84['object'] = NULL;
$arguments84['section'] = '';
$arguments84['format'] = '';
$arguments84['additionalParams'] = array (
);
$arguments84['absolute'] = false;
$arguments84['addQueryString'] = false;
$arguments84['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments84['fieldNamePrefix'] = NULL;
$arguments84['actionUri'] = NULL;
$arguments84['objectName'] = NULL;
$arguments84['useParentRequest'] = false;
$arguments84['enctype'] = NULL;
$arguments84['method'] = NULL;
$arguments84['onreset'] = NULL;
$arguments84['onsubmit'] = NULL;
$arguments84['class'] = NULL;
$arguments84['dir'] = NULL;
$arguments84['id'] = NULL;
$arguments84['lang'] = NULL;
$arguments84['style'] = NULL;
$arguments84['title'] = NULL;
$arguments84['accesskey'] = NULL;
$arguments84['tabindex'] = NULL;
$arguments84['onclick'] = NULL;
$renderChildrenClosure85 = function() use ($renderingContext, $self) {
$output86 = '';

$output86 .= '
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments87 = array();
$arguments87['name'] = 'lastVisitedNode';
$arguments87['additionalAttributes'] = NULL;
$arguments87['data'] = NULL;
$arguments87['value'] = NULL;
$arguments87['property'] = NULL;
$arguments87['class'] = NULL;
$arguments87['dir'] = NULL;
$arguments87['id'] = NULL;
$arguments87['lang'] = NULL;
$arguments87['style'] = NULL;
$arguments87['title'] = NULL;
$arguments87['accesskey'] = NULL;
$arguments87['tabindex'] = NULL;
$arguments87['onclick'] = NULL;
$renderChildrenClosure88 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper89 = $self->getViewHelper('$viewHelper89', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper89->setArguments($arguments87);
$viewHelper89->setRenderingContext($renderingContext);
$viewHelper89->setRenderChildrenClosure($renderChildrenClosure88);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output86 .= $viewHelper89->initializeArgumentsAndRender();

$output86 .= '
							<fieldset>
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments90 = array();
// Rendering Boolean node
$arguments90['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'username', $renderingContext));
$arguments90['then'] = NULL;
$arguments90['else'] = NULL;
$renderChildrenClosure91 = function() use ($renderingContext, $self) {
$output92 = '';

$output92 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments93 = array();
$renderChildrenClosure94 = function() use ($renderingContext, $self) {
$output95 = '';

$output95 .= '
										<div class="neos-controls">
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments96 = array();
// Rendering Boolean node
$arguments96['required'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('required');
$arguments96['id'] = 'username';
$arguments96['type'] = 'text';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments97 = array();
$arguments97['id'] = 'username';
$arguments97['value'] = 'Username';
$arguments97['arguments'] = array (
);
$arguments97['source'] = 'Main';
$arguments97['package'] = NULL;
$arguments97['quantity'] = NULL;
$arguments97['languageIdentifier'] = NULL;
$renderChildrenClosure98 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper99 = $self->getViewHelper('$viewHelper99', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper99->setArguments($arguments97);
$viewHelper99->setRenderingContext($renderingContext);
$viewHelper99->setRenderChildrenClosure($renderChildrenClosure98);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments96['placeholder'] = $viewHelper99->initializeArgumentsAndRender();
$arguments96['class'] = 'neos-span12';
$arguments96['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][username]';
$arguments96['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'username', $renderingContext);
$arguments96['additionalAttributes'] = NULL;
$arguments96['data'] = NULL;
$arguments96['property'] = NULL;
$arguments96['disabled'] = NULL;
$arguments96['maxlength'] = NULL;
$arguments96['readonly'] = NULL;
$arguments96['size'] = NULL;
$arguments96['autofocus'] = NULL;
$arguments96['errorClass'] = 'f3-form-error';
$arguments96['dir'] = NULL;
$arguments96['lang'] = NULL;
$arguments96['style'] = NULL;
$arguments96['title'] = NULL;
$arguments96['accesskey'] = NULL;
$arguments96['tabindex'] = NULL;
$arguments96['onclick'] = NULL;
$renderChildrenClosure100 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper101 = $self->getViewHelper('$viewHelper101', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper101->setArguments($arguments96);
$viewHelper101->setRenderingContext($renderingContext);
$viewHelper101->setRenderChildrenClosure($renderChildrenClosure100);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output95 .= $viewHelper101->initializeArgumentsAndRender();

$output95 .= '
										</div>
										<div class="neos-controls">
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments102 = array();
// Rendering Boolean node
$arguments102['required'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('required');
$arguments102['id'] = 'password';
$arguments102['type'] = 'password';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments103 = array();
$arguments103['id'] = 'password';
$arguments103['value'] = 'Password';
$arguments103['arguments'] = array (
);
$arguments103['source'] = 'Main';
$arguments103['package'] = NULL;
$arguments103['quantity'] = NULL;
$arguments103['languageIdentifier'] = NULL;
$renderChildrenClosure104 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper105 = $self->getViewHelper('$viewHelper105', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper105->setArguments($arguments103);
$viewHelper105->setRenderingContext($renderingContext);
$viewHelper105->setRenderChildrenClosure($renderChildrenClosure104);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments102['placeholder'] = $viewHelper105->initializeArgumentsAndRender();
$arguments102['class'] = 'neos-span12';
$arguments102['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][password]';
// Rendering Array
$array106 = array();
$array106['autofocus'] = 'autofocus';
$arguments102['additionalAttributes'] = $array106;
$arguments102['data'] = NULL;
$arguments102['value'] = NULL;
$arguments102['property'] = NULL;
$arguments102['disabled'] = NULL;
$arguments102['maxlength'] = NULL;
$arguments102['readonly'] = NULL;
$arguments102['size'] = NULL;
$arguments102['autofocus'] = NULL;
$arguments102['errorClass'] = 'f3-form-error';
$arguments102['dir'] = NULL;
$arguments102['lang'] = NULL;
$arguments102['style'] = NULL;
$arguments102['title'] = NULL;
$arguments102['accesskey'] = NULL;
$arguments102['tabindex'] = NULL;
$arguments102['onclick'] = NULL;
$renderChildrenClosure107 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper108 = $self->getViewHelper('$viewHelper108', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper108->setArguments($arguments102);
$viewHelper108->setRenderingContext($renderingContext);
$viewHelper108->setRenderChildrenClosure($renderChildrenClosure107);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output95 .= $viewHelper108->initializeArgumentsAndRender();

$output95 .= '
										</div>
									';
return $output95;
};
$viewHelper109 = $self->getViewHelper('$viewHelper109', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper109->setArguments($arguments93);
$viewHelper109->setRenderingContext($renderingContext);
$viewHelper109->setRenderChildrenClosure($renderChildrenClosure94);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output92 .= $viewHelper109->initializeArgumentsAndRender();

$output92 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments110 = array();
$renderChildrenClosure111 = function() use ($renderingContext, $self) {
$output112 = '';

$output112 .= '
										<div class="neos-controls">
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments113 = array();
// Rendering Boolean node
$arguments113['required'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('required');
$arguments113['id'] = 'username';
$arguments113['type'] = 'text';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments114 = array();
$arguments114['id'] = 'username';
$arguments114['value'] = 'Username';
$arguments114['arguments'] = array (
);
$arguments114['source'] = 'Main';
$arguments114['package'] = NULL;
$arguments114['quantity'] = NULL;
$arguments114['languageIdentifier'] = NULL;
$renderChildrenClosure115 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper116 = $self->getViewHelper('$viewHelper116', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper116->setArguments($arguments114);
$viewHelper116->setRenderingContext($renderingContext);
$viewHelper116->setRenderChildrenClosure($renderChildrenClosure115);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments113['placeholder'] = $viewHelper116->initializeArgumentsAndRender();
$arguments113['class'] = 'neos-span12';
$arguments113['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][username]';
// Rendering Array
$array117 = array();
$array117['autofocus'] = 'autofocus';
$arguments113['additionalAttributes'] = $array117;
$arguments113['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'username', $renderingContext);
$arguments113['data'] = NULL;
$arguments113['property'] = NULL;
$arguments113['disabled'] = NULL;
$arguments113['maxlength'] = NULL;
$arguments113['readonly'] = NULL;
$arguments113['size'] = NULL;
$arguments113['autofocus'] = NULL;
$arguments113['errorClass'] = 'f3-form-error';
$arguments113['dir'] = NULL;
$arguments113['lang'] = NULL;
$arguments113['style'] = NULL;
$arguments113['title'] = NULL;
$arguments113['accesskey'] = NULL;
$arguments113['tabindex'] = NULL;
$arguments113['onclick'] = NULL;
$renderChildrenClosure118 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper119 = $self->getViewHelper('$viewHelper119', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper119->setArguments($arguments113);
$viewHelper119->setRenderingContext($renderingContext);
$viewHelper119->setRenderChildrenClosure($renderChildrenClosure118);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output112 .= $viewHelper119->initializeArgumentsAndRender();

$output112 .= '
										</div>
										<div class="neos-controls">
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments120 = array();
// Rendering Boolean node
$arguments120['required'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('required');
$arguments120['id'] = 'password';
$arguments120['type'] = 'password';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments121 = array();
$arguments121['id'] = 'password';
$arguments121['value'] = 'Password';
$arguments121['arguments'] = array (
);
$arguments121['source'] = 'Main';
$arguments121['package'] = NULL;
$arguments121['quantity'] = NULL;
$arguments121['languageIdentifier'] = NULL;
$renderChildrenClosure122 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper123 = $self->getViewHelper('$viewHelper123', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper123->setArguments($arguments121);
$viewHelper123->setRenderingContext($renderingContext);
$viewHelper123->setRenderChildrenClosure($renderChildrenClosure122);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments120['placeholder'] = $viewHelper123->initializeArgumentsAndRender();
$arguments120['class'] = 'neos-span12';
$arguments120['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][password]';
$arguments120['additionalAttributes'] = NULL;
$arguments120['data'] = NULL;
$arguments120['value'] = NULL;
$arguments120['property'] = NULL;
$arguments120['disabled'] = NULL;
$arguments120['maxlength'] = NULL;
$arguments120['readonly'] = NULL;
$arguments120['size'] = NULL;
$arguments120['autofocus'] = NULL;
$arguments120['errorClass'] = 'f3-form-error';
$arguments120['dir'] = NULL;
$arguments120['lang'] = NULL;
$arguments120['style'] = NULL;
$arguments120['title'] = NULL;
$arguments120['accesskey'] = NULL;
$arguments120['tabindex'] = NULL;
$arguments120['onclick'] = NULL;
$renderChildrenClosure124 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper125 = $self->getViewHelper('$viewHelper125', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper125->setArguments($arguments120);
$viewHelper125->setRenderingContext($renderingContext);
$viewHelper125->setRenderChildrenClosure($renderChildrenClosure124);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output112 .= $viewHelper125->initializeArgumentsAndRender();

$output112 .= '
										</div>
									';
return $output112;
};
$viewHelper126 = $self->getViewHelper('$viewHelper126', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper126->setArguments($arguments110);
$viewHelper126->setRenderingContext($renderingContext);
$viewHelper126->setRenderChildrenClosure($renderChildrenClosure111);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output92 .= $viewHelper126->initializeArgumentsAndRender();

$output92 .= '
								';
return $output92;
};
$arguments90['__thenClosure'] = function() use ($renderingContext, $self) {
$output127 = '';

$output127 .= '
										<div class="neos-controls">
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments128 = array();
// Rendering Boolean node
$arguments128['required'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('required');
$arguments128['id'] = 'username';
$arguments128['type'] = 'text';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments129 = array();
$arguments129['id'] = 'username';
$arguments129['value'] = 'Username';
$arguments129['arguments'] = array (
);
$arguments129['source'] = 'Main';
$arguments129['package'] = NULL;
$arguments129['quantity'] = NULL;
$arguments129['languageIdentifier'] = NULL;
$renderChildrenClosure130 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper131 = $self->getViewHelper('$viewHelper131', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper131->setArguments($arguments129);
$viewHelper131->setRenderingContext($renderingContext);
$viewHelper131->setRenderChildrenClosure($renderChildrenClosure130);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments128['placeholder'] = $viewHelper131->initializeArgumentsAndRender();
$arguments128['class'] = 'neos-span12';
$arguments128['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][username]';
$arguments128['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'username', $renderingContext);
$arguments128['additionalAttributes'] = NULL;
$arguments128['data'] = NULL;
$arguments128['property'] = NULL;
$arguments128['disabled'] = NULL;
$arguments128['maxlength'] = NULL;
$arguments128['readonly'] = NULL;
$arguments128['size'] = NULL;
$arguments128['autofocus'] = NULL;
$arguments128['errorClass'] = 'f3-form-error';
$arguments128['dir'] = NULL;
$arguments128['lang'] = NULL;
$arguments128['style'] = NULL;
$arguments128['title'] = NULL;
$arguments128['accesskey'] = NULL;
$arguments128['tabindex'] = NULL;
$arguments128['onclick'] = NULL;
$renderChildrenClosure132 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper133 = $self->getViewHelper('$viewHelper133', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper133->setArguments($arguments128);
$viewHelper133->setRenderingContext($renderingContext);
$viewHelper133->setRenderChildrenClosure($renderChildrenClosure132);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output127 .= $viewHelper133->initializeArgumentsAndRender();

$output127 .= '
										</div>
										<div class="neos-controls">
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments134 = array();
// Rendering Boolean node
$arguments134['required'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('required');
$arguments134['id'] = 'password';
$arguments134['type'] = 'password';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments135 = array();
$arguments135['id'] = 'password';
$arguments135['value'] = 'Password';
$arguments135['arguments'] = array (
);
$arguments135['source'] = 'Main';
$arguments135['package'] = NULL;
$arguments135['quantity'] = NULL;
$arguments135['languageIdentifier'] = NULL;
$renderChildrenClosure136 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper137 = $self->getViewHelper('$viewHelper137', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper137->setArguments($arguments135);
$viewHelper137->setRenderingContext($renderingContext);
$viewHelper137->setRenderChildrenClosure($renderChildrenClosure136);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments134['placeholder'] = $viewHelper137->initializeArgumentsAndRender();
$arguments134['class'] = 'neos-span12';
$arguments134['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][password]';
// Rendering Array
$array138 = array();
$array138['autofocus'] = 'autofocus';
$arguments134['additionalAttributes'] = $array138;
$arguments134['data'] = NULL;
$arguments134['value'] = NULL;
$arguments134['property'] = NULL;
$arguments134['disabled'] = NULL;
$arguments134['maxlength'] = NULL;
$arguments134['readonly'] = NULL;
$arguments134['size'] = NULL;
$arguments134['autofocus'] = NULL;
$arguments134['errorClass'] = 'f3-form-error';
$arguments134['dir'] = NULL;
$arguments134['lang'] = NULL;
$arguments134['style'] = NULL;
$arguments134['title'] = NULL;
$arguments134['accesskey'] = NULL;
$arguments134['tabindex'] = NULL;
$arguments134['onclick'] = NULL;
$renderChildrenClosure139 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper140 = $self->getViewHelper('$viewHelper140', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper140->setArguments($arguments134);
$viewHelper140->setRenderingContext($renderingContext);
$viewHelper140->setRenderChildrenClosure($renderChildrenClosure139);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output127 .= $viewHelper140->initializeArgumentsAndRender();

$output127 .= '
										</div>
									';
return $output127;
};
$arguments90['__elseClosure'] = function() use ($renderingContext, $self) {
$output141 = '';

$output141 .= '
										<div class="neos-controls">
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments142 = array();
// Rendering Boolean node
$arguments142['required'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('required');
$arguments142['id'] = 'username';
$arguments142['type'] = 'text';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments143 = array();
$arguments143['id'] = 'username';
$arguments143['value'] = 'Username';
$arguments143['arguments'] = array (
);
$arguments143['source'] = 'Main';
$arguments143['package'] = NULL;
$arguments143['quantity'] = NULL;
$arguments143['languageIdentifier'] = NULL;
$renderChildrenClosure144 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper145 = $self->getViewHelper('$viewHelper145', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper145->setArguments($arguments143);
$viewHelper145->setRenderingContext($renderingContext);
$viewHelper145->setRenderChildrenClosure($renderChildrenClosure144);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments142['placeholder'] = $viewHelper145->initializeArgumentsAndRender();
$arguments142['class'] = 'neos-span12';
$arguments142['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][username]';
// Rendering Array
$array146 = array();
$array146['autofocus'] = 'autofocus';
$arguments142['additionalAttributes'] = $array146;
$arguments142['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'username', $renderingContext);
$arguments142['data'] = NULL;
$arguments142['property'] = NULL;
$arguments142['disabled'] = NULL;
$arguments142['maxlength'] = NULL;
$arguments142['readonly'] = NULL;
$arguments142['size'] = NULL;
$arguments142['autofocus'] = NULL;
$arguments142['errorClass'] = 'f3-form-error';
$arguments142['dir'] = NULL;
$arguments142['lang'] = NULL;
$arguments142['style'] = NULL;
$arguments142['title'] = NULL;
$arguments142['accesskey'] = NULL;
$arguments142['tabindex'] = NULL;
$arguments142['onclick'] = NULL;
$renderChildrenClosure147 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper148 = $self->getViewHelper('$viewHelper148', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper148->setArguments($arguments142);
$viewHelper148->setRenderingContext($renderingContext);
$viewHelper148->setRenderChildrenClosure($renderChildrenClosure147);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output141 .= $viewHelper148->initializeArgumentsAndRender();

$output141 .= '
										</div>
										<div class="neos-controls">
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments149 = array();
// Rendering Boolean node
$arguments149['required'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('required');
$arguments149['id'] = 'password';
$arguments149['type'] = 'password';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments150 = array();
$arguments150['id'] = 'password';
$arguments150['value'] = 'Password';
$arguments150['arguments'] = array (
);
$arguments150['source'] = 'Main';
$arguments150['package'] = NULL;
$arguments150['quantity'] = NULL;
$arguments150['languageIdentifier'] = NULL;
$renderChildrenClosure151 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper152 = $self->getViewHelper('$viewHelper152', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper152->setArguments($arguments150);
$viewHelper152->setRenderingContext($renderingContext);
$viewHelper152->setRenderChildrenClosure($renderChildrenClosure151);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments149['placeholder'] = $viewHelper152->initializeArgumentsAndRender();
$arguments149['class'] = 'neos-span12';
$arguments149['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][password]';
$arguments149['additionalAttributes'] = NULL;
$arguments149['data'] = NULL;
$arguments149['value'] = NULL;
$arguments149['property'] = NULL;
$arguments149['disabled'] = NULL;
$arguments149['maxlength'] = NULL;
$arguments149['readonly'] = NULL;
$arguments149['size'] = NULL;
$arguments149['autofocus'] = NULL;
$arguments149['errorClass'] = 'f3-form-error';
$arguments149['dir'] = NULL;
$arguments149['lang'] = NULL;
$arguments149['style'] = NULL;
$arguments149['title'] = NULL;
$arguments149['accesskey'] = NULL;
$arguments149['tabindex'] = NULL;
$arguments149['onclick'] = NULL;
$renderChildrenClosure153 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper154 = $self->getViewHelper('$viewHelper154', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper154->setArguments($arguments149);
$viewHelper154->setRenderingContext($renderingContext);
$viewHelper154->setRenderChildrenClosure($renderChildrenClosure153);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output141 .= $viewHelper154->initializeArgumentsAndRender();

$output141 .= '
										</div>
									';
return $output141;
};
$viewHelper155 = $self->getViewHelper('$viewHelper155', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper155->setArguments($arguments90);
$viewHelper155->setRenderingContext($renderingContext);
$viewHelper155->setRenderChildrenClosure($renderChildrenClosure91);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output86 .= $viewHelper155->initializeArgumentsAndRender();

$output86 .= '
								<div class="neos-actions">
									<!-- Forgot password link will be here -->
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\ButtonViewHelper
$arguments156 = array();
$arguments156['type'] = 'submit';
$arguments156['class'] = 'neos-span5 neos-pull-right neos-button neos-login-btn';
$arguments156['additionalAttributes'] = NULL;
$arguments156['data'] = NULL;
$arguments156['name'] = NULL;
$arguments156['value'] = NULL;
$arguments156['property'] = NULL;
$arguments156['autofocus'] = NULL;
$arguments156['disabled'] = NULL;
$arguments156['form'] = NULL;
$arguments156['formaction'] = NULL;
$arguments156['formenctype'] = NULL;
$arguments156['formmethod'] = NULL;
$arguments156['formnovalidate'] = NULL;
$arguments156['formtarget'] = NULL;
$arguments156['dir'] = NULL;
$arguments156['id'] = NULL;
$arguments156['lang'] = NULL;
$arguments156['style'] = NULL;
$arguments156['title'] = NULL;
$arguments156['accesskey'] = NULL;
$arguments156['tabindex'] = NULL;
$arguments156['onclick'] = NULL;
$renderChildrenClosure157 = function() use ($renderingContext, $self) {
$output158 = '';

$output158 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments159 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments160 = array();
$arguments160['id'] = 'login';
$arguments160['value'] = 'Login';
$arguments160['arguments'] = array (
);
$arguments160['source'] = 'Main';
$arguments160['package'] = NULL;
$arguments160['quantity'] = NULL;
$arguments160['languageIdentifier'] = NULL;
$renderChildrenClosure161 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper162 = $self->getViewHelper('$viewHelper162', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper162->setArguments($arguments160);
$viewHelper162->setRenderingContext($renderingContext);
$viewHelper162->setRenderChildrenClosure($renderChildrenClosure161);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments159['value'] = $viewHelper162->initializeArgumentsAndRender();
$arguments159['keepQuotes'] = false;
$arguments159['encoding'] = 'UTF-8';
$arguments159['doubleEncode'] = true;
$renderChildrenClosure163 = function() use ($renderingContext, $self) {
return NULL;
};
$value164 = ($arguments159['value'] !== NULL ? $arguments159['value'] : $renderChildrenClosure163());

$output158 .= !is_string($value164) && !(is_object($value164) && method_exists($value164, '__toString')) ? $value164 : htmlspecialchars($value164, ($arguments159['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments159['encoding'], $arguments159['doubleEncode']);

$output158 .= '
									';
return $output158;
};
$viewHelper165 = $self->getViewHelper('$viewHelper165', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\ButtonViewHelper');
$viewHelper165->setArguments($arguments156);
$viewHelper165->setRenderingContext($renderingContext);
$viewHelper165->setRenderChildrenClosure($renderChildrenClosure157);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\ButtonViewHelper

$output86 .= $viewHelper165->initializeArgumentsAndRender();

$output86 .= '
									<button class="neos-span5 neos-pull-right neos-button neos-login-btn neos-disabled neos-hidden">
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments166 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments167 = array();
$arguments167['id'] = 'authenticating';
$arguments167['value'] = 'Authenticating';
$arguments167['arguments'] = array (
);
$arguments167['source'] = 'Main';
$arguments167['package'] = NULL;
$arguments167['quantity'] = NULL;
$arguments167['languageIdentifier'] = NULL;
$renderChildrenClosure168 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper169 = $self->getViewHelper('$viewHelper169', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper169->setArguments($arguments167);
$viewHelper169->setRenderingContext($renderingContext);
$viewHelper169->setRenderChildrenClosure($renderChildrenClosure168);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments166['value'] = $viewHelper169->initializeArgumentsAndRender();
$arguments166['keepQuotes'] = false;
$arguments166['encoding'] = 'UTF-8';
$arguments166['doubleEncode'] = true;
$renderChildrenClosure170 = function() use ($renderingContext, $self) {
return NULL;
};
$value171 = ($arguments166['value'] !== NULL ? $arguments166['value'] : $renderChildrenClosure170());

$output86 .= !is_string($value171) && !(is_object($value171) && method_exists($value171, '__toString')) ? $value171 : htmlspecialchars($value171, ($arguments166['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments166['encoding'], $arguments166['doubleEncode']);

$output86 .= '
										<span class="neos-ellipsis"></span>
									</button>
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FlashMessagesViewHelper
$arguments172 = array();
$arguments172['as'] = 'flashMessages';
$arguments172['additionalAttributes'] = NULL;
$arguments172['data'] = NULL;
$arguments172['severity'] = NULL;
$arguments172['class'] = NULL;
$arguments172['dir'] = NULL;
$arguments172['id'] = NULL;
$arguments172['lang'] = NULL;
$arguments172['style'] = NULL;
$arguments172['title'] = NULL;
$arguments172['accesskey'] = NULL;
$arguments172['tabindex'] = NULL;
$arguments172['onclick'] = NULL;
$renderChildrenClosure173 = function() use ($renderingContext, $self) {
$output174 = '';

$output174 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments175 = array();
$arguments175['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessages', $renderingContext);
$arguments175['as'] = 'flashMessage';
$arguments175['key'] = '';
$arguments175['reverse'] = false;
$arguments175['iteration'] = NULL;
$renderChildrenClosure176 = function() use ($renderingContext, $self) {
$output177 = '';

$output177 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments178 = array();
// Rendering Boolean node
$arguments178['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.severity', $renderingContext), 'OK');
$arguments178['then'] = NULL;
$arguments178['else'] = NULL;
$renderChildrenClosure179 = function() use ($renderingContext, $self) {
return '
												<div class="neos-tooltip neos-bottom neos-in neos-tooltip-success">
											';
};
$viewHelper180 = $self->getViewHelper('$viewHelper180', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper180->setArguments($arguments178);
$viewHelper180->setRenderingContext($renderingContext);
$viewHelper180->setRenderChildrenClosure($renderChildrenClosure179);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output177 .= $viewHelper180->initializeArgumentsAndRender();

$output177 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments181 = array();
// Rendering Boolean node
$arguments181['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.severity', $renderingContext), 'Notice');
$arguments181['then'] = NULL;
$arguments181['else'] = NULL;
$renderChildrenClosure182 = function() use ($renderingContext, $self) {
return '
												<div class="neos-tooltip neos-bottom neos-in neos-tooltip-notice">
											';
};
$viewHelper183 = $self->getViewHelper('$viewHelper183', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper183->setArguments($arguments181);
$viewHelper183->setRenderingContext($renderingContext);
$viewHelper183->setRenderChildrenClosure($renderChildrenClosure182);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output177 .= $viewHelper183->initializeArgumentsAndRender();

$output177 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments184 = array();
// Rendering Boolean node
$arguments184['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.severity', $renderingContext), 'Warning');
$arguments184['then'] = NULL;
$arguments184['else'] = NULL;
$renderChildrenClosure185 = function() use ($renderingContext, $self) {
return '
												<div class="neos-tooltip neos-bottom neos-in neos-tooltip-warning">
											';
};
$viewHelper186 = $self->getViewHelper('$viewHelper186', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper186->setArguments($arguments184);
$viewHelper186->setRenderingContext($renderingContext);
$viewHelper186->setRenderChildrenClosure($renderChildrenClosure185);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output177 .= $viewHelper186->initializeArgumentsAndRender();

$output177 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments187 = array();
// Rendering Boolean node
$arguments187['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.severity', $renderingContext), 'Error');
$arguments187['then'] = NULL;
$arguments187['else'] = NULL;
$renderChildrenClosure188 = function() use ($renderingContext, $self) {
$output189 = '';

$output189 .= '
												<script>
													$(function () {
														$(\'fieldset\').effect(\'shake\', ';

$output189 .= '{times: 1}';

$output189 .= ', 60);
													});
												</script>
												<div class="neos-tooltip neos-bottom neos-in neos-tooltip-error">
											';
return $output189;
};
$viewHelper190 = $self->getViewHelper('$viewHelper190', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper190->setArguments($arguments187);
$viewHelper190->setRenderingContext($renderingContext);
$viewHelper190->setRenderChildrenClosure($renderChildrenClosure188);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output177 .= $viewHelper190->initializeArgumentsAndRender();

$output177 .= '
											<div class="neos-tooltip-arrow"></div>
											<div class="neos-tooltip-inner">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments191 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments192 = array();
$output193 = '';

$output193 .= 'flashMessage.';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments194 = array();
$arguments194['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.code', $renderingContext);
$arguments194['keepQuotes'] = false;
$arguments194['encoding'] = 'UTF-8';
$arguments194['doubleEncode'] = true;
$renderChildrenClosure195 = function() use ($renderingContext, $self) {
return NULL;
};
$value196 = ($arguments194['value'] !== NULL ? $arguments194['value'] : $renderChildrenClosure195());

$output193 .= !is_string($value196) && !(is_object($value196) && method_exists($value196, '__toString')) ? $value196 : htmlspecialchars($value196, ($arguments194['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments194['encoding'], $arguments194['doubleEncode']);
$arguments192['id'] = $output193;
$arguments192['package'] = 'TYPO3.Neos';
$arguments192['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage', $renderingContext);
$arguments192['arguments'] = array (
);
$arguments192['source'] = 'Main';
$arguments192['quantity'] = NULL;
$arguments192['languageIdentifier'] = NULL;
$renderChildrenClosure197 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper198 = $self->getViewHelper('$viewHelper198', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper198->setArguments($arguments192);
$viewHelper198->setRenderingContext($renderingContext);
$viewHelper198->setRenderChildrenClosure($renderChildrenClosure197);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments191['value'] = $viewHelper198->initializeArgumentsAndRender();
$arguments191['keepQuotes'] = false;
$arguments191['encoding'] = 'UTF-8';
$arguments191['doubleEncode'] = true;
$renderChildrenClosure199 = function() use ($renderingContext, $self) {
return NULL;
};
$value200 = ($arguments191['value'] !== NULL ? $arguments191['value'] : $renderChildrenClosure199());

$output177 .= !is_string($value200) && !(is_object($value200) && method_exists($value200, '__toString')) ? $value200 : htmlspecialchars($value200, ($arguments191['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments191['encoding'], $arguments191['doubleEncode']);

$output177 .= '
											</div>
										';
return $output177;
};

$output174 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments175, $renderChildrenClosure176, $renderingContext);

$output174 .= '
									';
return $output174;
};
$viewHelper201 = $self->getViewHelper('$viewHelper201', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FlashMessagesViewHelper');
$viewHelper201->setArguments($arguments172);
$viewHelper201->setRenderingContext($renderingContext);
$viewHelper201->setRenderChildrenClosure($renderChildrenClosure173);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FlashMessagesViewHelper

$output86 .= $viewHelper201->initializeArgumentsAndRender();

$output86 .= '
								</div>
							</fieldset>
						';
return $output86;
};
$viewHelper202 = $self->getViewHelper('$viewHelper202', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper202->setArguments($arguments84);
$viewHelper202->setRenderingContext($renderingContext);
$viewHelper202->setRenderChildrenClosure($renderChildrenClosure85);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output41 .= $viewHelper202->initializeArgumentsAndRender();

$output41 .= '
					</div>
				</div>
			</main>
			<footer class="neos-login-footer">
				<p>
					<a href="http://neos.io" target="_blank">Neos</a> –
					© 2006-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments203 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\DateViewHelper
$arguments204 = array();
$arguments204['date'] = 'now';
$arguments204['format'] = 'Y';
$arguments204['forceLocale'] = NULL;
$arguments204['localeFormatType'] = NULL;
$arguments204['localeFormatLength'] = NULL;
$arguments204['cldrFormat'] = NULL;
$renderChildrenClosure205 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper206 = $self->getViewHelper('$viewHelper206', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\DateViewHelper');
$viewHelper206->setArguments($arguments204);
$viewHelper206->setRenderingContext($renderingContext);
$viewHelper206->setRenderChildrenClosure($renderChildrenClosure205);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\DateViewHelper
$arguments203['value'] = $viewHelper206->initializeArgumentsAndRender();
$arguments203['keepQuotes'] = false;
$arguments203['encoding'] = 'UTF-8';
$arguments203['doubleEncode'] = true;
$renderChildrenClosure207 = function() use ($renderingContext, $self) {
return NULL;
};
$value208 = ($arguments203['value'] !== NULL ? $arguments203['value'] : $renderChildrenClosure207());

$output41 .= !is_string($value208) && !(is_object($value208) && method_exists($value208, '__toString')) ? $value208 : htmlspecialchars($value208, ($arguments203['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments203['encoding'], $arguments203['doubleEncode']);

$output41 .= ' This is free software, licensed under GPL3 or higher, and you are welcome to redistribute it under certain conditions;
					Neos comes with ABSOLUTELY NO WARRANTY;
					See <a href="http://neos.io" target="_blank">neos.io</a> for more details. Obstructing the appearance of this notice is prohibited by law.
				</p>
			</footer>
		</div>

		<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments209 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments210 = array();
$arguments210['path'] = '2/js/bootstrap.min.js';
$arguments210['package'] = 'TYPO3.Twitter.Bootstrap';
$arguments210['resource'] = NULL;
$arguments210['localize'] = true;
$renderChildrenClosure211 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper212 = $self->getViewHelper('$viewHelper212', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper212->setArguments($arguments210);
$viewHelper212->setRenderingContext($renderingContext);
$viewHelper212->setRenderChildrenClosure($renderChildrenClosure211);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments209['value'] = $viewHelper212->initializeArgumentsAndRender();
$arguments209['keepQuotes'] = false;
$arguments209['encoding'] = 'UTF-8';
$arguments209['doubleEncode'] = true;
$renderChildrenClosure213 = function() use ($renderingContext, $self) {
return NULL;
};
$value214 = ($arguments209['value'] !== NULL ? $arguments209['value'] : $renderChildrenClosure213());

$output41 .= !is_string($value214) && !(is_object($value214) && method_exists($value214, '__toString')) ? $value214 : htmlspecialchars($value214, ($arguments209['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments209['encoding'], $arguments209['doubleEncode']);

$output41 .= '"></script>
		<script>
			if ($(\'#username\').val()) {
				$(\'#password\').focus();
			}
			$(\'form\').on(\'submit\', function() {
				$(\'.neos-login-btn\').toggleClass(\'neos-hidden\');
			});
			try {
				$(\'form[name="login"] input[name="lastVisitedNode"]\').val(sessionStorage.getItem(\'TYPO3.Neos.lastVisitedNode\'));
			} catch(e) {}
		</script>
	</body>
';

return $output41;
}
/**
 * Main Render function
 */
public function render(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output215 = '';

$output215 .= '
';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments216 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\LayoutViewHelper
$arguments217 = array();
$arguments217['name'] = 'Default';
$renderChildrenClosure218 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper219 = $self->getViewHelper('$viewHelper219', $renderingContext, 'TYPO3\Fluid\ViewHelpers\LayoutViewHelper');
$viewHelper219->setArguments($arguments217);
$viewHelper219->setRenderingContext($renderingContext);
$viewHelper219->setRenderChildrenClosure($renderChildrenClosure218);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\LayoutViewHelper
$arguments216['value'] = $viewHelper219->initializeArgumentsAndRender();
$arguments216['keepQuotes'] = false;
$arguments216['encoding'] = 'UTF-8';
$arguments216['doubleEncode'] = true;
$renderChildrenClosure220 = function() use ($renderingContext, $self) {
return NULL;
};
$value221 = ($arguments216['value'] !== NULL ? $arguments216['value'] : $renderChildrenClosure220());

$output215 .= !is_string($value221) && !(is_object($value221) && method_exists($value221, '__toString')) ? $value221 : htmlspecialchars($value221, ($arguments216['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments216['encoding'], $arguments216['doubleEncode']);

$output215 .= '

';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\SectionViewHelper
$arguments222 = array();
$arguments222['name'] = 'head';
$renderChildrenClosure223 = function() use ($renderingContext, $self) {
$output224 = '';

$output224 .= '
	<title>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments225 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments226 = array();
$arguments226['id'] = 'login.index.title';
$arguments226['value'] = NULL;
$arguments226['arguments'] = array (
);
$arguments226['source'] = 'Main';
$arguments226['package'] = NULL;
$arguments226['quantity'] = NULL;
$arguments226['languageIdentifier'] = NULL;
$renderChildrenClosure227 = function() use ($renderingContext, $self) {
return 'Login to';
};
$viewHelper228 = $self->getViewHelper('$viewHelper228', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper228->setArguments($arguments226);
$viewHelper228->setRenderingContext($renderingContext);
$viewHelper228->setRenderChildrenClosure($renderChildrenClosure227);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments225['value'] = $viewHelper228->initializeArgumentsAndRender();
$arguments225['keepQuotes'] = false;
$arguments225['encoding'] = 'UTF-8';
$arguments225['doubleEncode'] = true;
$renderChildrenClosure229 = function() use ($renderingContext, $self) {
return NULL;
};
$value230 = ($arguments225['value'] !== NULL ? $arguments225['value'] : $renderChildrenClosure229());

$output224 .= !is_string($value230) && !(is_object($value230) && method_exists($value230, '__toString')) ? $value230 : htmlspecialchars($value230, ($arguments225['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments225['encoding'], $arguments225['doubleEncode']);

$output224 .= ' ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments231 = array();
$arguments231['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments231['keepQuotes'] = false;
$arguments231['encoding'] = 'UTF-8';
$arguments231['doubleEncode'] = true;
$renderChildrenClosure232 = function() use ($renderingContext, $self) {
return NULL;
};
$value233 = ($arguments231['value'] !== NULL ? $arguments231['value'] : $renderChildrenClosure232());

$output224 .= !is_string($value233) && !(is_object($value233) && method_exists($value233, '__toString')) ? $value233 : htmlspecialchars($value233, ($arguments231['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments231['encoding'], $arguments231['doubleEncode']);

$output224 .= '</title>
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments234 = array();
$arguments234['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'styles', $renderingContext);
$arguments234['as'] = 'style';
$arguments234['key'] = '';
$arguments234['reverse'] = false;
$arguments234['iteration'] = NULL;
$renderChildrenClosure235 = function() use ($renderingContext, $self) {
$output236 = '';

$output236 .= '
		<link rel="stylesheet" href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments237 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments238 = array();
$arguments238['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'style', $renderingContext);
$arguments238['package'] = NULL;
$arguments238['resource'] = NULL;
$arguments238['localize'] = true;
$renderChildrenClosure239 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper240 = $self->getViewHelper('$viewHelper240', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper240->setArguments($arguments238);
$viewHelper240->setRenderingContext($renderingContext);
$viewHelper240->setRenderChildrenClosure($renderChildrenClosure239);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments237['value'] = $viewHelper240->initializeArgumentsAndRender();
$arguments237['keepQuotes'] = false;
$arguments237['encoding'] = 'UTF-8';
$arguments237['doubleEncode'] = true;
$renderChildrenClosure241 = function() use ($renderingContext, $self) {
return NULL;
};
$value242 = ($arguments237['value'] !== NULL ? $arguments237['value'] : $renderChildrenClosure241());

$output236 .= !is_string($value242) && !(is_object($value242) && method_exists($value242, '__toString')) ? $value242 : htmlspecialchars($value242, ($arguments237['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments237['encoding'], $arguments237['doubleEncode']);

$output236 .= '" />
	';
return $output236;
};

$output224 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments234, $renderChildrenClosure235, $renderingContext);

$output224 .= '
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments243 = array();
// Rendering Boolean node
$arguments243['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.userInterface.backendLoginForm.backgroundImage', $renderingContext));
$arguments243['then'] = NULL;
$arguments243['else'] = NULL;
$renderChildrenClosure244 = function() use ($renderingContext, $self) {
$output245 = '';

$output245 .= '
		<style type="text/css">
			.neos-login-box:before {
				background-image: url(';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments246 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments247 = array();
$arguments247['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.userInterface.backendLoginForm.backgroundImage', $renderingContext);
$arguments247['package'] = NULL;
$arguments247['resource'] = NULL;
$arguments247['localize'] = true;
$renderChildrenClosure248 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper249 = $self->getViewHelper('$viewHelper249', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper249->setArguments($arguments247);
$viewHelper249->setRenderingContext($renderingContext);
$viewHelper249->setRenderChildrenClosure($renderChildrenClosure248);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments246['value'] = $viewHelper249->initializeArgumentsAndRender();
$arguments246['keepQuotes'] = false;
$arguments246['encoding'] = 'UTF-8';
$arguments246['doubleEncode'] = true;
$renderChildrenClosure250 = function() use ($renderingContext, $self) {
return NULL;
};
$value251 = ($arguments246['value'] !== NULL ? $arguments246['value'] : $renderChildrenClosure250());

$output245 .= !is_string($value251) && !(is_object($value251) && method_exists($value251, '__toString')) ? $value251 : htmlspecialchars($value251, ($arguments246['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments246['encoding'], $arguments246['doubleEncode']);

$output245 .= ');
			}
		</style>
	';
return $output245;
};
$viewHelper252 = $self->getViewHelper('$viewHelper252', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper252->setArguments($arguments243);
$viewHelper252->setRenderingContext($renderingContext);
$viewHelper252->setRenderChildrenClosure($renderChildrenClosure244);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output224 .= $viewHelper252->initializeArgumentsAndRender();

$output224 .= '
	<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments253 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments254 = array();
$arguments254['path'] = 'Library/jquery/jquery-2.0.3.js';
$arguments254['package'] = NULL;
$arguments254['resource'] = NULL;
$arguments254['localize'] = true;
$renderChildrenClosure255 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper256 = $self->getViewHelper('$viewHelper256', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper256->setArguments($arguments254);
$viewHelper256->setRenderingContext($renderingContext);
$viewHelper256->setRenderChildrenClosure($renderChildrenClosure255);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments253['value'] = $viewHelper256->initializeArgumentsAndRender();
$arguments253['keepQuotes'] = false;
$arguments253['encoding'] = 'UTF-8';
$arguments253['doubleEncode'] = true;
$renderChildrenClosure257 = function() use ($renderingContext, $self) {
return NULL;
};
$value258 = ($arguments253['value'] !== NULL ? $arguments253['value'] : $renderChildrenClosure257());

$output224 .= !is_string($value258) && !(is_object($value258) && method_exists($value258, '__toString')) ? $value258 : htmlspecialchars($value258, ($arguments253['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments253['encoding'], $arguments253['doubleEncode']);

$output224 .= '"></script>
	<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments259 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments260 = array();
$arguments260['path'] = 'Library/jquery-ui/js/jquery-ui-1.10.4.custom.js';
$arguments260['package'] = NULL;
$arguments260['resource'] = NULL;
$arguments260['localize'] = true;
$renderChildrenClosure261 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper262 = $self->getViewHelper('$viewHelper262', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper262->setArguments($arguments260);
$viewHelper262->setRenderingContext($renderingContext);
$viewHelper262->setRenderChildrenClosure($renderChildrenClosure261);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments259['value'] = $viewHelper262->initializeArgumentsAndRender();
$arguments259['keepQuotes'] = false;
$arguments259['encoding'] = 'UTF-8';
$arguments259['doubleEncode'] = true;
$renderChildrenClosure263 = function() use ($renderingContext, $self) {
return NULL;
};
$value264 = ($arguments259['value'] !== NULL ? $arguments259['value'] : $renderChildrenClosure263());

$output224 .= !is_string($value264) && !(is_object($value264) && method_exists($value264, '__toString')) ? $value264 : htmlspecialchars($value264, ($arguments259['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments259['encoding'], $arguments259['doubleEncode']);

$output224 .= '"></script>
';
return $output224;
};

$output215 .= '';

$output215 .= '

';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\SectionViewHelper
$arguments265 = array();
$arguments265['name'] = 'body';
$renderChildrenClosure266 = function() use ($renderingContext, $self) {
$output267 = '';

$output267 .= '
	';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments268 = array();
// Rendering Boolean node
$arguments268['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.userInterface.backendLoginForm.backgroundImage', $renderingContext));
$arguments268['then'] = NULL;
$arguments268['else'] = NULL;
$renderChildrenClosure269 = function() use ($renderingContext, $self) {
$output270 = '';

$output270 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments271 = array();
$renderChildrenClosure272 = function() use ($renderingContext, $self) {
$output273 = '';

$output273 .= '
			<body class="neos" style="background-image:url(';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments274 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments275 = array();
$arguments275['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.userInterface.backendLoginForm.backgroundImage', $renderingContext);
$arguments275['package'] = NULL;
$arguments275['resource'] = NULL;
$arguments275['localize'] = true;
$renderChildrenClosure276 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper277 = $self->getViewHelper('$viewHelper277', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper277->setArguments($arguments275);
$viewHelper277->setRenderingContext($renderingContext);
$viewHelper277->setRenderChildrenClosure($renderChildrenClosure276);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments274['value'] = $viewHelper277->initializeArgumentsAndRender();
$arguments274['keepQuotes'] = false;
$arguments274['encoding'] = 'UTF-8';
$arguments274['doubleEncode'] = true;
$renderChildrenClosure278 = function() use ($renderingContext, $self) {
return NULL;
};
$value279 = ($arguments274['value'] !== NULL ? $arguments274['value'] : $renderChildrenClosure278());

$output273 .= !is_string($value279) && !(is_object($value279) && method_exists($value279, '__toString')) ? $value279 : htmlspecialchars($value279, ($arguments274['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments274['encoding'], $arguments274['doubleEncode']);

$output273 .= ');">
		';
return $output273;
};
$viewHelper280 = $self->getViewHelper('$viewHelper280', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper280->setArguments($arguments271);
$viewHelper280->setRenderingContext($renderingContext);
$viewHelper280->setRenderChildrenClosure($renderChildrenClosure272);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output270 .= $viewHelper280->initializeArgumentsAndRender();

$output270 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments281 = array();
$renderChildrenClosure282 = function() use ($renderingContext, $self) {
return '
			<body class="neos">
		';
};
$viewHelper283 = $self->getViewHelper('$viewHelper283', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper283->setArguments($arguments281);
$viewHelper283->setRenderingContext($renderingContext);
$viewHelper283->setRenderChildrenClosure($renderChildrenClosure282);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output270 .= $viewHelper283->initializeArgumentsAndRender();

$output270 .= '
	';
return $output270;
};
$arguments268['__thenClosure'] = function() use ($renderingContext, $self) {
$output284 = '';

$output284 .= '
			<body class="neos" style="background-image:url(';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments285 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments286 = array();
$arguments286['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.userInterface.backendLoginForm.backgroundImage', $renderingContext);
$arguments286['package'] = NULL;
$arguments286['resource'] = NULL;
$arguments286['localize'] = true;
$renderChildrenClosure287 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper288 = $self->getViewHelper('$viewHelper288', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper288->setArguments($arguments286);
$viewHelper288->setRenderingContext($renderingContext);
$viewHelper288->setRenderChildrenClosure($renderChildrenClosure287);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments285['value'] = $viewHelper288->initializeArgumentsAndRender();
$arguments285['keepQuotes'] = false;
$arguments285['encoding'] = 'UTF-8';
$arguments285['doubleEncode'] = true;
$renderChildrenClosure289 = function() use ($renderingContext, $self) {
return NULL;
};
$value290 = ($arguments285['value'] !== NULL ? $arguments285['value'] : $renderChildrenClosure289());

$output284 .= !is_string($value290) && !(is_object($value290) && method_exists($value290, '__toString')) ? $value290 : htmlspecialchars($value290, ($arguments285['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments285['encoding'], $arguments285['doubleEncode']);

$output284 .= ');">
		';
return $output284;
};
$arguments268['__elseClosure'] = function() use ($renderingContext, $self) {
return '
			<body class="neos">
		';
};
$viewHelper291 = $self->getViewHelper('$viewHelper291', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper291->setArguments($arguments268);
$viewHelper291->setRenderingContext($renderingContext);
$viewHelper291->setRenderChildrenClosure($renderChildrenClosure269);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output267 .= $viewHelper291->initializeArgumentsAndRender();

$output267 .= '
		<div class="neos-modal-centered">
			<main class="neos-login-main">
				<div class="neos-login-box ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments292 = array();
// Rendering Boolean node
$arguments292['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.userInterface.backendLoginForm.backgroundImage', $renderingContext));
$arguments292['then'] = 'background-image-active';
$arguments292['else'] = NULL;
$renderChildrenClosure293 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper294 = $self->getViewHelper('$viewHelper294', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper294->setArguments($arguments292);
$viewHelper294->setRenderingContext($renderingContext);
$viewHelper294->setRenderChildrenClosure($renderChildrenClosure293);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output267 .= $viewHelper294->initializeArgumentsAndRender();

$output267 .= '">
					<figure class="neos-login-box-logo">
						<img class="neos-login-box-logo-resource" src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments295 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments296 = array();
$arguments296['path'] = 'Images/Login/Logo.svg';
$arguments296['package'] = NULL;
$arguments296['resource'] = NULL;
$arguments296['localize'] = true;
$renderChildrenClosure297 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper298 = $self->getViewHelper('$viewHelper298', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper298->setArguments($arguments296);
$viewHelper298->setRenderingContext($renderingContext);
$viewHelper298->setRenderChildrenClosure($renderChildrenClosure297);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments295['value'] = $viewHelper298->initializeArgumentsAndRender();
$arguments295['keepQuotes'] = false;
$arguments295['encoding'] = 'UTF-8';
$arguments295['doubleEncode'] = true;
$renderChildrenClosure299 = function() use ($renderingContext, $self) {
return NULL;
};
$value300 = ($arguments295['value'] !== NULL ? $arguments295['value'] : $renderChildrenClosure299());

$output267 .= !is_string($value300) && !(is_object($value300) && method_exists($value300, '__toString')) ? $value300 : htmlspecialchars($value300, ($arguments295['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments295['encoding'], $arguments295['doubleEncode']);

$output267 .= '" width="200px" height="200px" />
					</figure>

					<header>
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments301 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments302 = array();
$arguments302['id'] = 'login.index.title';
$arguments302['value'] = NULL;
$arguments302['arguments'] = array (
);
$arguments302['source'] = 'Main';
$arguments302['package'] = NULL;
$arguments302['quantity'] = NULL;
$arguments302['languageIdentifier'] = NULL;
$renderChildrenClosure303 = function() use ($renderingContext, $self) {
return 'Login to';
};
$viewHelper304 = $self->getViewHelper('$viewHelper304', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper304->setArguments($arguments302);
$viewHelper304->setRenderingContext($renderingContext);
$viewHelper304->setRenderChildrenClosure($renderChildrenClosure303);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments301['value'] = $viewHelper304->initializeArgumentsAndRender();
$arguments301['keepQuotes'] = false;
$arguments301['encoding'] = 'UTF-8';
$arguments301['doubleEncode'] = true;
$renderChildrenClosure305 = function() use ($renderingContext, $self) {
return NULL;
};
$value306 = ($arguments301['value'] !== NULL ? $arguments301['value'] : $renderChildrenClosure305());

$output267 .= !is_string($value306) && !(is_object($value306) && method_exists($value306, '__toString')) ? $value306 : htmlspecialchars($value306, ($arguments301['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments301['encoding'], $arguments301['doubleEncode']);

$output267 .= '
						<strong>';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments307 = array();
$arguments307['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments307['keepQuotes'] = false;
$arguments307['encoding'] = 'UTF-8';
$arguments307['doubleEncode'] = true;
$renderChildrenClosure308 = function() use ($renderingContext, $self) {
return NULL;
};
$value309 = ($arguments307['value'] !== NULL ? $arguments307['value'] : $renderChildrenClosure308());

$output267 .= !is_string($value309) && !(is_object($value309) && method_exists($value309, '__toString')) ? $value309 : htmlspecialchars($value309, ($arguments307['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments307['encoding'], $arguments307['doubleEncode']);

$output267 .= '</strong>
					</header>
					<div class="neos-login-body neos">
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper
$arguments310 = array();
$arguments310['name'] = 'login';
$arguments310['action'] = 'authenticate';
$arguments310['additionalAttributes'] = NULL;
$arguments310['data'] = NULL;
$arguments310['arguments'] = array (
);
$arguments310['controller'] = NULL;
$arguments310['package'] = NULL;
$arguments310['subpackage'] = NULL;
$arguments310['object'] = NULL;
$arguments310['section'] = '';
$arguments310['format'] = '';
$arguments310['additionalParams'] = array (
);
$arguments310['absolute'] = false;
$arguments310['addQueryString'] = false;
$arguments310['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments310['fieldNamePrefix'] = NULL;
$arguments310['actionUri'] = NULL;
$arguments310['objectName'] = NULL;
$arguments310['useParentRequest'] = false;
$arguments310['enctype'] = NULL;
$arguments310['method'] = NULL;
$arguments310['onreset'] = NULL;
$arguments310['onsubmit'] = NULL;
$arguments310['class'] = NULL;
$arguments310['dir'] = NULL;
$arguments310['id'] = NULL;
$arguments310['lang'] = NULL;
$arguments310['style'] = NULL;
$arguments310['title'] = NULL;
$arguments310['accesskey'] = NULL;
$arguments310['tabindex'] = NULL;
$arguments310['onclick'] = NULL;
$renderChildrenClosure311 = function() use ($renderingContext, $self) {
$output312 = '';

$output312 .= '
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper
$arguments313 = array();
$arguments313['name'] = 'lastVisitedNode';
$arguments313['additionalAttributes'] = NULL;
$arguments313['data'] = NULL;
$arguments313['value'] = NULL;
$arguments313['property'] = NULL;
$arguments313['class'] = NULL;
$arguments313['dir'] = NULL;
$arguments313['id'] = NULL;
$arguments313['lang'] = NULL;
$arguments313['style'] = NULL;
$arguments313['title'] = NULL;
$arguments313['accesskey'] = NULL;
$arguments313['tabindex'] = NULL;
$arguments313['onclick'] = NULL;
$renderChildrenClosure314 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper315 = $self->getViewHelper('$viewHelper315', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper');
$viewHelper315->setArguments($arguments313);
$viewHelper315->setRenderingContext($renderingContext);
$viewHelper315->setRenderChildrenClosure($renderChildrenClosure314);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\HiddenViewHelper

$output312 .= $viewHelper315->initializeArgumentsAndRender();

$output312 .= '
							<fieldset>
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments316 = array();
// Rendering Boolean node
$arguments316['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'username', $renderingContext));
$arguments316['then'] = NULL;
$arguments316['else'] = NULL;
$renderChildrenClosure317 = function() use ($renderingContext, $self) {
$output318 = '';

$output318 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments319 = array();
$renderChildrenClosure320 = function() use ($renderingContext, $self) {
$output321 = '';

$output321 .= '
										<div class="neos-controls">
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments322 = array();
// Rendering Boolean node
$arguments322['required'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('required');
$arguments322['id'] = 'username';
$arguments322['type'] = 'text';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments323 = array();
$arguments323['id'] = 'username';
$arguments323['value'] = 'Username';
$arguments323['arguments'] = array (
);
$arguments323['source'] = 'Main';
$arguments323['package'] = NULL;
$arguments323['quantity'] = NULL;
$arguments323['languageIdentifier'] = NULL;
$renderChildrenClosure324 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper325 = $self->getViewHelper('$viewHelper325', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper325->setArguments($arguments323);
$viewHelper325->setRenderingContext($renderingContext);
$viewHelper325->setRenderChildrenClosure($renderChildrenClosure324);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments322['placeholder'] = $viewHelper325->initializeArgumentsAndRender();
$arguments322['class'] = 'neos-span12';
$arguments322['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][username]';
$arguments322['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'username', $renderingContext);
$arguments322['additionalAttributes'] = NULL;
$arguments322['data'] = NULL;
$arguments322['property'] = NULL;
$arguments322['disabled'] = NULL;
$arguments322['maxlength'] = NULL;
$arguments322['readonly'] = NULL;
$arguments322['size'] = NULL;
$arguments322['autofocus'] = NULL;
$arguments322['errorClass'] = 'f3-form-error';
$arguments322['dir'] = NULL;
$arguments322['lang'] = NULL;
$arguments322['style'] = NULL;
$arguments322['title'] = NULL;
$arguments322['accesskey'] = NULL;
$arguments322['tabindex'] = NULL;
$arguments322['onclick'] = NULL;
$renderChildrenClosure326 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper327 = $self->getViewHelper('$viewHelper327', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper327->setArguments($arguments322);
$viewHelper327->setRenderingContext($renderingContext);
$viewHelper327->setRenderChildrenClosure($renderChildrenClosure326);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output321 .= $viewHelper327->initializeArgumentsAndRender();

$output321 .= '
										</div>
										<div class="neos-controls">
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments328 = array();
// Rendering Boolean node
$arguments328['required'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('required');
$arguments328['id'] = 'password';
$arguments328['type'] = 'password';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments329 = array();
$arguments329['id'] = 'password';
$arguments329['value'] = 'Password';
$arguments329['arguments'] = array (
);
$arguments329['source'] = 'Main';
$arguments329['package'] = NULL;
$arguments329['quantity'] = NULL;
$arguments329['languageIdentifier'] = NULL;
$renderChildrenClosure330 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper331 = $self->getViewHelper('$viewHelper331', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper331->setArguments($arguments329);
$viewHelper331->setRenderingContext($renderingContext);
$viewHelper331->setRenderChildrenClosure($renderChildrenClosure330);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments328['placeholder'] = $viewHelper331->initializeArgumentsAndRender();
$arguments328['class'] = 'neos-span12';
$arguments328['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][password]';
// Rendering Array
$array332 = array();
$array332['autofocus'] = 'autofocus';
$arguments328['additionalAttributes'] = $array332;
$arguments328['data'] = NULL;
$arguments328['value'] = NULL;
$arguments328['property'] = NULL;
$arguments328['disabled'] = NULL;
$arguments328['maxlength'] = NULL;
$arguments328['readonly'] = NULL;
$arguments328['size'] = NULL;
$arguments328['autofocus'] = NULL;
$arguments328['errorClass'] = 'f3-form-error';
$arguments328['dir'] = NULL;
$arguments328['lang'] = NULL;
$arguments328['style'] = NULL;
$arguments328['title'] = NULL;
$arguments328['accesskey'] = NULL;
$arguments328['tabindex'] = NULL;
$arguments328['onclick'] = NULL;
$renderChildrenClosure333 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper334 = $self->getViewHelper('$viewHelper334', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper334->setArguments($arguments328);
$viewHelper334->setRenderingContext($renderingContext);
$viewHelper334->setRenderChildrenClosure($renderChildrenClosure333);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output321 .= $viewHelper334->initializeArgumentsAndRender();

$output321 .= '
										</div>
									';
return $output321;
};
$viewHelper335 = $self->getViewHelper('$viewHelper335', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper335->setArguments($arguments319);
$viewHelper335->setRenderingContext($renderingContext);
$viewHelper335->setRenderChildrenClosure($renderChildrenClosure320);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output318 .= $viewHelper335->initializeArgumentsAndRender();

$output318 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments336 = array();
$renderChildrenClosure337 = function() use ($renderingContext, $self) {
$output338 = '';

$output338 .= '
										<div class="neos-controls">
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments339 = array();
// Rendering Boolean node
$arguments339['required'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('required');
$arguments339['id'] = 'username';
$arguments339['type'] = 'text';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments340 = array();
$arguments340['id'] = 'username';
$arguments340['value'] = 'Username';
$arguments340['arguments'] = array (
);
$arguments340['source'] = 'Main';
$arguments340['package'] = NULL;
$arguments340['quantity'] = NULL;
$arguments340['languageIdentifier'] = NULL;
$renderChildrenClosure341 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper342 = $self->getViewHelper('$viewHelper342', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper342->setArguments($arguments340);
$viewHelper342->setRenderingContext($renderingContext);
$viewHelper342->setRenderChildrenClosure($renderChildrenClosure341);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments339['placeholder'] = $viewHelper342->initializeArgumentsAndRender();
$arguments339['class'] = 'neos-span12';
$arguments339['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][username]';
// Rendering Array
$array343 = array();
$array343['autofocus'] = 'autofocus';
$arguments339['additionalAttributes'] = $array343;
$arguments339['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'username', $renderingContext);
$arguments339['data'] = NULL;
$arguments339['property'] = NULL;
$arguments339['disabled'] = NULL;
$arguments339['maxlength'] = NULL;
$arguments339['readonly'] = NULL;
$arguments339['size'] = NULL;
$arguments339['autofocus'] = NULL;
$arguments339['errorClass'] = 'f3-form-error';
$arguments339['dir'] = NULL;
$arguments339['lang'] = NULL;
$arguments339['style'] = NULL;
$arguments339['title'] = NULL;
$arguments339['accesskey'] = NULL;
$arguments339['tabindex'] = NULL;
$arguments339['onclick'] = NULL;
$renderChildrenClosure344 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper345 = $self->getViewHelper('$viewHelper345', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper345->setArguments($arguments339);
$viewHelper345->setRenderingContext($renderingContext);
$viewHelper345->setRenderChildrenClosure($renderChildrenClosure344);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output338 .= $viewHelper345->initializeArgumentsAndRender();

$output338 .= '
										</div>
										<div class="neos-controls">
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments346 = array();
// Rendering Boolean node
$arguments346['required'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('required');
$arguments346['id'] = 'password';
$arguments346['type'] = 'password';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments347 = array();
$arguments347['id'] = 'password';
$arguments347['value'] = 'Password';
$arguments347['arguments'] = array (
);
$arguments347['source'] = 'Main';
$arguments347['package'] = NULL;
$arguments347['quantity'] = NULL;
$arguments347['languageIdentifier'] = NULL;
$renderChildrenClosure348 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper349 = $self->getViewHelper('$viewHelper349', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper349->setArguments($arguments347);
$viewHelper349->setRenderingContext($renderingContext);
$viewHelper349->setRenderChildrenClosure($renderChildrenClosure348);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments346['placeholder'] = $viewHelper349->initializeArgumentsAndRender();
$arguments346['class'] = 'neos-span12';
$arguments346['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][password]';
$arguments346['additionalAttributes'] = NULL;
$arguments346['data'] = NULL;
$arguments346['value'] = NULL;
$arguments346['property'] = NULL;
$arguments346['disabled'] = NULL;
$arguments346['maxlength'] = NULL;
$arguments346['readonly'] = NULL;
$arguments346['size'] = NULL;
$arguments346['autofocus'] = NULL;
$arguments346['errorClass'] = 'f3-form-error';
$arguments346['dir'] = NULL;
$arguments346['lang'] = NULL;
$arguments346['style'] = NULL;
$arguments346['title'] = NULL;
$arguments346['accesskey'] = NULL;
$arguments346['tabindex'] = NULL;
$arguments346['onclick'] = NULL;
$renderChildrenClosure350 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper351 = $self->getViewHelper('$viewHelper351', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper351->setArguments($arguments346);
$viewHelper351->setRenderingContext($renderingContext);
$viewHelper351->setRenderChildrenClosure($renderChildrenClosure350);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output338 .= $viewHelper351->initializeArgumentsAndRender();

$output338 .= '
										</div>
									';
return $output338;
};
$viewHelper352 = $self->getViewHelper('$viewHelper352', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper352->setArguments($arguments336);
$viewHelper352->setRenderingContext($renderingContext);
$viewHelper352->setRenderChildrenClosure($renderChildrenClosure337);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output318 .= $viewHelper352->initializeArgumentsAndRender();

$output318 .= '
								';
return $output318;
};
$arguments316['__thenClosure'] = function() use ($renderingContext, $self) {
$output353 = '';

$output353 .= '
										<div class="neos-controls">
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments354 = array();
// Rendering Boolean node
$arguments354['required'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('required');
$arguments354['id'] = 'username';
$arguments354['type'] = 'text';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments355 = array();
$arguments355['id'] = 'username';
$arguments355['value'] = 'Username';
$arguments355['arguments'] = array (
);
$arguments355['source'] = 'Main';
$arguments355['package'] = NULL;
$arguments355['quantity'] = NULL;
$arguments355['languageIdentifier'] = NULL;
$renderChildrenClosure356 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper357 = $self->getViewHelper('$viewHelper357', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper357->setArguments($arguments355);
$viewHelper357->setRenderingContext($renderingContext);
$viewHelper357->setRenderChildrenClosure($renderChildrenClosure356);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments354['placeholder'] = $viewHelper357->initializeArgumentsAndRender();
$arguments354['class'] = 'neos-span12';
$arguments354['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][username]';
$arguments354['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'username', $renderingContext);
$arguments354['additionalAttributes'] = NULL;
$arguments354['data'] = NULL;
$arguments354['property'] = NULL;
$arguments354['disabled'] = NULL;
$arguments354['maxlength'] = NULL;
$arguments354['readonly'] = NULL;
$arguments354['size'] = NULL;
$arguments354['autofocus'] = NULL;
$arguments354['errorClass'] = 'f3-form-error';
$arguments354['dir'] = NULL;
$arguments354['lang'] = NULL;
$arguments354['style'] = NULL;
$arguments354['title'] = NULL;
$arguments354['accesskey'] = NULL;
$arguments354['tabindex'] = NULL;
$arguments354['onclick'] = NULL;
$renderChildrenClosure358 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper359 = $self->getViewHelper('$viewHelper359', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper359->setArguments($arguments354);
$viewHelper359->setRenderingContext($renderingContext);
$viewHelper359->setRenderChildrenClosure($renderChildrenClosure358);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output353 .= $viewHelper359->initializeArgumentsAndRender();

$output353 .= '
										</div>
										<div class="neos-controls">
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments360 = array();
// Rendering Boolean node
$arguments360['required'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('required');
$arguments360['id'] = 'password';
$arguments360['type'] = 'password';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments361 = array();
$arguments361['id'] = 'password';
$arguments361['value'] = 'Password';
$arguments361['arguments'] = array (
);
$arguments361['source'] = 'Main';
$arguments361['package'] = NULL;
$arguments361['quantity'] = NULL;
$arguments361['languageIdentifier'] = NULL;
$renderChildrenClosure362 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper363 = $self->getViewHelper('$viewHelper363', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper363->setArguments($arguments361);
$viewHelper363->setRenderingContext($renderingContext);
$viewHelper363->setRenderChildrenClosure($renderChildrenClosure362);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments360['placeholder'] = $viewHelper363->initializeArgumentsAndRender();
$arguments360['class'] = 'neos-span12';
$arguments360['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][password]';
// Rendering Array
$array364 = array();
$array364['autofocus'] = 'autofocus';
$arguments360['additionalAttributes'] = $array364;
$arguments360['data'] = NULL;
$arguments360['value'] = NULL;
$arguments360['property'] = NULL;
$arguments360['disabled'] = NULL;
$arguments360['maxlength'] = NULL;
$arguments360['readonly'] = NULL;
$arguments360['size'] = NULL;
$arguments360['autofocus'] = NULL;
$arguments360['errorClass'] = 'f3-form-error';
$arguments360['dir'] = NULL;
$arguments360['lang'] = NULL;
$arguments360['style'] = NULL;
$arguments360['title'] = NULL;
$arguments360['accesskey'] = NULL;
$arguments360['tabindex'] = NULL;
$arguments360['onclick'] = NULL;
$renderChildrenClosure365 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper366 = $self->getViewHelper('$viewHelper366', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper366->setArguments($arguments360);
$viewHelper366->setRenderingContext($renderingContext);
$viewHelper366->setRenderChildrenClosure($renderChildrenClosure365);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output353 .= $viewHelper366->initializeArgumentsAndRender();

$output353 .= '
										</div>
									';
return $output353;
};
$arguments316['__elseClosure'] = function() use ($renderingContext, $self) {
$output367 = '';

$output367 .= '
										<div class="neos-controls">
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments368 = array();
// Rendering Boolean node
$arguments368['required'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('required');
$arguments368['id'] = 'username';
$arguments368['type'] = 'text';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments369 = array();
$arguments369['id'] = 'username';
$arguments369['value'] = 'Username';
$arguments369['arguments'] = array (
);
$arguments369['source'] = 'Main';
$arguments369['package'] = NULL;
$arguments369['quantity'] = NULL;
$arguments369['languageIdentifier'] = NULL;
$renderChildrenClosure370 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper371 = $self->getViewHelper('$viewHelper371', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper371->setArguments($arguments369);
$viewHelper371->setRenderingContext($renderingContext);
$viewHelper371->setRenderChildrenClosure($renderChildrenClosure370);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments368['placeholder'] = $viewHelper371->initializeArgumentsAndRender();
$arguments368['class'] = 'neos-span12';
$arguments368['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][username]';
// Rendering Array
$array372 = array();
$array372['autofocus'] = 'autofocus';
$arguments368['additionalAttributes'] = $array372;
$arguments368['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'username', $renderingContext);
$arguments368['data'] = NULL;
$arguments368['property'] = NULL;
$arguments368['disabled'] = NULL;
$arguments368['maxlength'] = NULL;
$arguments368['readonly'] = NULL;
$arguments368['size'] = NULL;
$arguments368['autofocus'] = NULL;
$arguments368['errorClass'] = 'f3-form-error';
$arguments368['dir'] = NULL;
$arguments368['lang'] = NULL;
$arguments368['style'] = NULL;
$arguments368['title'] = NULL;
$arguments368['accesskey'] = NULL;
$arguments368['tabindex'] = NULL;
$arguments368['onclick'] = NULL;
$renderChildrenClosure373 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper374 = $self->getViewHelper('$viewHelper374', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper374->setArguments($arguments368);
$viewHelper374->setRenderingContext($renderingContext);
$viewHelper374->setRenderChildrenClosure($renderChildrenClosure373);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output367 .= $viewHelper374->initializeArgumentsAndRender();

$output367 .= '
										</div>
										<div class="neos-controls">
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper
$arguments375 = array();
// Rendering Boolean node
$arguments375['required'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('required');
$arguments375['id'] = 'password';
$arguments375['type'] = 'password';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments376 = array();
$arguments376['id'] = 'password';
$arguments376['value'] = 'Password';
$arguments376['arguments'] = array (
);
$arguments376['source'] = 'Main';
$arguments376['package'] = NULL;
$arguments376['quantity'] = NULL;
$arguments376['languageIdentifier'] = NULL;
$renderChildrenClosure377 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper378 = $self->getViewHelper('$viewHelper378', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper378->setArguments($arguments376);
$viewHelper378->setRenderingContext($renderingContext);
$viewHelper378->setRenderChildrenClosure($renderChildrenClosure377);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments375['placeholder'] = $viewHelper378->initializeArgumentsAndRender();
$arguments375['class'] = 'neos-span12';
$arguments375['name'] = '__authentication[TYPO3][Flow][Security][Authentication][Token][UsernamePassword][password]';
$arguments375['additionalAttributes'] = NULL;
$arguments375['data'] = NULL;
$arguments375['value'] = NULL;
$arguments375['property'] = NULL;
$arguments375['disabled'] = NULL;
$arguments375['maxlength'] = NULL;
$arguments375['readonly'] = NULL;
$arguments375['size'] = NULL;
$arguments375['autofocus'] = NULL;
$arguments375['errorClass'] = 'f3-form-error';
$arguments375['dir'] = NULL;
$arguments375['lang'] = NULL;
$arguments375['style'] = NULL;
$arguments375['title'] = NULL;
$arguments375['accesskey'] = NULL;
$arguments375['tabindex'] = NULL;
$arguments375['onclick'] = NULL;
$renderChildrenClosure379 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper380 = $self->getViewHelper('$viewHelper380', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper');
$viewHelper380->setArguments($arguments375);
$viewHelper380->setRenderingContext($renderingContext);
$viewHelper380->setRenderChildrenClosure($renderChildrenClosure379);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\TextfieldViewHelper

$output367 .= $viewHelper380->initializeArgumentsAndRender();

$output367 .= '
										</div>
									';
return $output367;
};
$viewHelper381 = $self->getViewHelper('$viewHelper381', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper381->setArguments($arguments316);
$viewHelper381->setRenderingContext($renderingContext);
$viewHelper381->setRenderChildrenClosure($renderChildrenClosure317);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output312 .= $viewHelper381->initializeArgumentsAndRender();

$output312 .= '
								<div class="neos-actions">
									<!-- Forgot password link will be here -->
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Form\ButtonViewHelper
$arguments382 = array();
$arguments382['type'] = 'submit';
$arguments382['class'] = 'neos-span5 neos-pull-right neos-button neos-login-btn';
$arguments382['additionalAttributes'] = NULL;
$arguments382['data'] = NULL;
$arguments382['name'] = NULL;
$arguments382['value'] = NULL;
$arguments382['property'] = NULL;
$arguments382['autofocus'] = NULL;
$arguments382['disabled'] = NULL;
$arguments382['form'] = NULL;
$arguments382['formaction'] = NULL;
$arguments382['formenctype'] = NULL;
$arguments382['formmethod'] = NULL;
$arguments382['formnovalidate'] = NULL;
$arguments382['formtarget'] = NULL;
$arguments382['dir'] = NULL;
$arguments382['id'] = NULL;
$arguments382['lang'] = NULL;
$arguments382['style'] = NULL;
$arguments382['title'] = NULL;
$arguments382['accesskey'] = NULL;
$arguments382['tabindex'] = NULL;
$arguments382['onclick'] = NULL;
$renderChildrenClosure383 = function() use ($renderingContext, $self) {
$output384 = '';

$output384 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments385 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments386 = array();
$arguments386['id'] = 'login';
$arguments386['value'] = 'Login';
$arguments386['arguments'] = array (
);
$arguments386['source'] = 'Main';
$arguments386['package'] = NULL;
$arguments386['quantity'] = NULL;
$arguments386['languageIdentifier'] = NULL;
$renderChildrenClosure387 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper388 = $self->getViewHelper('$viewHelper388', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper388->setArguments($arguments386);
$viewHelper388->setRenderingContext($renderingContext);
$viewHelper388->setRenderChildrenClosure($renderChildrenClosure387);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments385['value'] = $viewHelper388->initializeArgumentsAndRender();
$arguments385['keepQuotes'] = false;
$arguments385['encoding'] = 'UTF-8';
$arguments385['doubleEncode'] = true;
$renderChildrenClosure389 = function() use ($renderingContext, $self) {
return NULL;
};
$value390 = ($arguments385['value'] !== NULL ? $arguments385['value'] : $renderChildrenClosure389());

$output384 .= !is_string($value390) && !(is_object($value390) && method_exists($value390, '__toString')) ? $value390 : htmlspecialchars($value390, ($arguments385['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments385['encoding'], $arguments385['doubleEncode']);

$output384 .= '
									';
return $output384;
};
$viewHelper391 = $self->getViewHelper('$viewHelper391', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Form\ButtonViewHelper');
$viewHelper391->setArguments($arguments382);
$viewHelper391->setRenderingContext($renderingContext);
$viewHelper391->setRenderChildrenClosure($renderChildrenClosure383);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Form\ButtonViewHelper

$output312 .= $viewHelper391->initializeArgumentsAndRender();

$output312 .= '
									<button class="neos-span5 neos-pull-right neos-button neos-login-btn neos-disabled neos-hidden">
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments392 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments393 = array();
$arguments393['id'] = 'authenticating';
$arguments393['value'] = 'Authenticating';
$arguments393['arguments'] = array (
);
$arguments393['source'] = 'Main';
$arguments393['package'] = NULL;
$arguments393['quantity'] = NULL;
$arguments393['languageIdentifier'] = NULL;
$renderChildrenClosure394 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper395 = $self->getViewHelper('$viewHelper395', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper395->setArguments($arguments393);
$viewHelper395->setRenderingContext($renderingContext);
$viewHelper395->setRenderChildrenClosure($renderChildrenClosure394);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments392['value'] = $viewHelper395->initializeArgumentsAndRender();
$arguments392['keepQuotes'] = false;
$arguments392['encoding'] = 'UTF-8';
$arguments392['doubleEncode'] = true;
$renderChildrenClosure396 = function() use ($renderingContext, $self) {
return NULL;
};
$value397 = ($arguments392['value'] !== NULL ? $arguments392['value'] : $renderChildrenClosure396());

$output312 .= !is_string($value397) && !(is_object($value397) && method_exists($value397, '__toString')) ? $value397 : htmlspecialchars($value397, ($arguments392['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments392['encoding'], $arguments392['doubleEncode']);

$output312 .= '
										<span class="neos-ellipsis"></span>
									</button>
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\FlashMessagesViewHelper
$arguments398 = array();
$arguments398['as'] = 'flashMessages';
$arguments398['additionalAttributes'] = NULL;
$arguments398['data'] = NULL;
$arguments398['severity'] = NULL;
$arguments398['class'] = NULL;
$arguments398['dir'] = NULL;
$arguments398['id'] = NULL;
$arguments398['lang'] = NULL;
$arguments398['style'] = NULL;
$arguments398['title'] = NULL;
$arguments398['accesskey'] = NULL;
$arguments398['tabindex'] = NULL;
$arguments398['onclick'] = NULL;
$renderChildrenClosure399 = function() use ($renderingContext, $self) {
$output400 = '';

$output400 .= '
										';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments401 = array();
$arguments401['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessages', $renderingContext);
$arguments401['as'] = 'flashMessage';
$arguments401['key'] = '';
$arguments401['reverse'] = false;
$arguments401['iteration'] = NULL;
$renderChildrenClosure402 = function() use ($renderingContext, $self) {
$output403 = '';

$output403 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments404 = array();
// Rendering Boolean node
$arguments404['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.severity', $renderingContext), 'OK');
$arguments404['then'] = NULL;
$arguments404['else'] = NULL;
$renderChildrenClosure405 = function() use ($renderingContext, $self) {
return '
												<div class="neos-tooltip neos-bottom neos-in neos-tooltip-success">
											';
};
$viewHelper406 = $self->getViewHelper('$viewHelper406', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper406->setArguments($arguments404);
$viewHelper406->setRenderingContext($renderingContext);
$viewHelper406->setRenderChildrenClosure($renderChildrenClosure405);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output403 .= $viewHelper406->initializeArgumentsAndRender();

$output403 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments407 = array();
// Rendering Boolean node
$arguments407['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.severity', $renderingContext), 'Notice');
$arguments407['then'] = NULL;
$arguments407['else'] = NULL;
$renderChildrenClosure408 = function() use ($renderingContext, $self) {
return '
												<div class="neos-tooltip neos-bottom neos-in neos-tooltip-notice">
											';
};
$viewHelper409 = $self->getViewHelper('$viewHelper409', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper409->setArguments($arguments407);
$viewHelper409->setRenderingContext($renderingContext);
$viewHelper409->setRenderChildrenClosure($renderChildrenClosure408);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output403 .= $viewHelper409->initializeArgumentsAndRender();

$output403 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments410 = array();
// Rendering Boolean node
$arguments410['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.severity', $renderingContext), 'Warning');
$arguments410['then'] = NULL;
$arguments410['else'] = NULL;
$renderChildrenClosure411 = function() use ($renderingContext, $self) {
return '
												<div class="neos-tooltip neos-bottom neos-in neos-tooltip-warning">
											';
};
$viewHelper412 = $self->getViewHelper('$viewHelper412', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper412->setArguments($arguments410);
$viewHelper412->setRenderingContext($renderingContext);
$viewHelper412->setRenderChildrenClosure($renderChildrenClosure411);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output403 .= $viewHelper412->initializeArgumentsAndRender();

$output403 .= '
											';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments413 = array();
// Rendering Boolean node
$arguments413['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.severity', $renderingContext), 'Error');
$arguments413['then'] = NULL;
$arguments413['else'] = NULL;
$renderChildrenClosure414 = function() use ($renderingContext, $self) {
$output415 = '';

$output415 .= '
												<script>
													$(function () {
														$(\'fieldset\').effect(\'shake\', ';

$output415 .= '{times: 1}';

$output415 .= ', 60);
													});
												</script>
												<div class="neos-tooltip neos-bottom neos-in neos-tooltip-error">
											';
return $output415;
};
$viewHelper416 = $self->getViewHelper('$viewHelper416', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper416->setArguments($arguments413);
$viewHelper416->setRenderingContext($renderingContext);
$viewHelper416->setRenderChildrenClosure($renderChildrenClosure414);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output403 .= $viewHelper416->initializeArgumentsAndRender();

$output403 .= '
											<div class="neos-tooltip-arrow"></div>
											<div class="neos-tooltip-inner">';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments417 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments418 = array();
$output419 = '';

$output419 .= 'flashMessage.';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments420 = array();
$arguments420['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage.code', $renderingContext);
$arguments420['keepQuotes'] = false;
$arguments420['encoding'] = 'UTF-8';
$arguments420['doubleEncode'] = true;
$renderChildrenClosure421 = function() use ($renderingContext, $self) {
return NULL;
};
$value422 = ($arguments420['value'] !== NULL ? $arguments420['value'] : $renderChildrenClosure421());

$output419 .= !is_string($value422) && !(is_object($value422) && method_exists($value422, '__toString')) ? $value422 : htmlspecialchars($value422, ($arguments420['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments420['encoding'], $arguments420['doubleEncode']);
$arguments418['id'] = $output419;
$arguments418['package'] = 'TYPO3.Neos';
$arguments418['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'flashMessage', $renderingContext);
$arguments418['arguments'] = array (
);
$arguments418['source'] = 'Main';
$arguments418['quantity'] = NULL;
$arguments418['languageIdentifier'] = NULL;
$renderChildrenClosure423 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper424 = $self->getViewHelper('$viewHelper424', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper424->setArguments($arguments418);
$viewHelper424->setRenderingContext($renderingContext);
$viewHelper424->setRenderChildrenClosure($renderChildrenClosure423);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments417['value'] = $viewHelper424->initializeArgumentsAndRender();
$arguments417['keepQuotes'] = false;
$arguments417['encoding'] = 'UTF-8';
$arguments417['doubleEncode'] = true;
$renderChildrenClosure425 = function() use ($renderingContext, $self) {
return NULL;
};
$value426 = ($arguments417['value'] !== NULL ? $arguments417['value'] : $renderChildrenClosure425());

$output403 .= !is_string($value426) && !(is_object($value426) && method_exists($value426, '__toString')) ? $value426 : htmlspecialchars($value426, ($arguments417['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments417['encoding'], $arguments417['doubleEncode']);

$output403 .= '
											</div>
										';
return $output403;
};

$output400 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments401, $renderChildrenClosure402, $renderingContext);

$output400 .= '
									';
return $output400;
};
$viewHelper427 = $self->getViewHelper('$viewHelper427', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FlashMessagesViewHelper');
$viewHelper427->setArguments($arguments398);
$viewHelper427->setRenderingContext($renderingContext);
$viewHelper427->setRenderChildrenClosure($renderChildrenClosure399);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FlashMessagesViewHelper

$output312 .= $viewHelper427->initializeArgumentsAndRender();

$output312 .= '
								</div>
							</fieldset>
						';
return $output312;
};
$viewHelper428 = $self->getViewHelper('$viewHelper428', $renderingContext, 'TYPO3\Fluid\ViewHelpers\FormViewHelper');
$viewHelper428->setArguments($arguments310);
$viewHelper428->setRenderingContext($renderingContext);
$viewHelper428->setRenderChildrenClosure($renderChildrenClosure311);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\FormViewHelper

$output267 .= $viewHelper428->initializeArgumentsAndRender();

$output267 .= '
					</div>
				</div>
			</main>
			<footer class="neos-login-footer">
				<p>
					<a href="http://neos.io" target="_blank">Neos</a> –
					© 2006-';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments429 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\DateViewHelper
$arguments430 = array();
$arguments430['date'] = 'now';
$arguments430['format'] = 'Y';
$arguments430['forceLocale'] = NULL;
$arguments430['localeFormatType'] = NULL;
$arguments430['localeFormatLength'] = NULL;
$arguments430['cldrFormat'] = NULL;
$renderChildrenClosure431 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper432 = $self->getViewHelper('$viewHelper432', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Format\DateViewHelper');
$viewHelper432->setArguments($arguments430);
$viewHelper432->setRenderingContext($renderingContext);
$viewHelper432->setRenderChildrenClosure($renderChildrenClosure431);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Format\DateViewHelper
$arguments429['value'] = $viewHelper432->initializeArgumentsAndRender();
$arguments429['keepQuotes'] = false;
$arguments429['encoding'] = 'UTF-8';
$arguments429['doubleEncode'] = true;
$renderChildrenClosure433 = function() use ($renderingContext, $self) {
return NULL;
};
$value434 = ($arguments429['value'] !== NULL ? $arguments429['value'] : $renderChildrenClosure433());

$output267 .= !is_string($value434) && !(is_object($value434) && method_exists($value434, '__toString')) ? $value434 : htmlspecialchars($value434, ($arguments429['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments429['encoding'], $arguments429['doubleEncode']);

$output267 .= ' This is free software, licensed under GPL3 or higher, and you are welcome to redistribute it under certain conditions;
					Neos comes with ABSOLUTELY NO WARRANTY;
					See <a href="http://neos.io" target="_blank">neos.io</a> for more details. Obstructing the appearance of this notice is prohibited by law.
				</p>
			</footer>
		</div>

		<script src="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments435 = array();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments436 = array();
$arguments436['path'] = '2/js/bootstrap.min.js';
$arguments436['package'] = 'TYPO3.Twitter.Bootstrap';
$arguments436['resource'] = NULL;
$arguments436['localize'] = true;
$renderChildrenClosure437 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper438 = $self->getViewHelper('$viewHelper438', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper');
$viewHelper438->setArguments($arguments436);
$viewHelper438->setRenderingContext($renderingContext);
$viewHelper438->setRenderChildrenClosure($renderChildrenClosure437);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Uri\ResourceViewHelper
$arguments435['value'] = $viewHelper438->initializeArgumentsAndRender();
$arguments435['keepQuotes'] = false;
$arguments435['encoding'] = 'UTF-8';
$arguments435['doubleEncode'] = true;
$renderChildrenClosure439 = function() use ($renderingContext, $self) {
return NULL;
};
$value440 = ($arguments435['value'] !== NULL ? $arguments435['value'] : $renderChildrenClosure439());

$output267 .= !is_string($value440) && !(is_object($value440) && method_exists($value440, '__toString')) ? $value440 : htmlspecialchars($value440, ($arguments435['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments435['encoding'], $arguments435['doubleEncode']);

$output267 .= '"></script>
		<script>
			if ($(\'#username\').val()) {
				$(\'#password\').focus();
			}
			$(\'form\').on(\'submit\', function() {
				$(\'.neos-login-btn\').toggleClass(\'neos-hidden\');
			});
			try {
				$(\'form[name="login"] input[name="lastVisitedNode"]\').val(sessionStorage.getItem(\'TYPO3.Neos.lastVisitedNode\'));
			} catch(e) {}
		</script>
	</body>
';
return $output267;
};

$output215 .= '';

$output215 .= '
';

return $output215;
}


}
#0             135575    