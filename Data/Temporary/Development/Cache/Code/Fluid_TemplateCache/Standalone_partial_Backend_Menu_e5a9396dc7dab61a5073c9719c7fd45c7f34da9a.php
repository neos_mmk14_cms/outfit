<?php class FluidCache_Standalone_partial_Backend_Menu_e5a9396dc7dab61a5073c9719c7fd45c7f34da9a extends \TYPO3\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// TODO
	return new \TYPO3\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;

return NULL;
}
public function hasLayout() {
return FALSE;
}

/**
 * section moduleMenu
 */
public function section_f5e58f59b4cf43b09094648bf94e6f9a9115a00e(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output0 = '';

$output0 .= '
	<div class="neos-menu-section">
		';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper
$arguments1 = array();
$arguments1['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'module.modulePath', $renderingContext);
$output2 = '';

$output2 .= 'neos-menu-headline';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments3 = array();
// Rendering Boolean node
$arguments3['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'module.modulePath', $renderingContext), \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'modulePath', $renderingContext));
$arguments3['then'] = ' neos-active';
$arguments3['else'] = NULL;
$renderChildrenClosure4 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper5 = $self->getViewHelper('$viewHelper5', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper5->setArguments($arguments3);
$viewHelper5->setRenderingContext($renderingContext);
$viewHelper5->setRenderChildrenClosure($renderChildrenClosure4);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output2 .= $viewHelper5->initializeArgumentsAndRender();
$arguments1['class'] = $output2;
$arguments1['additionalAttributes'] = NULL;
$arguments1['data'] = NULL;
$arguments1['action'] = NULL;
$arguments1['arguments'] = array (
);
$arguments1['section'] = '';
$arguments1['format'] = '';
$arguments1['additionalParams'] = array (
);
$arguments1['addQueryString'] = false;
$arguments1['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments1['dir'] = NULL;
$arguments1['id'] = NULL;
$arguments1['lang'] = NULL;
$arguments1['style'] = NULL;
$arguments1['title'] = NULL;
$arguments1['accesskey'] = NULL;
$arguments1['tabindex'] = NULL;
$arguments1['onclick'] = NULL;
$arguments1['name'] = NULL;
$arguments1['rel'] = NULL;
$arguments1['rev'] = NULL;
$arguments1['target'] = NULL;
$renderChildrenClosure6 = function() use ($renderingContext, $self) {
$output7 = '';

$output7 .= '
			<i class="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments8 = array();
// Rendering Boolean node
$arguments8['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'module.icon', $renderingContext));
$arguments8['then'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'module.icon', $renderingContext);
$arguments8['else'] = 'icon-puzzle-piece';
$renderChildrenClosure9 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper10 = $self->getViewHelper('$viewHelper10', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper10->setArguments($arguments8);
$viewHelper10->setRenderingContext($renderingContext);
$viewHelper10->setRenderChildrenClosure($renderChildrenClosure9);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output7 .= $viewHelper10->initializeArgumentsAndRender();

$output7 .= '"></i>
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments11 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments12 = array();
$arguments12['id'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'module.label', $renderingContext);
$arguments12['source'] = 'Modules';
$arguments12['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'module.label', $renderingContext);
$arguments12['arguments'] = array (
);
$arguments12['package'] = NULL;
$arguments12['quantity'] = NULL;
$arguments12['languageIdentifier'] = NULL;
$renderChildrenClosure13 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper14 = $self->getViewHelper('$viewHelper14', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper14->setArguments($arguments12);
$viewHelper14->setRenderingContext($renderingContext);
$viewHelper14->setRenderChildrenClosure($renderChildrenClosure13);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments11['value'] = $viewHelper14->initializeArgumentsAndRender();
$arguments11['keepQuotes'] = false;
$arguments11['encoding'] = 'UTF-8';
$arguments11['doubleEncode'] = true;
$renderChildrenClosure15 = function() use ($renderingContext, $self) {
return NULL;
};
$value16 = ($arguments11['value'] !== NULL ? $arguments11['value'] : $renderChildrenClosure15());

$output7 .= !is_string($value16) && !(is_object($value16) && method_exists($value16, '__toString')) ? $value16 : htmlspecialchars($value16, ($arguments11['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments11['encoding'], $arguments11['doubleEncode']);

$output7 .= '
		';
return $output7;
};
$viewHelper17 = $self->getViewHelper('$viewHelper17', $renderingContext, 'TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper');
$viewHelper17->setArguments($arguments1);
$viewHelper17->setRenderingContext($renderingContext);
$viewHelper17->setRenderChildrenClosure($renderChildrenClosure6);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper

$output0 .= $viewHelper17->initializeArgumentsAndRender();

$output0 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments18 = array();
// Rendering Boolean node
$arguments18['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'module.submodules', $renderingContext));
$arguments18['then'] = NULL;
$arguments18['else'] = NULL;
$renderChildrenClosure19 = function() use ($renderingContext, $self) {
$output20 = '';

$output20 .= '
			<div class="neos-menu-list">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments21 = array();
$arguments21['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'module.submodules', $renderingContext);
$arguments21['as'] = 'submodule';
$arguments21['key'] = '';
$arguments21['reverse'] = false;
$arguments21['iteration'] = NULL;
$renderChildrenClosure22 = function() use ($renderingContext, $self) {
$output23 = '';

$output23 .= '
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments24 = array();
// Rendering Boolean node
$arguments24['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'submodule.hideInMenu', $renderingContext));
$arguments24['then'] = NULL;
$arguments24['else'] = NULL;
$renderChildrenClosure25 = function() use ($renderingContext, $self) {
$output26 = '';

$output26 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments27 = array();
$renderChildrenClosure28 = function() use ($renderingContext, $self) {
$output29 = '';

$output29 .= '
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper
$arguments30 = array();
$arguments30['section'] = 'submoduleMenu';
$arguments30['arguments'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), '_all', $renderingContext);
$arguments30['partial'] = NULL;
$arguments30['optional'] = false;
$renderChildrenClosure31 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper32 = $self->getViewHelper('$viewHelper32', $renderingContext, 'TYPO3\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper32->setArguments($arguments30);
$viewHelper32->setRenderingContext($renderingContext);
$viewHelper32->setRenderChildrenClosure($renderChildrenClosure31);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper

$output29 .= $viewHelper32->initializeArgumentsAndRender();

$output29 .= '
						';
return $output29;
};
$viewHelper33 = $self->getViewHelper('$viewHelper33', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper33->setArguments($arguments27);
$viewHelper33->setRenderingContext($renderingContext);
$viewHelper33->setRenderChildrenClosure($renderChildrenClosure28);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output26 .= $viewHelper33->initializeArgumentsAndRender();

$output26 .= '
					';
return $output26;
};
$arguments24['__elseClosure'] = function() use ($renderingContext, $self) {
$output34 = '';

$output34 .= '
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper
$arguments35 = array();
$arguments35['section'] = 'submoduleMenu';
$arguments35['arguments'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), '_all', $renderingContext);
$arguments35['partial'] = NULL;
$arguments35['optional'] = false;
$renderChildrenClosure36 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper37 = $self->getViewHelper('$viewHelper37', $renderingContext, 'TYPO3\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper37->setArguments($arguments35);
$viewHelper37->setRenderingContext($renderingContext);
$viewHelper37->setRenderChildrenClosure($renderChildrenClosure36);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper

$output34 .= $viewHelper37->initializeArgumentsAndRender();

$output34 .= '
						';
return $output34;
};
$viewHelper38 = $self->getViewHelper('$viewHelper38', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper38->setArguments($arguments24);
$viewHelper38->setRenderingContext($renderingContext);
$viewHelper38->setRenderChildrenClosure($renderChildrenClosure25);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output23 .= $viewHelper38->initializeArgumentsAndRender();

$output23 .= '
				';
return $output23;
};

$output20 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments21, $renderChildrenClosure22, $renderingContext);

$output20 .= '
			</div>
		';
return $output20;
};
$viewHelper39 = $self->getViewHelper('$viewHelper39', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper39->setArguments($arguments18);
$viewHelper39->setRenderingContext($renderingContext);
$viewHelper39->setRenderChildrenClosure($renderChildrenClosure19);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output0 .= $viewHelper39->initializeArgumentsAndRender();

$output0 .= '
	</div>
';

return $output0;
}
/**
 * section submoduleMenu
 */
public function section_8accdf636c223cfda421823bb877c78e4efcda9a(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output40 = '';

$output40 .= '
	';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper
$arguments41 = array();
$arguments41['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'submodule.modulePath', $renderingContext);
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments42 = array();
// Rendering Boolean node
$arguments42['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'submodule.modulePath', $renderingContext), \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'modulePath', $renderingContext));
$arguments42['then'] = ' neos-active';
$arguments42['else'] = NULL;
$renderChildrenClosure43 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper44 = $self->getViewHelper('$viewHelper44', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper44->setArguments($arguments42);
$viewHelper44->setRenderingContext($renderingContext);
$viewHelper44->setRenderChildrenClosure($renderChildrenClosure43);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments41['class'] = $viewHelper44->initializeArgumentsAndRender();
$arguments41['additionalAttributes'] = NULL;
$arguments41['data'] = NULL;
$arguments41['action'] = NULL;
$arguments41['arguments'] = array (
);
$arguments41['section'] = '';
$arguments41['format'] = '';
$arguments41['additionalParams'] = array (
);
$arguments41['addQueryString'] = false;
$arguments41['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments41['dir'] = NULL;
$arguments41['id'] = NULL;
$arguments41['lang'] = NULL;
$arguments41['style'] = NULL;
$arguments41['title'] = NULL;
$arguments41['accesskey'] = NULL;
$arguments41['tabindex'] = NULL;
$arguments41['onclick'] = NULL;
$arguments41['name'] = NULL;
$arguments41['rel'] = NULL;
$arguments41['rev'] = NULL;
$arguments41['target'] = NULL;
$renderChildrenClosure45 = function() use ($renderingContext, $self) {
$output46 = '';

$output46 .= '
		<i class="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments47 = array();
// Rendering Boolean node
$arguments47['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'submodule.icon', $renderingContext));
$arguments47['then'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'submodule.icon', $renderingContext);
$arguments47['else'] = 'icon-puzzle-piece';
$renderChildrenClosure48 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper49 = $self->getViewHelper('$viewHelper49', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper49->setArguments($arguments47);
$viewHelper49->setRenderingContext($renderingContext);
$viewHelper49->setRenderChildrenClosure($renderChildrenClosure48);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output46 .= $viewHelper49->initializeArgumentsAndRender();

$output46 .= '"></i>
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments50 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments51 = array();
$arguments51['id'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'submodule.label', $renderingContext);
$arguments51['source'] = 'Modules';
$arguments51['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'submodule.label', $renderingContext);
$arguments51['arguments'] = array (
);
$arguments51['package'] = NULL;
$arguments51['quantity'] = NULL;
$arguments51['languageIdentifier'] = NULL;
$renderChildrenClosure52 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper53 = $self->getViewHelper('$viewHelper53', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper53->setArguments($arguments51);
$viewHelper53->setRenderingContext($renderingContext);
$viewHelper53->setRenderChildrenClosure($renderChildrenClosure52);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments50['value'] = $viewHelper53->initializeArgumentsAndRender();
$arguments50['keepQuotes'] = false;
$arguments50['encoding'] = 'UTF-8';
$arguments50['doubleEncode'] = true;
$renderChildrenClosure54 = function() use ($renderingContext, $self) {
return NULL;
};
$value55 = ($arguments50['value'] !== NULL ? $arguments50['value'] : $renderChildrenClosure54());

$output46 .= !is_string($value55) && !(is_object($value55) && method_exists($value55, '__toString')) ? $value55 : htmlspecialchars($value55, ($arguments50['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments50['encoding'], $arguments50['doubleEncode']);

$output46 .= '
	';
return $output46;
};
$viewHelper56 = $self->getViewHelper('$viewHelper56', $renderingContext, 'TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper');
$viewHelper56->setArguments($arguments41);
$viewHelper56->setRenderingContext($renderingContext);
$viewHelper56->setRenderChildrenClosure($renderChildrenClosure45);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper

$output40 .= $viewHelper56->initializeArgumentsAndRender();

$output40 .= '
';

return $output40;
}
/**
 * Main Render function
 */
public function render(\TYPO3\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output57 = '';

$output57 .= '
<noscript>
	<div id="neos-menu-panel" class="neos-noscript">
		<div class="neos-menu-section">
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments58 = array();
$arguments58['package'] = 'TYPO3.Neos';
$arguments58['controller'] = 'Backend\\Backend';
$arguments58['action'] = 'index';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments59 = array();
$arguments59['id'] = 'contentView';
$arguments59['value'] = 'Content View';
$arguments59['arguments'] = array (
);
$arguments59['source'] = 'Main';
$arguments59['package'] = NULL;
$arguments59['quantity'] = NULL;
$arguments59['languageIdentifier'] = NULL;
$renderChildrenClosure60 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper61 = $self->getViewHelper('$viewHelper61', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper61->setArguments($arguments59);
$viewHelper61->setRenderingContext($renderingContext);
$viewHelper61->setRenderChildrenClosure($renderChildrenClosure60);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments58['title'] = $viewHelper61->initializeArgumentsAndRender();
$arguments58['class'] = 'neos-menu-headline';
$arguments58['additionalAttributes'] = NULL;
$arguments58['data'] = NULL;
$arguments58['arguments'] = array (
);
$arguments58['subpackage'] = NULL;
$arguments58['section'] = '';
$arguments58['format'] = '';
$arguments58['additionalParams'] = array (
);
$arguments58['addQueryString'] = false;
$arguments58['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments58['useParentRequest'] = false;
$arguments58['absolute'] = true;
$arguments58['dir'] = NULL;
$arguments58['id'] = NULL;
$arguments58['lang'] = NULL;
$arguments58['style'] = NULL;
$arguments58['accesskey'] = NULL;
$arguments58['tabindex'] = NULL;
$arguments58['onclick'] = NULL;
$arguments58['name'] = NULL;
$arguments58['rel'] = NULL;
$arguments58['rev'] = NULL;
$arguments58['target'] = NULL;
$renderChildrenClosure62 = function() use ($renderingContext, $self) {
$output63 = '';

$output63 .= '
				<i class="icon-file"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments64 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments65 = array();
$arguments65['id'] = 'content';
$arguments65['value'] = 'Content';
$arguments65['arguments'] = array (
);
$arguments65['source'] = 'Main';
$arguments65['package'] = NULL;
$arguments65['quantity'] = NULL;
$arguments65['languageIdentifier'] = NULL;
$renderChildrenClosure66 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper67 = $self->getViewHelper('$viewHelper67', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper67->setArguments($arguments65);
$viewHelper67->setRenderingContext($renderingContext);
$viewHelper67->setRenderChildrenClosure($renderChildrenClosure66);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments64['value'] = $viewHelper67->initializeArgumentsAndRender();
$arguments64['keepQuotes'] = false;
$arguments64['encoding'] = 'UTF-8';
$arguments64['doubleEncode'] = true;
$renderChildrenClosure68 = function() use ($renderingContext, $self) {
return NULL;
};
$value69 = ($arguments64['value'] !== NULL ? $arguments64['value'] : $renderChildrenClosure68());

$output63 .= !is_string($value69) && !(is_object($value69) && method_exists($value69, '__toString')) ? $value69 : htmlspecialchars($value69, ($arguments64['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments64['encoding'], $arguments64['doubleEncode']);

$output63 .= '
			';
return $output63;
};
$viewHelper70 = $self->getViewHelper('$viewHelper70', $renderingContext, 'TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper70->setArguments($arguments58);
$viewHelper70->setRenderingContext($renderingContext);
$viewHelper70->setRenderChildrenClosure($renderChildrenClosure62);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\Link\ActionViewHelper

$output57 .= $viewHelper70->initializeArgumentsAndRender();

$output57 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments71 = array();
// Rendering Boolean node
$arguments71['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sites', $renderingContext));
$arguments71['then'] = NULL;
$arguments71['else'] = NULL;
$renderChildrenClosure72 = function() use ($renderingContext, $self) {
$output73 = '';

$output73 .= '
				<div class="neos-menu-list">
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments74 = array();
$arguments74['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sites', $renderingContext);
$arguments74['as'] = 'site';
$arguments74['key'] = '';
$arguments74['reverse'] = false;
$arguments74['iteration'] = NULL;
$renderChildrenClosure75 = function() use ($renderingContext, $self) {
$output76 = '';

$output76 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments77 = array();
// Rendering Boolean node
$arguments77['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.uri', $renderingContext));
$arguments77['then'] = NULL;
$arguments77['else'] = NULL;
$renderChildrenClosure78 = function() use ($renderingContext, $self) {
$output79 = '';

$output79 .= '
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments80 = array();
$renderChildrenClosure81 = function() use ($renderingContext, $self) {
$output82 = '';

$output82 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments83 = array();
// Rendering Boolean node
$arguments83['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'modulePath', $renderingContext));
$arguments83['then'] = NULL;
$arguments83['else'] = NULL;
$renderChildrenClosure84 = function() use ($renderingContext, $self) {
$output85 = '';

$output85 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments86 = array();
$renderChildrenClosure87 = function() use ($renderingContext, $self) {
$output88 = '';

$output88 .= '
										<a href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments89 = array();
$arguments89['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.uri', $renderingContext);
$arguments89['keepQuotes'] = false;
$arguments89['encoding'] = 'UTF-8';
$arguments89['doubleEncode'] = true;
$renderChildrenClosure90 = function() use ($renderingContext, $self) {
return NULL;
};
$value91 = ($arguments89['value'] !== NULL ? $arguments89['value'] : $renderChildrenClosure90());

$output88 .= !is_string($value91) && !(is_object($value91) && method_exists($value91, '__toString')) ? $value91 : htmlspecialchars($value91, ($arguments89['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments89['encoding'], $arguments89['doubleEncode']);

$output88 .= '" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments92 = array();
$arguments92['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments92['keepQuotes'] = false;
$arguments92['encoding'] = 'UTF-8';
$arguments92['doubleEncode'] = true;
$renderChildrenClosure93 = function() use ($renderingContext, $self) {
return NULL;
};
$value94 = ($arguments92['value'] !== NULL ? $arguments92['value'] : $renderChildrenClosure93());

$output88 .= !is_string($value94) && !(is_object($value94) && method_exists($value94, '__toString')) ? $value94 : htmlspecialchars($value94, ($arguments92['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments92['encoding'], $arguments92['doubleEncode']);

$output88 .= '">
											<i class="icon-globe"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments95 = array();
$arguments95['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments95['keepQuotes'] = false;
$arguments95['encoding'] = 'UTF-8';
$arguments95['doubleEncode'] = true;
$renderChildrenClosure96 = function() use ($renderingContext, $self) {
return NULL;
};
$value97 = ($arguments95['value'] !== NULL ? $arguments95['value'] : $renderChildrenClosure96());

$output88 .= !is_string($value97) && !(is_object($value97) && method_exists($value97, '__toString')) ? $value97 : htmlspecialchars($value97, ($arguments95['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments95['encoding'], $arguments95['doubleEncode']);

$output88 .= '
										</a>
									';
return $output88;
};
$viewHelper98 = $self->getViewHelper('$viewHelper98', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper98->setArguments($arguments86);
$viewHelper98->setRenderingContext($renderingContext);
$viewHelper98->setRenderChildrenClosure($renderChildrenClosure87);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output85 .= $viewHelper98->initializeArgumentsAndRender();

$output85 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments99 = array();
$renderChildrenClosure100 = function() use ($renderingContext, $self) {
$output101 = '';

$output101 .= '
										<a href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments102 = array();
$arguments102['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.uri', $renderingContext);
$arguments102['keepQuotes'] = false;
$arguments102['encoding'] = 'UTF-8';
$arguments102['doubleEncode'] = true;
$renderChildrenClosure103 = function() use ($renderingContext, $self) {
return NULL;
};
$value104 = ($arguments102['value'] !== NULL ? $arguments102['value'] : $renderChildrenClosure103());

$output101 .= !is_string($value104) && !(is_object($value104) && method_exists($value104, '__toString')) ? $value104 : htmlspecialchars($value104, ($arguments102['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments102['encoding'], $arguments102['doubleEncode']);

$output101 .= '" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments105 = array();
$arguments105['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments105['keepQuotes'] = false;
$arguments105['encoding'] = 'UTF-8';
$arguments105['doubleEncode'] = true;
$renderChildrenClosure106 = function() use ($renderingContext, $self) {
return NULL;
};
$value107 = ($arguments105['value'] !== NULL ? $arguments105['value'] : $renderChildrenClosure106());

$output101 .= !is_string($value107) && !(is_object($value107) && method_exists($value107, '__toString')) ? $value107 : htmlspecialchars($value107, ($arguments105['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments105['encoding'], $arguments105['doubleEncode']);

$output101 .= '" class="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments108 = array();
// Rendering Boolean node
$arguments108['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.active', $renderingContext));
$arguments108['then'] = 'neos-active';
$arguments108['else'] = NULL;
$renderChildrenClosure109 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper110 = $self->getViewHelper('$viewHelper110', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper110->setArguments($arguments108);
$viewHelper110->setRenderingContext($renderingContext);
$viewHelper110->setRenderChildrenClosure($renderChildrenClosure109);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output101 .= $viewHelper110->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments111 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments112 = array();
$arguments112['subject'] = NULL;
$renderChildrenClosure113 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sites', $renderingContext);
};
$viewHelper114 = $self->getViewHelper('$viewHelper114', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper114->setArguments($arguments112);
$viewHelper114->setRenderingContext($renderingContext);
$viewHelper114->setRenderChildrenClosure($renderChildrenClosure113);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments111['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', $viewHelper114->initializeArgumentsAndRender(), 1);
$arguments111['then'] = ' neos-active';
$arguments111['else'] = NULL;
$renderChildrenClosure115 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper116 = $self->getViewHelper('$viewHelper116', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper116->setArguments($arguments111);
$viewHelper116->setRenderingContext($renderingContext);
$viewHelper116->setRenderChildrenClosure($renderChildrenClosure115);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output101 .= $viewHelper116->initializeArgumentsAndRender();

$output101 .= '">
											<i class="icon-globe"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments117 = array();
$arguments117['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments117['keepQuotes'] = false;
$arguments117['encoding'] = 'UTF-8';
$arguments117['doubleEncode'] = true;
$renderChildrenClosure118 = function() use ($renderingContext, $self) {
return NULL;
};
$value119 = ($arguments117['value'] !== NULL ? $arguments117['value'] : $renderChildrenClosure118());

$output101 .= !is_string($value119) && !(is_object($value119) && method_exists($value119, '__toString')) ? $value119 : htmlspecialchars($value119, ($arguments117['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments117['encoding'], $arguments117['doubleEncode']);

$output101 .= '
										</a>
									';
return $output101;
};
$viewHelper120 = $self->getViewHelper('$viewHelper120', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper120->setArguments($arguments99);
$viewHelper120->setRenderingContext($renderingContext);
$viewHelper120->setRenderChildrenClosure($renderChildrenClosure100);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output85 .= $viewHelper120->initializeArgumentsAndRender();

$output85 .= '
								';
return $output85;
};
$arguments83['__thenClosure'] = function() use ($renderingContext, $self) {
$output121 = '';

$output121 .= '
										<a href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments122 = array();
$arguments122['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.uri', $renderingContext);
$arguments122['keepQuotes'] = false;
$arguments122['encoding'] = 'UTF-8';
$arguments122['doubleEncode'] = true;
$renderChildrenClosure123 = function() use ($renderingContext, $self) {
return NULL;
};
$value124 = ($arguments122['value'] !== NULL ? $arguments122['value'] : $renderChildrenClosure123());

$output121 .= !is_string($value124) && !(is_object($value124) && method_exists($value124, '__toString')) ? $value124 : htmlspecialchars($value124, ($arguments122['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments122['encoding'], $arguments122['doubleEncode']);

$output121 .= '" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments125 = array();
$arguments125['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments125['keepQuotes'] = false;
$arguments125['encoding'] = 'UTF-8';
$arguments125['doubleEncode'] = true;
$renderChildrenClosure126 = function() use ($renderingContext, $self) {
return NULL;
};
$value127 = ($arguments125['value'] !== NULL ? $arguments125['value'] : $renderChildrenClosure126());

$output121 .= !is_string($value127) && !(is_object($value127) && method_exists($value127, '__toString')) ? $value127 : htmlspecialchars($value127, ($arguments125['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments125['encoding'], $arguments125['doubleEncode']);

$output121 .= '">
											<i class="icon-globe"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments128 = array();
$arguments128['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments128['keepQuotes'] = false;
$arguments128['encoding'] = 'UTF-8';
$arguments128['doubleEncode'] = true;
$renderChildrenClosure129 = function() use ($renderingContext, $self) {
return NULL;
};
$value130 = ($arguments128['value'] !== NULL ? $arguments128['value'] : $renderChildrenClosure129());

$output121 .= !is_string($value130) && !(is_object($value130) && method_exists($value130, '__toString')) ? $value130 : htmlspecialchars($value130, ($arguments128['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments128['encoding'], $arguments128['doubleEncode']);

$output121 .= '
										</a>
									';
return $output121;
};
$arguments83['__elseClosure'] = function() use ($renderingContext, $self) {
$output131 = '';

$output131 .= '
										<a href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments132 = array();
$arguments132['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.uri', $renderingContext);
$arguments132['keepQuotes'] = false;
$arguments132['encoding'] = 'UTF-8';
$arguments132['doubleEncode'] = true;
$renderChildrenClosure133 = function() use ($renderingContext, $self) {
return NULL;
};
$value134 = ($arguments132['value'] !== NULL ? $arguments132['value'] : $renderChildrenClosure133());

$output131 .= !is_string($value134) && !(is_object($value134) && method_exists($value134, '__toString')) ? $value134 : htmlspecialchars($value134, ($arguments132['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments132['encoding'], $arguments132['doubleEncode']);

$output131 .= '" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments135 = array();
$arguments135['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments135['keepQuotes'] = false;
$arguments135['encoding'] = 'UTF-8';
$arguments135['doubleEncode'] = true;
$renderChildrenClosure136 = function() use ($renderingContext, $self) {
return NULL;
};
$value137 = ($arguments135['value'] !== NULL ? $arguments135['value'] : $renderChildrenClosure136());

$output131 .= !is_string($value137) && !(is_object($value137) && method_exists($value137, '__toString')) ? $value137 : htmlspecialchars($value137, ($arguments135['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments135['encoding'], $arguments135['doubleEncode']);

$output131 .= '" class="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments138 = array();
// Rendering Boolean node
$arguments138['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.active', $renderingContext));
$arguments138['then'] = 'neos-active';
$arguments138['else'] = NULL;
$renderChildrenClosure139 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper140 = $self->getViewHelper('$viewHelper140', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper140->setArguments($arguments138);
$viewHelper140->setRenderingContext($renderingContext);
$viewHelper140->setRenderChildrenClosure($renderChildrenClosure139);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output131 .= $viewHelper140->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments141 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments142 = array();
$arguments142['subject'] = NULL;
$renderChildrenClosure143 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sites', $renderingContext);
};
$viewHelper144 = $self->getViewHelper('$viewHelper144', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper144->setArguments($arguments142);
$viewHelper144->setRenderingContext($renderingContext);
$viewHelper144->setRenderChildrenClosure($renderChildrenClosure143);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments141['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', $viewHelper144->initializeArgumentsAndRender(), 1);
$arguments141['then'] = ' neos-active';
$arguments141['else'] = NULL;
$renderChildrenClosure145 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper146 = $self->getViewHelper('$viewHelper146', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper146->setArguments($arguments141);
$viewHelper146->setRenderingContext($renderingContext);
$viewHelper146->setRenderChildrenClosure($renderChildrenClosure145);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output131 .= $viewHelper146->initializeArgumentsAndRender();

$output131 .= '">
											<i class="icon-globe"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments147 = array();
$arguments147['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments147['keepQuotes'] = false;
$arguments147['encoding'] = 'UTF-8';
$arguments147['doubleEncode'] = true;
$renderChildrenClosure148 = function() use ($renderingContext, $self) {
return NULL;
};
$value149 = ($arguments147['value'] !== NULL ? $arguments147['value'] : $renderChildrenClosure148());

$output131 .= !is_string($value149) && !(is_object($value149) && method_exists($value149, '__toString')) ? $value149 : htmlspecialchars($value149, ($arguments147['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments147['encoding'], $arguments147['doubleEncode']);

$output131 .= '
										</a>
									';
return $output131;
};
$viewHelper150 = $self->getViewHelper('$viewHelper150', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper150->setArguments($arguments83);
$viewHelper150->setRenderingContext($renderingContext);
$viewHelper150->setRenderChildrenClosure($renderChildrenClosure84);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output82 .= $viewHelper150->initializeArgumentsAndRender();

$output82 .= '
							';
return $output82;
};
$viewHelper151 = $self->getViewHelper('$viewHelper151', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper151->setArguments($arguments80);
$viewHelper151->setRenderingContext($renderingContext);
$viewHelper151->setRenderChildrenClosure($renderChildrenClosure81);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output79 .= $viewHelper151->initializeArgumentsAndRender();

$output79 .= '
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments152 = array();
$renderChildrenClosure153 = function() use ($renderingContext, $self) {
$output154 = '';

$output154 .= '
								<span title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments155 = array();
$arguments155['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments155['keepQuotes'] = false;
$arguments155['encoding'] = 'UTF-8';
$arguments155['doubleEncode'] = true;
$renderChildrenClosure156 = function() use ($renderingContext, $self) {
return NULL;
};
$value157 = ($arguments155['value'] !== NULL ? $arguments155['value'] : $renderChildrenClosure156());

$output154 .= !is_string($value157) && !(is_object($value157) && method_exists($value157, '__toString')) ? $value157 : htmlspecialchars($value157, ($arguments155['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments155['encoding'], $arguments155['doubleEncode']);

$output154 .= '" class="neos-menu-item neos-disabled">
									<i class="icon-globe"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments158 = array();
$arguments158['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments158['keepQuotes'] = false;
$arguments158['encoding'] = 'UTF-8';
$arguments158['doubleEncode'] = true;
$renderChildrenClosure159 = function() use ($renderingContext, $self) {
return NULL;
};
$value160 = ($arguments158['value'] !== NULL ? $arguments158['value'] : $renderChildrenClosure159());

$output154 .= !is_string($value160) && !(is_object($value160) && method_exists($value160, '__toString')) ? $value160 : htmlspecialchars($value160, ($arguments158['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments158['encoding'], $arguments158['doubleEncode']);

$output154 .= '
								</span>
							';
return $output154;
};
$viewHelper161 = $self->getViewHelper('$viewHelper161', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper161->setArguments($arguments152);
$viewHelper161->setRenderingContext($renderingContext);
$viewHelper161->setRenderChildrenClosure($renderChildrenClosure153);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output79 .= $viewHelper161->initializeArgumentsAndRender();

$output79 .= '
						';
return $output79;
};
$arguments77['__thenClosure'] = function() use ($renderingContext, $self) {
$output162 = '';

$output162 .= '
								';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments163 = array();
// Rendering Boolean node
$arguments163['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'modulePath', $renderingContext));
$arguments163['then'] = NULL;
$arguments163['else'] = NULL;
$renderChildrenClosure164 = function() use ($renderingContext, $self) {
$output165 = '';

$output165 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper
$arguments166 = array();
$renderChildrenClosure167 = function() use ($renderingContext, $self) {
$output168 = '';

$output168 .= '
										<a href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments169 = array();
$arguments169['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.uri', $renderingContext);
$arguments169['keepQuotes'] = false;
$arguments169['encoding'] = 'UTF-8';
$arguments169['doubleEncode'] = true;
$renderChildrenClosure170 = function() use ($renderingContext, $self) {
return NULL;
};
$value171 = ($arguments169['value'] !== NULL ? $arguments169['value'] : $renderChildrenClosure170());

$output168 .= !is_string($value171) && !(is_object($value171) && method_exists($value171, '__toString')) ? $value171 : htmlspecialchars($value171, ($arguments169['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments169['encoding'], $arguments169['doubleEncode']);

$output168 .= '" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments172 = array();
$arguments172['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments172['keepQuotes'] = false;
$arguments172['encoding'] = 'UTF-8';
$arguments172['doubleEncode'] = true;
$renderChildrenClosure173 = function() use ($renderingContext, $self) {
return NULL;
};
$value174 = ($arguments172['value'] !== NULL ? $arguments172['value'] : $renderChildrenClosure173());

$output168 .= !is_string($value174) && !(is_object($value174) && method_exists($value174, '__toString')) ? $value174 : htmlspecialchars($value174, ($arguments172['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments172['encoding'], $arguments172['doubleEncode']);

$output168 .= '">
											<i class="icon-globe"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments175 = array();
$arguments175['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments175['keepQuotes'] = false;
$arguments175['encoding'] = 'UTF-8';
$arguments175['doubleEncode'] = true;
$renderChildrenClosure176 = function() use ($renderingContext, $self) {
return NULL;
};
$value177 = ($arguments175['value'] !== NULL ? $arguments175['value'] : $renderChildrenClosure176());

$output168 .= !is_string($value177) && !(is_object($value177) && method_exists($value177, '__toString')) ? $value177 : htmlspecialchars($value177, ($arguments175['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments175['encoding'], $arguments175['doubleEncode']);

$output168 .= '
										</a>
									';
return $output168;
};
$viewHelper178 = $self->getViewHelper('$viewHelper178', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper178->setArguments($arguments166);
$viewHelper178->setRenderingContext($renderingContext);
$viewHelper178->setRenderChildrenClosure($renderChildrenClosure167);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ThenViewHelper

$output165 .= $viewHelper178->initializeArgumentsAndRender();

$output165 .= '
									';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments179 = array();
$renderChildrenClosure180 = function() use ($renderingContext, $self) {
$output181 = '';

$output181 .= '
										<a href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments182 = array();
$arguments182['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.uri', $renderingContext);
$arguments182['keepQuotes'] = false;
$arguments182['encoding'] = 'UTF-8';
$arguments182['doubleEncode'] = true;
$renderChildrenClosure183 = function() use ($renderingContext, $self) {
return NULL;
};
$value184 = ($arguments182['value'] !== NULL ? $arguments182['value'] : $renderChildrenClosure183());

$output181 .= !is_string($value184) && !(is_object($value184) && method_exists($value184, '__toString')) ? $value184 : htmlspecialchars($value184, ($arguments182['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments182['encoding'], $arguments182['doubleEncode']);

$output181 .= '" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments185 = array();
$arguments185['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments185['keepQuotes'] = false;
$arguments185['encoding'] = 'UTF-8';
$arguments185['doubleEncode'] = true;
$renderChildrenClosure186 = function() use ($renderingContext, $self) {
return NULL;
};
$value187 = ($arguments185['value'] !== NULL ? $arguments185['value'] : $renderChildrenClosure186());

$output181 .= !is_string($value187) && !(is_object($value187) && method_exists($value187, '__toString')) ? $value187 : htmlspecialchars($value187, ($arguments185['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments185['encoding'], $arguments185['doubleEncode']);

$output181 .= '" class="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments188 = array();
// Rendering Boolean node
$arguments188['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.active', $renderingContext));
$arguments188['then'] = 'neos-active';
$arguments188['else'] = NULL;
$renderChildrenClosure189 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper190 = $self->getViewHelper('$viewHelper190', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper190->setArguments($arguments188);
$viewHelper190->setRenderingContext($renderingContext);
$viewHelper190->setRenderChildrenClosure($renderChildrenClosure189);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output181 .= $viewHelper190->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments191 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments192 = array();
$arguments192['subject'] = NULL;
$renderChildrenClosure193 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sites', $renderingContext);
};
$viewHelper194 = $self->getViewHelper('$viewHelper194', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper194->setArguments($arguments192);
$viewHelper194->setRenderingContext($renderingContext);
$viewHelper194->setRenderChildrenClosure($renderChildrenClosure193);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments191['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', $viewHelper194->initializeArgumentsAndRender(), 1);
$arguments191['then'] = ' neos-active';
$arguments191['else'] = NULL;
$renderChildrenClosure195 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper196 = $self->getViewHelper('$viewHelper196', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper196->setArguments($arguments191);
$viewHelper196->setRenderingContext($renderingContext);
$viewHelper196->setRenderChildrenClosure($renderChildrenClosure195);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output181 .= $viewHelper196->initializeArgumentsAndRender();

$output181 .= '">
											<i class="icon-globe"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments197 = array();
$arguments197['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments197['keepQuotes'] = false;
$arguments197['encoding'] = 'UTF-8';
$arguments197['doubleEncode'] = true;
$renderChildrenClosure198 = function() use ($renderingContext, $self) {
return NULL;
};
$value199 = ($arguments197['value'] !== NULL ? $arguments197['value'] : $renderChildrenClosure198());

$output181 .= !is_string($value199) && !(is_object($value199) && method_exists($value199, '__toString')) ? $value199 : htmlspecialchars($value199, ($arguments197['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments197['encoding'], $arguments197['doubleEncode']);

$output181 .= '
										</a>
									';
return $output181;
};
$viewHelper200 = $self->getViewHelper('$viewHelper200', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper200->setArguments($arguments179);
$viewHelper200->setRenderingContext($renderingContext);
$viewHelper200->setRenderChildrenClosure($renderChildrenClosure180);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output165 .= $viewHelper200->initializeArgumentsAndRender();

$output165 .= '
								';
return $output165;
};
$arguments163['__thenClosure'] = function() use ($renderingContext, $self) {
$output201 = '';

$output201 .= '
										<a href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments202 = array();
$arguments202['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.uri', $renderingContext);
$arguments202['keepQuotes'] = false;
$arguments202['encoding'] = 'UTF-8';
$arguments202['doubleEncode'] = true;
$renderChildrenClosure203 = function() use ($renderingContext, $self) {
return NULL;
};
$value204 = ($arguments202['value'] !== NULL ? $arguments202['value'] : $renderChildrenClosure203());

$output201 .= !is_string($value204) && !(is_object($value204) && method_exists($value204, '__toString')) ? $value204 : htmlspecialchars($value204, ($arguments202['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments202['encoding'], $arguments202['doubleEncode']);

$output201 .= '" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments205 = array();
$arguments205['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments205['keepQuotes'] = false;
$arguments205['encoding'] = 'UTF-8';
$arguments205['doubleEncode'] = true;
$renderChildrenClosure206 = function() use ($renderingContext, $self) {
return NULL;
};
$value207 = ($arguments205['value'] !== NULL ? $arguments205['value'] : $renderChildrenClosure206());

$output201 .= !is_string($value207) && !(is_object($value207) && method_exists($value207, '__toString')) ? $value207 : htmlspecialchars($value207, ($arguments205['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments205['encoding'], $arguments205['doubleEncode']);

$output201 .= '">
											<i class="icon-globe"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments208 = array();
$arguments208['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments208['keepQuotes'] = false;
$arguments208['encoding'] = 'UTF-8';
$arguments208['doubleEncode'] = true;
$renderChildrenClosure209 = function() use ($renderingContext, $self) {
return NULL;
};
$value210 = ($arguments208['value'] !== NULL ? $arguments208['value'] : $renderChildrenClosure209());

$output201 .= !is_string($value210) && !(is_object($value210) && method_exists($value210, '__toString')) ? $value210 : htmlspecialchars($value210, ($arguments208['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments208['encoding'], $arguments208['doubleEncode']);

$output201 .= '
										</a>
									';
return $output201;
};
$arguments163['__elseClosure'] = function() use ($renderingContext, $self) {
$output211 = '';

$output211 .= '
										<a href="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments212 = array();
$arguments212['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.uri', $renderingContext);
$arguments212['keepQuotes'] = false;
$arguments212['encoding'] = 'UTF-8';
$arguments212['doubleEncode'] = true;
$renderChildrenClosure213 = function() use ($renderingContext, $self) {
return NULL;
};
$value214 = ($arguments212['value'] !== NULL ? $arguments212['value'] : $renderChildrenClosure213());

$output211 .= !is_string($value214) && !(is_object($value214) && method_exists($value214, '__toString')) ? $value214 : htmlspecialchars($value214, ($arguments212['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments212['encoding'], $arguments212['doubleEncode']);

$output211 .= '" title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments215 = array();
$arguments215['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments215['keepQuotes'] = false;
$arguments215['encoding'] = 'UTF-8';
$arguments215['doubleEncode'] = true;
$renderChildrenClosure216 = function() use ($renderingContext, $self) {
return NULL;
};
$value217 = ($arguments215['value'] !== NULL ? $arguments215['value'] : $renderChildrenClosure216());

$output211 .= !is_string($value217) && !(is_object($value217) && method_exists($value217, '__toString')) ? $value217 : htmlspecialchars($value217, ($arguments215['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments215['encoding'], $arguments215['doubleEncode']);

$output211 .= '" class="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments218 = array();
// Rendering Boolean node
$arguments218['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.active', $renderingContext));
$arguments218['then'] = 'neos-active';
$arguments218['else'] = NULL;
$renderChildrenClosure219 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper220 = $self->getViewHelper('$viewHelper220', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper220->setArguments($arguments218);
$viewHelper220->setRenderingContext($renderingContext);
$viewHelper220->setRenderChildrenClosure($renderChildrenClosure219);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output211 .= $viewHelper220->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments221 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments222 = array();
$arguments222['subject'] = NULL;
$renderChildrenClosure223 = function() use ($renderingContext, $self) {
return \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'sites', $renderingContext);
};
$viewHelper224 = $self->getViewHelper('$viewHelper224', $renderingContext, 'TYPO3\Fluid\ViewHelpers\CountViewHelper');
$viewHelper224->setArguments($arguments222);
$viewHelper224->setRenderingContext($renderingContext);
$viewHelper224->setRenderChildrenClosure($renderChildrenClosure223);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\CountViewHelper
$arguments221['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', $viewHelper224->initializeArgumentsAndRender(), 1);
$arguments221['then'] = ' neos-active';
$arguments221['else'] = NULL;
$renderChildrenClosure225 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper226 = $self->getViewHelper('$viewHelper226', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper226->setArguments($arguments221);
$viewHelper226->setRenderingContext($renderingContext);
$viewHelper226->setRenderChildrenClosure($renderChildrenClosure225);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output211 .= $viewHelper226->initializeArgumentsAndRender();

$output211 .= '">
											<i class="icon-globe"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments227 = array();
$arguments227['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments227['keepQuotes'] = false;
$arguments227['encoding'] = 'UTF-8';
$arguments227['doubleEncode'] = true;
$renderChildrenClosure228 = function() use ($renderingContext, $self) {
return NULL;
};
$value229 = ($arguments227['value'] !== NULL ? $arguments227['value'] : $renderChildrenClosure228());

$output211 .= !is_string($value229) && !(is_object($value229) && method_exists($value229, '__toString')) ? $value229 : htmlspecialchars($value229, ($arguments227['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments227['encoding'], $arguments227['doubleEncode']);

$output211 .= '
										</a>
									';
return $output211;
};
$viewHelper230 = $self->getViewHelper('$viewHelper230', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper230->setArguments($arguments163);
$viewHelper230->setRenderingContext($renderingContext);
$viewHelper230->setRenderChildrenClosure($renderChildrenClosure164);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output162 .= $viewHelper230->initializeArgumentsAndRender();

$output162 .= '
							';
return $output162;
};
$arguments77['__elseClosure'] = function() use ($renderingContext, $self) {
$output231 = '';

$output231 .= '
								<span title="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments232 = array();
$arguments232['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.nodeName', $renderingContext);
$arguments232['keepQuotes'] = false;
$arguments232['encoding'] = 'UTF-8';
$arguments232['doubleEncode'] = true;
$renderChildrenClosure233 = function() use ($renderingContext, $self) {
return NULL;
};
$value234 = ($arguments232['value'] !== NULL ? $arguments232['value'] : $renderChildrenClosure233());

$output231 .= !is_string($value234) && !(is_object($value234) && method_exists($value234, '__toString')) ? $value234 : htmlspecialchars($value234, ($arguments232['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments232['encoding'], $arguments232['doubleEncode']);

$output231 .= '" class="neos-menu-item neos-disabled">
									<i class="icon-globe"></i> ';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments235 = array();
$arguments235['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'site.name', $renderingContext);
$arguments235['keepQuotes'] = false;
$arguments235['encoding'] = 'UTF-8';
$arguments235['doubleEncode'] = true;
$renderChildrenClosure236 = function() use ($renderingContext, $self) {
return NULL;
};
$value237 = ($arguments235['value'] !== NULL ? $arguments235['value'] : $renderChildrenClosure236());

$output231 .= !is_string($value237) && !(is_object($value237) && method_exists($value237, '__toString')) ? $value237 : htmlspecialchars($value237, ($arguments235['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments235['encoding'], $arguments235['doubleEncode']);

$output231 .= '
								</span>
							';
return $output231;
};
$viewHelper238 = $self->getViewHelper('$viewHelper238', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper238->setArguments($arguments77);
$viewHelper238->setRenderingContext($renderingContext);
$viewHelper238->setRenderChildrenClosure($renderChildrenClosure78);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output76 .= $viewHelper238->initializeArgumentsAndRender();

$output76 .= '
					';
return $output76;
};

$output73 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments74, $renderChildrenClosure75, $renderingContext);

$output73 .= '
				</div>
			';
return $output73;
};
$viewHelper239 = $self->getViewHelper('$viewHelper239', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper239->setArguments($arguments71);
$viewHelper239->setRenderingContext($renderingContext);
$viewHelper239->setRenderChildrenClosure($renderChildrenClosure72);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output57 .= $viewHelper239->initializeArgumentsAndRender();

$output57 .= '
		</div>
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments240 = array();
$arguments240['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'modules', $renderingContext);
$arguments240['as'] = 'module';
$arguments240['key'] = '';
$arguments240['reverse'] = false;
$arguments240['iteration'] = NULL;
$renderChildrenClosure241 = function() use ($renderingContext, $self) {
$output242 = '';

$output242 .= '
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments243 = array();
// Rendering Boolean node
$arguments243['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'module.hideInMenu', $renderingContext));
$arguments243['then'] = NULL;
$arguments243['else'] = NULL;
$renderChildrenClosure244 = function() use ($renderingContext, $self) {
$output245 = '';

$output245 .= '
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments246 = array();
$renderChildrenClosure247 = function() use ($renderingContext, $self) {
$output248 = '';

$output248 .= '
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper
$arguments249 = array();
$arguments249['section'] = 'moduleMenu';
$arguments249['arguments'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), '_all', $renderingContext);
$arguments249['partial'] = NULL;
$arguments249['optional'] = false;
$renderChildrenClosure250 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper251 = $self->getViewHelper('$viewHelper251', $renderingContext, 'TYPO3\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper251->setArguments($arguments249);
$viewHelper251->setRenderingContext($renderingContext);
$viewHelper251->setRenderChildrenClosure($renderChildrenClosure250);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper

$output248 .= $viewHelper251->initializeArgumentsAndRender();

$output248 .= '
				';
return $output248;
};
$viewHelper252 = $self->getViewHelper('$viewHelper252', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper252->setArguments($arguments246);
$viewHelper252->setRenderingContext($renderingContext);
$viewHelper252->setRenderChildrenClosure($renderChildrenClosure247);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output245 .= $viewHelper252->initializeArgumentsAndRender();

$output245 .= '
			';
return $output245;
};
$arguments243['__elseClosure'] = function() use ($renderingContext, $self) {
$output253 = '';

$output253 .= '
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper
$arguments254 = array();
$arguments254['section'] = 'moduleMenu';
$arguments254['arguments'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), '_all', $renderingContext);
$arguments254['partial'] = NULL;
$arguments254['optional'] = false;
$renderChildrenClosure255 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper256 = $self->getViewHelper('$viewHelper256', $renderingContext, 'TYPO3\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper256->setArguments($arguments254);
$viewHelper256->setRenderingContext($renderingContext);
$viewHelper256->setRenderChildrenClosure($renderChildrenClosure255);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper

$output253 .= $viewHelper256->initializeArgumentsAndRender();

$output253 .= '
				';
return $output253;
};
$viewHelper257 = $self->getViewHelper('$viewHelper257', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper257->setArguments($arguments243);
$viewHelper257->setRenderingContext($renderingContext);
$viewHelper257->setRenderChildrenClosure($renderChildrenClosure244);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output242 .= $viewHelper257->initializeArgumentsAndRender();

$output242 .= '
		';
return $output242;
};

$output57 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments240, $renderChildrenClosure241, $renderingContext);

$output57 .= '
	</div>
</noscript>

';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\SectionViewHelper
$arguments258 = array();
$arguments258['name'] = 'moduleMenu';
$renderChildrenClosure259 = function() use ($renderingContext, $self) {
$output260 = '';

$output260 .= '
	<div class="neos-menu-section">
		';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper
$arguments261 = array();
$arguments261['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'module.modulePath', $renderingContext);
$output262 = '';

$output262 .= 'neos-menu-headline';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments263 = array();
// Rendering Boolean node
$arguments263['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'module.modulePath', $renderingContext), \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'modulePath', $renderingContext));
$arguments263['then'] = ' neos-active';
$arguments263['else'] = NULL;
$renderChildrenClosure264 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper265 = $self->getViewHelper('$viewHelper265', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper265->setArguments($arguments263);
$viewHelper265->setRenderingContext($renderingContext);
$viewHelper265->setRenderChildrenClosure($renderChildrenClosure264);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output262 .= $viewHelper265->initializeArgumentsAndRender();
$arguments261['class'] = $output262;
$arguments261['additionalAttributes'] = NULL;
$arguments261['data'] = NULL;
$arguments261['action'] = NULL;
$arguments261['arguments'] = array (
);
$arguments261['section'] = '';
$arguments261['format'] = '';
$arguments261['additionalParams'] = array (
);
$arguments261['addQueryString'] = false;
$arguments261['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments261['dir'] = NULL;
$arguments261['id'] = NULL;
$arguments261['lang'] = NULL;
$arguments261['style'] = NULL;
$arguments261['title'] = NULL;
$arguments261['accesskey'] = NULL;
$arguments261['tabindex'] = NULL;
$arguments261['onclick'] = NULL;
$arguments261['name'] = NULL;
$arguments261['rel'] = NULL;
$arguments261['rev'] = NULL;
$arguments261['target'] = NULL;
$renderChildrenClosure266 = function() use ($renderingContext, $self) {
$output267 = '';

$output267 .= '
			<i class="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments268 = array();
// Rendering Boolean node
$arguments268['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'module.icon', $renderingContext));
$arguments268['then'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'module.icon', $renderingContext);
$arguments268['else'] = 'icon-puzzle-piece';
$renderChildrenClosure269 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper270 = $self->getViewHelper('$viewHelper270', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper270->setArguments($arguments268);
$viewHelper270->setRenderingContext($renderingContext);
$viewHelper270->setRenderChildrenClosure($renderChildrenClosure269);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output267 .= $viewHelper270->initializeArgumentsAndRender();

$output267 .= '"></i>
			';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments271 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments272 = array();
$arguments272['id'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'module.label', $renderingContext);
$arguments272['source'] = 'Modules';
$arguments272['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'module.label', $renderingContext);
$arguments272['arguments'] = array (
);
$arguments272['package'] = NULL;
$arguments272['quantity'] = NULL;
$arguments272['languageIdentifier'] = NULL;
$renderChildrenClosure273 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper274 = $self->getViewHelper('$viewHelper274', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper274->setArguments($arguments272);
$viewHelper274->setRenderingContext($renderingContext);
$viewHelper274->setRenderChildrenClosure($renderChildrenClosure273);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments271['value'] = $viewHelper274->initializeArgumentsAndRender();
$arguments271['keepQuotes'] = false;
$arguments271['encoding'] = 'UTF-8';
$arguments271['doubleEncode'] = true;
$renderChildrenClosure275 = function() use ($renderingContext, $self) {
return NULL;
};
$value276 = ($arguments271['value'] !== NULL ? $arguments271['value'] : $renderChildrenClosure275());

$output267 .= !is_string($value276) && !(is_object($value276) && method_exists($value276, '__toString')) ? $value276 : htmlspecialchars($value276, ($arguments271['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments271['encoding'], $arguments271['doubleEncode']);

$output267 .= '
		';
return $output267;
};
$viewHelper277 = $self->getViewHelper('$viewHelper277', $renderingContext, 'TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper');
$viewHelper277->setArguments($arguments261);
$viewHelper277->setRenderingContext($renderingContext);
$viewHelper277->setRenderChildrenClosure($renderChildrenClosure266);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper

$output260 .= $viewHelper277->initializeArgumentsAndRender();

$output260 .= '
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments278 = array();
// Rendering Boolean node
$arguments278['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'module.submodules', $renderingContext));
$arguments278['then'] = NULL;
$arguments278['else'] = NULL;
$renderChildrenClosure279 = function() use ($renderingContext, $self) {
$output280 = '';

$output280 .= '
			<div class="neos-menu-list">
				';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ForViewHelper
$arguments281 = array();
$arguments281['each'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'module.submodules', $renderingContext);
$arguments281['as'] = 'submodule';
$arguments281['key'] = '';
$arguments281['reverse'] = false;
$arguments281['iteration'] = NULL;
$renderChildrenClosure282 = function() use ($renderingContext, $self) {
$output283 = '';

$output283 .= '
					';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments284 = array();
// Rendering Boolean node
$arguments284['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'submodule.hideInMenu', $renderingContext));
$arguments284['then'] = NULL;
$arguments284['else'] = NULL;
$renderChildrenClosure285 = function() use ($renderingContext, $self) {
$output286 = '';

$output286 .= '
						';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper
$arguments287 = array();
$renderChildrenClosure288 = function() use ($renderingContext, $self) {
$output289 = '';

$output289 .= '
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper
$arguments290 = array();
$arguments290['section'] = 'submoduleMenu';
$arguments290['arguments'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), '_all', $renderingContext);
$arguments290['partial'] = NULL;
$arguments290['optional'] = false;
$renderChildrenClosure291 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper292 = $self->getViewHelper('$viewHelper292', $renderingContext, 'TYPO3\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper292->setArguments($arguments290);
$viewHelper292->setRenderingContext($renderingContext);
$viewHelper292->setRenderChildrenClosure($renderChildrenClosure291);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper

$output289 .= $viewHelper292->initializeArgumentsAndRender();

$output289 .= '
						';
return $output289;
};
$viewHelper293 = $self->getViewHelper('$viewHelper293', $renderingContext, 'TYPO3\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper293->setArguments($arguments287);
$viewHelper293->setRenderingContext($renderingContext);
$viewHelper293->setRenderChildrenClosure($renderChildrenClosure288);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\ElseViewHelper

$output286 .= $viewHelper293->initializeArgumentsAndRender();

$output286 .= '
					';
return $output286;
};
$arguments284['__elseClosure'] = function() use ($renderingContext, $self) {
$output294 = '';

$output294 .= '
							';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper
$arguments295 = array();
$arguments295['section'] = 'submoduleMenu';
$arguments295['arguments'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), '_all', $renderingContext);
$arguments295['partial'] = NULL;
$arguments295['optional'] = false;
$renderChildrenClosure296 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper297 = $self->getViewHelper('$viewHelper297', $renderingContext, 'TYPO3\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper297->setArguments($arguments295);
$viewHelper297->setRenderingContext($renderingContext);
$viewHelper297->setRenderChildrenClosure($renderChildrenClosure296);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\RenderViewHelper

$output294 .= $viewHelper297->initializeArgumentsAndRender();

$output294 .= '
						';
return $output294;
};
$viewHelper298 = $self->getViewHelper('$viewHelper298', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper298->setArguments($arguments284);
$viewHelper298->setRenderingContext($renderingContext);
$viewHelper298->setRenderChildrenClosure($renderChildrenClosure285);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output283 .= $viewHelper298->initializeArgumentsAndRender();

$output283 .= '
				';
return $output283;
};

$output280 .= TYPO3\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments281, $renderChildrenClosure282, $renderingContext);

$output280 .= '
			</div>
		';
return $output280;
};
$viewHelper299 = $self->getViewHelper('$viewHelper299', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper299->setArguments($arguments278);
$viewHelper299->setRenderingContext($renderingContext);
$viewHelper299->setRenderChildrenClosure($renderChildrenClosure279);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output260 .= $viewHelper299->initializeArgumentsAndRender();

$output260 .= '
	</div>
';
return $output260;
};

$output57 .= '';

$output57 .= '

';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\SectionViewHelper
$arguments300 = array();
$arguments300['name'] = 'submoduleMenu';
$renderChildrenClosure301 = function() use ($renderingContext, $self) {
$output302 = '';

$output302 .= '
	';
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper
$arguments303 = array();
$arguments303['path'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'submodule.modulePath', $renderingContext);
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments304 = array();
// Rendering Boolean node
$arguments304['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'submodule.modulePath', $renderingContext), \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'modulePath', $renderingContext));
$arguments304['then'] = ' neos-active';
$arguments304['else'] = NULL;
$renderChildrenClosure305 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper306 = $self->getViewHelper('$viewHelper306', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper306->setArguments($arguments304);
$viewHelper306->setRenderingContext($renderingContext);
$viewHelper306->setRenderChildrenClosure($renderChildrenClosure305);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments303['class'] = $viewHelper306->initializeArgumentsAndRender();
$arguments303['additionalAttributes'] = NULL;
$arguments303['data'] = NULL;
$arguments303['action'] = NULL;
$arguments303['arguments'] = array (
);
$arguments303['section'] = '';
$arguments303['format'] = '';
$arguments303['additionalParams'] = array (
);
$arguments303['addQueryString'] = false;
$arguments303['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments303['dir'] = NULL;
$arguments303['id'] = NULL;
$arguments303['lang'] = NULL;
$arguments303['style'] = NULL;
$arguments303['title'] = NULL;
$arguments303['accesskey'] = NULL;
$arguments303['tabindex'] = NULL;
$arguments303['onclick'] = NULL;
$arguments303['name'] = NULL;
$arguments303['rel'] = NULL;
$arguments303['rev'] = NULL;
$arguments303['target'] = NULL;
$renderChildrenClosure307 = function() use ($renderingContext, $self) {
$output308 = '';

$output308 .= '
		<i class="';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper
$arguments309 = array();
// Rendering Boolean node
$arguments309['condition'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(\TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'submodule.icon', $renderingContext));
$arguments309['then'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'submodule.icon', $renderingContext);
$arguments309['else'] = 'icon-puzzle-piece';
$renderChildrenClosure310 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper311 = $self->getViewHelper('$viewHelper311', $renderingContext, 'TYPO3\Fluid\ViewHelpers\IfViewHelper');
$viewHelper311->setArguments($arguments309);
$viewHelper311->setRenderingContext($renderingContext);
$viewHelper311->setRenderChildrenClosure($renderChildrenClosure310);
// End of ViewHelper TYPO3\Fluid\ViewHelpers\IfViewHelper

$output308 .= $viewHelper311->initializeArgumentsAndRender();

$output308 .= '"></i>
		';
// Rendering ViewHelper TYPO3\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments312 = array();
// Rendering ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments313 = array();
$arguments313['id'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'submodule.label', $renderingContext);
$arguments313['source'] = 'Modules';
$arguments313['value'] = \TYPO3\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'submodule.label', $renderingContext);
$arguments313['arguments'] = array (
);
$arguments313['package'] = NULL;
$arguments313['quantity'] = NULL;
$arguments313['languageIdentifier'] = NULL;
$renderChildrenClosure314 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper315 = $self->getViewHelper('$viewHelper315', $renderingContext, 'TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper');
$viewHelper315->setArguments($arguments313);
$viewHelper315->setRenderingContext($renderingContext);
$viewHelper315->setRenderChildrenClosure($renderChildrenClosure314);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Backend\TranslateViewHelper
$arguments312['value'] = $viewHelper315->initializeArgumentsAndRender();
$arguments312['keepQuotes'] = false;
$arguments312['encoding'] = 'UTF-8';
$arguments312['doubleEncode'] = true;
$renderChildrenClosure316 = function() use ($renderingContext, $self) {
return NULL;
};
$value317 = ($arguments312['value'] !== NULL ? $arguments312['value'] : $renderChildrenClosure316());

$output308 .= !is_string($value317) && !(is_object($value317) && method_exists($value317, '__toString')) ? $value317 : htmlspecialchars($value317, ($arguments312['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), $arguments312['encoding'], $arguments312['doubleEncode']);

$output308 .= '
	';
return $output308;
};
$viewHelper318 = $self->getViewHelper('$viewHelper318', $renderingContext, 'TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper');
$viewHelper318->setArguments($arguments303);
$viewHelper318->setRenderingContext($renderingContext);
$viewHelper318->setRenderChildrenClosure($renderChildrenClosure307);
// End of ViewHelper TYPO3\Neos\ViewHelpers\Link\ModuleViewHelper

$output302 .= $viewHelper318->initializeArgumentsAndRender();

$output302 .= '
';
return $output302;
};

$output57 .= '';

$output57 .= '
';

return $output57;
}


}
#0             88542     