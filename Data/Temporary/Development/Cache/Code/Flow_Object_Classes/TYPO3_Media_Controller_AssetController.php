<?php 
namespace TYPO3\Media\Controller;

/*
 * This file is part of the TYPO3.Media package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use Doctrine\Common\Persistence\Proxy as DoctrineProxy;
use Doctrine\ORM\EntityNotFoundException;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Error\Message;
use TYPO3\Flow\I18n\Translator;
use TYPO3\Flow\Mvc\Controller\ActionController;
use TYPO3\Flow\Package\PackageManagerInterface;
use TYPO3\Flow\Mvc\View\JsonView;
use TYPO3\Flow\Mvc\View\ViewInterface;
use TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter;
use TYPO3\Flow\Resource\Resource as FlowResource;
use TYPO3\Flow\Utility\Files;
use TYPO3\Fluid\View\TemplateView;
use TYPO3\Media\Domain\Repository\AssetRepository;
use TYPO3\Media\Domain\Model\AssetInterface;
use TYPO3\Media\Domain\Repository\AudioRepository;
use TYPO3\Media\Domain\Repository\DocumentRepository;
use TYPO3\Media\Domain\Repository\ImageRepository;
use TYPO3\Media\Domain\Repository\TagRepository;
use TYPO3\Media\Domain\Repository\VideoRepository;
use TYPO3\Media\Domain\Model\Asset;
use TYPO3\Media\Domain\Model\AssetCollection;
use TYPO3\Media\Domain\Model\Tag;
use TYPO3\Media\Domain\Repository\AssetCollectionRepository;
use TYPO3\Media\Domain\Session\BrowserState;
use TYPO3\Media\Domain\Service\AssetService;
use TYPO3\Media\Exception\AssetServiceException;
use TYPO3\Media\TypeConverter\AssetInterfaceConverter;

/**
 * Controller for asset handling
 *
 * @Flow\Scope("singleton")
 */
class AssetController_Original extends ActionController
{
    const TAG_GIVEN = 0;
    const TAG_ALL = 1;
    const TAG_NONE = 2;

    const COLLECTION_GIVEN = 0;
    const COLLECTION_ALL = 1;

    /**
     * @Flow\Inject
     * @var AssetRepository
     */
    protected $assetRepository;

    /**
     * @Flow\Inject
     * @var TagRepository
     */
    protected $tagRepository;

    /**
     * @Flow\Inject
     * @var AssetCollectionRepository
     */
    protected $assetCollectionRepository;

    /**
     * @Flow\Inject
     * @var PackageManagerInterface
     */
    protected $packageManager;

    /**
     * @Flow\Inject(lazy = false)
     * @var BrowserState
     */
    protected $browserState;

    /**
     * @Flow\Inject
     * @var AssetService
     */
    protected $assetService;

    /**
     * @Flow\Inject
     * @var Translator
     */
    protected $translator;

    /**
     * @var array
     */
    protected $viewFormatToObjectNameMap = array(
        'html' => TemplateView::class,
        'json' => JsonView::class
    );

    /**
     * Set common variables on the view
     *
     * @param ViewInterface $view
     * @return void
     */
    protected function initializeView(ViewInterface $view)
    {
        $view->assignMultiple(array(
            'view' => $this->browserState->get('view'),
            'sortBy' => $this->browserState->get('sortBy'),
            'sortDirection' => $this->browserState->get('sortDirection'),
            'filter' => $this->browserState->get('filter'),
            'activeTag' => $this->browserState->get('activeTag'),
            'activeAssetCollection' => $this->browserState->get('activeAssetCollection')
        ));
    }

    /**
     * List existing assets
     *
     * @param string $view
     * @param string $sortBy
     * @param string $sortDirection
     * @param string $filter
     * @param integer $tagMode
     * @param Tag $tag
     * @param string $searchTerm
     * @param integer $collectionMode
     * @param AssetCollection $assetCollection
     * @return void
     */
    public function indexAction($view = null, $sortBy = null, $sortDirection = null, $filter = null, $tagMode = self::TAG_GIVEN, Tag $tag = null, $searchTerm = null, $collectionMode = self::COLLECTION_GIVEN, AssetCollection $assetCollection = null)
    {
        if ($view !== null) {
            $this->browserState->set('view', $view);
            $this->view->assign('view', $view);
        }
        if ($sortBy !== null) {
            $this->browserState->set('sortBy', $sortBy);
            $this->view->assign('sortBy', $sortBy);
        }
        if ($sortDirection !== null) {
            $this->browserState->set('sortDirection', $sortDirection);
            $this->view->assign('sortDirection', $sortDirection);
        }
        if ($filter !== null) {
            $this->browserState->set('filter', $filter);
            $this->view->assign('filter', $filter);
        }
        if ($tagMode === self::TAG_GIVEN && $tag !== null) {
            $this->browserState->set('activeTag', $tag);
            $this->view->assign('activeTag', $tag);
        } elseif ($tagMode === self::TAG_NONE || $tagMode === self::TAG_ALL) {
            $this->browserState->set('activeTag', null);
            $this->view->assign('activeTag', null);
        }
        $this->browserState->set('tagMode', $tagMode);

        if ($collectionMode === self::COLLECTION_GIVEN && $assetCollection !== null) {
            $this->browserState->set('activeAssetCollection', $assetCollection);
            $this->view->assign('activeAssetCollection', $assetCollection);
        } elseif ($collectionMode === self::COLLECTION_ALL) {
            $this->browserState->set('activeAssetCollection', null);
            $this->view->assign('activeAssetCollection', null);
        }
        $this->browserState->set('collectionMode', $collectionMode);
        try {
            /** @var AssetCollection $activeAssetCollection */
            $activeAssetCollection = $this->browserState->get('activeAssetCollection');
            if ($activeAssetCollection instanceof DoctrineProxy) {
                // To trigger a possible EntityNotFound have to load the entity
                $activeAssetCollection->__load();
            }
        } catch (EntityNotFoundException $exception) {
            // If a removed tasset collection is still in the browser state it can not be fetched
            $this->browserState->set('activeAssetCollection', null);
            $activeAssetCollection = null;
        }

        // Unset active tag if it isn't available in the active asset collection
        if ($this->browserState->get('activeTag') && $activeAssetCollection !== null && !$activeAssetCollection->getTags()->contains($this->browserState->get('activeTag'))) {
            $this->browserState->set('activeTag', null);
            $this->view->assign('activeTag', null);
        }

        if ($this->browserState->get('filter') !== 'All') {
            switch ($this->browserState->get('filter')) {
                case 'Image':
                    $this->assetRepository = new ImageRepository();
                    break;
                case 'Document':
                    $this->assetRepository = new DocumentRepository();
                    break;
                case 'Video':
                    $this->assetRepository = new VideoRepository();
                    break;
                case 'Audio':
                    $this->assetRepository = new AudioRepository();
                    break;
            }
        }

        switch ($this->browserState->get('sortBy')) {
            case 'Name':
                $this->assetRepository->setDefaultOrderings(array('resource.filename' => $this->browserState->get('sortDirection')));
                break;
            case 'Modified':
            default:
                $this->assetRepository->setDefaultOrderings(array('lastModified' => $this->browserState->get('sortDirection')));
                break;
        }

        if (!$this->browserState->get('activeTag') && $this->browserState->get('tagMode') === self::TAG_GIVEN) {
            $this->browserState->set('tagMode', self::TAG_ALL);
        }

        $assetCollections = array();
        foreach ($this->assetCollectionRepository->findAll() as $assetCollection) {
            $assetCollections[] = array('object' => $assetCollection, 'count' => $this->assetRepository->countByAssetCollection($assetCollection));
        }

        $tags = array();
        foreach ($activeAssetCollection !== null ? $activeAssetCollection->getTags() : $this->tagRepository->findAll() as $tag) {
            $tags[] = array('object' => $tag, 'count' => $this->assetRepository->countByTag($tag, $activeAssetCollection));
        }

        if ($searchTerm !== null) {
            $assets = $this->assetRepository->findBySearchTermOrTags($searchTerm, array(), $activeAssetCollection);
            $this->view->assign('searchTerm', $searchTerm);
        } elseif ($this->browserState->get('tagMode') === self::TAG_NONE) {
            $assets = $this->assetRepository->findUntagged($activeAssetCollection);
        } elseif ($this->browserState->get('activeTag') !== null) {
            $assets = $this->assetRepository->findByTag($this->browserState->get('activeTag'), $activeAssetCollection);
        } else {
            $assets = $activeAssetCollection === null ? $this->assetRepository->findAll() : $this->assetRepository->findByAssetCollection($activeAssetCollection);
        }

        $allCollectionsCount = $this->assetRepository->countAll();
        $maximumFileUploadSize = $this->maximumFileUploadSize();
        $this->view->assignMultiple(array(
            'assets' => $assets,
            'tags' => $tags,
            'allCollectionsCount' => $allCollectionsCount,
            'allCount' => $activeAssetCollection ? $this->assetRepository->countByAssetCollection($activeAssetCollection) : $allCollectionsCount,
            'untaggedCount' => $this->assetRepository->countUntagged($activeAssetCollection),
            'tagMode' => $this->browserState->get('tagMode'),
            'assetCollections' => $assetCollections,
            'argumentNamespace' => $this->request->getArgumentNamespace(),
            'maximumFileUploadSize' => $maximumFileUploadSize,
            'humanReadableMaximumFileUploadSize' => Files::bytesToSizeString($maximumFileUploadSize)
        ));
    }

    /**
     * New asset form
     *
     * @return void
     */
    public function newAction()
    {
        $maximumFileUploadSize = $this->maximumFileUploadSize();
        $this->view->assignMultiple(array(
            'tags' => $this->tagRepository->findAll(),
            'assetCollections' => $this->assetCollectionRepository->findAll(),
            'maximumFileUploadSize' => $maximumFileUploadSize,
            'humanReadableMaximumFileUploadSize' => Files::bytesToSizeString($maximumFileUploadSize)
        ));
    }

    /**
     * @param Asset $asset
     * @return void
     */
    public function replaceAssetResourceAction(Asset $asset)
    {
        $maximumFileUploadSize = $this->maximumFileUploadSize();
        $this->view->assignMultiple(array(
            'asset' => $asset,
            'maximumFileUploadSize' => $maximumFileUploadSize,
            'redirectPackageEnabled' => $this->packageManager->isPackageAvailable('Neos.RedirectHandler'),
            'humanReadableMaximumFileUploadSize' => Files::bytesToSizeString($maximumFileUploadSize)
        ));
    }

    /**
     * Replace the resource on an asset.
     *
     * @param AssetInterface $asset
     * @param FlowResource $resource
     * @param array $options
     * @return void
     */
    public function updateAssetResourceAction(AssetInterface $asset, FlowResource $resource, array $options = [])
    {
        try {
            $this->assetService->replaceAssetResource($asset, $resource, $options);
        } catch (\Exception $exception) {
            $this->addFlashMessage('couldNotReplaceAsset', '', Message::SEVERITY_OK, [], 1463472606);
            $this->forwardToReferringRequest();
        }

        $this->addFlashMessage('assetHasBeenReplaced', '', Message::SEVERITY_OK, [htmlspecialchars($asset->getLabel())]);
        $this->redirect('index');
    }

    /**
     * Edit an asset
     *
     * @param Asset $asset
     * @return void
     */
    public function editAction(Asset $asset)
    {
        $this->view->assignMultiple(array(
            'tags' => $asset->getAssetCollections()->count() > 0 ? $this->tagRepository->findByAssetCollections($asset->getAssetCollections()->toArray()) : $this->tagRepository->findAll(),
            'asset' => $asset,
            'assetCollections' => $this->assetCollectionRepository->findAll()
        ));
    }

    /**
     * @return void
     */
    protected function initializeUpdateAction()
    {
        $assetMappingConfiguration = $this->arguments->getArgument('asset')->getPropertyMappingConfiguration();
        $assetMappingConfiguration->allowProperties('title', 'resource', 'tags', 'assetCollections');
        $assetMappingConfiguration->setTypeConverterOption(PersistentObjectConverter::class, PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, true);
    }

    /**
     * Update an asset
     *
     * @param Asset $asset
     * @return void
     */
    public function updateAction(Asset $asset)
    {
        $this->assetRepository->update($asset);
        $this->addFlashMessage('assetHasBeenUpdated', '', Message::SEVERITY_OK, [htmlspecialchars($asset->getLabel())]);
        $this->redirect('index');
    }

    /**
     * Initialization for createAction
     *
     * @return void
     */
    protected function initializeCreateAction()
    {
        $assetMappingConfiguration = $this->arguments->getArgument('asset')->getPropertyMappingConfiguration();
        $assetMappingConfiguration->allowProperties('title', 'resource', 'tags', 'assetCollections');
        $assetMappingConfiguration->setTypeConverterOption(PersistentObjectConverter::class, PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, true);
        $assetMappingConfiguration->setTypeConverterOption(AssetInterfaceConverter::class, AssetInterfaceConverter::CONFIGURATION_ONE_PER_RESOURCE, true);
    }

    /**
     * Create a new asset
     *
     * @param Asset $asset
     * @return void
     */
    public function createAction(Asset $asset)
    {
        if ($this->persistenceManager->isNewObject($asset)) {
            $this->assetRepository->add($asset);
        }
        $this->addFlashMessage('assetHasBeenAdded', '', Message::SEVERITY_OK, [htmlspecialchars($asset->getLabel())]);
        $this->redirect('index', null, null, array(), 0, 201);
    }

    /**
     * Initialization for uploadAction
     *
     * @return void
     */
    protected function initializeUploadAction()
    {
        $assetMappingConfiguration = $this->arguments->getArgument('asset')->getPropertyMappingConfiguration();
        $assetMappingConfiguration->allowProperties('title', 'resource');
        $assetMappingConfiguration->setTypeConverterOption(PersistentObjectConverter::class, PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, true);
        $assetMappingConfiguration->setTypeConverterOption(AssetInterfaceConverter::class, AssetInterfaceConverter::CONFIGURATION_ONE_PER_RESOURCE, true);
    }

    /**
     * Upload a new asset. No redirection and no response body, for use by plupload (or similar).
     *
     * @param Asset $asset
     * @return string
     */
    public function uploadAction(Asset $asset)
    {
        if (($tag = $this->browserState->get('activeTag')) !== null) {
            $asset->addTag($tag);
        }

        if ($this->persistenceManager->isNewObject($asset)) {
            $this->assetRepository->add($asset);
        } else {
            $this->assetRepository->update($asset);
        }

        if (($assetCollection = $this->browserState->get('activeAssetCollection')) !== null && $assetCollection->addAsset($asset)) {
            $this->assetCollectionRepository->update($assetCollection);
        }

        $this->addFlashMessage('assetHasBeenAdded', '', Message::SEVERITY_OK, [htmlspecialchars($asset->getLabel())]);
        $this->response->setStatus(201);
        return '';
    }

    /**
     * Tags an asset with a tag.
     *
     * No redirection and no response body, no flash message, for use by plupload (or similar).
     *
     * @param Asset $asset
     * @param Tag $tag
     * @return boolean
     */
    public function tagAssetAction(Asset $asset, Tag $tag)
    {
        $success = false;
        if ($asset->addTag($tag)) {
            $this->assetRepository->update($asset);
            $success = true;
        }
        $this->view->assign('value', $success);
    }

    /**
     * Adds an asset to an asset collection
     *
     * @param Asset $asset
     * @param AssetCollection $assetCollection
     * @return boolean
     */
    public function addAssetToCollectionAction(Asset $asset, AssetCollection $assetCollection)
    {
        $success = false;
        if ($assetCollection->addAsset($asset)) {
            $this->assetCollectionRepository->update($assetCollection);
            $success = true;
        }
        $this->view->assign('value', $success);
    }

    /**
     * Delete an asset
     *
     * @param Asset $asset
     * @return void
     */
    public function deleteAction(Asset $asset)
    {
        try {
            $this->assetRepository->remove($asset);
            $this->addFlashMessage('assetHasBeenDeleted', '', Message::SEVERITY_OK, [htmlspecialchars($asset->getLabel())]);
        } catch (AssetServiceException $exception) {
            $this->addFlashMessage('assetCouldNotBeDeleted', '', Message::SEVERITY_WARNING, [], 1462196565);
        }

        $this->redirect('index');
    }

    /**
     * @param string $label
     * @return void
     * @Flow\Validate(argumentName="label", type="NotEmpty")
     * @Flow\Validate(argumentName="label", type="Label")
     */
    public function createTagAction($label)
    {
        $existingTag = $this->tagRepository->findOneByLabel($label);
        if ($existingTag !== null) {
            if (($assetCollection = $this->browserState->get('activeAssetCollection')) !== null && $assetCollection->addTag($existingTag)) {
                $this->assetCollectionRepository->update($assetCollection);
                $this->addFlashMessage('tagAlreadyExistsAndAddedToCollection', '', Message::SEVERITY_OK, [htmlspecialchars($label)]);
            }
        } else {
            $tag = new Tag($label);
            $this->tagRepository->add($tag);
            if (($assetCollection = $this->browserState->get('activeAssetCollection')) !== null && $assetCollection->addTag($tag)) {
                $this->assetCollectionRepository->update($assetCollection);
            }
            $this->addFlashMessage('tagHasBeenCreated', '', Message::SEVERITY_OK, [htmlspecialchars($label)]);
        }
        $this->redirect('index');
    }

    /**
     * @param Tag $tag
     * @return void
     */
    public function editTagAction(Tag $tag)
    {
        $this->view->assignMultiple(array(
            'tag' => $tag,
            'assetCollections' => $this->assetCollectionRepository->findAll()
        ));
    }

    /**
     * @param Tag $tag
     * @return void
     */
    public function updateTagAction(Tag $tag)
    {
        $this->tagRepository->update($tag);
        $this->addFlashMessage('tagHasBeenUpdated', '', Message::SEVERITY_OK, [htmlspecialchars($tag->getLabel())]);
        $this->redirect('index');
    }

    /**
     * @param Tag $tag
     * @return void
     */
    public function deleteTagAction(Tag $tag)
    {
        $taggedAssets = $this->assetRepository->findByTag($tag);
        foreach ($taggedAssets as $asset) {
            $asset->removeTag($tag);
            $this->assetRepository->update($asset);
        }
        $this->tagRepository->remove($tag);
        $this->addFlashMessage('tagHasBeenDeleted', '', Message::SEVERITY_OK, [htmlspecialchars($tag->getLabel())]);
        $this->redirect('index');
    }

    /**
     * @param string $title
     * @return void
     * @Flow\Validate(argumentName="title", type="NotEmpty")
     * @Flow\Validate(argumentName="title", type="Label")
     */
    public function createAssetCollectionAction($title)
    {
        $this->assetCollectionRepository->add(new AssetCollection($title));
        $this->addFlashMessage('collectionHasBeenCreated', '', Message::SEVERITY_OK, [htmlspecialchars($title)]);
        $this->redirect('index');
    }

    /**
     * @param AssetCollection $assetCollection
     * @return void
     */
    public function editAssetCollectionAction(AssetCollection $assetCollection)
    {
        $this->view->assignMultiple(array(
            'assetCollection' => $assetCollection,
            'tags' => $this->tagRepository->findAll()
        ));
    }

    /**
     * @param AssetCollection $assetCollection
     * @return void
     */
    public function updateAssetCollectionAction(AssetCollection $assetCollection)
    {
        $this->assetCollectionRepository->update($assetCollection);
        $this->addFlashMessage('collectionHasBeenUpdated', '', Message::SEVERITY_OK, [htmlspecialchars($assetCollection->getTitle())]);
        $this->redirect('index');
    }

    /**
     * @param AssetCollection $assetCollection
     * @return void
     */
    public function deleteAssetCollectionAction(AssetCollection $assetCollection)
    {
        if ($this->browserState->get('activeAssetCollection') === $assetCollection) {
            $this->browserState->set('activeAssetCollection', null);
        }
        $this->assetCollectionRepository->remove($assetCollection);
        $this->addFlashMessage('collectionHasBeenDeleted', '', Message::SEVERITY_OK, [htmlspecialchars($assetCollection->getTitle())]);
        $this->redirect('index');
    }

    /**
     * Returns the lowest configured maximum upload file size
     *
     * @return integer
     */
    protected function maximumFileUploadSize()
    {
        return min(Files::sizeStringToBytes(ini_get('post_max_size')), Files::sizeStringToBytes(ini_get('upload_max_filesize')));
    }

    /**
     * Add a translated flashMessage.
     *
     * @param string $messageBody The translation id for the message body.
     * @param string $messageTitle The translation id for the message title.
     * @param string $severity
     * @param array $messageArguments
     * @param integer $messageCode
     * @return void
     */
    public function addFlashMessage($messageBody, $messageTitle = '', $severity = Message::SEVERITY_OK, array $messageArguments = array(), $messageCode = null)
    {
        if (is_string($messageBody)) {
            $messageBody = $this->translator->translateById($messageBody, $messageArguments, null, null, 'Main', 'TYPO3.Media') ?: $messageBody;
        }

        $messageTitle = $this->translator->translateById($messageTitle, $messageArguments, null, null, 'Main', 'TYPO3.Media');
        parent::addFlashMessage($messageBody, $messageTitle, $severity, $messageArguments, $messageCode);
    }
}
namespace TYPO3\Media\Controller;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * Controller for asset handling
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class AssetController extends AssetController_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Aop\AdvicesTrait, \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;

    private $Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array();

    private $Flow_Aop_Proxy_groupedAdviceChains = array();

    private $Flow_Aop_Proxy_methodIsInAdviceMode = array();


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
        if (get_class($this) === 'TYPO3\Media\Controller\AssetController') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Media\Controller\AssetController', $this);
        if ('TYPO3\Media\Controller\AssetController' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    protected function Flow_Aop_Proxy_buildMethodsAndAdvicesArray()
    {
        if (method_exists(get_parent_class(), 'Flow_Aop_Proxy_buildMethodsAndAdvicesArray') && is_callable('parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray')) parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        $objectManager = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager;
        $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array(
            'indexAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'newAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'replaceAssetResourceAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'updateAssetResourceAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'editAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'updateAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'initializeCreateAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'createAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'initializeUploadAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'uploadAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'tagAssetAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'addAssetToCollectionAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'deleteAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'createTagAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'editTagAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'updateTagAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'deleteTagAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'createAssetCollectionAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'editAssetCollectionAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'updateAssetCollectionAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'deleteAssetCollectionAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
        );
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
        if (get_class($this) === 'TYPO3\Media\Controller\AssetController') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Media\Controller\AssetController', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
            $result = NULL;
        if (method_exists(get_parent_class(), '__wakeup') && is_callable('parent::__wakeup')) parent::__wakeup();
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __clone()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
    }

    /**
     * Autogenerated Proxy Method
     * @param string $view
     * @param string $sortBy
     * @param string $sortDirection
     * @param string $filter
     * @param integer $tagMode
     * @param Tag $tag
     * @param string $searchTerm
     * @param integer $collectionMode
     * @param AssetCollection $assetCollection
     * @return void
     */
    public function indexAction($view = NULL, $sortBy = NULL, $sortDirection = NULL, $filter = NULL, $tagMode = 0, \TYPO3\Media\Domain\Model\Tag $tag = NULL, $searchTerm = NULL, $collectionMode = 0, \TYPO3\Media\Domain\Model\AssetCollection $assetCollection = NULL)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction'])) {
            $result = parent::indexAction($view, $sortBy, $sortDirection, $filter, $tagMode, $tag, $searchTerm, $collectionMode, $assetCollection);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['view'] = $view;
                $methodArguments['sortBy'] = $sortBy;
                $methodArguments['sortDirection'] = $sortDirection;
                $methodArguments['filter'] = $filter;
                $methodArguments['tagMode'] = $tagMode;
                $methodArguments['tag'] = $tag;
                $methodArguments['searchTerm'] = $searchTerm;
                $methodArguments['collectionMode'] = $collectionMode;
                $methodArguments['assetCollection'] = $assetCollection;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('indexAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'indexAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @return void
     */
    public function newAction()
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['newAction'])) {
            $result = parent::newAction();

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['newAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('newAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'newAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['newAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['newAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Asset $asset
     * @return void
     */
    public function replaceAssetResourceAction(\TYPO3\Media\Domain\Model\Asset $asset)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['replaceAssetResourceAction'])) {
            $result = parent::replaceAssetResourceAction($asset);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['replaceAssetResourceAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['asset'] = $asset;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('replaceAssetResourceAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'replaceAssetResourceAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['replaceAssetResourceAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['replaceAssetResourceAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param AssetInterface $asset
     * @param FlowResource $resource
     * @param array $options
     * @return void
     */
    public function updateAssetResourceAction(\TYPO3\Media\Domain\Model\AssetInterface $asset, \TYPO3\Flow\Resource\Resource $resource, array $options = array())
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAssetResourceAction'])) {
            $result = parent::updateAssetResourceAction($asset, $resource, $options);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAssetResourceAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['asset'] = $asset;
                $methodArguments['resource'] = $resource;
                $methodArguments['options'] = $options;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('updateAssetResourceAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'updateAssetResourceAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAssetResourceAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAssetResourceAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Asset $asset
     * @return void
     */
    public function editAction(\TYPO3\Media\Domain\Model\Asset $asset)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['editAction'])) {
            $result = parent::editAction($asset);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['editAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['asset'] = $asset;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('editAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'editAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['editAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['editAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Asset $asset
     * @return void
     */
    public function updateAction(\TYPO3\Media\Domain\Model\Asset $asset)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAction'])) {
            $result = parent::updateAction($asset);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['asset'] = $asset;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('updateAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'updateAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @return void
     */
    protected function initializeCreateAction()
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['initializeCreateAction'])) {
            $result = parent::initializeCreateAction();

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['initializeCreateAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('initializeCreateAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'initializeCreateAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['initializeCreateAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['initializeCreateAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Asset $asset
     * @return void
     */
    public function createAction(\TYPO3\Media\Domain\Model\Asset $asset)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createAction'])) {
            $result = parent::createAction($asset);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['createAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['asset'] = $asset;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('createAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'createAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @return void
     */
    protected function initializeUploadAction()
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['initializeUploadAction'])) {
            $result = parent::initializeUploadAction();

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['initializeUploadAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('initializeUploadAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'initializeUploadAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['initializeUploadAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['initializeUploadAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Asset $asset
     * @return string
     */
    public function uploadAction(\TYPO3\Media\Domain\Model\Asset $asset)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['uploadAction'])) {
            $result = parent::uploadAction($asset);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['uploadAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['asset'] = $asset;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('uploadAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'uploadAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['uploadAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['uploadAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Asset $asset
     * @param Tag $tag
     * @return boolean
     */
    public function tagAssetAction(\TYPO3\Media\Domain\Model\Asset $asset, \TYPO3\Media\Domain\Model\Tag $tag)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['tagAssetAction'])) {
            $result = parent::tagAssetAction($asset, $tag);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['tagAssetAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['asset'] = $asset;
                $methodArguments['tag'] = $tag;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('tagAssetAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'tagAssetAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['tagAssetAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['tagAssetAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Asset $asset
     * @param AssetCollection $assetCollection
     * @return boolean
     */
    public function addAssetToCollectionAction(\TYPO3\Media\Domain\Model\Asset $asset, \TYPO3\Media\Domain\Model\AssetCollection $assetCollection)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['addAssetToCollectionAction'])) {
            $result = parent::addAssetToCollectionAction($asset, $assetCollection);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['addAssetToCollectionAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['asset'] = $asset;
                $methodArguments['assetCollection'] = $assetCollection;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('addAssetToCollectionAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'addAssetToCollectionAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['addAssetToCollectionAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['addAssetToCollectionAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Asset $asset
     * @return void
     */
    public function deleteAction(\TYPO3\Media\Domain\Model\Asset $asset)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAction'])) {
            $result = parent::deleteAction($asset);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['asset'] = $asset;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('deleteAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'deleteAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param string $label
     * @return void
     * @\TYPO3\Flow\Annotations\Validate(type="NotEmpty", argumentName="label")
     * @\TYPO3\Flow\Annotations\Validate(type="Label", argumentName="label")
     */
    public function createTagAction($label)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createTagAction'])) {
            $result = parent::createTagAction($label);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['createTagAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['label'] = $label;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('createTagAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'createTagAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createTagAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createTagAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Tag $tag
     * @return void
     */
    public function editTagAction(\TYPO3\Media\Domain\Model\Tag $tag)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['editTagAction'])) {
            $result = parent::editTagAction($tag);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['editTagAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['tag'] = $tag;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('editTagAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'editTagAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['editTagAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['editTagAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Tag $tag
     * @return void
     */
    public function updateTagAction(\TYPO3\Media\Domain\Model\Tag $tag)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateTagAction'])) {
            $result = parent::updateTagAction($tag);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['updateTagAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['tag'] = $tag;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('updateTagAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'updateTagAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateTagAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateTagAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Tag $tag
     * @return void
     */
    public function deleteTagAction(\TYPO3\Media\Domain\Model\Tag $tag)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteTagAction'])) {
            $result = parent::deleteTagAction($tag);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteTagAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['tag'] = $tag;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('deleteTagAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'deleteTagAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteTagAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteTagAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param string $title
     * @return void
     * @\TYPO3\Flow\Annotations\Validate(type="NotEmpty", argumentName="title")
     * @\TYPO3\Flow\Annotations\Validate(type="Label", argumentName="title")
     */
    public function createAssetCollectionAction($title)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createAssetCollectionAction'])) {
            $result = parent::createAssetCollectionAction($title);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['createAssetCollectionAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['title'] = $title;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('createAssetCollectionAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'createAssetCollectionAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createAssetCollectionAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createAssetCollectionAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param AssetCollection $assetCollection
     * @return void
     */
    public function editAssetCollectionAction(\TYPO3\Media\Domain\Model\AssetCollection $assetCollection)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['editAssetCollectionAction'])) {
            $result = parent::editAssetCollectionAction($assetCollection);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['editAssetCollectionAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['assetCollection'] = $assetCollection;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('editAssetCollectionAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'editAssetCollectionAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['editAssetCollectionAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['editAssetCollectionAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param AssetCollection $assetCollection
     * @return void
     */
    public function updateAssetCollectionAction(\TYPO3\Media\Domain\Model\AssetCollection $assetCollection)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAssetCollectionAction'])) {
            $result = parent::updateAssetCollectionAction($assetCollection);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAssetCollectionAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['assetCollection'] = $assetCollection;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('updateAssetCollectionAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'updateAssetCollectionAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAssetCollectionAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAssetCollectionAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param AssetCollection $assetCollection
     * @return void
     */
    public function deleteAssetCollectionAction(\TYPO3\Media\Domain\Model\AssetCollection $assetCollection)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAssetCollectionAction'])) {
            $result = parent::deleteAssetCollectionAction($assetCollection);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAssetCollectionAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['assetCollection'] = $assetCollection;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('deleteAssetCollectionAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Controller\AssetController', 'deleteAssetCollectionAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAssetCollectionAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAssetCollectionAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'assetRepository' => 'TYPO3\\Media\\Domain\\Repository\\AssetRepository',
  'tagRepository' => 'TYPO3\\Media\\Domain\\Repository\\TagRepository',
  'assetCollectionRepository' => 'TYPO3\\Media\\Domain\\Repository\\AssetCollectionRepository',
  'packageManager' => 'TYPO3\\Flow\\Package\\PackageManagerInterface',
  'browserState' => 'TYPO3\\Media\\Domain\\Session\\BrowserState',
  'assetService' => 'TYPO3\\Media\\Domain\\Service\\AssetService',
  'translator' => 'TYPO3\\Flow\\I18n\\Translator',
  'viewFormatToObjectNameMap' => 'array',
  'objectManager' => 'TYPO3\\Flow\\Object\\ObjectManagerInterface',
  'reflectionService' => 'TYPO3\\Flow\\Reflection\\ReflectionService',
  'mvcPropertyMappingConfigurationService' => 'TYPO3\\Flow\\Mvc\\Controller\\MvcPropertyMappingConfigurationService',
  'viewConfigurationManager' => 'TYPO3\\Flow\\Mvc\\ViewConfigurationManager',
  'view' => 'TYPO3\\Flow\\Mvc\\View\\ViewInterface',
  'viewObjectNamePattern' => 'string',
  'defaultViewObjectName' => 'string',
  'actionMethodName' => 'string',
  'errorMethodName' => 'string',
  'settings' => 'array',
  'systemLogger' => 'TYPO3\\Flow\\Log\\SystemLoggerInterface',
  'uriBuilder' => 'TYPO3\\Flow\\Mvc\\Routing\\UriBuilder',
  'validatorResolver' => 'TYPO3\\Flow\\Validation\\ValidatorResolver',
  'request' => 'TYPO3\\Flow\\Mvc\\ActionRequest',
  'response' => 'TYPO3\\Flow\\Http\\Response',
  'arguments' => 'TYPO3\\Flow\\Mvc\\Controller\\Arguments',
  'controllerContext' => 'TYPO3\\Flow\\Mvc\\Controller\\ControllerContext',
  'flashMessageContainer' => 'TYPO3\\Flow\\Mvc\\FlashMessageContainer',
  'persistenceManager' => 'TYPO3\\Flow\\Persistence\\PersistenceManagerInterface',
  'supportedMediaTypes' => 'array',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->injectSettings(\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get(\TYPO3\Flow\Configuration\ConfigurationManager::class)->getConfiguration('Settings', 'TYPO3.Media'));
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Media\Domain\Repository\AssetRepository', 'TYPO3\Media\Domain\Repository\AssetRepository', 'assetRepository', 'f32c311dcec701178d68823855159b62', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Media\Domain\Repository\AssetRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Media\Domain\Repository\TagRepository', 'TYPO3\Media\Domain\Repository\TagRepository', 'tagRepository', 'bed183be1c846ade49d5f1b7363f6f89', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Media\Domain\Repository\TagRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Media\Domain\Repository\AssetCollectionRepository', 'TYPO3\Media\Domain\Repository\AssetCollectionRepository', 'assetCollectionRepository', '1dff6f98c6d4278ab8575bd69f3cbee1', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Media\Domain\Repository\AssetCollectionRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Package\PackageManagerInterface', 'TYPO3\Flow\Package\PackageManager', 'packageManager', 'aad0cdb65adb124cf4b4d16c5b42256c', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Package\PackageManagerInterface'); });
        $this->browserState = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Media\Domain\Session\BrowserState');
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Media\Domain\Service\AssetService', 'TYPO3\Media\Domain\Service\AssetService', 'assetService', 'bb639bd7986d8031f2903c52fd36ae6a', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Media\Domain\Service\AssetService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\I18n\Translator', 'TYPO3\Flow\I18n\Translator', 'translator', '2f8185fb502197b0c84bc82ea7cd1c44', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\I18n\Translator'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Object\ObjectManagerInterface', 'TYPO3\Flow\Object\ObjectManager', 'objectManager', '0c3c44be7be16f2a287f1fb2d068dde4', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Object\ObjectManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Reflection\ReflectionService', 'TYPO3\Flow\Reflection\ReflectionService', 'reflectionService', '921ad637f16d2059757a908fceaf7076', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Reflection\ReflectionService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService', 'TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService', 'mvcPropertyMappingConfigurationService', '35acb49fbe78f28099d45aa647797c83', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\ViewConfigurationManager', 'TYPO3\Flow\Mvc\ViewConfigurationManager', 'viewConfigurationManager', '5a345bfd515fdb9f0c97080ff13c7079', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\ViewConfigurationManager'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Log\SystemLoggerInterface', '', 'systemLogger', '6d57d95a1c3cd7528e3e6ea15012dac8', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Log\SystemLoggerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Validation\ValidatorResolver', 'TYPO3\Flow\Validation\ValidatorResolver', 'validatorResolver', 'b457db29305ddeae13b61d92da000ca0', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Validation\ValidatorResolver'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\FlashMessageContainer', 'TYPO3\Flow\Mvc\FlashMessageContainer', 'flashMessageContainer', 'e4fd26f8afd3994317304b563b2a9561', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\FlashMessageContainer'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Persistence\PersistenceManagerInterface', 'TYPO3\Flow\Persistence\Doctrine\PersistenceManager', 'persistenceManager', 'f1bc82ad47156d95485678e33f27c110', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface'); });
        $this->Flow_Injected_Properties = array (
  0 => 'settings',
  1 => 'assetRepository',
  2 => 'tagRepository',
  3 => 'assetCollectionRepository',
  4 => 'packageManager',
  5 => 'browserState',
  6 => 'assetService',
  7 => 'translator',
  8 => 'objectManager',
  9 => 'reflectionService',
  10 => 'mvcPropertyMappingConfigurationService',
  11 => 'viewConfigurationManager',
  12 => 'systemLogger',
  13 => 'validatorResolver',
  14 => 'flashMessageContainer',
  15 => 'persistenceManager',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Media/Classes/TYPO3/Media/Controller/AssetController.php
#