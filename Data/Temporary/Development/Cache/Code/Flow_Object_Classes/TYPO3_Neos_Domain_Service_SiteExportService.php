<?php 
namespace TYPO3\Neos\Domain\Service;

/*
 * This file is part of the TYPO3.Neos package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Package\PackageManagerInterface;
use TYPO3\Flow\Utility\Files;
use TYPO3\Neos\Domain\Model\Site;
use TYPO3\Neos\Domain\Exception as NeosException;
use TYPO3\TYPO3CR\Domain\Service\ContextFactoryInterface;
use TYPO3\TYPO3CR\Domain\Service\ImportExport\NodeExportService;

/**
 * The Site Export Service
 *
 * @Flow\Scope("singleton")
 */
class SiteExportService_Original
{
    /**
     * @Flow\Inject
     * @var ContextFactoryInterface
     */
    protected $contextFactory;

    /**
     * @Flow\Inject
     *
     * @var NodeExportService
     */
    protected $nodeExportService;

    /**
     * @Flow\Inject
     * @var PackageManagerInterface
     */
    protected $packageManager;

    /**
     * Absolute path to exported resources, or NULL if resources should be inlined in the exported XML
     *
     * @var string
     */
    protected $resourcesPath = null;

    /**
     * The XMLWriter that is used to construct the export.
     *
     * @var \XMLWriter
     */
    protected $xmlWriter;

    /**
     * Fetches the site with the given name and exports it into XML.
     *
     * @param array<Site> $sites
     * @param boolean $tidy Whether to export formatted XML
     * @param string $nodeTypeFilter Filter the node type of the nodes, allows complex expressions (e.g. "TYPO3.Neos:Page", "!TYPO3.Neos:Page,TYPO3.Neos:Text")
     * @return string
     */
    public function export(array $sites, $tidy = false, $nodeTypeFilter = null)
    {
        $this->xmlWriter = new \XMLWriter();
        $this->xmlWriter->openMemory();
        $this->xmlWriter->setIndent($tidy);

        $this->exportSites($sites, $nodeTypeFilter);

        return $this->xmlWriter->outputMemory(true);
    }

    /**
     * Fetches the site with the given name and exports it into XML in the given package.
     *
     * @param array<Site> $sites
     * @param boolean $tidy Whether to export formatted XML
     * @param string $packageKey Package key where the export output should be saved to
     * @param string $nodeTypeFilter Filter the node type of the nodes, allows complex expressions (e.g. "TYPO3.Neos:Page", "!TYPO3.Neos:Page,TYPO3.Neos:Text")
     * @return void
     * @throws NeosException
     */
    public function exportToPackage(array $sites, $tidy = false, $packageKey, $nodeTypeFilter = null)
    {
        if (!$this->packageManager->isPackageActive($packageKey)) {
            throw new NeosException(sprintf('Error: Package "%s" is not active.', $packageKey), 1404375719);
        }
        $contentPathAndFilename = sprintf('resource://%s/Private/Content/Sites.xml', $packageKey);

        $this->resourcesPath = Files::concatenatePaths(array(dirname($contentPathAndFilename), 'Resources'));
        Files::createDirectoryRecursively($this->resourcesPath);

        $this->xmlWriter = new \XMLWriter();
        $this->xmlWriter->openUri($contentPathAndFilename);
        $this->xmlWriter->setIndent($tidy);

        $this->exportSites($sites, $nodeTypeFilter);

        $this->xmlWriter->flush();
    }

    /**
     * Fetches the site with the given name and exports it as XML into the given file.
     *
     * @param array<Site> $sites
     * @param boolean $tidy Whether to export formatted XML
     * @param string $pathAndFilename Path to where the export output should be saved to
     * @param string $nodeTypeFilter Filter the node type of the nodes, allows complex expressions (e.g. "TYPO3.Neos:Page", "!TYPO3.Neos:Page,TYPO3.Neos:Text")
     * @return void
     */
    public function exportToFile(array $sites, $tidy = false, $pathAndFilename, $nodeTypeFilter = null)
    {
        $this->resourcesPath = Files::concatenatePaths(array(dirname($pathAndFilename), 'Resources'));
        Files::createDirectoryRecursively($this->resourcesPath);

        $this->xmlWriter = new \XMLWriter();
        $this->xmlWriter->openUri($pathAndFilename);
        $this->xmlWriter->setIndent($tidy);

        $this->exportSites($sites, $nodeTypeFilter);

        $this->xmlWriter->flush();
    }

    /**
     * Exports the given sites to the XMLWriter
     *
     * @param array<Site> $sites
     * @param string $nodeTypeFilter
     * @return void
     */
    protected function exportSites(array $sites, $nodeTypeFilter)
    {
        $this->xmlWriter->startDocument('1.0', 'UTF-8');
        $this->xmlWriter->startElement('root');

        foreach ($sites as $site) {
            $this->exportSite($site, $nodeTypeFilter);
        }

        $this->xmlWriter->endElement();
        $this->xmlWriter->endDocument();
    }

    /**
     * Export the given $site to the XMLWriter
     *
     * @param Site $site
     * @param string $nodeTypeFilter
     * @return void
     */
    protected function exportSite(Site $site, $nodeTypeFilter)
    {
        /** @var ContentContext $contentContext */
        $contentContext = $this->contextFactory->create(array(
            'currentSite' => $site,
            'invisibleContentShown' => true,
            'inaccessibleContentShown' => true
        ));

        $siteNode = $contentContext->getCurrentSiteNode();

        $this->xmlWriter->startElement('site');
        $this->xmlWriter->writeAttribute('name', $site->getName());
        $this->xmlWriter->writeAttribute('state', $site->getState());
        $this->xmlWriter->writeAttribute('siteResourcesPackageKey', $site->getSiteResourcesPackageKey());
        $this->xmlWriter->writeAttribute('siteNodeName', $siteNode->getName());

        $this->nodeExportService->export($siteNode->getPath(), $contentContext->getWorkspaceName(), $this->xmlWriter, false, false, $this->resourcesPath, $nodeTypeFilter);

        $this->xmlWriter->endElement();
    }
}
namespace TYPO3\Neos\Domain\Service;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * The Site Export Service
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class SiteExportService extends SiteExportService_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        if (get_class($this) === 'TYPO3\Neos\Domain\Service\SiteExportService') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Neos\Domain\Service\SiteExportService', $this);
        if ('TYPO3\Neos\Domain\Service\SiteExportService' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'contextFactory' => 'TYPO3\\TYPO3CR\\Domain\\Service\\ContextFactoryInterface',
  'nodeExportService' => 'TYPO3\\TYPO3CR\\Domain\\Service\\ImportExport\\NodeExportService',
  'packageManager' => 'TYPO3\\Flow\\Package\\PackageManagerInterface',
  'resourcesPath' => 'string',
  'xmlWriter' => '\\XMLWriter',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {
        if (get_class($this) === 'TYPO3\Neos\Domain\Service\SiteExportService') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Neos\Domain\Service\SiteExportService', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Service\ContextFactoryInterface', 'TYPO3\Neos\Domain\Service\ContentContextFactory', 'contextFactory', '6b6e9d36a8365cb0dccb3d849ae9366e', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Service\ContextFactoryInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Service\ImportExport\NodeExportService', 'TYPO3\TYPO3CR\Domain\Service\ImportExport\NodeExportService', 'nodeExportService', '2cd24b0250f4f3b6208c80a73e19e681', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Service\ImportExport\NodeExportService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Package\PackageManagerInterface', 'TYPO3\Flow\Package\PackageManager', 'packageManager', 'aad0cdb65adb124cf4b4d16c5b42256c', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Package\PackageManagerInterface'); });
        $this->Flow_Injected_Properties = array (
  0 => 'contextFactory',
  1 => 'nodeExportService',
  2 => 'packageManager',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Neos/Classes/TYPO3/Neos/Domain/Service/SiteExportService.php
#