<?php 
namespace TYPO3\Neos\ViewHelpers;

/*
 * This file is part of the TYPO3.Neos package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;

/**
 * A View Helper to render a fluid template based on the given template path and filename.
 *
 * This will just set up a standalone Fluid view and render the template found at the
 * given path and filename. Any arguments passed will be assigned to that template,
 * the rendering result is returned.
 *
 * = Examples =
 *
 * <code title="Basic usage">
 * <neos:standaloneView templatePathAndFilename="fancyTemplatePathAndFilename" arguments="{foo: bar, quux: baz}" />
 * </code>
 * <output>
 * <some><fancy/></html
 * (depending on template and arguments given)
 * </output>
 *
 * @Flow\Scope("prototype")
 */
class StandaloneViewViewHelper_Original extends \TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * @var boolean
     */
    protected $escapeOutput = false;

    /**
     * @param string $templatePathAndFilename Path and filename of the template to render
     * @param array $arguments Arguments to assign to the template before rendering
     * @return string
     */
    public function render($templatePathAndFilename, $arguments = array())
    {
        $standaloneView = new \TYPO3\Fluid\View\StandaloneView($this->controllerContext->getRequest());
        $standaloneView->setTemplatePathAndFilename($templatePathAndFilename);
        return $standaloneView->assignMultiple($arguments)->render();
    }
}
namespace TYPO3\Neos\ViewHelpers;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * A View Helper to render a fluid template based on the given template path and filename.
 * 
 * This will just set up a standalone Fluid view and render the template found at the
 * given path and filename. Any arguments passed will be assigned to that template,
 * the rendering result is returned.
 * 
 * = Examples =
 * 
 * <code title="Basic usage">
 * <neos:standaloneView templatePathAndFilename="fancyTemplatePathAndFilename" arguments="{foo: bar, quux: baz}" />
 * </code>
 * <output>
 * <some><fancy/></html
 * (depending on template and arguments given)
 * </output>
 * @\TYPO3\Flow\Annotations\Scope("prototype")
 */
class StandaloneViewViewHelper extends StandaloneViewViewHelper_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        if ('TYPO3\Neos\ViewHelpers\StandaloneViewViewHelper' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'escapeOutput' => 'boolean',
  'arguments' => 'array',
  'templateVariableContainer' => '\\TYPO3\\Fluid\\Core\\ViewHelper\\TemplateVariableContainer',
  'controllerContext' => '\\TYPO3\\Flow\\Mvc\\Controller\\ControllerContext',
  'renderingContext' => 'TYPO3\\Fluid\\Core\\Rendering\\RenderingContextInterface',
  'renderChildrenClosure' => '\\Closure',
  'viewHelperVariableContainer' => '\\TYPO3\\Fluid\\Core\\ViewHelper\\ViewHelperVariableContainer',
  'objectManager' => 'TYPO3\\Flow\\Object\\ObjectManagerInterface',
  'systemLogger' => 'TYPO3\\Flow\\Log\\SystemLoggerInterface',
  'escapingInterceptorEnabled' => 'boolean',
  'escapeChildren' => 'boolean',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->injectObjectManager(\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Object\ObjectManagerInterface'));
        $this->injectSystemLogger(\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Log\SystemLoggerInterface'));
        $this->Flow_Injected_Properties = array (
  0 => 'objectManager',
  1 => 'systemLogger',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Neos/Classes/TYPO3/Neos/ViewHelpers/StandaloneViewViewHelper.php
#