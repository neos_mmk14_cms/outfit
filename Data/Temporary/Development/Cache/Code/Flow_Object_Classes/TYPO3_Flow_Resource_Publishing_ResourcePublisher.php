<?php 
namespace TYPO3\Flow\Resource\Publishing;

/*
 * This file is part of the TYPO3.Flow package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Log\SystemLoggerInterface;
use TYPO3\Flow\Resource\Resource as PersistentResource;
use TYPO3\Flow\Resource\ResourceManager;

/**
 * Resource Publisher (deprecated)
 *
 * NOTE: Although this class never belonged to the public API, the method
 *       getPersistentResourceWebUri() has been used in various packages.
 *       In order to keep backwards compatibility, we decided to leave this class
 *       containing the two methods in 3.0.x versions of Flow and mark them as deprecated.
 *
 *       Please make sure to use the new ResourceManager API instead!
 *
 * @Flow\Scope("singleton")
 * @deprecated
 */
class ResourcePublisher_Original
{
    /**
     * @Flow\Inject
     * @var ResourceManager
     */
    protected $resourceManager;

    /**
     * @Flow\Inject
     * @var SystemLoggerInterface
     */
    protected $systemLogger;

    /**
     * Returns the URI pointing to the published persistent resource
     *
     * @param PersistentResource $resource The resource to publish
     * @return mixed Either the web URI of the published resource or FALSE if the resource source file doesn't exist or the resource could not be published for other reasons
     * @deprecated since Flow 3.0. Use ResourceManager->getPublicPersistentResourceUri($resource) instead
     */
    public function getPersistentResourceWebUri(PersistentResource $resource)
    {
        $this->systemLogger->log('The deprecated method ResourcePublisher->getPersistentResourceWebUri() has been called' . $this->getCallee() . '. Please use ResourceManager->getPublicPersistentResourceUri() instead!', LOG_WARNING);
        return $this->resourceManager->getPublicPersistentResourceUri($resource);
    }

    /**
     * Returns the base URI for static resources
     *
     * IMPORTANT: This method merely exists in order to simplify migration from earlier versions of Flow which still
     * provided this method. This method has never been part of the public API and will be removed in the future.
     *
     * Note that, depending on your Resource Collection setup, this method will not always return the correct base URI,
     * because as of now there can be multiple publishing targets for static resources and URIs of the respective
     * target might not work by simply concatenating a base URI with the relative file name.
     *
     * This method will work for the default Flow setup using only the local file system.
     *
     * Make sure to refactor your client code to use the new resource management API instead. There is no direct
     * replacement for this method in the new API, but if you are dealing with static resources, use the resource stream
     * wrapper instead (through URLs like "resource://TYPO3.Flow/Public/Error/Debugger.css") or use
     * ResourceManager->getPublicPackageResourceUri() if you know the package key and relative path.
     *
     * Don't use this method. Ne pas utiliser cette méthode. No utilice este método. Finger weg!
     * U bent gewaarschuwd! You have been warned! Mēs jūs brīdinām! Mir hams euch fei gsagd! ;-)
     *
     * @return mixed Either the web URI of the published resource or FALSE if the resource source file doesn't exist or the resource could not be published for other reasons
     * @deprecated since Flow 3.0. You cannot retrieve a base path for static resources anymore, please use resource://* instead or call ResourceManager->getPublicPackageResourceUri()
     */
    public function getStaticResourcesWebBaseUri()
    {
        $this->systemLogger->log('The deprecated method ResourcePublisher->getStaticResourcesWebBaseUri() has been called' . $this->getCallee() . '. You cannot retrieve a base path for static resources anymore, please use resource://* instead or call ResourceManager->getPublicPackageResourceUri().', LOG_WARNING);
        return preg_replace('/\/Packages\/$/', '/', $this->resourceManager->getCollection(ResourceManager::DEFAULT_STATIC_COLLECTION_NAME)->getTarget()->getPublicStaticResourceUri(''));
    }

    /**
     * @return string
     */
    protected function getCallee()
    {
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        $backtraceStep = $backtrace[2];
        if (isset($backtraceStep['file']) && strpos($backtraceStep['file'], 'DependencyProxy') !== false) {
            $backtraceStep = $backtrace[3];
        }

        if (isset($backtraceStep['file'])) {
            return sprintf(' in file %s, line %s', $backtraceStep['file'], $backtraceStep['line']);
        } else {
            return '';
        }
    }
}
namespace TYPO3\Flow\Resource\Publishing;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * Resource Publisher (deprecated)
 * 
 * NOTE: Although this class never belonged to the public API, the method
 *       getPersistentResourceWebUri() has been used in various packages.
 *       In order to keep backwards compatibility, we decided to leave this class
 *       containing the two methods in 3.0.x versions of Flow and mark them as deprecated.
 * 
 *       Please make sure to use the new ResourceManager API instead!
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class ResourcePublisher extends ResourcePublisher_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        if (get_class($this) === 'TYPO3\Flow\Resource\Publishing\ResourcePublisher') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Flow\Resource\Publishing\ResourcePublisher', $this);
        if ('TYPO3\Flow\Resource\Publishing\ResourcePublisher' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'resourceManager' => 'TYPO3\\Flow\\Resource\\ResourceManager',
  'systemLogger' => 'TYPO3\\Flow\\Log\\SystemLoggerInterface',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {
        if (get_class($this) === 'TYPO3\Flow\Resource\Publishing\ResourcePublisher') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Flow\Resource\Publishing\ResourcePublisher', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Resource\ResourceManager', 'TYPO3\Flow\Resource\ResourceManager', 'resourceManager', '3b3239258e396ed88334e6f7199a1678', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Resource\ResourceManager'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Log\SystemLoggerInterface', '', 'systemLogger', '6d57d95a1c3cd7528e3e6ea15012dac8', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Log\SystemLoggerInterface'); });
        $this->Flow_Injected_Properties = array (
  0 => 'resourceManager',
  1 => 'systemLogger',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Framework/TYPO3.Flow/Classes/TYPO3/Flow/Resource/Publishing/ResourcePublisher.php
#