<?php 
namespace TYPO3\TYPO3CR\Domain\Service;

/*
 * This file is part of the TYPO3.TYPO3CR package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\TYPO3CR\Domain\Model\NodeData;
use TYPO3\TYPO3CR\Domain\Model\NodeInterface;
use TYPO3\TYPO3CR\Domain\Model\NodeType;
use TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository;
use TYPO3\TYPO3CR\Domain\Utility\NodePaths;
use TYPO3\TYPO3CR\Exception\NodeExistsException;
use TYPO3\TYPO3CR\Utility;

/**
 * Provide method to manage node
 *
 * @Flow\Scope("singleton")
 * @api
 */
class NodeService_Original implements NodeServiceInterface
{
    /**
     * @Flow\Inject
     * @var NodeTypeManager
     */
    protected $nodeTypeManager;

    /**
     * @Flow\Inject
     * @var NodeDataRepository
     */
    protected $nodeDataRepository;

    /**
     * @Flow\Inject
     * @var ContextFactory
     */
    protected $contextFactory;

    /**
     * Sets default node property values on the given node.
     *
     * @param NodeInterface $node
     * @return void
     */
    public function setDefaultValues(NodeInterface $node)
    {
        $nodeType = $node->getNodeType();
        foreach ($nodeType->getDefaultValuesForProperties() as $propertyName => $defaultValue) {
            if (trim($node->getProperty($propertyName)) === '') {
                $node->setProperty($propertyName, $defaultValue);
            }
        }
    }

    /**
     * Creates missing child nodes for the given node.
     *
     * @param NodeInterface $node
     * @return void
     */
    public function createChildNodes(NodeInterface $node)
    {
        $nodeType = $node->getNodeType();
        foreach ($nodeType->getAutoCreatedChildNodes() as $childNodeName => $childNodeType) {
            try {
                $node->createNode($childNodeName, $childNodeType);
            } catch (NodeExistsException $exception) {
                // If you have a node that has been marked as removed, but is needed again
                // the old node is recovered
                $childNodePath = NodePaths::addNodePathSegment($node->getPath(), $childNodeName);
                $contextProperties = $node->getContext()->getProperties();
                $contextProperties['removedContentShown'] = true;
                $context = $this->contextFactory->create($contextProperties);
                $childNode = $context->getNode($childNodePath);
                if ($childNode->isRemoved()) {
                    $childNode->setRemoved(false);
                }
            }
        }
    }

    /**
     * Removes all auto created child nodes that existed in the previous nodeType.
     *
     * @param NodeInterface $node
     * @param NodeType $oldNodeType
     * @return void
     */
    public function cleanUpAutoCreatedChildNodes(NodeInterface $node, NodeType $oldNodeType)
    {
        $newNodeType = $node->getNodeType();
        $autoCreatedChildNodesForNewNodeType = $newNodeType->getAutoCreatedChildNodes();
        $autoCreatedChildNodesForOldNodeType = $oldNodeType->getAutoCreatedChildNodes();
        $removedChildNodesFromOldNodeType = array_diff(
            array_keys($autoCreatedChildNodesForOldNodeType),
            array_keys($autoCreatedChildNodesForNewNodeType)
        );
        /** @var NodeInterface $childNode */
        foreach ($node->getChildNodes() as $childNode) {
            if (in_array($childNode->getName(), $removedChildNodesFromOldNodeType)) {
                $childNode->remove();
            }
        }
    }

    /**
     * Remove all properties not configured in the current Node Type.
     * This will not do anything on Nodes marked as removed as those could be queued up for deletion
     * which contradicts updates (that would be necessary to remove the properties).
     *
     * @param NodeInterface $node
     * @return void
     */
    public function cleanUpProperties(NodeInterface $node)
    {
        if ($node->isRemoved() === false) {
            $nodeData = $node->getNodeData();
            $nodeTypeProperties = $node->getNodeType()->getProperties();
            foreach ($node->getProperties() as $name => $value) {
                if (!isset($nodeTypeProperties[$name])) {
                    $nodeData->removeProperty($name);
                }
            }
        }
    }

    /**
     * @param NodeInterface $node
     * @param NodeType $nodeType
     * @return boolean
     */
    public function isNodeOfType(NodeInterface $node, NodeType $nodeType)
    {
        if ($node->getNodeType()->getName() === $nodeType->getName()) {
            return true;
        }
        $subNodeTypes = $this->nodeTypeManager->getSubNodeTypes($nodeType->getName());
        return isset($subNodeTypes[$node->getNodeType()->getName()]);
    }

    /**
     * Checks if the given node path exists in any possible context already.
     *
     * @param string $nodePath
     * @return boolean
     */
    public function nodePathExistsInAnyContext($nodePath)
    {
        return $this->nodeDataRepository->pathExists($nodePath);
    }

    /**
     * Checks if the given node path can be used for the given node.
     *
     * @param string $nodePath
     * @param NodeInterface $node
     * @return boolean
     */
    public function nodePathAvailableForNode($nodePath, NodeInterface $node)
    {
        /** @var NodeData $existingNodeData */
        $existingNodeDataObjects = $this->nodeDataRepository->findByPathWithoutReduce($nodePath, $node->getWorkspace(), true);
        foreach ($existingNodeDataObjects as $existingNodeData) {
            if ($existingNodeData->getMovedTo() !== null && $existingNodeData->getMovedTo() === $node->getNodeData()) {
                return true;
            }
        }
        return !$this->nodePathExistsInAnyContext($nodePath);
    }

    /**
     * Normalizes the given node path to a reference path and returns an absolute path.
     *
     * @param string $path The non-normalized path
     * @param string $referencePath a reference path in case the given path is relative.
     * @return string The normalized absolute path
     * @throws \InvalidArgumentException if your node path contains two consecutive slashes.
     */
    public function normalizePath($path, $referencePath = null)
    {
        return NodePaths::normalizePath($path, $referencePath);
    }

    /**
     * Generate a node name, optionally based on a suggested "ideal" name
     *
     * @param string $parentPath
     * @param string $idealNodeName Can be any string, doesn't need to be a valid node name.
     * @return string
     */
    public function generateUniqueNodeName($parentPath, $idealNodeName = null)
    {
        $possibleNodeName = $this->generatePossibleNodeName($idealNodeName);

        while ($this->nodePathExistsInAnyContext(NodePaths::addNodePathSegment($parentPath, $possibleNodeName))) {
            $possibleNodeName = $this->generatePossibleNodeName();
        }

        return $possibleNodeName;
    }

    /**
     * Generate possible node name. When an idealNodeName is given then this is put into a valid format for a node name,
     * otherwise a random node name in the form "node-alphanumeric" is generated.
     *
     * @param string $idealNodeName
     * @return string
     */
    protected function generatePossibleNodeName($idealNodeName = null)
    {
        if ($idealNodeName !== null) {
            $possibleNodeName = Utility::renderValidNodeName($idealNodeName);
        } else {
            $possibleNodeName = NodePaths::generateRandomNodeName();
        }

        return $possibleNodeName;
    }
}
namespace TYPO3\TYPO3CR\Domain\Service;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * Provide method to manage node
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class NodeService extends NodeService_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        if (get_class($this) === 'TYPO3\TYPO3CR\Domain\Service\NodeService') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\TYPO3CR\Domain\Service\NodeService', $this);
        if ('TYPO3\TYPO3CR\Domain\Service\NodeService' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'nodeTypeManager' => 'TYPO3\\TYPO3CR\\Domain\\Service\\NodeTypeManager',
  'nodeDataRepository' => 'TYPO3\\TYPO3CR\\Domain\\Repository\\NodeDataRepository',
  'contextFactory' => 'TYPO3\\TYPO3CR\\Domain\\Service\\ContextFactory',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {
        if (get_class($this) === 'TYPO3\TYPO3CR\Domain\Service\NodeService') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\TYPO3CR\Domain\Service\NodeService', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Service\NodeTypeManager', 'TYPO3\TYPO3CR\Domain\Service\NodeTypeManager', 'nodeTypeManager', '478a517efacb3d47415a96d9caded2e9', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Service\NodeTypeManager'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository', 'TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository', 'nodeDataRepository', '6d8e58e235099c88f352e23317321129', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Service\ContextFactory', 'TYPO3\TYPO3CR\Domain\Service\ContextFactory', 'contextFactory', '93663bf362ae5b5202fec420ab22f12f', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Service\ContextFactory'); });
        $this->Flow_Injected_Properties = array (
  0 => 'nodeTypeManager',
  1 => 'nodeDataRepository',
  2 => 'contextFactory',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.TYPO3CR/Classes/TYPO3/TYPO3CR/Domain/Service/NodeService.php
#