<?php 
namespace TYPO3\Flow\Configuration;

/*
 * This file is part of the TYPO3.Flow package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use Symfony\Component\Yaml\Yaml;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Error\Notice;
use TYPO3\Flow\Error\Result;
use TYPO3\Flow\Utility\Arrays;
use TYPO3\Flow\Utility\Files;

/**
 * A validator for all configuration entries using Schema
 *
 * Writing Custom Schemata
 * =======================
 *
 * The schemas are searched in the path "Resources/Private/Schema" of all
 * active packages. The schema-filenames must match the pattern
 * [type].[path].schema.yaml. The type and/or the path can also be
 * expressed as subdirectories of Resources/Private/Schema. So
 * Settings/TYPO3/Flow.persistence.schema.yaml will match the same paths
 * like Settings.TYPO3.Flow.persistence.schema.yaml or
 * Settings/TYPO3.Flow/persistence.schema.yaml
 *
 * @Flow\Scope("singleton")
 */
class ConfigurationSchemaValidator_Original
{
    /**
     * @Flow\Inject
     * @var \TYPO3\Flow\Configuration\ConfigurationManager
     */
    protected $configurationManager;

    /**
     * @Flow\Inject
     * @var \TYPO3\Flow\Package\PackageManagerInterface
     */
    protected $packageManager;

    /**
     * @Flow\Inject
     * @var \TYPO3\Flow\Utility\SchemaValidator
     */
    protected $schemaValidator;

    /**
     * Validate the given $configurationType and $path
     *
     * @param string $configurationType (optional) the configuration type to validate. if NULL, validates all configuration.
     * @param string $path (optional) configuration path to validate
     * @param array $loadedSchemaFiles (optional). if given, will be filled with a list of loaded schema files
     * @return \TYPO3\Flow\Error\Result the result of the validation
     * @throws Exception\SchemaValidationException
     */
    public function validate($configurationType = null, $path = null, &$loadedSchemaFiles = [])
    {
        if ($configurationType === null) {
            $configurationTypes = $this->configurationManager->getAvailableConfigurationTypes();
        } else {
            $configurationTypes = [$configurationType];
        }

        $result = new Result();
        foreach ($configurationTypes as $configurationType) {
            $resultForEachType = $this->validateSingleType($configurationType, $path, $loadedSchemaFiles);
            $result->forProperty($configurationType)->merge($resultForEachType);
        }
        return $result;
    }

    /**
     * Validate a single configuration type
     *
     * @param string $configurationType the configuration typr to validate
     * @param string $path configuration path to validate, or NULL.
     * @param array $loadedSchemaFiles will be filled with a list of loaded schema files
     * @return \TYPO3\Flow\Error\Result
     * @throws Exception\SchemaValidationException
     */
    protected function validateSingleType($configurationType, $path, &$loadedSchemaFiles)
    {
        $availableConfigurationTypes = $this->configurationManager->getAvailableConfigurationTypes();
        if (in_array($configurationType, $availableConfigurationTypes) === false) {
            throw new Exception\SchemaValidationException('The configuration type "' . $configurationType . '" was not found. Only the following configuration types are supported: "' . implode('", "', $availableConfigurationTypes) . '"', 1364984886);
        }

        $configuration = $this->configurationManager->getConfiguration($configurationType);

        // find schema files for the given type and path
        $schemaFileInfos = [];
        $activePackages = $this->packageManager->getActivePackages();
        foreach ($activePackages as $package) {
            $packageKey = $package->getPackageKey();
            $packageSchemaPath = Files::concatenatePaths([$package->getResourcesPath(), 'Private/Schema']);
            if (is_dir($packageSchemaPath)) {
                foreach (Files::getRecursiveDirectoryGenerator($packageSchemaPath, '.schema.yaml') as $schemaFile) {
                    $schemaName = substr($schemaFile, strlen($packageSchemaPath) + 1, -strlen('.schema.yaml'));
                    $schemaNameParts = explode('.', str_replace('/', '.', $schemaName), 2);

                    $schemaType = $schemaNameParts[0];
                    $schemaPath = isset($schemaNameParts[1]) ? $schemaNameParts[1] : null;

                    if ($schemaType === $configurationType && ($path === null || strpos($schemaPath, $path) === 0)) {
                        $schemaFileInfos[] = [
                            'file' => $schemaFile,
                            'name' => $schemaName,
                            'path' => $schemaPath,
                            'packageKey' => $packageKey
                        ];
                    }
                }
            }
        }

        if (count($schemaFileInfos) === 0) {
            throw new Exception\SchemaValidationException('No schema files found for configuration type "' . $configurationType . '"' . ($path !== null ? ' and path "' . $path . '".': '.'), 1364985056);
        }

        $result = new Result();
        foreach ($schemaFileInfos as $schemaFileInfo) {
            $loadedSchemaFiles[] = $schemaFileInfo['file'];

            if ($schemaFileInfo['path'] !== null) {
                $data = Arrays::getValueByPath($configuration, $schemaFileInfo['path']);
            } else {
                $data = $configuration;
            }

            if (empty($data)) {
                $result->addNotice(new Notice('No configuration found, skipping schema "%s".', 1364985445, [substr($schemaFileInfo['file'], strlen(FLOW_PATH_ROOT))]));
            } else {
                $parsedSchema = Yaml::parse($schemaFileInfo['file']);
                $validationResultForSingleSchema = $this->schemaValidator->validate($data, $parsedSchema);

                if ($schemaFileInfo['path'] !== null) {
                    $result->forProperty($schemaFileInfo['path'])->merge($validationResultForSingleSchema);
                } else {
                    $result->merge($validationResultForSingleSchema);
                }
            }
        }

        return $result;
    }
}
namespace TYPO3\Flow\Configuration;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * A validator for all configuration entries using Schema
 * 
 * Writing Custom Schemata
 * =======================
 * 
 * The schemas are searched in the path "Resources/Private/Schema" of all
 * active packages. The schema-filenames must match the pattern
 * [type].[path].schema.yaml. The type and/or the path can also be
 * expressed as subdirectories of Resources/Private/Schema. So
 * Settings/TYPO3/Flow.persistence.schema.yaml will match the same paths
 * like Settings.TYPO3.Flow.persistence.schema.yaml or
 * Settings/TYPO3.Flow/persistence.schema.yaml
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class ConfigurationSchemaValidator extends ConfigurationSchemaValidator_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        if (get_class($this) === 'TYPO3\Flow\Configuration\ConfigurationSchemaValidator') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Flow\Configuration\ConfigurationSchemaValidator', $this);
        if ('TYPO3\Flow\Configuration\ConfigurationSchemaValidator' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'configurationManager' => '\\TYPO3\\Flow\\Configuration\\ConfigurationManager',
  'packageManager' => '\\TYPO3\\Flow\\Package\\PackageManagerInterface',
  'schemaValidator' => '\\TYPO3\\Flow\\Utility\\SchemaValidator',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {
        if (get_class($this) === 'TYPO3\Flow\Configuration\ConfigurationSchemaValidator') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Flow\Configuration\ConfigurationSchemaValidator', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Configuration\ConfigurationManager', 'TYPO3\Flow\Configuration\ConfigurationManager', 'configurationManager', '13edcae8fd67699bb78dadc8c1eac29c', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Configuration\ConfigurationManager'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Package\PackageManagerInterface', 'TYPO3\Flow\Package\PackageManager', 'packageManager', 'aad0cdb65adb124cf4b4d16c5b42256c', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Package\PackageManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Utility\SchemaValidator', 'TYPO3\Flow\Utility\SchemaValidator', 'schemaValidator', '1aee6e37460cf41f65ff674c80107cff', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Utility\SchemaValidator'); });
        $this->Flow_Injected_Properties = array (
  0 => 'configurationManager',
  1 => 'packageManager',
  2 => 'schemaValidator',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Framework/TYPO3.Flow/Classes/TYPO3/Flow/Configuration/ConfigurationSchemaValidator.php
#