<?php 
namespace TYPO3\Neos\EventLog\Domain\Repository;

/*
 * This file is part of the TYPO3.Neos package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\Doctrine\Repository;
use TYPO3\Flow\Persistence\QueryInterface;
use TYPO3\Flow\Persistence\QueryResultInterface;
use TYPO3\Flow\Reflection\Exception\PropertyNotAccessibleException;
use TYPO3\Neos\EventLog\Domain\Model\NodeEvent;

/**
 * The repository for events
 *
 * @Flow\Scope("singleton")
 */
class EventRepository_Original extends Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = array(
        'uid' => QueryInterface::ORDER_ASCENDING
    );

    /**
     * Find all events which are "top-level" and in a given workspace (or are not NodeEvents)
     *
     * @param integer $offset
     * @param integer $limit
     * @param string $workspaceName
     * @return QueryResultInterface
     * @throws PropertyNotAccessibleException
     */
    public function findRelevantEventsByWorkspace($offset, $limit, $workspaceName)
    {
        $query = $this->prepareRelevantEventsQuery();
        $query->getQueryBuilder()->select('DISTINCT e');
        $query->getQueryBuilder()
            ->andWhere('e NOT INSTANCE OF ' . NodeEvent::class . ' OR e IN (SELECT nodeevent.uid FROM ' . NodeEvent::class . ' nodeevent WHERE nodeevent.workspaceName = :workspaceName AND nodeevent.parentEvent IS NULL)')
            ->setParameter('workspaceName', $workspaceName);
        $query->getQueryBuilder()->setFirstResult($offset);
        $query->getQueryBuilder()->setMaxResults($limit);

        return $query->execute();
    }

    /**
     * Find all events which are "top-level", i.e. do not have a parent event.
     *
     * @param integer $offset
     * @param integer $limit
     * @return QueryResultInterface
     * @throws PropertyNotAccessibleException
     */
    public function findRelevantEvents($offset, $limit)
    {
        $query = $this->prepareRelevantEventsQuery();

        $query->getQueryBuilder()->setFirstResult($offset);
        $query->getQueryBuilder()->setMaxResults($limit);

        return $query->execute();
    }

    /**
     * @return \TYPO3\Flow\Persistence\Doctrine\Query
     */
    protected function prepareRelevantEventsQuery()
    {
        $query = $this->createQuery();
        $queryBuilder = $query->getQueryBuilder();

        $queryBuilder->andWhere(
            $queryBuilder->expr()->isNull('e.parentEvent')
        );

        $queryBuilder->orderBy('e.uid', 'DESC');

        return $query;
    }

    /**
     * Remove all events without checking foreign keys. Needed for clearing the table during tests.
     *
     * @return void
     */
    public function removeAll()
    {
        $classMetaData = $this->entityManager->getClassMetadata($this->getEntityClassName());
        $connection = $this->entityManager->getConnection();
        $databasePlatform = $connection->getDatabasePlatform();
        $truncateTableQuery = $databasePlatform->getTruncateTableSql($classMetaData->getTableName());
        $connection->executeUpdate($truncateTableQuery);
    }
}
namespace TYPO3\Neos\EventLog\Domain\Repository;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * The repository for events
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class EventRepository extends EventRepository_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     * @param \Doctrine\Common\Persistence\ObjectManager $entityManager The EntityManager to use.
     * @param \Doctrine\Common\Persistence\Mapping\ClassMetadata $classMetadata The class descriptor.
     */
    public function __construct()
    {
        $arguments = func_get_args();
        if (get_class($this) === 'TYPO3\Neos\EventLog\Domain\Repository\EventRepository') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Neos\EventLog\Domain\Repository\EventRepository', $this);

        if (!array_key_exists(0, $arguments)) $arguments[0] = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('Doctrine\Common\Persistence\ObjectManager');
        if (!array_key_exists(0, $arguments)) throw new \TYPO3\Flow\Object\Exception\UnresolvedDependenciesException('Missing required constructor argument $entityManager in class ' . __CLASS__ . '. Please check your calling code and Dependency Injection configuration.', 1296143787);
        call_user_func_array('parent::__construct', $arguments);
        if ('TYPO3\Neos\EventLog\Domain\Repository\EventRepository' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'defaultOrderings' => 'array',
  'persistenceManager' => 'TYPO3\\Flow\\Persistence\\PersistenceManagerInterface',
  'objectType' => 'string',
  '_entityName' => 'string',
  '_em' => 'Doctrine\\ORM\\EntityManager',
  '_class' => '\\Doctrine\\ORM\\Mapping\\ClassMetadata',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {
        if (get_class($this) === 'TYPO3\Neos\EventLog\Domain\Repository\EventRepository') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Neos\EventLog\Domain\Repository\EventRepository', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Persistence\PersistenceManagerInterface', 'TYPO3\Flow\Persistence\Doctrine\PersistenceManager', 'persistenceManager', 'f1bc82ad47156d95485678e33f27c110', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface'); });
        $this->Flow_Injected_Properties = array (
  0 => 'persistenceManager',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Neos/Classes/TYPO3/Neos/EventLog/Domain/Repository/EventRepository.php
#