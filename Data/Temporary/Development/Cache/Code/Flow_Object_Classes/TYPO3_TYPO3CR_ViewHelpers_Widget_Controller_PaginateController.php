<?php 
namespace TYPO3\TYPO3CR\ViewHelpers\Widget\Controller;

/*
 * This file is part of the TYPO3.TYPO3CR package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Utility\Arrays;
use TYPO3\Fluid\Core\Widget\AbstractWidgetController;
use TYPO3\TYPO3CR\Domain\Model\NodeInterface;
use TYPO3\TYPO3CR\Exception\PageNotFoundException;

/**
 * The widget controller for the Node Paginate Widget
 */
class PaginateController_Original extends AbstractWidgetController
{
    /**
     * @var NodeInterface
     */
    protected $parentNode;

    /**
     * @var array<\TYPO3\TYPO3CR\Domain\Model\NodeInterface>
     */
    protected $nodes;

    /**
     * @var string
     */
    protected $nodeTypeFilter = '';

    /**
     * @var array
     */
    protected $configuration = array('itemsPerPage' => 10, 'insertAbove' => false, 'insertBelow' => true, 'maximumNumberOfLinks' => 99, 'maximumNumberOfItems' => 0, 'maximumNumberOfNodes' => 0);

    /**
     * @var integer
     */
    protected $currentPage = 1;

    /**
     * @var integer
     */
    protected $pagesBefore = 0;

    /**
     * @var integer
     */
    protected $pagesAfter = 0;

    /**
     * @var integer
     */
    protected $maximumNumberOfLinks = 99;

    /**
     * @var integer
     */
    protected $maximumNumberOfNodes = 0;

    /**
     * @var integer
     */
    protected $numberOfPages = 1;

    /**
     * @var integer
     */
    protected $displayRangeStart;

    /**
     * @var integer
     */
    protected $displayRangeEnd;

    /**
     * @return void
     */
    protected function initializeAction()
    {
        $this->parentNode = $this->widgetConfiguration['parentNode'];
        $this->nodes = $this->widgetConfiguration['nodes'];
        $this->nodeTypeFilter = $this->widgetConfiguration['nodeTypeFilter'] ?: null;
        $this->configuration = Arrays::arrayMergeRecursiveOverrule($this->configuration, $this->widgetConfiguration['configuration'], true);
        $this->maximumNumberOfNodes = $this->configuration['maximumNumberOfNodes'];

        $numberOfNodes = ($this->parentNode === null ? count($this->nodes) : $this->parentNode->getNumberOfChildNodes($this->nodeTypeFilter));
        if ($this->maximumNumberOfNodes > 0 && $numberOfNodes > $this->maximumNumberOfNodes) {
            $numberOfNodes = $this->maximumNumberOfNodes;
        }

        $this->numberOfPages = ceil($numberOfNodes / (integer)$this->configuration['itemsPerPage']);
        $this->maximumNumberOfLinks = (integer)$this->configuration['maximumNumberOfLinks'];
    }

    /**
     * @param integer $currentPage
     * @return void
     * @throws PageNotFoundException
     */
    public function indexAction($currentPage = 1)
    {
        $this->currentPage = (integer)$currentPage;
        if ($this->currentPage < 1) {
            $this->currentPage = 1;
        } elseif ($this->currentPage > $this->numberOfPages) {
            throw new PageNotFoundException();
        }

        $itemsPerPage = (integer)$this->configuration['itemsPerPage'];
        if ($this->maximumNumberOfNodes > 0 && $this->maximumNumberOfNodes < $itemsPerPage) {
            $itemsPerPage = $this->maximumNumberOfNodes;
        }
        $offset = ($this->currentPage > 1) ? (integer)($itemsPerPage * ($this->currentPage - 1)) : null;
        if ($this->parentNode === null) {
            $nodes = array_slice($this->nodes, $offset, $itemsPerPage);
        } else {
            $nodes = $this->parentNode->getChildNodes($this->nodeTypeFilter, $itemsPerPage, $offset);
        }

        $this->view->assign('contentArguments', array(
            $this->widgetConfiguration['as'] => $nodes
        ));
        $this->view->assign('configuration', $this->configuration);
        $this->view->assign('pagination', $this->buildPagination());
    }

    /**
     * If a certain number of links should be displayed, adjust before and after
     * amounts accordingly.
     *
     * @return void
     */
    protected function calculateDisplayRange()
    {
        $maximumNumberOfLinks = $this->maximumNumberOfLinks;
        if ($maximumNumberOfLinks > $this->numberOfPages) {
            $maximumNumberOfLinks = $this->numberOfPages;
        }
        $delta = floor($maximumNumberOfLinks / 2);
        $this->displayRangeStart = $this->currentPage - $delta;
        $this->displayRangeEnd = $this->currentPage + $delta + ($maximumNumberOfLinks % 2 === 0 ? 1 : 0);
        if ($this->displayRangeStart < 1) {
            $this->displayRangeEnd -= $this->displayRangeStart - 1;
        }
        if ($this->displayRangeEnd > $this->numberOfPages) {
            $this->displayRangeStart -= ($this->displayRangeEnd - $this->numberOfPages);
        }
        $this->displayRangeStart = (integer)max($this->displayRangeStart, 1);
        $this->displayRangeEnd = (integer)min($this->displayRangeEnd, $this->numberOfPages);
    }

    /**
     * Returns an array with the keys "pages", "current", "numberOfPages", "nextPage" & "previousPage"
     *
     * @return array
     */
    protected function buildPagination()
    {
        $this->calculateDisplayRange();
        $pages = array();
        for ($i = $this->displayRangeStart; $i <= $this->displayRangeEnd; $i++) {
            $pages[] = array('number' => $i, 'isCurrent' => ($i === $this->currentPage));
        }
        $pagination = array(
            'pages' => $pages,
            'current' => $this->currentPage,
            'numberOfPages' => $this->numberOfPages,
            'displayRangeStart' => $this->displayRangeStart,
            'displayRangeEnd' => $this->displayRangeEnd,
            'hasLessPages' => $this->displayRangeStart > 2,
            'hasMorePages' => $this->displayRangeEnd + 1 < $this->numberOfPages
        );
        if ($this->currentPage < $this->numberOfPages) {
            $pagination['nextPage'] = $this->currentPage + 1;
        }
        if ($this->currentPage > 1) {
            $pagination['previousPage'] = $this->currentPage - 1;
        }
        return $pagination;
    }
}
namespace TYPO3\TYPO3CR\ViewHelpers\Widget\Controller;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * The widget controller for the Node Paginate Widget
 */
class PaginateController extends PaginateController_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Aop\AdvicesTrait, \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;

    private $Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array();

    private $Flow_Aop_Proxy_groupedAdviceChains = array();

    private $Flow_Aop_Proxy_methodIsInAdviceMode = array();


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
        if ('TYPO3\TYPO3CR\ViewHelpers\Widget\Controller\PaginateController' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    protected function Flow_Aop_Proxy_buildMethodsAndAdvicesArray()
    {
        if (method_exists(get_parent_class(), 'Flow_Aop_Proxy_buildMethodsAndAdvicesArray') && is_callable('parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray')) parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        $objectManager = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager;
        $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array(
            'indexAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
        );
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
            $result = NULL;
        if (method_exists(get_parent_class(), '__wakeup') && is_callable('parent::__wakeup')) parent::__wakeup();
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __clone()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
    }

    /**
     * Autogenerated Proxy Method
     * @param integer $currentPage
     * @return void
     * @throws PageNotFoundException
     */
    public function indexAction($currentPage = 1)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction'])) {
            $result = parent::indexAction($currentPage);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['currentPage'] = $currentPage;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('indexAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\TYPO3CR\ViewHelpers\Widget\Controller\PaginateController', 'indexAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'parentNode' => 'TYPO3\\TYPO3CR\\Domain\\Model\\NodeInterface',
  'nodes' => 'array<\\TYPO3\\TYPO3CR\\Domain\\Model\\NodeInterface>',
  'nodeTypeFilter' => 'string',
  'configuration' => 'array',
  'currentPage' => 'integer',
  'pagesBefore' => 'integer',
  'pagesAfter' => 'integer',
  'maximumNumberOfLinks' => 'integer',
  'maximumNumberOfNodes' => 'integer',
  'numberOfPages' => 'integer',
  'displayRangeStart' => 'integer',
  'displayRangeEnd' => 'integer',
  'widgetConfiguration' => 'array',
  'objectManager' => 'TYPO3\\Flow\\Object\\ObjectManagerInterface',
  'reflectionService' => 'TYPO3\\Flow\\Reflection\\ReflectionService',
  'mvcPropertyMappingConfigurationService' => 'TYPO3\\Flow\\Mvc\\Controller\\MvcPropertyMappingConfigurationService',
  'viewConfigurationManager' => 'TYPO3\\Flow\\Mvc\\ViewConfigurationManager',
  'view' => 'TYPO3\\Flow\\Mvc\\View\\ViewInterface',
  'viewObjectNamePattern' => 'string',
  'viewFormatToObjectNameMap' => 'array',
  'defaultViewObjectName' => 'string',
  'actionMethodName' => 'string',
  'errorMethodName' => 'string',
  'settings' => 'array',
  'systemLogger' => 'TYPO3\\Flow\\Log\\SystemLoggerInterface',
  'uriBuilder' => 'TYPO3\\Flow\\Mvc\\Routing\\UriBuilder',
  'validatorResolver' => 'TYPO3\\Flow\\Validation\\ValidatorResolver',
  'request' => 'TYPO3\\Flow\\Mvc\\ActionRequest',
  'response' => 'TYPO3\\Flow\\Http\\Response',
  'arguments' => 'TYPO3\\Flow\\Mvc\\Controller\\Arguments',
  'controllerContext' => 'TYPO3\\Flow\\Mvc\\Controller\\ControllerContext',
  'flashMessageContainer' => 'TYPO3\\Flow\\Mvc\\FlashMessageContainer',
  'persistenceManager' => 'TYPO3\\Flow\\Persistence\\PersistenceManagerInterface',
  'supportedMediaTypes' => 'array',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->injectSettings(\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get(\TYPO3\Flow\Configuration\ConfigurationManager::class)->getConfiguration('Settings', 'TYPO3.TYPO3CR'));
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Object\ObjectManagerInterface', 'TYPO3\Flow\Object\ObjectManager', 'objectManager', '0c3c44be7be16f2a287f1fb2d068dde4', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Object\ObjectManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Reflection\ReflectionService', 'TYPO3\Flow\Reflection\ReflectionService', 'reflectionService', '921ad637f16d2059757a908fceaf7076', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Reflection\ReflectionService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService', 'TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService', 'mvcPropertyMappingConfigurationService', '35acb49fbe78f28099d45aa647797c83', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\ViewConfigurationManager', 'TYPO3\Flow\Mvc\ViewConfigurationManager', 'viewConfigurationManager', '5a345bfd515fdb9f0c97080ff13c7079', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\ViewConfigurationManager'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Log\SystemLoggerInterface', '', 'systemLogger', '6d57d95a1c3cd7528e3e6ea15012dac8', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Log\SystemLoggerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Validation\ValidatorResolver', 'TYPO3\Flow\Validation\ValidatorResolver', 'validatorResolver', 'b457db29305ddeae13b61d92da000ca0', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Validation\ValidatorResolver'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\FlashMessageContainer', 'TYPO3\Flow\Mvc\FlashMessageContainer', 'flashMessageContainer', 'e4fd26f8afd3994317304b563b2a9561', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\FlashMessageContainer'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Persistence\PersistenceManagerInterface', 'TYPO3\Flow\Persistence\Doctrine\PersistenceManager', 'persistenceManager', 'f1bc82ad47156d95485678e33f27c110', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface'); });
        $this->Flow_Injected_Properties = array (
  0 => 'settings',
  1 => 'objectManager',
  2 => 'reflectionService',
  3 => 'mvcPropertyMappingConfigurationService',
  4 => 'viewConfigurationManager',
  5 => 'systemLogger',
  6 => 'validatorResolver',
  7 => 'flashMessageContainer',
  8 => 'persistenceManager',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.TYPO3CR/Classes/TYPO3/TYPO3CR/ViewHelpers/Widget/Controller/PaginateController.php
#