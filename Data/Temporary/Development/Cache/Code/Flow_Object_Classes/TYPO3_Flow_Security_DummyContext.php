<?php 
namespace TYPO3\Flow\Security;

/*
 * This file is part of the TYPO3.Flow package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Mvc\ActionRequest;
use TYPO3\Flow\Mvc\RequestInterface;
use TYPO3\Flow\Security\Authentication\TokenInterface;
use TYPO3\Flow\Security\Policy\Role;

/**
 * This is dummy implementation of a security context, which holds
 * security information like roles oder details of authenticated users.
 * These information can be set manually on the context as needed.
 *
 * @Flow\Scope("prototype")
 */
class DummyContext_Original extends Context
{
    /**
     * TRUE if the context is initialized in the current request, FALSE or NULL otherwise.
     *
     * @var boolean
     * @Flow\Transient
     */
    protected $initialized = false;

    /**
     * Array of configured tokens (might have request patterns)
     * @var array
     */
    protected $tokens = [];

    /**
     * @var string
     */
    protected $csrfProtectionToken;

    /**
     * @var RequestInterface
     */
    protected $interceptedRequest;

    /**
     * @Flow\Transient
     * @var Role[]
     */
    protected $roles = null;

    /**
     * @param boolean $initialized
     * @return void
     */
    public function setInitialized($initialized)
    {
        $this->initialized = $initialized;
    }

    /**
     * @return boolean TRUE if the Context is initialized, FALSE otherwise.
     */
    public function isInitialized()
    {
        return $this->initialized;
    }

    /**
     * Get the token authentication strategy
     *
     * @return int One of the AUTHENTICATE_* constants
     */
    public function getAuthenticationStrategy()
    {
        return $this->authenticationStrategy;
    }

    /**
     * Sets the Authentication\Tokens of the security context which should be active.
     *
     * @param TokenInterface[] $tokens Array of set tokens
     * @return array
     */
    public function setAuthenticationTokens(array $tokens)
    {
        return $this->tokens = $tokens;
    }

    /**
     * Returns all Authentication\Tokens of the security context which are
     * active for the current request. If a token has a request pattern that cannot match
     * against the current request it is determined as not active.
     *
     * @return TokenInterface[] Array of set tokens
     */
    public function getAuthenticationTokens()
    {
        return $this->tokens;
    }

    /**
     * Returns all Authentication\Tokens of the security context which are
     * active for the current request and of the given type. If a token has a request pattern that cannot match
     * against the current request it is determined as not active.
     *
     * @param string $className The class name
     * @return TokenInterface[] Array of set tokens of the specified type
     */
    public function getAuthenticationTokensOfType($className)
    {
        $tokens = [];
        foreach ($this->tokens as $token) {
            if ($token instanceof $className) {
                $tokens[] = $token;
            }
        }

        return $tokens;
    }

    /**
     * Returns the roles of all authenticated accounts, including inherited roles.
     *
     * If no authenticated roles could be found the "Anonymous" role is returned.
     *
     * The "TYPO3.Flow:Everybody" roles is always returned.
     *
     * @return Role[]
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set an array of role objects.
     *
     * @param Role[] $roles
     * @return void
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    /**
     * Returns TRUE, if at least one of the currently authenticated accounts holds
     * a role with the given identifier, also recursively.
     *
     * @param string $roleIdentifier The string representation of the role to search for
     * @return boolean TRUE, if a role with the given string representation was found
     */
    public function hasRole($roleIdentifier)
    {
        if ($roleIdentifier === 'TYPO3.Flow:Everybody') {
            return true;
        }
        if ($roleIdentifier === 'TYPO3.Flow:Anonymous') {
            return (!empty($this->roles));
        }
        if ($roleIdentifier === 'TYPO3.Flow:AuthenticatedUser') {
            return (empty($this->roles));
        }

        return isset($this->roles[$roleIdentifier]);
    }

    /**
     * @param string $csrfProtectionToken
     * @return void
     */
    public function setCsrfProtectionToken($csrfProtectionToken)
    {
        $this->csrfProtectionToken = $csrfProtectionToken;
    }

    /**
     * Returns the current CSRF protection token. A new one is created when needed, depending on the  configured CSRF
     * protection strategy.
     *
     * @return string
     */
    public function getCsrfProtectionToken()
    {
        return $this->csrfProtectionToken;
    }

    /**
     * Returns TRUE if the context has CSRF protection tokens.
     *
     * @return boolean TRUE, if the token is valid. FALSE otherwise.
     */
    public function hasCsrfProtectionTokens()
    {
        return isset($this->csrfProtectionToken);
    }

    /**
     * Returns TRUE if the given string is a valid CSRF protection token. The token will be removed if the configured
     * csrf strategy is 'onePerUri'.
     *
     * @param string $csrfToken The token string to be validated
     * @return boolean TRUE, if the token is valid. FALSE otherwise.
     */
    public function isCsrfProtectionTokenValid($csrfToken)
    {
        return ($csrfToken === $this->csrfProtectionToken);
    }

    /**
     * Sets an action request, to be stored for later resuming after it
     * has been intercepted by a security exception.
     *
     * @param ActionRequest $interceptedRequest
     * @return void
     * @Flow\Session(autoStart=true)
     */
    public function setInterceptedRequest(ActionRequest $interceptedRequest = null)
    {
        $this->interceptedRequest = $interceptedRequest;
    }

    /**
     * Returns the request, that has been stored for later resuming after it
     * has been intercepted by a security exception, NULL if there is none.
     *
     * @return ActionRequest
     */
    public function getInterceptedRequest()
    {
        return $this->interceptedRequest;
    }

    /**
     * Clears the security context.
     *
     * @return void
     */
    public function clearContext()
    {
        $this->roles = null;
        $this->tokens = [];
        $this->csrfProtectionToken = null;
        $this->interceptedRequest = null;
        $this->initialized = false;
    }
}
namespace TYPO3\Flow\Security;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * This is dummy implementation of a security context, which holds
 * security information like roles oder details of authenticated users.
 * These information can be set manually on the context as needed.
 * @\TYPO3\Flow\Annotations\Scope("prototype")
 */
class DummyContext extends DummyContext_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Aop\AdvicesTrait, \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;

    private $Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array();

    private $Flow_Aop_Proxy_groupedAdviceChains = array();

    private $Flow_Aop_Proxy_methodIsInAdviceMode = array();


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
        if ('TYPO3\Flow\Security\DummyContext' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }

        $isSameClass = get_class($this) === 'TYPO3\Flow\Security\DummyContext';
        if ($isSameClass) {
            \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->registerShutdownObject($this, 'shutdownObject');
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    protected function Flow_Aop_Proxy_buildMethodsAndAdvicesArray()
    {
        if (method_exists(get_parent_class(), 'Flow_Aop_Proxy_buildMethodsAndAdvicesArray') && is_callable('parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray')) parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        $objectManager = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager;
        $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array(
            'setInterceptedRequest' => array(
                'TYPO3\Flow\Aop\Advice\BeforeAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\BeforeAdvice('TYPO3\Flow\Session\Aspect\LazyLoadingAspect', 'initializeSession', $objectManager, NULL),
                ),
            ),
        );
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
            $result = NULL;
        if (method_exists(get_parent_class(), '__wakeup') && is_callable('parent::__wakeup')) parent::__wakeup();

        $isSameClass = get_class($this) === 'TYPO3\Flow\Security\DummyContext';
        $classParents = class_parents($this);
        $classImplements = class_implements($this);
        $isClassProxy = array_search('TYPO3\Flow\Security\DummyContext', $classParents) !== FALSE && array_search('Doctrine\ORM\Proxy\Proxy', $classImplements) !== FALSE;

        if ($isSameClass || $isClassProxy) {
            \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->registerShutdownObject($this, 'shutdownObject');
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __clone()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
    }

    /**
     * Autogenerated Proxy Method
     * @param ActionRequest $interceptedRequest
     * @return void
     * @\TYPO3\Flow\Annotations\Session(autoStart=true)
     */
    public function setInterceptedRequest(\TYPO3\Flow\Mvc\ActionRequest $interceptedRequest = NULL)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['setInterceptedRequest'])) {
            $result = parent::setInterceptedRequest($interceptedRequest);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['setInterceptedRequest'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['interceptedRequest'] = $interceptedRequest;
            
                if (isset($this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['setInterceptedRequest']['TYPO3\Flow\Aop\Advice\BeforeAdvice'])) {
                    $advices = $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['setInterceptedRequest']['TYPO3\Flow\Aop\Advice\BeforeAdvice'];
                    $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Flow\Security\DummyContext', 'setInterceptedRequest', $methodArguments);
                    foreach ($advices as $advice) {
                        $advice->invoke($joinPoint);
                    }

                    $methodArguments = $joinPoint->getMethodArguments();
                }

                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Flow\Security\DummyContext', 'setInterceptedRequest', $methodArguments);
                $result = $this->Flow_Aop_Proxy_invokeJoinPoint($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['setInterceptedRequest']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['setInterceptedRequest']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
  0 => 'initialized',
  1 => 'roles',
  2 => 'activeTokens',
  3 => 'inactiveTokens',
  4 => 'request',
  5 => 'authorizationChecksDisabled',
  6 => 'contextHash',
);
        $propertyVarTags = array (
  'initialized' => 'boolean',
  'tokens' => 'array',
  'csrfProtectionToken' => 'string',
  'interceptedRequest' => 'TYPO3\\Flow\\Mvc\\RequestInterface',
  'roles' => 'Role[]',
  'tokenStatusLabels' => 'array',
  'activeTokens' => 'TokenInterface[]',
  'inactiveTokens' => 'array',
  'authenticationStrategy' => 'integer',
  'request' => 'TYPO3\\Flow\\Mvc\\ActionRequest',
  'authenticationManager' => 'TYPO3\\Flow\\Security\\Authentication\\AuthenticationManagerInterface',
  'sessionManager' => 'TYPO3\\Flow\\Session\\SessionManagerInterface',
  'securityLogger' => 'TYPO3\\Flow\\Log\\SecurityLoggerInterface',
  'policyService' => 'TYPO3\\Flow\\Security\\Policy\\PolicyService',
  'hashService' => 'TYPO3\\Flow\\Security\\Cryptography\\HashService',
  'csrfProtectionStrategy' => 'integer',
  'csrfProtectionTokens' => 'array',
  'authorizationChecksDisabled' => 'boolean',
  'contextHash' => 'string',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->injectAuthenticationManager(\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\Authentication\AuthenticationManagerInterface'));
        $this->injectSettings(\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get(\TYPO3\Flow\Configuration\ConfigurationManager::class)->getConfiguration('Settings', 'TYPO3.Flow'));
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Session\SessionManagerInterface', 'TYPO3\Flow\Session\SessionManager', 'sessionManager', '6d688453b8aad32bc9f6d9d11b89eb10', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Session\SessionManagerInterface'); });
        $this->securityLogger = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Log\SecurityLoggerInterface');
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Security\Policy\PolicyService', 'TYPO3\Flow\Security\Policy\PolicyService', 'policyService', '16231078e783810895dba92e364c25f7', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\Policy\PolicyService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Security\Cryptography\HashService', 'TYPO3\Flow\Security\Cryptography\HashService', 'hashService', 'af606f3838da2ad86bf0ed2ff61be394', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\Cryptography\HashService'); });
        $this->Flow_Injected_Properties = array (
  0 => 'authenticationManager',
  1 => 'settings',
  2 => 'sessionManager',
  3 => 'securityLogger',
  4 => 'policyService',
  5 => 'hashService',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Framework/TYPO3.Flow/Classes/TYPO3/Flow/Security/DummyContext.php
#