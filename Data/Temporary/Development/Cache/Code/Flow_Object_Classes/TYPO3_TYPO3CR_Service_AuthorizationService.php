<?php 
namespace TYPO3\TYPO3CR\Service;

/*
 * This file is part of the TYPO3.TYPO3CR package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Security\Authorization\PrivilegeManagerInterface;
use TYPO3\Flow\Security\Context;
use TYPO3\TYPO3CR\Domain\Model\NodeInterface;
use TYPO3\TYPO3CR\Domain\Model\NodeType;
use TYPO3\TYPO3CR\Domain\Service\NodeTypeManager;
use TYPO3\TYPO3CR\Security\Authorization\Privilege\Node\CreateNodePrivilege;
use TYPO3\TYPO3CR\Security\Authorization\Privilege\Node\CreateNodePrivilegeSubject;
use TYPO3\TYPO3CR\Security\Authorization\Privilege\Node\EditNodePrivilege;
use TYPO3\TYPO3CR\Security\Authorization\Privilege\Node\EditNodePropertyPrivilege;
use TYPO3\TYPO3CR\Security\Authorization\Privilege\Node\NodePrivilegeSubject;
use TYPO3\TYPO3CR\Security\Authorization\Privilege\Node\PropertyAwareNodePrivilegeSubject;
use TYPO3\TYPO3CR\Security\Authorization\Privilege\Node\ReadNodePropertyPrivilege;
use TYPO3\TYPO3CR\Security\Authorization\Privilege\Node\RemoveNodePrivilege;

/**
 * This service provides API methods to check for privileges
 * on nodes and permissions for node actions.
 *
 * @Flow\Scope("singleton")
 */
class AuthorizationService_Original
{
    /**
     * @Flow\Inject
     * @var Context
     */
    protected $securityContext;

    /**
     * @Flow\Inject
     * @var PrivilegeManagerInterface
     */
    protected $privilegeManager;

    /**
     * @Flow\Inject
     * @var NodeTypeManager
     */
    protected $nodeTypeManager;

    /**
     * Returns TRUE if the currently authenticated user is allowed to edit the given $node, otherwise FALSE
     *
     * @param NodeInterface $node
     * @return boolean
     */
    public function isGrantedToEditNode(NodeInterface $node)
    {
        return $this->privilegeManager->isGranted(EditNodePrivilege::class, new NodePrivilegeSubject($node));
    }

    /**
     * Returns TRUE if the currently authenticated user is allowed to create a node of type $typeOfNewNode within the given $referenceNode
     *
     * @param NodeInterface $referenceNode
     * @param NodeType $typeOfNewNode
     * @return boolean
     */
    public function isGrantedToCreateNode(NodeInterface $referenceNode, NodeType $typeOfNewNode = null)
    {
        return $this->privilegeManager->isGranted(CreateNodePrivilege::class, new CreateNodePrivilegeSubject($referenceNode, $typeOfNewNode));
    }

    /**
     * Returns the node types that the currently authenticated user is *denied* to create within the given $referenceNode
     *
     * @param NodeInterface $referenceNode
     * @return string[] Array of granted node type names
     */
    public function getNodeTypeNamesDeniedForCreation(NodeInterface $referenceNode)
    {
        $privilegeSubject = new CreateNodePrivilegeSubject($referenceNode);

        $allNodeTypes = $this->nodeTypeManager->getNodeTypes();

        $deniedCreationNodeTypes = array();
        $grantedCreationNodeTypes = array();
        $abstainedCreationNodeTypes = array();
        foreach ($this->securityContext->getRoles() as $role) {
            /** @var CreateNodePrivilege $createNodePrivilege */
            foreach ($role->getPrivilegesByType(CreateNodePrivilege::class) as $createNodePrivilege) {
                if (!$createNodePrivilege->matchesSubject($privilegeSubject)) {
                    continue;
                }

                $affectedNodeTypes = ($createNodePrivilege->getCreationNodeTypes() !== array() ? $createNodePrivilege->getCreationNodeTypes() : $allNodeTypes);

                if ($createNodePrivilege->isGranted()) {
                    $grantedCreationNodeTypes = array_merge($grantedCreationNodeTypes, $affectedNodeTypes);
                } elseif ($createNodePrivilege->isDenied()) {
                    $deniedCreationNodeTypes = array_merge($deniedCreationNodeTypes, $affectedNodeTypes);
                } else {
                    $abstainedCreationNodeTypes = array_merge($abstainedCreationNodeTypes, $affectedNodeTypes);
                }
            }
        }
        $implicitlyDeniedNodeTypes = array_diff($abstainedCreationNodeTypes, $grantedCreationNodeTypes);
        return array_merge($implicitlyDeniedNodeTypes, $deniedCreationNodeTypes);
    }

    /**
     * Returns TRUE if the currently authenticated user is allowed to remove the given $node
     *
     * @param NodeInterface $node
     * @return boolean
     */
    public function isGrantedToRemoveNode(NodeInterface $node)
    {
        $privilegeSubject = new NodePrivilegeSubject($node);
        return $this->privilegeManager->isGranted(RemoveNodePrivilege::class, $privilegeSubject);
    }

    /**
     * @param NodeInterface $node
     * @param string $propertyName
     * @return boolean
     */
    public function isGrantedToReadNodeProperty(NodeInterface $node, $propertyName)
    {
        $privilegeSubject = new PropertyAwareNodePrivilegeSubject($node, null, $propertyName);
        return $this->privilegeManager->isGranted(ReadNodePropertyPrivilege::class, $privilegeSubject);
    }

    /**
     * @param NodeInterface $node
     * @param string $propertyName
     * @return boolean
     */
    public function isGrantedToEditNodeProperty(NodeInterface $node, $propertyName)
    {
        $privilegeSubject = new PropertyAwareNodePrivilegeSubject($node, null, $propertyName);
        return $this->privilegeManager->isGranted(EditNodePropertyPrivilege::class, $privilegeSubject);
    }

    /**
     * @param NodeInterface $node
     * @return string[] Array of granted node property names
     */
    public function getDeniedNodePropertiesForEditing(NodeInterface $node)
    {
        $privilegeSubject = new PropertyAwareNodePrivilegeSubject($node);

        $deniedNodePropertyNames = array();
        $grantedNodePropertyNames = array();
        $abstainedNodePropertyNames = array();
        foreach ($this->securityContext->getRoles() as $role) {
            /** @var EditNodePropertyPrivilege $editNodePropertyPrivilege */
            foreach ($role->getPrivilegesByType(EditNodePropertyPrivilege::class) as $editNodePropertyPrivilege) {
                if (!$editNodePropertyPrivilege->matchesSubject($privilegeSubject)) {
                    continue;
                }
                if ($editNodePropertyPrivilege->isGranted()) {
                    $grantedNodePropertyNames = array_merge($grantedNodePropertyNames, $editNodePropertyPrivilege->getNodePropertyNames());
                } elseif ($editNodePropertyPrivilege->isDenied()) {
                    $deniedNodePropertyNames = array_merge($deniedNodePropertyNames, $editNodePropertyPrivilege->getNodePropertyNames());
                } else {
                    $abstainedNodePropertyNames = array_merge($abstainedNodePropertyNames, $editNodePropertyPrivilege->getNodePropertyNames());
                }
            }
        }

        $implicitlyDeniedNodePropertyNames = array_diff($abstainedNodePropertyNames, $grantedNodePropertyNames);
        return array_merge($implicitlyDeniedNodePropertyNames, $deniedNodePropertyNames);
    }
}
namespace TYPO3\TYPO3CR\Service;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * This service provides API methods to check for privileges
 * on nodes and permissions for node actions.
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class AuthorizationService extends AuthorizationService_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        if (get_class($this) === 'TYPO3\TYPO3CR\Service\AuthorizationService') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\TYPO3CR\Service\AuthorizationService', $this);
        if ('TYPO3\TYPO3CR\Service\AuthorizationService' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'securityContext' => 'TYPO3\\Flow\\Security\\Context',
  'privilegeManager' => 'TYPO3\\Flow\\Security\\Authorization\\PrivilegeManagerInterface',
  'nodeTypeManager' => 'TYPO3\\TYPO3CR\\Domain\\Service\\NodeTypeManager',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {
        if (get_class($this) === 'TYPO3\TYPO3CR\Service\AuthorizationService') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\TYPO3CR\Service\AuthorizationService', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Security\Context', 'TYPO3\Flow\Security\Context', 'securityContext', '48836470c14129ade5f39e28c4816673', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\Context'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Security\Authorization\PrivilegeManagerInterface', 'TYPO3\Flow\Security\Authorization\PrivilegeManager', 'privilegeManager', 'e6ac4a39049e0c768364696b9ef49ce5', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\Authorization\PrivilegeManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Service\NodeTypeManager', 'TYPO3\TYPO3CR\Domain\Service\NodeTypeManager', 'nodeTypeManager', '478a517efacb3d47415a96d9caded2e9', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Service\NodeTypeManager'); });
        $this->Flow_Injected_Properties = array (
  0 => 'securityContext',
  1 => 'privilegeManager',
  2 => 'nodeTypeManager',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.TYPO3CR/Classes/TYPO3/TYPO3CR/Service/AuthorizationService.php
#