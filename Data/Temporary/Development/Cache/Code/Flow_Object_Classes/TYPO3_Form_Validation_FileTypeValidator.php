<?php 
namespace TYPO3\Form\Validation;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "TYPO3.Form".            *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */


/**
 * The given $value is valid if it is an \TYPO3\Flow\Resource\Resource of the configured resolution
 * Note: a value of NULL or empty string ('') is considered valid
 */
class FileTypeValidator_Original extends \TYPO3\Flow\Validation\Validator\AbstractValidator
{
    /**
     * @var array
     */
    protected $supportedOptions = array(
        'allowedExtensions' => array(array(), 'Array of allowed file extensions', 'array', true)
    );

    /**
     * The given $value is valid if it is an \TYPO3\Flow\Resource\Resource of the configured resolution
     * Note: a value of NULL or empty string ('') is considered valid
     *
     * @param \TYPO3\Flow\Resource\Resource $resource The resource object that should be validated
     * @return void
     * @api
     */
    protected function isValid($resource)
    {
        if (!$resource instanceof \TYPO3\Flow\Resource\Resource) {
            $this->addError('The given value was not a Resource instance.', 1327865587);
            return;
        }
        $fileExtension = $resource->getFileExtension();
        if ($fileExtension === null || $fileExtension === '') {
            $this->addError('The file has no file extension.', 1327865808);
            return;
        }
        if (!in_array($fileExtension, $this->options['allowedExtensions'])) {
            $this->addError('The file extension "%s" is not allowed.', 1327865764, array($resource->getFileExtension()));
            return;
        }
    }
}
namespace TYPO3\Form\Validation;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * The given $value is valid if it is an \TYPO3\Flow\Resource\Resource of the configured resolution
 * Note: a value of NULL or empty string ('') is considered valid
 */
class FileTypeValidator extends FileTypeValidator_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'supportedOptions' => 'array',
  'acceptsEmptyValues' => 'boolean',
  'options' => 'array',
  'result' => 'TYPO3\\Flow\\Error\\Result',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_setRelatedEntities();
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Form/Classes/TYPO3/Form/Validation/FileTypeValidator.php
#