<?php 
namespace TYPO3\Flow\Command;

/*
 * This file is part of the TYPO3.Flow package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Cli\CommandController;
use TYPO3\Flow\Error\Message;
use TYPO3\Flow\Exception;
use TYPO3\Flow\Object\ObjectManagerInterface;
use TYPO3\Flow\Package\PackageManagerInterface;
use TYPO3\Flow\Persistence\PersistenceManagerInterface;
use TYPO3\Flow\Resource\CollectionInterface;
use TYPO3\Flow\Resource\Publishing\MessageCollector;
use TYPO3\Flow\Resource\Resource;
use TYPO3\Flow\Resource\ResourceManager;
use TYPO3\Flow\Resource\ResourceRepository;
use TYPO3\Media\Domain\Repository\AssetRepository;

use TYPO3\Media\Domain\Repository\ThumbnailRepository;

/**
 * Resource command controller for the TYPO3.Flow package
 *
 * @Flow\Scope("singleton")
 */
class ResourceCommandController_Original extends CommandController
{
    /**
     * @Flow\Inject
     * @var ResourceManager
     */
    protected $resourceManager;

    /**
     * @Flow\Inject
     * @var ResourceRepository
     */
    protected $resourceRepository;

    /**
     * @Flow\Inject
     * @var PersistenceManagerInterface
     */
    protected $persistenceManager;

    /**
     * @Flow\Inject
     * @var PackageManagerInterface
     */
    protected $packageManager;

    /**
     * @Flow\Inject
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @Flow\Inject
     * @var MessageCollector
     */
    protected $messageCollector;

    /**
     * Publish resources
     *
     * This command publishes the resources of the given or - if none was specified, all - resource collections
     * to their respective configured publishing targets.
     *
     * @param string $collection If specified, only resources of this collection are published. Example: 'persistent'
     * @return void
     */
    public function publishCommand($collection = null)
    {
        try {
            if ($collection === null) {
                $collections = $this->resourceManager->getCollections();
            } else {
                $collections = [];
                $collections[$collection] = $this->resourceManager->getCollection($collection);
                if ($collections[$collection] === null) {
                    $this->outputLine('Collection "%s" does not exist.', [$collection]);
                    $this->quit(1);
                }
            }

            foreach ($collections as $collection) {
                /** @var CollectionInterface $collection */
                $this->outputLine('Publishing resources of collection "%s"', [$collection->getName()]);
                $target = $collection->getTarget();
                $target->publishCollection($collection, function ($iteration) {
                    $this->clearState($iteration);
                });
            }

            if ($this->messageCollector->hasMessages()) {
                $this->outputLine();
                $this->outputLine('The resources were published, but a few inconsistencies were detected. You can check and probably fix the integrity of the resource registry by using the resource:clean command.');
                $this->messageCollector->flush(function (Message $notification) {
                    $this->outputLine($notification->getSeverity() . ': ' . $notification->getMessage());
                });
            }
        } catch (Exception $exception) {
            $this->outputLine();
            $this->outputLine('An error occurred while publishing resources (see full description below). You can check and probably fix the integrity of the resource registry by using the resource:clean command.');
            $this->outputLine('%s (Exception code: %s)', [get_class($exception), $exception->getCode()]);
            $this->outputLine($exception->getMessage());
            $this->quit(1);
        }
    }

    /**
     * Copy resources
     *
     * This command copies all resources from one collection to another storage identified by name.
     * The target storage must be empty and must not be identical to the current storage of the collection.
     *
     * This command merely copies the binary data from one storage to another, it does not change the related
     * Resource objects in the database in any way. Since the Resource objects in the database refer to a
     * collection name, you can use this command for migrating from one storage to another my configuring
     * the new storage with the name of the old storage collection after the resources have been copied.
     *
     * @param string $sourceCollection The name of the collection you want to copy the assets from
     * @param string $targetCollection The name of the collection you want to copy the assets to
     * @param boolean $publish If enabled, the target collection will be published after the resources have been copied
     * @return void
     */
    public function copyCommand($sourceCollection, $targetCollection, $publish = false)
    {
        $sourceCollectionName = $sourceCollection;
        $sourceCollection = $this->resourceManager->getCollection($sourceCollectionName);
        if ($sourceCollection === null) {
            $this->outputLine('The source collection "%s" does not exist.', array($sourceCollectionName));
            $this->quit(1);
        }

        $targetCollectionName = $targetCollection;
        $targetCollection = $this->resourceManager->getCollection($targetCollection);
        if ($targetCollection === null) {
            $this->outputLine('The target collection "%s" does not exist.', array($targetCollectionName));
            $this->quit(1);
        }

        if (!empty($targetCollection->getObjects())) {
            $this->outputLine('The target collection "%s" is not empty.', array($targetCollectionName));
            $this->quit(1);
        }

        $sourceObjects = $sourceCollection->getObjects();
        $this->outputLine('Copying resource objects from collection "%s" to collection "%s" ...', [$sourceCollectionName, $targetCollectionName]);
        $this->outputLine();

        $this->output->progressStart(count($sourceObjects));
        foreach ($sourceCollection->getObjects() as $resource) {
            /** @var \TYPO3\Flow\Resource\Storage\Object $resource */
            $this->output->progressAdvance();
            $targetCollection->importResource($resource->getStream());
        }
        $this->output->progressFinish();
        $this->outputLine();

        if ($publish) {
            $this->outputLine('Publishing copied resources to the target "%s" ...', [$targetCollection->getTarget()->getName()]);
            $targetCollection->getTarget()->publishCollection($sourceCollection);
        }

        $this->outputLine('Done.');
        $this->outputLine('Hint: If you want to use the target collection as a replacement for your current one, you can now modify your settings accordingly.');
    }

    /**
     * Clean up resource registry
     *
     * This command checks the resource registry (that is the database tables) for orphaned resource objects which don't
     * seem to have any corresponding data anymore (for example: the file in Data/Persistent/Resources has been deleted
     * without removing the related Resource object).
     *
     * If the TYPO3.Media package is active, this command will also detect any assets referring to broken resources
     * and will remove the respective Asset object from the database when the broken resource is removed.
     *
     * This command will ask you interactively what to do before deleting anything.
     *
     * @return void
     */
    public function cleanCommand()
    {
        $this->outputLine('Checking if resource data exists for all known resource objects ...');
        $this->outputLine();

        $mediaPackagePresent = $this->packageManager->isPackageActive('TYPO3.Media');

        $resourcesCount = $this->resourceRepository->countAll();
        $this->output->progressStart($resourcesCount);

        $brokenResources = [];
        $relatedAssets = new \SplObjectStorage();
        $relatedThumbnails = new \SplObjectStorage();
        $iterator = $this->resourceRepository->findAllIterator();
        foreach ($this->resourceRepository->iterate($iterator, function ($iteration) {
            $this->clearState($iteration);
        }) as $resource) {
            $this->output->progressAdvance(1);
            /* @var Resource $resource */
            $stream = $resource->getStream();
            if (!is_resource($stream)) {
                $brokenResources[] = $resource->getSha1();
            }
        }

        $this->output->progressFinish();
        $this->outputLine();

        if ($mediaPackagePresent && count($brokenResources) > 0) {
            /* @var AssetRepository $assetRepository */
            $assetRepository = $this->objectManager->get(AssetRepository::class);
            /* @var ThumbnailRepository $thumbnailRepository */
            $thumbnailRepository = $this->objectManager->get(ThumbnailRepository::class);

            foreach ($brokenResources as $key => $resourceSha1) {
                $resource = $this->resourceRepository->findOneBySha1($resourceSha1);
                $brokenResources[$key] = $resource;
                $assets = $assetRepository->findByResource($resource);
                if ($assets !== null) {
                    $relatedAssets[$resource] = $assets;
                }
                $thumbnails = $thumbnailRepository->findByResource($resource);
                if ($assets !== null) {
                    $relatedThumbnails[$resource] = $thumbnails;
                }
            }
        }

        if (count($brokenResources) > 0) {
            $this->outputLine('<b>Found %s broken resource(s):</b>', [count($brokenResources)]);
            $this->outputLine();

            foreach ($brokenResources as $resource) {
                $this->outputLine('%s (%s) from "%s" collection', [$resource->getFilename(), $resource->getSha1(), $resource->getCollectionName()]);
                if (isset($relatedAssets[$resource])) {
                    foreach ($relatedAssets[$resource] as $asset) {
                        $this->outputLine(' -> %s (%s)', [get_class($asset), $asset->getIdentifier()]);
                    }
                }
            }
            $response = null;
            while (!in_array($response, ['y', 'n', 'c'])) {
                $response = $this->output->ask('<comment>Do you want to remove all broken resource objects and related assets from the database? (y/n/c) </comment>');
            }

            switch ($response) {
                case 'y':
                    $brokenAssetCounter = 0;
                    $brokenThumbnailCounter = 0;
                    foreach ($brokenResources as $sha1 => $resource) {
                        $this->outputLine('- delete %s (%s) from "%s" collection', [
                            $resource->getFilename(),
                            $resource->getSha1(),
                            $resource->getCollectionName()
                        ]);
                        $resource->disableLifecycleEvents();
                        $this->resourceRepository->remove($resource);
                        if (isset($relatedAssets[$resource])) {
                            foreach ($relatedAssets[$resource] as $asset) {
                                $assetRepository->remove($asset);
                                $brokenAssetCounter++;
                            }
                        }
                        if (isset($relatedThumbnails[$resource])) {
                            foreach ($relatedThumbnails[$resource] as $thumbnail) {
                                $thumbnailRepository->remove($thumbnail);
                                $brokenThumbnailCounter++;
                            }
                        }
                        $this->persistenceManager->persistAll();
                    }
                    $brokenResourcesCounter = count($brokenResources);
                    if ($brokenResourcesCounter > 0) {
                        $this->outputLine('Removed %s resource object(s) from the database.', [$brokenResourcesCounter]);
                    }
                    if ($brokenAssetCounter > 0) {
                        $this->outputLine('Removed %s asset object(s) from the database.', [$brokenAssetCounter]);
                    }
                    if ($brokenThumbnailCounter > 0) {
                        $this->outputLine('Removed %s thumbnail object(s) from the database.', [$brokenThumbnailCounter]);
                    }
                    break;
                case 'n':
                    $this->outputLine('Did not delete any resource objects.');
                break;
                case 'c':
                    $this->outputLine('Stopping. Did not delete any resource objects.');
                    $this->quit(0);
                break;
            }
        }
    }

    /**
     * This method is used internal as a callback method to clear doctrine states
     *
     * @param integer $iteration
     * @return void
     */
    protected function clearState($iteration)
    {
        if ($iteration % 1000 === 0) {
            $this->persistenceManager->clearState();
        }
    }
}
namespace TYPO3\Flow\Command;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * Resource command controller for the TYPO3.Flow package
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class ResourceCommandController extends ResourceCommandController_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        if (get_class($this) === 'TYPO3\Flow\Command\ResourceCommandController') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Flow\Command\ResourceCommandController', $this);
        parent::__construct();
        if ('TYPO3\Flow\Command\ResourceCommandController' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'resourceManager' => 'TYPO3\\Flow\\Resource\\ResourceManager',
  'resourceRepository' => 'TYPO3\\Flow\\Resource\\ResourceRepository',
  'persistenceManager' => 'TYPO3\\Flow\\Persistence\\PersistenceManagerInterface',
  'packageManager' => 'TYPO3\\Flow\\Package\\PackageManagerInterface',
  'objectManager' => 'TYPO3\\Flow\\Object\\ObjectManagerInterface',
  'messageCollector' => 'TYPO3\\Flow\\Resource\\Publishing\\MessageCollector',
  'request' => 'TYPO3\\Flow\\Cli\\Request',
  'response' => 'TYPO3\\Flow\\Cli\\Response',
  'arguments' => 'TYPO3\\Flow\\Mvc\\Controller\\Arguments',
  'commandMethodName' => 'string',
  'commandManager' => 'TYPO3\\Flow\\Cli\\CommandManager',
  'output' => 'TYPO3\\Flow\\Cli\\ConsoleOutput',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {
        if (get_class($this) === 'TYPO3\Flow\Command\ResourceCommandController') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Flow\Command\ResourceCommandController', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->injectCommandManager(\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Cli\CommandManager'));
        $this->injectObjectManager(\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Object\ObjectManagerInterface'));
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Resource\ResourceManager', 'TYPO3\Flow\Resource\ResourceManager', 'resourceManager', '3b3239258e396ed88334e6f7199a1678', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Resource\ResourceManager'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Resource\ResourceRepository', 'TYPO3\Flow\Resource\ResourceRepository', 'resourceRepository', 'bb0e0fb67bce65073a482bd8ef9ffa4a', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Resource\ResourceRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Persistence\PersistenceManagerInterface', 'TYPO3\Flow\Persistence\Doctrine\PersistenceManager', 'persistenceManager', 'f1bc82ad47156d95485678e33f27c110', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Package\PackageManagerInterface', 'TYPO3\Flow\Package\PackageManager', 'packageManager', 'aad0cdb65adb124cf4b4d16c5b42256c', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Package\PackageManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Resource\Publishing\MessageCollector', 'TYPO3\Flow\Resource\Publishing\MessageCollector', 'messageCollector', '2dee2941a9d1ead2a7060406b3eec25d', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Resource\Publishing\MessageCollector'); });
        $this->Flow_Injected_Properties = array (
  0 => 'commandManager',
  1 => 'objectManager',
  2 => 'resourceManager',
  3 => 'resourceRepository',
  4 => 'persistenceManager',
  5 => 'packageManager',
  6 => 'messageCollector',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Framework/TYPO3.Flow/Classes/TYPO3/Flow/Command/ResourceCommandController.php
#