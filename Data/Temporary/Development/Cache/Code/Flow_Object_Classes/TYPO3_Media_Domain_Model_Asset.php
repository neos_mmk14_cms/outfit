<?php 
namespace TYPO3\Media\Domain\Model;

/*
 * This file is part of the TYPO3.Media package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Log\SystemLoggerInterface;
use TYPO3\Flow\Object\ObjectManagerInterface;
use TYPO3\Flow\Persistence\PersistenceManagerInterface;
use TYPO3\Flow\Resource\Resource as FlowResource;
use TYPO3\Flow\Resource\ResourceManager;
use TYPO3\Flow\Utility\MediaTypes;
use TYPO3\Media\Domain\Repository\AssetRepository;
use TYPO3\Media\Domain\Service\AssetService;
use TYPO3\Media\Domain\Service\ThumbnailService;

/**
 * An Asset, the base for all more specific assets in this package.
 *
 * It can be used as is to represent any asset for which no better match is available.
 *
 * @Flow\Entity
 * @ORM\InheritanceType("JOINED")
 */
class Asset_Original implements AssetInterface
{
    /**
     * @Flow\Inject
     * @var PersistenceManagerInterface
     */
    protected $persistenceManager;

    /**
     * @Flow\Inject
     * @var SystemLoggerInterface
     */
    protected $systemLogger;

    /**
     * @Flow\Inject
     * @var ResourceManager
     */
    protected $resourceManager;

    /**
     * @Flow\Inject
     * @var ThumbnailService
     */
    protected $thumbnailService;

    /**
     * @Flow\Inject
     * @var AssetService
     */
    protected $assetService;

    /**
     * @Flow\Inject
     * @var AssetRepository
     */
    protected $assetRepository;

    /**
     * @var \DateTime
     */
    protected $lastModified;

    /**
     * @var string
     * @Flow\Validate(type="StringLength", options={ "maximum"=255 })
     */
    protected $title = '';

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $caption = '';

    /**
     * @var FlowResource
     * @ORM\OneToOne(orphanRemoval=true, cascade={"all"})
     */
    protected $resource;

    /**
     * @var Collection<\TYPO3\Media\Domain\Model\Thumbnail>
     * @ORM\OneToMany(orphanRemoval=true, cascade={"all"}, mappedBy="originalAsset")
     */
    protected $thumbnails;

    /**
     * @var Collection<\TYPO3\Media\Domain\Model\Tag>
     * @ORM\ManyToMany
     * @ORM\OrderBy({"label"="ASC"})
     * @Flow\Lazy
     */
    protected $tags;

    /**
     * @var Collection<\TYPO3\Media\Domain\Model\AssetCollection>
     * @ORM\ManyToMany(mappedBy="assets", cascade={"persist"})
     * @ORM\OrderBy({"title"="ASC"})
     * @Flow\Lazy
     */
    protected $assetCollections;

    /**
     * Constructs an asset. The resource is set internally and then initialize()
     * is called.
     *
     * @param FlowResource $resource
     */
    public function __construct(FlowResource $resource)
    {
        $this->tags = new ArrayCollection();
        $this->thumbnails = new ArrayCollection();
        $this->resource = $resource;
        $this->lastModified = new \DateTime();
        $this->assetCollections = new ArrayCollection();
    }

    /**
     * @param integer $initializationCause
     * @return void
     */
    public function initializeObject($initializationCause)
    {
        // FIXME: This is a workaround for after the resource management changes that introduced the property.
        if ($this->thumbnails === null) {
            $this->thumbnails = new ArrayCollection();
        }
        if ($initializationCause === ObjectManagerInterface::INITIALIZATIONCAUSE_CREATED) {
            $this->emitAssetCreated($this);
        }
    }

    /**
     * Override this to initialize upon instantiation.
     *
     * @return void
     */
    protected function initialize()
    {
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->persistenceManager->getIdentifierByObject($this);
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        if (empty($this->title)) {
            return $this->getResource()->getFilename() ?: $this->getIdentifier();
        }
        return $this->getTitle();
    }

    /**
     * Returns the last modification timestamp for this asset
     *
     * @return \DateTime The date and time of last modification.
     * @api
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }

    /**
     * Sets the asset resource and (re-)initializes the asset.
     *
     * @param FlowResource $resource
     * @return void
     */
    public function setResource(FlowResource $resource)
    {
        $this->lastModified = new \DateTime();
        $this->resource = $resource;
        $this->refresh();
    }

    /**
     * Resource of the original file
     *
     * @return FlowResource
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * Returns a file extension fitting to the media type of this asset
     *
     * @return string
     */
    public function getFileExtension()
    {
        return MediaTypes::getFilenameExtensionFromMediaType($this->resource->getMediaType());
    }

    /**
     * Returns the IANA media type of this asset
     *
     * @return string
     */
    public function getMediaType()
    {
        return $this->resource->getMediaType();
    }

    /**
     * Sets the title of this image (optional)
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->lastModified = new \DateTime();
        $this->title = $title;
    }

    /**
     * The title of this image
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the caption of this asset (optional)
     *
     * @param string $caption
     * @return void
     */
    public function setCaption($caption)
    {
        $this->lastModified = new \DateTime();
        $this->caption = $caption;
    }

    /**
     * The caption of this asset
     *
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * Return the tags assigned to this asset
     *
     * @return Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Add a single tag to this asset
     *
     * @param Tag $tag The tag to add
     * @return boolean TRUE if the tag added was new, FALSE if it already existed
     */
    public function addTag(Tag $tag)
    {
        if (!$this->tags->contains($tag)) {
            $this->lastModified = new \DateTime();
            $this->tags->add($tag);
            return true;
        }

        return false;
    }

    /**
     * Returns a thumbnail of this asset
     *
     * If the maximum width / height is not specified or exceeds the original asset's dimensions, the width / height of
     * the original asset is used.
     *
     * @param integer $maximumWidth The thumbnail's maximum width in pixels
     * @param integer $maximumHeight The thumbnail's maximum height in pixels
     * @param string $ratioMode Whether the resulting image should be cropped if both edge's sizes are supplied that would hurt the aspect ratio
     * @param boolean $allowUpScaling Whether the resulting image should be upscaled
     * @return Thumbnail
     * @api
     */
    public function getThumbnail($maximumWidth = null, $maximumHeight = null, $ratioMode = ImageInterface::RATIOMODE_INSET, $allowUpScaling = null)
    {
        $thumbnailConfiguration = new ThumbnailConfiguration(null, $maximumWidth, null, $maximumHeight, $ratioMode === ImageInterface::RATIOMODE_OUTBOUND, $allowUpScaling);
        return $this->thumbnailService->getThumbnail($this, $thumbnailConfiguration);
    }

    /**
     * An internal method which adds a thumbnail which was generated by the ThumbnailService.
     *
     * @param Thumbnail $thumbnail
     * @return mixed
     * @see getThumbnail()
     */
    public function addThumbnail(Thumbnail $thumbnail)
    {
        $this->thumbnails->add($thumbnail);
    }

    /**
     * Refreshes this asset after the Resource or any other parameters affecting thumbnails have been modified
     *
     * @return void
     */
    public function refresh()
    {
        $assetClassType = str_replace('TYPO3\Media\Domain\Model\\', '', get_class($this));
        $this->systemLogger->log(sprintf('%s: refresh() called, clearing all thumbnails. Filename: %s. Resource SHA1: %s', $assetClassType, $this->getResource()->getFilename(), $this->getResource()->getSha1()), LOG_DEBUG);

        // whitelist objects so they can be deleted (even during safe requests)
        $this->persistenceManager->whitelistObject($this);
        foreach ($this->thumbnails as $thumbnail) {
            $this->persistenceManager->whitelistObject($thumbnail);
        }

        $this->thumbnails->clear();
    }

    /**
     * Set the tags assigned to this asset
     *
     * @param Collection $tags
     * @return void
     */
    public function setTags(Collection $tags)
    {
        $this->lastModified = new \DateTime();
        $this->tags = $tags;
    }

    /**
     * Remove a single tag from this asset
     *
     * @param Tag $tag
     * @return boolean
     */
    public function removeTag(Tag $tag)
    {
        if ($this->tags->contains($tag)) {
            $this->lastModified = new \DateTime();
            $this->tags->removeElement($tag);

            return true;
        }

        return false;
    }

    /**
     * Return the asset collections this asset is included in
     *
     * @return Collection
     */
    public function getAssetCollections()
    {
        return $this->assetCollections;
    }

    /**
     * Set the asset collections that include this asset
     *
     * @param Collection $assetCollections
     * @return void
     */
    public function setAssetCollections(Collection $assetCollections)
    {
        $this->lastModified = new \DateTime();
        foreach ($this->assetCollections as $existingAssetCollection) {
            $existingAssetCollection->removeAsset($this);
        }
        foreach ($assetCollections as $newAssetCollection) {
            $newAssetCollection->addAsset($this);
        }
        foreach ($this->assetCollections as $assetCollection) {
            if (!$assetCollections->contains($assetCollection)) {
                $assetCollections->add($assetCollection);
            }
        }
        $this->assetCollections = $assetCollections;
    }

    /**
     * Signals that an asset was created.
     * @deprecated Will be removed with next major version of TYPO3.Media.
     * Use AssetService::emitAssetCreated signal instead.
     *
     * @Flow\Signal
     * @param AssetInterface $asset
     * @return void
     */
    protected function emitAssetCreated(AssetInterface $asset)
    {
    }

    /**
     * Returns true if the asset is still in use.
     *
     * @return boolean
     * @api
     */
    public function isInUse()
    {
        return $this->assetService->isInUse($this);
    }

    /**
     * Returns the number of times the asset is in use.
     *
     * @return integer
     * @api
     */
    public function getUsageCount()
    {
        return $this->assetService->getUsageCount($this);
    }
}
namespace TYPO3\Media\Domain\Model;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * An Asset, the base for all more specific assets in this package.
 * 
 * It can be used as is to represent any asset for which no better match is available.
 * @\TYPO3\Flow\Annotations\Entity
 * @\Doctrine\ORM\Mapping\InheritanceType("JOINED")
 */
class Asset extends Asset_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface, \TYPO3\Flow\Persistence\Aspect\PersistenceMagicInterface {

    use \TYPO3\Flow\Aop\AdvicesTrait, \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(length=40)
     * introduced by TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect
     */
    protected $Persistence_Object_Identifier = NULL;

    private $Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array();

    private $Flow_Aop_Proxy_groupedAdviceChains = array();

    private $Flow_Aop_Proxy_methodIsInAdviceMode = array();


    /**
     * Autogenerated Proxy Method
     * @param FlowResource $resource
     */
    public function __construct()
    {
        $arguments = func_get_args();

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['__construct'])) {

        if (!array_key_exists(0, $arguments)) $arguments[0] = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Resource\Resource');
        if (!array_key_exists(0, $arguments)) throw new \TYPO3\Flow\Object\Exception\UnresolvedDependenciesException('Missing required constructor argument $resource in class ' . __CLASS__ . '. Note that constructor injection is only support for objects of scope singleton (and this is not a singleton) – for other scopes you must pass each required argument to the constructor yourself.', 1296143788);
        call_user_func_array('parent::__construct', $arguments);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['__construct'] = TRUE;
            try {
            
                $methodArguments = [];

                if (array_key_exists(0, $arguments)) $methodArguments['resource'] = $arguments[0];
            
                if (isset($this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['__construct']['TYPO3\Flow\Aop\Advice\BeforeAdvice'])) {
                    $advices = $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['__construct']['TYPO3\Flow\Aop\Advice\BeforeAdvice'];
                    $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Domain\Model\Asset', '__construct', $methodArguments);
                    foreach ($advices as $advice) {
                        $advice->invoke($joinPoint);
                    }

                    $methodArguments = $joinPoint->getMethodArguments();
                }

                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Domain\Model\Asset', '__construct', $methodArguments);
                $result = $this->Flow_Aop_Proxy_invokeJoinPoint($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['__construct']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['__construct']);
            return;
        }
        if ('TYPO3\Media\Domain\Model\Asset' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }

        $isSameClass = get_class($this) === 'TYPO3\Media\Domain\Model\Asset';
        if ($isSameClass) {
            $this->initializeObject(1);
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    protected function Flow_Aop_Proxy_buildMethodsAndAdvicesArray()
    {
        if (method_exists(get_parent_class(), 'Flow_Aop_Proxy_buildMethodsAndAdvicesArray') && is_callable('parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray')) parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        $objectManager = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager;
        $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array(
            '__clone' => array(
                'TYPO3\Flow\Aop\Advice\BeforeAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\BeforeAdvice('TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect', 'generateUuid', $objectManager, NULL),
                ),
                'TYPO3\Flow\Aop\Advice\AfterReturningAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AfterReturningAdvice('TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect', 'cloneObject', $objectManager, NULL),
                ),
            ),
            '__construct' => array(
                'TYPO3\Flow\Aop\Advice\BeforeAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\BeforeAdvice('TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect', 'generateUuid', $objectManager, NULL),
                ),
            ),
            'emitAssetCreated' => array(
                'TYPO3\Flow\Aop\Advice\AfterReturningAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AfterReturningAdvice('TYPO3\Flow\SignalSlot\SignalAspect', 'forwardSignalToDispatcher', $objectManager, NULL),
                ),
            ),
        );
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
            $result = NULL;
        if (method_exists(get_parent_class(), '__wakeup') && is_callable('parent::__wakeup')) parent::__wakeup();

        $isSameClass = get_class($this) === 'TYPO3\Media\Domain\Model\Asset';
        $classParents = class_parents($this);
        $classImplements = class_implements($this);
        $isClassProxy = array_search('TYPO3\Media\Domain\Model\Asset', $classParents) !== FALSE && array_search('Doctrine\ORM\Proxy\Proxy', $classImplements) !== FALSE;

        if ($isSameClass || $isClassProxy) {
            $this->initializeObject(2);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __clone()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['__clone'])) {
            $result = NULL;

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['__clone'] = TRUE;
            try {
            
                $methodArguments = [];

                if (isset($this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['__clone']['TYPO3\Flow\Aop\Advice\BeforeAdvice'])) {
                    $advices = $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['__clone']['TYPO3\Flow\Aop\Advice\BeforeAdvice'];
                    $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Domain\Model\Asset', '__clone', $methodArguments);
                    foreach ($advices as $advice) {
                        $advice->invoke($joinPoint);
                    }

                    $methodArguments = $joinPoint->getMethodArguments();
                }

                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Domain\Model\Asset', '__clone', $methodArguments);
                $result = $this->Flow_Aop_Proxy_invokeJoinPoint($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

                if (isset($this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['__clone']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'])) {
                    $advices = $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['__clone']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'];
                    $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Domain\Model\Asset', '__clone', $methodArguments, NULL, $result);
                    foreach ($advices as $advice) {
                        $advice->invoke($joinPoint);
                    }

                    $methodArguments = $joinPoint->getMethodArguments();
                }

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['__clone']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['__clone']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param AssetInterface $asset
     * @return void
     * @\TYPO3\Flow\Annotations\Signal
     */
    protected function emitAssetCreated(\TYPO3\Media\Domain\Model\AssetInterface $asset)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitAssetCreated'])) {
            $result = parent::emitAssetCreated($asset);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['emitAssetCreated'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['asset'] = $asset;
            
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Domain\Model\Asset', 'emitAssetCreated', $methodArguments);
                $result = $this->Flow_Aop_Proxy_invokeJoinPoint($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

                if (isset($this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['emitAssetCreated']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'])) {
                    $advices = $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['emitAssetCreated']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'];
                    $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Media\Domain\Model\Asset', 'emitAssetCreated', $methodArguments, NULL, $result);
                    foreach ($advices as $advice) {
                        $advice->invoke($joinPoint);
                    }

                    $methodArguments = $joinPoint->getMethodArguments();
                }

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitAssetCreated']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitAssetCreated']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'persistenceManager' => 'TYPO3\\Flow\\Persistence\\PersistenceManagerInterface',
  'systemLogger' => 'TYPO3\\Flow\\Log\\SystemLoggerInterface',
  'resourceManager' => 'TYPO3\\Flow\\Resource\\ResourceManager',
  'thumbnailService' => 'TYPO3\\Media\\Domain\\Service\\ThumbnailService',
  'assetService' => 'TYPO3\\Media\\Domain\\Service\\AssetService',
  'assetRepository' => 'TYPO3\\Media\\Domain\\Repository\\AssetRepository',
  'lastModified' => '\\DateTime',
  'title' => 'string',
  'caption' => 'string',
  'resource' => 'TYPO3\\Flow\\Resource\\Resource',
  'thumbnails' => 'Doctrine\\Common\\Collections\\Collection<\\TYPO3\\Media\\Domain\\Model\\Thumbnail>',
  'tags' => 'Doctrine\\Common\\Collections\\Collection<\\TYPO3\\Media\\Domain\\Model\\Tag>',
  'assetCollections' => 'Doctrine\\Common\\Collections\\Collection<\\TYPO3\\Media\\Domain\\Model\\AssetCollection>',
  'Persistence_Object_Identifier' => 'string',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Persistence\PersistenceManagerInterface', 'TYPO3\Flow\Persistence\Doctrine\PersistenceManager', 'persistenceManager', 'f1bc82ad47156d95485678e33f27c110', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Log\SystemLoggerInterface', '', 'systemLogger', '6d57d95a1c3cd7528e3e6ea15012dac8', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Log\SystemLoggerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Resource\ResourceManager', 'TYPO3\Flow\Resource\ResourceManager', 'resourceManager', '3b3239258e396ed88334e6f7199a1678', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Resource\ResourceManager'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Media\Domain\Service\ThumbnailService', 'TYPO3\Media\Domain\Service\ThumbnailService', 'thumbnailService', 'e0f38102928d4218f03b26fd6d61aeca', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Media\Domain\Service\ThumbnailService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Media\Domain\Service\AssetService', 'TYPO3\Media\Domain\Service\AssetService', 'assetService', 'bb639bd7986d8031f2903c52fd36ae6a', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Media\Domain\Service\AssetService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Media\Domain\Repository\AssetRepository', 'TYPO3\Media\Domain\Repository\AssetRepository', 'assetRepository', 'f32c311dcec701178d68823855159b62', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Media\Domain\Repository\AssetRepository'); });
        $this->Flow_Injected_Properties = array (
  0 => 'persistenceManager',
  1 => 'systemLogger',
  2 => 'resourceManager',
  3 => 'thumbnailService',
  4 => 'assetService',
  5 => 'assetRepository',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Media/Classes/TYPO3/Media/Domain/Model/Asset.php
#