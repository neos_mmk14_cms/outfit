<?php 
namespace TYPO3\TYPO3CR\Migration\Command;

/*
 * This file is part of the TYPO3.TYPO3CR package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Cli\CommandController;
use TYPO3\Flow\Configuration\Source\YamlSource;
use TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository;
use TYPO3\TYPO3CR\Domain\Service\ContextFactoryInterface;
use TYPO3\TYPO3CR\Migration\Domain\Factory\MigrationFactory;
use TYPO3\TYPO3CR\Migration\Domain\Repository\MigrationStatusRepository;
use TYPO3\TYPO3CR\Migration\Exception\MigrationException;
use TYPO3\Flow\Persistence\Doctrine\Exception\DatabaseException;
use TYPO3\TYPO3CR\Migration\Service\NodeMigration;
use TYPO3\TYPO3CR\Migration\Domain\Model\MigrationStatus;
use TYPO3\TYPO3CR\Migration\Domain\Model\MigrationConfiguration;
use TYPO3\Flow\Annotations as Flow;

/**
 * Command controller for tasks related to node handling.
 *
 * @Flow\Scope("singleton")
 */
class NodeCommandController_Original extends CommandController
{
    /**
     * @Flow\Inject
     * @var YamlSource
     */
    protected $yamlSourceImporter;

    /**
     * @Flow\Inject
     * @var NodeDataRepository
     */
    protected $nodeDataRepository;

    /**
     * @Flow\Inject
     * @var MigrationStatusRepository
     */
    protected $migrationStatusRepository;

    /**
     * @Flow\Inject
     * @var MigrationFactory
     */
    protected $migrationFactory;

    /**
     * @Flow\Inject
     * @var ContextFactoryInterface
     */
    protected $contextFactory;

    /**
     * Do the configured migrations in the given migration.
     *
     * By default the up direction is applied, using the direction parameter this can
     * be changed.
     *
     * @param string $version The version of the migration configuration you want to use.
     * @param boolean $confirmation Confirm application of this migration, only needed if the given migration contains any warnings.
     * @param string $direction The direction to work in, MigrationStatus::DIRECTION_UP or MigrationStatus::DIRECTION_DOWN
     * @return void
     */
    public function migrateCommand($version, $confirmation = false, $direction = MigrationStatus::DIRECTION_UP)
    {
        try {
            $migrationConfiguration = $direction === MigrationStatus::DIRECTION_UP ?
                $this->migrationFactory->getMigrationForVersion($version)->getUpConfiguration() :
                $this->migrationFactory->getMigrationForVersion($version)->getDownConfiguration();

            $this->outputCommentsAndWarnings($migrationConfiguration);
            if ($migrationConfiguration->hasWarnings() && $confirmation === false) {
                $this->outputLine();
                $this->outputLine('Migration has warnings. You need to confirm execution by adding the "--confirmation TRUE" option to the command.');
                $this->quit(1);
            }

            $nodeMigrationService = new NodeMigration($migrationConfiguration->getMigration());
            $nodeMigrationService->execute();
            $migrationStatus = new MigrationStatus($version, $direction, new \DateTime());
            $this->migrationStatusRepository->add($migrationStatus);
            $this->outputLine();
            $this->outputLine('Successfully applied migration.');
        } catch (MigrationException $e) {
            $this->outputLine();
            $this->outputLine('Error: ' . $e->getMessage());
            $this->quit(1);
        } catch (DatabaseException $exception) {
            $this->outputLine();
            $this->outputLine('An exception occurred during the migration, run a ./flow doctrine:migrate and run the migration again.');
            $this->quit(1);
        }
    }

    /**
     * List available and applied migrations
     *
     * @return void
     * @see typo3.typo3cr.migration:node:listavailablemigrations
     */
    public function migrationStatusCommand()
    {
        $this->outputLine();

        $availableMigrations = $this->migrationFactory->getAvailableMigrationsForCurrentConfigurationType();
        if (count($availableMigrations) === 0) {
            $this->outputLine('No migrations available.');
            $this->quit();
        }

        $appliedMigrations = $this->migrationStatusRepository->findAll();
        $appliedMigrationsDictionary = array();
        /** @var $appliedMigration MigrationStatus */
        foreach ($appliedMigrations as $appliedMigration) {
            $appliedMigrationsDictionary[$appliedMigration->getVersion()][] = $appliedMigration;
        }

        $tableRows = array();
        foreach ($availableMigrations as $version => $migration) {
            $migrationUpConfigurationComments = $this->migrationFactory->getMigrationForVersion($version)->getUpConfiguration()->getComments();

            if (isset($appliedMigrationsDictionary[$version])) {
                $applicationInformation = $this->phraseMigrationApplicationInformation($appliedMigrationsDictionary[$version]);
                if ($applicationInformation !== '') {
                    $migrationUpConfigurationComments .= PHP_EOL . '<b>Applied:</b>' . PHP_EOL . $applicationInformation;
                }
            }

            $tableRows[] = array(
                $version,
                $migration['formattedVersionNumber'],
                $migration['package']->getPackageKey(),
                wordwrap($migrationUpConfigurationComments, 60)
            );
        }

        $this->outputLine('<b>Available migrations</b>');
        $this->outputLine();
        $this->output->outputTable($tableRows, array('Version', 'Date', 'Package', 'Comments'));
    }

    /**
     * Helper to output comments and warnings for the given configuration.
     *
     * @param MigrationConfiguration $migrationConfiguration
     * @return void
     */
    protected function outputCommentsAndWarnings(MigrationConfiguration $migrationConfiguration)
    {
        if ($migrationConfiguration->hasComments()) {
            $this->outputLine();
            $this->outputLine('<b>Comments</b>');
            $this->outputFormatted($migrationConfiguration->getComments(), array(), 2);
        }

        if ($migrationConfiguration->hasWarnings()) {
            $this->outputLine();
            $this->outputLine('<b><u>Warnings</u></b>');
            $this->outputFormatted($migrationConfiguration->getWarnings(), array(), 2);
        }
    }

    /**
     * @param array $migrationsInVersion
     * @return string
     */
    protected function phraseMigrationApplicationInformation($migrationsInVersion)
    {
        usort($migrationsInVersion, function (MigrationStatus $migrationA, MigrationStatus $migrationB) {
            return $migrationA->getApplicationTimeStamp() > $migrationB->getApplicationTimeStamp();
        });

        $applied = array();
        /** @var MigrationStatus $migrationStatus */
        foreach ($migrationsInVersion as $migrationStatus) {
            $applied[] = sprintf(
                '%s applied on %s',
                str_pad(strtoupper($migrationStatus->getDirection()), 5, ' ', STR_PAD_LEFT),
                $migrationStatus->getApplicationTimeStamp()->format('Y-m-d H:i:s')
            );
        }
        return implode(PHP_EOL, $applied);
    }
}
namespace TYPO3\TYPO3CR\Migration\Command;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * Command controller for tasks related to node handling.
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class NodeCommandController extends NodeCommandController_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        if (get_class($this) === 'TYPO3\TYPO3CR\Migration\Command\NodeCommandController') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\TYPO3CR\Migration\Command\NodeCommandController', $this);
        parent::__construct();
        if ('TYPO3\TYPO3CR\Migration\Command\NodeCommandController' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'yamlSourceImporter' => 'TYPO3\\Flow\\Configuration\\Source\\YamlSource',
  'nodeDataRepository' => 'TYPO3\\TYPO3CR\\Domain\\Repository\\NodeDataRepository',
  'migrationStatusRepository' => 'TYPO3\\TYPO3CR\\Migration\\Domain\\Repository\\MigrationStatusRepository',
  'migrationFactory' => 'TYPO3\\TYPO3CR\\Migration\\Domain\\Factory\\MigrationFactory',
  'contextFactory' => 'TYPO3\\TYPO3CR\\Domain\\Service\\ContextFactoryInterface',
  'request' => 'TYPO3\\Flow\\Cli\\Request',
  'response' => 'TYPO3\\Flow\\Cli\\Response',
  'arguments' => 'TYPO3\\Flow\\Mvc\\Controller\\Arguments',
  'commandMethodName' => 'string',
  'objectManager' => 'TYPO3\\Flow\\Object\\ObjectManagerInterface',
  'commandManager' => 'TYPO3\\Flow\\Cli\\CommandManager',
  'output' => 'TYPO3\\Flow\\Cli\\ConsoleOutput',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {
        if (get_class($this) === 'TYPO3\TYPO3CR\Migration\Command\NodeCommandController') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\TYPO3CR\Migration\Command\NodeCommandController', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->injectCommandManager(\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Cli\CommandManager'));
        $this->injectObjectManager(\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Object\ObjectManagerInterface'));
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Configuration\Source\YamlSource', 'TYPO3\Flow\Configuration\Source\YamlSource', 'yamlSourceImporter', '3ff75d2363593363cb0d0607df40c19a', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Configuration\Source\YamlSource'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository', 'TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository', 'nodeDataRepository', '6d8e58e235099c88f352e23317321129', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Migration\Domain\Repository\MigrationStatusRepository', 'TYPO3\TYPO3CR\Migration\Domain\Repository\MigrationStatusRepository', 'migrationStatusRepository', 'c4900acc13f5432e71eaaac13dbf3cf6', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Migration\Domain\Repository\MigrationStatusRepository'); });
        $this->migrationFactory = new \TYPO3\TYPO3CR\Migration\Domain\Factory\MigrationFactory();
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Service\ContextFactoryInterface', 'TYPO3\Neos\Domain\Service\ContentContextFactory', 'contextFactory', '6b6e9d36a8365cb0dccb3d849ae9366e', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Service\ContextFactoryInterface'); });
        $this->Flow_Injected_Properties = array (
  0 => 'commandManager',
  1 => 'objectManager',
  2 => 'yamlSourceImporter',
  3 => 'nodeDataRepository',
  4 => 'migrationStatusRepository',
  5 => 'migrationFactory',
  6 => 'contextFactory',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.TYPO3CR/Classes/TYPO3/TYPO3CR/Migration/Command/NodeCommandController.php
#