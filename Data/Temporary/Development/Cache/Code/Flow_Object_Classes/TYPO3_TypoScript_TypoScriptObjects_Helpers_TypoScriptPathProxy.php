<?php 
namespace TYPO3\TypoScript\TypoScriptObjects\Helpers;

/*
 * This file is part of the TYPO3.TypoScript package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Exception;
use TYPO3\Fluid\Core\Parser\SyntaxTree\TemplateObjectAccessInterface;
use TYPO3\TypoScript\Core\ExceptionHandlers\ContextDependentHandler;
use TYPO3\TypoScript\Exception\UnsupportedProxyMethodException;
use TYPO3\TypoScript\TypoScriptObjects\TemplateImplementation;

/**
 * A proxy object representing a TypoScript path inside a Fluid Template. It allows
 * to render arbitrary TypoScript objects or Eel expressions using the already-known
 * property path syntax.
 *
 * It wraps a part of the TypoScript tree which does not contain TypoScript objects or Eel expressions.
 *
 * This class is instantiated inside TemplateImplementation and is never used outside.
 */
class TypoScriptPathProxy_Original implements TemplateObjectAccessInterface, \ArrayAccess, \IteratorAggregate, \Countable
{
    /**
     * Reference to the TypoScript Runtime which controls the whole rendering
     *
     * @var \TYPO3\TypoScript\Core\Runtime
     */
    protected $tsRuntime;

    /**
     * Reference to the "parent" TypoScript object
     *
     * @var TemplateImplementation
     */
    protected $templateImplementation;

    /**
     * The TypoScript path this object proxies
     *
     * @var string
     */
    protected $path;

    /**
     * This is a part of the TypoScript tree built when evaluating $this->path.
     *
     * @var array
     */
    protected $partialTypoScriptTree;

    /**
     * @Flow\Inject
     * @var \TYPO3\Flow\Log\SystemLoggerInterface
     */
    protected $systemLogger;

    /**
     * Constructor.
     *
     * @param TemplateImplementation $templateImplementation
     * @param string $path
     * @param array $partialTypoScriptTree
     */
    public function __construct(TemplateImplementation $templateImplementation, $path, array $partialTypoScriptTree)
    {
        $this->templateImplementation = $templateImplementation;
        $this->tsRuntime = $templateImplementation->getTsRuntime();
        $this->path = $path;
        $this->partialTypoScriptTree = $partialTypoScriptTree;
    }

    /**
     * TRUE if a given subpath exists, FALSE otherwise.
     *
     * @param string $offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->partialTypoScriptTree[$offset]);
    }

    /**
     * Return the object at $offset; evaluating simple types right away, and
     * wrapping arrays into ourselves again.
     *
     * @param string $offset
     * @return mixed|TypoScriptPathProxy
     */
    public function offsetGet($offset)
    {
        if (!isset($this->partialTypoScriptTree[$offset])) {
            return null;
        }
        if (!is_array($this->partialTypoScriptTree[$offset])) {
            // Simple type; we call "evaluate" nevertheless to make sure processors are applied.
            return $this->tsRuntime->evaluate($this->path . '/' . $offset);
        } else {
            // arbitrary array (could be Eel expression, TypoScript object, nested sub-array) again, so we wrap it with ourselves.
            return new TypoScriptPathProxy($this->templateImplementation, $this->path . '/' . $offset, $this->partialTypoScriptTree[$offset]);
        }
    }

    /**
     * Stub to implement the ArrayAccess interface cleanly
     *
     * @param string $offset
     * @param mixed $value
     * @throws UnsupportedProxyMethodException
     */
    public function offsetSet($offset, $value)
    {
        throw new UnsupportedProxyMethodException('Setting a property of a path proxy not supported. (tried to set: ' . $this->path . ' -- ' . $offset . ')', 1372667221);
    }

    /**
     * Stub to implement the ArrayAccess interface cleanly
     *
     * @param string $offset
     * @throws UnsupportedProxyMethodException
     */
    public function offsetUnset($offset)
    {
        throw new UnsupportedProxyMethodException('Unsetting a property of a path proxy not supported. (tried to unset: ' . $this->path . ' -- ' . $offset . ')', 1372667331);
    }

    /**
     * Post-Processor which is called whenever this object is encountered in a Fluid
     * object access.
     *
     * Evaluates TypoScript objects and eel expressions.
     *
     * @return TypoScriptPathProxy|mixed
     */
    public function objectAccess()
    {
        if (isset($this->partialTypoScriptTree['__objectType'])) {
            try {
                return $this->tsRuntime->evaluate($this->path);
            } catch (\Exception $exception) {
                return $this->tsRuntime->handleRenderingException($this->path, $exception);
            }
        } elseif (isset($this->partialTypoScriptTree['__eelExpression'])) {
            return $this->tsRuntime->evaluate($this->path, $this->templateImplementation);
        } elseif (isset($this->partialTypoScriptTree['__value'])) {
            return $this->partialTypoScriptTree['__value'];
        }

        return $this;
    }

    /**
     * Iterates through all subelements.
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        $evaluatedArray = array();
        foreach ($this->partialTypoScriptTree as $key => $value) {
            if (!is_array($value)) {
                $evaluatedArray[$key] = $value;
            } elseif (isset($value['__objectType'])) {
                $evaluatedArray[$key] = $this->tsRuntime->evaluate($this->path . '/' . $key);
            } elseif (isset($value['__eelExpression'])) {
                $evaluatedArray[$key] = $this->tsRuntime->evaluate($this->path . '/' . $key, $this->templateImplementation);
            } else {
                $evaluatedArray[$key] = new TypoScriptPathProxy($this->templateImplementation, $this->path . '/' . $key, $this->partialTypoScriptTree[$key]);
            }
        }
        return new \ArrayIterator($evaluatedArray);
    }

    /**
     * @return integer
     */
    public function count()
    {
        return count($this->partialTypoScriptTree);
    }

    /**
     * Finally evaluate the TypoScript path
     *
     * As PHP does not like throwing an exception here, we render any exception using the configured TypoScript exception
     * handler and will also catch and log any exceptions resulting from that as a last resort.
     *
     * @return string
     */
    public function __toString()
    {
        try {
            return (string)$this->tsRuntime->evaluate($this->path);
        } catch (\Exception $exception) {
            try {
                return $this->tsRuntime->handleRenderingException($this->path, $exception);
            } catch (\Exception $exceptionHandlerException) {
                try {
                    // Throwing an exception in __toString causes a fatal error, so if that happens we catch them and use the context dependent exception handler instead.
                    $contextDependentExceptionHandler = new ContextDependentHandler();
                    $contextDependentExceptionHandler->setRuntime($this->tsRuntime);
                    return $contextDependentExceptionHandler->handleRenderingException($this->path, $exception);
                } catch (\Exception $contextDepndentExceptionHandlerException) {
                    $this->systemLogger->logException($contextDepndentExceptionHandlerException, array('path' => $this->path));
                    return sprintf(
                        '<!-- Exception while rendering exception in %s: %s (%s) -->',
                        $this->path,
                        $contextDepndentExceptionHandlerException->getMessage(),
                        $contextDepndentExceptionHandlerException instanceof Exception ? 'see reference code ' . $contextDepndentExceptionHandlerException->getReferenceCode() . ' in log' : $contextDepndentExceptionHandlerException->getCode()
                    );
                }
            }
        }
    }
}
namespace TYPO3\TypoScript\TypoScriptObjects\Helpers;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * A proxy object representing a TypoScript path inside a Fluid Template. It allows
 * to render arbitrary TypoScript objects or Eel expressions using the already-known
 * property path syntax.
 * 
 * It wraps a part of the TypoScript tree which does not contain TypoScript objects or Eel expressions.
 * 
 * This class is instantiated inside TemplateImplementation and is never used outside.
 */
class TypoScriptPathProxy extends TypoScriptPathProxy_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     * @param TemplateImplementation $templateImplementation
     * @param string $path
     * @param array $partialTypoScriptTree
     */
    public function __construct()
    {
        $arguments = func_get_args();

        if (!array_key_exists(0, $arguments)) $arguments[0] = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TypoScript\TypoScriptObjects\TemplateImplementation');
        if (!array_key_exists(0, $arguments)) throw new \TYPO3\Flow\Object\Exception\UnresolvedDependenciesException('Missing required constructor argument $templateImplementation in class ' . __CLASS__ . '. Note that constructor injection is only support for objects of scope singleton (and this is not a singleton) – for other scopes you must pass each required argument to the constructor yourself.', 1296143788);
        if (!array_key_exists(1, $arguments)) throw new \TYPO3\Flow\Object\Exception\UnresolvedDependenciesException('Missing required constructor argument $path in class ' . __CLASS__ . '. Note that constructor injection is only support for objects of scope singleton (and this is not a singleton) – for other scopes you must pass each required argument to the constructor yourself.', 1296143788);
        if (!array_key_exists(2, $arguments)) throw new \TYPO3\Flow\Object\Exception\UnresolvedDependenciesException('Missing required constructor argument $partialTypoScriptTree in class ' . __CLASS__ . '. Note that constructor injection is only support for objects of scope singleton (and this is not a singleton) – for other scopes you must pass each required argument to the constructor yourself.', 1296143788);
        call_user_func_array('parent::__construct', $arguments);
        if ('TYPO3\TypoScript\TypoScriptObjects\Helpers\TypoScriptPathProxy' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'tsRuntime' => '\\TYPO3\\TypoScript\\Core\\Runtime',
  'templateImplementation' => 'TYPO3\\TypoScript\\TypoScriptObjects\\TemplateImplementation',
  'path' => 'string',
  'partialTypoScriptTree' => 'array',
  'systemLogger' => '\\TYPO3\\Flow\\Log\\SystemLoggerInterface',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Log\SystemLoggerInterface', '', 'systemLogger', '6d57d95a1c3cd7528e3e6ea15012dac8', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Log\SystemLoggerInterface'); });
        $this->Flow_Injected_Properties = array (
  0 => 'systemLogger',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.TypoScript/Classes/TYPO3/TypoScript/TypoScriptObjects/Helpers/TypoScriptPathProxy.php
#