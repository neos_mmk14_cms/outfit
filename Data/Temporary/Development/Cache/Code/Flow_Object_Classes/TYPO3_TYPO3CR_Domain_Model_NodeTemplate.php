<?php 
namespace TYPO3\TYPO3CR\Domain\Model;

/*
 * This file is part of the TYPO3.TYPO3CR package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Validation\Validator\UuidValidator;
use TYPO3\TYPO3CR\Domain\Utility\NodePaths;

/**
 * A container of properties which can be used as a template for generating new nodes.
 *
 * @api
 */
class NodeTemplate_Original extends AbstractNodeData
{
    /**
     * The UUID to use for the new node. Use with care.
     *
     * @var string
     */
    protected $identifier;

    /**
     * The node name which acts as a path segment for its node path
     *
     * @var string
     */
    protected $name;

    /**
     * Allows to set a UUID to use for the node that will be created from this
     * NodeTemplate. Use with care, usually identifier generation should be left
     * to the TYPO3CR.
     *
     * @param string $identifier
     * @return void
     * @throws \InvalidArgumentException
     */
    public function setIdentifier($identifier)
    {
        if (preg_match(UuidValidator::PATTERN_MATCH_UUID, $identifier) !== 1) {
            throw new \InvalidArgumentException(sprintf('Invalid UUID "%s" given.', $identifier), 1385026112);
        }
        $this->identifier = $identifier;
    }

    /**
     * Returns the UUID set in this NodeTemplate.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set the name to $newName
     *
     * @param string $newName
     * @return void
     * @throws \InvalidArgumentException
     * @api
     */
    public function setName($newName)
    {
        if (!is_string($newName) || preg_match(NodeInterface::MATCH_PATTERN_NAME, $newName) !== 1) {
            throw new \InvalidArgumentException('Invalid node name "' . $newName . '" (a node name must only contain characters, numbers and the "-" sign).', 1364290839);
        }
        $this->name = $newName;
    }

    /**
     * Get the name of this node template.
     *
     * If a name has been set using setName(), it is returned. If not, but the
     * template has a (non-empty) title property, this property is used to
     * generate a valid name. As a last resort a random name is returned (in
     * the form "name-XXXXX").
     *
     * @return string
     * @api
     */
    public function getName()
    {
        if ($this->name !== null) {
            return $this->name;
        }

        return NodePaths::generateRandomNodeName();
    }

    /**
     * A NodeTemplate is not stored in any workspace, thus this method returns NULL.
     *
     * @return void
     */
    public function getWorkspace()
    {
    }
}
namespace TYPO3\TYPO3CR\Domain\Model;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * A container of properties which can be used as a template for generating new nodes.
 */
class NodeTemplate extends NodeTemplate_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        parent::__construct();
        if ('TYPO3\TYPO3CR\Domain\Model\NodeTemplate' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'identifier' => 'string',
  'name' => 'string',
  'properties' => 'array<mixed>',
  'contentObjectProxy' => 'TYPO3\\TYPO3CR\\Domain\\Model\\ContentObjectProxy',
  'nodeType' => 'string',
  'creationDateTime' => '\\DateTimeInterface',
  'lastModificationDateTime' => '\\DateTimeInterface',
  'lastPublicationDateTime' => '\\DateTimeInterface',
  'hidden' => 'boolean',
  'hiddenBeforeDateTime' => '\\DateTimeInterface',
  'hiddenAfterDateTime' => '\\DateTimeInterface',
  'hiddenInIndex' => 'boolean',
  'accessRoles' => 'array<string>',
  'nodeDataRepository' => 'TYPO3\\TYPO3CR\\Domain\\Repository\\NodeDataRepository',
  'persistenceManager' => 'TYPO3\\Flow\\Persistence\\PersistenceManagerInterface',
  'nodeTypeManager' => 'TYPO3\\TYPO3CR\\Domain\\Service\\NodeTypeManager',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository', 'TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository', 'nodeDataRepository', '6d8e58e235099c88f352e23317321129', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Persistence\PersistenceManagerInterface', 'TYPO3\Flow\Persistence\Doctrine\PersistenceManager', 'persistenceManager', 'f1bc82ad47156d95485678e33f27c110', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Service\NodeTypeManager', 'TYPO3\TYPO3CR\Domain\Service\NodeTypeManager', 'nodeTypeManager', '478a517efacb3d47415a96d9caded2e9', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Service\NodeTypeManager'); });
        $this->Flow_Injected_Properties = array (
  0 => 'nodeDataRepository',
  1 => 'persistenceManager',
  2 => 'nodeTypeManager',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.TYPO3CR/Classes/TYPO3/TYPO3CR/Domain/Model/NodeTemplate.php
#