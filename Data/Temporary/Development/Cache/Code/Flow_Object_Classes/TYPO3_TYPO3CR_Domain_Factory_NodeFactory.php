<?php 
namespace TYPO3\TYPO3CR\Domain\Factory;

/*
 * This file is part of the TYPO3.TYPO3CR package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Object\ObjectManagerInterface;
use TYPO3\Flow\Reflection\ReflectionService;
use TYPO3\Flow\Security\Context as SecurityContext;
use TYPO3\TYPO3CR\Domain\Model\NodeData;
use TYPO3\TYPO3CR\Domain\Model\NodeInterface;
use TYPO3\TYPO3CR\Domain\Service\Context;
use TYPO3\TYPO3CR\Domain\Service\ContextFactoryInterface;
use TYPO3\TYPO3CR\Exception\NodeConfigurationException;

/**
 * This factory creates nodes based on node data. Its main purpose is to
 * assure that nodes created for a certain node data container and context
 * are unique in memory.
 *
 * @Flow\Scope("singleton")
 */
class NodeFactory_Original
{
    /**
     * @var array<\TYPO3\TYPO3CR\Domain\Model\Node>
     */
    protected $nodes = array();

    /**
     * @Flow\Inject
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @Flow\Inject
     * @var SecurityContext
     */
    protected $securityContext;

    /**
     * @Flow\Inject
     * @var ContextFactoryInterface
     */
    protected $contextFactory;


    /**
     * Creates a node from the given NodeData container.
     *
     * If this factory has previously created a Node for the given $node and it's dimensions,
     * it will return the same node again.
     *
     * @param NodeData $nodeData
     * @param Context $context
     * @return NodeInterface
     * @throws NodeConfigurationException if a configured 'class' for a Node does not exist or does not inherit NodeInterface
     */
    public function createFromNodeData(NodeData $nodeData, Context $context)
    {
        if ($nodeData->isInternal()) {
            return null;
        }

        $internalNodeIdentifier = $nodeData->getIdentifier() . spl_object_hash($context);

        // In case there is a Node with an internal NodeData (because the NodeData was changed in the meantime) we need to flush it.
        if (isset($this->nodes[$internalNodeIdentifier]) && $this->nodes[$internalNodeIdentifier]->getNodeData()->isInternal()) {
            unset($this->nodes[$internalNodeIdentifier]);
        }

        if (!isset($this->nodes[$internalNodeIdentifier])) {
            // Warning: Alternative node implementations are considered internal for now, feature can change or be removed anytime. We want to be sure it works well and makes sense before declaring it public.
            $class = $nodeData->getNodeType()->getConfiguration('class') ?: $this->objectManager->getClassNameByObjectName(NodeInterface::class);
            if (!in_array($class, static::getNodeInterfaceImplementations($this->objectManager))) {
                throw new NodeConfigurationException('The configured implementation class name "' . $class . '" for NodeType "' . $nodeData->getNodeType() . '" does not inherit from ' .NodeInterface::class.'.', 1406884014);
            }
            $this->nodes[$internalNodeIdentifier] = new $class($nodeData, $context);
        }
        $node = $this->nodes[$internalNodeIdentifier];

        return $this->filterNodeByContext($node, $context);
    }

    /**
     * Get all NodeInterface implementations to check if a configured node class is in there.
     *
     * @param ObjectManagerInterface $objectManager
     * @return array
     * @Flow\CompileStatic
     */
    public static function getNodeInterfaceImplementations($objectManager)
    {
        $reflectionService = $objectManager->get(ReflectionService::class);
        $nodeImplementations = $reflectionService->getAllImplementationClassNamesForInterface(NodeInterface::class);
        return $nodeImplementations;
    }

    /**
     * Filter a node by the current context.
     * Will either return the node or NULL if it is not permitted in current context.
     *
     * @param NodeInterface $node
     * @param Context $context
     * @return NodeInterface|NULL
     */
    protected function filterNodeByContext(NodeInterface $node, Context $context)
    {
        $this->securityContext->withoutAuthorizationChecks(function () use (&$node, $context) {
            if (!$context->isRemovedContentShown() && $node->isRemoved()) {
                $node = null;
                return;
            }
            if (!$context->isInvisibleContentShown() && !$node->isVisible()) {
                $node = null;
                return;
            }
            if (!$context->isInaccessibleContentShown() && !$node->isAccessible()) {
                $node = null;
            }
        });
        return $node;
    }

    /**
     * Generates a Context that exactly fits the given NodeData Workspace and Dimensions.
     *
     * TODO: We could get more specific about removed and invisible content by adding some more logic here that generates fitting values.
     *
     * @param NodeData $nodeData
     * @return Context
     */
    public function createContextMatchingNodeData(NodeData $nodeData)
    {
        return $this->contextFactory->create(array(
            'workspaceName' => $nodeData->getWorkspace()->getName(),
            'invisibleContentShown' => true,
            'inaccessibleContentShown' => true,
            'removedContentShown' => true,
            'dimensions' => $nodeData->getDimensionValues()
        ));
    }

    /**
     * Reset the node instances (for testing)
     *
     * @return void
     */
    public function reset()
    {
        $this->nodes = array();
    }
}
namespace TYPO3\TYPO3CR\Domain\Factory;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * This factory creates nodes based on node data. Its main purpose is to
 * assure that nodes created for a certain node data container and context
 * are unique in memory.
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class NodeFactory extends NodeFactory_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        if (get_class($this) === 'TYPO3\TYPO3CR\Domain\Factory\NodeFactory') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\TYPO3CR\Domain\Factory\NodeFactory', $this);
        if ('TYPO3\TYPO3CR\Domain\Factory\NodeFactory' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'nodes' => 'array<\\TYPO3\\TYPO3CR\\Domain\\Model\\Node>',
  'objectManager' => 'TYPO3\\Flow\\Object\\ObjectManagerInterface',
  'securityContext' => 'TYPO3\\Flow\\Security\\Context',
  'contextFactory' => 'TYPO3\\TYPO3CR\\Domain\\Service\\ContextFactoryInterface',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {
        if (get_class($this) === 'TYPO3\TYPO3CR\Domain\Factory\NodeFactory') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\TYPO3CR\Domain\Factory\NodeFactory', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Object\ObjectManagerInterface', 'TYPO3\Flow\Object\ObjectManager', 'objectManager', '0c3c44be7be16f2a287f1fb2d068dde4', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Object\ObjectManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Security\Context', 'TYPO3\Flow\Security\Context', 'securityContext', '48836470c14129ade5f39e28c4816673', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\Context'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Service\ContextFactoryInterface', 'TYPO3\Neos\Domain\Service\ContentContextFactory', 'contextFactory', '6b6e9d36a8365cb0dccb3d849ae9366e', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Service\ContextFactoryInterface'); });
        $this->Flow_Injected_Properties = array (
  0 => 'objectManager',
  1 => 'securityContext',
  2 => 'contextFactory',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.TYPO3CR/Classes/TYPO3/TYPO3CR/Domain/Factory/NodeFactory.php
#