<?php 
namespace TYPO3\Form\FormElements;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "TYPO3.Form".            *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;

/**
 * A Section, being part of a bigger Page
 *
 * **This class is not meant to be subclassed by developers.**
 *
 * This class contains multiple FormElements ({@link FormElementInterface}).
 *
 * Please see {@link FormDefinition} for an in-depth explanation.
 *
 * Once we support traits, the duplicated code between AbstractFormElement and Section could be extracted to a Trait.
 */
class Section_Original extends \TYPO3\Form\Core\Model\AbstractSection implements \TYPO3\Form\Core\Model\FormElementInterface
{
    /**
     * @var array
     */
    protected $properties = array();

    /**
     * Will be called as soon as the element is (tried to be) added to a form
     * @see registerInFormIfPossible()
     *
     * @return void
     * @internal
     */
    public function initializeFormElement()
    {
    }

    /**
     * Returns a unique identifier of this element.
     * While element identifiers are only unique within one form,
     * this includes the identifier of the form itself, making it "globally" unique
     *
     * @return string the "globally" unique identifier of this element
     * @api
     */
    public function getUniqueIdentifier()
    {
        $formDefinition = $this->getRootForm();
        return sprintf('%s-%s', $formDefinition->getIdentifier(), $this->identifier);
    }

    /**
     * Get the default value with which the Form Element should be initialized
     * during display.
     * Note: This is currently not used for section elements
     *
     * @return mixed the default value for this Form Element
     * @api
     */
    public function getDefaultValue()
    {
        return null;
    }

    /**
     * Set the default value with which the Form Element should be initialized
     * during display.
     * Note: This is currently ignored for section elements
     *
     * @param mixed $defaultValue the default value for this Form Element
     * @api
     */
    public function setDefaultValue($defaultValue)
    {
    }


    /**
     * Get all element-specific configuration properties
     *
     * @return array
     * @api
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * Set an element-specific configuration property.
     *
     * @param string $key
     * @param mixed $value
     * @return void
     * @api
     */
    public function setProperty($key, $value)
    {
        $this->properties[$key] = $value;
    }

    /**
     * Set the rendering option $key to $value.
     *
     * @param string $key
     * @param mixed $value
     * @api
     * @return mixed
     */
    public function setRenderingOption($key, $value)
    {
        $this->renderingOptions[$key] = $value;
    }

    /**
     * Get all validators on the element
     *
     * @return \SplObjectStorage
     */
    public function getValidators()
    {
        $formDefinition = $this->getRootForm();
        return $formDefinition->getProcessingRule($this->getIdentifier())->getValidators();
    }

    /**
     * Add a validator to the element
     *
     * @param \TYPO3\Flow\Validation\Validator\ValidatorInterface $validator
     * @return void
     */
    public function addValidator(\TYPO3\Flow\Validation\Validator\ValidatorInterface $validator)
    {
        $formDefinition = $this->getRootForm();
        $formDefinition->getProcessingRule($this->getIdentifier())->addValidator($validator);
    }

    /**
     * Whether or not this element is required
     *
     * @return boolean
     * @api
     */
    public function isRequired()
    {
        foreach ($this->getValidators() as $validator) {
            if ($validator instanceof \TYPO3\Flow\Validation\Validator\NotEmptyValidator) {
                return true;
            }
        }
        return false;
    }
}
namespace TYPO3\Form\FormElements;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * A Section, being part of a bigger Page
 * 
 * **This class is not meant to be subclassed by developers.**
 * 
 * This class contains multiple FormElements ({@link FormElementInterface}).
 * 
 * Please see {@link FormDefinition} for an in-depth explanation.
 * 
 * Once we support traits, the duplicated code between AbstractFormElement and Section could be extracted to a Trait.
 */
class Section extends Section_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait;


    /**
     * Autogenerated Proxy Method
     * @param string $identifier The Section identifier
     * @param string $type The Section type
     * @throws \TYPO3\Form\Exception\IdentifierNotValidException if the identifier was no non-empty string
     */
    public function __construct()
    {
        $arguments = func_get_args();
        if (!array_key_exists(0, $arguments)) throw new \TYPO3\Flow\Object\Exception\UnresolvedDependenciesException('Missing required constructor argument $identifier in class ' . __CLASS__ . '. Note that constructor injection is only support for objects of scope singleton (and this is not a singleton) – for other scopes you must pass each required argument to the constructor yourself.', 1296143788);
        if (!array_key_exists(1, $arguments)) throw new \TYPO3\Flow\Object\Exception\UnresolvedDependenciesException('Missing required constructor argument $type in class ' . __CLASS__ . '. Note that constructor injection is only support for objects of scope singleton (and this is not a singleton) – for other scopes you must pass each required argument to the constructor yourself.', 1296143788);
        call_user_func_array('parent::__construct', $arguments);
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'properties' => 'array',
  'renderables' => 'array<TYPO3\\Form\\Core\\Model\\RenderableInterface>',
  'type' => 'string',
  'identifier' => 'string',
  'parentRenderable' => 'TYPO3\\Form\\Core\\Model\\Renderable\\CompositeRenderableInterface',
  'label' => 'string',
  'renderingOptions' => 'array',
  'rendererClassName' => 'string',
  'index' => 'integer',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_setRelatedEntities();
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Form/Classes/TYPO3/Form/FormElements/Section.php
#