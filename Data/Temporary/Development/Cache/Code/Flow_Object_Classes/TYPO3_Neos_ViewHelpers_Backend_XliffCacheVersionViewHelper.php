<?php 
namespace TYPO3\Neos\ViewHelpers\Backend;

/*
 * This file is part of the TYPO3.Neos package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\Neos\Service\XliffService;

/**
 * ViewHelper for rendering the current version identifier for the
 * xliff cache.
 */
class XliffCacheVersionViewHelper_Original extends AbstractViewHelper
{

    /**
     * @Flow\Inject
     * @var XliffService
     */
    protected $xliffService;

    /**
     * @return string The current cache version identifier
     */
    public function render()
    {
        return $this->xliffService->getCacheVersion();
    }
}
namespace TYPO3\Neos\ViewHelpers\Backend;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * ViewHelper for rendering the current version identifier for the
 * xliff cache.
 */
class XliffCacheVersionViewHelper extends XliffCacheVersionViewHelper_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        if ('TYPO3\Neos\ViewHelpers\Backend\XliffCacheVersionViewHelper' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'xliffService' => 'TYPO3\\Neos\\Service\\XliffService',
  'arguments' => 'array',
  'templateVariableContainer' => '\\TYPO3\\Fluid\\Core\\ViewHelper\\TemplateVariableContainer',
  'controllerContext' => '\\TYPO3\\Flow\\Mvc\\Controller\\ControllerContext',
  'renderingContext' => 'TYPO3\\Fluid\\Core\\Rendering\\RenderingContextInterface',
  'renderChildrenClosure' => '\\Closure',
  'viewHelperVariableContainer' => '\\TYPO3\\Fluid\\Core\\ViewHelper\\ViewHelperVariableContainer',
  'objectManager' => 'TYPO3\\Flow\\Object\\ObjectManagerInterface',
  'systemLogger' => 'TYPO3\\Flow\\Log\\SystemLoggerInterface',
  'escapingInterceptorEnabled' => 'boolean',
  'escapeChildren' => 'boolean',
  'escapeOutput' => 'boolean',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->injectObjectManager(\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Object\ObjectManagerInterface'));
        $this->injectSystemLogger(\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Log\SystemLoggerInterface'));
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Neos\Service\XliffService', 'TYPO3\Neos\Service\XliffService', 'xliffService', '79d46d7f2990e1f2311b001d1cc98659', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Neos\Service\XliffService'); });
        $this->Flow_Injected_Properties = array (
  0 => 'objectManager',
  1 => 'systemLogger',
  2 => 'xliffService',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Neos/Classes/TYPO3/Neos/ViewHelpers/Backend/XliffCacheVersionViewHelper.php
#