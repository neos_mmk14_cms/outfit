<?php 
namespace TYPO3\Neos\Validation\Validator;

/*
 * This file is part of the TYPO3.Neos package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Validation\Validator\AbstractValidator;

/**
 * Validator for http://tools.ietf.org/html/rfc1123 compatible host names
 */
class HostnameValidator_Original extends AbstractValidator
{
    /**
     * @var array
     */
    protected $supportedOptions = array(
        'ignoredHostnames' => array('', 'Hostnames that are not to be validated', 'string'),
    );

    /**
     * Validates if the hostname is valid.
     *
     * @param mixed $hostname The hostname that should be validated
     * @return void
     */
    protected function isValid($hostname)
    {
        $pattern = '/(?=^.{4,253}$)(^((?!-)[a-zA-Z0-9-]{1,63}(?<!-)\.)*(?!-)[a-zA-Z]{2,63}(?<!-)$)/';

        if ($this->options['ignoredHostnames']) {
            $ignoredHostnames = explode(',', $this->options['ignoredHostnames']);
            if (in_array($hostname, $ignoredHostnames)) {
                return;
            }
        }

        if (!preg_match($pattern, $hostname)) {
            $this->addError('The hostname "%1$s" was not valid.', 1415392993, array($hostname));
        }
    }
}
namespace TYPO3\Neos\Validation\Validator;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * Validator for http://tools.ietf.org/html/rfc1123 compatible host names
 */
class HostnameValidator extends HostnameValidator_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'supportedOptions' => 'array',
  'acceptsEmptyValues' => 'boolean',
  'options' => 'array',
  'result' => 'TYPO3\\Flow\\Error\\Result',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_setRelatedEntities();
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Neos/Classes/TYPO3/Neos/Validation/Validator/HostnameValidator.php
#