<?php 
namespace TYPO3\Flow\Validation\Validator;

/*
 * This file is part of the TYPO3.Flow package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\I18n\Cldr\Reader\DatesReader;
use TYPO3\Flow\I18n;

/**
 * Validator for DateTime objects.
 *
 * @api
 */
class DateTimeValidator_Original extends AbstractValidator
{
    /**
     * @var array
     */
    protected $supportedOptions = [
        'locale' => [null, 'The locale to use for date parsing', 'string|Locale'],
        'strictMode' => [true, 'Use strict mode for date parsing', 'boolean'],
        'formatLength' => [DatesReader::FORMAT_LENGTH_DEFAULT, 'The format length, see DatesReader::FORMAT_LENGTH_*', 'string'],
        'formatType' => [DatesReader::FORMAT_TYPE_DATE, 'The format type, see DatesReader::FORMAT_TYPE_*', 'string']
    ];

    /**
     * @Flow\Inject
     * @var I18n\Service
     */
    protected $localizationService;

    /**
     * @Flow\Inject
     * @var I18n\Parser\DatetimeParser
     */
    protected $datetimeParser;

    /**
     * Checks if the given value is a valid DateTime object.
     *
     * @param mixed $value The value that should be validated
     * @return void
     * @api
     */
    protected function isValid($value)
    {
        if ($value instanceof \DateTimeInterface) {
            return;
        }
        if (!isset($this->options['locale'])) {
            $locale = $this->localizationService->getConfiguration()->getDefaultLocale();
        } elseif (is_string($this->options['locale'])) {
            $locale = new I18n\Locale($this->options['locale']);
        } elseif ($this->options['locale'] instanceof I18n\Locale) {
            $locale = $this->options['locale'];
        } else {
            $this->addError('The "locale" option can be only set to string identifier, or Locale object.', 1281454676);
            return;
        }

        $strictMode = $this->options['strictMode'];

        $formatLength = $this->options['formatLength'];
        DatesReader::validateFormatLength($formatLength);

        $formatType = $this->options['formatType'];
        DatesReader::validateFormatType($formatType);

        if ($formatType === DatesReader::FORMAT_TYPE_TIME) {
            if ($this->datetimeParser->parseTime($value, $locale, $formatLength, $strictMode) === false) {
                $this->addError('A valid time is expected.', 1281454830);
            }
        } elseif ($formatType === DatesReader::FORMAT_TYPE_DATETIME) {
            if ($this->datetimeParser->parseDateAndTime($value, $locale, $formatLength, $strictMode) === false) {
                $this->addError('A valid date and time is expected.', 1281454831);
            }
        } else {
            if ($this->datetimeParser->parseDate($value, $locale, $formatLength, $strictMode) === false) {
                $this->addError('A valid date is expected.', 1281454832);
            }
        }
    }
}
namespace TYPO3\Flow\Validation\Validator;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * Validator for DateTime objects.
 */
class DateTimeValidator extends DateTimeValidator_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     * @param array $options Options for the validator
     * @throws InvalidValidationOptionsException if unsupported options are found
     */
    public function __construct()
    {
        $arguments = func_get_args();
        call_user_func_array('parent::__construct', $arguments);
        if ('TYPO3\Flow\Validation\Validator\DateTimeValidator' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'supportedOptions' => 'array',
  'localizationService' => 'TYPO3\\Flow\\I18n\\Service',
  'datetimeParser' => 'TYPO3\\Flow\\I18n\\Parser\\DatetimeParser',
  'acceptsEmptyValues' => 'boolean',
  'options' => 'array',
  'result' => 'TYPO3\\Flow\\Error\\Result',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\I18n\Service', 'TYPO3\Flow\I18n\Service', 'localizationService', 'd147918505b040be63714e111bab34f3', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\I18n\Service'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\I18n\Parser\DatetimeParser', 'TYPO3\Flow\I18n\Parser\DatetimeParser', 'datetimeParser', '1f293b0eccc45289c02788eac3dd1f85', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\I18n\Parser\DatetimeParser'); });
        $this->Flow_Injected_Properties = array (
  0 => 'localizationService',
  1 => 'datetimeParser',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Framework/TYPO3.Flow/Classes/TYPO3/Flow/Validation/Validator/DateTimeValidator.php
#