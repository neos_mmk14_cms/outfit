<?php 
namespace TYPO3\Neos;

/*
 * This file is part of the TYPO3.Neos package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Cache\CacheManager;
use TYPO3\Flow\Core\Bootstrap;
use TYPO3\Flow\Monitor\FileMonitor;
use TYPO3\Flow\Mvc\Routing\RouterCachingService;
use TYPO3\Flow\Package\Package as BasePackage;
use TYPO3\Flow\Persistence\Doctrine\PersistenceManager;
use TYPO3\Neos\Domain\Model\Site;
use TYPO3\Neos\Domain\Service\SiteImportService;
use TYPO3\Neos\Domain\Service\SiteService;
use TYPO3\Neos\EventLog\Integrations\TYPO3CRIntegrationService;
use TYPO3\Neos\Routing\Cache\RouteCacheFlusher;
use TYPO3\Neos\Service\PublishingService;
use TYPO3\Neos\Utility\NodeUriPathSegmentGenerator;
use TYPO3\TYPO3CR\Domain\Model\Node;
use TYPO3\TYPO3CR\Domain\Model\NodeInterface;
use TYPO3\TYPO3CR\Domain\Model\Workspace;
use TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository;
use TYPO3\TYPO3CR\Domain\Service\Context;
use TYPO3\TypoScript\Core\Cache\ContentCache;

/**
 * The Neos Package
 */
class Package_Original extends BasePackage
{
    /**
     * @param Bootstrap $bootstrap The current bootstrap
     * @return void
     */
    public function boot(Bootstrap $bootstrap)
    {
        $dispatcher = $bootstrap->getSignalSlotDispatcher();

        $flushConfigurationCache = function () use ($bootstrap) {
            $cacheManager = $bootstrap->getEarlyInstance(CacheManager::class);
            $cacheManager->getCache('TYPO3_Neos_Configuration_Version')->flush();
        };

        $flushXliffServiceCache = function () use ($bootstrap) {
            $cacheManager = $bootstrap->getEarlyInstance(CacheManager::class);
            $cacheManager->getCache('TYPO3_Neos_XliffToJsonTranslations')->flush();
        };

        $dispatcher->connect(FileMonitor::class, 'filesHaveChanged', function ($fileMonitorIdentifier, array $changedFiles) use ($flushConfigurationCache, $flushXliffServiceCache) {
            switch ($fileMonitorIdentifier) {
                case 'TYPO3CR_NodeTypesConfiguration':
                case 'Flow_ConfigurationFiles':
                    $flushConfigurationCache();
                    break;
                case 'Flow_TranslationFiles':
                    $flushConfigurationCache();
                    $flushXliffServiceCache();
            }
        });

        $dispatcher->connect(Site::class, 'siteChanged', $flushConfigurationCache);
        $dispatcher->connect(Site::class, 'siteChanged', 'TYPO3\Flow\Mvc\Routing\RouterCachingService', 'flushCaches');

        $dispatcher->connect(Node::class, 'nodeUpdated', 'TYPO3\Neos\TypoScript\Cache\ContentCacheFlusher', 'registerNodeChange');
        $dispatcher->connect(Node::class, 'nodeAdded', 'TYPO3\Neos\TypoScript\Cache\ContentCacheFlusher', 'registerNodeChange');
        $dispatcher->connect(Node::class, 'nodeRemoved', 'TYPO3\Neos\TypoScript\Cache\ContentCacheFlusher', 'registerNodeChange');
        $dispatcher->connect(Node::class, 'beforeNodeMove', 'TYPO3\Neos\TypoScript\Cache\ContentCacheFlusher', 'registerNodeChange');

        $dispatcher->connect('TYPO3\Media\Domain\Service\AssetService', 'assetResourceReplaced', 'TYPO3\Neos\TypoScript\Cache\ContentCacheFlusher', 'registerAssetResourceChange');

        $dispatcher->connect(Node::class, 'nodeAdded', NodeUriPathSegmentGenerator::class, '::setUniqueUriPathSegment');
        $dispatcher->connect(Node::class, 'nodePropertyChanged', Service\ImageVariantGarbageCollector::class, 'removeUnusedImageVariant');
        $dispatcher->connect(Node::class, 'nodePropertyChanged', function (NodeInterface $node, $propertyName) use ($bootstrap) {
            if ($propertyName === 'uriPathSegment') {
                NodeUriPathSegmentGenerator::setUniqueUriPathSegment($node);
                $bootstrap->getObjectManager()->get(RouteCacheFlusher::class)->registerNodeChange($node);
            }
        });
        $dispatcher->connect(Node::class, 'nodePathChanged', function (NodeInterface $node, $oldPath, $newPath, $recursion) {
            if (!$recursion) {
                NodeUriPathSegmentGenerator::setUniqueUriPathSegment($node);
            }
        });

        $dispatcher->connect(PublishingService::class, 'nodePublished', 'TYPO3\Neos\TypoScript\Cache\ContentCacheFlusher', 'registerNodeChange');
        $dispatcher->connect(PublishingService::class, 'nodeDiscarded', 'TYPO3\Neos\TypoScript\Cache\ContentCacheFlusher', 'registerNodeChange');

        $dispatcher->connect(Node::class, 'nodePathChanged', 'TYPO3\Neos\Routing\Cache\RouteCacheFlusher', 'registerNodeChange');
        $dispatcher->connect(Node::class, 'nodeRemoved', 'TYPO3\Neos\Routing\Cache\RouteCacheFlusher', 'registerNodeChange');
        $dispatcher->connect('TYPO3\Neos\Service\PublishingService', 'nodeDiscarded', 'TYPO3\Neos\Routing\Cache\RouteCacheFlusher', 'registerNodeChange');
        $dispatcher->connect(PublishingService::class, 'nodePublished', 'TYPO3\Neos\Routing\Cache\RouteCacheFlusher', 'registerNodeChange');
        $dispatcher->connect(PublishingService::class, 'nodePublished', function ($node, $targetWorkspace) use ($bootstrap) {
            $cacheManager = $bootstrap->getObjectManager()->get(CacheManager::class);
            if ($cacheManager->hasCache('Flow_Persistence_Doctrine')) {
                $cacheManager->getCache('Flow_Persistence_Doctrine')->flush();
            }
        });
        $dispatcher->connect(PersistenceManager::class, 'allObjectsPersisted', RouteCacheFlusher::class, 'commit');

        $dispatcher->connect(SiteService::class, 'sitePruned', ContentCache::class, 'flush');
        $dispatcher->connect(SiteService::class, 'sitePruned', RouterCachingService::class, 'flushCaches');

        $dispatcher->connect(SiteImportService::class, 'siteImported', ContentCache::class, 'flush');
        $dispatcher->connect(SiteImportService::class, 'siteImported', RouterCachingService::class, 'flushCaches');

        // Eventlog
        $dispatcher->connect(Node::class, 'beforeNodeCreate', TYPO3CRIntegrationService::class, 'beforeNodeCreate');
        $dispatcher->connect(Node::class, 'afterNodeCreate', TYPO3CRIntegrationService::class, 'afterNodeCreate');

        $dispatcher->connect(Node::class, 'nodeUpdated', TYPO3CRIntegrationService::class, 'nodeUpdated');
        $dispatcher->connect(Node::class, 'nodeRemoved', TYPO3CRIntegrationService::class, 'nodeRemoved');

        $dispatcher->connect(Node::class, 'beforeNodePropertyChange', TYPO3CRIntegrationService::class, 'beforeNodePropertyChange');
        $dispatcher->connect(Node::class, 'nodePropertyChanged', TYPO3CRIntegrationService::class, 'nodePropertyChanged');

        $dispatcher->connect(Node::class, 'beforeNodeCopy', TYPO3CRIntegrationService::class, 'beforeNodeCopy');
        $dispatcher->connect(Node::class, 'afterNodeCopy', TYPO3CRIntegrationService::class, 'afterNodeCopy');

        $dispatcher->connect(Node::class, 'beforeNodeMove', TYPO3CRIntegrationService::class, 'beforeNodeMove');
        $dispatcher->connect(Node::class, 'afterNodeMove', TYPO3CRIntegrationService::class, 'afterNodeMove');

        $dispatcher->connect(Context::class, 'beforeAdoptNode', TYPO3CRIntegrationService::class, 'beforeAdoptNode');
        $dispatcher->connect(Context::class, 'afterAdoptNode', TYPO3CRIntegrationService::class, 'afterAdoptNode');

        $dispatcher->connect(Workspace::class, 'beforeNodePublishing', TYPO3CRIntegrationService::class, 'beforeNodePublishing');
        $dispatcher->connect(Workspace::class, 'afterNodePublishing', TYPO3CRIntegrationService::class, 'afterNodePublishing');

        $dispatcher->connect(PersistenceManager::class, 'allObjectsPersisted', TYPO3CRIntegrationService::class, 'updateEventsAfterPublish');
        $dispatcher->connect(NodeDataRepository::class, 'repositoryObjectsPersisted', TYPO3CRIntegrationService::class, 'updateEventsAfterPublish');
    }
}
namespace TYPO3\Neos;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * The Neos Package
 */
class Package extends Package_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait;


    /**
     * Autogenerated Proxy Method
     * @param string $packageKey Key of this package
     * @param string $composerName
     * @param string $packagePath Absolute path to the location of the package's composer manifest
     * @param array $autoloadConfiguration
     * @throws Exception\InvalidPackageKeyException
     */
    public function __construct()
    {
        $arguments = func_get_args();
        if (!array_key_exists(0, $arguments)) throw new \TYPO3\Flow\Object\Exception\UnresolvedDependenciesException('Missing required constructor argument $packageKey in class ' . __CLASS__ . '. Note that constructor injection is only support for objects of scope singleton (and this is not a singleton) – for other scopes you must pass each required argument to the constructor yourself.', 1296143788);
        if (!array_key_exists(1, $arguments)) throw new \TYPO3\Flow\Object\Exception\UnresolvedDependenciesException('Missing required constructor argument $composerName in class ' . __CLASS__ . '. Note that constructor injection is only support for objects of scope singleton (and this is not a singleton) – for other scopes you must pass each required argument to the constructor yourself.', 1296143788);
        if (!array_key_exists(2, $arguments)) throw new \TYPO3\Flow\Object\Exception\UnresolvedDependenciesException('Missing required constructor argument $packagePath in class ' . __CLASS__ . '. Note that constructor injection is only support for objects of scope singleton (and this is not a singleton) – for other scopes you must pass each required argument to the constructor yourself.', 1296143788);
        call_user_func_array('parent::__construct', $arguments);
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'packageKey' => 'string',
  'composerName' => 'string',
  'packagePath' => 'string',
  'protected' => 'boolean',
  'packageMetaData' => '\\TYPO3\\Flow\\Package\\MetaData',
  'namespace' => 'string',
  'namespaces' => 'string[]',
  'autoloadTypes' => 'string[]',
  'objectManagementEnabled' => 'boolean',
  'autoloadConfiguration' => 'array',
  'flattenedAutoloadConfiguration' => 'array',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_setRelatedEntities();
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Neos/Classes/TYPO3/Neos/Package.php
#