<?php 
namespace TYPO3\Neos\Domain\Service;

/*
 * This file is part of the TYPO3.Neos package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\Flow\Security\Account;
use TYPO3\Flow\Security\AccountFactory;
use TYPO3\Flow\Security\AccountRepository;
use TYPO3\Flow\Security\Authentication\AuthenticationManagerInterface;
use TYPO3\Flow\Security\Authentication\Token\UsernamePassword;
use TYPO3\Flow\Security\Authentication\TokenInterface;
use TYPO3\Flow\Security\Authorization\PrivilegeManagerInterface;
use TYPO3\Flow\Security\Context;
use TYPO3\Flow\Security\Cryptography\HashService;
use TYPO3\Flow\Security\Exception\NoSuchRoleException;
use TYPO3\Flow\Security\Policy\PolicyService;
use TYPO3\Flow\Security\Policy\Role;
use TYPO3\Flow\Utility\Now;
use TYPO3\Neos\Domain\Exception;
use TYPO3\Neos\Domain\Model\User;
use TYPO3\Neos\Domain\Repository\UserRepository;
use TYPO3\Neos\Service\PublishingService;
use TYPO3\Party\Domain\Model\PersonName;
use TYPO3\Party\Domain\Repository\PartyRepository;
use TYPO3\Party\Domain\Service\PartyService;
use TYPO3\TYPO3CR\Domain\Model\Workspace;
use TYPO3\TYPO3CR\Domain\Repository\WorkspaceRepository;
use TYPO3\Neos\Utility\User as UserUtility;

/**
 * A service for managing users
 *
 * @Flow\Scope("singleton")
 * @api
 */
class UserService_Original
{

    /**
     * Might be configurable in the future, for now centralising this as a "constant"
     *
     * @var string
     */
    protected $defaultAuthenticationProviderName = 'Typo3BackendProvider';

    /**
     * @Flow\Inject
     * @var WorkspaceRepository
     */
    protected $workspaceRepository;

    /**
     * @Flow\Inject
     * @var PublishingService
     */
    protected $publishingService;

    /**
     * @Flow\Inject
     * @var PartyRepository
     */
    protected $partyRepository;

    /**
     * @Flow\Inject
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @Flow\Inject
     * @var PartyService
     */
    protected $partyService;

    /**
     * @Flow\Inject
     * @var AccountFactory
     */
    protected $accountFactory;

    /**
     * @Flow\Inject
     * @var AccountRepository
     */
    protected $accountRepository;

    /**
     * @Flow\Inject
     * @var PolicyService
     */
    protected $policyService;

    /**
     * @Flow\Inject
     * @var AuthenticationManagerInterface
     */
    protected $authenticationManager;

    /**
     * @Flow\Inject
     * @var PrivilegeManagerInterface
     */
    protected $privilegeManager;

    /**
     * @Flow\Inject
     * @var Context
     */
    protected $securityContext;

    /**
     * @Flow\Inject
     * @var HashService
     */
    protected $hashService;

    /**
     * @Flow\Inject(lazy = FALSE)
     * @var Now
     */
    protected $now;

    /**
     * @var array
     */
    protected $runtimeUserCache = [];

    /**
     * Retrieves a list of all existing users
     *
     * @return array<User> The users
     * @api
     */
    public function getUsers()
    {
        return $this->userRepository->findAll();
    }

    /**
     * Retrieves an existing user by the given username
     *
     * @param string $username The username
     * @param string $authenticationProviderName Name of the authentication provider to use. Example: "Typo3BackendProvider"
     * @return User The user, or null if the user does not exist
     * @throws Exception
     * @api
     */
    public function getUser($username, $authenticationProviderName = null)
    {
        if ($authenticationProviderName !== null && isset($this->runtimeUserCache['a_' . $authenticationProviderName][$username])) {
            return $this->runtimeUserCache['a_' . $authenticationProviderName][$username];
        } elseif (isset($this->runtimeUserCache['u_' . $username])) {
            return $this->runtimeUserCache['u_' . $username];
        }
        $account = $this->accountRepository->findByAccountIdentifierAndAuthenticationProviderName($username, $authenticationProviderName ?: $this->defaultAuthenticationProviderName);
        if (!$account instanceof Account) {
            return null;
        }
        $user = $this->partyService->getAssignedPartyOfAccount($account);
        if (!$user instanceof User) {
            throw new Exception(sprintf('Unexpected user type "%s". An account with the identifier "%s" exists, but the corresponding party is not a Neos User.', get_class($user), $username), 1422270948);
        }
        if ($authenticationProviderName !== null) {
            if (!isset($this->runtimeUserCache['a_' . $authenticationProviderName])) {
                $this->runtimeUserCache['a_' . $authenticationProviderName] = [];
            }
            $this->runtimeUserCache['a_' . $authenticationProviderName][$username] = $user;
        } else {
            $this->runtimeUserCache['u_' . $username] = $user;
        }
        return $user;
    }

    /**
     * Returns the username of the given user
     *
     * Technically, this method will look for the user's backend account (or, if authenticationProviderName is specified,
     * for the account matching the given authentication provider) and return the account's identifier.
     *
     * @param User $user
     * @param string $authenticationProviderName
     * @return string The username or null if the given user does not have a backend account
     */
    public function getUsername(User $user, $authenticationProviderName = null)
    {
        $authenticationProviderName = $authenticationProviderName ?: $this->defaultAuthenticationProviderName;
        foreach ($user->getAccounts() as $account) {
            /** @var Account $account */
            if ($account->getAuthenticationProviderName() === $authenticationProviderName) {
                return $account->getAccountIdentifier();
            }
        }
        return null;
    }

    /**
     * Returns the currently logged in user, if any
     *
     * @return User The currently logged in user, or null
     * @api
     */
    public function getCurrentUser()
    {
        if ($this->securityContext->canBeInitialized() === true) {
            $account = $this->securityContext->getAccount();
            if ($account !== null) {
                return $this->getUser($account->getAccountIdentifier());
            }
        }

        return null;
    }

    /**
     * Creates a user based on the given information
     *
     * The created user and account are automatically added to their respective repositories and thus be persisted.
     *
     * @param string $username The username of the user to be created.
     * @param string $password Password of the user to be created
     * @param string $firstName First name of the user to be created
     * @param string $lastName Last name of the user to be created
     * @param array $roleIdentifiers A list of role identifiers to assign
     * @param string $authenticationProviderName Name of the authentication provider to use. Example: "Typo3BackendProvider"
     * @return User The created user instance
     * @api
     */
    public function createUser($username, $password, $firstName, $lastName, array $roleIdentifiers = null, $authenticationProviderName = null)
    {
        $user = new User();
        $name = new PersonName('', $firstName, '', $lastName, '', $username);
        $user->setName($name);

        return $this->addUser($username, $password, $user, $roleIdentifiers, $authenticationProviderName);
    }

    /**
     * Adds a user whose User object has been created elsewhere
     *
     * This method basically "creates" a user like createUser() would, except that it does not create the User
     * object itself. If you need to create the User object elsewhere, for example in your ActionController, make sure
     * to call this method for registering the new user instead of adding it to the PartyRepository manually.
     *
     * This method also creates a new user workspace for the given user if no such workspace exist.
     *
     * @param string $username The username of the user to be created.
     * @param string $password Password of the user to be created
     * @param User $user The pre-built user object to start with
     * @param array $roleIdentifiers A list of role identifiers to assign
     * @param string $authenticationProviderName Name of the authentication provider to use. Example: "Typo3BackendProvider"
     * @return User The same user object
     * @api
     */
    public function addUser($username, $password, User $user, array $roleIdentifiers = null, $authenticationProviderName = null)
    {
        if ($roleIdentifiers === null) {
            $roleIdentifiers = array('TYPO3.Neos:Editor');
        }
        $roleIdentifiers = $this->normalizeRoleIdentifiers($roleIdentifiers);
        $account = $this->accountFactory->createAccountWithPassword($username, $password, $roleIdentifiers, $authenticationProviderName ?: $this->defaultAuthenticationProviderName);
        $this->partyService->assignAccountToParty($account, $user);

        $this->partyRepository->add($user);
        $this->accountRepository->add($account);

        $this->createPersonalWorkspace($user, $account);

        $this->emitUserCreated($user);

        return $user;
    }

    /**
     * Signals that a new user, including a new account has been created.
     *
     * @param User $user The created user
     * @return void
     * @Flow\Signal
     * @api
     */
    public function emitUserCreated(User $user)
    {
    }

    /**
     * Deletes the specified user and all remaining content in his personal workspaces
     *
     * @param User $user The user to delete
     * @return void
     * @throws Exception
     * @api
     */
    public function deleteUser(User $user)
    {
        $backendUserRole = $this->policyService->getRole('TYPO3.Neos:AbstractEditor');
        foreach ($user->getAccounts() as $account) {
            /** @var Account $account */
            if ($account->hasRole($backendUserRole)) {
                $this->deletePersonalWorkspace($account->getAccountIdentifier());
            }
            $this->accountRepository->remove($account);
        }

        $this->removeOwnerFromUsersWorkspaces($user);

        $this->partyRepository->remove($user);
        $this->emitUserDeleted($user);
    }

    /**
     * Signals that the given user has been deleted.
     *
     * @param User $user The created user
     * @return void
     * @Flow\Signal
     * @api
     */
    public function emitUserDeleted(User $user)
    {
    }

    /**
     * Sets a new password for the given user
     *
     * This method will iterate over all accounts owned by the given user and, if the account uses a UsernamePasswordToken,
     * sets a new password accordingly.
     *
     * @param User $user The user to set the password for
     * @param string $password A new password
     * @return void
     * @api
     */
    public function setUserPassword(User $user, $password)
    {
        $tokens = $this->authenticationManager->getTokens();
        $indexedTokens = array();
        foreach ($tokens as $token) {
            /** @var TokenInterface $token */
            $indexedTokens[$token->getAuthenticationProviderName()] = $token;
        }

        foreach ($user->getAccounts() as $account) {
            /** @var Account $account */
            $authenticationProviderName = $account->getAuthenticationProviderName();
            if (isset($indexedTokens[$authenticationProviderName]) && $indexedTokens[$authenticationProviderName] instanceof UsernamePassword) {
                $account->setCredentialsSource($this->hashService->hashPassword($password));
                $this->accountRepository->update($account);
            }
        }
    }

    /**
     * Updates the given user in the respective repository and potentially executes further actions depending on what
     * has been changed.
     *
     * Note: changes to the user's account will not be committed for persistence. Please use addRoleToAccount(), removeRoleFromAccount(),
     * setRolesForAccount() and setUserPassword() for changing account properties.
     *
     * @param User $user The modified user
     * @return void
     * @api
     */
    public function updateUser(User $user)
    {
        $this->partyRepository->update($user);
        $this->emitUserUpdated($user);
    }

    /**
     * Adds the specified role to all accounts of the given user and potentially carries out further actions which are needed to
     * properly reflect these changes.
     *
     * @param User $user The user to add roles to
     * @param string $roleIdentifier A fully qualified role identifier, or a role identifier relative to the TYPO3.Neos namespace
     * @return integer How often this role has been added to accounts owned by the user
     * @api
     */
    public function addRoleToUser(User $user, $roleIdentifier)
    {
        $counter = 0;
        foreach ($user->getAccounts() as $account) {
            $counter += $this->addRoleToAccount($account, $roleIdentifier);
        }

        return $counter;
    }

    /**
     * Removes the specified role from all accounts of the given user and potentially carries out further actions which are needed to
     * properly reflect these changes.
     *
     * @param User $user The user to remove roles from
     * @param string $roleIdentifier A fully qualified role identifier, or a role identifier relative to the TYPO3.Neos namespace
     * @return integer How often this role has been removed from accounts owned by the user
     * @api
     */
    public function removeRoleFromUser(User $user, $roleIdentifier)
    {
        $counter = 0;
        foreach ($user->getAccounts() as $account) {
            $counter += $this->removeRoleFromAccount($account, $roleIdentifier);
        }

        return $counter;
    }

    /**
     * Signals that the given user data has been updated.
     *
     * @param User $user The created user
     * @return void
     * @Flow\Signal
     * @api
     */
    public function emitUserUpdated(User $user)
    {
    }

    /**
     * Overrides any assigned roles of the given account and potentially carries out further actions which are needed
     * to properly reflect these changes.
     *
     * @param Account $account The account to assign the roles to
     * @param array $newRoleIdentifiers A list of fully qualified role identifiers, or role identifiers relative to the TYPO3.Neos namespace
     * @return void
     * @api
     */
    public function setRolesForAccount(Account $account, array $newRoleIdentifiers)
    {
        $currentRoles = $account->getRoles();

        foreach ($currentRoles as $roleIdentifier => $role) {
            $roleIdentifier = $this->normalizeRoleIdentifier($roleIdentifier);
            if (!in_array($roleIdentifier, $newRoleIdentifiers)) {
                $this->removeRoleFromAccount($account, $roleIdentifier);
            }
        }

        foreach ($newRoleIdentifiers as $roleIdentifier) {
            if (!in_array($roleIdentifier, array_keys($currentRoles))) {
                $this->addRoleToAccount($account, $roleIdentifier);
            }
        }
    }

    /**
     * Adds the specified role to the given account and potentially carries out further actions which are needed to
     * properly reflect these changes.
     *
     * @param Account $account The account to add roles to
     * @param string $roleIdentifier A fully qualified role identifier, or a role identifier relative to the TYPO3.Neos namespace
     * @return integer How often this role has been added to the given account (effectively can be 1 or 0)
     * @api
     */
    public function addRoleToAccount(Account $account, $roleIdentifier)
    {
        $roleIdentifier = $this->normalizeRoleIdentifier($roleIdentifier);
        $role = $this->policyService->getRole($roleIdentifier);

        if (!$account->hasRole($role)) {
            $account->addRole($role);
            $this->accountRepository->update($account);
            $this->emitRolesAdded($account, array($role));

            return 1;
        }

        return 0;
    }

    /**
     * Signals that new roles have been assigned to the given account
     *
     * @param Account $account The account
     * @param array<Role> An array of Role objects which have been added for that account
     * @return void
     * @Flow\Signal
     * @api
     */
    public function emitRolesAdded(Account $account, array $roles)
    {
    }

    /**
     * Removes the specified role from the given account and potentially carries out further actions which are needed to
     * properly reflect these changes.
     *
     * @param Account $account The account to remove roles from
     * @param string $roleIdentifier A fully qualified role identifier, or a role identifier relative to the TYPO3.Neos namespace
     * @return integer How often this role has been removed from the given account (effectively can be 1 or 0)
     * @api
     */
    public function removeRoleFromAccount(Account $account, $roleIdentifier)
    {
        $roleIdentifier = $this->normalizeRoleIdentifier($roleIdentifier);
        $role = $this->policyService->getRole($roleIdentifier);

        /** @var Account $account */
        if ($account->hasRole($role)) {
            $account->removeRole($role);
            $this->accountRepository->update($account);
            $this->emitRolesRemoved($account, array($role));

            return 1;
        }

        return 0;
    }

    /**
     * Signals that roles have been removed to the given account
     *
     * @param Account $account The account
     * @param array<Role> An array of Role objects which have been removed
     * @return void
     * @Flow\Signal
     * @api
     */
    public function emitRolesRemoved(Account $account, array $roles)
    {
    }

    /**
     * Reactivates the given user
     *
     * @param User $user The user to deactivate
     * @return void
     * @api
     */
    public function activateUser(User $user)
    {
        foreach ($user->getAccounts() as $account) {
            /** @var Account $account */
            $account->setExpirationDate(null);
            $this->accountRepository->update($account);
        }
        $this->emitUserActivated($user);
    }

    /**
     * Signals that the given user has been activated
     *
     * @param User $user The user
     * @return void
     * @Flow\Signal
     * @api
     */
    public function emitUserActivated(User $user)
    {
    }

    /**
     * Deactivates the given user
     *
     * @param User $user The user to deactivate
     * @return void
     * @api
     */
    public function deactivateUser(User $user)
    {
        /** @var Account $account */
        foreach ($user->getAccounts() as $account) {
            $account->setExpirationDate($this->now);
            $this->accountRepository->update($account);
        }
        $this->emitUserDeactivated($user);
    }

    /**
     * Checks if the current user may publish to the given workspace according to one the roles of the user's accounts
     *
     * In future versions, this logic may be implemented in Neos in a more generic way (for example, by means of an
     * ACL object), but for now, this method exists in order to at least centralize and encapsulate the required logic.
     *
     * @param Workspace $workspace The workspace
     * @return boolean
     */
    public function currentUserCanPublishToWorkspace(Workspace $workspace)
    {
        if ($workspace->getName() === 'live') {
            return $this->securityContext->hasRole('TYPO3.Neos:LivePublisher');
        }

        if ($workspace->getOwner() === $this->getCurrentUser() || $workspace->getOwner() === null) {
            return true;
        }

        return false;
    }

    /**
     * Checks if the current user may read the given workspace according to one the roles of the user's accounts
     *
     * In future versions, this logic may be implemented in Neos in a more generic way (for example, by means of an
     * ACL object), but for now, this method exists in order to at least centralize and encapsulate the required logic.
     *
     * @param Workspace $workspace The workspace
     * @return boolean
     */
    public function currentUserCanReadWorkspace(Workspace $workspace)
    {
        if ($workspace->getName() === 'live') {
            return true;
        }

        if ($workspace->getOwner() === $this->getCurrentUser() || $workspace->getOwner() === null) {
            return true;
        }

        return false;
    }

    /**
     * Checks if the current user may manage the given workspace according to one the roles of the user's accounts
     *
     * In future versions, this logic may be implemented in Neos in a more generic way (for example, by means of an
     * ACL object), but for now, this method exists in order to at least centralize and encapsulate the required logic.
     *
     * @param Workspace $workspace The workspace
     * @return boolean
     */
    public function currentUserCanManageWorkspace(Workspace $workspace)
    {
        if ($workspace->isPersonalWorkspace()) {
            return false;
        }

        if ($workspace->isInternalWorkspace()) {
            return $this->privilegeManager->isPrivilegeTargetGranted('TYPO3.Neos:Backend.Module.Management.Workspaces.ManageInternalWorkspaces');
        }

        if ($workspace->isPrivateWorkspace() && $workspace->getOwner() === $this->getCurrentUser()) {
            return $this->privilegeManager->isPrivilegeTargetGranted('TYPO3.Neos:Backend.Module.Management.Workspaces.ManageOwnWorkspaces');
        }

        if ($workspace->isPrivateWorkspace() && $workspace->getOwner() !== $this->getCurrentUser()) {
            return $this->privilegeManager->isPrivilegeTargetGranted('TYPO3.Neos:Backend.Module.Management.Workspaces.ManageAllPrivateWorkspaces');
        }

        return false;
    }

    /**
     * Checks if the current user may transfer ownership of the given workspace
     *
     * In future versions, this logic may be implemented in Neos in a more generic way (for example, by means of an
     * ACL object), but for now, this method exists in order to at least centralize and encapsulate the required logic.
     *
     * @param Workspace $workspace The workspace
     * @return boolean
     */
    public function currentUserCanTransferOwnershipOfWorkspace(Workspace $workspace)
    {
        if ($workspace->isPersonalWorkspace()) {
            return false;
        }

        // The privilege to manage shared workspaces is needed, because regular editors should not change ownerships
        // of their internal workspaces, even if it was technically possible, because they wouldn't be able to change
        // ownership back to themselves.
        return $this->privilegeManager->isPrivilegeTargetGranted('TYPO3.Neos:Backend.Module.Management.Workspaces.ManageInternalWorkspaces');
    }

    /**
     * Returns the default authentication provider name
     *
     * @return string
     * @api
     */
    public function getDefaultAuthenticationProviderName()
    {
        return $this->defaultAuthenticationProviderName;
    }

    /**
     * Signals that the given user has been activated
     *
     * @param User $user The user
     * @return void
     * @Flow\Signal
     * @api
     */
    public function emitUserDeactivated(User $user)
    {
    }

    /**
     * Replaces role identifiers not containing a "." into fully qualified role identifiers from the TYPO3.Neos namespace.
     *
     * @param array $roleIdentifiers
     * @return array
     */
    protected function normalizeRoleIdentifiers(array $roleIdentifiers)
    {
        foreach ($roleIdentifiers as &$roleIdentifier) {
            $roleIdentifier = $this->normalizeRoleIdentifier($roleIdentifier);
        }

        return $roleIdentifiers;
    }

    /**
     * Replaces a role identifier not containing a "." into fully qualified role identifier from the TYPO3.Neos namespace.
     *
     * @param string $roleIdentifier
     * @return string
     * @throws NoSuchRoleException
     */
    protected function normalizeRoleIdentifier($roleIdentifier)
    {
        if (strpos($roleIdentifier, ':') === false) {
            $roleIdentifier = 'TYPO3.Neos:' . $roleIdentifier;
        }
        if (!$this->policyService->hasRole($roleIdentifier)) {
            throw new NoSuchRoleException(sprintf('The role %s does not exist.', $roleIdentifier), 1422540184);
        }

        return $roleIdentifier;
    }

    /**
     * Returns an array with all roles of a user's accounts, including parent roles, the "Everybody" role and the
     * "AuthenticatedUser" role, assuming that the user is logged in.
     *
     * @param User $user The user
     * @return array
     */
    protected function getAllRoles(User $user)
    {
        $roles = array(
            'TYPO3.Flow:Everybody' => $this->policyService->getRole('TYPO3.Flow:Everybody'),
            'TYPO3.Flow:AuthenticatedUser' => $this->policyService->getRole('TYPO3.Flow:AuthenticatedUser')
        );

        /** @var Account $account */
        foreach ($user->getAccounts() as $account) {
            $accountRoles = $account->getRoles();
            /** @var $currentRole Role */
            foreach ($accountRoles as $currentRole) {
                if (!in_array($currentRole, $roles)) {
                    $roles[$currentRole->getIdentifier()] = $currentRole;
                }
                /** @var $currentParentRole Role */
                foreach ($currentRole->getAllParentRoles() as $currentParentRole) {
                    if (!in_array($currentParentRole, $roles)) {
                        $roles[$currentParentRole->getIdentifier()] = $currentParentRole;
                    }
                }
            }
        }

        return $roles;
    }

    /**
     * Creates a personal workspace for the given user's account if it does not exist already.
     *
     * @param User $user The new user to create a workspace for
     * @param Account $account The user's backend account
     * @throws IllegalObjectTypeException
     */
    protected function createPersonalWorkspace(User $user, Account $account)
    {
        $userWorkspaceName = UserUtility::getPersonalWorkspaceNameForUsername($account->getAccountIdentifier());
        $userWorkspace = $this->workspaceRepository->findByIdentifier($userWorkspaceName);
        if ($userWorkspace === null) {
            $liveWorkspace = $this->workspaceRepository->findByIdentifier('live');
            if (!($liveWorkspace instanceof Workspace)) {
                $liveWorkspace = new Workspace('live');
                $liveWorkspace->setTitle('Live');
                $this->workspaceRepository->add($liveWorkspace);
            }

            $userWorkspace = new Workspace($userWorkspaceName, $liveWorkspace, $user);
            $userWorkspace->setTitle((string)$user->getName());
            $this->workspaceRepository->add($userWorkspace);
        }
    }

    /**
     * Removes all personal workspaces of the given user's account if these workspaces exist. Also removes
     * all possibly existing content of these workspaces.
     *
     * @param string $accountIdentifier Identifier of the user's account
     * @return void
     */
    protected function deletePersonalWorkspace($accountIdentifier)
    {
        $userWorkspace = $this->workspaceRepository->findByIdentifier(UserUtility::getPersonalWorkspaceNameForUsername($accountIdentifier));
        if ($userWorkspace instanceof Workspace) {
            $this->publishingService->discardAllNodes($userWorkspace);
            $this->workspaceRepository->remove($userWorkspace);
        }
    }

    /**
     * Removes ownership of all workspaces currently owned by the given user
     *
     * @param User $user The user currently owning workspaces
     * @return void
     */
    protected function removeOwnerFromUsersWorkspaces(User $user)
    {
        /** @var Workspace $workspace */
        foreach ($this->workspaceRepository->findByOwner($user) as $workspace) {
            $workspace->setOwner(null);
            $this->workspaceRepository->update($workspace);
        }
    }
}
namespace TYPO3\Neos\Domain\Service;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * A service for managing users
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class UserService extends UserService_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Aop\AdvicesTrait, \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;

    private $Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array();

    private $Flow_Aop_Proxy_groupedAdviceChains = array();

    private $Flow_Aop_Proxy_methodIsInAdviceMode = array();


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
        if (get_class($this) === 'TYPO3\Neos\Domain\Service\UserService') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Neos\Domain\Service\UserService', $this);
        if ('TYPO3\Neos\Domain\Service\UserService' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    protected function Flow_Aop_Proxy_buildMethodsAndAdvicesArray()
    {
        if (method_exists(get_parent_class(), 'Flow_Aop_Proxy_buildMethodsAndAdvicesArray') && is_callable('parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray')) parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        $objectManager = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager;
        $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array(
            'emitUserCreated' => array(
                'TYPO3\Flow\Aop\Advice\AfterReturningAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AfterReturningAdvice('TYPO3\Flow\SignalSlot\SignalAspect', 'forwardSignalToDispatcher', $objectManager, NULL),
                ),
            ),
            'emitUserDeleted' => array(
                'TYPO3\Flow\Aop\Advice\AfterReturningAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AfterReturningAdvice('TYPO3\Flow\SignalSlot\SignalAspect', 'forwardSignalToDispatcher', $objectManager, NULL),
                ),
            ),
            'emitUserUpdated' => array(
                'TYPO3\Flow\Aop\Advice\AfterReturningAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AfterReturningAdvice('TYPO3\Flow\SignalSlot\SignalAspect', 'forwardSignalToDispatcher', $objectManager, NULL),
                ),
            ),
            'emitRolesAdded' => array(
                'TYPO3\Flow\Aop\Advice\AfterReturningAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AfterReturningAdvice('TYPO3\Flow\SignalSlot\SignalAspect', 'forwardSignalToDispatcher', $objectManager, NULL),
                ),
            ),
            'emitRolesRemoved' => array(
                'TYPO3\Flow\Aop\Advice\AfterReturningAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AfterReturningAdvice('TYPO3\Flow\SignalSlot\SignalAspect', 'forwardSignalToDispatcher', $objectManager, NULL),
                ),
            ),
            'emitUserActivated' => array(
                'TYPO3\Flow\Aop\Advice\AfterReturningAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AfterReturningAdvice('TYPO3\Flow\SignalSlot\SignalAspect', 'forwardSignalToDispatcher', $objectManager, NULL),
                ),
            ),
            'emitUserDeactivated' => array(
                'TYPO3\Flow\Aop\Advice\AfterReturningAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AfterReturningAdvice('TYPO3\Flow\SignalSlot\SignalAspect', 'forwardSignalToDispatcher', $objectManager, NULL),
                ),
            ),
        );
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
        if (get_class($this) === 'TYPO3\Neos\Domain\Service\UserService') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Neos\Domain\Service\UserService', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
            $result = NULL;
        if (method_exists(get_parent_class(), '__wakeup') && is_callable('parent::__wakeup')) parent::__wakeup();
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __clone()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
    }

    /**
     * Autogenerated Proxy Method
     * @param User $user The created user
     * @return void
     * @\TYPO3\Flow\Annotations\Signal
     */
    public function emitUserCreated(\TYPO3\Neos\Domain\Model\User $user)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserCreated'])) {
            $result = parent::emitUserCreated($user);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserCreated'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['user'] = $user;
            
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Domain\Service\UserService', 'emitUserCreated', $methodArguments);
                $result = $this->Flow_Aop_Proxy_invokeJoinPoint($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

                if (isset($this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['emitUserCreated']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'])) {
                    $advices = $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['emitUserCreated']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'];
                    $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Domain\Service\UserService', 'emitUserCreated', $methodArguments, NULL, $result);
                    foreach ($advices as $advice) {
                        $advice->invoke($joinPoint);
                    }

                    $methodArguments = $joinPoint->getMethodArguments();
                }

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserCreated']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserCreated']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param User $user The created user
     * @return void
     * @\TYPO3\Flow\Annotations\Signal
     */
    public function emitUserDeleted(\TYPO3\Neos\Domain\Model\User $user)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserDeleted'])) {
            $result = parent::emitUserDeleted($user);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserDeleted'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['user'] = $user;
            
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Domain\Service\UserService', 'emitUserDeleted', $methodArguments);
                $result = $this->Flow_Aop_Proxy_invokeJoinPoint($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

                if (isset($this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['emitUserDeleted']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'])) {
                    $advices = $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['emitUserDeleted']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'];
                    $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Domain\Service\UserService', 'emitUserDeleted', $methodArguments, NULL, $result);
                    foreach ($advices as $advice) {
                        $advice->invoke($joinPoint);
                    }

                    $methodArguments = $joinPoint->getMethodArguments();
                }

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserDeleted']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserDeleted']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param User $user The created user
     * @return void
     * @\TYPO3\Flow\Annotations\Signal
     */
    public function emitUserUpdated(\TYPO3\Neos\Domain\Model\User $user)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserUpdated'])) {
            $result = parent::emitUserUpdated($user);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserUpdated'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['user'] = $user;
            
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Domain\Service\UserService', 'emitUserUpdated', $methodArguments);
                $result = $this->Flow_Aop_Proxy_invokeJoinPoint($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

                if (isset($this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['emitUserUpdated']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'])) {
                    $advices = $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['emitUserUpdated']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'];
                    $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Domain\Service\UserService', 'emitUserUpdated', $methodArguments, NULL, $result);
                    foreach ($advices as $advice) {
                        $advice->invoke($joinPoint);
                    }

                    $methodArguments = $joinPoint->getMethodArguments();
                }

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserUpdated']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserUpdated']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Account $account The account
     * @param array<Role> An array of Role objects which have been added for that account
     * @return void
     * @\TYPO3\Flow\Annotations\Signal
     */
    public function emitRolesAdded(\TYPO3\Flow\Security\Account $account, array $roles)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitRolesAdded'])) {
            $result = parent::emitRolesAdded($account, $roles);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['emitRolesAdded'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['account'] = $account;
                $methodArguments['roles'] = $roles;
            
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Domain\Service\UserService', 'emitRolesAdded', $methodArguments);
                $result = $this->Flow_Aop_Proxy_invokeJoinPoint($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

                if (isset($this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['emitRolesAdded']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'])) {
                    $advices = $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['emitRolesAdded']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'];
                    $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Domain\Service\UserService', 'emitRolesAdded', $methodArguments, NULL, $result);
                    foreach ($advices as $advice) {
                        $advice->invoke($joinPoint);
                    }

                    $methodArguments = $joinPoint->getMethodArguments();
                }

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitRolesAdded']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitRolesAdded']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Account $account The account
     * @param array<Role> An array of Role objects which have been removed
     * @return void
     * @\TYPO3\Flow\Annotations\Signal
     */
    public function emitRolesRemoved(\TYPO3\Flow\Security\Account $account, array $roles)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitRolesRemoved'])) {
            $result = parent::emitRolesRemoved($account, $roles);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['emitRolesRemoved'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['account'] = $account;
                $methodArguments['roles'] = $roles;
            
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Domain\Service\UserService', 'emitRolesRemoved', $methodArguments);
                $result = $this->Flow_Aop_Proxy_invokeJoinPoint($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

                if (isset($this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['emitRolesRemoved']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'])) {
                    $advices = $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['emitRolesRemoved']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'];
                    $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Domain\Service\UserService', 'emitRolesRemoved', $methodArguments, NULL, $result);
                    foreach ($advices as $advice) {
                        $advice->invoke($joinPoint);
                    }

                    $methodArguments = $joinPoint->getMethodArguments();
                }

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitRolesRemoved']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitRolesRemoved']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param User $user The user
     * @return void
     * @\TYPO3\Flow\Annotations\Signal
     */
    public function emitUserActivated(\TYPO3\Neos\Domain\Model\User $user)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserActivated'])) {
            $result = parent::emitUserActivated($user);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserActivated'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['user'] = $user;
            
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Domain\Service\UserService', 'emitUserActivated', $methodArguments);
                $result = $this->Flow_Aop_Proxy_invokeJoinPoint($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

                if (isset($this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['emitUserActivated']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'])) {
                    $advices = $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['emitUserActivated']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'];
                    $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Domain\Service\UserService', 'emitUserActivated', $methodArguments, NULL, $result);
                    foreach ($advices as $advice) {
                        $advice->invoke($joinPoint);
                    }

                    $methodArguments = $joinPoint->getMethodArguments();
                }

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserActivated']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserActivated']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param User $user The user
     * @return void
     * @\TYPO3\Flow\Annotations\Signal
     */
    public function emitUserDeactivated(\TYPO3\Neos\Domain\Model\User $user)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserDeactivated'])) {
            $result = parent::emitUserDeactivated($user);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserDeactivated'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['user'] = $user;
            
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Domain\Service\UserService', 'emitUserDeactivated', $methodArguments);
                $result = $this->Flow_Aop_Proxy_invokeJoinPoint($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

                if (isset($this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['emitUserDeactivated']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'])) {
                    $advices = $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['emitUserDeactivated']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'];
                    $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Domain\Service\UserService', 'emitUserDeactivated', $methodArguments, NULL, $result);
                    foreach ($advices as $advice) {
                        $advice->invoke($joinPoint);
                    }

                    $methodArguments = $joinPoint->getMethodArguments();
                }

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserDeactivated']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['emitUserDeactivated']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'defaultAuthenticationProviderName' => 'string',
  'workspaceRepository' => 'TYPO3\\TYPO3CR\\Domain\\Repository\\WorkspaceRepository',
  'publishingService' => 'TYPO3\\Neos\\Service\\PublishingService',
  'partyRepository' => 'TYPO3\\Party\\Domain\\Repository\\PartyRepository',
  'userRepository' => 'TYPO3\\Neos\\Domain\\Repository\\UserRepository',
  'partyService' => 'TYPO3\\Party\\Domain\\Service\\PartyService',
  'accountFactory' => 'TYPO3\\Flow\\Security\\AccountFactory',
  'accountRepository' => 'TYPO3\\Flow\\Security\\AccountRepository',
  'policyService' => 'TYPO3\\Flow\\Security\\Policy\\PolicyService',
  'authenticationManager' => 'TYPO3\\Flow\\Security\\Authentication\\AuthenticationManagerInterface',
  'privilegeManager' => 'TYPO3\\Flow\\Security\\Authorization\\PrivilegeManagerInterface',
  'securityContext' => 'TYPO3\\Flow\\Security\\Context',
  'hashService' => 'TYPO3\\Flow\\Security\\Cryptography\\HashService',
  'now' => 'TYPO3\\Flow\\Utility\\Now',
  'runtimeUserCache' => 'array',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Repository\WorkspaceRepository', 'TYPO3\TYPO3CR\Domain\Repository\WorkspaceRepository', 'workspaceRepository', '2e64c564c983af14b47d0c9ae8992997', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Repository\WorkspaceRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Neos\Service\PublishingService', 'TYPO3\Neos\Service\PublishingService', 'publishingService', '6fcea527449934bf6820d8ecf224e34c', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Neos\Service\PublishingService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Party\Domain\Repository\PartyRepository', 'TYPO3\Party\Domain\Repository\PartyRepository', 'partyRepository', 'ab219dc818a4e84959d27bb17ce67d6e', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Party\Domain\Repository\PartyRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Neos\Domain\Repository\UserRepository', 'TYPO3\Neos\Domain\Repository\UserRepository', 'userRepository', '36e38014fbedd0b2380814688f34a490', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Neos\Domain\Repository\UserRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Party\Domain\Service\PartyService', 'TYPO3\Party\Domain\Service\PartyService', 'partyService', 'e99501a1a4b88e8f885d90045e81e070', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Party\Domain\Service\PartyService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Security\AccountFactory', 'TYPO3\Flow\Security\AccountFactory', 'accountFactory', 'f316baee16699e7bca10b2c9f1eac706', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\AccountFactory'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Security\AccountRepository', 'TYPO3\Flow\Security\AccountRepository', 'accountRepository', 'd68c73088546244eb016f396195a461c', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\AccountRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Security\Policy\PolicyService', 'TYPO3\Flow\Security\Policy\PolicyService', 'policyService', '16231078e783810895dba92e364c25f7', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\Policy\PolicyService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Security\Authentication\AuthenticationManagerInterface', 'TYPO3\Flow\Security\Authentication\AuthenticationProviderManager', 'authenticationManager', 'd4b358f5a262d346229c2bf11ebd0c1d', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\Authentication\AuthenticationManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Security\Authorization\PrivilegeManagerInterface', 'TYPO3\Flow\Security\Authorization\PrivilegeManager', 'privilegeManager', 'e6ac4a39049e0c768364696b9ef49ce5', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\Authorization\PrivilegeManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Security\Context', 'TYPO3\Flow\Security\Context', 'securityContext', '48836470c14129ade5f39e28c4816673', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\Context'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Security\Cryptography\HashService', 'TYPO3\Flow\Security\Cryptography\HashService', 'hashService', 'af606f3838da2ad86bf0ed2ff61be394', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\Cryptography\HashService'); });
        $this->now = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Utility\Now');
        $this->Flow_Injected_Properties = array (
  0 => 'workspaceRepository',
  1 => 'publishingService',
  2 => 'partyRepository',
  3 => 'userRepository',
  4 => 'partyService',
  5 => 'accountFactory',
  6 => 'accountRepository',
  7 => 'policyService',
  8 => 'authenticationManager',
  9 => 'privilegeManager',
  10 => 'securityContext',
  11 => 'hashService',
  12 => 'now',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Neos/Classes/TYPO3/Neos/Domain/Service/UserService.php
#