<?php 
namespace TYPO3\Flow\Persistence\Aspect;

/*
 * This file is part of the TYPO3.Flow package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Aop\JoinPointInterface;
use TYPO3\Flow\Persistence\PersistenceManagerInterface;
use TYPO3\Flow\Reflection\ObjectAccess;
use TYPO3\Flow\Reflection\ReflectionService;
use TYPO3\Flow\Utility\Algorithms;

/**
 * Adds the aspect of persistence magic to relevant objects
 *
 * @Flow\Scope("singleton")
 * @Flow\Aspect
 * @Flow\Introduce("TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect->isEntityOrValueObject", interfaceName="TYPO3\Flow\Persistence\Aspect\PersistenceMagicInterface")
 */
class PersistenceMagicAspect_Original
{
    /**
     * If the extension "igbinary" is installed, use it for increased performance
     *
     * @var boolean
     */
    protected $useIgBinary;

    /**
     * @Flow\Inject
     * @var PersistenceManagerInterface
     */
    protected $persistenceManager;

    /**
     * @Flow\Inject
     * @var ReflectionService
     */
    protected $reflectionService;

    /**
     * @Flow\Pointcut("classAnnotatedWith(TYPO3\Flow\Annotations\Entity) || classAnnotatedWith(Doctrine\ORM\Mapping\Entity)")
     */
    public function isEntity()
    {
    }

    /**
     * @Flow\Pointcut("classAnnotatedWith(TYPO3\Flow\Annotations\ValueObject) && !filter(TYPO3\Flow\Persistence\Aspect\EmbeddedValueObjectPointcutFilter)")
     */
    public function isNonEmbeddedValueObject()
    {
    }

    /**
     * @Flow\Pointcut("TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect->isEntity || TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect->isNonEmbeddedValueObject")
     */
    public function isEntityOrValueObject()
    {
    }

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(length=40)
     * @Flow\Introduce("TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect->isEntityOrValueObject && filter(TYPO3\Flow\Persistence\Doctrine\Mapping\Driver\FlowAnnotationDriver)")
     */
    protected $Persistence_Object_Identifier;

    /**
     * Initializes this aspect
     *
     * @return void
     */
    public function initializeObject()
    {
        $this->useIgBinary = extension_loaded('igbinary');
    }

    /**
     * After returning advice, making sure we have an UUID for each and every entity.
     *
     * @param JoinPointInterface $joinPoint The current join point
     * @return void
     * @Flow\Before("TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect->isEntity && method(.*->(__construct|__clone)()) && filter(TYPO3\Flow\Persistence\Doctrine\Mapping\Driver\FlowAnnotationDriver)")
     */
    public function generateUuid(JoinPointInterface $joinPoint)
    {
        /** @var $proxy PersistenceMagicInterface */
        $proxy = $joinPoint->getProxy();
        ObjectAccess::setProperty($proxy, 'Persistence_Object_Identifier', Algorithms::generateUUID(), true);
        $this->persistenceManager->registerNewObject($proxy);
    }

    /**
     * After returning advice, generates the value hash for the object
     *
     * @param JoinPointInterface $joinPoint The current join point
     * @return void
     * @Flow\After("TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect->isNonEmbeddedValueObject && method(.*->__construct()) && filter(TYPO3\Flow\Persistence\Doctrine\Mapping\Driver\FlowAnnotationDriver)")
     */
    public function generateValueHash(JoinPointInterface $joinPoint)
    {
        $proxy = $joinPoint->getProxy();
        $proxyClassName = get_class($proxy);
        $hashSourceParts = [];

        $classSchema = $this->reflectionService->getClassSchema($proxyClassName);
        foreach ($classSchema->getProperties() as $property => $propertySchema) {
            // Currently, private properties are transient. Should this behaviour change, they need to be included
            // in the value hash generation
            if ($classSchema->isPropertyTransient($property)
                || $this->reflectionService->isPropertyPrivate($proxyClassName, $property)) {
                continue;
            }

            $propertyValue = ObjectAccess::getProperty($proxy, $property, true);

            if (is_object($propertyValue) === true) {
                // The persistence manager will return NULL if the given object is unknown to persistence
                $propertyValue = ($this->persistenceManager->getIdentifierByObject($propertyValue)) ?: $propertyValue;
            }

            $hashSourceParts[$property] = $propertyValue;
        }

        ksort($hashSourceParts);

        $hashSourceParts['__class_name__'] = $proxyClassName;
        $serializedSource = ($this->useIgBinary === true) ? igbinary_serialize($hashSourceParts) : serialize($hashSourceParts);

        $proxy = $joinPoint->getProxy();
        ObjectAccess::setProperty($proxy, 'Persistence_Object_Identifier', sha1($serializedSource), true);
    }

    /**
     * Mark object as cloned after cloning.
     *
     * Note: this is not used by anything in the Flow base distribution,
     * but might be needed by custom backends (like TYPO3.CouchDB).
     *
     * @param JoinPointInterface $joinPoint
     * @return void
     * @Flow\AfterReturning("TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect->isEntityOrValueObject && method(.*->__clone())")
     */
    public function cloneObject(JoinPointInterface $joinPoint)
    {
        $joinPoint->getProxy()->Flow_Persistence_clone = true;
    }
}
namespace TYPO3\Flow\Persistence\Aspect;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * Adds the aspect of persistence magic to relevant objects
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 * @\TYPO3\Flow\Annotations\Aspect
 * @\TYPO3\Flow\Annotations\Introduce(pointcutExpression="TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect->isEntityOrValueObject", interfaceName="TYPO3\Flow\Persistence\Aspect\PersistenceMagicInterface")
 */
class PersistenceMagicAspect extends PersistenceMagicAspect_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        if (get_class($this) === 'TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect', $this);
        if ('TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }

        $isSameClass = get_class($this) === 'TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect';
        if ($isSameClass) {
            $this->initializeObject(1);
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'useIgBinary' => 'boolean',
  'persistenceManager' => 'TYPO3\\Flow\\Persistence\\PersistenceManagerInterface',
  'reflectionService' => 'TYPO3\\Flow\\Reflection\\ReflectionService',
  'Persistence_Object_Identifier' => 'string',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {
        if (get_class($this) === 'TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
            $result = NULL;

        $isSameClass = get_class($this) === 'TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect';
        $classParents = class_parents($this);
        $classImplements = class_implements($this);
        $isClassProxy = array_search('TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect', $classParents) !== FALSE && array_search('Doctrine\ORM\Proxy\Proxy', $classImplements) !== FALSE;

        if ($isSameClass || $isClassProxy) {
            $this->initializeObject(2);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Persistence\PersistenceManagerInterface', 'TYPO3\Flow\Persistence\Doctrine\PersistenceManager', 'persistenceManager', 'f1bc82ad47156d95485678e33f27c110', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Reflection\ReflectionService', 'TYPO3\Flow\Reflection\ReflectionService', 'reflectionService', '921ad637f16d2059757a908fceaf7076', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Reflection\ReflectionService'); });
        $this->Flow_Injected_Properties = array (
  0 => 'persistenceManager',
  1 => 'reflectionService',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Framework/TYPO3.Flow/Classes/TYPO3/Flow/Persistence/Aspect/PersistenceMagicAspect.php
#