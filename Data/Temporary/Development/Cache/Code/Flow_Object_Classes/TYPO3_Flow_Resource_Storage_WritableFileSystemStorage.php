<?php 
namespace TYPO3\Flow\Resource\Storage;

/*
 * This file is part of the TYPO3.Flow package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Resource\Resource as PersistentResource;
use TYPO3\Flow\Resource\Storage\Exception as StorageException;
use TYPO3\Flow\Utility\Files;

/**
 * A resource storage based on the (local) file system
 */
class WritableFileSystemStorage_Original extends FileSystemStorage implements WritableStorageInterface
{
    /**
     * Initializes this resource storage
     *
     * @return void
     * @throws Exception
     */
    public function initializeObject()
    {
        if (!is_writable($this->path)) {
            Files::createDirectoryRecursively($this->path);
        }
        if (!is_dir($this->path) && !is_link($this->path)) {
            throw new StorageException('The directory "' . $this->path . '" which was configured as a resource storage does not exist.', 1361533189);
        }
        if (!is_writable($this->path)) {
            throw new StorageException('The directory "' . $this->path . '" which was configured as a resource storage is not writable.', 1361533190);
        }
    }

    /**
     * Imports a resource (file) from the given URI or PHP resource stream into this storage.
     *
     * On a successful import this method returns a Resource object representing the newly imported persistent resource.
     *
     * @param string | resource $source The URI (or local path and filename) or the PHP resource stream to import the resource from
     * @param string $collectionName Name of the collection the new Resource belongs to
     * @throws StorageException
     * @return Resource A resource object representing the imported resource
     */
    public function importResource($source, $collectionName)
    {
        $temporaryTargetPathAndFilename = $this->environment->getPathToTemporaryDirectory() . uniqid('TYPO3_Flow_ResourceImport_');

        if (is_resource($source)) {
            try {
                $target = fopen($temporaryTargetPathAndFilename, 'wb');
                stream_copy_to_stream($source, $target);
                fclose($target);
            } catch (\Exception $exception) {
                throw new StorageException(sprintf('Could import the content stream to temporary file "%s".', $temporaryTargetPathAndFilename), 1380880079);
            }
        } else {
            try {
                copy($source, $temporaryTargetPathAndFilename);
            } catch (\Exception $exception) {
                throw new StorageException(sprintf('Could not copy the file from "%s" to temporary file "%s".', $source, $temporaryTargetPathAndFilename), 1375198876);
            }
        }

        return $this->importTemporaryFile($temporaryTargetPathAndFilename, $collectionName);
    }

    /**
     * Imports a resource from the given string content into this storage.
     *
     * On a successful import this method returns a Resource object representing the newly
     * imported persistent resource.
     *
     * The specified filename will be used when presenting the resource to a user. Its file extension is
     * important because the resource management will derive the IANA Media Type from it.
     *
     * @param string $content The actual content to import
     * @param string $collectionName Name of the collection the new Resource belongs to
     * @return PersistentResource A resource object representing the imported resource
     * @throws StorageException
     */
    public function importResourceFromContent($content, $collectionName)
    {
        $temporaryTargetPathAndFilename = $this->environment->getPathToTemporaryDirectory() . uniqid('TYPO3_Flow_ResourceImport_');
        try {
            file_put_contents($temporaryTargetPathAndFilename, $content);
        } catch (\Exception $exception) {
            throw new StorageException(sprintf('Could import the content stream to temporary file "%s".', $temporaryTargetPathAndFilename), 1381156098);
        }

        return $this->importTemporaryFile($temporaryTargetPathAndFilename, $collectionName);
    }

    /**
     * Deletes the storage data related to the given Resource object
     *
     * @param PersistentResource $resource The Resource to delete the storage data of
     * @return boolean TRUE if removal was successful
     */
    public function deleteResource(PersistentResource $resource)
    {
        $pathAndFilename = $this->getStoragePathAndFilenameByHash($resource->getSha1());
        if (!file_exists($pathAndFilename)) {
            return true;
        }
        if (unlink($pathAndFilename) === false) {
            return false;
        }
        Files::removeEmptyDirectoriesOnPath(dirname($pathAndFilename));
        return true;
    }

    /**
     * Imports the given temporary file into the storage and creates the new resource object.
     *
     * Note: the temporary file is (re-)moved by this method.
     *
     * @param string $temporaryPathAndFileName
     * @param string $collectionName
     * @return PersistentResource
     * @throws StorageException
     */
    protected function importTemporaryFile($temporaryPathAndFileName, $collectionName)
    {
        $this->fixFilePermissions($temporaryPathAndFileName);
        $sha1Hash = sha1_file($temporaryPathAndFileName);
        $targetPathAndFilename = $this->getStoragePathAndFilenameByHash($sha1Hash);

        if (!is_file($targetPathAndFilename)) {
            $this->moveTemporaryFileToFinalDestination($temporaryPathAndFileName, $targetPathAndFilename);
        } else {
            unlink($temporaryPathAndFileName);
        }

        $resource = new PersistentResource();
        $resource->setFileSize(filesize($targetPathAndFilename));
        $resource->setCollectionName($collectionName);
        $resource->setSha1($sha1Hash);
        $resource->setMd5(md5_file($targetPathAndFilename));

        return $resource;
    }

    /**
     * Move a temporary file to the final destination, creating missing path segments on the way.
     *
     * @param string $temporaryFile
     * @param string $finalTargetPathAndFilename
     * @return void
     * @throws StorageException
     */
    protected function moveTemporaryFileToFinalDestination($temporaryFile, $finalTargetPathAndFilename)
    {
        if (!file_exists(dirname($finalTargetPathAndFilename))) {
            Files::createDirectoryRecursively(dirname($finalTargetPathAndFilename));
        }
        if (copy($temporaryFile, $finalTargetPathAndFilename) === false) {
            throw new StorageException(sprintf('The temporary file of the file import could not be moved to the final target "%s".', $finalTargetPathAndFilename), 1381156103);
        }
        unlink($temporaryFile);

        $this->fixFilePermissions($finalTargetPathAndFilename);
    }

    /**
     * Fixes the permissions as needed for Flow to run fine in web and cli context.
     *
     * @param string $pathAndFilename
     * @return void
     */
    protected function fixFilePermissions($pathAndFilename)
    {
        @chmod($pathAndFilename, 0666 ^ umask());
    }
}
namespace TYPO3\Flow\Resource\Storage;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * A resource storage based on the (local) file system
 */
class WritableFileSystemStorage extends WritableFileSystemStorage_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     * @param string $name Name of this storage instance, according to the resource settings
     * @param array $options Options for this storage
     * @throws Exception
     */
    public function __construct()
    {
        $arguments = func_get_args();
        if (!array_key_exists(0, $arguments)) throw new \TYPO3\Flow\Object\Exception\UnresolvedDependenciesException('Missing required constructor argument $name in class ' . __CLASS__ . '. Note that constructor injection is only support for objects of scope singleton (and this is not a singleton) – for other scopes you must pass each required argument to the constructor yourself.', 1296143788);
        call_user_func_array('parent::__construct', $arguments);
        if ('TYPO3\Flow\Resource\Storage\WritableFileSystemStorage' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }

        $isSameClass = get_class($this) === 'TYPO3\Flow\Resource\Storage\WritableFileSystemStorage';
        if ($isSameClass) {
            $this->initializeObject(1);
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'name' => 'string',
  'path' => 'string',
  'environment' => 'TYPO3\\Flow\\Utility\\Environment',
  'resourceManager' => 'TYPO3\\Flow\\Resource\\ResourceManager',
  'resourceRepository' => 'TYPO3\\Flow\\Resource\\ResourceRepository',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
            $result = NULL;

        $isSameClass = get_class($this) === 'TYPO3\Flow\Resource\Storage\WritableFileSystemStorage';
        $classParents = class_parents($this);
        $classImplements = class_implements($this);
        $isClassProxy = array_search('TYPO3\Flow\Resource\Storage\WritableFileSystemStorage', $classParents) !== FALSE && array_search('Doctrine\ORM\Proxy\Proxy', $classImplements) !== FALSE;

        if ($isSameClass || $isClassProxy) {
            $this->initializeObject(2);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Utility\Environment', 'TYPO3\Flow\Utility\Environment', 'environment', 'd7473831479e64d04a54de9aedcdc371', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Utility\Environment'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Resource\ResourceManager', 'TYPO3\Flow\Resource\ResourceManager', 'resourceManager', '3b3239258e396ed88334e6f7199a1678', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Resource\ResourceManager'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Resource\ResourceRepository', 'TYPO3\Flow\Resource\ResourceRepository', 'resourceRepository', 'bb0e0fb67bce65073a482bd8ef9ffa4a', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Resource\ResourceRepository'); });
        $this->Flow_Injected_Properties = array (
  0 => 'environment',
  1 => 'resourceManager',
  2 => 'resourceRepository',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Framework/TYPO3.Flow/Classes/TYPO3/Flow/Resource/Storage/WritableFileSystemStorage.php
#