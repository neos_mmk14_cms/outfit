<?php 
namespace TYPO3\Neos\Controller\Module\Administration;

/*
 * This file is part of the TYPO3.Neos package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Error\Message;
use TYPO3\Flow\I18n\EelHelper\TranslationHelper;
use TYPO3\Flow\Property\PropertyMappingConfiguration;
use TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter;
use TYPO3\Flow\Security\Account;
use TYPO3\Flow\Security\Authorization\PrivilegeManagerInterface;
use TYPO3\Flow\Security\Policy\PolicyService;
use TYPO3\Neos\Controller\Module\AbstractModuleController;
use TYPO3\Neos\Domain\Model\User;
use TYPO3\Neos\Domain\Service\UserService;
use TYPO3\Party\Domain\Model\ElectronicAddress;

/**
 * The Neos User Admin module controller that allows for managing Neos users
 */
class UsersController_Original extends AbstractModuleController
{
    /**
     * @Flow\Inject
     * @var PrivilegeManagerInterface
     */
    protected $privilegeManager;

    /**
     * @Flow\Inject
     * @var PolicyService
     */
    protected $policyService;

    /**
     * @Flow\Inject
     * @var UserService
     */
    protected $userService;

    /**
     * @var User
     */
    protected $currentUser;

    /**
     * @return void
     */
    protected function initializeAction()
    {
        parent::initializeAction();
        $translationHelper = new TranslationHelper();
        $this->setTitle($translationHelper->translate($this->moduleConfiguration['label']) . ' :: ' . $translationHelper->translate(str_replace('label', 'action.', $this->moduleConfiguration['label']) . $this->request->getControllerActionName()));
        if ($this->arguments->hasArgument('user')) {
            $propertyMappingConfigurationForUser = $this->arguments->getArgument('user')->getPropertyMappingConfiguration();
            $propertyMappingConfigurationForUserName = $propertyMappingConfigurationForUser->forProperty('user.name');
            $propertyMappingConfigurationForPrimaryAccount = $propertyMappingConfigurationForUser->forProperty('user.primaryAccount');
            $propertyMappingConfigurationForPrimaryAccount->setTypeConverterOption(PersistentObjectConverter::class, PersistentObjectConverter::CONFIGURATION_TARGET_TYPE, Account::class);
            /** @var PropertyMappingConfiguration $propertyMappingConfiguration */
            foreach (array($propertyMappingConfigurationForUser, $propertyMappingConfigurationForUserName, $propertyMappingConfigurationForPrimaryAccount) as $propertyMappingConfiguration) {
                $propertyMappingConfiguration->setTypeConverterOption(PersistentObjectConverter::class, PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, true);
                $propertyMappingConfiguration->setTypeConverterOption(PersistentObjectConverter::class, PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED, true);
            }
        }
        $this->currentUser = $this->userService->getCurrentUser();
    }

    /**
     * Shows a list of all users
     *
     * @return void
     */
    public function indexAction()
    {
        $this->view->assignMultiple(array(
            'currentUser' => $this->currentUser,
            'users' => $this->userService->getUsers()
        ));
    }

    /**
     * Shows details for the specified user
     *
     * @param User $user
     * @return void
     */
    public function showAction(User $user)
    {
        $this->view->assignMultiple(array(
            'currentUser' => $this->currentUser,
            'user' => $user
        ));
    }

    /**
     * Renders a form for creating a new user
     *
     * @param User $user
     * @return void
     */
    public function newAction(User $user = null)
    {
        $this->view->assignMultiple(array(
            'currentUser' => $this->currentUser,
            'user' => $user,
            'roles' => $this->policyService->getRoles()
        ));
    }

    /**
     * Create a new user
     *
     * @param string $username The user name (ie. account identifier) of the new user
     * @param array $password Expects an array in the format array('<password>', '<password confirmation>')
     * @param User $user The user to create
     * @param array $roleIdentifiers A list of roles (role identifiers) to assign to the new user
     * @Flow\Validate(argumentName="username", type="\TYPO3\Flow\Validation\Validator\NotEmptyValidator")
     * @Flow\Validate(argumentName="username", type="\TYPO3\Neos\Validation\Validator\UserDoesNotExistValidator")
     * @Flow\Validate(argumentName="password", type="\TYPO3\Neos\Validation\Validator\PasswordValidator", options={ "allowEmpty"=0, "minimum"=1, "maximum"=255 })
     * @return void
     */
    public function createAction($username, array $password, User $user, array $roleIdentifiers)
    {
        $this->userService->addUser($username, $password[0], $user, $roleIdentifiers);
        $this->addFlashMessage('The user "%s" has been created.', 'User created', Message::SEVERITY_OK, array(htmlspecialchars($username)), 1416225561);
        $this->redirect('index');
    }

    /**
     * Edit an existing user
     *
     * @param User $user
     * @return void
     */
    public function editAction(User $user)
    {
        $this->assignElectronicAddressOptions();

        $this->view->assignMultiple(array(
            'user' => $user,
            'availableRoles' => $this->policyService->getRoles()
        ));
    }

    /**
     * Update a given user
     *
     * @param User $user The user to update, including updated data already (name, email address etc)
     * @return void
     */
    public function updateAction(User $user)
    {
        $this->userService->updateUser($user);
        $this->addFlashMessage('The user "%s" has been updated.', 'User updated', Message::SEVERITY_OK, array($user->getName()->getFullName()), 1412374498);
        $this->redirect('index');
    }

    /**
     * Delete the given user
     *
     * @param User $user
     * @return void
     */
    public function deleteAction(User $user)
    {
        if ($user === $this->currentUser) {
            $this->addFlashMessage('You can not delete the currently logged in user', 'Current user can\'t be deleted', Message::SEVERITY_WARNING, array(), 1412374546);
            $this->redirect('index');
        }
        $this->userService->deleteUser($user);
        $this->addFlashMessage('The user "%s" has been deleted.', 'User deleted', Message::SEVERITY_NOTICE, array(htmlspecialchars($user->getName()->getFullName())), 1412374546);
        $this->redirect('index');
    }

    /**
     * Edit the given account
     *
     * @param Account $account
     * @return void
     */
    public function editAccountAction(Account $account)
    {
        $this->view->assignMultiple(array(
            'account' => $account,
            'user' => $this->userService->getUser($account->getAccountIdentifier(), $account->getAuthenticationProviderName()),
            'availableRoles' => $this->policyService->getRoles()
        ));
    }

    /**
     * Update a given account
     *
     * @param Account $account The account to update
     * @param array $roleIdentifiers A possibly updated list of roles for the user's primary account
     * @param array $password Expects an array in the format array('<password>', '<password confirmation>')
     * @Flow\Validate(argumentName="password", type="\TYPO3\Neos\Validation\Validator\PasswordValidator", options={ "allowEmpty"=1, "minimum"=1, "maximum"=255 })
     * @return void
     */
    public function updateAccountAction(Account $account, array $roleIdentifiers, array $password = array())
    {
        $user = $this->userService->getUser($account->getAccountIdentifier(), $account->getAuthenticationProviderName());
        if ($user === $this->currentUser) {
            $roles = array();
            foreach ($roleIdentifiers as $roleIdentifier) {
                $roles[$roleIdentifier] = $this->policyService->getRole($roleIdentifier);
            }
            if (!$this->privilegeManager->isPrivilegeTargetGrantedForRoles($roles, 'TYPO3.Neos:Backend.Module.Administration.Users')) {
                $this->addFlashMessage('With the selected roles the currently logged in user wouldn\'t have access to this module any longer. Please adjust the assigned roles!', 'Don\'t lock yourself out', Message::SEVERITY_WARNING, array(), 1416501197);
                $this->forward('edit', null, null, array('user' => $this->currentUser));
            }
        }
        $password = array_shift($password);
        if (strlen(trim(strval($password))) > 0) {
            $this->userService->setUserPassword($user, $password);
        }

        $this->userService->setRolesForAccount($account, $roleIdentifiers);
        $this->addFlashMessage('The account has been updated.', 'Account updated', Message::SEVERITY_OK);
        $this->redirect('edit', null, null, array('user' => $user));
    }

    /**
     * The add new electronic address action
     *
     * @param User $user
     * @Flow\IgnoreValidation("$user")
     * @return void
     */
    public function newElectronicAddressAction(User $user)
    {
        $this->assignElectronicAddressOptions();
        $this->view->assign('user', $user);
    }

    /**
     * Create an new electronic address
     *
     * @param User $user
     * @param ElectronicAddress $electronicAddress
     * @return void
     */
    public function createElectronicAddressAction(User $user, ElectronicAddress $electronicAddress)
    {
        /** @var User $user */
        $user->addElectronicAddress($electronicAddress);
        $this->userService->updateUser($user);

        $this->addFlashMessage('An electronic address "%s" (%s) has been added.', 'Electronic address added', Message::SEVERITY_OK, array(htmlspecialchars($electronicAddress->getIdentifier()), htmlspecialchars($electronicAddress->getType())), 1412374814);
        $this->redirect('edit', null, null, array('user' => $user));
    }

    /**
     * Delete an electronic address action
     *
     * @param User $user
     * @param ElectronicAddress $electronicAddress
     * @return void
     */
    public function deleteElectronicAddressAction(User $user, ElectronicAddress $electronicAddress)
    {
        $user->removeElectronicAddress($electronicAddress);
        $this->userService->updateUser($user);

        $this->addFlashMessage('The electronic address "%s" (%s) has been deleted for "%s".', 'Electronic address removed', Message::SEVERITY_NOTICE, array(htmlspecialchars($electronicAddress->getIdentifier()), htmlspecialchars($electronicAddress->getType()), htmlspecialchars($user->getName())), 1412374678);
        $this->redirect('edit', null, null, array('user' => $user));
    }

    /**
     *  @return void
     */
    protected function assignElectronicAddressOptions()
    {
        $electronicAddress = new ElectronicAddress();
        $electronicAddressTypes = array();
        foreach ($electronicAddress->getAvailableElectronicAddressTypes() as $type) {
            $electronicAddressTypes[$type] = $type;
        }
        $electronicAddressUsageTypes = array();
        $translationHelper = new TranslationHelper();
        foreach ($electronicAddress->getAvailableUsageTypes() as $type) {
            $electronicAddressUsageTypes[$type] = $translationHelper->translate('users.electronicAddress.usage.type.' . $type, $type, [], 'Modules', 'TYPO3.Neos');
        }
        array_unshift($electronicAddressUsageTypes, '');
        $this->view->assignMultiple(array(
            'electronicAddressTypes' => $electronicAddressTypes,
            'electronicAddressUsageTypes' => $electronicAddressUsageTypes
        ));
    }
}
namespace TYPO3\Neos\Controller\Module\Administration;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * The Neos User Admin module controller that allows for managing Neos users
 */
class UsersController extends UsersController_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Aop\AdvicesTrait, \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;

    private $Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array();

    private $Flow_Aop_Proxy_groupedAdviceChains = array();

    private $Flow_Aop_Proxy_methodIsInAdviceMode = array();


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
        if ('TYPO3\Neos\Controller\Module\Administration\UsersController' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }

        $isSameClass = get_class($this) === 'TYPO3\Neos\Controller\Module\Administration\UsersController';
        if ($isSameClass) {
            $this->initializeObject(1);
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    protected function Flow_Aop_Proxy_buildMethodsAndAdvicesArray()
    {
        if (method_exists(get_parent_class(), 'Flow_Aop_Proxy_buildMethodsAndAdvicesArray') && is_callable('parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray')) parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        $objectManager = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager;
        $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array(
            'indexAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'showAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'newAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'createAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'editAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'updateAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'deleteAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'editAccountAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'updateAccountAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'newElectronicAddressAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'createElectronicAddressAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'deleteElectronicAddressAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
        );
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
            $result = NULL;
        if (method_exists(get_parent_class(), '__wakeup') && is_callable('parent::__wakeup')) parent::__wakeup();

        $isSameClass = get_class($this) === 'TYPO3\Neos\Controller\Module\Administration\UsersController';
        $classParents = class_parents($this);
        $classImplements = class_implements($this);
        $isClassProxy = array_search('TYPO3\Neos\Controller\Module\Administration\UsersController', $classParents) !== FALSE && array_search('Doctrine\ORM\Proxy\Proxy', $classImplements) !== FALSE;

        if ($isSameClass || $isClassProxy) {
            $this->initializeObject(2);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __clone()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
    }

    /**
     * Autogenerated Proxy Method
     * @return void
     */
    public function indexAction()
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction'])) {
            $result = parent::indexAction();

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('indexAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\UsersController', 'indexAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param User $user
     * @return void
     */
    public function showAction(\TYPO3\Neos\Domain\Model\User $user)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['showAction'])) {
            $result = parent::showAction($user);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['showAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['user'] = $user;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('showAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\UsersController', 'showAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['showAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['showAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param User $user
     * @return void
     */
    public function newAction(\TYPO3\Neos\Domain\Model\User $user = NULL)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['newAction'])) {
            $result = parent::newAction($user);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['newAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['user'] = $user;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('newAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\UsersController', 'newAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['newAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['newAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param string $username The user name (ie. account identifier) of the new user
     * @param array $password Expects an array in the format array('<password>', '<password confirmation>')
     * @param User $user The user to create
     * @param array $roleIdentifiers A list of roles (role identifiers) to assign to the new user
     * @return void
     * @\TYPO3\Flow\Annotations\Validate(type="\TYPO3\Flow\Validation\Validator\NotEmptyValidator", argumentName="username")
     * @\TYPO3\Flow\Annotations\Validate(type="\TYPO3\Neos\Validation\Validator\UserDoesNotExistValidator", argumentName="username")
     * @\TYPO3\Flow\Annotations\Validate(type="\TYPO3\Neos\Validation\Validator\PasswordValidator", options={ "allowEmpty"=0, "minimum"=1, "maximum"=255 }, argumentName="password")
     */
    public function createAction($username, array $password, \TYPO3\Neos\Domain\Model\User $user, array $roleIdentifiers)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createAction'])) {
            $result = parent::createAction($username, $password, $user, $roleIdentifiers);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['createAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['username'] = $username;
                $methodArguments['password'] = $password;
                $methodArguments['user'] = $user;
                $methodArguments['roleIdentifiers'] = $roleIdentifiers;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('createAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\UsersController', 'createAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param User $user
     * @return void
     */
    public function editAction(\TYPO3\Neos\Domain\Model\User $user)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['editAction'])) {
            $result = parent::editAction($user);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['editAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['user'] = $user;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('editAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\UsersController', 'editAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['editAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['editAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param User $user The user to update, including updated data already (name, email address etc)
     * @return void
     */
    public function updateAction(\TYPO3\Neos\Domain\Model\User $user)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAction'])) {
            $result = parent::updateAction($user);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['user'] = $user;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('updateAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\UsersController', 'updateAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param User $user
     * @return void
     */
    public function deleteAction(\TYPO3\Neos\Domain\Model\User $user)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAction'])) {
            $result = parent::deleteAction($user);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['user'] = $user;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('deleteAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\UsersController', 'deleteAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Account $account
     * @return void
     */
    public function editAccountAction(\TYPO3\Flow\Security\Account $account)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['editAccountAction'])) {
            $result = parent::editAccountAction($account);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['editAccountAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['account'] = $account;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('editAccountAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\UsersController', 'editAccountAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['editAccountAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['editAccountAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Account $account The account to update
     * @param array $roleIdentifiers A possibly updated list of roles for the user's primary account
     * @param array $password Expects an array in the format array('<password>', '<password confirmation>')
     * @return void
     * @\TYPO3\Flow\Annotations\Validate(type="\TYPO3\Neos\Validation\Validator\PasswordValidator", options={ "allowEmpty"=1, "minimum"=1, "maximum"=255 }, argumentName="password")
     */
    public function updateAccountAction(\TYPO3\Flow\Security\Account $account, array $roleIdentifiers, array $password = array())
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAccountAction'])) {
            $result = parent::updateAccountAction($account, $roleIdentifiers, $password);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAccountAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['account'] = $account;
                $methodArguments['roleIdentifiers'] = $roleIdentifiers;
                $methodArguments['password'] = $password;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('updateAccountAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\UsersController', 'updateAccountAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAccountAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAccountAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param User $user
     * @return void
     * @\TYPO3\Flow\Annotations\IgnoreValidation(argumentName="user")
     */
    public function newElectronicAddressAction(\TYPO3\Neos\Domain\Model\User $user)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['newElectronicAddressAction'])) {
            $result = parent::newElectronicAddressAction($user);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['newElectronicAddressAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['user'] = $user;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('newElectronicAddressAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\UsersController', 'newElectronicAddressAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['newElectronicAddressAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['newElectronicAddressAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param User $user
     * @param ElectronicAddress $electronicAddress
     * @return void
     */
    public function createElectronicAddressAction(\TYPO3\Neos\Domain\Model\User $user, \TYPO3\Party\Domain\Model\ElectronicAddress $electronicAddress)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createElectronicAddressAction'])) {
            $result = parent::createElectronicAddressAction($user, $electronicAddress);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['createElectronicAddressAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['user'] = $user;
                $methodArguments['electronicAddress'] = $electronicAddress;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('createElectronicAddressAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\UsersController', 'createElectronicAddressAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createElectronicAddressAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createElectronicAddressAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param User $user
     * @param ElectronicAddress $electronicAddress
     * @return void
     */
    public function deleteElectronicAddressAction(\TYPO3\Neos\Domain\Model\User $user, \TYPO3\Party\Domain\Model\ElectronicAddress $electronicAddress)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteElectronicAddressAction'])) {
            $result = parent::deleteElectronicAddressAction($user, $electronicAddress);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteElectronicAddressAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['user'] = $user;
                $methodArguments['electronicAddress'] = $electronicAddress;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('deleteElectronicAddressAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\UsersController', 'deleteElectronicAddressAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteElectronicAddressAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteElectronicAddressAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'privilegeManager' => 'TYPO3\\Flow\\Security\\Authorization\\PrivilegeManagerInterface',
  'policyService' => 'TYPO3\\Flow\\Security\\Policy\\PolicyService',
  'userService' => 'TYPO3\\Neos\\Domain\\Service\\UserService',
  'currentUser' => 'TYPO3\\Neos\\Domain\\Model\\User',
  'moduleConfiguration' => 'array',
  'objectManager' => 'TYPO3\\Flow\\Object\\ObjectManagerInterface',
  'reflectionService' => 'TYPO3\\Flow\\Reflection\\ReflectionService',
  'mvcPropertyMappingConfigurationService' => 'TYPO3\\Flow\\Mvc\\Controller\\MvcPropertyMappingConfigurationService',
  'viewConfigurationManager' => 'TYPO3\\Flow\\Mvc\\ViewConfigurationManager',
  'view' => 'TYPO3\\Flow\\Mvc\\View\\ViewInterface',
  'viewObjectNamePattern' => 'string',
  'viewFormatToObjectNameMap' => 'array',
  'defaultViewObjectName' => 'string',
  'actionMethodName' => 'string',
  'errorMethodName' => 'string',
  'settings' => 'array',
  'systemLogger' => 'TYPO3\\Flow\\Log\\SystemLoggerInterface',
  'uriBuilder' => 'TYPO3\\Flow\\Mvc\\Routing\\UriBuilder',
  'validatorResolver' => 'TYPO3\\Flow\\Validation\\ValidatorResolver',
  'request' => 'TYPO3\\Flow\\Mvc\\ActionRequest',
  'response' => 'TYPO3\\Flow\\Http\\Response',
  'arguments' => 'TYPO3\\Flow\\Mvc\\Controller\\Arguments',
  'controllerContext' => 'TYPO3\\Flow\\Mvc\\Controller\\ControllerContext',
  'flashMessageContainer' => 'TYPO3\\Flow\\Mvc\\FlashMessageContainer',
  'persistenceManager' => 'TYPO3\\Flow\\Persistence\\PersistenceManagerInterface',
  'supportedMediaTypes' => 'array',
  '_localizationService' => '\\TYPO3\\Flow\\I18n\\Service',
  '_userService' => '\\TYPO3\\Neos\\Service\\UserService',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->injectSettings(\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get(\TYPO3\Flow\Configuration\ConfigurationManager::class)->getConfiguration('Settings', 'TYPO3.Neos'));
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Security\Authorization\PrivilegeManagerInterface', 'TYPO3\Flow\Security\Authorization\PrivilegeManager', 'privilegeManager', 'e6ac4a39049e0c768364696b9ef49ce5', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\Authorization\PrivilegeManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Security\Policy\PolicyService', 'TYPO3\Flow\Security\Policy\PolicyService', 'policyService', '16231078e783810895dba92e364c25f7', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\Policy\PolicyService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Neos\Domain\Service\UserService', 'TYPO3\Neos\Domain\Service\UserService', 'userService', 'c16c69de7cee1c632e68b46dd677eeac', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Neos\Domain\Service\UserService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Object\ObjectManagerInterface', 'TYPO3\Flow\Object\ObjectManager', 'objectManager', '0c3c44be7be16f2a287f1fb2d068dde4', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Object\ObjectManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Reflection\ReflectionService', 'TYPO3\Flow\Reflection\ReflectionService', 'reflectionService', '921ad637f16d2059757a908fceaf7076', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Reflection\ReflectionService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService', 'TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService', 'mvcPropertyMappingConfigurationService', '35acb49fbe78f28099d45aa647797c83', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\ViewConfigurationManager', 'TYPO3\Flow\Mvc\ViewConfigurationManager', 'viewConfigurationManager', '5a345bfd515fdb9f0c97080ff13c7079', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\ViewConfigurationManager'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Log\SystemLoggerInterface', '', 'systemLogger', '6d57d95a1c3cd7528e3e6ea15012dac8', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Log\SystemLoggerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Validation\ValidatorResolver', 'TYPO3\Flow\Validation\ValidatorResolver', 'validatorResolver', 'b457db29305ddeae13b61d92da000ca0', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Validation\ValidatorResolver'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\FlashMessageContainer', 'TYPO3\Flow\Mvc\FlashMessageContainer', 'flashMessageContainer', 'e4fd26f8afd3994317304b563b2a9561', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\FlashMessageContainer'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Persistence\PersistenceManagerInterface', 'TYPO3\Flow\Persistence\Doctrine\PersistenceManager', 'persistenceManager', 'f1bc82ad47156d95485678e33f27c110', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\I18n\Service', 'TYPO3\Flow\I18n\Service', '_localizationService', 'd147918505b040be63714e111bab34f3', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\I18n\Service'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Neos\Service\UserService', 'TYPO3\Neos\Service\UserService', '_userService', 'bede53034a0bcd605fa08b132fe980ca', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Neos\Service\UserService'); });
        $this->Flow_Injected_Properties = array (
  0 => 'settings',
  1 => 'privilegeManager',
  2 => 'policyService',
  3 => 'userService',
  4 => 'objectManager',
  5 => 'reflectionService',
  6 => 'mvcPropertyMappingConfigurationService',
  7 => 'viewConfigurationManager',
  8 => 'systemLogger',
  9 => 'validatorResolver',
  10 => 'flashMessageContainer',
  11 => 'persistenceManager',
  12 => '_localizationService',
  13 => '_userService',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Neos/Classes/TYPO3/Neos/Controller/Module/Administration/UsersController.php
#