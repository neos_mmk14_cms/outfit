<?php 
namespace TYPO3\Setup\ViewHelpers\Widget\Controller;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "TYPO3.Setup".           *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\DBAL\Platforms\MySqlPlatform;
use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Configuration\ConfigurationManager;

/**
 * Controller for the DatabaseSelector Fluid Widget
 */
class DatabaseSelectorController_Original extends \TYPO3\Fluid\Core\Widget\AbstractWidgetController {

	/**
	 * @Flow\Inject
	 * @var ConfigurationManager
	 */
	protected $configurationManager;

	/**
	 * @return void
	 */
	public function indexAction() {
		$this->view->assign('driverDropdownFieldId', $this->widgetConfiguration['driverDropdownFieldId']);
		$this->view->assign('userFieldId', $this->widgetConfiguration['userFieldId']);
		$this->view->assign('passwordFieldId', $this->widgetConfiguration['passwordFieldId']);
		$this->view->assign('hostFieldId', $this->widgetConfiguration['hostFieldId']);
		$this->view->assign('dbNameTextFieldId', $this->widgetConfiguration['dbNameTextFieldId']);
		$this->view->assign('dbNameDropdownFieldId', $this->widgetConfiguration['dbNameDropdownFieldId']);
		$this->view->assign('statusContainerId', $this->widgetConfiguration['statusContainerId']);
		$this->view->assign('metadataStatusContainerId', $this->widgetConfiguration['metadataStatusContainerId']);
	}

	/**
	 * @param string $driver
	 * @param string $user
	 * @param string $password
	 * @param string $host
	 * @return string
	 */
	public function checkConnectionAction($driver, $user, $password, $host) {
		$this->response->setHeader('Content-Type', 'application/json');
		$connectionSettings = $this->buildConnectionSettingsArray($driver, $user, $password, $host);
		try {
			$connection = $this->getConnectionAndConnect($connectionSettings);
			$databases = $connection->getSchemaManager()->listDatabases();
			$result = array('success' => TRUE, 'databases' => $databases);
		} catch (\PDOException $exception) {
			$result = array('success' => FALSE, 'errorMessage' => $exception->getMessage(), 'errorCode' => $exception->getCode());
		} catch (\Doctrine\DBAL\DBALException $exception) {
			$result = array('success' => FALSE, 'errorMessage' => $exception->getMessage(), 'errorCode' => $exception->getCode());
		} catch (\Exception $exception) {
			$result = array('success' => FALSE, 'errorMessage' => 'Unexpected exception (check logs)', 'errorCode' => $exception->getCode());
		}
		return json_encode($result);
	}

	/**
	 * This fetches information about the database provided, in particular the charset being used.
	 * Depending on whether it is utf8 or not, the (JSON-) response is layed out accordingly.
	 *
	 * @param string $driver
	 * @param string $user
	 * @param string $password
	 * @param string $host
	 * @param string $databaseName
	 * @return string
	 */
	public function getMetadataAction($driver, $user, $password, $host, $databaseName) {
		$this->response->setHeader('Content-Type', 'application/json');
		$connectionSettings = $this->buildConnectionSettingsArray($driver, $user, $password, $host);
		$connectionSettings['dbname'] = $databaseName;
		try {
			$connection = $this->getConnectionAndConnect($connectionSettings);
			$databasePlatform = $connection->getDatabasePlatform();
			if ($databasePlatform instanceof MySqlPlatform) {
				$queryResult = $connection->executeQuery('SHOW VARIABLES LIKE \'character_set_database\'')->fetch();
				$databaseCharacterSet = strtolower($queryResult['Value']);
			} elseif ($databasePlatform instanceof PostgreSqlPlatform) {
				$queryResult = $connection->executeQuery('SELECT pg_encoding_to_char(encoding) FROM pg_database WHERE datname = ?', array($databaseName))->fetch();
				$databaseCharacterSet = strtolower($queryResult['pg_encoding_to_char']);
			} else {
				$result = array('level' => 'error', 'message' => sprintf('Only MySQL/MariaDB and PostgreSQL are supported, the selected database is "%s".', $databasePlatform->getName()));
			}
			if (isset($databaseCharacterSet)) {
				if ($databaseCharacterSet === 'utf8') {
					$result = array('level' => 'notice', 'message' => 'The selected database\'s character set is set to "utf8" which is the recommended setting.');
				} else {
					$result = array(
						'level' => 'warning',
						'message' => sprintf('The selected database\'s character set is "%s", however changing it to "utf8" is urgently recommended. This setup tool won\'t do this for you.', $databaseCharacterSet)
					);
				}
			}
		} catch (\PDOException $exception) {
			$result = array('level' => 'error', 'message' => $exception->getMessage(), 'errorCode' => $exception->getCode());
		} catch (\Doctrine\DBAL\DBALException $exception) {
			$result = array('level' => 'error', 'message' => $exception->getMessage(), 'errorCode' => $exception->getCode());
		} catch (\Exception $exception) {
			$result = array('level' => 'error', 'message' => 'Unexpected exception', 'errorCode' => $exception->getCode());
		}
		return json_encode($result);
	}

	/**
	 * @param string $driver
	 * @param string $user
	 * @param string $password
	 * @param string $host
	 * @return array
	 */
	protected function buildConnectionSettingsArray($driver, $user, $password, $host) {
		$settings = $this->configurationManager->getConfiguration(ConfigurationManager::CONFIGURATION_TYPE_SETTINGS, 'TYPO3.Flow');
		$connectionSettings = $settings['persistence']['backendOptions'];
		$connectionSettings['driver'] = $driver;
		$connectionSettings['user'] = $user;
		$connectionSettings['password'] = $password;
		$connectionSettings['host'] = $host;
		if ($connectionSettings['driver'] === 'pdo_pgsql') {
			$connectionSettings['dbname'] = 'template1';
			return $connectionSettings;
		} else {
			unset($connectionSettings['dbname']);
			return $connectionSettings;
		}
	}

	/**
	 * @param array $connectionSettings
	 * @return \Doctrine\DBAL\Connection
	 */
	protected function getConnectionAndConnect(array $connectionSettings) {
		$connection = \Doctrine\DBAL\DriverManager::getConnection($connectionSettings);
		$connection->connect();
		return $connection;
	}
}
namespace TYPO3\Setup\ViewHelpers\Widget\Controller;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * Controller for the DatabaseSelector Fluid Widget
 */
class DatabaseSelectorController extends DatabaseSelectorController_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Aop\AdvicesTrait, \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;

    private $Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array();

    private $Flow_Aop_Proxy_groupedAdviceChains = array();

    private $Flow_Aop_Proxy_methodIsInAdviceMode = array();


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
        if ('TYPO3\Setup\ViewHelpers\Widget\Controller\DatabaseSelectorController' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    protected function Flow_Aop_Proxy_buildMethodsAndAdvicesArray()
    {
        if (method_exists(get_parent_class(), 'Flow_Aop_Proxy_buildMethodsAndAdvicesArray') && is_callable('parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray')) parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        $objectManager = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager;
        $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array(
            'indexAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'checkConnectionAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'getMetadataAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
        );
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
            $result = NULL;
        if (method_exists(get_parent_class(), '__wakeup') && is_callable('parent::__wakeup')) parent::__wakeup();
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __clone()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
    }

    /**
     * Autogenerated Proxy Method
     * @return void
     */
    public function indexAction()
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction'])) {
            $result = parent::indexAction();

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('indexAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Setup\ViewHelpers\Widget\Controller\DatabaseSelectorController', 'indexAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param string $driver
     * @param string $user
     * @param string $password
     * @param string $host
     * @return string
     */
    public function checkConnectionAction($driver, $user, $password, $host)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['checkConnectionAction'])) {
            $result = parent::checkConnectionAction($driver, $user, $password, $host);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['checkConnectionAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['driver'] = $driver;
                $methodArguments['user'] = $user;
                $methodArguments['password'] = $password;
                $methodArguments['host'] = $host;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('checkConnectionAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Setup\ViewHelpers\Widget\Controller\DatabaseSelectorController', 'checkConnectionAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['checkConnectionAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['checkConnectionAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param string $driver
     * @param string $user
     * @param string $password
     * @param string $host
     * @param string $databaseName
     * @return string
     */
    public function getMetadataAction($driver, $user, $password, $host, $databaseName)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['getMetadataAction'])) {
            $result = parent::getMetadataAction($driver, $user, $password, $host, $databaseName);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['getMetadataAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['driver'] = $driver;
                $methodArguments['user'] = $user;
                $methodArguments['password'] = $password;
                $methodArguments['host'] = $host;
                $methodArguments['databaseName'] = $databaseName;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('getMetadataAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Setup\ViewHelpers\Widget\Controller\DatabaseSelectorController', 'getMetadataAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['getMetadataAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['getMetadataAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'configurationManager' => 'TYPO3\\Flow\\Configuration\\ConfigurationManager',
  'widgetConfiguration' => 'array',
  'objectManager' => 'TYPO3\\Flow\\Object\\ObjectManagerInterface',
  'reflectionService' => 'TYPO3\\Flow\\Reflection\\ReflectionService',
  'mvcPropertyMappingConfigurationService' => 'TYPO3\\Flow\\Mvc\\Controller\\MvcPropertyMappingConfigurationService',
  'viewConfigurationManager' => 'TYPO3\\Flow\\Mvc\\ViewConfigurationManager',
  'view' => 'TYPO3\\Flow\\Mvc\\View\\ViewInterface',
  'viewObjectNamePattern' => 'string',
  'viewFormatToObjectNameMap' => 'array',
  'defaultViewObjectName' => 'string',
  'actionMethodName' => 'string',
  'errorMethodName' => 'string',
  'settings' => 'array',
  'systemLogger' => 'TYPO3\\Flow\\Log\\SystemLoggerInterface',
  'uriBuilder' => 'TYPO3\\Flow\\Mvc\\Routing\\UriBuilder',
  'validatorResolver' => 'TYPO3\\Flow\\Validation\\ValidatorResolver',
  'request' => 'TYPO3\\Flow\\Mvc\\ActionRequest',
  'response' => 'TYPO3\\Flow\\Http\\Response',
  'arguments' => 'TYPO3\\Flow\\Mvc\\Controller\\Arguments',
  'controllerContext' => 'TYPO3\\Flow\\Mvc\\Controller\\ControllerContext',
  'flashMessageContainer' => 'TYPO3\\Flow\\Mvc\\FlashMessageContainer',
  'persistenceManager' => 'TYPO3\\Flow\\Persistence\\PersistenceManagerInterface',
  'supportedMediaTypes' => 'array',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->injectSettings(\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get(\TYPO3\Flow\Configuration\ConfigurationManager::class)->getConfiguration('Settings', 'TYPO3.Setup'));
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Configuration\ConfigurationManager', 'TYPO3\Flow\Configuration\ConfigurationManager', 'configurationManager', '13edcae8fd67699bb78dadc8c1eac29c', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Configuration\ConfigurationManager'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Object\ObjectManagerInterface', 'TYPO3\Flow\Object\ObjectManager', 'objectManager', '0c3c44be7be16f2a287f1fb2d068dde4', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Object\ObjectManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Reflection\ReflectionService', 'TYPO3\Flow\Reflection\ReflectionService', 'reflectionService', '921ad637f16d2059757a908fceaf7076', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Reflection\ReflectionService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService', 'TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService', 'mvcPropertyMappingConfigurationService', '35acb49fbe78f28099d45aa647797c83', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\ViewConfigurationManager', 'TYPO3\Flow\Mvc\ViewConfigurationManager', 'viewConfigurationManager', '5a345bfd515fdb9f0c97080ff13c7079', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\ViewConfigurationManager'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Log\SystemLoggerInterface', '', 'systemLogger', '6d57d95a1c3cd7528e3e6ea15012dac8', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Log\SystemLoggerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Validation\ValidatorResolver', 'TYPO3\Flow\Validation\ValidatorResolver', 'validatorResolver', 'b457db29305ddeae13b61d92da000ca0', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Validation\ValidatorResolver'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\FlashMessageContainer', 'TYPO3\Flow\Mvc\FlashMessageContainer', 'flashMessageContainer', 'e4fd26f8afd3994317304b563b2a9561', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\FlashMessageContainer'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Persistence\PersistenceManagerInterface', 'TYPO3\Flow\Persistence\Doctrine\PersistenceManager', 'persistenceManager', 'f1bc82ad47156d95485678e33f27c110', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface'); });
        $this->Flow_Injected_Properties = array (
  0 => 'settings',
  1 => 'configurationManager',
  2 => 'objectManager',
  3 => 'reflectionService',
  4 => 'mvcPropertyMappingConfigurationService',
  5 => 'viewConfigurationManager',
  6 => 'systemLogger',
  7 => 'validatorResolver',
  8 => 'flashMessageContainer',
  9 => 'persistenceManager',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Setup/Classes/TYPO3/Setup/ViewHelpers/Widget/Controller/DatabaseSelectorController.php
#