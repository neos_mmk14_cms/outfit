<?php 
namespace TYPO3\Setup\Controller;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "TYPO3.Setup".           *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Scope("singleton")
 */
class SetupController_Original extends \TYPO3\Flow\Mvc\Controller\ActionController {

	/**
	 * The authentication manager
	 * @var \TYPO3\Flow\Security\Authentication\AuthenticationManagerInterface
	 * @Flow\Inject
	 */
	protected $authenticationManager;

	/**
	 * @var \TYPO3\Flow\Configuration\Source\YamlSource
	 * @Flow\Inject
	 */
	protected $configurationSource;

	/**
	 * The settings parsed from Settings.yaml
	 *
	 * @var array
	 */
	protected $distributionSettings;

	/**
	 * Contains the current step to be executed
	 * @see determineCurrentStepIndex()
	 *
	 * @var integer
	 */
	protected $currentStepIndex = 0;

	/**
	 * @return void
	 */
	protected function initializeAction() {
		$this->distributionSettings = $this->configurationSource->load(FLOW_PATH_CONFIGURATION . \TYPO3\Flow\Configuration\ConfigurationManager::CONFIGURATION_TYPE_SETTINGS);
	}

	/**
	 * @param integer $step
	 * @return void
	 * @Flow\SkipCsrfProtection
	 */
	public function indexAction($step = 0) {
		$this->currentStepIndex = $step;
		$this->checkRequestedStepIndex();
		$currentStep = $this->instantiateCurrentStep();
		$controller = $this;
		$callback = function(\TYPO3\Form\Core\Model\FinisherContext $finisherContext) use ($controller, $currentStep) {
			$controller->postProcessStep($finisherContext->getFormValues(), $currentStep);
		};
		$formDefinition = $currentStep->getFormDefinition($callback);
		if ($this->currentStepIndex > 0) {
			$formDefinition->setRenderingOption('previousStepUri', $this->uriBuilder->uriFor('index', array('step' => $this->currentStepIndex - 1)));
		}
		if ($currentStep->isOptional()) {
			$formDefinition->setRenderingOption('nextStepUri', $this->uriBuilder->uriFor('index', array('step' => $this->currentStepIndex + 1)));
		}
		$totalAmountOfSteps = count($this->settings['steps']);
		if ($this->currentStepIndex === $totalAmountOfSteps - 1) {
			$formDefinition->setRenderingOption('finalStep', TRUE);
			$this->authenticationManager->logout();
		}
		$response = new \TYPO3\Flow\Http\Response($this->response);
		$form = $formDefinition->bind($this->request, $response);

		try {
			$renderedForm = $form->render();
		} catch (\TYPO3\Setup\Exception $exception) {
			$this->addFlashMessage($exception->getMessage(), 'Exception while executing setup step', \TYPO3\Flow\Error\Message::SEVERITY_ERROR);
			$this->redirect('index', NULL, NULL, array('step' => $this->currentStepIndex));
		}
		$this->view->assignMultiple(array(
			'form' => $renderedForm,
			'totalAmountOfSteps' => $totalAmountOfSteps,
			'currentStepNumber' => $this->currentStepIndex + 1
		));
	}

	/**
	 * @return void
	 * @throws \TYPO3\Setup\Exception
	 */
	protected function checkRequestedStepIndex() {
		if (!isset($this->settings['stepOrder']) || !is_array($this->settings['stepOrder'])) {
			throw new \TYPO3\Setup\Exception('No "stepOrder" configured, setup can\'t be invoked', 1332167136);
		}
		$stepOrder = $this->settings['stepOrder'];
		if (!array_key_exists($this->currentStepIndex, $stepOrder)) {
			// TODO instead of throwing an exception we might also quietly jump to another step
			throw new \TYPO3\Setup\Exception(sprintf('No setup step #%d configured, setup can\'t be invoked', $this->currentStepIndex), 1332167418);
		}
		while ($this->checkRequiredConditions($stepOrder[$this->currentStepIndex]) !== TRUE) {
			if ($this->currentStepIndex === 0) {
				throw new \TYPO3\Setup\Exception('Not all requirements are met for the first setup step, aborting setup', 1332169088);
			}
			$this->addFlashMessage('Not all requirements are met for step "%s"', '', \TYPO3\Flow\Error\Message::SEVERITY_ERROR, array($stepOrder[$this->currentStepIndex]));
			$this->redirect('index', NULL, NULL, array('step' => $this->currentStepIndex - 1));
		};
	}

	/**
	 * @return \TYPO3\Setup\Step\StepInterface
	 * @throws \TYPO3\Setup\Exception
	 */
	protected function instantiateCurrentStep() {
		$currentStepIdentifier = $this->settings['stepOrder'][$this->currentStepIndex];
		$currentStepConfiguration = $this->settings['steps'][$currentStepIdentifier];
		if (!isset($currentStepConfiguration['className'])) {
			throw new \TYPO3\Setup\Exception(sprintf('No className specified for setup step "%s", setup can\'t be invoked', $currentStepIdentifier), 1332169398);
		}
		$currentStep = new $currentStepConfiguration['className']();
		if (!$currentStep instanceof \TYPO3\Setup\Step\StepInterface) {
			throw new \TYPO3\Setup\Exception(sprintf('ClassName %s of setup step "%s" does not implement StepInterface, setup can\'t be invoked', $currentStepConfiguration['className'], $currentStepIdentifier), 1332169576);
		}
		if (isset($currentStepConfiguration['options'])) {
			$currentStep->setOptions($currentStepConfiguration['options']);
		}
		$currentStep->setDistributionSettings($this->distributionSettings);
		return $currentStep;
	}

	/**
	 * @param string $stepIdentifier
	 * @return boolean TRUE if all required conditions were met, otherwise FALSE
	 * @throws \TYPO3\Setup\Exception
	 */
	protected function checkRequiredConditions($stepIdentifier) {
		if (!isset($this->settings['steps'][$stepIdentifier]) || !is_array($this->settings['steps'][$stepIdentifier])) {
			throw new \TYPO3\Setup\Exception(sprintf('No configuration found for setup step "%s", setup can\'t be invoked', $stepIdentifier), 1332167685);
		}
		$stepConfiguration = $this->settings['steps'][$stepIdentifier];
		if (!isset($stepConfiguration['requiredConditions'])) {
			return TRUE;
		}
		foreach ($stepConfiguration['requiredConditions'] as $index => $conditionConfiguration) {
			if (!isset($conditionConfiguration['className'])) {
				throw new \TYPO3\Setup\Exception(sprintf('No condition className specified for condition #%d in setup step "%s", setup can\'t be invoked', $index, $stepIdentifier), 1332168070);
			}
			$condition = new $conditionConfiguration['className']();
			if (!$condition instanceof \TYPO3\Setup\Condition\ConditionInterface) {
				throw new \TYPO3\Setup\Exception(sprintf('Condition #%d (%s) in setup step "%s" does not implement ConditionInterface, setup can\'t be invoked', $index, $conditionConfiguration['className'], $stepIdentifier), 1332168070);
			}
			if (isset($conditionConfiguration['options'])) {
				$condition->setOptions($conditionConfiguration['options']);
			}
			if ($condition->isMet() !== TRUE) {
				return FALSE;
			}
		}
		return TRUE;
	}

	/**
	 * @param array $formValues
	 * @param \TYPO3\Setup\Step\StepInterface $currentStep
	 * @return void
	 */
	public function postProcessStep(array $formValues, \TYPO3\Setup\Step\StepInterface $currentStep) {
		try {
			$currentStep->postProcessFormValues($formValues);
		} catch (\TYPO3\Setup\Exception $exception) {
			$this->addFlashMessage($exception->getMessage(), 'Exception while executing setup step', \TYPO3\Flow\Error\Message::SEVERITY_ERROR);
			$this->redirect('index', NULL, NULL, array('step' => $this->currentStepIndex));
		}
		$this->redirect('index', NULL, NULL, array('step' => $this->currentStepIndex + 1));
	}

}
namespace TYPO3\Setup\Controller;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * 
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class SetupController extends SetupController_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Aop\AdvicesTrait, \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;

    private $Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array();

    private $Flow_Aop_Proxy_groupedAdviceChains = array();

    private $Flow_Aop_Proxy_methodIsInAdviceMode = array();


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
        if (get_class($this) === 'TYPO3\Setup\Controller\SetupController') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Setup\Controller\SetupController', $this);
        if ('TYPO3\Setup\Controller\SetupController' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    protected function Flow_Aop_Proxy_buildMethodsAndAdvicesArray()
    {
        if (method_exists(get_parent_class(), 'Flow_Aop_Proxy_buildMethodsAndAdvicesArray') && is_callable('parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray')) parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        $objectManager = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager;
        $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array(
            'indexAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
        );
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
        if (get_class($this) === 'TYPO3\Setup\Controller\SetupController') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Setup\Controller\SetupController', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
            $result = NULL;
        if (method_exists(get_parent_class(), '__wakeup') && is_callable('parent::__wakeup')) parent::__wakeup();
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __clone()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
    }

    /**
     * Autogenerated Proxy Method
     * @param integer $step
     * @return void
     * @\TYPO3\Flow\Annotations\SkipCsrfProtection
     */
    public function indexAction($step = 0)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction'])) {
            $result = parent::indexAction($step);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['step'] = $step;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('indexAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Setup\Controller\SetupController', 'indexAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'authenticationManager' => '\\TYPO3\\Flow\\Security\\Authentication\\AuthenticationManagerInterface',
  'configurationSource' => '\\TYPO3\\Flow\\Configuration\\Source\\YamlSource',
  'distributionSettings' => 'array',
  'currentStepIndex' => 'integer',
  'objectManager' => 'TYPO3\\Flow\\Object\\ObjectManagerInterface',
  'reflectionService' => 'TYPO3\\Flow\\Reflection\\ReflectionService',
  'mvcPropertyMappingConfigurationService' => 'TYPO3\\Flow\\Mvc\\Controller\\MvcPropertyMappingConfigurationService',
  'viewConfigurationManager' => 'TYPO3\\Flow\\Mvc\\ViewConfigurationManager',
  'view' => 'TYPO3\\Flow\\Mvc\\View\\ViewInterface',
  'viewObjectNamePattern' => 'string',
  'viewFormatToObjectNameMap' => 'array',
  'defaultViewObjectName' => 'string',
  'actionMethodName' => 'string',
  'errorMethodName' => 'string',
  'settings' => 'array',
  'systemLogger' => 'TYPO3\\Flow\\Log\\SystemLoggerInterface',
  'uriBuilder' => 'TYPO3\\Flow\\Mvc\\Routing\\UriBuilder',
  'validatorResolver' => 'TYPO3\\Flow\\Validation\\ValidatorResolver',
  'request' => 'TYPO3\\Flow\\Mvc\\ActionRequest',
  'response' => 'TYPO3\\Flow\\Http\\Response',
  'arguments' => 'TYPO3\\Flow\\Mvc\\Controller\\Arguments',
  'controllerContext' => 'TYPO3\\Flow\\Mvc\\Controller\\ControllerContext',
  'flashMessageContainer' => 'TYPO3\\Flow\\Mvc\\FlashMessageContainer',
  'persistenceManager' => 'TYPO3\\Flow\\Persistence\\PersistenceManagerInterface',
  'supportedMediaTypes' => 'array',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->injectSettings(\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get(\TYPO3\Flow\Configuration\ConfigurationManager::class)->getConfiguration('Settings', 'TYPO3.Setup'));
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Security\Authentication\AuthenticationManagerInterface', 'TYPO3\Flow\Security\Authentication\AuthenticationProviderManager', 'authenticationManager', 'd4b358f5a262d346229c2bf11ebd0c1d', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\Authentication\AuthenticationManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Configuration\Source\YamlSource', 'TYPO3\Flow\Configuration\Source\YamlSource', 'configurationSource', '3ff75d2363593363cb0d0607df40c19a', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Configuration\Source\YamlSource'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Object\ObjectManagerInterface', 'TYPO3\Flow\Object\ObjectManager', 'objectManager', '0c3c44be7be16f2a287f1fb2d068dde4', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Object\ObjectManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Reflection\ReflectionService', 'TYPO3\Flow\Reflection\ReflectionService', 'reflectionService', '921ad637f16d2059757a908fceaf7076', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Reflection\ReflectionService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService', 'TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService', 'mvcPropertyMappingConfigurationService', '35acb49fbe78f28099d45aa647797c83', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\ViewConfigurationManager', 'TYPO3\Flow\Mvc\ViewConfigurationManager', 'viewConfigurationManager', '5a345bfd515fdb9f0c97080ff13c7079', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\ViewConfigurationManager'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Log\SystemLoggerInterface', '', 'systemLogger', '6d57d95a1c3cd7528e3e6ea15012dac8', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Log\SystemLoggerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Validation\ValidatorResolver', 'TYPO3\Flow\Validation\ValidatorResolver', 'validatorResolver', 'b457db29305ddeae13b61d92da000ca0', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Validation\ValidatorResolver'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\FlashMessageContainer', 'TYPO3\Flow\Mvc\FlashMessageContainer', 'flashMessageContainer', 'e4fd26f8afd3994317304b563b2a9561', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\FlashMessageContainer'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Persistence\PersistenceManagerInterface', 'TYPO3\Flow\Persistence\Doctrine\PersistenceManager', 'persistenceManager', 'f1bc82ad47156d95485678e33f27c110', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface'); });
        $this->Flow_Injected_Properties = array (
  0 => 'settings',
  1 => 'authenticationManager',
  2 => 'configurationSource',
  3 => 'objectManager',
  4 => 'reflectionService',
  5 => 'mvcPropertyMappingConfigurationService',
  6 => 'viewConfigurationManager',
  7 => 'systemLogger',
  8 => 'validatorResolver',
  9 => 'flashMessageContainer',
  10 => 'persistenceManager',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Setup/Classes/TYPO3/Setup/Controller/SetupController.php
#