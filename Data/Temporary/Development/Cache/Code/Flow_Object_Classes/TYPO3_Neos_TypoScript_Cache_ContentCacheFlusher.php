<?php 
namespace TYPO3\Neos\TypoScript\Cache;

/*
 * This file is part of the TYPO3.Neos package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Log\SystemLoggerInterface;
use TYPO3\Media\Domain\Model\AssetInterface;
use TYPO3\Media\Domain\Service\AssetService;
use TYPO3\Neos\Domain\Model\Dto\AssetUsageInNodeProperties;
use TYPO3\TYPO3CR\Domain\Model\NodeInterface;
use TYPO3\TYPO3CR\Domain\Model\NodeType;
use TYPO3\TypoScript\Core\Cache\ContentCache;

/**
 * This service flushes TypoScript content caches triggered by node changes.
 *
 * The method registerNodeChange() is triggered by a signal which is configured in the Package class of the TYPO3.Neos
 * package (this package). Information on changed nodes is collected by this method and the respective TypoScript content
 * cache entries are flushed in one operation during Flow's shutdown procedure.
 *
 * @Flow\Scope("singleton")
 */
class ContentCacheFlusher_Original
{
    /**
     * @Flow\Inject
     * @var ContentCache
     */
    protected $contentCache;

    /**
     * @Flow\Inject
     * @var SystemLoggerInterface
     */
    protected $systemLogger;

    /**
     * @var array
     */
    protected $tagsToFlush = array();

    /**
     * @Flow\Inject
     * @var AssetService
     */
    protected $assetService;

    /**
     * Register a node change for a later cache flush. This method is triggered by a signal sent via TYPO3CR's Node
     * model or the Neos Publishing Service.
     *
     * @param NodeInterface $node The node which has changed in some way
     * @return void
     */
    public function registerNodeChange(NodeInterface $node)
    {
        $this->tagsToFlush[ContentCache::TAG_EVERYTHING] = 'which were tagged with "Everything".';

        $nodeTypesToFlush = $this->getAllImplementedNodeTypes($node->getNodeType());
        foreach ($nodeTypesToFlush as $nodeType) {
            $nodeTypeName = $nodeType->getName();
            $this->tagsToFlush['NodeType_' . $nodeTypeName] = sprintf('which were tagged with "NodeType_%s" because node "%s" has changed and was of type "%s".', $nodeTypeName, $node->getPath(), $node->getNodeType()->getName());
        }

        $this->tagsToFlush['Node_' . $node->getIdentifier()] = sprintf('which were tagged with "Node_%s" because node "%s" has changed.', $node->getIdentifier(), $node->getPath());

        $originalNode = $node;
        while ($node->getDepth() > 1) {
            $node = $node->getParent();
            // Workaround for issue #56566 in TYPO3.TYPO3CR
            if ($node === null) {
                break;
            }
            $tagName = 'DescendantOf_' . $node->getIdentifier();
            $this->tagsToFlush[$tagName] = sprintf('which were tagged with "%s" because node "%s" has changed.', $tagName, $originalNode->getPath());
        }
    }

    /**
     * Fetches possible usages of the asset and registers nodes that use the asset as changed.
     *
     * @param AssetInterface $asset
     * @return void
     */
    public function registerAssetResourceChange(AssetInterface $asset)
    {
        if (!$asset->isInUse()) {
            return;
        }

        foreach ($this->assetService->getUsageReferences($asset) as $reference) {
            if (!$reference instanceof AssetUsageInNodeProperties) {
                continue;
            }

            $this->registerNodeChange($reference->getNode());
        }
    }

    /**
     * Flush caches according to the previously registered node changes.
     *
     * @return void
     */
    public function shutdownObject()
    {
        if ($this->tagsToFlush !== array()) {
            foreach ($this->tagsToFlush as $tag => $logMessage) {
                $affectedEntries = $this->contentCache->flushByTag($tag);
                if ($affectedEntries > 0) {
                    $this->systemLogger->log(sprintf('Content cache: Removed %s entries %s', $affectedEntries, $logMessage), LOG_DEBUG);
                }
            }
        }
    }

    /**
     * @param NodeType $nodeType
     * @return array<NodeType>
     */
    protected function getAllImplementedNodeTypes(NodeType $nodeType)
    {
        $types = array($nodeType);
        foreach ($nodeType->getDeclaredSuperTypes() as $superType) {
            $types = array_merge($types, $this->getAllImplementedNodeTypes($superType));
        }
        return $types;
    }
}
namespace TYPO3\Neos\TypoScript\Cache;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * This service flushes TypoScript content caches triggered by node changes.
 * 
 * The method registerNodeChange() is triggered by a signal which is configured in the Package class of the TYPO3.Neos
 * package (this package). Information on changed nodes is collected by this method and the respective TypoScript content
 * cache entries are flushed in one operation during Flow's shutdown procedure.
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class ContentCacheFlusher extends ContentCacheFlusher_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        if (get_class($this) === 'TYPO3\Neos\TypoScript\Cache\ContentCacheFlusher') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Neos\TypoScript\Cache\ContentCacheFlusher', $this);
        if ('TYPO3\Neos\TypoScript\Cache\ContentCacheFlusher' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }

        $isSameClass = get_class($this) === 'TYPO3\Neos\TypoScript\Cache\ContentCacheFlusher';
        if ($isSameClass) {
            \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->registerShutdownObject($this, 'shutdownObject');
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'contentCache' => 'TYPO3\\TypoScript\\Core\\Cache\\ContentCache',
  'systemLogger' => 'TYPO3\\Flow\\Log\\SystemLoggerInterface',
  'tagsToFlush' => 'array',
  'assetService' => 'TYPO3\\Media\\Domain\\Service\\AssetService',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {
        if (get_class($this) === 'TYPO3\Neos\TypoScript\Cache\ContentCacheFlusher') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Neos\TypoScript\Cache\ContentCacheFlusher', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
            $result = NULL;

        $isSameClass = get_class($this) === 'TYPO3\Neos\TypoScript\Cache\ContentCacheFlusher';
        $classParents = class_parents($this);
        $classImplements = class_implements($this);
        $isClassProxy = array_search('TYPO3\Neos\TypoScript\Cache\ContentCacheFlusher', $classParents) !== FALSE && array_search('Doctrine\ORM\Proxy\Proxy', $classImplements) !== FALSE;

        if ($isSameClass || $isClassProxy) {
            \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->registerShutdownObject($this, 'shutdownObject');
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TypoScript\Core\Cache\ContentCache', 'TYPO3\TypoScript\Core\Cache\ContentCache', 'contentCache', '47de6efd5fde41cfd6e087327f547a00', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TypoScript\Core\Cache\ContentCache'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Log\SystemLoggerInterface', '', 'systemLogger', '6d57d95a1c3cd7528e3e6ea15012dac8', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Log\SystemLoggerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Media\Domain\Service\AssetService', 'TYPO3\Media\Domain\Service\AssetService', 'assetService', 'bb639bd7986d8031f2903c52fd36ae6a', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Media\Domain\Service\AssetService'); });
        $this->Flow_Injected_Properties = array (
  0 => 'contentCache',
  1 => 'systemLogger',
  2 => 'assetService',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Neos/Classes/TYPO3/Neos/TypoScript/Cache/ContentCacheFlusher.php
#