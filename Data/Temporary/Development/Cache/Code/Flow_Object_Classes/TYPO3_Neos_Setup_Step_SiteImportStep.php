<?php 
namespace TYPO3\Neos\Setup\Step;

/*
 * This file is part of the TYPO3.Neos package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Log\SystemLoggerInterface;
use TYPO3\Flow\Mvc\FlashMessageContainer;
use TYPO3\Flow\Object\ObjectManagerInterface;
use TYPO3\Flow\Package\PackageManagerInterface;
use TYPO3\Flow\Persistence\PersistenceManagerInterface;
use TYPO3\Flow\Validation\Validator\NotEmptyValidator;
use TYPO3\Form\Core\Model\FinisherContext;
use TYPO3\Form\Core\Model\FormDefinition;
use TYPO3\Form\Finishers\ClosureFinisher;
use TYPO3\Neos\Domain\Repository\DomainRepository;
use TYPO3\Neos\Domain\Repository\SiteRepository;
use TYPO3\Neos\Domain\Service\SiteImportService;
use TYPO3\Neos\Validation\Validator\PackageKeyValidator;
use TYPO3\Setup\Exception as SetupException;
use TYPO3\Flow\Error\Message;
use TYPO3\Setup\Exception;
use TYPO3\Setup\Step\AbstractStep;
use TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository;
use TYPO3\TYPO3CR\Domain\Repository\WorkspaceRepository;
use TYPO3\TYPO3CR\Domain\Service\ContextFactoryInterface;

/**
 * @Flow\Scope("singleton")
 */
class SiteImportStep_Original extends AbstractStep
{
    /**
     * @var boolean
     */
    protected $optional = true;

    /**
     * @Flow\Inject
     * @var PackageManagerInterface
     */
    protected $packageManager;

    /**
     * @Flow\Inject
     * @var SiteRepository
     */
    protected $siteRepository;

    /**
     * @Flow\Inject
     * @var SiteImportService
     */
    protected $siteImportService;

    /**
     * @Flow\Inject
     * @var DomainRepository
     */
    protected $domainRepository;

    /**
     * @Flow\Inject
     * @var FlashMessageContainer
     */
    protected $flashMessageContainer;

    /**
     * @Flow\Inject
     * @var NodeDataRepository
     */
    protected $nodeDataRepository;

    /**
     * @Flow\Inject
     * @var WorkspaceRepository
     */
    protected $workspaceRepository;

    /**
     * @Flow\Inject
     * @var PersistenceManagerInterface
     */
    protected $persistenceManager;

    /**
     * @Flow\Inject
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var ClosureFinisher
     */
    protected $closureFinisher;

    /**
     * @var SystemLoggerInterface
     * @Flow\Inject
     */
    protected $systemLogger;

    /**
     * @Flow\Inject
     * @var ContextFactoryInterface
     */
    protected $contextFactory;

    /**
     * Returns the form definitions for the step
     *
     * @param FormDefinition $formDefinition
     * @return void
     */
    protected function buildForm(FormDefinition $formDefinition)
    {
        $page1 = $formDefinition->createPage('page1');
        $page1->setRenderingOption('header', 'Create a new site');

        $introduction = $page1->createElement('introduction', 'TYPO3.Form:StaticText');
        $introduction->setProperty('text', 'There are two ways of creating a site. Choose between the following:');

        $importSection = $page1->createElement('import', 'TYPO3.Form:Section');
        $importSection->setLabel('Import a site from an existing site package');

        $sitePackages = array();
        foreach ($this->packageManager->getFilteredPackages('available', null, 'typo3-flow-site') as $package) {
            $sitePackages[$package->getPackageKey()] = $package->getPackageKey();
        }

        if (count($sitePackages) > 0) {
            $site = $importSection->createElement('site', 'TYPO3.Form:SingleSelectDropdown');
            $site->setLabel('Select a site package');
            $site->setProperty('options', $sitePackages);
            $site->addValidator(new NotEmptyValidator());

            $sites = $this->siteRepository->findAll();
            if ($sites->count() > 0) {
                $prune = $importSection->createElement('prune', 'TYPO3.Form:Checkbox');
                $prune->setLabel('Delete existing sites');
            }
        } else {
            $error = $importSection->createElement('noSitePackagesError', 'TYPO3.Form:StaticText');
            $error->setProperty('text', 'No site packages were available, make sure you have an active site package');
            $error->setProperty('elementClassAttribute', 'alert alert-warning');
        }

        if ($this->packageManager->isPackageActive('TYPO3.Neos.Kickstarter')) {
            $separator = $page1->createElement('separator', 'TYPO3.Form:StaticText');
            $separator->setProperty('elementClassAttribute', 'section-separator');

            $newPackageSection = $page1->createElement('newPackageSection', 'TYPO3.Form:Section');
            $newPackageSection->setLabel('Create a new site package with a dummy site');
            $packageName = $newPackageSection->createElement('packageKey', 'TYPO3.Form:SingleLineText');
            $packageName->setLabel('Package Name (in form "Vendor.DomainCom")');
            $packageName->addValidator(new PackageKeyValidator());

            $siteName = $newPackageSection->createElement('siteName', 'TYPO3.Form:SingleLineText');
            $siteName->setLabel('Site Name (e.g. "domain.com")');
        } else {
            $error = $importSection->createElement('neosKickstarterUnavailableError', 'TYPO3.Form:StaticText');
            $error->setProperty('text', 'The Neos Kickstarter package (TYPO3.Neos.Kickstarter) is not installed, install it for kickstarting new sites (using "composer require typo3/neos-kickstarter")');
            $error->setProperty('elementClassAttribute', 'alert alert-warning');
        }

        $explanation = $page1->createElement('explanation', 'TYPO3.Form:StaticText');
        $explanation->setProperty('text', 'Notice the difference between a site package and a site. A site package is a Flow package that can be used for creating multiple site instances.');
        $explanation->setProperty('elementClassAttribute', 'alert alert-info');

        $step = $this;
        $callback = function (FinisherContext $finisherContext) use ($step) {
            $step->importSite($finisherContext);
        };
        $this->closureFinisher = new ClosureFinisher();
        $this->closureFinisher->setOption('closure', $callback);
        $formDefinition->addFinisher($this->closureFinisher);

        $formDefinition->setRenderingOption('skipStepNotice', 'You can always import a site using the site:import command');
    }

    /**
     * @param FinisherContext $finisherContext
     * @return void
     * @throws Exception
     */
    public function importSite(FinisherContext $finisherContext)
    {
        $formValues = $finisherContext->getFormRuntime()->getFormState()->getFormValues();

        if (isset($formValues['prune']) && intval($formValues['prune']) === 1) {
            $this->nodeDataRepository->removeAll();
            $this->workspaceRepository->removeAll();
            $this->domainRepository->removeAll();
            $this->siteRepository->removeAll();
            $this->persistenceManager->persistAll();
        }

        if (!empty($formValues['packageKey'])) {
            if ($this->packageManager->isPackageAvailable($formValues['packageKey'])) {
                throw new Exception(sprintf('The package key "%s" already exists.', $formValues['packageKey']), 1346759486);
            }
            $packageKey = $formValues['packageKey'];
            $siteName = $formValues['siteName'];

            $generatorService = $this->objectManager->get('TYPO3\Neos\Kickstarter\Service\GeneratorService');
            $generatorService->generateSitePackage($packageKey, $siteName);
        } elseif (!empty($formValues['site'])) {
            $packageKey = $formValues['site'];
        }

        $this->deactivateOtherSitePackages($packageKey);
        $this->packageManager->activatePackage($packageKey);

        if (!empty($packageKey)) {
            try {
                $contentContext = $this->contextFactory->create(array('workspaceName' => 'live'));
                $this->siteImportService->importFromPackage($packageKey, $contentContext);
            } catch (\Exception $exception) {
                $finisherContext->cancel();
                $this->systemLogger->logException($exception);
                throw new SetupException(sprintf('Error: During the import of the "Sites.xml" from the package "%s" an exception occurred: %s', $packageKey, $exception->getMessage()), 1351000864);
            }
        }
    }

    /**
     * If Site Packages already exist and are active, we will deactivate them in order to prevent
     * interactions with the newly created or imported package (like Content Dimensions being used).
     *
     * @param string $packageKey
     * @return array
     */
    protected function deactivateOtherSitePackages($packageKey)
    {
        $sitePackagesToDeactivate = $this->packageManager->getFilteredPackages('active', null, 'typo3-flow-site');
        $deactivatedSitePackages = array();
        foreach ($sitePackagesToDeactivate as $sitePackageToDeactivate) {
            if ($sitePackageToDeactivate->getPackageKey() !== $packageKey) {
                $this->packageManager->deactivatePackage($sitePackageToDeactivate->getPackageKey());
                $deactivatedSitePackages[] = $sitePackageToDeactivate->getPackageKey();
            }
        }

        if (count($deactivatedSitePackages) >= 1) {
            $this->flashMessageContainer->addMessage(new Message(sprintf('The existing Site Packages "%s" were deactivated, in order to prevent interactions with the newly created package "%s".', implode(', ', $deactivatedSitePackages), $packageKey)));
        }
    }
}
namespace TYPO3\Neos\Setup\Step;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * 
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class SiteImportStep extends SiteImportStep_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        if (get_class($this) === 'TYPO3\Neos\Setup\Step\SiteImportStep') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Neos\Setup\Step\SiteImportStep', $this);
        if ('TYPO3\Neos\Setup\Step\SiteImportStep' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }

        $isSameClass = get_class($this) === 'TYPO3\Neos\Setup\Step\SiteImportStep';
        if ($isSameClass) {
            $this->initializeObject(1);
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'optional' => 'boolean',
  'packageManager' => 'TYPO3\\Flow\\Package\\PackageManagerInterface',
  'siteRepository' => 'TYPO3\\Neos\\Domain\\Repository\\SiteRepository',
  'siteImportService' => 'TYPO3\\Neos\\Domain\\Service\\SiteImportService',
  'domainRepository' => 'TYPO3\\Neos\\Domain\\Repository\\DomainRepository',
  'flashMessageContainer' => 'TYPO3\\Flow\\Mvc\\FlashMessageContainer',
  'nodeDataRepository' => 'TYPO3\\TYPO3CR\\Domain\\Repository\\NodeDataRepository',
  'workspaceRepository' => 'TYPO3\\TYPO3CR\\Domain\\Repository\\WorkspaceRepository',
  'persistenceManager' => 'TYPO3\\Flow\\Persistence\\PersistenceManagerInterface',
  'objectManager' => 'TYPO3\\Flow\\Object\\ObjectManagerInterface',
  'closureFinisher' => 'TYPO3\\Form\\Finishers\\ClosureFinisher',
  'systemLogger' => 'TYPO3\\Flow\\Log\\SystemLoggerInterface',
  'contextFactory' => 'TYPO3\\TYPO3CR\\Domain\\Service\\ContextFactoryInterface',
  'formSettings' => 'array',
  'configurationManager' => '\\TYPO3\\Flow\\Configuration\\ConfigurationManager',
  'options' => 'array',
  'distributionSettings' => 'array',
  'presetName' => 'string',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {
        if (get_class($this) === 'TYPO3\Neos\Setup\Step\SiteImportStep') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Neos\Setup\Step\SiteImportStep', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
            $result = NULL;

        $isSameClass = get_class($this) === 'TYPO3\Neos\Setup\Step\SiteImportStep';
        $classParents = class_parents($this);
        $classImplements = class_implements($this);
        $isClassProxy = array_search('TYPO3\Neos\Setup\Step\SiteImportStep', $classParents) !== FALSE && array_search('Doctrine\ORM\Proxy\Proxy', $classImplements) !== FALSE;

        if ($isSameClass || $isClassProxy) {
            $this->initializeObject(2);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Package\PackageManagerInterface', 'TYPO3\Flow\Package\PackageManager', 'packageManager', 'aad0cdb65adb124cf4b4d16c5b42256c', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Package\PackageManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Neos\Domain\Repository\SiteRepository', 'TYPO3\Neos\Domain\Repository\SiteRepository', 'siteRepository', '5c3f2ab0e14ff0be3090c1f3efe77d7a', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Neos\Domain\Repository\SiteRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Neos\Domain\Service\SiteImportService', 'TYPO3\Neos\Domain\Service\SiteImportService', 'siteImportService', 'a382bdbc7e75d00f0510a58eb9dd5b14', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Neos\Domain\Service\SiteImportService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Neos\Domain\Repository\DomainRepository', 'TYPO3\Neos\Domain\Repository\DomainRepository', 'domainRepository', '6f2987c5f47777b01540a314d984b09c', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Neos\Domain\Repository\DomainRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\FlashMessageContainer', 'TYPO3\Flow\Mvc\FlashMessageContainer', 'flashMessageContainer', 'e4fd26f8afd3994317304b563b2a9561', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\FlashMessageContainer'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository', 'TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository', 'nodeDataRepository', '6d8e58e235099c88f352e23317321129', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Repository\WorkspaceRepository', 'TYPO3\TYPO3CR\Domain\Repository\WorkspaceRepository', 'workspaceRepository', '2e64c564c983af14b47d0c9ae8992997', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Repository\WorkspaceRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Persistence\PersistenceManagerInterface', 'TYPO3\Flow\Persistence\Doctrine\PersistenceManager', 'persistenceManager', 'f1bc82ad47156d95485678e33f27c110', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Object\ObjectManagerInterface', 'TYPO3\Flow\Object\ObjectManager', 'objectManager', '0c3c44be7be16f2a287f1fb2d068dde4', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Object\ObjectManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Log\SystemLoggerInterface', '', 'systemLogger', '6d57d95a1c3cd7528e3e6ea15012dac8', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Log\SystemLoggerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Service\ContextFactoryInterface', 'TYPO3\Neos\Domain\Service\ContentContextFactory', 'contextFactory', '6b6e9d36a8365cb0dccb3d849ae9366e', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Service\ContextFactoryInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Configuration\ConfigurationManager', 'TYPO3\Flow\Configuration\ConfigurationManager', 'configurationManager', '13edcae8fd67699bb78dadc8c1eac29c', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Configuration\ConfigurationManager'); });
        $this->Flow_Injected_Properties = array (
  0 => 'packageManager',
  1 => 'siteRepository',
  2 => 'siteImportService',
  3 => 'domainRepository',
  4 => 'flashMessageContainer',
  5 => 'nodeDataRepository',
  6 => 'workspaceRepository',
  7 => 'persistenceManager',
  8 => 'objectManager',
  9 => 'systemLogger',
  10 => 'contextFactory',
  11 => 'configurationManager',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Neos/Classes/TYPO3/Neos/Setup/Step/SiteImportStep.php
#