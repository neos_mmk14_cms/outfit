<?php 
namespace TYPO3\Neos\EventLog\Integrations;

/*
 * This file is part of the TYPO3.Neos package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Event\OnFlushEventArgs;
use TYPO3\Eel\CompilingEvaluator;
use TYPO3\Eel\Exception;
use TYPO3\Eel\Utility;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Log\SystemLoggerInterface;
use TYPO3\Neos\EventLog\Domain\Service\EventEmittingService;

/**
 * Monitors entity changes
 *
 * @Flow\Scope("singleton")
 */
class EntityIntegrationService_Original extends AbstractIntegrationService
{
    /**
     * Doctrine's Entity Manager. Note that "ObjectManager" is the name of the related
     * interface ...
     *
     * @Flow\Inject
     * @var ObjectManager
     */
    protected $entityManager;

    /**
     * @Flow\Inject
     * @var EventEmittingService
     */
    protected $eventEmittingService;

    /**
     * @Flow\Inject(lazy=FALSE)
     * @var CompilingEvaluator
     */
    protected $eelEvaluator;

    /**
     * @Flow\InjectConfiguration("eventLog.monitorEntities")
     * @var array
     */
    protected $monitorEntitiesSetting;

    /**
     * @Flow\Inject
     * @var SystemLoggerInterface
     */
    protected $logger;

    /**
     * Dummy method which is called in a prePersist signal. If we remove that, this object is never instantiated and thus
     * cannot hook into the Doctrine EntityManager.
     *
     * @return void
     */
    public function dummyMethodToEnsureInstanceExists()
    {
        // intentionally empty
    }

    /**
     * Record events for entity changes.
     *
     * Note: this method is registered as an Doctrine event listener in the settings of this package.
     *
     * TODO: Update/Delete of Entities
     *
     * @param OnFlushEventArgs $eventArgs
     * @return void
     * @throws Exception
     */
    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        if (!$this->eventEmittingService->isEnabled()) {
            return;
        }

        $entityManager = $eventArgs->getEntityManager();
        $unitOfWork = $entityManager->getUnitOfWork();

        foreach ($unitOfWork->getScheduledEntityInsertions() as $entity) {
            $className = get_class($entity);
            if (isset($this->monitorEntitiesSetting[$className])) {
                $entityMonitoringConfiguration = $this->monitorEntitiesSetting[$className];

                if (isset($entityMonitoringConfiguration['events']['created'])) {
                    $this->initializeAccountIdentifier();
                    $data = array();
                    foreach ($entityMonitoringConfiguration['data'] as $key => $eelExpression) {
                        $data[$key] = Utility::evaluateEelExpression($eelExpression, $this->eelEvaluator, array('entity' => $entity));
                    }

                    $event = $this->eventEmittingService->emit($entityMonitoringConfiguration['events']['created'], $data);
                    $unitOfWork->computeChangeSet($entityManager->getClassMetadata('TYPO3\Neos\EventLog\Domain\Model\Event'), $event);
                }
            }
        }

        foreach ($unitOfWork->getScheduledEntityDeletions() as $entity) {
            $className = get_class($entity);
            if (isset($this->monitorEntitiesSetting[$className])) {
                $entityMonitoringConfiguration = $this->monitorEntitiesSetting[$className];

                if (isset($entityMonitoringConfiguration['events']['deleted'])) {
                    $this->initializeAccountIdentifier();
                    $data = array();
                    foreach ($entityMonitoringConfiguration['data'] as $key => $eelExpression) {
                        $data[$key] = Utility::evaluateEelExpression($eelExpression, $this->eelEvaluator, array('entity' => $entity));
                    }

                    $event = $this->eventEmittingService->emit($entityMonitoringConfiguration['events']['deleted'], $data);
                    $unitOfWork->computeChangeSet($entityManager->getClassMetadata('TYPO3\Neos\EventLog\Domain\Model\Event'), $event);
                }
            }
        }
    }

    /**
     * @param array $monitorEntitiesSetting
     * @return void
     */
    public function setMonitorEntitiesSetting($monitorEntitiesSetting)
    {
        $this->monitorEntitiesSetting = $monitorEntitiesSetting;
    }
}
namespace TYPO3\Neos\EventLog\Integrations;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * Monitors entity changes
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class EntityIntegrationService extends EntityIntegrationService_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        if (get_class($this) === 'TYPO3\Neos\EventLog\Integrations\EntityIntegrationService') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Neos\EventLog\Integrations\EntityIntegrationService', $this);
        if ('TYPO3\Neos\EventLog\Integrations\EntityIntegrationService' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'entityManager' => 'Doctrine\\Common\\Persistence\\ObjectManager',
  'eventEmittingService' => 'TYPO3\\Neos\\EventLog\\Domain\\Service\\EventEmittingService',
  'eelEvaluator' => 'TYPO3\\Eel\\CompilingEvaluator',
  'monitorEntitiesSetting' => 'array',
  'logger' => 'TYPO3\\Flow\\Log\\SystemLoggerInterface',
  'securityContext' => 'TYPO3\\Flow\\Security\\Context',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {
        if (get_class($this) === 'TYPO3\Neos\EventLog\Integrations\EntityIntegrationService') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Neos\EventLog\Integrations\EntityIntegrationService', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('Doctrine\Common\Persistence\ObjectManager', 'Doctrine\Common\Persistence\ObjectManager', 'entityManager', 'ea59127cf49656654065ffe160cf78e1', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('Doctrine\Common\Persistence\ObjectManager'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Neos\EventLog\Domain\Service\EventEmittingService', 'TYPO3\Neos\EventLog\Domain\Service\EventEmittingService', 'eventEmittingService', 'd3423df31112e34168f719624458fd33', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Neos\EventLog\Domain\Service\EventEmittingService'); });
        $this->eelEvaluator = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Eel\CompilingEvaluator');
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Log\SystemLoggerInterface', '', 'logger', '6d57d95a1c3cd7528e3e6ea15012dac8', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Log\SystemLoggerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Security\Context', 'TYPO3\Flow\Security\Context', 'securityContext', '48836470c14129ade5f39e28c4816673', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\Context'); });
        $this->setMonitorEntitiesSetting(\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get(\TYPO3\Flow\Configuration\ConfigurationManager::class)->getConfiguration('Settings', 'TYPO3.Neos.eventLog.monitorEntities'));
        $this->Flow_Injected_Properties = array (
  0 => 'entityManager',
  1 => 'eventEmittingService',
  2 => 'eelEvaluator',
  3 => 'logger',
  4 => 'securityContext',
  5 => 'monitorEntitiesSetting',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Neos/Classes/TYPO3/Neos/EventLog/Integrations/EntityIntegrationService.php
#