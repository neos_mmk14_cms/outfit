<?php 
namespace TYPO3\Neos\Service;

/*
 * This file is part of the TYPO3.Neos package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Cache\Frontend\VariableFrontend;
use TYPO3\Flow\Error\Result;
use TYPO3\Flow\I18n\Exception;
use TYPO3\Flow\I18n\Xliff\XliffParser;
use TYPO3\Flow\Package\PackageManagerInterface;
use TYPO3\Flow\Utility\Arrays;
use TYPO3\Flow\Utility\Files;
use TYPO3\Flow\I18n\Locale;
use TYPO3\Flow\I18n\Service as LocalizationService;
use TYPO3\Flow\Utility\Unicode\Functions as UnicodeFunctions;

/**
 * The XLIFF service provides methods to find XLIFF files and parse them to json
 *
 * @Flow\Scope("singleton")
 */
class XliffService_Original
{
    /**
     * A relative path for translations inside the package resources.
     *
     * @var string
     */
    protected $xliffBasePath = 'Private/Translations/';

    /**
     * @Flow\Inject
     * @var XliffParser
     */
    protected $xliffParser;

    /**
     * @Flow\Inject
     * @var LocalizationService
     */
    protected $localizationService;

    /**
     * @Flow\Inject
     * @var VariableFrontend
     */
    protected $xliffToJsonTranslationsCache;

    /**
     * @Flow\InjectConfiguration(path="userInterface.scrambleTranslatedLabels", package="TYPO3.Neos")
     * @var boolean
     */
    protected $scrambleTranslatedLabels = false;

    /**
     * @Flow\InjectConfiguration(path="userInterface.translation.autoInclude", package="TYPO3.Neos")
     * @var array
     */
    protected $packagesRegisteredForAutoInclusion = [];

    /**
     * @Flow\Inject
     * @var PackageManagerInterface
     */
    protected $packageManager;

    /**
     * Return the json array for a given locale, sourceCatalog, xliffPath and package.
     * The json will be cached.
     *
     * @param Locale $locale The locale
     * @return Result
     * @throws Exception
     */
    public function getCachedJson(Locale $locale)
    {
        $cacheIdentifier = md5($locale);

        if ($this->xliffToJsonTranslationsCache->has($cacheIdentifier)) {
            $json = $this->xliffToJsonTranslationsCache->get($cacheIdentifier);
        } else {
            $labels = [];
            $localeChain = $this->localizationService->getLocaleChain($locale);

            foreach ($this->packagesRegisteredForAutoInclusion as $packageKey => $sourcesToBeIncluded) {
                if (!is_array($sourcesToBeIncluded)) {
                    continue;
                }

                $translationBasePath = Files::concatenatePaths([
                    $this->packageManager->getPackage($packageKey)->getResourcesPath(),
                    $this->xliffBasePath
                ]);

                // We merge labels in the chain from the worst choice to best choice
                foreach (array_reverse($localeChain) as $allowedLocale) {
                    $localeSourcePath = Files::getNormalizedPath(Files::concatenatePaths([$translationBasePath, $allowedLocale]));
                    foreach ($sourcesToBeIncluded as $sourceName) {
                        foreach (glob($localeSourcePath . $sourceName . '.xlf') as $xliffPathAndFilename) {
                            $xliffPathInfo = pathinfo($xliffPathAndFilename);
                            $sourceName = str_replace($localeSourcePath, '', $xliffPathInfo['dirname'] . '/' . $xliffPathInfo['filename']);
                            $labels = Arrays::arrayMergeRecursiveOverrule($labels, $this->parseXliffToArray($xliffPathAndFilename, $packageKey, $sourceName));
                        }
                    }
                }
            }

            $json = json_encode($labels);
            $this->xliffToJsonTranslationsCache->set($cacheIdentifier, $json);
        }

        return $json;
    }

    /**
     * Read the xliff file and create the desired json
     *
     * @param string $xliffPathAndFilename The file to read
     * @param string $packageKey
     * @param string $sourceName
     * @return array
     *
     * @todo remove the override handling once Flow takes care of that, see FLOW-61
     */
    public function parseXliffToArray($xliffPathAndFilename, $packageKey, $sourceName)
    {
        /** @var array $parsedData */
        $parsedData = $this->xliffParser->getParsedData($xliffPathAndFilename);
        $arrayData = array();
        foreach ($parsedData['translationUnits'] as $key => $value) {
            $valueToStore = !empty($value[0]['target']) ? $value[0]['target'] : $value[0]['source'];

            if ($this->scrambleTranslatedLabels) {
                $valueToStore = str_repeat('#', UnicodeFunctions::strlen($valueToStore));
            }

            $this->setArrayDataValue($arrayData, str_replace('.', '_', $packageKey) . '.' . str_replace('/', '_', $sourceName) . '.' . str_replace('.', '_', $key), $valueToStore);
        }

        return $arrayData;
    }

    /**
     * @return integer The current cache version identifier
     */
    public function getCacheVersion()
    {
        $version = $this->xliffToJsonTranslationsCache->get('ConfigurationVersion');
        if ($version === false) {
            $version = time();
            $this->xliffToJsonTranslationsCache->set('ConfigurationVersion', (string)$version);
        }
        return $version;
    }

    /**
     * Helper method to create the needed json array from a dotted xliff id
     *
     * @param array $arrayPointer
     * @param string $key
     * @param string $value
     * @return void
     */
    protected function setArrayDataValue(array &$arrayPointer, $key, $value)
    {
        $keys = explode('.', $key);

        // Extract the last key
        $lastKey = array_pop($keys);

        // Walk/build the array to the specified key
        while ($arrayKey = array_shift($keys)) {
            if (!array_key_exists($arrayKey, $arrayPointer)) {
                $arrayPointer[$arrayKey] = array();
            }
            $arrayPointer = &$arrayPointer[$arrayKey];
        }

        // Set the final key
        $arrayPointer[$lastKey] = $value;
    }
}
namespace TYPO3\Neos\Service;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * The XLIFF service provides methods to find XLIFF files and parse them to json
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class XliffService extends XliffService_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        if (get_class($this) === 'TYPO3\Neos\Service\XliffService') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Neos\Service\XliffService', $this);
        if ('TYPO3\Neos\Service\XliffService' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'xliffBasePath' => 'string',
  'xliffParser' => 'TYPO3\\Flow\\I18n\\Xliff\\XliffParser',
  'localizationService' => 'TYPO3\\Flow\\I18n\\Service',
  'xliffToJsonTranslationsCache' => 'TYPO3\\Flow\\Cache\\Frontend\\VariableFrontend',
  'scrambleTranslatedLabels' => 'boolean',
  'packagesRegisteredForAutoInclusion' => 'array',
  'packageManager' => 'TYPO3\\Flow\\Package\\PackageManagerInterface',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {
        if (get_class($this) === 'TYPO3\Neos\Service\XliffService') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Neos\Service\XliffService', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('', '', 'xliffToJsonTranslationsCache', '9477adc6eec7e860cfcf96b174d29bf0', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Cache\CacheManager')->getCache('TYPO3_Neos_XliffToJsonTranslations'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\I18n\Xliff\XliffParser', 'TYPO3\Flow\I18n\Xliff\XliffParser', 'xliffParser', '0576b29ad0b8f91e49ad1d210fffe5c1', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\I18n\Xliff\XliffParser'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\I18n\Service', 'TYPO3\Flow\I18n\Service', 'localizationService', 'd147918505b040be63714e111bab34f3', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\I18n\Service'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Package\PackageManagerInterface', 'TYPO3\Flow\Package\PackageManager', 'packageManager', 'aad0cdb65adb124cf4b4d16c5b42256c', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Package\PackageManagerInterface'); });
        $this->scrambleTranslatedLabels = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get(\TYPO3\Flow\Configuration\ConfigurationManager::class)->getConfiguration('Settings', 'TYPO3.Neos.userInterface.scrambleTranslatedLabels');
        $this->packagesRegisteredForAutoInclusion = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get(\TYPO3\Flow\Configuration\ConfigurationManager::class)->getConfiguration('Settings', 'TYPO3.Neos.userInterface.translation.autoInclude');
        $this->Flow_Injected_Properties = array (
  0 => 'xliffToJsonTranslationsCache',
  1 => 'xliffParser',
  2 => 'localizationService',
  3 => 'packageManager',
  4 => 'scrambleTranslatedLabels',
  5 => 'packagesRegisteredForAutoInclusion',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Neos/Classes/TYPO3/Neos/Service/XliffService.php
#