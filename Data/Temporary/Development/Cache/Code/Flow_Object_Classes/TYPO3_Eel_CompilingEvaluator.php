<?php 
namespace TYPO3\Eel;

/*
 * This file is part of the TYPO3.Eel package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Cache\Frontend\PhpFrontend;

/**
 * An evaluator that compiles expressions down to PHP code
 *
 * This simple implementation will lazily parse and evaluate the generated PHP
 * code into a function with a name built from the hashed expression.
 *
 * @Flow\Scope("singleton")
 */
class CompilingEvaluator_Original implements EelEvaluatorInterface
{
    /**
     * @var array
     */
    protected $newExpressions = [];

    /**
     * @Flow\Inject(lazy=false)
     * @var PhpFrontend
     */
    protected $expressionCache;

    /**
     * Initialize the Evaluator
     */
    public function initializeObject()
    {
        $this->expressionCache->requireOnce('cachedExpressionClosures');
    }

    /**
     * Shutdown the Evaluator
     */
    public function shutdownObject()
    {
        if (count($this->newExpressions) > 0) {
            $changesToPersist = false;
            $codeToBeCached = $this->expressionCache->get('cachedExpressionClosures');
            /**
             * At this point a race condition could happen, that we try to prevent with an additional check.
             * So we compare the evaluated expressions during this request with the methods the cache has at
             * this point and only add methods that are not present. Only if we added anything we write the cache.
             */
            foreach ($this->newExpressions as $functionName => $newExpression) {
                if (strpos($codeToBeCached, $functionName) === false) {
                    $codeToBeCached .= 'if (!function_exists(\'' . $functionName . '\')) { ' . $newExpression . ' }' . chr(10);
                    $changesToPersist = true;
                }
            }

            if ($changesToPersist) {
                $this->expressionCache->set('cachedExpressionClosures', $codeToBeCached);
            }
        }
    }

    /**
     * Evaluate an expression under a given context
     *
     * @param string $expression
     * @param Context $context
     * @return mixed
     */
    public function evaluate($expression, Context $context)
    {
        $expression = trim($expression);
        $identifier = md5($expression);
        $functionName = 'expression_' . $identifier;

        if (!function_exists($functionName)) {
            $code = $this->generateEvaluatorCode($expression);
            $functionDeclaration = 'function ' . $functionName . '($context){return ' . $code . ';}';
            $this->newExpressions[$functionName] = $functionDeclaration;
            eval($functionDeclaration);
        }

        $result = $functionName($context);
        if ($result instanceof Context) {
            return $result->unwrap();
        } else {
            return $result;
        }
    }

    /**
     * Internal generator method
     *
     * Used by unit tests to debug generated PHP code.
     *
     * @param string $expression
     * @return string
     * @throws ParserException
     */
    protected function generateEvaluatorCode($expression)
    {
        $parser = new CompilingEelParser($expression);
        $result = $parser->match_Expression();

        if ($result === false) {
            throw new ParserException(sprintf('Expression "%s" could not be parsed.', $expression), 1344513194);
        } elseif ($parser->pos !== strlen($expression)) {
            throw new ParserException(sprintf('Expression "%s" could not be parsed. Error starting at character %d: "%s".', $expression, $parser->pos, substr($expression, $parser->pos)), 1327682383);
        } elseif (!array_key_exists('code', $result)) {
            throw new ParserException(sprintf('Parser error, no code in result %s ', json_encode($result)), 1334491498);
        }
        return $result['code'];
    }
}
namespace TYPO3\Eel;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * An evaluator that compiles expressions down to PHP code
 * 
 * This simple implementation will lazily parse and evaluate the generated PHP
 * code into a function with a name built from the hashed expression.
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class CompilingEvaluator extends CompilingEvaluator_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        if (get_class($this) === 'TYPO3\Eel\CompilingEvaluator') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Eel\CompilingEvaluator', $this);
        if (get_class($this) === 'TYPO3\Eel\CompilingEvaluator') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Eel\EelEvaluatorInterface', $this);
        if ('TYPO3\Eel\CompilingEvaluator' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }

        $isSameClass = get_class($this) === 'TYPO3\Eel\CompilingEvaluator';
        if ($isSameClass) {
            $this->initializeObject(1);
        }

        $isSameClass = get_class($this) === 'TYPO3\Eel\CompilingEvaluator';
        if ($isSameClass) {
            \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->registerShutdownObject($this, 'shutdownObject');
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'newExpressions' => 'array',
  'expressionCache' => 'TYPO3\\Flow\\Cache\\Frontend\\PhpFrontend',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {
        if (get_class($this) === 'TYPO3\Eel\CompilingEvaluator') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Eel\CompilingEvaluator', $this);
        if (get_class($this) === 'TYPO3\Eel\CompilingEvaluator') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Eel\EelEvaluatorInterface', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
            $result = NULL;

        $isSameClass = get_class($this) === 'TYPO3\Eel\CompilingEvaluator';
        $classParents = class_parents($this);
        $classImplements = class_implements($this);
        $isClassProxy = array_search('TYPO3\Eel\CompilingEvaluator', $classParents) !== FALSE && array_search('Doctrine\ORM\Proxy\Proxy', $classImplements) !== FALSE;

        if ($isSameClass || $isClassProxy) {
            $this->initializeObject(2);
        }

        $isSameClass = get_class($this) === 'TYPO3\Eel\CompilingEvaluator';
        $classParents = class_parents($this);
        $classImplements = class_implements($this);
        $isClassProxy = array_search('TYPO3\Eel\CompilingEvaluator', $classParents) !== FALSE && array_search('Doctrine\ORM\Proxy\Proxy', $classImplements) !== FALSE;

        if ($isSameClass || $isClassProxy) {
            \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->registerShutdownObject($this, 'shutdownObject');
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('', '', 'expressionCache', '991863581f543cf0f9e83125c30b42c8', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Cache\CacheManager')->getCache('Eel_Expression_Code'); });
        $this->Flow_Injected_Properties = array (
  0 => 'expressionCache',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Framework/TYPO3.Eel/Classes/TYPO3/Eel/CompilingEvaluator.php
#