<?php 
namespace TYPO3\Party\Domain\Model;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "Party".                 *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * A person name
 *
 * @Flow\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 */
class PersonName_Original {

	/**
	 * @var string
	 */
	protected $title;

	/**
	 * @var string
	 */
	protected $firstName;

	/**
	 * @var string
	 */
	protected $middleName;

	/**
	 * @var string
	 */
	protected $lastName;

	/**
	 * @var string
	 */
	protected $otherName;

	/**
	 * @var string
	 */
	protected $alias;

	/**
	 * @var string
	 */
	protected $fullName;

	/**
	 * Constructs this person name
	 *
	 * @param string $title the title, e.g. "Mr." or "Mr. Phd"
	 * @param string $firstName the first name
	 * @param string $middleName the middle name
	 * @param string $lastName the last name
	 * @param string $otherName the "other" name, e.g. "IV." or "jr."
	 * @param string $alias an alias or nickname
	 * @var string
	 */
	public function __construct($title = '', $firstName = '', $middleName = '', $lastName = '', $otherName = '', $alias = '') {
		$this->title = $title;
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
		$this->otherName = $otherName;
		$this->alias = $alias;

		$this->generateFullName();
	}

	/**
	 * @return void
	 */
	protected function generateFullName() {
		$nameParts = array(
			$this->title,
			$this->firstName,
			$this->middleName,
			$this->lastName,
			$this->otherName
		);
		$nameParts = array_map('trim', $nameParts);
		$filledNameParts = array();
		foreach ($nameParts as $namePart) {
			if($namePart !== '') {
				$filledNameParts[] = $namePart;
			}
		}
		$this->fullName = implode(' ', $filledNameParts);
	}

	/**
	 * Setter for firstName
	 *
	 * @param string $firstName
	 * @return void
	 */
	public function setFirstName($firstName) {
		$this->firstName = $firstName;
		$this->generateFullName();
	}

	/**
	 * Setter for middleName
	 *
	 * @param string $middleName
	 * @return void
	 */
	public function setMiddleName($middleName) {
		$this->middleName = $middleName;
		$this->generateFullName();
	}

	/**
	 * Setter for lastName
	 *
	 * @param string $lastName
	 * @return void
	 */
	public function setLastName($lastName) {
		$this->lastName = $lastName;
		$this->generateFullName();
	}

	/**
	 * Setter for title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
		$this->generateFullName();
	}

	/**
	 * Setter for otherName
	 *
	 * @param string $otherName
	 * @return void
	 */
	public function setOtherName($otherName) {
		$this->otherName = $otherName;
		$this->generateFullName();
	}

	/**
	 * Setter for alias
	 *
	 * @param string $alias
	 * @return void
	 */
	public function setAlias($alias) {
		$this->alias = $alias;
	}

	/**
	 * Getter for firstName
	 *
	 * @return string
	 */
	public function getFirstName() {
		return $this->firstName;
	}

	/**
	 * Getter for middleName
	 *
	 * @return string
	 */
	public function getMiddleName() {
		return $this->middleName;
	}

	/**
	 * Getter for lastName
	 *
	 * @return string
	 */
	public function getLastName() {
		return $this->lastName;
	}

	/**
	 * Getter for title
	 *
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Getter for otherName
	 *
	 * @return string
	 */
	public function getOtherName() {
		return $this->otherName;
	}

	/**
	 * Getter for alias
	 *
	 * @return string
	 */
	public function getAlias() {
		return $this->alias;
	}

	/**
	 * Returns the full name, e.g. "Mr. PhD John W. Doe"
	 *
	 * @return string The full person name
	 */
	public function getFullName() {
		return $this->fullName;
	}

	/**
	 * An alias for getFullName()
	 *
	 * @return string The full person name
	 */
	public function  __toString() {
		return $this->fullName;
	}
}
namespace TYPO3\Party\Domain\Model;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * A person name
 * @\TYPO3\Flow\Annotations\Entity
 * @\Doctrine\ORM\Mapping\InheritanceType("SINGLE_TABLE")
 */
class PersonName extends PersonName_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface, \TYPO3\Flow\Persistence\Aspect\PersistenceMagicInterface {

    use \TYPO3\Flow\Aop\AdvicesTrait, \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait;

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(length=40)
     * introduced by TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect
     */
    protected $Persistence_Object_Identifier = NULL;

    private $Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array();

    private $Flow_Aop_Proxy_groupedAdviceChains = array();

    private $Flow_Aop_Proxy_methodIsInAdviceMode = array();


    /**
     * Autogenerated Proxy Method
     * @param string $title the title, e.g. "Mr." or "Mr. Phd
     * @param string $firstName the first name
     * @param string $middleName the middle name
     * @param string $lastName the last name
     * @param string $otherName the "other" name, e.g. "IV." or "jr.
     * @param string $alias an alias or nickname
     */
    public function __construct()
    {
        $arguments = func_get_args();

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['__construct'])) {
        call_user_func_array('parent::__construct', $arguments);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['__construct'] = TRUE;
            try {
            
                $methodArguments = [];

                if (array_key_exists(0, $arguments)) $methodArguments['title'] = $arguments[0];
                if (array_key_exists(1, $arguments)) $methodArguments['firstName'] = $arguments[1];
                if (array_key_exists(2, $arguments)) $methodArguments['middleName'] = $arguments[2];
                if (array_key_exists(3, $arguments)) $methodArguments['lastName'] = $arguments[3];
                if (array_key_exists(4, $arguments)) $methodArguments['otherName'] = $arguments[4];
                if (array_key_exists(5, $arguments)) $methodArguments['alias'] = $arguments[5];
            
                if (isset($this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['__construct']['TYPO3\Flow\Aop\Advice\BeforeAdvice'])) {
                    $advices = $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['__construct']['TYPO3\Flow\Aop\Advice\BeforeAdvice'];
                    $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Party\Domain\Model\PersonName', '__construct', $methodArguments);
                    foreach ($advices as $advice) {
                        $advice->invoke($joinPoint);
                    }

                    $methodArguments = $joinPoint->getMethodArguments();
                }

                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Party\Domain\Model\PersonName', '__construct', $methodArguments);
                $result = $this->Flow_Aop_Proxy_invokeJoinPoint($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['__construct']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['__construct']);
            return;
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    protected function Flow_Aop_Proxy_buildMethodsAndAdvicesArray()
    {
        if (method_exists(get_parent_class(), 'Flow_Aop_Proxy_buildMethodsAndAdvicesArray') && is_callable('parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray')) parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        $objectManager = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager;
        $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array(
            '__clone' => array(
                'TYPO3\Flow\Aop\Advice\BeforeAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\BeforeAdvice('TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect', 'generateUuid', $objectManager, NULL),
                ),
                'TYPO3\Flow\Aop\Advice\AfterReturningAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AfterReturningAdvice('TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect', 'cloneObject', $objectManager, NULL),
                ),
            ),
            '__construct' => array(
                'TYPO3\Flow\Aop\Advice\BeforeAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\BeforeAdvice('TYPO3\Flow\Persistence\Aspect\PersistenceMagicAspect', 'generateUuid', $objectManager, NULL),
                ),
            ),
        );
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        $this->Flow_setRelatedEntities();
            $result = NULL;
        if (method_exists(get_parent_class(), '__wakeup') && is_callable('parent::__wakeup')) parent::__wakeup();
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __clone()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['__clone'])) {
            $result = NULL;

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['__clone'] = TRUE;
            try {
            
                $methodArguments = [];

                if (isset($this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['__clone']['TYPO3\Flow\Aop\Advice\BeforeAdvice'])) {
                    $advices = $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['__clone']['TYPO3\Flow\Aop\Advice\BeforeAdvice'];
                    $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Party\Domain\Model\PersonName', '__clone', $methodArguments);
                    foreach ($advices as $advice) {
                        $advice->invoke($joinPoint);
                    }

                    $methodArguments = $joinPoint->getMethodArguments();
                }

                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Party\Domain\Model\PersonName', '__clone', $methodArguments);
                $result = $this->Flow_Aop_Proxy_invokeJoinPoint($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

                if (isset($this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['__clone']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'])) {
                    $advices = $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices['__clone']['TYPO3\Flow\Aop\Advice\AfterReturningAdvice'];
                    $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Party\Domain\Model\PersonName', '__clone', $methodArguments, NULL, $result);
                    foreach ($advices as $advice) {
                        $advice->invoke($joinPoint);
                    }

                    $methodArguments = $joinPoint->getMethodArguments();
                }

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['__clone']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['__clone']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'title' => 'string',
  'firstName' => 'string',
  'middleName' => 'string',
  'lastName' => 'string',
  'otherName' => 'string',
  'alias' => 'string',
  'fullName' => 'string',
  'Persistence_Object_Identifier' => 'string',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Party/Classes/TYPO3/Party/Domain/Model/PersonName.php
#