<?php 
namespace TYPO3\Neos\Domain\Service;

/*
 * This file is part of the TYPO3.Neos package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Utility\Arrays;
use TYPO3\Neos\Domain\Model\Domain;
use TYPO3\Neos\Domain\Model\Site;
use TYPO3\TYPO3CR\Domain\Service\ContextFactory;
use TYPO3\TYPO3CR\Exception\InvalidNodeContextException;

/**
 * ContentContextFactory which ensures contexts stay unique. Make sure to
 * get ContextFactoryInterface injected instead of this class.
 *
 * See \TYPO3\TYPO3CR\Domain\Service\ContextFactory->build for detailed
 * explanations about the usage.
 *
 * @Flow\Scope("singleton")
 */
class ContentContextFactory_Original extends ContextFactory
{
    /**
     * @Flow\Inject
     * @var \TYPO3\Neos\Domain\Repository\DomainRepository
     */
    protected $domainRepository;

    /**
     * @Flow\Inject
     * @var \TYPO3\Neos\Domain\Repository\SiteRepository
     */
    protected $siteRepository;

    /**
     * The context implementation this factory will create
     *
     * @var string
     */
    protected $contextImplementation = ContentContext::class;

    /**
     * Creates the actual Context instance.
     * This needs to be overridden if the Builder is extended.
     *
     * @param array $contextProperties
     * @return ContentContext
     */
    protected function buildContextInstance(array $contextProperties)
    {
        $contextProperties = $this->removeDeprecatedProperties($contextProperties);

        return new ContentContext(
            $contextProperties['workspaceName'],
            $contextProperties['currentDateTime'],
            $contextProperties['dimensions'],
            $contextProperties['targetDimensions'],
            $contextProperties['invisibleContentShown'],
            $contextProperties['removedContentShown'],
            $contextProperties['inaccessibleContentShown'],
            $contextProperties['currentSite'],
            $contextProperties['currentDomain']
        );
    }

    /**
     * Merges the given context properties with sane defaults for the context implementation.
     *
     * @param array $contextProperties
     * @return array
     */
    protected function mergeContextPropertiesWithDefaults(array $contextProperties)
    {
        $contextProperties = $this->removeDeprecatedProperties($contextProperties);

        $defaultContextProperties = array(
            'workspaceName' => 'live',
            'currentDateTime' => $this->now,
            'dimensions' => array(),
            'targetDimensions' => array(),
            'invisibleContentShown' => false,
            'removedContentShown' => false,
            'inaccessibleContentShown' => false,
            'currentSite' => null,
            'currentDomain' => null
        );

        if (!isset($contextProperties['currentSite'])) {
            $defaultContextProperties = $this->setDefaultSiteAndDomainFromCurrentRequest($defaultContextProperties);
        }

        $mergedProperties = Arrays::arrayMergeRecursiveOverrule($defaultContextProperties, $contextProperties, true);

        $this->mergeDimensionValues($contextProperties, $mergedProperties);
        $this->mergeTargetDimensionContextProperties($contextProperties, $mergedProperties, $defaultContextProperties);

        return $mergedProperties;
    }

    /**
     * Determines the current domain and site from the request and sets the resulting values as
     * as defaults.
     *
     * @param array $defaultContextProperties
     * @return array
     */
    protected function setDefaultSiteAndDomainFromCurrentRequest(array $defaultContextProperties)
    {
        $currentDomain = $this->domainRepository->findOneByActiveRequest();
        if ($currentDomain !== null) {
            $defaultContextProperties['currentSite'] = $currentDomain->getSite();
            $defaultContextProperties['currentDomain'] = $currentDomain;
        } else {
            $defaultContextProperties['currentSite'] = $this->siteRepository->findFirstOnline();
        }

        return $defaultContextProperties;
    }


    /**
     * This creates the actual identifier and needs to be overridden by builders extending this.
     *
     * @param array $contextProperties
     * @return string
     */
    protected function getIdentifierSource(array $contextProperties)
    {
        ksort($contextProperties);
        $identifierSource = $this->contextImplementation;
        foreach ($contextProperties as $propertyName => $propertyValue) {
            if ($propertyName === 'dimensions') {
                $stringParts = array();
                foreach ($propertyValue as $dimensionName => $dimensionValues) {
                    $stringParts[] = $dimensionName . '=' . implode(',', $dimensionValues);
                }
                $stringValue = implode('&', $stringParts);
            } elseif ($propertyName === 'targetDimensions') {
                $stringParts = array();
                foreach ($propertyValue as $dimensionName => $dimensionValue) {
                    $stringParts[] = $dimensionName . '=' . $dimensionValue;
                }
                $stringValue = implode('&', $stringParts);
            } elseif ($propertyValue instanceof \DateTimeInterface) {
                $stringValue = $propertyValue->getTimestamp();
            } elseif ($propertyValue instanceof Site) {
                $stringValue = $propertyValue->getNodeName();
            } elseif ($propertyValue instanceof Domain) {
                $stringValue = $propertyValue->getHostPattern();
            } else {
                $stringValue = (string)$propertyValue;
            }
            $identifierSource .= ':' . $stringValue;
        }

        return $identifierSource;
    }

    /**
     * @param array $contextProperties
     * @return void
     * @throws InvalidNodeContextException
     */
    protected function validateContextProperties($contextProperties)
    {
        parent::validateContextProperties($contextProperties);

        if (isset($contextProperties['currentSite'])) {
            if (!$contextProperties['currentSite'] instanceof Site) {
                throw new InvalidNodeContextException('You tried to set currentSite in the context and did not provide a \\TYPO3\Neos\\Domain\\Model\\Site object as value.', 1373145297);
            }
        }
        if (isset($contextProperties['currentDomain'])) {
            if (!$contextProperties['currentDomain'] instanceof Domain) {
                throw new InvalidNodeContextException('You tried to set currentDomain in the context and did not provide a \\TYPO3\Neos\\Domain\\Model\\Domain object as value.', 1373145384);
            }
        }
    }
}
namespace TYPO3\Neos\Domain\Service;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * ContentContextFactory which ensures contexts stay unique. Make sure to
 * get ContextFactoryInterface injected instead of this class.
 * 
 * See \TYPO3\TYPO3CR\Domain\Service\ContextFactory->build for detailed
 * explanations about the usage.
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class ContentContextFactory extends ContentContextFactory_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        if (method_exists(get_parent_class(), 'Flow_Aop_Proxy_buildMethodsAndAdvicesArray') && is_callable('parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray')) parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
        if (get_class($this) === 'TYPO3\Neos\Domain\Service\ContentContextFactory') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Neos\Domain\Service\ContentContextFactory', $this);
        if (get_class($this) === 'TYPO3\Neos\Domain\Service\ContentContextFactory') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\TYPO3CR\Domain\Service\ContextFactoryInterface', $this);
        if ('TYPO3\Neos\Domain\Service\ContentContextFactory' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {
        if (method_exists(get_parent_class(), 'Flow_Aop_Proxy_buildMethodsAndAdvicesArray') && is_callable('parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray')) parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
        if (get_class($this) === 'TYPO3\Neos\Domain\Service\ContentContextFactory') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Neos\Domain\Service\ContentContextFactory', $this);
        if (get_class($this) === 'TYPO3\Neos\Domain\Service\ContentContextFactory') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\TYPO3CR\Domain\Service\ContextFactoryInterface', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'domainRepository' => '\\TYPO3\\Neos\\Domain\\Repository\\DomainRepository',
  'siteRepository' => '\\TYPO3\\Neos\\Domain\\Repository\\SiteRepository',
  'contextImplementation' => 'string',
  'contextInstances' => 'array<TYPO3\\TYPO3CR\\Domain\\Service\\Context>',
  'contentDimensionRepository' => 'TYPO3\\TYPO3CR\\Domain\\Repository\\ContentDimensionRepository',
  'now' => 'TYPO3\\Flow\\Utility\\Now',
  'securityContext' => 'TYPO3\\Flow\\Security\\Context',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Neos\Domain\Repository\DomainRepository', 'TYPO3\Neos\Domain\Repository\DomainRepository', 'domainRepository', '6f2987c5f47777b01540a314d984b09c', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Neos\Domain\Repository\DomainRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Neos\Domain\Repository\SiteRepository', 'TYPO3\Neos\Domain\Repository\SiteRepository', 'siteRepository', '5c3f2ab0e14ff0be3090c1f3efe77d7a', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Neos\Domain\Repository\SiteRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Repository\ContentDimensionRepository', 'TYPO3\TYPO3CR\Domain\Repository\ContentDimensionRepository', 'contentDimensionRepository', 'c4ce8954e3d47ef3fdb068b6c07c9ebb', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Repository\ContentDimensionRepository'); });
        $this->now = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Utility\Now');
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Security\Context', 'TYPO3\Flow\Security\Context', 'securityContext', '48836470c14129ade5f39e28c4816673', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\Context'); });
        $this->Flow_Injected_Properties = array (
  0 => 'domainRepository',
  1 => 'siteRepository',
  2 => 'contentDimensionRepository',
  3 => 'now',
  4 => 'securityContext',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Neos/Classes/TYPO3/Neos/Domain/Service/ContentContextFactory.php
#