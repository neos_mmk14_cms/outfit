<?php 
namespace TYPO3\Neos\Controller\Module\Administration;

/*
 * This file is part of the TYPO3.Neos package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Error\Error;
use TYPO3\Flow\Error\Message;
use TYPO3\Flow\Error\Warning;
use TYPO3\Flow\Package;
use TYPO3\Flow\Package\Exception\ProtectedPackageKeyException;
use TYPO3\Flow\Package\Exception\UnknownPackageException;
use TYPO3\Flow\Package\Exception;
use TYPO3\Neos\Controller\Module\AbstractModuleController;

/**
 * The TYPO3 Package Management module controller
 *
 * @Flow\Scope("singleton")
 */
class PackagesController_Original extends AbstractModuleController
{
    /**
     * @Flow\Inject
     * @var \TYPO3\Flow\Package\PackageManagerInterface
     */
    protected $packageManager;

    /**
     * @return void
     */
    public function indexAction()
    {
        $packageGroups = array();
        foreach ($this->packageManager->getAvailablePackages() as $package) {
            /** @var Package $package */
            $packagePath = substr($package->getPackagepath(), strlen(FLOW_PATH_PACKAGES));
            $packageGroup = substr($packagePath, 0, strpos($packagePath, '/'));
            $packageGroups[$packageGroup][$package->getPackageKey()] = array(
                'sanitizedPackageKey' => str_replace('.', '', $package->getPackageKey()),
                'version' => $package->getInstalledVersion(),
                'name' => $package->getComposerManifest('name'),
                'type' => $package->getComposerManifest('type'),
                'description' => $package->getPackageMetaData()->getDescription(),
                'metaData' => $package->getPackageMetaData(),
                'isActive' => $this->packageManager->isPackageActive($package->getPackageKey()),
                'isFrozen' => $this->packageManager->isPackageFrozen($package->getPackageKey()),
                'isProtected' => $package->isProtected()
            );
        }
        ksort($packageGroups);
        foreach (array_keys($packageGroups) as $packageGroup) {
            ksort($packageGroups[$packageGroup]);
        }
        $this->view->assignMultiple(array(
            'packageGroups' => $packageGroups,
            'isDevelopmentContext' => $this->objectManager->getContext()->isDevelopment()
        ));
    }

    /**
     * Activate package
     *
     * @param string $packageKey Package to activate
     * @return void
     */
    public function activateAction($packageKey)
    {
        $this->flashMessageContainer->addMessage($this->activatePackage($packageKey));
        $this->redirect('index');
    }

    /**
     * Deactivate package
     *
     * @param string $packageKey Package to deactivate
     * @return void
     */
    public function deactivateAction($packageKey)
    {
        $this->flashMessageContainer->addMessage($this->deactivatePackage($packageKey));
        $this->redirect('index');
    }

    /**
     * Delete package
     *
     * @param string $packageKey Package to delete
     * @return void
     */
    public function deleteAction($packageKey)
    {
        $this->flashMessageContainer->addMessage($this->deletePackage($packageKey));
        $this->redirect('index');
    }

    /**
     * Freeze package
     *
     * @param string $packageKey Package to freeze
     * @return void
     */
    public function freezeAction($packageKey)
    {
        $this->flashMessageContainer->addMessage($this->freezePackage($packageKey));
        $this->redirect('index');
    }

    /**
     * Unfreeze package
     *
     * @param string $packageKey Package to freeze
     * @return void
     */
    public function unfreezeAction($packageKey)
    {
        $this->packageManager->unfreezePackage($packageKey);
        $this->flashMessageContainer->addMessage(new Message('%s has been unfrozen', 1347464246, array($packageKey)));
        $this->redirect('index');
    }

    /**
     * @param array $packageKeys
     * @param string $action
     * @return void
     * @throws \RuntimeException
     */
    public function batchAction(array $packageKeys, $action)
    {
        switch ($action) {
            case 'freeze':
                $frozenPackages = array();
                foreach ($packageKeys as $packageKey) {
                    $message = $this->freezePackage($packageKey);
                    if ($message instanceof Error || $message instanceof Warning) {
                        $this->flashMessageContainer->addMessage($message);
                    } else {
                        array_push($frozenPackages, $packageKey);
                    }
                }
                if (count($frozenPackages) > 0) {
                    $message = new Message('Following packages have been frozen: %s', 1412547087, array(implode(', ', $frozenPackages)));
                } else {
                    $message = new Warning('Unable to freeze the selected packages', 1412547216);
                }
            break;
            case 'unfreeze':
                foreach ($packageKeys as $packageKey) {
                    $this->packageManager->unfreezePackage($packageKey);
                }
                $message = new Message('Following packages have been unfrozen: %s', 1412547219, array(implode(', ', $packageKeys)));
            break;
            case 'activate':
                $activatedPackages = array();
                foreach ($packageKeys as $packageKey) {
                    $message = $this->activatePackage($packageKey);
                    if ($message instanceof Error || $message instanceof Warning) {
                        $this->flashMessageContainer->addMessage($message);
                    } else {
                        array_push($activatedPackages, $packageKey);
                    }
                }
                if (count($activatedPackages) > 0) {
                    $message = new Message('Following packages have been activated: %s', 1412547283, array(implode(', ', $activatedPackages)));
                } else {
                    $message = new Warning('Unable to activate the selected packages', 1412547324);
                }
            break;
            case 'deactivate':
                $deactivatedPackages = array();
                foreach ($packageKeys as $packageKey) {
                    $message = $this->deactivatePackage($packageKey);
                    if ($message instanceof Error || $message instanceof Warning) {
                        $this->flashMessageContainer->addMessage($message);
                    } else {
                        array_push($deactivatedPackages, $packageKey);
                    }
                }
                if (count($deactivatedPackages) > 0) {
                    $message = new Message('Following packages have been deactivated: %s', 1412545904, array(implode(', ', $deactivatedPackages)));
                } else {
                    $message = new Warning('Unable to deactivate the selected packages', 1412545976);
                }
            break;
            case 'delete':
                $deletedPackages = array();
                foreach ($packageKeys as $packageKey) {
                    $message = $this->deletePackage($packageKey);
                    if ($message instanceof Error || $message instanceof Warning) {
                        $this->flashMessageContainer->addMessage($message);
                    } else {
                        array_push($deletedPackages, $packageKey);
                    }
                }
                if (count($deletedPackages) > 0) {
                    $message = new Message('Following packages have been deleted: %s', 1412547479, array(implode(', ', $deletedPackages)));
                } else {
                    $message = new Warning('Unable to delete the selected packages', 1412546138);
                }
            break;
            default:
                throw new \RuntimeException('Invalid action "' . $action . '" given.', 1347463918);
        }

        $this->flashMessageContainer->addMessage($message);
        $this->redirect('index');
    }

    /**
     * @param string $packageKey
     * @return Error|Message
     */
    protected function activatePackage($packageKey)
    {
        try {
            $this->packageManager->activatePackage($packageKey);
            $message = new Message('The package %s is activated', 1343231680, array($packageKey));
        } catch (UnknownPackageException $exception) {
            $message = new Error('The package %s is not present and can not be activated', 1343231681, array($packageKey));
        }
        return $message;
    }

    /**
     * @param string $packageKey
     * @return Error|Message
     */
    protected function deactivatePackage($packageKey)
    {
        try {
            $this->packageManager->deactivatePackage($packageKey);
            $message = new Message('%s was deactivated', 1343231678, array($packageKey));
        } catch (ProtectedPackageKeyException $exception) {
            $message = new Error('The package %s is protected and can not be deactivated', 1343231679, array($packageKey));
        }
        return $message;
    }

    /**
     * @param string $packageKey
     * @return Error|Message
     */
    protected function deletePackage($packageKey)
    {
        try {
            $this->packageManager->deletePackage($packageKey);
            $message = new Message('Package %s has been deleted', 1343231685, array($packageKey));
        } catch (UnknownPackageException $exception) {
            $message = new Error($exception->getMessage(), 1343231686);
        } catch (ProtectedPackageKeyException $exception) {
            $message = new Error($exception->getMessage(), 1343231687);
        } catch (Exception $exception) {
            $message = new Error($exception->getMessage(), 1343231688);
        }
        return $message;
    }

    /**
     * @param string $packageKey
     * @return Error|Message
     */
    protected function freezePackage($packageKey)
    {
        try {
            $this->packageManager->freezePackage($packageKey);
            $message = new Message('Package %s has been frozen', 1343231689, array($packageKey));
        } catch (\LogicException $exception) {
            $message = new Error($exception->getMessage(), 1343231690);
        } catch (UnknownPackageException $exception) {
            $message = new Error($exception->getMessage(), 1343231691);
        }
        return $message;
    }
}
namespace TYPO3\Neos\Controller\Module\Administration;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * The TYPO3 Package Management module controller
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class PackagesController extends PackagesController_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Aop\AdvicesTrait, \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;

    private $Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array();

    private $Flow_Aop_Proxy_groupedAdviceChains = array();

    private $Flow_Aop_Proxy_methodIsInAdviceMode = array();


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
        if (get_class($this) === 'TYPO3\Neos\Controller\Module\Administration\PackagesController') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Neos\Controller\Module\Administration\PackagesController', $this);
        if ('TYPO3\Neos\Controller\Module\Administration\PackagesController' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }

        $isSameClass = get_class($this) === 'TYPO3\Neos\Controller\Module\Administration\PackagesController';
        if ($isSameClass) {
            $this->initializeObject(1);
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    protected function Flow_Aop_Proxy_buildMethodsAndAdvicesArray()
    {
        if (method_exists(get_parent_class(), 'Flow_Aop_Proxy_buildMethodsAndAdvicesArray') && is_callable('parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray')) parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        $objectManager = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager;
        $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array(
            'indexAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'activateAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'deactivateAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'deleteAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'freezeAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'unfreezeAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'batchAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
        );
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
        if (get_class($this) === 'TYPO3\Neos\Controller\Module\Administration\PackagesController') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Neos\Controller\Module\Administration\PackagesController', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
            $result = NULL;
        if (method_exists(get_parent_class(), '__wakeup') && is_callable('parent::__wakeup')) parent::__wakeup();

        $isSameClass = get_class($this) === 'TYPO3\Neos\Controller\Module\Administration\PackagesController';
        $classParents = class_parents($this);
        $classImplements = class_implements($this);
        $isClassProxy = array_search('TYPO3\Neos\Controller\Module\Administration\PackagesController', $classParents) !== FALSE && array_search('Doctrine\ORM\Proxy\Proxy', $classImplements) !== FALSE;

        if ($isSameClass || $isClassProxy) {
            $this->initializeObject(2);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __clone()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
    }

    /**
     * Autogenerated Proxy Method
     * @return void
     */
    public function indexAction()
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction'])) {
            $result = parent::indexAction();

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('indexAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\PackagesController', 'indexAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['indexAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param string $packageKey Package to activate
     * @return void
     */
    public function activateAction($packageKey)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['activateAction'])) {
            $result = parent::activateAction($packageKey);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['activateAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['packageKey'] = $packageKey;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('activateAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\PackagesController', 'activateAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['activateAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['activateAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param string $packageKey Package to deactivate
     * @return void
     */
    public function deactivateAction($packageKey)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deactivateAction'])) {
            $result = parent::deactivateAction($packageKey);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['deactivateAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['packageKey'] = $packageKey;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('deactivateAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\PackagesController', 'deactivateAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deactivateAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deactivateAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param string $packageKey Package to delete
     * @return void
     */
    public function deleteAction($packageKey)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAction'])) {
            $result = parent::deleteAction($packageKey);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['packageKey'] = $packageKey;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('deleteAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\PackagesController', 'deleteAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param string $packageKey Package to freeze
     * @return void
     */
    public function freezeAction($packageKey)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['freezeAction'])) {
            $result = parent::freezeAction($packageKey);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['freezeAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['packageKey'] = $packageKey;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('freezeAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\PackagesController', 'freezeAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['freezeAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['freezeAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param string $packageKey Package to freeze
     * @return void
     */
    public function unfreezeAction($packageKey)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['unfreezeAction'])) {
            $result = parent::unfreezeAction($packageKey);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['unfreezeAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['packageKey'] = $packageKey;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('unfreezeAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\PackagesController', 'unfreezeAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['unfreezeAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['unfreezeAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param array $packageKeys
     * @param string $action
     * @return void
     * @throws \RuntimeException
     */
    public function batchAction(array $packageKeys, $action)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['batchAction'])) {
            $result = parent::batchAction($packageKeys, $action);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['batchAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['packageKeys'] = $packageKeys;
                $methodArguments['action'] = $action;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('batchAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Controller\Module\Administration\PackagesController', 'batchAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['batchAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['batchAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'packageManager' => '\\TYPO3\\Flow\\Package\\PackageManagerInterface',
  'moduleConfiguration' => 'array',
  'objectManager' => 'TYPO3\\Flow\\Object\\ObjectManagerInterface',
  'reflectionService' => 'TYPO3\\Flow\\Reflection\\ReflectionService',
  'mvcPropertyMappingConfigurationService' => 'TYPO3\\Flow\\Mvc\\Controller\\MvcPropertyMappingConfigurationService',
  'viewConfigurationManager' => 'TYPO3\\Flow\\Mvc\\ViewConfigurationManager',
  'view' => 'TYPO3\\Flow\\Mvc\\View\\ViewInterface',
  'viewObjectNamePattern' => 'string',
  'viewFormatToObjectNameMap' => 'array',
  'defaultViewObjectName' => 'string',
  'actionMethodName' => 'string',
  'errorMethodName' => 'string',
  'settings' => 'array',
  'systemLogger' => 'TYPO3\\Flow\\Log\\SystemLoggerInterface',
  'uriBuilder' => 'TYPO3\\Flow\\Mvc\\Routing\\UriBuilder',
  'validatorResolver' => 'TYPO3\\Flow\\Validation\\ValidatorResolver',
  'request' => 'TYPO3\\Flow\\Mvc\\ActionRequest',
  'response' => 'TYPO3\\Flow\\Http\\Response',
  'arguments' => 'TYPO3\\Flow\\Mvc\\Controller\\Arguments',
  'controllerContext' => 'TYPO3\\Flow\\Mvc\\Controller\\ControllerContext',
  'flashMessageContainer' => 'TYPO3\\Flow\\Mvc\\FlashMessageContainer',
  'persistenceManager' => 'TYPO3\\Flow\\Persistence\\PersistenceManagerInterface',
  'supportedMediaTypes' => 'array',
  '_localizationService' => '\\TYPO3\\Flow\\I18n\\Service',
  '_userService' => '\\TYPO3\\Neos\\Service\\UserService',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->injectSettings(\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get(\TYPO3\Flow\Configuration\ConfigurationManager::class)->getConfiguration('Settings', 'TYPO3.Neos'));
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Package\PackageManagerInterface', 'TYPO3\Flow\Package\PackageManager', 'packageManager', 'aad0cdb65adb124cf4b4d16c5b42256c', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Package\PackageManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Object\ObjectManagerInterface', 'TYPO3\Flow\Object\ObjectManager', 'objectManager', '0c3c44be7be16f2a287f1fb2d068dde4', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Object\ObjectManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Reflection\ReflectionService', 'TYPO3\Flow\Reflection\ReflectionService', 'reflectionService', '921ad637f16d2059757a908fceaf7076', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Reflection\ReflectionService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService', 'TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService', 'mvcPropertyMappingConfigurationService', '35acb49fbe78f28099d45aa647797c83', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\ViewConfigurationManager', 'TYPO3\Flow\Mvc\ViewConfigurationManager', 'viewConfigurationManager', '5a345bfd515fdb9f0c97080ff13c7079', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\ViewConfigurationManager'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Log\SystemLoggerInterface', '', 'systemLogger', '6d57d95a1c3cd7528e3e6ea15012dac8', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Log\SystemLoggerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Validation\ValidatorResolver', 'TYPO3\Flow\Validation\ValidatorResolver', 'validatorResolver', 'b457db29305ddeae13b61d92da000ca0', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Validation\ValidatorResolver'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\FlashMessageContainer', 'TYPO3\Flow\Mvc\FlashMessageContainer', 'flashMessageContainer', 'e4fd26f8afd3994317304b563b2a9561', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\FlashMessageContainer'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Persistence\PersistenceManagerInterface', 'TYPO3\Flow\Persistence\Doctrine\PersistenceManager', 'persistenceManager', 'f1bc82ad47156d95485678e33f27c110', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\I18n\Service', 'TYPO3\Flow\I18n\Service', '_localizationService', 'd147918505b040be63714e111bab34f3', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\I18n\Service'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Neos\Service\UserService', 'TYPO3\Neos\Service\UserService', '_userService', 'bede53034a0bcd605fa08b132fe980ca', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Neos\Service\UserService'); });
        $this->Flow_Injected_Properties = array (
  0 => 'settings',
  1 => 'packageManager',
  2 => 'objectManager',
  3 => 'reflectionService',
  4 => 'mvcPropertyMappingConfigurationService',
  5 => 'viewConfigurationManager',
  6 => 'systemLogger',
  7 => 'validatorResolver',
  8 => 'flashMessageContainer',
  9 => 'persistenceManager',
  10 => '_localizationService',
  11 => '_userService',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Neos/Classes/TYPO3/Neos/Controller/Module/Administration/PackagesController.php
#