<?php 
namespace TYPO3\Eel\FlowQuery;

/*
 * This file is part of the TYPO3.Eel package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Eel\FlowQuery\OperationInterface;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Object\ObjectManagerInterface;
use TYPO3\Flow\Reflection\ReflectionService;

/**
 * FlowQuery Operation Resolver
 *
 * @Flow\Scope("singleton")
 */
class OperationResolver_Original implements OperationResolverInterface
{
    /**
     * @var ObjectManagerInterface
     * @Flow\Inject
     */
    protected $objectManager;

    /**
     * @var ReflectionService
     * @Flow\Inject
     */
    protected $reflectionService;

    /**
     * 2-dimensional array of registered operations:
     * shortOperationName => priority => operation class name
     *
     * @var array
     */
    protected $operations = [];

    /**
     * associative array of registered final operations:
     * shortOperationName => shortOperationName
     *
     * @var array
     */
    protected $finalOperationNames = [];

    /**
     * Initializer, building up $this->operations and $this->finalOperationNames
     */
    public function initializeObject()
    {
        $operationsAndFinalOperationNames = static::buildOperationsAndFinalOperationNames($this->objectManager);
        $this->operations = $operationsAndFinalOperationNames[0];
        $this->finalOperationNames = $operationsAndFinalOperationNames[1];
    }

    /**
     * @param ObjectManagerInterface $objectManager
     * @return array Array of sorted operations and array of final operation names
     * @throws FlowQueryException
     * @Flow\CompileStatic
     */
    public static function buildOperationsAndFinalOperationNames($objectManager)
    {
        $operations = [];
        $finalOperationNames = [];

        $reflectionService = $objectManager->get(ReflectionService::class);
        $operationClassNames = $reflectionService->getAllImplementationClassNamesForInterface(OperationInterface::class);
        /** @var $operationClassName OperationInterface */
        foreach ($operationClassNames as $operationClassName) {
            $shortOperationName = $operationClassName::getShortName();
            $operationPriority = $operationClassName::getPriority();
            $isFinalOperation = $operationClassName::isFinal();

            if (!isset($operations[$shortOperationName])) {
                $operations[$shortOperationName] = [];
            }

            if (isset($operations[$shortOperationName][$operationPriority])) {
                throw new FlowQueryException(sprintf('Operation with name "%s" and priority %s is already defined in class %s, and the class %s has the same priority and name.', $shortOperationName, $operationPriority, $operations[$shortOperationName][$operationPriority], $operationClassName), 1332491678);
            }
            $operations[$shortOperationName][$operationPriority] = $operationClassName;

            if ($isFinalOperation) {
                $finalOperationNames[$shortOperationName] = $shortOperationName;
            }
        }

        foreach ($operations as &$operation) {
            krsort($operation, SORT_NUMERIC);
        }

        return [$operations, $finalOperationNames];
    }

    /**
     * @param string $operationName
     * @return boolean TRUE if $operationName is final
     */
    public function isFinalOperation($operationName)
    {
        return isset($this->finalOperationNames[$operationName]);
    }

    /**
     * Resolve an operation, taking runtime constraints into account.
     *
     * @param string      $operationName
     * @param array|mixed $context
     * @throws FlowQueryException
     * @return OperationInterface the resolved operation
     */
    public function resolveOperation($operationName, $context)
    {
        if (!isset($this->operations[$operationName])) {
            throw new FlowQueryException('Operation "' . $operationName . '" not found.', 1332491837);
        }

        foreach ($this->operations[$operationName] as $operationClassName) {
            $operation = $this->objectManager->get($operationClassName);
            if ($operation->canEvaluate($context)) {
                return $operation;
            }
        }
        throw new FlowQueryException('No operation which satisfies the runtime constraints found for "' . $operationName . '".', 1332491864);
    }

    /**
     * @param string $operationName
     * @return boolean
     */
    public function hasOperation($operationName)
    {
        return isset($this->operations[$operationName]);
    }
}
namespace TYPO3\Eel\FlowQuery;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * FlowQuery Operation Resolver
 * @\TYPO3\Flow\Annotations\Scope("singleton")
 */
class OperationResolver extends OperationResolver_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {
        if (get_class($this) === 'TYPO3\Eel\FlowQuery\OperationResolver') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Eel\FlowQuery\OperationResolver', $this);
        if (get_class($this) === 'TYPO3\Eel\FlowQuery\OperationResolver') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Eel\FlowQuery\OperationResolverInterface', $this);
        if ('TYPO3\Eel\FlowQuery\OperationResolver' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }

        $isSameClass = get_class($this) === 'TYPO3\Eel\FlowQuery\OperationResolver';
        if ($isSameClass) {
            $this->initializeObject(1);
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'objectManager' => 'TYPO3\\Flow\\Object\\ObjectManagerInterface',
  'reflectionService' => 'TYPO3\\Flow\\Reflection\\ReflectionService',
  'operations' => 'array',
  'finalOperationNames' => 'array',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {
        if (get_class($this) === 'TYPO3\Eel\FlowQuery\OperationResolver') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Eel\FlowQuery\OperationResolver', $this);
        if (get_class($this) === 'TYPO3\Eel\FlowQuery\OperationResolver') \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->setInstance('TYPO3\Eel\FlowQuery\OperationResolverInterface', $this);

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
            $result = NULL;

        $isSameClass = get_class($this) === 'TYPO3\Eel\FlowQuery\OperationResolver';
        $classParents = class_parents($this);
        $classImplements = class_implements($this);
        $isClassProxy = array_search('TYPO3\Eel\FlowQuery\OperationResolver', $classParents) !== FALSE && array_search('Doctrine\ORM\Proxy\Proxy', $classImplements) !== FALSE;

        if ($isSameClass || $isClassProxy) {
            $this->initializeObject(2);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Object\ObjectManagerInterface', 'TYPO3\Flow\Object\ObjectManager', 'objectManager', '0c3c44be7be16f2a287f1fb2d068dde4', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Object\ObjectManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Reflection\ReflectionService', 'TYPO3\Flow\Reflection\ReflectionService', 'reflectionService', '921ad637f16d2059757a908fceaf7076', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Reflection\ReflectionService'); });
        $this->Flow_Injected_Properties = array (
  0 => 'objectManager',
  1 => 'reflectionService',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Framework/TYPO3.Eel/Classes/TYPO3/Eel/FlowQuery/OperationResolver.php
#