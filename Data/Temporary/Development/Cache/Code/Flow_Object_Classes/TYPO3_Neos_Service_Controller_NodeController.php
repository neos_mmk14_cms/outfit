<?php 
namespace TYPO3\Neos\Service\Controller;

/*
 * This file is part of the TYPO3.Neos package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Eel\FlowQuery\FlowQuery;
use TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter;
use TYPO3\Neos\Domain\Repository\DomainRepository;
use TYPO3\Neos\Domain\Service\NodeSearchService;
use TYPO3\Neos\Service\NodeOperations;
use TYPO3\Neos\Service\View\NodeView;
use TYPO3\TYPO3CR\Domain\Factory\NodeFactory;
use TYPO3\TYPO3CR\Domain\Model\NodeInterface;
use TYPO3\TYPO3CR\Domain\Model\Node;
use TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository;
use TYPO3\TYPO3CR\Domain\Service\ContextFactoryInterface;
use TYPO3\TYPO3CR\Domain\Service\NodeTypeManager;
use TYPO3\TYPO3CR\Exception\NodeException;
use TYPO3\TYPO3CR\TypeConverter\NodeConverter;

/**
 * Service Controller for managing Nodes
 *
 * Note: This controller should be, step-by-step, transformed into a clean REST controller (see NEOS-190 and NEOS-199).
 *       Since this is a rather big endeavor, we slice the elephant and move methods in a clean way from here to the
 *       new NodesController (\TYPO3\Neos\Controller\Service\NodesController)
 */
class NodeController_Original extends AbstractServiceController
{
    /**
     * @var NodeView
     */
    protected $view;

    /**
     * @var array
     */
    protected $viewFormatToObjectNameMap = array(
        'html' => NodeView::class,
        'json' => NodeView::class
    );

    /**
     * @var array
     */
    protected $supportedMediaTypes = array(
        'text/html',
        'application/json'
    );

    /**
     * @Flow\Inject
     * @var NodeTypeManager
     */
    protected $nodeTypeManager;

    /**
     * @Flow\Inject
     * @var NodeSearchService
     */
    protected $nodeSearchService;

    /**
     * @Flow\Inject
     * @var NodeFactory
     */
    protected $nodeFactory;

    /**
     * @Flow\Inject
     * @var ContextFactoryInterface
     */
    protected $contextFactory;

    /**
     * @Flow\Inject
     * @var NodeDataRepository
     */
    protected $nodeDataRepository;

    /**
     * @Flow\Inject
     * @var NodeOperations
     */
    protected $nodeOperations;

    /**
     * @Flow\Inject
     * @var DomainRepository
     */
    protected $domainRepository;

    /**
     * Select special error action
     *
     * @return void
     */
    protected function initializeAction()
    {
        if ($this->arguments->hasArgument('referenceNode')) {
            $this->arguments->getArgument('referenceNode')->getPropertyMappingConfiguration()->setTypeConverterOption(NodeConverter::class, NodeConverter::REMOVED_CONTENT_SHOWN, true);
        }
        $this->uriBuilder->setRequest($this->request->getMainRequest());
        if (in_array($this->request->getControllerActionName(), array('update', 'updateAndRender'), true)) {
            // Set PropertyMappingConfiguration for updating the node (and attached objects)
            $propertyMappingConfiguration = $this->arguments->getArgument('node')->getPropertyMappingConfiguration();
            $propertyMappingConfiguration->allowOverrideTargetType();
            $propertyMappingConfiguration->allowAllProperties();
            $propertyMappingConfiguration->skipUnknownProperties();
            $propertyMappingConfiguration->setTypeConverterOption(PersistentObjectConverter::class, PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED, true);
            $propertyMappingConfiguration->setTypeConverterOption(PersistentObjectConverter::class, PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, true);
        }
    }

    #
    # Actions which are not yet refactored to REST below (see NEOS-199):
    #

    /**
     * Return child nodes of specified node for usage in a TreeLoader
     *
     * @param Node $node The node to find child nodes for
     * @param string $nodeTypeFilter A node type filter
     * @param integer $depth levels of childNodes (0 = unlimited)
     * @param Node $untilNode expand the child nodes until $untilNode is reached, independent of $depth
     * @return void
     */
    public function getChildNodesForTreeAction(Node $node, $nodeTypeFilter, $depth, Node $untilNode)
    {
        $this->view->assignChildNodes($node, $nodeTypeFilter, NodeView::STYLE_TREE, $depth, $untilNode);
    }

    /**
     * Return child nodes of specified node for usage in a TreeLoader based on filter
     *
     * @param Node $node The node to find child nodes for
     * @param string $term
     * @param string $nodeType
     * @return void
     */
    public function filterChildNodesForTreeAction(Node $node, $term, $nodeType)
    {
        $nodeTypes = strlen($nodeType) > 0 ? array($nodeType) : array_keys($this->nodeTypeManager->getSubNodeTypes('TYPO3.Neos:Document', false));
        $context = $node->getContext();
        if ($term !== '') {
            $nodes = $this->nodeSearchService->findByProperties($term, $nodeTypes, $context, $node);
        } else {
            $nodes = array();
            $nodeDataRecords = $this->nodeDataRepository->findByParentAndNodeTypeRecursively($node->getPath(), implode(',', $nodeTypes), $context->getWorkspace(), $context->getDimensions());
            foreach ($nodeDataRecords as $nodeData) {
                $matchedNode = $this->nodeFactory->createFromNodeData($nodeData, $context);
                if ($matchedNode !== null) {
                    $nodes[$matchedNode->getPath()] = $matchedNode;
                }
            }
        }
        $this->view->assignFilteredChildNodes(
            $node,
            $nodes
        );
    }

    /**
     * Creates a new node
     *
     * We need to call persistAll() in order to return the nextUri. We can't persist only the nodes in NodeDataRepository
     * because they might be connected to images / resources which need to be updated at the same time.
     *
     * @param Node $referenceNode
     * @param array $nodeData
     * @param string $position where the node should be added (allowed: before, into, after)
     * @return void
     */
    public function createAction(Node $referenceNode, array $nodeData, $position)
    {
        $newNode = $this->nodeOperations->create($referenceNode, $nodeData, $position);

        if ($this->request->getHttpRequest()->isMethodSafe() === false) {
            $this->persistenceManager->persistAll();
        }

        $nextUri = $this->uriBuilder->reset()->setFormat('html')->setCreateAbsoluteUri(true)->uriFor('show', array('node' => $newNode), 'Frontend\Node', 'TYPO3.Neos');
        $this->view->assign('value', array('data' => array('nextUri' => $nextUri), 'success' => true));
    }

    /**
     * Creates a new node and renders the node inside the containing content collection.
     *
     * @param Node $referenceNode
     * @param string $typoScriptPath The TypoScript path of the collection
     * @param array $nodeData
     * @param string $position where the node should be added (allowed: before, into, after)
     * @return string
     */
    public function createAndRenderAction(Node $referenceNode, $typoScriptPath, array $nodeData, $position)
    {
        $newNode = $this->nodeOperations->create($referenceNode, $nodeData, $position);
        $this->redirectToRenderNode($newNode, $typoScriptPath);
    }

    /**
     * Creates a new node and returns tree structure
     *
     * @param Node $referenceNode
     * @param array $nodeData
     * @param string $position where the node should be added, -1 is before, 0 is in, 1 is after
     * @param string $nodeTypeFilter
     * @return void
     */
    public function createNodeForTheTreeAction(Node $referenceNode, array $nodeData, $position, $nodeTypeFilter = '')
    {
        $newNode = $this->nodeOperations->create($referenceNode, $nodeData, $position);
        $this->view->assignNodeAndChildNodes($newNode, $nodeTypeFilter);
    }

    /**
     * Move $node before, into or after $targetNode
     *
     * We need to call persistAll() in order to return the nextUri. We can't persist only the nodes in NodeDataRepository
     * because they might be connected to images / resources which need to be updated at the same time.
     *
     * @param Node $node The node to be moved
     * @param Node $targetNode The target node to be moved "to", see $position
     * @param string $position where the node should be added (allowed: before, into, after)
     * @return void
     */
    public function moveAction(Node $node, Node $targetNode, $position)
    {
        $node = $this->nodeOperations->move($node, $targetNode, $position);

        if ($this->request->getHttpRequest()->isMethodSafe() === false) {
            $this->persistenceManager->persistAll();
        }

        $data = array('newNodePath' => $node->getContextPath());
        if ($node->getNodeType()->isOfType('TYPO3.Neos:Document')) {
            $data['nextUri'] = $this->uriBuilder->reset()->setFormat('html')->setCreateAbsoluteUri(true)->uriFor('show', array('node' => $node), 'Frontend\Node', 'TYPO3.Neos');
        }
        $this->view->assign('value', array('data' => $data, 'success' => true));
    }

    /**
     * Move the given node before, into or after the target node depending on the given position and renders it's content collection.
     *
     * @param Node $node The node to be moved
     * @param Node $targetNode The target node to be moved "to", see $position
     * @param string $position Where the node should be added in relation to $targetNode (allowed: before, into, after)
     * @param string $typoScriptPath The TypoScript path of the collection
     * @return void
     */
    public function moveAndRenderAction(Node $node, Node $targetNode, $position, $typoScriptPath)
    {
        $this->nodeOperations->move($node, $targetNode, $position);
        $this->redirectToRenderNode($node, $typoScriptPath);
    }

    /**
     * Copy $node before, into or after $targetNode
     *
     * We need to call persistAll() in order to return the nextUri. We can't persist only the nodes in NodeDataRepository
     * because they might be connected to images / resources which need to be updated at the same time.
     *
     * @param Node $node The node to be copied
     * @param Node $targetNode The target node to be copied "to", see $position
     * @param string $position Where the node should be added in relation to $targetNode (allowed: before, into, after)
     * @param string $nodeName Optional node name (if empty random node name will be generated)
     * @return void
     * @throws NodeException
     */
    public function copyAction(Node $node, Node $targetNode, $position, $nodeName = null)
    {
        $copiedNode = $this->nodeOperations->copy($node, $targetNode, $position, $nodeName);

        if ($this->request->getHttpRequest()->isMethodSafe() === false) {
            $this->persistenceManager->persistAll();
        }

        $q = new FlowQuery(array($copiedNode));
        $closestDocumentNode = $q->closest('[instanceof TYPO3.Neos:Document]')->get(0);

        $requestData = array(
            'nextUri' => $this->uriBuilder->reset()->setFormat('html')->setCreateAbsoluteUri(true)->uriFor('show', array('node' => $closestDocumentNode), 'Frontend\Node', 'TYPO3.Neos'),
            'newNodePath' => $copiedNode->getContextPath()
        );

        if ($node->getNodeType()->isOfType('TYPO3.Neos:Document')) {
            $requestData['nodeUri'] = $this->uriBuilder->reset()->setFormat('html')->setCreateAbsoluteUri(true)->uriFor('show', array('node' => $copiedNode), 'Frontend\Node', 'TYPO3.Neos');
        }

        $this->view->assign('value', array('data' => $requestData, 'success' => true));
    }

    /**
     * Copies the given node before, into or after the target node depending on the given position and renders it's content collection.
     *
     * @param Node $node The node to be copied
     * @param Node $targetNode The target node to be copied "to", see $position
     * @param string $position Where the node should be added in relation to $targetNode (allowed: before, into, after)
     * @param string $nodeName Optional node name (if empty random node name will be generated)
     * @param string $typoScriptPath The TypoScript path of the collection
     * @return void
     */
    public function copyAndRenderAction(Node $node, Node $targetNode, $position, $typoScriptPath, $nodeName = null)
    {
        $copiedNode = $this->nodeOperations->copy($node, $targetNode, $position, $nodeName);
        $this->redirectToRenderNode($copiedNode, $typoScriptPath);
    }

    /**
     * Updates the specified node.
     *
     * Returns the following data:
     *
     * - the (possibly changed) workspace name of the node
     * - the URI of the closest document node. If $node is a document node (f.e. a Page), the own URI is returned.
     *   This is important to handle renames of nodes correctly.
     *
     * Note: We do not call $nodeDataRepository->update() here, as TYPO3CR has a stateful API for now.
     *       We need to call persistAll() in order to return the nextUri. We can't persist only the nodes in NodeDataRepository
     *       because they might be connected to images / resources which need to be updated at the same time.
     *
     * @param Node $node The node to be updated
     * @return void
     */
    public function updateAction(Node $node)
    {
        if ($this->request->getHttpRequest()->isMethodSafe() === false) {
            $this->persistenceManager->persistAll();
        }

        $q = new FlowQuery(array($node));
        $closestDocumentNode = $q->closest('[instanceof TYPO3.Neos:Document]')->get(0);
        $nextUri = $this->uriBuilder->reset()->setFormat('html')->setCreateAbsoluteUri(true)->uriFor('show', array('node' => $closestDocumentNode), 'Frontend\Node', 'TYPO3.Neos');
        $this->view->assign('value', array(
            'data' => array(
                'workspaceNameOfNode' => $node->getWorkspace()->getName(),
                'labelOfNode' => $node->getLabel(),
                'nextUri' => $nextUri
            ),
            'success' => true
        ));
    }

    /**
     * Updates the specified node and renders it's content collection.
     *
     * @param Node $node The node to be updated
     * @param string $typoScriptPath The TypoScript path of the collection
     * @return void
     */
    public function updateAndRenderAction(Node $node, $typoScriptPath)
    {
        $this->redirectToRenderNode($node, $typoScriptPath);
    }

    /**
     * Deletes the specified node and all of its sub nodes
     *
     * We need to call persistAll() in order to return the nextUri. We can't persist only the nodes in NodeDataRepository
     * because they might be connected to images / resources which need to be removed at the same time.
     *
     * @param Node $node
     * @return void
     */
    public function deleteAction(Node $node)
    {
        if ($this->request->getHttpRequest()->isMethodSafe() === false) {
            $this->persistenceManager->persistAll();
        }

        $q = new FlowQuery(array($node));
        $node->remove();
        $closestDocumentNode = $q->closest('[instanceof TYPO3.Neos:Document]')->get(0);
        $nextUri = $this->uriBuilder->reset()->setFormat('html')->setCreateAbsoluteUri(true)->uriFor('show', array('node' => $closestDocumentNode), 'Frontend\Node', 'TYPO3.Neos');

        $this->view->assign('value', array('data' => array('nextUri' => $nextUri), 'success' => true));
    }

    /**
     * Search a page, needed for internal links.
     *
     * @deprecated will be removed with 3.0, use Service/NodesController->indexAction() instead
     * @param string $query
     * @return void
     */
    public function searchPageAction($query)
    {
        $searchResult = array();

        $documentNodeTypes = $this->nodeTypeManager->getSubNodeTypes('TYPO3.Neos:Document');
        /** @var NodeInterface $node */
        foreach ($this->nodeSearchService->findByProperties($query, $documentNodeTypes, $this->createContext('live')) as $node) {
            $searchResult[$node->getPath()] = $this->processNodeForEditorPlugins($node);
        }

        $this->view->assign('value', array('searchResult' => $searchResult, 'success' => true));
    }

    /**
     * Takes care of creating a redirect to properly render the collection the given node is in.
     *
     * @param NodeInterface $node
     * @param string $typoScriptPath
     * @return string
     */
    protected function redirectToRenderNode(NodeInterface $node, $typoScriptPath)
    {
        $q = new FlowQuery(array($node));
        $closestContentCollection = $q->closest('[instanceof TYPO3.Neos:ContentCollection]')->get(0);
        $closestDocumentNode = $q->closest('[instanceof TYPO3.Neos:Document]')->get(0);

        $this->redirect('show', 'Frontend\\Node', 'TYPO3.Neos', [
            'node' => $closestDocumentNode,
            '__nodeContextPath' => $closestContentCollection->getContextPath(),
            '__affectedNodeContextPath' => $node->getContextPath(),
            '__typoScriptPath' => $typoScriptPath
        ], 0, 303, 'html');
    }

    /**
     * Returns an array with the data needed by for example the Hallo and Aloha
     * link plugins to represent the passed Node instance.
     *
     * @param NodeInterface $node
     * @return array
     */
    protected function processNodeForEditorPlugins(NodeInterface $node)
    {
        return array(
            'id' => $node->getPath(),
            'name' => $node->getLabel(),
            'url' => $this->uriBuilder->uriFor('show', array('node' => $node), 'Frontend\Node', 'TYPO3.Neos'),
            'type' => 'neos/internal-link'
        );
    }

    /**
     * Create a Context for a workspace given by name to be used in this controller.
     *
     * @param string $workspaceName Name of the current workspace
     * @return \TYPO3\TYPO3CR\Domain\Service\Context
     */
    protected function createContext($workspaceName)
    {
        $contextProperties = array(
            'workspaceName' => $workspaceName
        );

        $currentDomain = $this->domainRepository->findOneByActiveRequest();
        if ($currentDomain !== null) {
            $contextProperties['currentSite'] = $currentDomain->getSite();
            $contextProperties['currentDomain'] = $currentDomain;
        }

        return $this->contextFactory->create($contextProperties);
    }
}
namespace TYPO3\Neos\Service\Controller;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * Service Controller for managing Nodes
 * 
 * Note: This controller should be, step-by-step, transformed into a clean REST controller (see NEOS-190 and NEOS-199).
 *       Since this is a rather big endeavor, we slice the elephant and move methods in a clean way from here to the
 *       new NodesController (\TYPO3\Neos\Controller\Service\NodesController)
 */
class NodeController extends NodeController_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Aop\AdvicesTrait, \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;

    private $Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array();

    private $Flow_Aop_Proxy_groupedAdviceChains = array();

    private $Flow_Aop_Proxy_methodIsInAdviceMode = array();


    /**
     * Autogenerated Proxy Method
     */
    public function __construct()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
        if ('TYPO3\Neos\Service\Controller\NodeController' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }

        $isSameClass = get_class($this) === 'TYPO3\Neos\Service\Controller\NodeController';
        if ($isSameClass) {
            $this->initializeObject(1);
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    protected function Flow_Aop_Proxy_buildMethodsAndAdvicesArray()
    {
        if (method_exists(get_parent_class(), 'Flow_Aop_Proxy_buildMethodsAndAdvicesArray') && is_callable('parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray')) parent::Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        $objectManager = \TYPO3\Flow\Core\Bootstrap::$staticObjectManager;
        $this->Flow_Aop_Proxy_targetMethodsAndGroupedAdvices = array(
            'getChildNodesForTreeAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'filterChildNodesForTreeAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'createAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'createAndRenderAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'createNodeForTheTreeAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'moveAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'moveAndRenderAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'copyAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'copyAndRenderAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'updateAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'updateAndRenderAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'deleteAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'searchPageAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
            'errorAction' => array(
                'TYPO3\Flow\Aop\Advice\AroundAdvice' => array(
                    new \TYPO3\Flow\Aop\Advice\AroundAdvice('TYPO3\Flow\Security\Aspect\PolicyEnforcementAspect', 'enforcePolicy', $objectManager, NULL),
                ),
            ),
        );
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
            $result = NULL;
        if (method_exists(get_parent_class(), '__wakeup') && is_callable('parent::__wakeup')) parent::__wakeup();

        $isSameClass = get_class($this) === 'TYPO3\Neos\Service\Controller\NodeController';
        $classParents = class_parents($this);
        $classImplements = class_implements($this);
        $isClassProxy = array_search('TYPO3\Neos\Service\Controller\NodeController', $classParents) !== FALSE && array_search('Doctrine\ORM\Proxy\Proxy', $classImplements) !== FALSE;

        if ($isSameClass || $isClassProxy) {
            $this->initializeObject(2);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __clone()
    {

        $this->Flow_Aop_Proxy_buildMethodsAndAdvicesArray();
    }

    /**
     * Autogenerated Proxy Method
     * @param Node $node The node to find child nodes for
     * @param string $nodeTypeFilter A node type filter
     * @param integer $depth levels of childNodes (0 = unlimited)
     * @param Node $untilNode expand the child nodes until $untilNode is reached, independent of $depth
     * @return void
     */
    public function getChildNodesForTreeAction(\TYPO3\TYPO3CR\Domain\Model\Node $node, $nodeTypeFilter, $depth, \TYPO3\TYPO3CR\Domain\Model\Node $untilNode)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['getChildNodesForTreeAction'])) {
            $result = parent::getChildNodesForTreeAction($node, $nodeTypeFilter, $depth, $untilNode);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['getChildNodesForTreeAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['node'] = $node;
                $methodArguments['nodeTypeFilter'] = $nodeTypeFilter;
                $methodArguments['depth'] = $depth;
                $methodArguments['untilNode'] = $untilNode;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('getChildNodesForTreeAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Service\Controller\NodeController', 'getChildNodesForTreeAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['getChildNodesForTreeAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['getChildNodesForTreeAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Node $node The node to find child nodes for
     * @param string $term
     * @param string $nodeType
     * @return void
     */
    public function filterChildNodesForTreeAction(\TYPO3\TYPO3CR\Domain\Model\Node $node, $term, $nodeType)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['filterChildNodesForTreeAction'])) {
            $result = parent::filterChildNodesForTreeAction($node, $term, $nodeType);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['filterChildNodesForTreeAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['node'] = $node;
                $methodArguments['term'] = $term;
                $methodArguments['nodeType'] = $nodeType;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('filterChildNodesForTreeAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Service\Controller\NodeController', 'filterChildNodesForTreeAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['filterChildNodesForTreeAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['filterChildNodesForTreeAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Node $referenceNode
     * @param array $nodeData
     * @param string $position where the node should be added (allowed: before, into, after)
     * @return void
     */
    public function createAction(\TYPO3\TYPO3CR\Domain\Model\Node $referenceNode, array $nodeData, $position)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createAction'])) {
            $result = parent::createAction($referenceNode, $nodeData, $position);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['createAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['referenceNode'] = $referenceNode;
                $methodArguments['nodeData'] = $nodeData;
                $methodArguments['position'] = $position;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('createAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Service\Controller\NodeController', 'createAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Node $referenceNode
     * @param string $typoScriptPath The TypoScript path of the collection
     * @param array $nodeData
     * @param string $position where the node should be added (allowed: before, into, after)
     * @return string
     */
    public function createAndRenderAction(\TYPO3\TYPO3CR\Domain\Model\Node $referenceNode, $typoScriptPath, array $nodeData, $position)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createAndRenderAction'])) {
            $result = parent::createAndRenderAction($referenceNode, $typoScriptPath, $nodeData, $position);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['createAndRenderAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['referenceNode'] = $referenceNode;
                $methodArguments['typoScriptPath'] = $typoScriptPath;
                $methodArguments['nodeData'] = $nodeData;
                $methodArguments['position'] = $position;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('createAndRenderAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Service\Controller\NodeController', 'createAndRenderAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createAndRenderAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createAndRenderAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Node $referenceNode
     * @param array $nodeData
     * @param string $position where the node should be added, -1 is before, 0 is in, 1 is after
     * @param string $nodeTypeFilter
     * @return void
     */
    public function createNodeForTheTreeAction(\TYPO3\TYPO3CR\Domain\Model\Node $referenceNode, array $nodeData, $position, $nodeTypeFilter = '')
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createNodeForTheTreeAction'])) {
            $result = parent::createNodeForTheTreeAction($referenceNode, $nodeData, $position, $nodeTypeFilter);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['createNodeForTheTreeAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['referenceNode'] = $referenceNode;
                $methodArguments['nodeData'] = $nodeData;
                $methodArguments['position'] = $position;
                $methodArguments['nodeTypeFilter'] = $nodeTypeFilter;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('createNodeForTheTreeAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Service\Controller\NodeController', 'createNodeForTheTreeAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createNodeForTheTreeAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['createNodeForTheTreeAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Node $node The node to be moved
     * @param Node $targetNode The target node to be moved "to", see $position
     * @param string $position where the node should be added (allowed: before, into, after)
     * @return void
     */
    public function moveAction(\TYPO3\TYPO3CR\Domain\Model\Node $node, \TYPO3\TYPO3CR\Domain\Model\Node $targetNode, $position)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['moveAction'])) {
            $result = parent::moveAction($node, $targetNode, $position);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['moveAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['node'] = $node;
                $methodArguments['targetNode'] = $targetNode;
                $methodArguments['position'] = $position;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('moveAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Service\Controller\NodeController', 'moveAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['moveAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['moveAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Node $node The node to be moved
     * @param Node $targetNode The target node to be moved "to", see $position
     * @param string $position Where the node should be added in relation to $targetNode (allowed: before, into, after)
     * @param string $typoScriptPath The TypoScript path of the collection
     * @return void
     */
    public function moveAndRenderAction(\TYPO3\TYPO3CR\Domain\Model\Node $node, \TYPO3\TYPO3CR\Domain\Model\Node $targetNode, $position, $typoScriptPath)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['moveAndRenderAction'])) {
            $result = parent::moveAndRenderAction($node, $targetNode, $position, $typoScriptPath);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['moveAndRenderAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['node'] = $node;
                $methodArguments['targetNode'] = $targetNode;
                $methodArguments['position'] = $position;
                $methodArguments['typoScriptPath'] = $typoScriptPath;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('moveAndRenderAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Service\Controller\NodeController', 'moveAndRenderAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['moveAndRenderAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['moveAndRenderAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Node $node The node to be copied
     * @param Node $targetNode The target node to be copied "to", see $position
     * @param string $position Where the node should be added in relation to $targetNode (allowed: before, into, after)
     * @param string $nodeName Optional node name (if empty random node name will be generated)
     * @return void
     * @throws NodeException
     */
    public function copyAction(\TYPO3\TYPO3CR\Domain\Model\Node $node, \TYPO3\TYPO3CR\Domain\Model\Node $targetNode, $position, $nodeName = NULL)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['copyAction'])) {
            $result = parent::copyAction($node, $targetNode, $position, $nodeName);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['copyAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['node'] = $node;
                $methodArguments['targetNode'] = $targetNode;
                $methodArguments['position'] = $position;
                $methodArguments['nodeName'] = $nodeName;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('copyAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Service\Controller\NodeController', 'copyAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['copyAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['copyAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Node $node The node to be copied
     * @param Node $targetNode The target node to be copied "to", see $position
     * @param string $position Where the node should be added in relation to $targetNode (allowed: before, into, after)
     * @param string $nodeName Optional node name (if empty random node name will be generated)
     * @param string $typoScriptPath The TypoScript path of the collection
     * @return void
     */
    public function copyAndRenderAction(\TYPO3\TYPO3CR\Domain\Model\Node $node, \TYPO3\TYPO3CR\Domain\Model\Node $targetNode, $position, $typoScriptPath, $nodeName = NULL)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['copyAndRenderAction'])) {
            $result = parent::copyAndRenderAction($node, $targetNode, $position, $typoScriptPath, $nodeName);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['copyAndRenderAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['node'] = $node;
                $methodArguments['targetNode'] = $targetNode;
                $methodArguments['position'] = $position;
                $methodArguments['typoScriptPath'] = $typoScriptPath;
                $methodArguments['nodeName'] = $nodeName;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('copyAndRenderAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Service\Controller\NodeController', 'copyAndRenderAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['copyAndRenderAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['copyAndRenderAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Node $node The node to be updated
     * @return void
     */
    public function updateAction(\TYPO3\TYPO3CR\Domain\Model\Node $node)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAction'])) {
            $result = parent::updateAction($node);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['node'] = $node;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('updateAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Service\Controller\NodeController', 'updateAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Node $node The node to be updated
     * @param string $typoScriptPath The TypoScript path of the collection
     * @return void
     */
    public function updateAndRenderAction(\TYPO3\TYPO3CR\Domain\Model\Node $node, $typoScriptPath)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAndRenderAction'])) {
            $result = parent::updateAndRenderAction($node, $typoScriptPath);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAndRenderAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['node'] = $node;
                $methodArguments['typoScriptPath'] = $typoScriptPath;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('updateAndRenderAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Service\Controller\NodeController', 'updateAndRenderAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAndRenderAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['updateAndRenderAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param Node $node
     * @return void
     */
    public function deleteAction(\TYPO3\TYPO3CR\Domain\Model\Node $node)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAction'])) {
            $result = parent::deleteAction($node);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['node'] = $node;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('deleteAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Service\Controller\NodeController', 'deleteAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['deleteAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @param string $query
     * @return void
     */
    public function searchPageAction($query)
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['searchPageAction'])) {
            $result = parent::searchPageAction($query);

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['searchPageAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $methodArguments['query'] = $query;
            
                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('searchPageAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Service\Controller\NodeController', 'searchPageAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['searchPageAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['searchPageAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     * @return void
     */
    public function errorAction()
    {

        if (isset($this->Flow_Aop_Proxy_methodIsInAdviceMode['errorAction'])) {
            $result = parent::errorAction();

        } else {
            $this->Flow_Aop_Proxy_methodIsInAdviceMode['errorAction'] = TRUE;
            try {
            
                $methodArguments = [];

                $adviceChains = $this->Flow_Aop_Proxy_getAdviceChains('errorAction');
                $adviceChain = $adviceChains['TYPO3\Flow\Aop\Advice\AroundAdvice'];
                $adviceChain->rewind();
                $joinPoint = new \TYPO3\Flow\Aop\JoinPoint($this, 'TYPO3\Neos\Service\Controller\NodeController', 'errorAction', $methodArguments, $adviceChain);
                $result = $adviceChain->proceed($joinPoint);
                $methodArguments = $joinPoint->getMethodArguments();

            } catch (\Exception $exception) {
                unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['errorAction']);
                throw $exception;
            }
            unset($this->Flow_Aop_Proxy_methodIsInAdviceMode['errorAction']);
        }
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'view' => 'TYPO3\\Neos\\Service\\View\\NodeView',
  'viewFormatToObjectNameMap' => 'array',
  'supportedMediaTypes' => 'array',
  'nodeTypeManager' => 'TYPO3\\TYPO3CR\\Domain\\Service\\NodeTypeManager',
  'nodeSearchService' => 'TYPO3\\Neos\\Domain\\Service\\NodeSearchService',
  'nodeFactory' => 'TYPO3\\TYPO3CR\\Domain\\Factory\\NodeFactory',
  'contextFactory' => 'TYPO3\\TYPO3CR\\Domain\\Service\\ContextFactoryInterface',
  'nodeDataRepository' => 'TYPO3\\TYPO3CR\\Domain\\Repository\\NodeDataRepository',
  'nodeOperations' => 'TYPO3\\Neos\\Service\\NodeOperations',
  'domainRepository' => 'TYPO3\\Neos\\Domain\\Repository\\DomainRepository',
  'objectManager' => 'TYPO3\\Flow\\Object\\ObjectManagerInterface',
  'reflectionService' => 'TYPO3\\Flow\\Reflection\\ReflectionService',
  'mvcPropertyMappingConfigurationService' => 'TYPO3\\Flow\\Mvc\\Controller\\MvcPropertyMappingConfigurationService',
  'viewConfigurationManager' => 'TYPO3\\Flow\\Mvc\\ViewConfigurationManager',
  'viewObjectNamePattern' => 'string',
  'defaultViewObjectName' => 'string',
  'actionMethodName' => 'string',
  'errorMethodName' => 'string',
  'settings' => 'array',
  'systemLogger' => 'TYPO3\\Flow\\Log\\SystemLoggerInterface',
  'uriBuilder' => 'TYPO3\\Flow\\Mvc\\Routing\\UriBuilder',
  'validatorResolver' => 'TYPO3\\Flow\\Validation\\ValidatorResolver',
  'request' => 'TYPO3\\Flow\\Mvc\\ActionRequest',
  'response' => 'TYPO3\\Flow\\Http\\Response',
  'arguments' => 'TYPO3\\Flow\\Mvc\\Controller\\Arguments',
  'controllerContext' => 'TYPO3\\Flow\\Mvc\\Controller\\ControllerContext',
  'flashMessageContainer' => 'TYPO3\\Flow\\Mvc\\FlashMessageContainer',
  'persistenceManager' => 'TYPO3\\Flow\\Persistence\\PersistenceManagerInterface',
  '_localizationService' => '\\TYPO3\\Flow\\I18n\\Service',
  '_userService' => '\\TYPO3\\Neos\\Service\\UserService',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->injectSettings(\TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get(\TYPO3\Flow\Configuration\ConfigurationManager::class)->getConfiguration('Settings', 'TYPO3.Neos'));
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Service\NodeTypeManager', 'TYPO3\TYPO3CR\Domain\Service\NodeTypeManager', 'nodeTypeManager', '478a517efacb3d47415a96d9caded2e9', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Service\NodeTypeManager'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Neos\Domain\Service\NodeSearchService', 'TYPO3\Neos\Domain\Service\NodeSearchService', 'nodeSearchService', '8fbb345eb0af55ff53e13b5dddb8aab5', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Neos\Domain\Service\NodeSearchService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Factory\NodeFactory', 'TYPO3\TYPO3CR\Domain\Factory\NodeFactory', 'nodeFactory', 'bc9bb21d5b30e2ec064f6bb8e860feb4', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Factory\NodeFactory'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Service\ContextFactoryInterface', 'TYPO3\Neos\Domain\Service\ContentContextFactory', 'contextFactory', '6b6e9d36a8365cb0dccb3d849ae9366e', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Service\ContextFactoryInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository', 'TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository', 'nodeDataRepository', '6d8e58e235099c88f352e23317321129', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Repository\NodeDataRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Neos\Service\NodeOperations', 'TYPO3\Neos\Service\NodeOperations', 'nodeOperations', 'b85a227aaba6305a81dffff1fd1c9613', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Neos\Service\NodeOperations'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Neos\Domain\Repository\DomainRepository', 'TYPO3\Neos\Domain\Repository\DomainRepository', 'domainRepository', '6f2987c5f47777b01540a314d984b09c', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Neos\Domain\Repository\DomainRepository'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Object\ObjectManagerInterface', 'TYPO3\Flow\Object\ObjectManager', 'objectManager', '0c3c44be7be16f2a287f1fb2d068dde4', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Object\ObjectManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Reflection\ReflectionService', 'TYPO3\Flow\Reflection\ReflectionService', 'reflectionService', '921ad637f16d2059757a908fceaf7076', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Reflection\ReflectionService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService', 'TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService', 'mvcPropertyMappingConfigurationService', '35acb49fbe78f28099d45aa647797c83', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\Controller\MvcPropertyMappingConfigurationService'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\ViewConfigurationManager', 'TYPO3\Flow\Mvc\ViewConfigurationManager', 'viewConfigurationManager', '5a345bfd515fdb9f0c97080ff13c7079', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\ViewConfigurationManager'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Log\SystemLoggerInterface', '', 'systemLogger', '6d57d95a1c3cd7528e3e6ea15012dac8', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Log\SystemLoggerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Validation\ValidatorResolver', 'TYPO3\Flow\Validation\ValidatorResolver', 'validatorResolver', 'b457db29305ddeae13b61d92da000ca0', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Validation\ValidatorResolver'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Mvc\FlashMessageContainer', 'TYPO3\Flow\Mvc\FlashMessageContainer', 'flashMessageContainer', 'e4fd26f8afd3994317304b563b2a9561', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Mvc\FlashMessageContainer'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Persistence\PersistenceManagerInterface', 'TYPO3\Flow\Persistence\Doctrine\PersistenceManager', 'persistenceManager', 'f1bc82ad47156d95485678e33f27c110', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\I18n\Service', 'TYPO3\Flow\I18n\Service', '_localizationService', 'd147918505b040be63714e111bab34f3', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\I18n\Service'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Neos\Service\UserService', 'TYPO3\Neos\Service\UserService', '_userService', 'bede53034a0bcd605fa08b132fe980ca', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Neos\Service\UserService'); });
        $this->Flow_Injected_Properties = array (
  0 => 'settings',
  1 => 'nodeTypeManager',
  2 => 'nodeSearchService',
  3 => 'nodeFactory',
  4 => 'contextFactory',
  5 => 'nodeDataRepository',
  6 => 'nodeOperations',
  7 => 'domainRepository',
  8 => 'objectManager',
  9 => 'reflectionService',
  10 => 'mvcPropertyMappingConfigurationService',
  11 => 'viewConfigurationManager',
  12 => 'systemLogger',
  13 => 'validatorResolver',
  14 => 'flashMessageContainer',
  15 => 'persistenceManager',
  16 => '_localizationService',
  17 => '_userService',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Neos/Classes/TYPO3/Neos/Service/Controller/NodeController.php
#