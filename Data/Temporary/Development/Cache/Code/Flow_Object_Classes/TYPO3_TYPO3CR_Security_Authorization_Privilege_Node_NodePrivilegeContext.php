<?php 
namespace TYPO3\TYPO3CR\Security\Authorization\Privilege\Node;

/*
 * This file is part of the TYPO3.TYPO3CR package.
 *
 * (c) Contributors of the Neos Project - www.neos.io
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Security\Context as SecurityContext;
use TYPO3\Flow\Validation\Validator\UuidValidator;
use TYPO3\TYPO3CR\Domain\Model\NodeInterface;
use TYPO3\TYPO3CR\Domain\Service\ContentDimensionPresetSourceInterface;
use TYPO3\TYPO3CR\Domain\Service\ContextFactory;

/**
 * An Eel context matching expression for the node privileges
 */
class NodePrivilegeContext_Original
{
    /**
     * @Flow\Inject
     * @var ContextFactory
     */
    protected $contextFactory;

    /**
     * @Flow\Inject
     * @var SecurityContext
     */
    protected $securityContext;

    /**
     * @Flow\Inject
     * @var ContentDimensionPresetSourceInterface
     */
    protected $contentDimensionPresetSource;

    /**
     * @var NodeInterface
     */
    protected $node;

    /**
     * @param NodeInterface $node
     */
    public function __construct(NodeInterface $node = null)
    {
        $this->node = $node;
    }

    /**
     * @param NodeInterface $node
     * @return void
     */
    public function setNode(NodeInterface $node)
    {
        $this->node = $node;
    }

    /**
     * @param string $nodePathOrIdentifier
     * @return boolean
     */
    public function isDescendantNodeOf($nodePathOrIdentifier)
    {
        if ($this->node === null) {
            return true;
        }
        if (preg_match(UuidValidator::PATTERN_MATCH_UUID, $nodePathOrIdentifier) === 1) {
            if ($this->node->getIdentifier() === $nodePathOrIdentifier) {
                return true;
            }
            $node = $this->getNodeByIdentifier($nodePathOrIdentifier);
            if ($node === null) {
                return false;
            }
            $nodePath = $node->getPath() . '/';
        } else {
            $nodePath = rtrim($nodePathOrIdentifier, '/') . '/';
        }
        return substr($this->node->getPath() . '/', 0, strlen($nodePath)) === $nodePath;
    }

    /**
     * @param string|array $nodeTypes
     * @return boolean
     */
    public function nodeIsOfType($nodeTypes)
    {
        if ($this->node === null) {
            return true;
        }
        if (!is_array($nodeTypes)) {
            $nodeTypes = array($nodeTypes);
        }

        foreach ($nodeTypes as $nodeType) {
            if ($this->node->getNodeType()->isOfType($nodeType)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param string|array $workspaceNames
     * @return boolean
     */
    public function isInWorkspace($workspaceNames)
    {
        if ($this->node === null) {
            return true;
        }

        return in_array($this->node->getWorkspace()->getName(), $workspaceNames);
    }

    /**
     * Matches if the currently-selected preset in the passed $dimensionName is one of $presets.
     *
     * Example: isInDimensionPreset('language', 'de') checks whether the currently-selected language
     * preset (in the Neos backend) is "de".
     *
     * Implementation Note: We deliberately work on the Dimension Preset Name, and not on the
     * dimension values itself; as the preset is user-visible and the actual dimension-values
     * for a preset are just implementation details.
     *
     * @param string $dimensionName
     * @param string|array $presets
     * @return boolean
     */
    public function isInDimensionPreset($dimensionName, $presets)
    {
        if ($this->node === null) {
            return true;
        }

        $dimensionValues = $this->node->getContext()->getDimensions();
        if (!isset($dimensionValues[$dimensionName])) {
            return false;
        }

        $preset = $this->contentDimensionPresetSource->findPresetByDimensionValues($dimensionName, $dimensionValues[$dimensionName]);

        if ($preset === null) {
            return false;
        }
        $presetIdentifier = $preset['identifier'];

        if (!is_array($presets)) {
            $presets = array($presets);
        }

        return in_array($presetIdentifier, $presets);
    }

    /**
     * @param string $nodeIdentifier
     * @return NodeInterface
     */
    protected function getNodeByIdentifier($nodeIdentifier)
    {
        $context = $this->contextFactory->create();
        $node = null;
        $this->securityContext->withoutAuthorizationChecks(function () use ($nodeIdentifier, $context, &$node) {
            $node = $context->getNodeByIdentifier($nodeIdentifier);
        });
        $context->getFirstLevelNodeCache()->setByIdentifier($nodeIdentifier, null);
        return $node;
    }
}
namespace TYPO3\TYPO3CR\Security\Authorization\Privilege\Node;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * An Eel context matching expression for the node privileges
 */
class NodePrivilegeContext extends NodePrivilegeContext_Original implements \TYPO3\Flow\Object\Proxy\ProxyInterface {

    use \TYPO3\Flow\Object\Proxy\ObjectSerializationTrait, \TYPO3\Flow\Object\DependencyInjection\PropertyInjectionTrait;


    /**
     * Autogenerated Proxy Method
     * @param NodeInterface $node
     */
    public function __construct()
    {
        $arguments = func_get_args();
        call_user_func_array('parent::__construct', $arguments);
        if ('TYPO3\TYPO3CR\Security\Authorization\Privilege\Node\NodePrivilegeContext' === get_class($this)) {
            $this->Flow_Proxy_injectProperties();
        }
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __sleep()
    {
            $result = NULL;
        $this->Flow_Object_PropertiesToSerialize = array();

        $transientProperties = array (
);
        $propertyVarTags = array (
  'contextFactory' => 'TYPO3\\TYPO3CR\\Domain\\Service\\ContextFactory',
  'securityContext' => 'TYPO3\\Flow\\Security\\Context',
  'contentDimensionPresetSource' => 'TYPO3\\TYPO3CR\\Domain\\Service\\ContentDimensionPresetSourceInterface',
  'node' => 'TYPO3\\TYPO3CR\\Domain\\Model\\NodeInterface',
);
        $result = $this->Flow_serializeRelatedEntities($transientProperties, $propertyVarTags);
        return $result;
    }

    /**
     * Autogenerated Proxy Method
     */
    public function __wakeup()
    {

        $this->Flow_setRelatedEntities();
        $this->Flow_Proxy_injectProperties();
    }

    /**
     * Autogenerated Proxy Method
     */
    private function Flow_Proxy_injectProperties()
    {
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Service\ContextFactory', 'TYPO3\TYPO3CR\Domain\Service\ContextFactory', 'contextFactory', '93663bf362ae5b5202fec420ab22f12f', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Service\ContextFactory'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\Flow\Security\Context', 'TYPO3\Flow\Security\Context', 'securityContext', '48836470c14129ade5f39e28c4816673', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\Flow\Security\Context'); });
        $this->Flow_Proxy_LazyPropertyInjection('TYPO3\TYPO3CR\Domain\Service\ContentDimensionPresetSourceInterface', 'TYPO3\Neos\Domain\Service\ConfigurationContentDimensionPresetSource', 'contentDimensionPresetSource', '4ff8ff1d5fc420f4cfc913278389cab4', function() { return \TYPO3\Flow\Core\Bootstrap::$staticObjectManager->get('TYPO3\TYPO3CR\Domain\Service\ContentDimensionPresetSourceInterface'); });
        $this->Flow_Injected_Properties = array (
  0 => 'contextFactory',
  1 => 'securityContext',
  2 => 'contentDimensionPresetSource',
);
    }
}
# PathAndFilename: /Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.TYPO3CR/Classes/TYPO3/TYPO3CR/Security/Authorization/Privilege/Node/NodePrivilegeContext.php
#