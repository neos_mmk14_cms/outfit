<?php return array (
'flow_aop_expression_53741467a7649d0891002bc32d7a040a' => 
						function(\TYPO3\Flow\Aop\JoinPointInterface $joinPoint, $objectManager) {
							$currentObject = $joinPoint->getProxy();
							$globalObjectNames = $objectManager->getSettingsByPath(array('TYPO3', 'Flow', 'aop', 'globalObjects'));
							$globalObjects = array_map(function($objectName) use ($objectManager) { return $objectManager->get($objectName); }, $globalObjectNames);
							return (\TYPO3\Flow\Reflection\ObjectAccess::getPropertyPath($joinPoint->getMethodArgument('workspace'), 'owner') === \TYPO3\Flow\Reflection\ObjectAccess::getPropertyPath($globalObjects['userInformation'], 'backendUser'));
						},
'flow_aop_expression_0ff9d6dbe6ebb5fd27262cebefdabe50' => 
						function(\TYPO3\Flow\Aop\JoinPointInterface $joinPoint, $objectManager) {
							$currentObject = $joinPoint->getProxy();
							return (\TYPO3\Flow\Reflection\ObjectAccess::getPropertyPath($joinPoint->getMethodArgument('workspace'), 'owner') === null);
						},
'flow_aop_expression_c45902e1839c27a972d774bfe84f14b0' => 
						function(\TYPO3\Flow\Aop\JoinPointInterface $joinPoint, $objectManager) {
							$currentObject = $joinPoint->getProxy();
							$globalObjectNames = $objectManager->getSettingsByPath(array('TYPO3', 'Flow', 'aop', 'globalObjects'));
							$globalObjects = array_map(function($objectName) use ($objectManager) { return $objectManager->get($objectName); }, $globalObjectNames);
							return (\TYPO3\Flow\Reflection\ObjectAccess::getPropertyPath($currentObject, 'workspace.owner') !== \TYPO3\Flow\Reflection\ObjectAccess::getPropertyPath($globalObjects['userInformation'], 'backendUser') && \TYPO3\Flow\Reflection\ObjectAccess::getPropertyPath($currentObject, 'workspace.personalWorkspace') === false);
						},
'flow_aop_expression_904ba832ea57dc56fe1c0db766ca6326' => 
						function(\TYPO3\Flow\Aop\JoinPointInterface $joinPoint, $objectManager) {
							$currentObject = $joinPoint->getProxy();
							return (\TYPO3\Flow\Reflection\ObjectAccess::getPropertyPath($joinPoint->getMethodArgument('workspace'), 'baseWorkspace.name') === "live");
						},
'flow_aop_expression_ffacaf524ea6aee19b00fdf177cea681' => 
						function(\TYPO3\Flow\Aop\JoinPointInterface $joinPoint, $objectManager) {
							$currentObject = $joinPoint->getProxy();
							$globalObjectNames = $objectManager->getSettingsByPath(array('TYPO3', 'Flow', 'aop', 'globalObjects'));
							$globalObjects = array_map(function($objectName) use ($objectManager) { return $objectManager->get($objectName); }, $globalObjectNames);
							return ($joinPoint->getMethodArgument('user') === \TYPO3\Flow\Reflection\ObjectAccess::getPropertyPath($globalObjects['securityContext'], 'party'));
						},
'flow_aop_expression_0ed81da153855e407f375c0286c7854c' => 
						function(\TYPO3\Flow\Aop\JoinPointInterface $joinPoint, $objectManager) {
							$currentObject = $joinPoint->getProxy();
							$globalObjectNames = $objectManager->getSettingsByPath(array('TYPO3', 'Flow', 'aop', 'globalObjects'));
							$globalObjects = array_map(function($objectName) use ($objectManager) { return $objectManager->get($objectName); }, $globalObjectNames);
							return (\TYPO3\Flow\Reflection\ObjectAccess::getPropertyPath($joinPoint->getMethodArgument('workspace'), 'owner') === \TYPO3\Flow\Reflection\ObjectAccess::getPropertyPath($globalObjects['userInformation'], 'backendUser'));
						},
'flow_aop_expression_df1b68f40bebb5761549b93073c93c3f' => 
						function(\TYPO3\Flow\Aop\JoinPointInterface $joinPoint, $objectManager) {
							$currentObject = $joinPoint->getProxy();
							$globalObjectNames = $objectManager->getSettingsByPath(array('TYPO3', 'Flow', 'aop', 'globalObjects'));
							$globalObjects = array_map(function($objectName) use ($objectManager) { return $objectManager->get($objectName); }, $globalObjectNames);
							return (\TYPO3\Flow\Reflection\ObjectAccess::getPropertyPath($joinPoint->getMethodArgument('workspace'), 'name') === \TYPO3\Flow\Reflection\ObjectAccess::getPropertyPath($globalObjects['userInformation'], 'userWorkspaceName'));
						},
'flow_aop_expression_c955b124f1d105221cc0162249dbaa5d' => 
						function(\TYPO3\Flow\Aop\JoinPointInterface $joinPoint, $objectManager) {
							$currentObject = $joinPoint->getProxy();
							return ($joinPoint->getMethodArgument('workspaceName') === "live");
						},
'flow_aop_expression_ffaa6145f54102d4a542b5407e6b4001' => 
						function(\TYPO3\Flow\Aop\JoinPointInterface $joinPoint, $objectManager) {
							$currentObject = $joinPoint->getProxy();
							$globalObjectNames = $objectManager->getSettingsByPath(array('TYPO3', 'Flow', 'aop', 'globalObjects'));
							$globalObjects = array_map(function($objectName) use ($objectManager) { return $objectManager->get($objectName); }, $globalObjectNames);
							return ($joinPoint->getMethodArgument('workspaceName') === \TYPO3\Flow\Reflection\ObjectAccess::getPropertyPath($globalObjects['userInformation'], 'userWorkspaceName'));
						},
'flow_aop_expression_8b597241b571198343be48bd7c5e287b' => 
						function(\TYPO3\Flow\Aop\JoinPointInterface $joinPoint, $objectManager) {
							$currentObject = $joinPoint->getProxy();
							return (\TYPO3\Flow\Reflection\ObjectAccess::getPropertyPath($joinPoint->getMethodArgument('targetWorkspace'), 'name') === "live");
						},
'flow_aop_expression_46ecb13d0e453e79814866d922c3bda1' => 
						function(\TYPO3\Flow\Aop\JoinPointInterface $joinPoint, $objectManager) {
							$currentObject = $joinPoint->getProxy();
							$globalObjectNames = $objectManager->getSettingsByPath(array('TYPO3', 'Flow', 'aop', 'globalObjects'));
							$globalObjects = array_map(function($objectName) use ($objectManager) { return $objectManager->get($objectName); }, $globalObjectNames);
							return (\TYPO3\Flow\Reflection\ObjectAccess::getPropertyPath($currentObject, 'workspace.owner') !== \TYPO3\Flow\Reflection\ObjectAccess::getPropertyPath($globalObjects['userInformation'], 'backendUser') && \TYPO3\Flow\Reflection\ObjectAccess::getPropertyPath($currentObject, 'workspace.personalWorkspace') === true);
						},
);
#