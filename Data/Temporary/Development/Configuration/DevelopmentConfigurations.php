<?php return array (
  'Settings' => 
  array (
    'TYPO3' => 
    array (
      'Flow' => 
      array (
        'aop' => 
        array (
          'globalObjects' => 
          array (
            'securityContext' => 'TYPO3\\Flow\\Security\\Context',
            'userInformation' => 'TYPO3\\Neos\\Service\\UserService',
          ),
        ),
        'compatibility' => 
        array (
          'uriBuilder' => 
          array (
            'createRelativePaths' => false,
          ),
        ),
        'core' => 
        array (
          'context' => 'Development',
          'applicationPackageKey' => 'TYPO3.Neos',
          'applicationName' => 'Neos',
          'phpBinaryPathAndFilename' => '/usr/bin/php',
          'subRequestEnvironmentVariables' => 
          array (
            'XDEBUG_CONFIG' => 'idekey=FLOW_SUBREQUEST remote_port=9001',
          ),
          'subRequestPhpIniPathAndFilename' => NULL,
          'subRequestIniEntries' => 
          array (
          ),
        ),
        'error' => 
        array (
          'exceptionHandler' => 
          array (
            'className' => 'TYPO3\\Flow\\Error\\DebugExceptionHandler',
            'defaultRenderingOptions' => 
            array (
              'renderTechnicalDetails' => true,
              'logException' => true,
            ),
            'renderingGroups' => 
            array (
              'notFoundExceptions' => 
              array (
                'matchingStatusCodes' => 
                array (
                  0 => 404,
                ),
                'options' => 
                array (
                  'logException' => false,
                  'templatePathAndFilename' => 'resource://TYPO3.Neos/Private/Templates/Error/Index.html',
                  'variables' => 
                  array (
                    'errorDescription' => 'Sorry, the page you requested was not found.',
                  ),
                  'layoutRootPath' => 'resource://TYPO3.Neos/Private/Layouts/',
                  'format' => 'html',
                ),
              ),
              'databaseConnectionExceptions' => 
              array (
                'matchingExceptionClassNames' => 
                array (
                  0 => 'TYPO3\\Flow\\Persistence\\Doctrine\\Exception\\DatabaseException',
                  1 => 'TYPO3\\Flow\\Persistence\\Doctrine\\Exception\\DatabaseConnectionException',
                  2 => 'TYPO3\\Flow\\Persistence\\Doctrine\\Exception\\DatabaseStructureException',
                ),
                'options' => 
                array (
                  'templatePathAndFilename' => 'resource://TYPO3.Neos/Private/Templates/Error/Index.html',
                  'variables' => 
                  array (
                    'errorDescription' => 'Sorry, the database connection couldn\'t be established.',
                    'showSetupLink' => true,
                  ),
                  'layoutRootPath' => 'resource://TYPO3.Neos/Private/Layouts/',
                  'format' => 'html',
                ),
              ),
              'noHomepageException' => 
              array (
                'matchingExceptionClassNames' => 
                array (
                  0 => 'TYPO3\\Neos\\Routing\\Exception\\NoHomepageException',
                ),
                'options' => 
                array (
                  'templatePathAndFilename' => 'resource://TYPO3.Neos/Private/Templates/Error/Index.html',
                  'layoutRootPath' => 'resource://TYPO3.Neos/Private/Layouts/',
                  'format' => 'html',
                  'variables' => 
                  array (
                    'showSetupLink' => true,
                  ),
                ),
              ),
            ),
          ),
          'errorHandler' => 
          array (
            'exceptionalErrors' => 
            array (
              0 => 256,
              1 => 4096,
              2 => 2,
              3 => 8,
              4 => 512,
              5 => 1024,
              6 => 2048,
            ),
          ),
          'debugger' => 
          array (
            'ignoredClasses' => 
            array (
              'TYPO3\\\\Flow\\\\Aop.*' => true,
              'TYPO3\\\\Flow\\\\Cac.*' => true,
              'TYPO3\\\\Flow\\\\Core\\\\.*' => true,
              'TYPO3\\\\Flow\\\\Con.*' => true,
              'TYPO3\\\\Flow\\\\Http\\\\RequestHandler' => true,
              'TYPO3\\\\Flow\\\\Uti.*' => true,
              'TYPO3\\\\Flow\\\\Mvc\\\\Routing.*' => true,
              'TYPO3\\\\Flow\\\\Log.*' => true,
              'TYPO3\\\\Flow\\\\Obj.*' => true,
              'TYPO3\\\\Flow\\\\Pac.*' => true,
              'TYPO3\\\\Flow\\\\Persistence\\\\(?!Doctrine\\\\Mapping).*' => true,
              'TYPO3\\\\Flow\\\\Pro.*' => true,
              'TYPO3\\\\Flow\\\\Ref.*' => true,
              'TYPO3\\\\Flow\\\\Sec.*' => true,
              'TYPO3\\\\Flow\\\\Sig.*' => true,
              'TYPO3\\\\Flow\\\\.*ResourceManager' => true,
              'TYPO3\\\\Fluid\\\\.*' => true,
              '.+Service$' => true,
              '.+Repository$' => true,
              'PHPUnit_Framework_MockObject_InvocationMocker' => true,
              'TYPO3\\\\TYPO3CR\\\\Domain\\\\Service\\\\NodeTypeManager' => true,
              'TYPO3\\\\TYPO3CR\\\\Domain\\\\Factory\\\\NodeFactory' => true,
              'TYPO3\\\\TYPO3CR\\\\Domain\\\\Service\\\\Cache\\\\FirstLevelNodeCache' => true,
              'TYPO3\\\\Neos\\\\Domain\\\\Service\\\\ContentContextFactory' => true,
            ),
          ),
        ),
        'mvc' => 
        array (
          'routes' => 
          array (
            'TYPO3.Media' => 
            array (
              'position' => 'before TYPO3.Neos',
            ),
          ),
        ),
        'http' => 
        array (
          'applicationToken' => 'MinorVersion',
          'baseUri' => NULL,
          'chain' => 
          array (
            'preprocess' => 
            array (
              'position' => 'before process',
              'chain' => 
              array (
                'trustedProxies' => 
                array (
                  'position' => 'start',
                  'component' => 'TYPO3\\Flow\\Http\\Component\\TrustedProxiesComponent',
                ),
              ),
            ),
            'process' => 
            array (
              'chain' => 
              array (
                'routing' => 
                array (
                  'position' => 'start',
                  'component' => 'TYPO3\\Flow\\Mvc\\Routing\\RoutingComponent',
                ),
                'dispatching' => 
                array (
                  'component' => 'TYPO3\\Flow\\Mvc\\DispatchComponent',
                ),
                'ajaxWidget' => 
                array (
                  'position' => 'before routing',
                  'component' => 'TYPO3\\Fluid\\Core\\Widget\\AjaxWidgetComponent',
                ),
                'redirect' => 
                array (
                  'position' => 'after routing',
                  'component' => 'Neos\\RedirectHandler\\RedirectComponent',
                ),
              ),
            ),
            'postprocess' => 
            array (
              'chain' => 
              array (
                'standardsCompliance' => 
                array (
                  'position' => 'end',
                  'component' => 'TYPO3\\Flow\\Http\\Component\\StandardsComplianceComponent',
                ),
              ),
            ),
          ),
          'trustedProxies' => 
          array (
            'proxies' => '*',
            'headers' => 
            array (
              'clientIp' => 'Client-Ip,X-Forwarded-For,X-Forwarded,X-Cluster-Client-Ip,Forwarded-For,Forwarded',
              'host' => 'X-Forwarded-Host',
              'port' => 'X-Forwarded-Port',
              'proto' => 'X-Forwarded-Proto',
            ),
          ),
        ),
        'log' => 
        array (
          'systemLogger' => 
          array (
            'logger' => 'TYPO3\\Flow\\Log\\Logger',
            'backend' => 'TYPO3\\Flow\\Log\\Backend\\FileBackend',
            'backendOptions' => 
            array (
              'logFileURL' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Data/Logs/System_Development.log',
              'createParentDirectories' => true,
              'severityThreshold' => 7,
              'maximumLogFileSize' => 10485760,
              'logFilesToKeep' => 1,
              'logMessageOrigin' => false,
            ),
          ),
          'securityLogger' => 
          array (
            'backend' => 'TYPO3\\Flow\\Log\\Backend\\FileBackend',
            'backendOptions' => 
            array (
              'logFileURL' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Data/Logs/Security_Development.log',
              'createParentDirectories' => true,
              'severityThreshold' => 7,
              'maximumLogFileSize' => 10485760,
              'logFilesToKeep' => 1,
              'logIpAddress' => true,
            ),
          ),
          'sqlLogger' => 
          array (
            'backend' => 'TYPO3\\Flow\\Log\\Backend\\FileBackend',
            'backendOptions' => 
            array (
              'logFileURL' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Data/Logs/Query_Development.log',
              'createParentDirectories' => true,
              'severityThreshold' => 7,
              'maximumLogFileSize' => 10485760,
              'logFilesToKeep' => 1,
            ),
          ),
          'i18nLogger' => 
          array (
            'backend' => 'TYPO3\\Flow\\Log\\Backend\\FileBackend',
            'backendOptions' => 
            array (
              'logFileURL' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Data/Logs/I18n_Development.log',
              'createParentDirectories' => true,
              'severityThreshold' => 7,
              'maximumLogFileSize' => 1048576,
              'logFilesToKeep' => 1,
            ),
          ),
        ),
        'i18n' => 
        array (
          'defaultLocale' => 'en',
          'fallbackRule' => 
          array (
            'strict' => false,
            'order' => 
            array (
            ),
          ),
          'scan' => 
          array (
            'includePaths' => 
            array (
              '/Public/' => true,
              '/Private/' => true,
            ),
            'excludePatterns' => 
            array (
              '/node_modules/' => true,
              '/bower_components/' => true,
              '/\\..*/' => true,
            ),
          ),
        ),
        'object' => 
        array (
          'registerFunctionalTestClasses' => false,
          'includeClasses' => 
          array (
          ),
        ),
        'package' => 
        array (
          'inactiveByDefault' => 
          array (
            0 => 'neos.composerplugin',
            1 => 'Composer.Installers',
          ),
          'packagesPathByType' => 
          array (
            'typo3-flow-package' => 'Application',
            'neos-package' => 'Application',
            'typo3-flow-framework' => 'Framework',
            'neos-framework' => 'Framework',
            'typo3-flow-site' => 'Sites',
            'typo3-flow-plugin' => 'Plugins',
          ),
        ),
        'persistence' => 
        array (
          'backendOptions' => 
          array (
            'driver' => 'pdo_mysql',
            'host' => '127.0.0.1',
            'dbname' => 'Neos',
            'user' => 'Neos',
            'password' => 'Contrus',
            'charset' => 'utf8',
          ),
          'cacheAllQueryResults' => false,
          'doctrine' => 
          array (
            'enable' => true,
            'sqlLogger' => NULL,
            'filters' => 
            array (
              'Flow_Security_Entity_Filter' => 'TYPO3\\Flow\\Security\\Authorization\\Privilege\\Entity\\Doctrine\\SqlFilter',
            ),
            'dbal' => 
            array (
              'mappingTypes' => 
              array (
                'flow_json_array' => 
                array (
                  'dbType' => 'json_array',
                  'className' => 'TYPO3\\Flow\\Persistence\\Doctrine\\DataTypes\\JsonArrayType',
                ),
                'objectarray' => 
                array (
                  'dbType' => 'array',
                  'className' => 'TYPO3\\Flow\\Persistence\\Doctrine\\DataTypes\\ObjectArray',
                ),
              ),
            ),
            'eventSubscribers' => 
            array (
            ),
            'eventListeners' => 
            array (
              'TYPO3\\Media\\Domain\\EventListener\\ImageEventListener' => 
              array (
                'events' => 
                array (
                  0 => 'postRemove',
                ),
                'listener' => 'TYPO3\\Media\\Domain\\EventListener\\ImageEventListener',
              ),
              'Gedmo\\Timestampable\\TimestampableListener' => 
              array (
                'events' => 
                array (
                  0 => 'prePersist',
                  1 => 'onFlush',
                  2 => 'loadClassMetadata',
                ),
                'listener' => 'Gedmo\\Timestampable\\TimestampableListener',
              ),
              'TYPO3\\Neos\\Domain\\EventListener\\AccountPostEventListener' => 
              array (
                'events' => 
                array (
                  0 => 'postPersist',
                  1 => 'postUpdate',
                  2 => 'postRemove',
                ),
                'listener' => 'TYPO3\\Neos\\Domain\\EventListener\\AccountPostEventListener',
              ),
              'TYPO3\\Neos\\EventLog\\Integrations\\EntityIntegrationService' => 
              array (
                'events' => 
                array (
                  0 => 'onFlush',
                ),
                'listener' => 'TYPO3\\Neos\\EventLog\\Integrations\\EntityIntegrationService',
              ),
              'TYPO3\\Neos\\EventLog\\Integrations\\TYPO3CRIntegrationService' => 
              array (
                'events' => 
                array (
                  0 => 'preFlush',
                ),
                'listener' => 'TYPO3\\Neos\\EventLog\\Integrations\\TYPO3CRIntegrationService',
              ),
            ),
            'secondLevelCache' => 
            array (
            ),
            'migrations' => 
            array (
              'ignoredTables' => 
              array (
              ),
            ),
            'dql' => 
            array (
              'customStringFunctions' => 
              array (
                'NEOSCR_TOSTRING' => 'TYPO3\\TYPO3CR\\Persistence\\Ast\\ToStringFunction',
              ),
            ),
          ),
        ),
        'reflection' => 
        array (
          'ignoredTags' => 
          array (
            'api' => true,
            'package' => true,
            'subpackage' => true,
            'license' => true,
            'copyright' => true,
            'author' => true,
            'const' => true,
            'see' => true,
            'todo' => true,
            'scope' => true,
            'fixme' => true,
            'test' => true,
            'expectedException' => true,
            'expectedExceptionMessage' => true,
            'expectedExceptionCode' => true,
            'depends' => true,
            'dataProvider' => true,
            'group' => true,
            'codeCoverageIgnore' => true,
            'requires' => true,
            'Given' => true,
            'When' => true,
            'Then' => true,
            'BeforeScenario' => true,
            'AfterScenario' => true,
            'fixtures' => true,
            'Isolated' => true,
            'AfterFeature' => true,
            'BeforeFeature' => true,
            'BeforeStep' => true,
            'AfterStep' => true,
            'WithoutSecurityChecks' => true,
            'covers' => true,
          ),
          'logIncorrectDocCommentHints' => false,
        ),
        'resource' => 
        array (
          'uploadExtensionBlacklist' => 
          array (
            'aspx' => true,
            'cgi' => true,
            'php3' => true,
            'php4' => true,
            'php5' => true,
            'phtml' => true,
            'php' => true,
            'pl' => true,
            'py' => true,
            'pyc' => true,
            'pyo' => true,
            'rb' => true,
          ),
          'storages' => 
          array (
            'defaultPersistentResourcesStorage' => 
            array (
              'storage' => 'TYPO3\\Flow\\Resource\\Storage\\WritableFileSystemStorage',
              'storageOptions' => 
              array (
                'path' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Data/Persistent/Resources/',
              ),
            ),
            'defaultStaticResourcesStorage' => 
            array (
              'storage' => 'TYPO3\\Flow\\Resource\\Storage\\PackageStorage',
            ),
          ),
          'collections' => 
          array (
            'static' => 
            array (
              'storage' => 'defaultStaticResourcesStorage',
              'target' => 'localWebDirectoryStaticResourcesTarget',
              'pathPatterns' => 
              array (
                0 => '*/Resources/Public/*',
              ),
            ),
            'persistent' => 
            array (
              'storage' => 'defaultPersistentResourcesStorage',
              'target' => 'localWebDirectoryPersistentResourcesTarget',
            ),
          ),
          'targets' => 
          array (
            'localWebDirectoryStaticResourcesTarget' => 
            array (
              'target' => 'TYPO3\\Flow\\Resource\\Target\\FileSystemSymlinkTarget',
              'targetOptions' => 
              array (
                'path' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Web/_Resources/Static/Packages/',
                'baseUri' => '_Resources/Static/Packages/',
                'extensionBlacklist' => 
                array (
                  'aspx' => true,
                  'cgi' => true,
                  'php3' => true,
                  'php4' => true,
                  'php5' => true,
                  'phtml' => true,
                  'php' => true,
                  'pl' => true,
                  'py' => true,
                  'pyc' => true,
                  'pyo' => true,
                  'rb' => true,
                ),
              ),
            ),
            'localWebDirectoryPersistentResourcesTarget' => 
            array (
              'target' => 'TYPO3\\Flow\\Resource\\Target\\FileSystemSymlinkTarget',
              'targetOptions' => 
              array (
                'path' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Web/_Resources/Persistent/',
                'baseUri' => '_Resources/Persistent/',
                'extensionBlacklist' => 
                array (
                  'aspx' => true,
                  'cgi' => true,
                  'php3' => true,
                  'php4' => true,
                  'php5' => true,
                  'phtml' => true,
                  'php' => true,
                  'pl' => true,
                  'py' => true,
                  'pyc' => true,
                  'pyo' => true,
                  'rb' => true,
                ),
                'subdivideHashPathSegment' => false,
              ),
            ),
          ),
        ),
        'security' => 
        array (
          'firewall' => 
          array (
            'rejectAll' => false,
            'filters' => 
            array (
              'TYPO3.Flow:CsrfProtection' => 
              array (
                'pattern' => 'CsrfProtection',
                'interceptor' => 'AccessDeny',
              ),
            ),
          ),
          'authentication' => 
          array (
            'providers' => 
            array (
              'Typo3SetupProvider' => 
              array (
                'provider' => 'FileBasedSimpleKeyProvider',
                'providerOptions' => 
                array (
                  'keyName' => 'SetupKey',
                  'authenticateRoles' => 
                  array (
                    0 => 'TYPO3.Setup:SetupUser',
                  ),
                ),
                'requestPatterns' => 
                array (
                  'controllerObjectName' => 'TYPO3\\Setup\\Controller\\.*|TYPO3\\Setup\\ViewHelpers\\Widget\\Controller\\.*',
                ),
                'entryPoint' => 'WebRedirect',
                'entryPointOptions' => 
                array (
                  'uri' => 'setup/login',
                ),
              ),
              'Typo3BackendProvider' => 
              array (
                'label' => 'Neos Backend',
                'provider' => 'PersistedUsernamePasswordProvider',
                'requestPatterns' => 
                array (
                  'TYPO3.Neos:backendControllers' => 
                  array (
                    'pattern' => 'ControllerObjectName',
                    'patternOptions' => 
                    array (
                      'controllerObjectNamePattern' => 'TYPO3\\Neos\\Controller\\.*',
                    ),
                  ),
                  'TYPO3.Neos:serviceControllers' => 
                  array (
                    'pattern' => 'ControllerObjectName',
                    'patternOptions' => 
                    array (
                      'controllerObjectNamePattern' => 'TYPO3\\Neos\\Service\\.*',
                    ),
                  ),
                  'TYPO3.Neos:mediaControllers' => 
                  array (
                    'pattern' => 'ControllerObjectName',
                    'patternOptions' => 
                    array (
                      'controllerObjectNamePattern' => 'TYPO3\\Media\\Controller\\.*',
                    ),
                  ),
                  'Flowpack\\Neos\\FrontendLogin\\Security\\NeosRequestPattern' => 'backend',
                ),
                'entryPoint' => 'WebRedirect',
                'entryPointOptions' => 
                array (
                  'routeValues' => 
                  array (
                    '@package' => 'TYPO3.Neos',
                    '@controller' => 'Login',
                    '@action' => 'index',
                    '@format' => 'html',
                  ),
                ),
              ),
              'Flowpack.Neos.FrontendLogin:Frontend' => 
              array (
                'provider' => 'PersistedUsernamePasswordProvider',
                'requestPatterns' => 
                array (
                  'Flowpack\\Neos\\FrontendLogin\\Security\\NeosRequestPattern' => 'frontend',
                ),
              ),
            ),
            'authenticationStrategy' => 'oneToken',
          ),
          'authorization' => 
          array (
            'allowAccessIfAllVotersAbstain' => false,
          ),
          'csrf' => 
          array (
            'csrfStrategy' => 'onePerSession',
          ),
          'cryptography' => 
          array (
            'hashingStrategies' => 
            array (
              'default' => 'bcrypt',
              'fallback' => 'pbkdf2',
              'pbkdf2' => 'TYPO3\\Flow\\Security\\Cryptography\\Pbkdf2HashingStrategy',
              'bcrypt' => 'TYPO3\\Flow\\Security\\Cryptography\\BCryptHashingStrategy',
              'saltedmd5' => 'TYPO3\\Flow\\Security\\Cryptography\\SaltedMd5HashingStrategy',
            ),
            'Pbkdf2HashingStrategy' => 
            array (
              'dynamicSaltLength' => 8,
              'iterationCount' => 10000,
              'derivedKeyLength' => 64,
              'algorithm' => 'sha256',
            ),
            'BCryptHashingStrategy' => 
            array (
              'cost' => 14,
            ),
            'RSAWalletServicePHP' => 
            array (
              'keystorePath' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Data/Persistent/RsaWalletData',
              'openSSLConfiguration' => 
              array (
              ),
            ),
          ),
        ),
        'session' => 
        array (
          'inactivityTimeout' => 3600,
          'name' => 'TYPO3_Flow_Session',
          'garbageCollection' => 
          array (
            'probability' => 1,
            'maximumPerRun' => 1000,
          ),
          'cookie' => 
          array (
            'lifetime' => 0,
            'path' => '/',
            'secure' => false,
            'httponly' => true,
            'domain' => NULL,
          ),
        ),
        'utility' => 
        array (
          'lockStrategyClassName' => 'TYPO3\\Flow\\Utility\\Lock\\FlockLockStrategy',
        ),
        'Utility' => 
        array (
          'Lock' => 
          array (
          ),
          'Unicode' => 
          array (
          ),
        ),
        'Error' => 
        array (
        ),
      ),
      'DocTools' => 
      array (
        'collections' => 
        array (
          'Flow' => 
          array (
            'commandReferences' => 
            array (
              0 => 'Flow:FlowCommands',
            ),
            'references' => 
            array (
              0 => 'Flow:FluidViewHelpers',
              1 => 'Flow:FlowValidators',
              2 => 'Flow:FlowSignals',
              3 => 'Flow:FlowTypeConverters',
              4 => 'Flow:FlowAnnotations',
            ),
          ),
          'Media' => 
          array (
            'commandReferences' => 
            array (
              0 => 'Media:Commands',
            ),
            'references' => 
            array (
              0 => 'Media:ViewHelpers',
              1 => 'Media:Validators',
            ),
          ),
          'Neos' => 
          array (
            'commandReferences' => 
            array (
              0 => 'Neos:NeosCommands',
            ),
            'references' => 
            array (
              0 => 'Neos:FluidViewHelpers',
              1 => 'Neos:MediaViewHelpers',
              2 => 'Neos:FormViewHelpers',
              3 => 'Neos:NeosViewHelpers',
              4 => 'Neos:Typo3CrViewHelpers',
              5 => 'Neos:TypoScriptViewHelpers',
              6 => 'Neos:FlowValidators',
              7 => 'Neos:PartyValidators',
              8 => 'Neos:MediaValidators',
              9 => 'Neos:FlowSignals',
              10 => 'Neos:NeosSignals',
              11 => 'Neos:MediaSignals',
              12 => 'Neos:Typo3CrSignals',
              13 => 'Neos:FlowQueryOperations',
              14 => 'Neos:EelHelpers',
            ),
          ),
        ),
        'commandReferences' => 
        array (
          'Flow:FlowCommands' => 
          array (
            'title' => 'Flow Command Reference',
            'packageKeys' => 
            array (
              0 => 'TYPO3.Flow',
              1 => 'TYPO3.Party',
              2 => 'TYPO3.Fluid',
              3 => 'TYPO3.Kickstart',
              4 => 'TYPO3.Welcome',
            ),
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Framework/TYPO3.Flow/Documentation/TheDefinitiveGuide/PartV/CommandReference.rst',
          ),
          'Media:Commands' => 
          array (
            'title' => 'Media Command Reference',
            'packageKeys' => 
            array (
              0 => 'TYPO3.Media',
            ),
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Media/Documentation/References/Commands.rst',
          ),
          'Neos:NeosCommands' => 
          array (
            'title' => 'Command Reference',
            'packageKeys' => 
            array (
              0 => 'TYPO3.Flow',
              1 => 'TYPO3.Party',
              2 => 'TYPO3.Fluid',
              3 => 'TYPO3.Kickstart',
              4 => 'TYPO3.Welcome',
              5 => 'TYPO3.Media',
              6 => 'TYPO3.TYPO3CR',
              7 => 'TYPO3.Neos.Kickstarter',
              8 => 'TYPO3.Neos',
            ),
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Neos/Documentation/References/CommandReference.rst',
          ),
        ),
        'references' => 
        array (
          'Flow:FluidViewHelpers' => 
          array (
            'title' => 'Fluid ViewHelper Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Framework/TYPO3.Flow/Documentation/TheDefinitiveGuide/PartV/FluidViewHelperReference.rst',
            'affectedClasses' => 
            array (
              'parentClassName' => 'TYPO3\\Fluid\\Core\\ViewHelper\\AbstractViewHelper',
              'classNamePattern' => '/^TYPO3\\\\Fluid\\\\ViewHelpers\\\\.*$/i',
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\FluidViewHelperClassParser',
              'options' => 
              array (
                'namespaces' => 
                array (
                  'f' => 'TYPO3\\Fluid\\ViewHelpers',
                ),
              ),
            ),
          ),
          'Flow:FlowValidators' => 
          array (
            'title' => 'Flow Validator Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Framework/TYPO3.Flow/Documentation/TheDefinitiveGuide/PartV/ValidatorReference.rst',
            'affectedClasses' => 
            array (
              'parentClassName' => 'TYPO3\\Flow\\Validation\\Validator\\AbstractValidator',
              'classNamePattern' => '/^TYPO3\\\\Flow\\\\Validation\\\\Validator\\\\.*$/i',
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\FlowValidatorClassParser',
            ),
          ),
          'Flow:FlowSignals' => 
          array (
            'title' => 'Flow Signals Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Framework/TYPO3.Flow/Documentation/TheDefinitiveGuide/PartV/SignalsReference.rst',
            'affectedClasses' => 
            array (
              'classesContainingMethodsAnnotatedWith' => 'TYPO3\\Flow\\Annotations\\Signal',
              'classNamePattern' => '/^TYPO3\\\\Flow\\\\.*$/i',
              'includeAbstractClasses' => true,
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\SignalsParser',
            ),
          ),
          'Flow:FlowTypeConverters' => 
          array (
            'title' => 'Flow TypeConverter Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Framework/TYPO3.Flow/Documentation/TheDefinitiveGuide/PartV/TypeConverterReference.rst',
            'affectedClasses' => 
            array (
              'parentClassName' => 'TYPO3\\Flow\\Property\\TypeConverter\\AbstractTypeConverter',
              'classNamePattern' => '/^TYPO3\\\\Flow\\\\.*$/i',
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\FlowTypeConverterClassParser',
            ),
          ),
          'Flow:FlowAnnotations' => 
          array (
            'title' => 'Flow Annotation Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Framework/TYPO3.Flow/Documentation/TheDefinitiveGuide/PartV/AnnotationReference.rst',
            'affectedClasses' => 
            array (
              'classNamePattern' => '/^TYPO3\\\\Flow\\\\Annotations\\\\.*$/i',
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\FlowAnnotationClassParser',
            ),
          ),
          'Media:Validators' => 
          array (
            'title' => 'Media Validator Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Media/Documentation/References/Validators.rst',
            'affectedClasses' => 
            array (
              'parentClassName' => 'TYPO3\\Flow\\Validation\\Validator\\AbstractValidator',
              'classNamePattern' => '/^TYPO3\\\\Media\\\\Validator\\\\.*$/i',
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\FlowValidatorClassParser',
            ),
          ),
          'Media:ViewHelpers' => 
          array (
            'title' => 'Media ViewHelper Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Media/Documentation/References/ViewHelpers.rst',
            'affectedClasses' => 
            array (
              'parentClassName' => 'TYPO3\\Fluid\\Core\\ViewHelper\\AbstractViewHelper',
              'classNamePattern' => '/^TYPO3\\\\Media\\\\ViewHelpers\\\\.*$/i',
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\FluidViewHelperClassParser',
              'options' => 
              array (
                'namespaces' => 
                array (
                  'typo3.media' => 'TYPO3\\Media\\ViewHelpers',
                ),
              ),
            ),
          ),
          'Neos:FluidViewHelpers' => 
          array (
            'title' => 'Fluid ViewHelper Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Neos/Documentation/References/ViewHelpers/Fluid.rst',
            'affectedClasses' => 
            array (
              'parentClassName' => 'TYPO3\\Fluid\\Core\\ViewHelper\\AbstractViewHelper',
              'classNamePattern' => '/^TYPO3\\\\Fluid\\\\ViewHelpers\\\\.*$/i',
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\FluidViewHelperClassParser',
              'options' => 
              array (
                'namespaces' => 
                array (
                  'f' => 'TYPO3\\Fluid\\ViewHelpers',
                ),
              ),
            ),
          ),
          'Neos:MediaViewHelpers' => 
          array (
            'title' => 'Media ViewHelper Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Neos/Documentation/References/ViewHelpers/Media.rst',
            'affectedClasses' => 
            array (
              'parentClassName' => 'TYPO3\\Fluid\\Core\\ViewHelper\\AbstractViewHelper',
              'classNamePattern' => '/^TYPO3\\\\Media\\\\ViewHelpers\\\\.*$/i',
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\FluidViewHelperClassParser',
              'options' => 
              array (
                'namespaces' => 
                array (
                  'typo3.media' => 'TYPO3\\Media\\ViewHelpers',
                ),
              ),
            ),
          ),
          'Neos:FormViewHelpers' => 
          array (
            'title' => 'Form ViewHelper Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Neos/Documentation/References/ViewHelpers/Form.rst',
            'affectedClasses' => 
            array (
              'parentClassName' => 'TYPO3\\Fluid\\Core\\ViewHelper\\AbstractViewHelper',
              'classNamePattern' => '/^TYPO3\\\\Form\\\\ViewHelpers\\\\.*$/i',
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\FluidViewHelperClassParser',
              'options' => 
              array (
                'namespaces' => 
                array (
                  'typo3.form' => 'TYPO3\\Form\\ViewHelpers',
                ),
              ),
            ),
          ),
          'Neos:NeosViewHelpers' => 
          array (
            'title' => 'Neos ViewHelper Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Neos/Documentation/References/ViewHelpers/Neos.rst',
            'affectedClasses' => 
            array (
              'parentClassName' => 'TYPO3\\Fluid\\Core\\ViewHelper\\AbstractViewHelper',
              'classNamePattern' => '/^TYPO3\\\\Neos\\\\ViewHelpers\\\\.*$/i',
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\FluidViewHelperClassParser',
              'options' => 
              array (
                'namespaces' => 
                array (
                  'f' => 'TYPO3\\Fluid\\ViewHelpers',
                  'neos' => 'TYPO3\\Neos\\ViewHelpers',
                ),
              ),
            ),
          ),
          'Neos:Typo3CrViewHelpers' => 
          array (
            'title' => 'Content Repository ViewHelper Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Neos/Documentation/References/ViewHelpers/TYPO3CR.rst',
            'affectedClasses' => 
            array (
              'parentClassName' => 'TYPO3\\Fluid\\Core\\ViewHelper\\AbstractViewHelper',
              'classNamePattern' => '/^TYPO3\\\\TYPO3CR\\\\ViewHelpers\\\\.*$/i',
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\FluidViewHelperClassParser',
              'options' => 
              array (
                'namespaces' => 
                array (
                  'f' => 'TYPO3\\Fluid\\ViewHelpers',
                  'neos' => 'TYPO3\\Neos\\ViewHelpers',
                ),
              ),
            ),
          ),
          'Neos:TypoScriptViewHelpers' => 
          array (
            'title' => 'TypoScript ViewHelper Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Neos/Documentation/References/ViewHelpers/TypoScript.rst',
            'affectedClasses' => 
            array (
              'parentClassName' => 'TYPO3\\Fluid\\Core\\ViewHelper\\AbstractViewHelper',
              'classNamePattern' => '/^TYPO3\\\\TypoScript\\\\ViewHelpers\\\\.*$/i',
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\FluidViewHelperClassParser',
              'options' => 
              array (
                'namespaces' => 
                array (
                  'f' => 'TYPO3\\Fluid\\ViewHelpers',
                  'ts' => 'TYPO3\\TypoScript\\ViewHelpers',
                ),
              ),
            ),
          ),
          'Neos:FlowValidators' => 
          array (
            'title' => 'Flow Validator Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Neos/Documentation/References/Validators/Flow.rst',
            'affectedClasses' => 
            array (
              'parentClassName' => 'TYPO3\\Flow\\Validation\\Validator\\AbstractValidator',
              'classNamePattern' => '/^TYPO3\\\\Flow\\\\Validation\\\\Validator\\\\.*$/i',
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\FlowValidatorClassParser',
            ),
          ),
          'Neos:PartyValidators' => 
          array (
            'title' => 'Party Validator Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Neos/Documentation/References/Validators/Party.rst',
            'affectedClasses' => 
            array (
              'parentClassName' => 'TYPO3\\Flow\\Validation\\Validator\\AbstractValidator',
              'classNamePattern' => '/^TYPO3\\\\Party\\\\Validation\\\\Validator\\\\.*$/i',
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\FlowValidatorClassParser',
            ),
          ),
          'Neos:MediaValidators' => 
          array (
            'title' => 'Media Validator Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Neos/Documentation/References/Validators/Media.rst',
            'affectedClasses' => 
            array (
              'parentClassName' => 'TYPO3\\Flow\\Validation\\Validator\\AbstractValidator',
              'classNamePattern' => '/^TYPO3\\\\Media\\\\Validator\\\\.*$/i',
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\FlowValidatorClassParser',
            ),
          ),
          'Neos:FlowSignals' => 
          array (
            'title' => 'Flow Signals Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Neos/Documentation/References/Signals/Flow.rst',
            'affectedClasses' => 
            array (
              'classesContainingMethodsAnnotatedWith' => 'TYPO3\\Flow\\Annotations\\Signal',
              'classNamePattern' => '/^TYPO3\\\\Flow\\\\.*$/i',
              'includeAbstractClasses' => true,
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\SignalsParser',
            ),
          ),
          'Neos:NeosSignals' => 
          array (
            'title' => 'Neos Signals Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Neos/Documentation/References/Signals/Neos.rst',
            'affectedClasses' => 
            array (
              'classesContainingMethodsAnnotatedWith' => 'TYPO3\\Flow\\Annotations\\Signal',
              'classNamePattern' => '/^TYPO3\\\\Neos\\\\.*$/i',
              'includeAbstractClasses' => true,
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\SignalsParser',
            ),
          ),
          'Neos:MediaSignals' => 
          array (
            'title' => 'Media Signals Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Neos/Documentation/References/Signals/Media.rst',
            'affectedClasses' => 
            array (
              'classesContainingMethodsAnnotatedWith' => 'TYPO3\\Flow\\Annotations\\Signal',
              'classNamePattern' => '/^TYPO3\\\\Media\\\\.*$/i',
              'includeAbstractClasses' => true,
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\SignalsParser',
            ),
          ),
          'Neos:Typo3CrSignals' => 
          array (
            'title' => 'Content Repository Signals Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Neos/Documentation/References/Signals/TYPO3CR.rst',
            'affectedClasses' => 
            array (
              'classesContainingMethodsAnnotatedWith' => 'TYPO3\\Flow\\Annotations\\Signal',
              'classNamePattern' => '/^TYPO3\\\\TYPO3CR\\\\.*$/i',
              'includeAbstractClasses' => true,
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\SignalsParser',
            ),
          ),
          'Neos:FlowQueryOperations' => 
          array (
            'title' => 'FlowQuery Operation Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Neos/Documentation/References/FlowQueryOperationReference.rst',
            'affectedClasses' => 
            array (
              'interface' => 'TYPO3\\Eel\\FlowQuery\\OperationInterface',
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\FlowQueryOperationClassParser',
            ),
          ),
          'Neos:EelHelpers' => 
          array (
            'title' => 'Eel Helpers Reference',
            'savePathAndFilename' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Neos/TYPO3.Neos/Documentation/References/EelHelpersReference.rst',
            'affectedClasses' => 
            array (
              'interface' => 'TYPO3\\Eel\\ProtectedContextAwareInterface',
              'classNamePattern' => '/^.*Helper$/i',
            ),
            'parser' => 
            array (
              'implementationClassName' => 'TYPO3\\DocTools\\Domain\\Service\\EelHelperClassParser',
            ),
          ),
        ),
        'bundles' => 
        array (
          'Form' => 
          array (
            'documentationRootPath' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Form/Documentation/Guide/source',
            'renderedDocumentationRootPath' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Data/Temporary/Documentation/Form',
            'configurationRootPath' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Form/Documentation/Guide/source',
            'imageRootPath' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Packages/Application/TYPO3.Form/Documentation/Guide/Images/',
          ),
        ),
      ),
      'Fluid' => 
      array (
      ),
      'Eel' => 
      array (
      ),
      'TypoScript' => 
      array (
        'rendering' => 
        array (
          'exceptionHandler' => 'TYPO3\\TypoScript\\Core\\ExceptionHandlers\\ThrowingHandler',
          'innerExceptionHandler' => 'TYPO3\\TypoScript\\Core\\ExceptionHandlers\\BubblingHandler',
        ),
        'debugMode' => false,
        'enableContentCache' => true,
        'defaultContext' => 
        array (
          'String' => 'TYPO3\\Eel\\Helper\\StringHelper',
          'Array' => 'TYPO3\\Eel\\Helper\\ArrayHelper',
          'Date' => 'TYPO3\\Eel\\Helper\\DateHelper',
          'Configuration' => 'TYPO3\\Eel\\Helper\\ConfigurationHelper',
          'Math' => 'TYPO3\\Eel\\Helper\\MathHelper',
          'Json' => 'TYPO3\\Eel\\Helper\\JsonHelper',
          'Security' => 'TYPO3\\Eel\\Helper\\SecurityHelper',
          'Translation' => 'TYPO3\\Flow\\I18n\\EelHelper\\TranslationHelper',
          'Type' => 'TYPO3\\Eel\\Helper\\TypeHelper',
          'I18n' => 'TYPO3\\Flow\\I18n\\EelHelper\\TranslationHelper',
          'Neos.Node' => 'TYPO3\\Neos\\TypoScript\\Helper\\NodeHelper',
          'Neos.Link' => 'TYPO3\\Neos\\TypoScript\\Helper\\LinkHelper',
          'Neos.Array' => 'TYPO3\\Neos\\TypoScript\\Helper\\ArrayHelper',
          'Neos.Rendering' => 'TYPO3\\Neos\\TypoScript\\Helper\\RenderingHelper',
          'Neos.Caching' => 'TYPO3\\Neos\\TypoScript\\Helper\\CachingHelper',
        ),
      ),
      'Imagine' => 
      array (
        'driver' => 'Gd',
        'profile' => 
        array (
          'RGB' => 'color.org/sRGB_IEC61966-2-1_black_scaled.icc',
          'CMYK' => 'Adobe/CMYK/USWebUncoated.icc',
          'Grayscale' => 'colormanagement.org/ISOcoated_v2_grey1c_bas.ICC',
        ),
      ),
      'Media' => 
      array (
        'behaviourFlag' => '1.2',
        'asyncThumbnails' => true,
        'thumbnailPresets' => 
        array (
          'TYPO3.Neos:Thumbnail' => 
          array (
            'maximumWidth' => 250,
            'maximumHeight' => 250,
          ),
          'TYPO3.Neos:Preview' => 
          array (
            'maximumWidth' => 1000,
            'maximumHeight' => 1000,
          ),
        ),
        'autoCreateThumbnailPresets' => true,
        'asset' => 
        array (
          'modelMappingStrategy' => 
          array (
            'default' => 'TYPO3\\Media\\Domain\\Model\\Document',
            'patterns' => 
            array (
              '[image/.*]' => 
              array (
                'className' => 'TYPO3\\Media\\Domain\\Model\\Image',
              ),
              '[audio/.*]' => 
              array (
                'className' => 'TYPO3\\Media\\Domain\\Model\\Audio',
              ),
              '[video/.*]' => 
              array (
                'className' => 'TYPO3\\Media\\Domain\\Model\\Video',
              ),
            ),
          ),
        ),
        'image' => 
        array (
          'defaultOptions' => 
          array (
            'quality' => 90,
            'convertCMYKToRGB' => true,
          ),
        ),
        'thumbnailGenerators' => 
        array (
          'TYPO3\\Media\\Domain\\Model\\ThumbnailGenerator\\DocumentThumbnailGenerator' => 
          array (
            'resolution' => 120,
            'supportedExtensions' => 
            array (
              0 => 'pdf',
              1 => 'eps',
              2 => 'ai',
            ),
            'paginableDocuments' => 
            array (
              0 => 'pdf',
            ),
          ),
          'TYPO3\\Media\\Domain\\Model\\ThumbnailGenerator\\FontDocumentThumbnailGenerator' => 
          array (
            'supportedExtensions' => 
            array (
              0 => 'ttf',
              1 => 'otf',
            ),
          ),
        ),
        'bodyClasses' => 'neos neos-module media-browser',
        'scripts' => 
        array (
          0 => 'resource://TYPO3.Neos/Public/Library/jquery/jquery-2.0.3.js',
          1 => 'resource://TYPO3.Twitter.Bootstrap/Public/2/js/bootstrap.min.js',
          2 => 'resource://TYPO3.Neos/Public/Library/bootstrap-components.js',
          3 => 'resource://TYPO3.Neos/Public/JavaScript/Modules/media-browser.js',
        ),
        'styles' => 
        array (
          0 => 'resource://TYPO3.Neos/Public/Styles/Neos.css',
          1 => 'resource://TYPO3.Media/Public/Libraries/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css',
          2 => 'resource://TYPO3.Media/Public/Libraries/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.css',
        ),
      ),
      'Party' => 
      array (
      ),
      'Form' => 
      array (
        'yamlPersistenceManager' => 
        array (
          'savePath' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Data/Forms/',
        ),
        'supertypeResolver' => 
        array (
          'hiddenProperties' => 
          array (
          ),
        ),
        'presets' => 
        array (
          'default' => 
          array (
            'title' => 'Default',
            'stylesheets' => 
            array (
            ),
            'javaScripts' => 
            array (
            ),
            'formElementTypes' => 
            array (
              'TYPO3.Form:Base' => 
              array (
                'renderingOptions' => 
                array (
                  'templatePathPattern' => 'resource://{@package}/Private/Form/{@type}.html',
                  'partialPathPattern' => 'resource://{@package}/Private/Form/Partials/{@type}.html',
                  'layoutPathPattern' => 'resource://{@package}/Private/Form/Layouts/{@type}.html',
                  'skipUnknownElements' => false,
                  'translationPackage' => 'TYPO3.Flow',
                ),
              ),
              'TYPO3.Form:Form' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:Base' => true,
                ),
                'rendererClassName' => 'TYPO3\\Form\\Core\\Renderer\\FluidFormRenderer',
                'renderingOptions' => 
                array (
                  'renderableNameInTemplate' => 'form',
                ),
              ),
              'TYPO3.Form:RemovableMixin' => 
              array (
              ),
              'TYPO3.Form:ReadOnlyFormElement' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:Base' => true,
                  'TYPO3.Form:RemovableMixin' => true,
                ),
                'implementationClassName' => 'TYPO3\\Form\\FormElements\\GenericFormElement',
                'renderingOptions' => 
                array (
                  'renderableNameInTemplate' => 'element',
                ),
              ),
              'TYPO3.Form:FormElement' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:Base' => true,
                  'TYPO3.Form:RemovableMixin' => true,
                ),
                'implementationClassName' => 'TYPO3\\Form\\FormElements\\GenericFormElement',
                'properties' => 
                array (
                  'containerClassAttribute' => 'input',
                  'elementClassAttribute' => '',
                  'elementErrorClassAttribute' => 'error',
                ),
                'renderingOptions' => 
                array (
                  'renderableNameInTemplate' => 'element',
                ),
              ),
              'TYPO3.Form:Page' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:Base' => true,
                  'TYPO3.Form:RemovableMixin' => true,
                ),
                'implementationClassName' => 'TYPO3\\Form\\Core\\Model\\Page',
                'renderingOptions' => 
                array (
                  'renderableNameInTemplate' => 'page',
                ),
              ),
              'TYPO3.Form:PreviewPage' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:Page' => true,
                ),
              ),
              'TYPO3.Form:Section' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:FormElement' => true,
                ),
                'implementationClassName' => 'TYPO3\\Form\\FormElements\\Section',
                'renderingOptions' => 
                array (
                  'renderableNameInTemplate' => 'section',
                ),
              ),
              'TYPO3.Form:TextMixin' => 
              array (
              ),
              'TYPO3.Form:SingleLineText' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:FormElement' => true,
                  'TYPO3.Form:TextMixin' => true,
                ),
              ),
              'TYPO3.Form:Password' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:FormElement' => true,
                  'TYPO3.Form:TextMixin' => true,
                ),
              ),
              'TYPO3.Form:PasswordWithConfirmation' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:Password' => true,
                ),
                'implementationClassName' => 'TYPO3\\Form\\FormElements\\PasswordWithConfirmation',
                'properties' => 
                array (
                  'elementClassAttribute' => 'input-medium',
                  'confirmationLabel' => 'Confirmation',
                  'confirmationClassAttribute' => 'input-medium',
                ),
              ),
              'TYPO3.Form:MultiLineText' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:FormElement' => true,
                  'TYPO3.Form:TextMixin' => true,
                ),
                'properties' => 
                array (
                  'elementClassAttribute' => 'xxlarge',
                ),
              ),
              'TYPO3.Form:SelectionMixin' => 
              array (
              ),
              'TYPO3.Form:SingleSelectionMixin' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:SelectionMixin' => true,
                ),
              ),
              'TYPO3.Form:MultiSelectionMixin' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:SelectionMixin' => true,
                ),
              ),
              'TYPO3.Form:Checkbox' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:FormElement' => true,
                ),
                'properties' => 
                array (
                  'elementClassAttribute' => 'add-on',
                  'value' => 1,
                ),
              ),
              'TYPO3.Form:MultipleSelectCheckboxes' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:FormElement' => true,
                  'TYPO3.Form:MultiSelectionMixin' => true,
                ),
              ),
              'TYPO3.Form:MultipleSelectDropdown' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:FormElement' => true,
                  'TYPO3.Form:MultiSelectionMixin' => true,
                ),
                'properties' => 
                array (
                  'elementClassAttribute' => 'xlarge',
                ),
              ),
              'TYPO3.Form:SingleSelectRadiobuttons' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:FormElement' => true,
                  'TYPO3.Form:SingleSelectionMixin' => true,
                ),
                'properties' => 
                array (
                  'elementClassAttribute' => 'xlarge',
                ),
              ),
              'TYPO3.Form:SingleSelectDropdown' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:FormElement' => true,
                  'TYPO3.Form:SingleSelectionMixin' => true,
                ),
              ),
              'TYPO3.Form:DatePicker' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:FormElement' => true,
                ),
                'implementationClassName' => 'TYPO3\\Form\\FormElements\\DatePicker',
                'properties' => 
                array (
                  'elementClassAttribute' => 'small',
                  'timeSelectorClassAttribute' => 'mini',
                  'dateFormat' => 'Y-m-d',
                  'enableDatePicker' => true,
                  'displayTimeSelector' => false,
                ),
              ),
              'TYPO3.Form:FileUpload' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:FormElement' => true,
                ),
                'implementationClassName' => 'TYPO3\\Form\\FormElements\\FileUpload',
                'properties' => 
                array (
                  'allowedExtensions' => 
                  array (
                    0 => 'pdf',
                    1 => 'doc',
                  ),
                ),
              ),
              'TYPO3.Form:ImageUpload' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:FormElement' => true,
                ),
                'implementationClassName' => 'TYPO3\\Form\\FormElements\\ImageUpload',
                'properties' => 
                array (
                  'allowedTypes' => 
                  array (
                    0 => 'jpeg',
                    1 => 'png',
                    2 => 'bmp',
                  ),
                ),
              ),
              'TYPO3.Form:StaticText' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:ReadOnlyFormElement' => true,
                ),
                'properties' => 
                array (
                  'text' => '',
                ),
              ),
              'TYPO3.Form:HiddenField' => 
              array (
                'superTypes' => 
                array (
                  'TYPO3.Form:FormElement' => true,
                ),
              ),
            ),
            'finisherPresets' => 
            array (
              'TYPO3.Form:Closure' => 
              array (
                'implementationClassName' => 'TYPO3\\Form\\Finishers\\ClosureFinisher',
                'options' => 
                array (
                ),
              ),
              'TYPO3.Form:Confirmation' => 
              array (
                'implementationClassName' => 'TYPO3\\Form\\Finishers\\ConfirmationFinisher',
                'options' => 
                array (
                ),
              ),
              'TYPO3.Form:Email' => 
              array (
                'implementationClassName' => 'TYPO3\\Form\\Finishers\\EmailFinisher',
                'options' => 
                array (
                ),
              ),
              'TYPO3.Form:FlashMessage' => 
              array (
                'implementationClassName' => 'TYPO3\\Form\\Finishers\\FlashMessageFinisher',
                'options' => 
                array (
                ),
              ),
              'TYPO3.Form:Redirect' => 
              array (
                'implementationClassName' => 'TYPO3\\Form\\Finishers\\RedirectFinisher',
                'options' => 
                array (
                ),
              ),
            ),
            'validatorPresets' => 
            array (
              'TYPO3.Flow:NotEmpty' => 
              array (
                'implementationClassName' => 'TYPO3\\Flow\\Validation\\Validator\\NotEmptyValidator',
              ),
              'TYPO3.Flow:DateTimeRange' => 
              array (
                'implementationClassName' => 'TYPO3\\Flow\\Validation\\Validator\\DateTimeRangeValidator',
              ),
              'TYPO3.Flow:Alphanumeric' => 
              array (
                'implementationClassName' => 'TYPO3\\Flow\\Validation\\Validator\\AlphanumericValidator',
              ),
              'TYPO3.Flow:Text' => 
              array (
                'implementationClassName' => 'TYPO3\\Flow\\Validation\\Validator\\TextValidator',
              ),
              'TYPO3.Flow:StringLength' => 
              array (
                'implementationClassName' => 'TYPO3\\Flow\\Validation\\Validator\\StringLengthValidator',
              ),
              'TYPO3.Flow:EmailAddress' => 
              array (
                'implementationClassName' => 'TYPO3\\Flow\\Validation\\Validator\\EmailAddressValidator',
              ),
              'TYPO3.Flow:Integer' => 
              array (
                'implementationClassName' => 'TYPO3\\Flow\\Validation\\Validator\\IntegerValidator',
              ),
              'TYPO3.Flow:Float' => 
              array (
                'implementationClassName' => 'TYPO3\\Flow\\Validation\\Validator\\FloatValidator',
              ),
              'TYPO3.Flow:NumberRange' => 
              array (
                'implementationClassName' => 'TYPO3\\Flow\\Validation\\Validator\\NumberRangeValidator',
              ),
              'TYPO3.Flow:RegularExpression' => 
              array (
                'implementationClassName' => 'TYPO3\\Flow\\Validation\\Validator\\RegularExpressionValidator',
              ),
              'TYPO3.Flow:Count' => 
              array (
                'implementationClassName' => 'TYPO3\\Flow\\Validation\\Validator\\CountValidator',
              ),
            ),
          ),
          'typo3.setup' => 
          array (
            'title' => 'Setup Elements',
            'parentPreset' => 'default',
            'formElementTypes' => 
            array (
              'TYPO3.Form:Base' => 
              array (
                'renderingOptions' => 
                array (
                  'layoutPathPattern' => 'resource://TYPO3.Setup/Private/Form/Layouts/{@type}.html',
                ),
              ),
              'TYPO3.Form:Form' => 
              array (
                'renderingOptions' => 
                array (
                  'templatePathPattern' => 'resource://TYPO3.Setup/Private/Form/{@type}.html',
                ),
              ),
              'TYPO3.Setup:LinkElement' => 
              array (
                'superTypes' => 
                array (
                  0 => 'TYPO3.Form:ReadOnlyFormElement',
                ),
                'properties' => 
                array (
                  'text' => '',
                  'class' => 'btn',
                  'href' => '',
                ),
              ),
              'TYPO3.Setup:DatabaseSelector' => 
              array (
                'superTypes' => 
                array (
                  0 => 'TYPO3.Form:FormElement',
                ),
                'properties' => 
                array (
                  'elementClassAttribute' => 'form-control',
                ),
              ),
              'TYPO3.Form:SingleLineText' => 
              array (
                'properties' => 
                array (
                  'elementClassAttribute' => 'form-control',
                ),
              ),
              'TYPO3.Form:Password' => 
              array (
                'properties' => 
                array (
                  'elementClassAttribute' => 'form-control',
                ),
              ),
              'TYPO3.Form:PasswordWithConfirmation' => 
              array (
                'renderingOptions' => 
                array (
                  'templatePathPattern' => 'resource://TYPO3.Setup/Private/Form/{@type}.html',
                ),
                'properties' => 
                array (
                  'elementClassAttribute' => 'form-control',
                  'confirmationClassAttribute' => 'form-control',
                ),
              ),
              'TYPO3.Form:Checkbox' => 
              array (
                'renderingOptions' => 
                array (
                  'templatePathPattern' => 'resource://TYPO3.Setup/Private/Form/{@type}.html',
                ),
                'properties' => 
                array (
                  'elementClassAttribute' => 'checkbox',
                ),
              ),
              'TYPO3.Form:MultipleSelectDropdown' => 
              array (
                'properties' => 
                array (
                  'elementClassAttribute' => 'form-control',
                ),
              ),
              'TYPO3.Form:SingleSelectDropdown' => 
              array (
                'renderingOptions' => 
                array (
                  'templatePathPattern' => 'resource://TYPO3.Setup/Private/Form/{@type}.html',
                ),
              ),
            ),
          ),
        ),
      ),
      'Twitter' => 
      array (
        'Bootstrap' => 
        array (
          'viewHelpers' => 
          array (
            'partialRootPath' => 'resource://TYPO3.Twitter.Bootstrap/Private/Partials/',
            'templates' => 
            array (
              'TYPO3\\Twitter\\Bootstrap\\ViewHelpers\\Navigation\\MenuViewHelper' => 'resource://TYPO3.Twitter.Bootstrap/Private/Templates/Navigation/Menu.html',
            ),
          ),
        ),
      ),
      'Setup' => 
      array (
        'initialPasswordFile' => '/Applications/XAMPP/xamppfiles/htdocs/outfit/Data/SetupPassword.txt',
        'stepOrder' => 
        array (
          0 => 'neosRequirements',
          1 => 'database',
          2 => 'administrator',
          3 => 'siteimport',
          4 => 'final',
        ),
        'steps' => 
        array (
          'database' => 
          array (
            'className' => 'TYPO3\\Setup\\Step\\DatabaseStep',
            'requiredConditions' => 
            array (
              0 => 
              array (
                'className' => 'TYPO3\\Setup\\Condition\\PdoDriverCondition',
              ),
            ),
          ),
          'final' => 
          array (
            'className' => 'TYPO3\\Neos\\Setup\\Step\\FinalStep',
          ),
          'neosRequirements' => 
          array (
            'className' => 'TYPO3\\Neos\\Setup\\Step\\NeosSpecificRequirementsStep',
          ),
          'administrator' => 
          array (
            'className' => 'TYPO3\\Neos\\Setup\\Step\\AdministratorStep',
            'requiredConditions' => 
            array (
              0 => 
              array (
                'className' => 'TYPO3\\Setup\\Condition\\DatabaseConnectionCondition',
              ),
            ),
          ),
          'siteimport' => 
          array (
            'className' => 'TYPO3\\Neos\\Setup\\Step\\SiteImportStep',
            'requiredConditions' => 
            array (
              0 => 
              array (
                'className' => 'TYPO3\\Setup\\Condition\\DatabaseConnectionCondition',
              ),
            ),
          ),
        ),
        'view' => 
        array (
          'title' => 'Neos Setup',
        ),
        'http' => 
        array (
          'chain' => 
          array (
            'preprocess' => 
            array (
              'chain' => 
              array (
                'configureRouting' => 
                array (
                  'position' => 'start',
                  'component' => 'TYPO3\\Setup\\Core\\ConfigureRoutingComponent',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3CR' => 
      array (
        'contentDimensions' => 
        array (
        ),
        'labelGenerator' => 
        array (
          'eel' => 
          array (
            'defaultContext' => 
            array (
              'String' => 'TYPO3\\Eel\\Helper\\StringHelper',
              'Array' => 'TYPO3\\Eel\\Helper\\ArrayHelper',
              'Date' => 'TYPO3\\Eel\\Helper\\DateHelper',
              'Configuration' => 'TYPO3\\Eel\\Helper\\ConfigurationHelper',
              'Math' => 'TYPO3\\Eel\\Helper\\MathHelper',
              'Json' => 'TYPO3\\Eel\\Helper\\JsonHelper',
              'I18n' => 'TYPO3\\Flow\\I18n\\EelHelper\\TranslationHelper',
            ),
          ),
        ),
        'fallbackNodeType' => 'TYPO3.Neos:FallbackNode',
      ),
      'Neos' => 
      array (
        'typoScript' => 
        array (
          'enableObjectTreeCache' => false,
          'autoInclude' => 
          array (
            'TYPO3.TypoScript' => true,
            'TYPO3.Neos' => true,
            'TYPO3.Neos.NodeTypes' => true,
            'Flowpack.Neos.FrontendLogin' => true,
            'TYPO3.Neos.Seo' => true,
          ),
        ),
        'routing' => 
        array (
          'supportEmptySegmentForDimensions' => true,
        ),
        'nodeTypes' => 
        array (
          'groups' => 
          array (
            'general' => 
            array (
              'position' => 'start',
              'label' => 'TYPO3.Neos:Main:nodeTypes.groups.general',
              'collapsed' => false,
            ),
            'structure' => 
            array (
              'position' => 100,
              'label' => 'TYPO3.Neos:Main:nodeTypes.groups.structure',
              'collapsed' => false,
            ),
            'plugins' => 
            array (
              'position' => 200,
              'label' => 'TYPO3.Neos:Main:nodeTypes.groups.plugins',
              'collapsed' => true,
            ),
          ),
        ),
        'userInterface' => 
        array (
          'loadMinifiedJavascript' => true,
          'requireJsWaitSeconds' => 30,
          'scrambleTranslatedLabels' => false,
          'translation' => 
          array (
            'autoInclude' => 
            array (
              'TYPO3.Neos' => 
              array (
                0 => 'Main',
                1 => 'Inspector',
                2 => 'Modules',
                3 => 'NodeTypes/*',
              ),
              'TYPO3.Neos.NodeTypes' => 
              array (
                0 => 'NodeTypes/*',
              ),
              'Flowpack.Neos.FrontendLogin' => 
              array (
                0 => 'NodeTypes/*',
              ),
              'TYPO3.Neos.Seo' => 
              array (
                0 => 'NodeTypes/*',
              ),
            ),
          ),
          'requireJsPathMapping' => 
          array (
            'TYPO3.Neos/Validation' => 'resource://TYPO3.Neos/Public/JavaScript/Shared/Validation/',
            'TYPO3.Neos/Inspector/Editors' => 'resource://TYPO3.Neos/Public/JavaScript/Content/Inspector/Editors/',
            'TYPO3.Neos/Inspector/Handlers' => 'resource://TYPO3.Neos/Public/JavaScript/Content/Inspector/Handlers/',
            'TYPO3.Neos/Inspector/Views' => 'resource://TYPO3.Neos/Public/JavaScript/Content/Inspector/Views/',
          ),
          'defaultLanguage' => 'en',
          'availableLanguages' => 
          array (
            'da' => 'Dansk – Danish',
            'de' => 'Deutsch – German',
            'en' => 'English – English',
            'es' => 'Español – Spanish',
            'fi' => 'Suomi – Finnish',
            'fr' => 'Français – French',
            'km' => 'ភាសាខ្មែរ – Khmer',
            'lv' => 'Latviešu – Latvian',
            'nl' => 'Nederlands – Dutch',
            'no' => 'Norsk bokmål – Norwegian Bokmål',
            'pl' => 'Polski – Polish',
            'pt-BR' => 'Português (Brasil) – Portuguese (Brazil)',
            'ru' => 'Pусский – Russian',
            'zh-CN' => '简体中文 – Chinese, Simplified',
          ),
          'navigateComponent' => 
          array (
            'nodeTree' => 
            array (
              'loadingDepth' => 4,
              'presets' => 
              array (
                'default' => 
                array (
                  'baseNodeType' => 'TYPO3.Neos:Document',
                ),
              ),
            ),
            'structureTree' => 
            array (
              'loadingDepth' => 4,
            ),
          ),
          'inspector' => 
          array (
            'dataTypes' => 
            array (
              'string' => 
              array (
                'editor' => 'TYPO3.Neos/Inspector/Editors/TextFieldEditor',
                'defaultValue' => '',
              ),
              'integer' => 
              array (
                'editor' => 'TYPO3.Neos/Inspector/Editors/TextFieldEditor',
                'defaultValue' => 0,
              ),
              'boolean' => 
              array (
                'editor' => 'TYPO3.Neos/Inspector/Editors/BooleanEditor',
                'defaultValue' => false,
              ),
              'array' => 
              array (
                'typeConverter' => 'TYPO3\\Flow\\Property\\TypeConverter\\TypedArrayConverter',
                'editor' => 'TYPO3.Neos/Inspector/Editors/SelectBoxEditor',
                'editorOptions' => 
                array (
                  'multiple' => true,
                  'placeholder' => 'Choose',
                ),
                'defaultValue' => 
                array (
                ),
              ),
              'TYPO3\\Media\\Domain\\Model\\ImageInterface' => 
              array (
                'typeConverter' => 'TYPO3\\Media\\TypeConverter\\ImageInterfaceJsonSerializer',
                'editor' => 'TYPO3.Neos/Inspector/Editors/ImageEditor',
                'editorOptions' => 
                array (
                  'maximumFileSize' => NULL,
                  'features' => 
                  array (
                    'crop' => true,
                    'resize' => false,
                  ),
                  'crop' => 
                  array (
                    'aspectRatio' => 
                    array (
                      'options' => 
                      array (
                        'square' => 
                        array (
                          'width' => 1,
                          'height' => 1,
                          'label' => 'Square',
                        ),
                        'fourFive' => 
                        array (
                          'width' => 4,
                          'height' => 5,
                        ),
                        'fiveSeven' => 
                        array (
                          'width' => 5,
                          'height' => 7,
                        ),
                        'twoThree' => 
                        array (
                          'width' => 2,
                          'height' => 3,
                        ),
                        'fourThree' => 
                        array (
                          'width' => 4,
                          'height' => 3,
                        ),
                        'sixteenNine' => 
                        array (
                          'width' => 16,
                          'height' => 9,
                        ),
                      ),
                      'enableOriginal' => true,
                      'allowCustom' => true,
                      'locked' => 
                      array (
                        'width' => 0,
                        'height' => 0,
                      ),
                    ),
                  ),
                ),
              ),
              'TYPO3\\Media\\Domain\\Model\\Asset' => 
              array (
                'typeConverter' => 'TYPO3\\Neos\\TypeConverter\\EntityToIdentityConverter',
                'editor' => 'TYPO3.Neos/Inspector/Editors/AssetEditor',
              ),
              'array<TYPO3\\Media\\Domain\\Model\\Asset>' => 
              array (
                'editor' => 'TYPO3.Neos/Inspector/Editors/AssetEditor',
                'editorOptions' => 
                array (
                  'multiple' => true,
                ),
              ),
              'DateTime' => 
              array (
                'typeConverter' => 'TYPO3\\Neos\\Service\\Mapping\\DateStringConverter',
                'editor' => 'TYPO3.Neos/Inspector/Editors/DateTimeEditor',
                'editorOptions' => 
                array (
                  'format' => 'd-m-Y',
                ),
              ),
              'reference' => 
              array (
                'typeConverter' => 'TYPO3\\Neos\\Service\\Mapping\\NodeReferenceConverter',
                'editor' => 'TYPO3.Neos/Inspector/Editors/ReferenceEditor',
              ),
              'references' => 
              array (
                'typeConverter' => 'TYPO3\\Neos\\Service\\Mapping\\NodeReferenceConverter',
                'editor' => 'TYPO3.Neos/Inspector/Editors/ReferencesEditor',
              ),
            ),
            'editors' => 
            array (
              'TYPO3.Neos/Inspector/Editors/CodeEditor' => 
              array (
                'editorOptions' => 
                array (
                  'buttonLabel' => 'TYPO3.Neos:Main:content.inspector.editors.codeEditor.editCode',
                ),
              ),
              'TYPO3.Neos/Inspector/Editors/DateTimeEditor' => 
              array (
                'editorOptions' => 
                array (
                  'placeholder' => 'TYPO3.Neos:Main:content.inspector.editors.dateTimeEditor.noDateSet',
                ),
              ),
              'TYPO3.Neos/Inspector/Editors/AssetEditor' => 
              array (
                'editorOptions' => 
                array (
                  'fileChooserLabel' => 'TYPO3.Neos:Main:choose',
                ),
              ),
              'TYPO3.Neos/Inspector/Editors/ImageEditor' => 
              array (
                'editorOptions' => 
                array (
                  'fileChooserLabel' => 'TYPO3.Neos:Main:choose',
                ),
              ),
              'TYPO3.Neos/Inspector/Editors/LinkEditor' => 
              array (
                'editorOptions' => 
                array (
                  'placeholder' => 'TYPO3.Neos:Main:content.inspector.editors.linkEditor.search',
                ),
              ),
              'TYPO3.Neos/Inspector/Editors/ReferencesEditor' => 
              array (
                'editorOptions' => 
                array (
                  'placeholder' => 'TYPO3.Neos:Main:typeToSearch',
                ),
              ),
              'TYPO3.Neos/Inspector/Editors/ReferenceEditor' => 
              array (
                'editorOptions' => 
                array (
                  'placeholder' => 'TYPO3.Neos:Main:typeToSearch',
                ),
              ),
              'TYPO3.Neos/Inspector/Editors/SelectBoxEditor' => 
              array (
                'editorOptions' => 
                array (
                  'placeholder' => 'TYPO3.Neos:Main:choose',
                ),
              ),
            ),
          ),
          'defaultEditPreviewMode' => 'inPlace',
          'editPreviewModes' => 
          array (
            'live' => 
            array (
              'isEditingMode' => false,
              'isPreviewMode' => false,
              'typoScriptRenderingPath' => '',
              'title' => 'Live',
            ),
            'inPlace' => 
            array (
              'isEditingMode' => true,
              'isPreviewMode' => false,
              'typoScriptRenderingPath' => '',
              'title' => 'TYPO3.Neos:Main:editPreviewModes.inPlace',
              'position' => 100,
            ),
            'rawContent' => 
            array (
              'isEditingMode' => true,
              'isPreviewMode' => false,
              'typoScriptRenderingPath' => 'rawContent',
              'title' => 'TYPO3.Neos:Main:editPreviewModes.rawContent',
              'position' => 200,
            ),
            'desktop' => 
            array (
              'isEditingMode' => false,
              'isPreviewMode' => true,
              'typoScriptRenderingPath' => '',
              'title' => 'TYPO3.Neos:Main:editPreviewModes.desktop',
              'position' => 100,
            ),
          ),
          'backendLoginForm' => 
          array (
            'backgroundImage' => 'resource://TYPO3.Neos/Public/Images/Login/Wallpaper23.jpg',
            'stylesheets' => 
            array (
              'TYPO3.Neos:DefaultStyles' => 'resource://TYPO3.Neos/Public/Styles/Login.css',
            ),
          ),
        ),
        'moduleConfiguration' => 
        array (
          'widgetTemplatePathAndFileName' => 'resource://TYPO3.Neos/Private/Templates/Module/Widget.html',
        ),
        'modules' => 
        array (
          'management' => 
          array (
            'label' => 'TYPO3.Neos:Modules:management.label',
            'controller' => '\\TYPO3\\Neos\\Controller\\Module\\ManagementController',
            'description' => 'TYPO3.Neos:Modules:management.description',
            'icon' => 'icon-briefcase',
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Management',
            'submodules' => 
            array (
              'workspaces' => 
              array (
                'label' => 'TYPO3.Neos:Modules:workspaces.label',
                'controller' => '\\TYPO3\\Neos\\Controller\\Module\\Management\\WorkspacesController',
                'description' => 'TYPO3.Neos:Modules:workspaces.description',
                'icon' => 'icon-th-large',
                'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Management.Workspaces',
              ),
              'media' => 
              array (
                'label' => 'TYPO3.Neos:Modules:media.label',
                'controller' => '\\TYPO3\\Neos\\Controller\\Module\\Management\\AssetController',
                'description' => 'TYPO3.Neos:Modules:media.description',
                'icon' => 'icon-camera',
                'privilegeTarget' => 'TYPO3.Media:ManageAssets',
              ),
              'history' => 
              array (
                'label' => 'TYPO3.Neos:Modules:history.label',
                'controller' => '\\TYPO3\\Neos\\Controller\\Module\\Management\\HistoryController',
                'description' => 'TYPO3.Neos:Modules:history.description',
                'icon' => 'icon-calendar',
                'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Management.History',
              ),
            ),
          ),
          'administration' => 
          array (
            'label' => 'TYPO3.Neos:Modules:administration.label',
            'controller' => '\\TYPO3\\Neos\\Controller\\Module\\AdministrationController',
            'description' => 'TYPO3.Neos:Modules:administration.description',
            'icon' => 'icon-gears',
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Administration',
            'submodules' => 
            array (
              'users' => 
              array (
                'label' => 'TYPO3.Neos:Modules:users.label',
                'controller' => '\\TYPO3\\Neos\\Controller\\Module\\Administration\\UsersController',
                'description' => 'TYPO3.Neos:Modules:users.description',
                'icon' => 'icon-group',
                'actions' => 
                array (
                  'new' => 
                  array (
                    'label' => 'TYPO3.Neos:Modules:users.actions.new.label',
                    'title' => 'TYPO3.Neos:Modules:users.actions.new.title',
                  ),
                ),
                'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Administration.Users',
              ),
              'packages' => 
              array (
                'label' => 'TYPO3.Neos:Modules:packages.label',
                'controller' => '\\TYPO3\\Neos\\Controller\\Module\\Administration\\PackagesController',
                'description' => 'TYPO3.Neos:Modules:packages.description',
                'icon' => 'icon-archive',
                'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Administration.Packages',
              ),
              'sites' => 
              array (
                'label' => 'TYPO3.Neos:Modules:sites.label',
                'controller' => '\\TYPO3\\Neos\\Controller\\Module\\Administration\\SitesController',
                'description' => 'TYPO3.Neos:Modules:sites.description',
                'icon' => 'icon-globe',
                'actions' => 
                array (
                  'newSite' => 
                  array (
                    'label' => 'TYPO3.Neos:Modules:sites.actions.newSite.label',
                    'title' => 'TYPO3.Neos:Modules:sites.actions.newSite.title',
                  ),
                ),
                'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Administration.Sites',
              ),
              'configuration' => 
              array (
                'label' => 'TYPO3.Neos:Modules:configuration.label',
                'controller' => '\\TYPO3\\Neos\\Controller\\Module\\Administration\\ConfigurationController',
                'description' => 'TYPO3.Neos:Modules:configuration.description',
                'icon' => 'icon-list-alt',
                'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Administration.Configuration',
              ),
            ),
          ),
          'user' => 
          array (
            'label' => 'TYPO3.Neos:Modules:user.label',
            'controller' => '\\TYPO3\\Neos\\Controller\\Module\\UserController',
            'hideInMenu' => true,
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.User',
            'submodules' => 
            array (
              'usersettings' => 
              array (
                'label' => 'TYPO3.Neos:Modules:userSettings.label',
                'controller' => '\\TYPO3\\Neos\\Controller\\Module\\User\\UserSettingsController',
                'description' => 'TYPO3.Neos:Modules:userSettings.description',
                'icon' => 'icon-user',
                'privilegeTarget' => 'TYPO3.Neos:Backend.Module.User.UserSettings',
              ),
            ),
          ),
        ),
        'eventLog' => 
        array (
          'enabled' => false,
          'monitorEntities' => 
          array (
            'TYPO3\\Flow\\Security\\Account' => 
            array (
              'events' => 
              array (
                'created' => 'Account.Created',
                'deleted' => 'Account.Deleted',
              ),
              'data' => 
              array (
                'accountIdentifier' => '${entity.accountIdentifier}',
                'authenticationProviderName' => '${entity.authenticationProviderName}',
                'name' => '${entity.party.name.fullName}',
              ),
            ),
          ),
        ),
        'transliterationRules' => 
        array (
          'da' => 
          array (
            'Å' => 'Aa',
            'Ø' => 'Oe',
            'å' => 'aa',
            'ø' => 'oe',
          ),
          'de' => 
          array (
            'Ä' => 'Ae',
            'Ö' => 'Oe',
            'Ü' => 'Ue',
            'ä' => 'ae',
            'ö' => 'oe',
            'ü' => 'ue',
          ),
        ),
        'NodeTypes' => 
        array (
        ),
        'Seo' => 
        array (
          'twitterCard' => 
          array (
            'siteHandle' => NULL,
          ),
        ),
        'Kickstarter' => 
        array (
        ),
      ),
      'Kickstart' => 
      array (
      ),
    ),
    'Neos' => 
    array (
      'Diff' => 
      array (
      ),
      'Utility' => 
      array (
        'Files' => 
        array (
        ),
        'ObjectHandling' => 
        array (
        ),
        'Arrays' => 
        array (
        ),
        'MediaTypes' => 
        array (
        ),
        'OpcodeCache' => 
        array (
        ),
        'Pdo' => 
        array (
        ),
        'Schema' => 
        array (
        ),
      ),
      'Outfit' => 
      array (
      ),
      'RedirectHandler' => 
      array (
        'features' => 
        array (
          'hitCounter' => true,
        ),
        'statusCode' => 
        array (
          'redirect' => 307,
          'gone' => 410,
        ),
        'DatabaseStorage' => 
        array (
        ),
        'NeosAdapter' => 
        array (
        ),
      ),
    ),
    'paragonie' => 
    array (
      'randomcompat' => 
      array (
      ),
    ),
    'ramsey' => 
    array (
      'uuid' => 
      array (
      ),
    ),
    'Doctrine' => 
    array (
      'Common' => 
      array (
        'Collections' => 
        array (
        ),
        'Inflector' => 
        array (
        ),
        'Lexer' => 
        array (
        ),
      ),
      'DBAL' => 
      array (
      ),
      'ORM' => 
      array (
      ),
    ),
    'doctrine' => 
    array (
      'cache' => 
      array (
      ),
      'annotations' => 
      array (
      ),
      'common' => 
      array (
      ),
      'instantiator' => 
      array (
      ),
      'migrations' => 
      array (
      ),
    ),
    'symfony' => 
    array (
      'polyfillmbstring' => 
      array (
      ),
      'debug' => 
      array (
      ),
      'console' => 
      array (
      ),
      'yaml' => 
      array (
      ),
      'domcrawler' => 
      array (
      ),
      'cssselector' => 
      array (
      ),
    ),
    'psr' => 
    array (
      'log' => 
      array (
      ),
    ),
    'zendframework' => 
    array (
      'zendeventmanager' => 
      array (
      ),
      'zendcode' => 
      array (
      ),
    ),
    'ocramius' => 
    array (
      'proxymanager' => 
      array (
      ),
    ),
    'neos' => 
    array (
      'composerplugin' => 
      array (
      ),
    ),
    'Behat' => 
    array (
      'Transliterator' => 
      array (
      ),
    ),
    'gedmo' => 
    array (
      'doctrineextensions' => 
      array (
      ),
    ),
    'league' => 
    array (
      'commonmark' => 
      array (
      ),
    ),
    'Flowpack' => 
    array (
      'Neos' => 
      array (
        'FrontendLogin' => 
        array (
        ),
      ),
      'Behat' => 
      array (
      ),
    ),
    'webmozart' => 
    array (
      'assert' => 
      array (
      ),
    ),
    'sebastian' => 
    array (
      'codeunitreverselookup' => 
      array (
      ),
      'diff' => 
      array (
      ),
      'recursioncontext' => 
      array (
      ),
      'exporter' => 
      array (
      ),
      'comparator' => 
      array (
      ),
      'environment' => 
      array (
      ),
      'globalstate' => 
      array (
      ),
      'objectenumerator' => 
      array (
      ),
      'resourceoperations' => 
      array (
      ),
      'version' => 
      array (
      ),
    ),
    'phpunit' => 
    array (
      'phpfileiterator' => 
      array (
      ),
      'phptokenstream' => 
      array (
      ),
      'phptexttemplate' => 
      array (
      ),
      'phpcodecoverage' => 
      array (
      ),
      'phptimer' => 
      array (
      ),
      'phpunitmockobjects' => 
      array (
      ),
      'phpunit' => 
      array (
      ),
    ),
    'phpdocumentor' => 
    array (
      'reflectioncommon' => 
      array (
      ),
      'typeresolver' => 
      array (
      ),
      'reflectiondocblock' => 
      array (
      ),
    ),
    'phpspec' => 
    array (
      'prophecy' => 
      array (
      ),
    ),
    'myclabs' => 
    array (
      'deepcopy' => 
      array (
      ),
    ),
    'org' => 
    array (
      'bovigo' => 
      array (
        'vfs' => 
        array (
        ),
      ),
    ),
    'imagine' => 
    array (
      'imagine' => 
      array (
      ),
    ),
  ),
  'Caches' => 
  array (
    'Fluid_TemplateCache' => 
    array (
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\PhpFrontend',
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\FileBackend',
    ),
    'Eel_Expression_Code' => 
    array (
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\PhpFrontend',
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\SimpleFileBackend',
    ),
    'Default' => 
    array (
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\VariableFrontend',
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\FileBackend',
      'backendOptions' => 
      array (
        'defaultLifetime' => 0,
      ),
      'persistent' => false,
    ),
    'Flow_Cache_ResourceFiles' => 
    array (
    ),
    'Flow_Core' => 
    array (
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\StringFrontend',
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\SimpleFileBackend',
    ),
    'Flow_I18n_AvailableLocalesCache' => 
    array (
    ),
    'Flow_I18n_XmlModelCache' => 
    array (
    ),
    'Flow_I18n_Cldr_CldrModelCache' => 
    array (
    ),
    'Flow_I18n_Cldr_Reader_DatesReaderCache' => 
    array (
    ),
    'Flow_I18n_Cldr_Reader_NumbersReaderCache' => 
    array (
    ),
    'Flow_I18n_Cldr_Reader_PluralsReaderCache' => 
    array (
    ),
    'Flow_Monitor' => 
    array (
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\SimpleFileBackend',
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\StringFrontend',
    ),
    'Flow_Mvc_Routing_Route' => 
    array (
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\FileBackend',
    ),
    'Flow_Mvc_Routing_Resolve' => 
    array (
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\StringFrontend',
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\FileBackend',
    ),
    'Flow_Mvc_ViewConfigurations' => 
    array (
    ),
    'Flow_Object_Classes' => 
    array (
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\PhpFrontend',
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\SimpleFileBackend',
    ),
    'Flow_Object_Configuration' => 
    array (
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\SimpleFileBackend',
    ),
    'Flow_Persistence_Doctrine' => 
    array (
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\SimpleFileBackend',
    ),
    'Flow_Persistence_Doctrine_Results' => 
    array (
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\FileBackend',
      'backendOptions' => 
      array (
        'defaultLifetime' => 60,
      ),
    ),
    'Flow_Persistence_Doctrine_SecondLevel' => 
    array (
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\SimpleFileBackend',
    ),
    'Flow_Reflection_Status' => 
    array (
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\StringFrontend',
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\SimpleFileBackend',
    ),
    'Flow_Reflection_CompiletimeData' => 
    array (
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\SimpleFileBackend',
    ),
    'Flow_Reflection_RuntimeData' => 
    array (
    ),
    'Flow_Reflection_RuntimeClassSchemata' => 
    array (
    ),
    'Flow_Resource_Status' => 
    array (
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\StringFrontend',
    ),
    'Flow_Security_Authorization_Privilege_Method' => 
    array (
    ),
    'Flow_Security_Cryptography_RSAWallet' => 
    array (
      'backendOptions' => 
      array (
        'defaultLifetime' => 30,
      ),
    ),
    'Flow_Security_Cryptography_HashService' => 
    array (
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\StringFrontend',
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\SimpleFileBackend',
      'persistent' => true,
    ),
    'Flow_Session_MetaData' => 
    array (
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\FileBackend',
    ),
    'Flow_Session_Storage' => 
    array (
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\FileBackend',
    ),
    'Flow_Aop_RuntimeExpressions' => 
    array (
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\PhpFrontend',
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\SimpleFileBackend',
    ),
    'Flow_PropertyMapper' => 
    array (
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\VariableFrontend',
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\NullBackend',
    ),
    'TYPO3_TypoScript_Content' => 
    array (
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\StringFrontend',
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\FileBackend',
    ),
    'TYPO3_Media_ImageSize' => 
    array (
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\VariableFrontend',
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\SimpleFileBackend',
      'backendOptions' => 
      array (
        'defaultLifetime' => 0,
      ),
    ),
    'TYPO3_TYPO3CR_FullNodeTypeConfiguration' => 
    array (
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\VariableFrontend',
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\NullBackend',
    ),
    'TYPO3_Neos_Configuration_Version' => 
    array (
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\StringFrontend',
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\FileBackend',
    ),
    'TYPO3_Neos_TypoScript' => 
    array (
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\VariableFrontend',
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\FileBackend',
    ),
    'TYPO3_Neos_XliffToJsonTranslations' => 
    array (
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\VariableFrontend',
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\FileBackend',
    ),
    'TYPO3_Neos_LoginTokenCache' => 
    array (
      'frontend' => 'TYPO3\\Flow\\Cache\\Frontend\\StringFrontend',
      'backend' => 'TYPO3\\Flow\\Cache\\Backend\\SimpleFileBackend',
      'backendOptions' => 
      array (
        'defaultLifetime' => 30,
      ),
    ),
  ),
  'Routes' => 
  array (
    0 => 
    array (
      'name' => 'TYPO3 Neos :: Authentication :: Login form',
      'uriPattern' => 'neos/login(.{@format})',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@controller' => 'Login',
        '@action' => 'index',
        '@format' => 'html',
      ),
      'httpMethods' => 
      array (
        0 => 'GET',
      ),
      'appendExceedingArguments' => true,
    ),
    1 => 
    array (
      'name' => 'TYPO3 Neos :: Authentication :: Token login',
      'uriPattern' => 'neos/login/token/{token}',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@controller' => 'Login',
        '@action' => 'tokenLogin',
        '@format' => 'html',
      ),
      'httpMethods' => 
      array (
        0 => 'GET',
      ),
    ),
    2 => 
    array (
      'name' => 'TYPO3 Neos :: Authentication :: Authenticate',
      'uriPattern' => 'neos/login(.{@format})',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@controller' => 'Login',
        '@action' => 'authenticate',
        '@format' => 'html',
      ),
      'httpMethods' => 
      array (
        0 => 'POST',
      ),
    ),
    3 => 
    array (
      'name' => 'TYPO3 Neos :: Authentication :: Logout',
      'uriPattern' => 'neos/logout(.{@format})',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@controller' => 'Login',
        '@action' => 'logout',
        '@format' => 'html',
      ),
      'httpMethods' => 
      array (
        0 => 'POST',
      ),
    ),
    4 => 
    array (
      'name' => 'TYPO3 Neos :: Backend :: Overview',
      'uriPattern' => 'neos',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'index',
        '@format' => 'html',
        '@controller' => 'Backend\\Backend',
      ),
    ),
    5 => 
    array (
      'name' => 'TYPO3 Neos :: Backend :: Content Module - Media Browser',
      'uriPattern' => 'neos/content/assets(/{@action}).{@format}',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'index',
        '@format' => 'html',
        '@controller' => 'Backend\\MediaBrowser',
      ),
      'appendExceedingArguments' => true,
    ),
    6 => 
    array (
      'name' => 'TYPO3 Neos :: Backend :: Content Module - Image Browser',
      'uriPattern' => 'neos/content/images(/{@action}).{@format}',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'index',
        '@format' => 'html',
        '@controller' => 'Backend\\ImageBrowser',
      ),
      'appendExceedingArguments' => true,
    ),
    7 => 
    array (
      'name' => 'TYPO3 Neos :: Backend :: Content Module - Asset upload',
      'uriPattern' => 'neos/content/upload-asset',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'uploadAsset',
        '@format' => 'html',
        '@controller' => 'Backend\\Content',
      ),
      'appendExceedingArguments' => true,
      'httpMethods' => 
      array (
        0 => 'POST',
      ),
    ),
    8 => 
    array (
      'name' => 'TYPO3 Neos :: Backend :: Content Module - Image metadata',
      'uriPattern' => 'neos/content/image-with-metadata',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'imageWithMetadata',
        '@format' => 'html',
        '@controller' => 'Backend\\Content',
      ),
      'appendExceedingArguments' => true,
    ),
    9 => 
    array (
      'name' => 'TYPO3 Neos :: Backend :: Content Module - Asset metadata',
      'uriPattern' => 'neos/content/asset-with-metadata',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'assetsWithMetadata',
        '@format' => 'html',
        '@controller' => 'Backend\\Content',
      ),
    ),
    10 => 
    array (
      'name' => 'TYPO3 Neos :: Backend :: Content Module - Create ImageVariant',
      'uriPattern' => 'neos/content/create-image-variant',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'createImageVariant',
        '@format' => 'html',
        '@controller' => 'Backend\\Content',
      ),
      'httpMethods' => 
      array (
        0 => 'POST',
      ),
    ),
    11 => 
    array (
      'name' => 'TYPO3 Neos :: Backend :: Content Module - Plugin Views',
      'uriPattern' => 'neos/content/plugin-views',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'pluginViews',
        '@format' => 'html',
        '@controller' => 'Backend\\Content',
      ),
    ),
    12 => 
    array (
      'name' => 'TYPO3 Neos :: Backend :: Content Module - Master Plugins',
      'uriPattern' => 'neos/content/master-plugins',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'masterPlugins',
        '@format' => 'html',
        '@controller' => 'Backend\\Content',
      ),
    ),
    13 => 
    array (
      'name' => 'TYPO3 Neos :: Backend :: Schema - VIE',
      'uriPattern' => 'neos/schema/vie',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'vieSchema',
        '@format' => 'html',
        '@controller' => 'Backend\\Schema',
      ),
      'appendExceedingArguments' => true,
    ),
    14 => 
    array (
      'name' => 'TYPO3 Neos :: Backend :: Schema - NodeType',
      'uriPattern' => 'neos/schema/node-type',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'nodeTypeSchema',
        '@format' => 'html',
        '@controller' => 'Backend\\Schema',
      ),
      'appendExceedingArguments' => true,
    ),
    15 => 
    array (
      'name' => 'TYPO3 Neos :: Backend :: Menu',
      'uriPattern' => 'neos/menu',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'index',
        '@format' => 'html',
        '@controller' => 'Backend\\Menu',
      ),
      'appendExceedingArguments' => true,
    ),
    16 => 
    array (
      'name' => 'TYPO3 Neos :: Backend :: Settings',
      'uriPattern' => 'neos/settings/{@action}',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'index',
        '@format' => 'html',
        '@controller' => 'Backend\\Settings',
      ),
      'appendExceedingArguments' => true,
    ),
    17 => 
    array (
      'name' => 'TYPO3 Neos :: Backend :: Modules',
      'uriPattern' => 'neos/{module}',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'index',
        '@format' => 'html',
        '@controller' => 'Backend\\Module',
      ),
      'routeParts' => 
      array (
        'module' => 
        array (
          'handler' => 'TYPO3\\Neos\\Routing\\BackendModuleRoutePartHandler',
        ),
      ),
    ),
    18 => 
    array (
      'name' => 'TYPO3 Neos :: Backend :: Modules & arguments',
      'uriPattern' => 'neos/{module}/{moduleArguments}',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'index',
        '@format' => 'html',
        '@controller' => 'Backend\\Module',
      ),
      'routeParts' => 
      array (
        'module' => 
        array (
          'handler' => 'TYPO3\\Neos\\Routing\\BackendModuleRoutePartHandler',
        ),
        'moduleArguments' => 
        array (
          'handler' => 'TYPO3\\Neos\\Routing\\BackendModuleArgumentsRoutePartHandler',
        ),
      ),
      'toLowerCase' => false,
      'appendExceedingArguments' => true,
    ),
    19 => 
    array (
      'name' => 'TYPO3 Neos :: Backend :: Backend switch site',
      'uriPattern' => 'neos/switch/to/{site}',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'switchSite',
        '@format' => 'html',
        '@controller' => 'Backend\\Backend',
      ),
      'routeParts' => 
      array (
        'site' => 
        array (
          'objectType' => 'TYPO3\\Neos\\Domain\\Model\\Site',
          'uriPattern' => '{name}',
        ),
      ),
    ),
    20 => 
    array (
      'name' => 'TYPO3 Neos :: Backend :: Backend UI XLIFF labels',
      'uriPattern' => 'neos/xliff.json',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'xliffAsJson',
        '@format' => 'html',
        '@controller' => 'Backend\\Backend',
      ),
      'appendExceedingArguments' => true,
    ),
    21 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Nodes - index',
      'uriPattern' => 'neos/service/nodes',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'index',
        '@controller' => 'Service\\Nodes',
      ),
      'appendExceedingArguments' => true,
      'httpMethods' => 
      array (
        0 => 'GET',
      ),
    ),
    22 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Nodes - single node',
      'uriPattern' => 'neos/service/nodes/{identifier}',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'show',
        '@controller' => 'Service\\Nodes',
      ),
      'appendExceedingArguments' => true,
      'httpMethods' => 
      array (
        0 => 'HEAD',
        1 => 'GET',
      ),
    ),
    23 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Nodes - create/adopt node',
      'uriPattern' => 'neos/service/nodes',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'create',
        '@controller' => 'Service\\Nodes',
      ),
      'appendExceedingArguments' => true,
      'httpMethods' => 
      array (
        0 => 'POST',
      ),
    ),
    24 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - UserPreferencesController->index',
      'uriPattern' => 'neos/service/user-preferences',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'index',
        '@subpackage' => 'Service',
        '@controller' => 'UserPreference',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'GET',
      ),
    ),
    25 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - UserPreferencesController->update',
      'uriPattern' => 'neos/service/user-preferences',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'update',
        '@subpackage' => 'Service',
        '@controller' => 'UserPreference',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'PUT',
      ),
    ),
    26 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Assets - index',
      'uriPattern' => 'neos/service/assets',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'index',
        '@controller' => 'Service\\Assets',
      ),
      'appendExceedingArguments' => true,
      'httpMethods' => 
      array (
        0 => 'GET',
      ),
    ),
    27 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Assets - single asset',
      'uriPattern' => 'neos/service/assets/{identifier}',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'show',
        '@controller' => 'Service\\Assets',
      ),
      'appendExceedingArguments' => true,
      'httpMethods' => 
      array (
        0 => 'HEAD',
        1 => 'GET',
      ),
    ),
    28 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - WorkspacesController->indexAction()',
      'uriPattern' => 'neos/service/workspaces',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'index',
        '@controller' => 'Service\\Workspaces',
      ),
      'appendExceedingArguments' => true,
      'httpMethods' => 
      array (
        0 => 'GET',
      ),
    ),
    29 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - WorkspacesController->showAction()',
      'uriPattern' => 'neos/service/workspaces/{workspace}',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'show',
        '@controller' => 'Service\\Workspaces',
      ),
      'httpMethods' => 
      array (
        0 => 'GET',
      ),
    ),
    30 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - WorkspacesController->createAction()',
      'uriPattern' => 'neos/service/workspaces/{baseWorkspace}/{workspaceName}',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'create',
        '@controller' => 'Service\\Workspaces',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'POST',
      ),
    ),
    31 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - WorkspacesController->updateAction()',
      'uriPattern' => 'neos/service/workspaces/{workspace.__identity}',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'update',
        '@controller' => 'Service\\Workspaces',
        '@format' => 'json',
      ),
      'appendExceedingArguments' => true,
      'httpMethods' => 
      array (
        0 => 'PUT',
      ),
    ),
    32 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - WorkspaceController->getWorkspaceWideUnpublishedNodes',
      'uriPattern' => 'neos/service/workspaces-rpc/get-workspace-wide-unpublished-nodes',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'getWorkspaceWideUnpublishedNodes',
        '@subpackage' => 'Service',
        '@controller' => 'Workspace',
        '@format' => 'json',
      ),
      'appendExceedingArguments' => true,
      'httpMethods' => 
      array (
        0 => 'GET',
      ),
    ),
    33 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - WorkspaceController->publishNode',
      'uriPattern' => 'neos/service/workspaces-rpc/publish-node',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'publishNode',
        '@subpackage' => 'Service',
        '@controller' => 'Workspace',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'PUT',
      ),
    ),
    34 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - WorkspaceController->publishNodes',
      'uriPattern' => 'neos/service/workspaces-rpc/publish-nodes',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'publishNodes',
        '@subpackage' => 'Service',
        '@controller' => 'Workspace',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'PUT',
      ),
    ),
    35 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - WorkspaceController->discardNode',
      'uriPattern' => 'neos/service/workspaces-rpc/discard-node',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'discardNode',
        '@subpackage' => 'Service',
        '@controller' => 'Workspace',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'PUT',
      ),
    ),
    36 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - WorkspaceController->discardNodes',
      'uriPattern' => 'neos/service/workspaces-rpc/discard-nodes',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'discardNodes',
        '@subpackage' => 'Service',
        '@controller' => 'Workspace',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'PUT',
      ),
    ),
    37 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - WorkspaceController->publishAll',
      'uriPattern' => 'neos/service/workspaces-rpc/publish-all',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'publishAll',
        '@subpackage' => 'Service',
        '@controller' => 'Workspace',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'PUT',
      ),
    ),
    38 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - WorkspaceController->discardAll',
      'uriPattern' => 'neos/service/workspaces-rpc/discard-all',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'discardAll',
        '@subpackage' => 'Service',
        '@controller' => 'Workspace',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'PUT',
      ),
    ),
    39 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - NodeController->getChildNodesForTree',
      'uriPattern' => 'neos/service/node/get-child-nodes-for-tree',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'getChildNodesForTree',
        '@subpackage' => 'Service',
        '@controller' => 'Node',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'GET',
      ),
    ),
    40 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - NodeController->filterChildNodesForTree',
      'uriPattern' => 'neos/service/node/filter-child-nodes-for-tree',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'filterChildNodesForTree',
        '@subpackage' => 'Service',
        '@controller' => 'Node',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'GET',
      ),
    ),
    41 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - NodeController->searchPage',
      'uriPattern' => 'neos/service/node/search-page',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'searchPage',
        '@subpackage' => 'Service',
        '@controller' => 'Node',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'GET',
      ),
    ),
    42 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - NodeController->create',
      'uriPattern' => 'neos/service/node/create',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'create',
        '@subpackage' => 'Service',
        '@controller' => 'Node',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'POST',
      ),
    ),
    43 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - NodeController->createAndRender',
      'uriPattern' => 'neos/service/node/create-and-render',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'createAndRender',
        '@subpackage' => 'Service',
        '@controller' => 'Node',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'POST',
      ),
    ),
    44 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - NodeController->createNodeForTheTree',
      'uriPattern' => 'neos/service/node/create-node-for-the-tree',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'createNodeForTheTree',
        '@subpackage' => 'Service',
        '@controller' => 'Node',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'POST',
      ),
    ),
    45 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - NodeController->discardNode',
      'uriPattern' => 'neos/service/node/discard-node',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'discardNode',
        '@subpackage' => 'Service',
        '@controller' => 'Node',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'PUT',
      ),
    ),
    46 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - NodeController->move',
      'uriPattern' => 'neos/service/node/move',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'move',
        '@subpackage' => 'Service',
        '@controller' => 'Node',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'PUT',
      ),
    ),
    47 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - NodeController->moveAndRender',
      'uriPattern' => 'neos/service/node/move-and-render',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'moveAndRender',
        '@subpackage' => 'Service',
        '@controller' => 'Node',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'PUT',
      ),
    ),
    48 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - NodeController->copy',
      'uriPattern' => 'neos/service/node/copy',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'copy',
        '@subpackage' => 'Service',
        '@controller' => 'Node',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'PUT',
      ),
    ),
    49 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - NodeController->copyAndRender',
      'uriPattern' => 'neos/service/node/copy-and-render',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'copyAndRender',
        '@subpackage' => 'Service',
        '@controller' => 'Node',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'PUT',
      ),
    ),
    50 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - NodeController->update',
      'uriPattern' => 'neos/service/node/update',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'update',
        '@subpackage' => 'Service',
        '@controller' => 'Node',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'PUT',
      ),
    ),
    51 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - NodeController->updateAndRender',
      'uriPattern' => 'neos/service/node/update-and-render',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'updateAndRender',
        '@subpackage' => 'Service',
        '@controller' => 'Node',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'PUT',
      ),
    ),
    52 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - NodeController->delete',
      'uriPattern' => 'neos/service/node/delete',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'delete',
        '@subpackage' => 'Service',
        '@controller' => 'Node',
        '@format' => 'json',
      ),
      'httpMethods' => 
      array (
        0 => 'POST',
      ),
    ),
    53 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - DataSourceController->index',
      'uriPattern' => 'neos/service/data-source(/{dataSourceIdentifier)',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'index',
        '@subpackage' => 'Service',
        '@controller' => 'DataSource',
        '@format' => 'json',
        'dataSourceIdentifier' => '',
      ),
      'appendExceedingArguments' => true,
      'httpMethods' => 
      array (
        0 => 'GET',
      ),
    ),
    54 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - ContentDimensionController->index()',
      'uriPattern' => 'neos/service/content-dimensions',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'index',
        '@controller' => 'Service\\ContentDimensions',
      ),
      'appendExceedingArguments' => true,
      'httpMethods' => 
      array (
        0 => 'GET',
      ),
    ),
    55 => 
    array (
      'name' => 'TYPO3 Neos :: Service :: Services - ContentDimensionController->show()',
      'uriPattern' => 'neos/service/content-dimensions/{dimensionName}',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@action' => 'show',
        '@controller' => 'Service\\ContentDimensions',
      ),
      'appendExceedingArguments' => true,
      'httpMethods' => 
      array (
        0 => 'GET',
      ),
    ),
    56 => 
    array (
      'name' => 'TYPO3 Neos :: Frontend :: Homepage',
      'uriPattern' => '{node}',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@controller' => 'Frontend\\Node',
        '@action' => 'show',
        '@format' => 'html',
      ),
      'routeParts' => 
      array (
        'node' => 
        array (
          'handler' => 'TYPO3\\Neos\\Routing\\FrontendNodeRoutePartHandlerInterface',
          'options' => 
          array (
            'onlyMatchSiteNodes' => true,
          ),
        ),
      ),
      'appendExceedingArguments' => true,
    ),
    57 => 
    array (
      'name' => 'TYPO3 Neos :: Frontend :: content with URI suffix',
      'uriPattern' => '{node}.html',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@controller' => 'Frontend\\Node',
        '@action' => 'show',
        '@format' => 'html',
      ),
      'routeParts' => 
      array (
        'node' => 
        array (
          'handler' => 'TYPO3\\Neos\\Routing\\FrontendNodeRoutePartHandlerInterface',
        ),
      ),
      'appendExceedingArguments' => true,
    ),
    58 => 
    array (
      'name' => 'TYPO3 Neos :: Frontend :: Dummy wireframe route to enable uri resolution while in wireframe mode.',
      'uriPattern' => '{node}.html',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Neos',
        '@controller' => 'Frontend\\Node',
        '@action' => 'showWireframe',
        '@format' => 'html',
      ),
      'routeParts' => 
      array (
        'node' => 
        array (
          'handler' => 'TYPO3\\Neos\\Routing\\FrontendNodeRoutePartHandlerInterface',
        ),
      ),
      'appendExceedingArguments' => true,
    ),
    59 => 
    array (
      'name' => 'TYPO3.Media :: Thumbnail',
      'uriPattern' => 'media/thumbnail/{thumbnail}',
      'defaults' => 
      array (
        '@package' => 'TYPO3.Media',
        '@controller' => 'Thumbnail',
        '@action' => 'thumbnail',
      ),
      'httpMethods' => 
      array (
        0 => 'GET',
      ),
    ),
  ),
  'Views' => 
  array (
    0 => 
    array (
      'requestFilter' => 'parentRequest.isPackage("TYPO3.Neos") && isFormat("html") && (isPackage("TYPO3.Neos") && isController("Module\\Management\\Asset")) || (isPackage("TYPO3.Media") && isController("Asset"))',
      'options' => 
      array (
        'layoutRootPaths' => 
        array (
          'TYPO3.Neos' => 'resource://TYPO3.Neos/Private/Layouts/Media',
          'TYPO3.Media' => 'resource://TYPO3.Media/Private/Layouts',
        ),
        'partialRootPaths' => 
        array (
          'TYPO3.Neos' => 'resource://TYPO3.Neos/Private/Partials/Media',
          'TYPO3.Media' => 'resource://TYPO3.Media/Private/Partials',
        ),
        'templateRootPaths' => 
        array (
          'TYPO3.Neos' => 'resource://TYPO3.Neos/Private/Templates/Media',
          'TYPO3.Media' => 'resource://TYPO3.Media/Private/Templates',
        ),
        'templatePathAndFilenamePattern' => '@templateRoot/Asset/@action.@format',
      ),
    ),
    1 => 
    array (
      'requestFilter' => 'isPackage("TYPO3.Neos") && isFormat("html") && (isController("Backend\\MediaBrowser") || isController("Backend\\ImageBrowser"))',
      'options' => 
      array (
        'layoutRootPaths' => 
        array (
          'TYPO3.Media' => 'resource://TYPO3.Media/Private/Layouts',
          'TYPO3.Neos' => 'resource://TYPO3.Neos/Private/Layouts/Media',
        ),
        'partialRootPaths' => 
        array (
          'TYPO3.Neos' => 'resource://TYPO3.Neos/Private/Partials/Media',
          'TYPO3.Media' => 'resource://TYPO3.Media/Private/Partials',
        ),
        'templateRootPathPattern' => 'resource://TYPO3.Neos/Private/Templates/Media',
        'templatePathAndFilenamePattern' => '@templateRoot/@subpackage/Asset/@action.@format',
      ),
    ),
  ),
  'Policy' => 
  array (
    'roles' => 
    array (
      'TYPO3.Flow:Everybody' => 
      array (
        'abstract' => true,
        'privileges' => 
        array (
          0 => 
          array (
            'privilegeTarget' => 'TYPO3.Media:Thumbnail',
            'permission' => 'GRANT',
          ),
          1 => 
          array (
            'privilegeTarget' => 'TYPO3.Setup:LoginController',
            'permission' => 'GRANT',
          ),
          2 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:PublicFrontendAccess',
            'permission' => 'GRANT',
          ),
          3 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:BackendLogin',
            'permission' => 'GRANT',
          ),
          4 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:WidgetControllers',
            'permission' => 'GRANT',
          ),
          5 => 
          array (
            'privilegeTarget' => 'Flowpack.Neos.FrontendLogin:LoginForm',
            'permission' => 'GRANT',
          ),
        ),
      ),
      'TYPO3.Flow:Anonymous' => 
      array (
        'abstract' => true,
      ),
      'TYPO3.Flow:AuthenticatedUser' => 
      array (
        'abstract' => true,
      ),
      'TYPO3.Setup:SetupUser' => 
      array (
        'privileges' => 
        array (
          0 => 
          array (
            'privilegeTarget' => 'TYPO3.Setup:SetupController',
            'permission' => 'GRANT',
          ),
          1 => 
          array (
            'privilegeTarget' => 'TYPO3.Setup:WidgetControllers',
            'permission' => 'GRANT',
          ),
        ),
      ),
      'TYPO3.TYPO3CR:Administrator' => 
      array (
        'abstract' => true,
      ),
      'TYPO3.Neos:LivePublisher' => 
      array (
        'privileges' => 
        array (
          0 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.PublishToLiveWorkspace',
            'permission' => 'GRANT',
          ),
          1 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.PublishAllToLiveWorkspace',
            'permission' => 'GRANT',
          ),
          2 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Media.ReplaceAssetResource',
            'permission' => 'GRANT',
          ),
        ),
      ),
      'TYPO3.Neos:AbstractEditor' => 
      array (
        'abstract' => true,
        'parentRoles' => 
        array (
          0 => 'TYPO3.TYPO3CR:Administrator',
        ),
        'privileges' => 
        array (
          0 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.GeneralAccess',
            'permission' => 'GRANT',
          ),
          1 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Content',
            'permission' => 'GRANT',
          ),
          2 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.PersonalWorkspaceReadAccess.NodeConverter',
            'permission' => 'GRANT',
          ),
          3 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.EditContent',
            'permission' => 'GRANT',
          ),
          4 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.PublishOwnWorkspaceContent',
            'permission' => 'GRANT',
          ),
          5 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.DiscardOwnWorkspaceContent',
            'permission' => 'GRANT',
          ),
          6 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Service.Workspaces.Index',
            'permission' => 'GRANT',
          ),
          7 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Service.Workspaces.ManageOwnWorkspaces',
            'permission' => 'GRANT',
          ),
          8 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Management.Workspaces.ManageOwnWorkspaces',
            'permission' => 'GRANT',
          ),
          9 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.CreateWorkspaces',
            'permission' => 'GRANT',
          ),
          10 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.ContentDimensions',
            'permission' => 'GRANT',
          ),
          11 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.DataSource',
            'permission' => 'GRANT',
          ),
          12 => 
          array (
            'privilegeTarget' => 'TYPO3.Media:ManageAssets',
            'permission' => 'GRANT',
          ),
          13 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Media.ManageAssets',
            'permission' => 'GRANT',
          ),
          14 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.User',
            'permission' => 'GRANT',
          ),
          15 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.User.UserSettings',
            'permission' => 'GRANT',
          ),
          16 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.User.UserSettings.UpdateOwnSettings',
            'permission' => 'GRANT',
          ),
          17 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.EditUserPreferences',
            'permission' => 'GRANT',
          ),
          18 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Management',
            'permission' => 'GRANT',
          ),
          19 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Management.Workspaces',
            'permission' => 'GRANT',
          ),
          20 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Management.History',
            'permission' => 'GRANT',
          ),
        ),
      ),
      'TYPO3.Neos:RestrictedEditor' => 
      array (
        'parentRoles' => 
        array (
          0 => 'TYPO3.Neos:AbstractEditor',
        ),
      ),
      'TYPO3.Neos:Editor' => 
      array (
        'parentRoles' => 
        array (
          0 => 'TYPO3.Neos:AbstractEditor',
          1 => 'TYPO3.Neos:LivePublisher',
        ),
      ),
      'TYPO3.Neos:Administrator' => 
      array (
        'parentRoles' => 
        array (
          0 => 'TYPO3.Neos:Editor',
        ),
        'privileges' => 
        array (
          0 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Administration',
            'permission' => 'GRANT',
          ),
          1 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Administration.Users',
            'permission' => 'GRANT',
          ),
          2 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Administration.Packages',
            'permission' => 'GRANT',
          ),
          3 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Management.Workspaces.ManageInternalWorkspaces',
            'permission' => 'GRANT',
          ),
          4 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Management.Workspaces.ManageAllPrivateWorkspaces',
            'permission' => 'GRANT',
          ),
          5 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Administration.Sites',
            'permission' => 'GRANT',
          ),
          6 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Administration.Configuration',
            'permission' => 'GRANT',
          ),
          7 => 
          array (
            'privilegeTarget' => 'TYPO3.Media:ManageAssetCollections',
            'permission' => 'GRANT',
          ),
          8 => 
          array (
            'privilegeTarget' => 'TYPO3.Neos:Backend.Module.Media.ManageAssetCollections',
            'permission' => 'GRANT',
          ),
        ),
      ),
    ),
    'privilegeTargets' => 
    array (
      'TYPO3\\Flow\\Security\\Authorization\\Privilege\\Method\\MethodPrivilege' => 
      array (
        'TYPO3.Media:ManageAssets' => 
        array (
          'matcher' => 'method(TYPO3\\Media\\Controller\\AssetController->(index|new|edit|update|initializeCreate|create|replaceAssetResource|updateAssetResource|initializeUpload|upload|tagAsset|delete|createTag|editTag|updateTag|deleteTag|addAssetToCollection)Action())',
        ),
        'TYPO3.Media:ManageAssetCollections' => 
        array (
          'matcher' => 'method(TYPO3\\Media\\Controller\\AssetController->(createAssetCollection|editAssetCollection|updateAssetCollection|deleteAssetCollection)Action())',
        ),
        'TYPO3.Media:Thumbnail' => 
        array (
          'matcher' => 'method(TYPO3\\Media\\Controller\\ThumbnailController->thumbnailAction())',
        ),
        'TYPO3.Setup:LoginController' => 
        array (
          'matcher' => 'method(TYPO3\\Setup\\Controller\\LoginController->(login|authenticate|generateNewPassword)Action())',
        ),
        'TYPO3.Setup:SetupController' => 
        array (
          'matcher' => 'method(TYPO3\\Setup\\Controller\\SetupController->indexAction()) || method(TYPO3\\Setup\\Controller\\LoginController->logoutAction())',
        ),
        'TYPO3.Setup:WidgetControllers' => 
        array (
          'matcher' => 'method(public TYPO3\\Setup\\ViewHelpers\\Widget\\Controller\\.+Controller->.+Action())',
        ),
        'TYPO3.Neos:AllControllerActions' => 
        array (
          'matcher' => 'within(TYPO3\\Flow\\Mvc\\Controller\\AbstractController) && method(public .*->(?!initialize).*Action())',
        ),
        'TYPO3.Neos:WidgetControllers' => 
        array (
          'matcher' => 'method(TYPO3\\Fluid\\ViewHelpers\\Widget\\Controller\\AutocompleteController->(index|autocomplete)Action()) || method(TYPO3\\Fluid\\ViewHelpers\\Widget\\Controller\\PaginateController->indexAction()) || method(TYPO3\\TYPO3CR\\ViewHelpers\\Widget\\Controller\\PaginateController->indexAction()) || method(TYPO3\\Neos\\ViewHelpers\\Widget\\Controller\\LinkRepositoryController->(index|search|lookup)Action())',
        ),
        'TYPO3.Neos:PublicFrontendAccess' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Frontend\\NodeController->showAction()) || method(TYPO3\\Neos\\TypeConverter\\NodeConverter->prepareContextProperties(workspaceName === "live"))',
        ),
        'TYPO3.Neos:BackendLogin' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\LoginController->(index|tokenLogin|authenticate)Action()) || method(TYPO3\\Flow\\Security\\Authentication\\Controller\\AbstractAuthenticationController->authenticateAction())',
        ),
        'TYPO3.Neos:Backend.GeneralAccess' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Backend\\BackendController->(index|switchSite|xliffAsJson)Action()) || method(TYPO3\\Neos\\Controller\\Backend\\ModuleController->indexAction()) || method(TYPO3\\Neos\\Controller\\LoginController->logoutAction()) || method(TYPO3\\Flow\\Security\\Authentication\\Controller\\AbstractAuthenticationController->logoutAction()) || method(TYPO3\\Neos\\Controller\\Module\\AbstractModuleController->indexAction())',
        ),
        'TYPO3.Neos:Backend.Module.Content' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Backend\\SchemaController->(nodeTypeSchema|vieSchema)Action()) || method(TYPO3\\Neos\\Controller\\Backend\\MenuController->indexAction()) || method(TYPO3\\Neos\\Controller\\Backend\\SettingsController->editPreviewAction())',
        ),
        'TYPO3.Neos:Backend.PersonalWorkspaceReadAccess.NodeConverter' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\TypeConverter\\NodeConverter->prepareContextProperties(workspaceName === current.userInformation.userWorkspaceName))',
        ),
        'TYPO3.Neos:Backend.OtherUsersPersonalWorkspaceAccess' => 
        array (
          'matcher' => 'method(TYPO3\\TYPO3CR\\Domain\\Service\\Context->validateWorkspace()) && evaluate(this.workspace.owner !== current.userInformation.backendUser, this.workspace.personalWorkspace === true)',
        ),
        'TYPO3.Neos:Backend.EditContent' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Service\\Controller\\NodeController->(show|getPrimaryChildNode|getChildNodesForTree|filterChildNodesForTree|getChildNodes|getChildNodesFromParent|create|createAndRender|createNodeForTheTree|move|moveBefore|moveAfter|moveInto|moveAndRender|copy|copyBefore|copyAfter|copyInto|copyAndRender|update|updateAndRender|delete|searchPage|error)Action()) || method(TYPO3\\Neos\\Controller\\Backend\\ContentController->(uploadAsset|assetsWithMetadata|imageWithMetadata|pluginViews|createImageVariant|masterPlugins|error)Action()) || method(TYPO3\\Neos\\Controller\\Service\\AssetsController->(index|show|error)Action()) || method(TYPO3\\Neos\\Controller\\Service\\NodesController->(index|show|create|error)Action()) || method(TYPO3\\Neos\\Service\\Controller\\AbstractServiceController->(error)Action())',
        ),
        'TYPO3.Neos:Backend.PublishToLiveWorkspace' => 
        array (
          'matcher' => 'method(TYPO3\\TYPO3CR\\Domain\\Model\\Workspace->(publish|publishNode|publishNodes)(targetWorkspace.name === "live"))',
        ),
        'TYPO3.Neos:Backend.PublishAllToLiveWorkspace' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Module\\Management\\WorkspacesController->publishWorkspaceAction(workspace.baseWorkspace.name === "live"))',
        ),
        'TYPO3.Neos:Backend.PublishOwnWorkspaceContent' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Service\\Controller\\WorkspaceController->(publishNode|publishNodes|error)Action()) || method(TYPO3\\Neos\\Service\\Controller\\WorkspaceController->publishAllAction(workspaceName = current.userInformation.userWorkspaceName)) || method(TYPO3\\Neos\\Service\\Controller\\WorkspaceController->getWorkspaceWideUnpublishedNodesAction(workspace.name = current.userInformation.userWorkspaceName)) || method(TYPO3\\Neos\\Service\\Controller\\AbstractServiceController->(error)Action())',
        ),
        'TYPO3.Neos:Backend.DiscardOwnWorkspaceContent' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Service\\Controller\\WorkspaceController->(discardNode|discardNodes|error)Action()) || method(TYPO3\\Neos\\Service\\Controller\\WorkspaceController->discardAllAction(workspace.name === current.userInformation.userWorkspaceName)) || method(TYPO3\\Neos\\Service\\Controller\\AbstractServiceController->(error)Action())',
        ),
        'TYPO3.Neos:Backend.CreateWorkspaces' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Service\\WorkspacesController->(new|create)Action()) || method(TYPO3\\Neos\\Controller\\Module\\Management\\WorkspacesController->(create|new)Action())',
        ),
        'TYPO3.Neos:Backend.Module.Management.Workspaces' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Module\\Management\\WorkspacesController->(index|show|publishNode|discardNode|publishOrDiscardNodes|publishWorkspace|discardWorkspace|rebaseAndRedirect)Action()) || method(TYPO3\\Neos\\Service\\Controller\\AbstractServiceController->(error)Action())',
        ),
        'TYPO3.Neos:Backend.Module.Management.Workspaces.ManageOwnWorkspaces' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Module\\Management\\WorkspacesController->(publishWorkspace|discardWorkspace|edit|update|delete)Action(workspace.owner === current.userInformation.backendUser))',
        ),
        'TYPO3.Neos:Backend.Module.Management.Workspaces.ManageInternalWorkspaces' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Module\\Management\\WorkspacesController->(publishWorkspace|discardWorkspace|edit|update|delete)Action(workspace.owner === null))',
        ),
        'TYPO3.Neos:Backend.Module.Management.Workspaces.ManageAllPrivateWorkspaces' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Module\\Management\\WorkspacesController->(publishWorkspace|discardWorkspace|edit|update|delete)Action()) && evaluate(this.workspace.owner !== current.userInformation.backendUser, this.workspace.personalWorkspace === false)',
        ),
        'TYPO3.Neos:Backend.Service.Workspaces.Index' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Service\\WorkspacesController->(index|error|show)Action())',
        ),
        'TYPO3.Neos:Backend.Service.Workspaces.ManageOwnWorkspaces' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Service\\WorkspacesController->(update|delete)Action(workspace.owner === current.userInformation.backendUser))',
        ),
        'TYPO3.Neos:Backend.Module.User' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Module\\UserController->indexAction())',
        ),
        'TYPO3.Neos:Backend.Module.User.UserSettings' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Module\\User\\UserSettingsController->(index|newElectronicAddress|createElectronicAddress|deleteElectronicAddress|edit|editAccount|updateAccount)Action())',
        ),
        'TYPO3.Neos:Backend.Module.User.UserSettings.UpdateOwnSettings' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Module\\User\\UserSettingsController->updateAccountAction(user === current.securityContext.account)) || method(TYPO3\\Neos\\Controller\\Module\\User\\UserSettingsController->updateAction(user === current.securityContext.party))',
        ),
        'TYPO3.Neos:Backend.EditUserPreferences' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Service\\Controller\\UserPreferenceController->(index|update|error)Action()) || method(TYPO3\\Neos\\Service\\Controller\\AbstractServiceController->(error)Action())',
        ),
        'TYPO3.Neos:Backend.Module.Administration.Users' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Module\\Administration\\UsersController->(index|show|new|create|edit|update|delete|newElectronicAddress|createElectronicAddress|deleteElectronicAddress|editAccount|updateAccount)Action())',
        ),
        'TYPO3.Neos:Backend.Module.Media.ManageAssets' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\(Module\\Management\\Asset|Backend\\MediaBrowser|Backend\\ImageBrowser)Controller->(index|new|edit|update|initializeCreate|create|initializeUpload|upload|tagAsset|delete|createTag|editTag|updateTag|deleteTag|addAssetToCollection|relatedNodes)Action())',
        ),
        'TYPO3.Neos:Backend.Module.Media.ReplaceAssetResource' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\(Module\\Management\\Asset|Backend\\MediaBrowser|Backend\\ImageBrowser)Controller->(replaceAssetResource|updateAssetResource)Action())',
        ),
        'TYPO3.Neos:Backend.Module.Media.ManageAssetCollections' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\(Module\\Management\\Asset|Backend\\MediaBrowser|Backend\\ImageBrowser)Controller->(createAssetCollection|editAssetCollection|updateAssetCollection|deleteAssetCollection)Action())',
        ),
        'TYPO3.Neos:Backend.ContentDimensions' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Service\\ContentDimensionsController->(index|show|error)Action())',
        ),
        'TYPO3.Neos:Backend.DataSource' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Service\\Controller\\DataSourceController->(index|error)Action()) || method(TYPO3\\Neos\\Service\\Controller\\AbstractServiceController->(error)Action())',
        ),
        'TYPO3.Neos:Backend.Module.Management' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Module\\ManagementController->indexAction())',
        ),
        'TYPO3.Neos:Backend.Module.Management.History' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Module\\Management\\HistoryController->(index)Action())',
        ),
        'TYPO3.Neos:Backend.Module.Administration' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Module\\AdministrationController->indexAction())',
        ),
        'TYPO3.Neos:Backend.Module.Administration.Packages' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Module\\Administration\\PackagesController->(index|activate|deactivate|delete|freeze|unfreeze|batch)Action())',
        ),
        'TYPO3.Neos:Backend.Module.Administration.Sites' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Module\\Administration\\SitesController->(index|edit|updateSite|newSite|createSite|deleteSite|activateSite|deactivateSite|editDomain|updateDomain|newDomain|createDomain|deleteDomain|activateDomain|deactivateDomain)Action())',
        ),
        'TYPO3.Neos:Backend.Module.Administration.Configuration' => 
        array (
          'matcher' => 'method(TYPO3\\Neos\\Controller\\Module\\Administration\\ConfigurationController->indexAction())',
        ),
        'Flowpack.Neos.FrontendLogin:LoginForm' => 
        array (
          'matcher' => 'method(TYPO3\\Flow\\Security\\Authentication\\Controller\\AbstractAuthenticationController->(?!initialize).*Action()) || method(Flowpack\\Neos\\FrontendLogin\\Controller\\AuthenticationController->(?!initialize).*Action())',
        ),
      ),
    ),
  ),
  'Objects' => 
  array (
    'Neos.Diff' => 
    array (
    ),
    'Neos.Utility.Files' => 
    array (
    ),
    'TYPO3.Flow.Utility.Lock' => 
    array (
    ),
    'Neos.Utility.ObjectHandling' => 
    array (
    ),
    'Neos.Utility.Arrays' => 
    array (
    ),
    'Neos.Utility.MediaTypes' => 
    array (
    ),
    'Neos.Utility.OpcodeCache' => 
    array (
    ),
    'Neos.Utility.Pdo' => 
    array (
    ),
    'TYPO3.Flow.Error' => 
    array (
    ),
    'Neos.Utility.Schema' => 
    array (
      'TYPO3\\Flow\\Utility\\SchemaGenerator' => 
      array (
        'scope' => 'singleton',
      ),
      'TYPO3\\Flow\\Utility\\SchemaValidator' => 
      array (
        'scope' => 'singleton',
      ),
    ),
    'TYPO3.Flow.Utility.Unicode' => 
    array (
    ),
    'paragonie.randomcompat' => 
    array (
    ),
    'ramsey.uuid' => 
    array (
    ),
    'Doctrine.Common.Collections' => 
    array (
    ),
    'Doctrine.Common.Inflector' => 
    array (
    ),
    'doctrine.cache' => 
    array (
    ),
    'Doctrine.Common.Lexer' => 
    array (
    ),
    'doctrine.annotations' => 
    array (
    ),
    'doctrine.common' => 
    array (
    ),
    'Doctrine.DBAL' => 
    array (
    ),
    'doctrine.instantiator' => 
    array (
    ),
    'symfony.polyfillmbstring' => 
    array (
    ),
    'psr.log' => 
    array (
    ),
    'symfony.debug' => 
    array (
    ),
    'symfony.console' => 
    array (
    ),
    'Doctrine.ORM' => 
    array (
    ),
    'symfony.yaml' => 
    array (
    ),
    'zendframework.zendeventmanager' => 
    array (
    ),
    'zendframework.zendcode' => 
    array (
    ),
    'ocramius.proxymanager' => 
    array (
    ),
    'doctrine.migrations' => 
    array (
    ),
    'symfony.domcrawler' => 
    array (
    ),
    'neos.composerplugin' => 
    array (
    ),
    'Behat.Transliterator' => 
    array (
    ),
    'gedmo.doctrineextensions' => 
    array (
    ),
    'league.commonmark' => 
    array (
    ),
    'TYPO3.Fluid' => 
    array (
      'TYPO3\\Fluid\\Core\\Compiler\\TemplateCompiler' => 
      array (
        'properties' => 
        array (
          'templateCache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Fluid_TemplateCache',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Fluid\\View\\TemplateView' => 
      array (
        'properties' => 
        array (
          'renderingContext' => 
          array (
            'object' => 'TYPO3\\Fluid\\Core\\Rendering\\RenderingContext',
          ),
        ),
      ),
      'TYPO3\\Fluid\\View\\StandaloneView' => 
      array (
        'properties' => 
        array (
          'renderingContext' => 
          array (
            'object' => 'TYPO3\\Fluid\\Core\\Rendering\\RenderingContext',
          ),
        ),
      ),
    ),
    'TYPO3.Eel' => 
    array (
      'TYPO3\\Eel\\CompilingEvaluator' => 
      array (
        'properties' => 
        array (
          'expressionCache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Eel_Expression_Code',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Eel\\EelEvaluatorInterface' => 
      array (
        'className' => 'TYPO3\\Eel\\CompilingEvaluator',
      ),
      'TYPO3\\Eel\\FlowQuery\\OperationResolverInterface' => 
      array (
        'className' => 'TYPO3\\Eel\\FlowQuery\\OperationResolver',
      ),
    ),
    'TYPO3.Flow' => 
    array (
      'DateTime' => 
      array (
        'scope' => 'prototype',
        'autowiring' => 'off',
      ),
      'TYPO3\\Flow\\Cache\\CacheFactory' => 
      array (
        'arguments' => 
        array (
          1 => 
          array (
            'setting' => 'TYPO3.Flow.context',
          ),
        ),
      ),
      'TYPO3\\Flow\\I18n\\Service' => 
      array (
        'properties' => 
        array (
          'cache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_I18n_AvailableLocalesCache',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Flow\\I18n\\Cldr\\CldrModel' => 
      array (
        'properties' => 
        array (
          'cache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_I18n_Cldr_CldrModelCache',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Flow\\I18n\\Xliff\\XliffModel' => 
      array (
        'properties' => 
        array (
          'cache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_I18n_XmlModelCache',
                ),
              ),
            ),
          ),
          'i18nLogger' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Log\\LoggerFactory',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_I18n',
                ),
                2 => 
                array (
                  'value' => 'TYPO3\\Flow\\Log\\Logger',
                ),
                3 => 
                array (
                  'setting' => 'TYPO3.Flow.log.i18nLogger.backend',
                ),
                4 => 
                array (
                  'setting' => 'TYPO3.Flow.log.i18nLogger.backendOptions',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Flow\\I18n\\Cldr\\Reader\\DatesReader' => 
      array (
        'properties' => 
        array (
          'cache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_I18n_Cldr_Reader_DatesReaderCache',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Flow\\I18n\\Cldr\\Reader\\NumbersReader' => 
      array (
        'properties' => 
        array (
          'cache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_I18n_Cldr_Reader_NumbersReaderCache',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Flow\\I18n\\Cldr\\Reader\\PluralsReader' => 
      array (
        'properties' => 
        array (
          'cache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_I18n_Cldr_Reader_PluralsReaderCache',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Flow\\Log\\Backend\\FileBackend' => 
      array (
        'autowiring' => 'off',
      ),
      'TYPO3\\Flow\\Log\\Backend\\NullBackend' => 
      array (
        'autowiring' => 'off',
      ),
      'TYPO3\\Flow\\Log\\SystemLoggerInterface' => 
      array (
        'scope' => 'singleton',
        'factoryObjectName' => 'TYPO3\\Flow\\Log\\LoggerFactory',
        'arguments' => 
        array (
          1 => 
          array (
            'value' => 'SystemLogger',
          ),
          2 => 
          array (
            'setting' => 'TYPO3.Flow.log.systemLogger.logger',
          ),
          3 => 
          array (
            'setting' => 'TYPO3.Flow.log.systemLogger.backend',
          ),
          4 => 
          array (
            'setting' => 'TYPO3.Flow.log.systemLogger.backendOptions',
          ),
        ),
      ),
      'TYPO3\\Flow\\Log\\SecurityLoggerInterface' => 
      array (
        'scope' => 'singleton',
        'factoryObjectName' => 'TYPO3\\Flow\\Log\\LoggerFactory',
        'arguments' => 
        array (
          1 => 
          array (
            'value' => 'Flow_Security',
          ),
          2 => 
          array (
            'value' => 'TYPO3\\Flow\\Log\\Logger',
          ),
          3 => 
          array (
            'setting' => 'TYPO3.Flow.log.securityLogger.backend',
          ),
          4 => 
          array (
            'setting' => 'TYPO3.Flow.log.securityLogger.backendOptions',
          ),
        ),
      ),
      'TYPO3\\Flow\\Monitor\\ChangeDetectionStrategy\\ModificationTimeStrategy' => 
      array (
        'properties' => 
        array (
          'cache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_Monitor',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Flow\\Monitor\\FileMonitor' => 
      array (
        'properties' => 
        array (
          'cache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_Monitor',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Flow\\Http\\Component\\ComponentChain' => 
      array (
        'factoryObjectName' => 'TYPO3\\Flow\\Http\\Component\\ComponentChainFactory',
        'arguments' => 
        array (
          1 => 
          array (
            'setting' => 'TYPO3.Flow.http.chain',
          ),
        ),
      ),
      'TYPO3\\Flow\\Mvc\\Routing\\RouterCachingService' => 
      array (
        'properties' => 
        array (
          'routeCache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_Mvc_Routing_Route',
                ),
              ),
            ),
          ),
          'resolveCache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_Mvc_Routing_Resolve',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Flow\\Mvc\\ViewConfigurationManager' => 
      array (
        'properties' => 
        array (
          'cache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_Mvc_ViewConfigurations',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Flow\\Object\\ObjectManagerInterface' => 
      array (
        'className' => 'TYPO3\\Flow\\Object\\ObjectManager',
        'scope' => 'singleton',
        'autowiring' => 'off',
      ),
      'TYPO3\\Flow\\Object\\ObjectManager' => 
      array (
        'autowiring' => 'off',
      ),
      'TYPO3\\Flow\\Object\\CompileTimeObjectManager' => 
      array (
        'autowiring' => 'off',
      ),
      'TYPO3\\Flow\\Package\\PackageManagerInterface' => 
      array (
        'scope' => 'singleton',
      ),
      'Doctrine\\Common\\Persistence\\ObjectManager' => 
      array (
        'scope' => 'singleton',
        'factoryObjectName' => 'TYPO3\\Flow\\Persistence\\Doctrine\\EntityManagerFactory',
      ),
      'TYPO3\\Flow\\Persistence\\PersistenceManagerInterface' => 
      array (
        'className' => 'TYPO3\\Flow\\Persistence\\Doctrine\\PersistenceManager',
        'factoryObjectName' => 'TYPO3\\Flow\\Core\\Bootstrap',
        'factoryMethodName' => 'initializePersistenceManager',
      ),
      'TYPO3\\Flow\\Persistence\\Doctrine\\Logging\\SqlLogger' => 
      array (
        'properties' => 
        array (
          'logger' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Log\\LoggerFactory',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Sql_Queries',
                ),
                2 => 
                array (
                  'value' => 'TYPO3\\Flow\\Log\\Logger',
                ),
                3 => 
                array (
                  'value' => 'TYPO3\\Flow\\Log\\Backend\\FileBackend',
                ),
                4 => 
                array (
                  'setting' => 'TYPO3.Flow.log.sqlLogger.backendOptions',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Flow\\Property\\PropertyMapper' => 
      array (
        'properties' => 
        array (
          'cache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_PropertyMapper',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Flow\\Resource\\ResourceManager' => 
      array (
        'properties' => 
        array (
          'statusCache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_Resource_Status',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Flow\\Security\\Authentication\\AuthenticationManagerInterface' => 
      array (
        'className' => 'TYPO3\\Flow\\Security\\Authentication\\AuthenticationProviderManager',
      ),
      'TYPO3\\Flow\\Security\\Cryptography\\RsaWalletServiceInterface' => 
      array (
        'className' => 'TYPO3\\Flow\\Security\\Cryptography\\RsaWalletServicePhp',
        'scope' => 'singleton',
        'properties' => 
        array (
          'keystoreCache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_Security_Cryptography_RSAWallet',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Flow\\Security\\Authorization\\PrivilegeManagerInterface' => 
      array (
        'className' => 'TYPO3\\Flow\\Security\\Authorization\\PrivilegeManager',
      ),
      'TYPO3\\Flow\\Security\\Authorization\\FirewallInterface' => 
      array (
        'className' => 'TYPO3\\Flow\\Security\\Authorization\\FilterFirewall',
      ),
      'TYPO3\\Flow\\Security\\Cryptography\\HashService' => 
      array (
        'properties' => 
        array (
          'cache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_Security_Cryptography_HashService',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Flow\\Security\\Cryptography\\Pbkdf2HashingStrategy' => 
      array (
        'scope' => 'singleton',
        'arguments' => 
        array (
          1 => 
          array (
            'setting' => 'TYPO3.Flow.security.cryptography.Pbkdf2HashingStrategy.dynamicSaltLength',
          ),
          2 => 
          array (
            'setting' => 'TYPO3.Flow.security.cryptography.Pbkdf2HashingStrategy.iterationCount',
          ),
          3 => 
          array (
            'setting' => 'TYPO3.Flow.security.cryptography.Pbkdf2HashingStrategy.derivedKeyLength',
          ),
          4 => 
          array (
            'setting' => 'TYPO3.Flow.security.cryptography.Pbkdf2HashingStrategy.algorithm',
          ),
        ),
      ),
      'TYPO3\\Flow\\Security\\Cryptography\\BCryptHashingStrategy' => 
      array (
        'scope' => 'singleton',
        'arguments' => 
        array (
          1 => 
          array (
            'setting' => 'TYPO3.Flow.security.cryptography.BCryptHashingStrategy.cost',
          ),
        ),
      ),
      'TYPO3\\Flow\\Security\\Authorization\\Privilege\\Method\\MethodTargetExpressionParser' => 
      array (
        'scope' => 'singleton',
      ),
      'TYPO3\\Flow\\Security\\Authorization\\Privilege\\Method\\MethodPrivilegePointcutFilter' => 
      array (
        'scope' => 'singleton',
        'properties' => 
        array (
          'objectManager' => 
          array (
            'object' => 'TYPO3\\Flow\\Object\\ObjectManagerInterface',
          ),
        ),
      ),
      'TYPO3\\Flow\\Security\\Authorization\\Privilege\\Entity\\Doctrine\\EntityPrivilegeExpressionEvaluator' => 
      array (
        'properties' => 
        array (
          'expressionCache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Eel_Expression_Code',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Flow\\Session\\SessionInterface' => 
      array (
        'scope' => 'singleton',
        'factoryObjectName' => 'TYPO3\\Flow\\Session\\SessionManagerInterface',
        'factoryMethodName' => 'getCurrentSession',
      ),
      'TYPO3\\Flow\\Session\\Session' => 
      array (
        'properties' => 
        array (
          'metaDataCache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_Session_MetaData',
                ),
              ),
            ),
          ),
          'storageCache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_Session_Storage',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Flow\\Session\\SessionManagerInterface' => 
      array (
        'className' => 'TYPO3\\Flow\\Session\\SessionManager',
      ),
      'TYPO3\\Flow\\Session\\SessionManager' => 
      array (
        'properties' => 
        array (
          'metaDataCache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'Flow_Session_MetaData',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Flow\\Utility\\PdoHelper' => 
      array (
        'autowiring' => 'off',
        'scope' => 'prototype',
      ),
    ),
    'TYPO3.TypoScript' => 
    array (
      'TYPO3\\TypoScript\\View\\TypoScriptView' => 
      array (
        'properties' => 
        array (
          'fallbackView' => 
          array (
            'object' => 'TYPO3\\Fluid\\View\\TemplateView',
          ),
        ),
      ),
      'TYPO3\\TypoScript\\TypoScriptObjects\\Helpers\\FluidView' => 
      array (
        'properties' => 
        array (
          'renderingContext' => 
          array (
            'object' => 'TYPO3\\Fluid\\Core\\Rendering\\RenderingContext',
          ),
        ),
      ),
      'TYPO3\\TypoScript\\Core\\Cache\\ContentCache' => 
      array (
        'properties' => 
        array (
          'cache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'TYPO3_TypoScript_Content',
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Imagine' => 
    array (
      'Imagine\\Image\\ImagineInterface' => 
      array (
        'factoryObjectName' => 'TYPO3\\Imagine\\ImagineFactory',
      ),
    ),
    'TYPO3.Media' => 
    array (
      'TYPO3\\Media\\Domain\\Service\\ImageService' => 
      array (
        'properties' => 
        array (
          'imagineService' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Imagine\\ImagineFactory',
            ),
          ),
          'imageSizeCache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'TYPO3_Media_ImageSize',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Media\\Domain\\Model\\ImageInterface' => 
      array (
        'className' => 'TYPO3\\Media\\Domain\\Model\\Image',
      ),
      'TYPO3\\Media\\Domain\\Strategy\\AssetModelMappingStrategyInterface' => 
      array (
        'className' => 'TYPO3\\Media\\Domain\\Strategy\\ConfigurationAssetModelMappingStrategy',
      ),
    ),
    'TYPO3.Party' => 
    array (
    ),
    'TYPO3.Form' => 
    array (
      'TYPO3\\Form\\Core\\Renderer\\FluidFormRenderer' => 
      array (
        'properties' => 
        array (
          'renderingContext' => 
          array (
            'object' => 'TYPO3\\Fluid\\Core\\Rendering\\RenderingContext',
          ),
        ),
      ),
      'TYPO3\\Form\\Persistence\\FormPersistenceManagerInterface' => 
      array (
        'className' => 'TYPO3\\Form\\Persistence\\YamlPersistenceManager',
      ),
    ),
    'TYPO3.Twitter.Bootstrap' => 
    array (
    ),
    'TYPO3.Setup' => 
    array (
    ),
    'TYPO3.TYPO3CR' => 
    array (
      'TYPO3\\TYPO3CR\\Domain\\Repository\\ContentDimensionRepository' => 
      array (
        'properties' => 
        array (
          'dimensionsConfiguration' => 
          array (
            'setting' => 'TYPO3.TYPO3CR.contentDimensions',
          ),
        ),
      ),
      'TYPO3\\TYPO3CR\\Domain\\Service\\PublishingServiceInterface' => 
      array (
        'className' => 'TYPO3\\TYPO3CR\\Domain\\Service\\PublishingService',
      ),
      'TYPO3\\TYPO3CR\\Domain\\Model\\NodeInterface' => 
      array (
        'className' => 'TYPO3\\TYPO3CR\\Domain\\Model\\Node',
      ),
      'TYPO3\\TYPO3CR\\Domain\\Service\\ConfigurationContentDimensionPresetSource' => 
      array (
        'properties' => 
        array (
          'configuration' => 
          array (
            'setting' => 'TYPO3.TYPO3CR.contentDimensions',
          ),
        ),
      ),
      'TYPO3\\TYPO3CR\\Domain\\Service\\ContentDimensionPresetSourceInterface' => 
      array (
        'className' => 'TYPO3\\TYPO3CR\\Domain\\Service\\ConfigurationContentDimensionPresetSource',
      ),
      'TYPO3\\TYPO3CR\\Domain\\Model\\NodeLabelGeneratorInterface' => 
      array (
        'className' => 'TYPO3\\TYPO3CR\\Domain\\Model\\ExpressionBasedNodeLabelGenerator',
      ),
      'TYPO3\\TYPO3CR\\Domain\\Model\\NodeDataLabelGeneratorInterface' => 
      array (
        'className' => 'TYPO3\\TYPO3CR\\Domain\\Model\\FallbackNodeDataLabelGenerator',
      ),
      'TYPO3\\TYPO3CR\\Domain\\Service\\NodeServiceInterface' => 
      array (
        'className' => 'TYPO3\\TYPO3CR\\Domain\\Service\\NodeService',
      ),
      'TYPO3\\TYPO3CR\\Domain\\Service\\NodeTypeManager' => 
      array (
        'properties' => 
        array (
          'fullConfigurationCache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'TYPO3_TYPO3CR_FullNodeTypeConfiguration',
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos' => 
    array (
      'TYPO3\\Neos\\ViewHelpers\\Backend\\ConfigurationCacheVersionViewHelper' => 
      array (
        'properties' => 
        array (
          'configurationCache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'TYPO3_Neos_Configuration_Version',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\TYPO3CR\\Domain\\Service\\ContextFactoryInterface' => 
      array (
        'className' => 'TYPO3\\Neos\\Domain\\Service\\ContentContextFactory',
      ),
      'TYPO3\\TYPO3CR\\Domain\\Service\\PublishingServiceInterface' => 
      array (
        'className' => 'TYPO3\\Neos\\Service\\PublishingService',
      ),
      'TYPO3\\Neos\\Aspects\\TypoScriptCachingAspect' => 
      array (
        'properties' => 
        array (
          'typoScriptCache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'TYPO3_Neos_TypoScript',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\TYPO3CR\\Domain\\Service\\ContentDimensionPresetSourceInterface' => 
      array (
        'className' => 'TYPO3\\Neos\\Domain\\Service\\ConfigurationContentDimensionPresetSource',
      ),
      'TYPO3\\TYPO3CR\\Domain\\Service\\NodeServiceInterface' => 
      array (
        'className' => 'TYPO3\\Neos\\TYPO3CR\\NeosNodeService',
      ),
      'TYPO3\\Neos\\TYPO3CR\\NeosNodeServiceInterface' => 
      array (
        'className' => 'TYPO3\\Neos\\TYPO3CR\\NeosNodeService',
      ),
      'TYPO3\\Neos\\Domain\\Service\\ContentDimensionPresetSourceInterface' => 
      array (
        'className' => 'TYPO3\\Neos\\Domain\\Service\\ConfigurationContentDimensionPresetSource',
      ),
      'TYPO3\\Neos\\Domain\\Service\\ConfigurationContentDimensionPresetSource' => 
      array (
        'properties' => 
        array (
          'configuration' => 
          array (
            'setting' => 'TYPO3.TYPO3CR.contentDimensions',
          ),
        ),
      ),
      'TYPO3\\Neos\\Routing\\FrontendNodeRoutePartHandlerInterface' => 
      array (
        'className' => 'TYPO3\\Neos\\Routing\\FrontendNodeRoutePartHandler',
      ),
      'TYPO3\\Neos\\Domain\\Service\\NodeSearchServiceInterface' => 
      array (
        'className' => 'TYPO3\\Neos\\Domain\\Service\\NodeSearchService',
      ),
      'TYPO3\\Neos\\Service\\XliffService' => 
      array (
        'properties' => 
        array (
          'xliffToJsonTranslationsCache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'TYPO3_Neos_XliffToJsonTranslations',
                ),
              ),
            ),
          ),
        ),
      ),
      'League\\CommonMark\\CommonMarkConverter' => 
      array (
        'className' => 'League\\CommonMark\\CommonMarkConverter',
      ),
      'TYPO3\\Neos\\Controller\\Backend\\BackendController' => 
      array (
        'properties' => 
        array (
          'loginTokenCache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'TYPO3_Neos_LoginTokenCache',
                ),
              ),
            ),
          ),
        ),
      ),
      'TYPO3\\Neos\\Controller\\LoginController' => 
      array (
        'properties' => 
        array (
          'loginTokenCache' => 
          array (
            'object' => 
            array (
              'factoryObjectName' => 'TYPO3\\Flow\\Cache\\CacheManager',
              'factoryMethodName' => 'getCache',
              'arguments' => 
              array (
                1 => 
                array (
                  'value' => 'TYPO3_Neos_LoginTokenCache',
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.NodeTypes' => 
    array (
    ),
    'Neos.Outfit' => 
    array (
    ),
    'Flowpack.Neos.FrontendLogin' => 
    array (
    ),
    'webmozart.assert' => 
    array (
    ),
    'symfony.cssselector' => 
    array (
    ),
    'sebastian.codeunitreverselookup' => 
    array (
    ),
    'sebastian.diff' => 
    array (
    ),
    'sebastian.recursioncontext' => 
    array (
    ),
    'sebastian.exporter' => 
    array (
    ),
    'sebastian.comparator' => 
    array (
    ),
    'sebastian.environment' => 
    array (
    ),
    'sebastian.globalstate' => 
    array (
    ),
    'sebastian.objectenumerator' => 
    array (
    ),
    'sebastian.resourceoperations' => 
    array (
    ),
    'sebastian.version' => 
    array (
    ),
    'phpunit.phpfileiterator' => 
    array (
    ),
    'phpunit.phptokenstream' => 
    array (
    ),
    'phpunit.phptexttemplate' => 
    array (
    ),
    'phpunit.phpcodecoverage' => 
    array (
    ),
    'phpunit.phptimer' => 
    array (
    ),
    'phpunit.phpunitmockobjects' => 
    array (
    ),
    'phpdocumentor.reflectioncommon' => 
    array (
    ),
    'phpdocumentor.typeresolver' => 
    array (
    ),
    'phpdocumentor.reflectiondocblock' => 
    array (
    ),
    'phpspec.prophecy' => 
    array (
    ),
    'myclabs.deepcopy' => 
    array (
    ),
    'phpunit.phpunit' => 
    array (
    ),
    'org.bovigo.vfs' => 
    array (
    ),
    'imagine.imagine' => 
    array (
    ),
    'TYPO3.Neos.Seo' => 
    array (
    ),
    'TYPO3.Kickstart' => 
    array (
    ),
    'Flowpack.Behat' => 
    array (
    ),
    'Neos.RedirectHandler' => 
    array (
    ),
    'Neos.RedirectHandler.DatabaseStorage' => 
    array (
    ),
    'Neos.RedirectHandler.NeosAdapter' => 
    array (
    ),
    'TYPO3.Neos.Kickstarter' => 
    array (
    ),
  ),
  'NodeTypes' => 
  array (
    'unstructured' => 
    array (
      'abstract' => false,
      'constraints' => 
      array (
        'nodeTypes' => 
        array (
          '*' => true,
        ),
      ),
    ),
    'TYPO3.Neos:Node' => 
    array (
      'label' => '${String.cropAtWord(String.trim(String.stripTags(String.pregReplace(q(node).property(\'title\') || q(node).property(\'text\') || ((I18n.translate(node.nodeType.label) || node.nodeType.name) + (node.autoCreated ? \' (\' + node.name + \')\' : \'\')), \'/<br\\W*?\\/?>|\\x{00a0}|[[^:print:]]|\\s+/u\', \' \'))), 100, \'...\')}',
      'abstract' => true,
      'ui' => 
      array (
        'inspector' => 
        array (
          'tabs' => 
          array (
            'default' => 
            array (
              'label' => 'i18n',
              'position' => 10,
              'icon' => 'icon-pencil',
            ),
            'meta' => 
            array (
              'label' => 'i18n',
              'position' => 20,
              'icon' => 'icon-cog',
            ),
          ),
          'groups' => 
          array (
            'type' => 
            array (
              'label' => 'i18n',
              'tab' => 'meta',
              'icon' => 'icon-exchange',
              'position' => 990,
            ),
            'nodeInfo' => 
            array (
              'label' => 'i18n',
              'icon' => 'icon-info',
              'tab' => 'meta',
              'position' => 1000,
              'collapsed' => true,
            ),
          ),
          'views' => 
          array (
            'nodeInfo' => 
            array (
              'label' => 'i18n',
              'group' => 'nodeInfo',
              'view' => 'TYPO3.Neos/Inspector/Views/NodeInfoView',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        '_removed' => 
        array (
          'type' => 'boolean',
        ),
        '_creationDateTime' => 
        array (
          'type' => 'DateTime',
        ),
        '_lastModificationDateTime' => 
        array (
          'type' => 'DateTime',
        ),
        '_lastPublicationDateTime' => 
        array (
          'type' => 'DateTime',
        ),
        '_path' => 
        array (
          'type' => 'string',
        ),
        '_name' => 
        array (
          'type' => 'string',
        ),
        '_nodeType' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadIfChanged' => true,
            'inspector' => 
            array (
              'group' => 'type',
              'position' => 100,
              'editor' => 'TYPO3.Neos/Inspector/Editors/NodeTypeEditor',
              'editorOptions' => 
              array (
                'placeholder' => 'Loading ...',
                'baseNodeType' => '',
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos:Hidable' => 
    array (
      'abstract' => true,
      'ui' => 
      array (
        'inspector' => 
        array (
          'groups' => 
          array (
            'visibility' => 
            array (
              'label' => 'TYPO3.Neos:Inspector:groups.visibility',
              'icon' => 'icon-eye',
              'position' => 100,
              'tab' => 'meta',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        '_hidden' => 
        array (
          'type' => 'boolean',
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'visibility',
              'position' => 30,
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos:Timable' => 
    array (
      'abstract' => true,
      'ui' => 
      array (
        'inspector' => 
        array (
          'groups' => 
          array (
            'visibility' => 
            array (
              'label' => 'TYPO3.Neos:Inspector:groups.visibility',
              'icon' => 'icon-eye',
              'position' => 100,
              'tab' => 'meta',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        '_hiddenBeforeDateTime' => 
        array (
          'type' => 'DateTime',
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'visibility',
              'position' => 10,
              'editorOptions' => 
              array (
                'format' => 'd-m-Y H:i',
              ),
            ),
          ),
          'validation' => 
          array (
            'TYPO3.Neos/Validation/DateTimeValidator' => 
            array (
            ),
          ),
        ),
        '_hiddenAfterDateTime' => 
        array (
          'type' => 'DateTime',
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'visibility',
              'position' => 20,
              'editorOptions' => 
              array (
                'format' => 'd-m-Y H:i',
              ),
            ),
          ),
          'validation' => 
          array (
            'TYPO3.Neos/Validation/DateTimeValidator' => 
            array (
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos:Document' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Node' => true,
        'TYPO3.Neos:Hidable' => true,
        'TYPO3.Neos:Timable' => true,
        'TYPO3.Neos.Seo:TitleTagMixin' => true,
        'TYPO3.Neos.Seo:SeoMetaTagsMixin' => true,
        'TYPO3.Neos.Seo:TwitterCardMixin' => true,
        'TYPO3.Neos.Seo:CanonicalLinkMixin' => true,
        'TYPO3.Neos.Seo:OpenGraphMixin' => true,
        'TYPO3.Neos.Seo:XmlSitemapMixin' => true,
      ),
      'abstract' => true,
      'aggregate' => true,
      'constraints' => 
      array (
        'nodeTypes' => 
        array (
          '*' => false,
          'TYPO3.Neos:Document' => true,
        ),
      ),
      'ui' => 
      array (
        'label' => 'Document',
        'search' => 
        array (
          'searchCategory' => 'Documents',
        ),
        'inspector' => 
        array (
          'groups' => 
          array (
            'document' => 
            array (
              'label' => 'i18n',
              'position' => 10,
              'icon' => 'icon-file',
            ),
          ),
          'tabs' => 
          array (
            'seo' => 
            array (
              'label' => 'TYPO3.Neos.Seo:NodeTypes.Document:tabs.seo',
              'position' => 30,
              'icon' => 'icon-bullseye',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        '_nodeType' => 
        array (
          'ui' => 
          array (
            'inspector' => 
            array (
              'editorOptions' => 
              array (
                'baseNodeType' => 'TYPO3.Neos:Document',
              ),
            ),
          ),
        ),
        'title' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadPageIfChanged' => true,
            'inspector' => 
            array (
              'group' => 'document',
            ),
          ),
          'validation' => 
          array (
            'TYPO3.Neos/Validation/NotEmptyValidator' => 
            array (
            ),
            'TYPO3.Neos/Validation/StringLengthValidator' => 
            array (
              'minimum' => 1,
              'maximum' => 255,
            ),
          ),
        ),
        'uriPathSegment' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadPageIfChanged' => true,
            'inspector' => 
            array (
              'group' => 'document',
            ),
          ),
          'validation' => 
          array (
            'TYPO3.Neos/Validation/NotEmptyValidator' => 
            array (
            ),
            'TYPO3.Neos/Validation/StringLengthValidator' => 
            array (
              'minimum' => 1,
              'maximum' => 255,
            ),
            'regularExpression' => 
            array (
              'regularExpression' => '/^[a-z0-9\\-]+$/i',
            ),
          ),
        ),
        '_hidden' => 
        array (
          'ui' => 
          array (
            'reloadPageIfChanged' => true,
          ),
        ),
        '_hiddenInIndex' => 
        array (
          'type' => 'boolean',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadPageIfChanged' => true,
            'inspector' => 
            array (
              'group' => 'visibility',
              'position' => 40,
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos:Shortcut' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Document' => true,
        'TYPO3.Neos.Seo:TitleTagMixin' => false,
        'TYPO3.Neos.Seo:SeoMetaTagsMixin' => false,
        'TYPO3.Neos.Seo:TwitterCardMixin' => false,
        'TYPO3.Neos.Seo:CanonicalLinkMixin' => false,
        'TYPO3.Neos.Seo:OpenGraphMixin' => false,
        'TYPO3.Neos.Seo:XmlSitemapMixin' => false,
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'icon' => 'icon-share',
        'position' => 200,
        'inspector' => 
        array (
          'groups' => 
          array (
            'document' => 
            array (
              'label' => 'i18n',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        'targetMode' => 
        array (
          'type' => 'string',
          'defaultValue' => 'firstChildNode',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadPageIfChanged' => true,
            'inspector' => 
            array (
              'group' => 'document',
              'editor' => 'TYPO3.Neos/Inspector/Editors/SelectBoxEditor',
              'editorOptions' => 
              array (
                'values' => 
                array (
                  'firstChildNode' => 
                  array (
                    'label' => 'i18n',
                  ),
                  'parentNode' => 
                  array (
                    'label' => 'i18n',
                  ),
                  'selectedTarget' => 
                  array (
                    'label' => 'i18n',
                  ),
                ),
              ),
              'editorListeners' => 
              array (
                'removeTargetIfNotUsed' => 
                array (
                  'property' => 'target',
                  'handler' => 'TYPO3.Neos/Inspector/Handlers/ShortcutHandler',
                ),
              ),
            ),
          ),
        ),
        'target' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadPageIfChanged' => true,
            'inspector' => 
            array (
              'group' => 'document',
              'editor' => 'TYPO3.Neos/Inspector/Editors/LinkEditor',
              'editorListeners' => 
              array (
                'setTargetModeIfNotEmpty' => 
                array (
                  'property' => 'targetMode',
                  'handler' => 'TYPO3.Neos/Inspector/Handlers/ShortcutHandler',
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos:Plugin' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Content' => true,
      ),
      'abstract' => true,
      'ui' => 
      array (
        'label' => 'i18n',
        'group' => 'plugins',
        'icon' => 'icon-puzzle-piece',
        'inspector' => 
        array (
          'groups' => 
          array (
            'pluginSettings' => 
            array (
              'label' => 'i18n',
              'icon' => 'icon-sliders',
            ),
          ),
        ),
      ),
      'postprocessors' => 
      array (
        'PluginPostprocessor' => 
        array (
          'postprocessor' => 'TYPO3\\Neos\\NodeTypePostprocessor\\PluginNodeTypePostprocessor',
        ),
      ),
    ),
    'TYPO3.Neos:PluginView' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Content' => true,
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'group' => 'plugins',
        'icon' => 'icon-puzzle-piece',
        'position' => 100,
        'inspector' => 
        array (
          'groups' => 
          array (
            'pluginViews' => 
            array (
              'label' => 'i18n',
              'position' => 100,
              'icon' => 'icon-puzzle-piece',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        'plugin' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadIfChanged' => true,
            'inspector' => 
            array (
              'group' => 'pluginViews',
              'position' => 10,
              'editor' => 'TYPO3.Neos/Inspector/Editors/MasterPluginEditor',
            ),
          ),
        ),
        'view' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadIfChanged' => true,
            'inspector' => 
            array (
              'group' => 'pluginViews',
              'position' => 20,
              'editor' => 'TYPO3.Neos/Inspector/Editors/PluginViewEditor',
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos:Content' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Node' => true,
        'TYPO3.Neos:Hidable' => true,
        'TYPO3.Neos:Timable' => true,
      ),
      'abstract' => true,
      'constraints' => 
      array (
        'nodeTypes' => 
        array (
          '*' => false,
        ),
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'icon' => 'icon-square-o',
        'group' => 'general',
        'search' => 
        array (
          'searchCategory' => 'Content',
        ),
        'inspector' => 
        array (
          'groups' => 
          array (
            'type' => 
            array (
              'label' => 'i18n',
              'position' => 100,
              'tab' => 'meta',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        '_nodeType' => 
        array (
          'ui' => 
          array (
            'inspector' => 
            array (
              'editorOptions' => 
              array (
                'baseNodeType' => 'TYPO3.Neos:Content',
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos:ContentCollection' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Node' => true,
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'icon' => 'icon-folder-open-alt',
      ),
      'constraints' => 
      array (
        'nodeTypes' => 
        array (
          'TYPO3.Neos:Document' => false,
          '*' => true,
        ),
      ),
    ),
    'TYPO3.Neos:FallbackNode' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Node' => true,
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'icon' => 'icon-remove-sign',
      ),
      'properties' => 
      array (
        '_nodeType' => 
        array (
          'ui' => 
          array (
            'inspector' => 
            array (
              'editorOptions' => 
              array (
                'baseNodeType' => 'TYPO3.Neos:Content',
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.NodeTypes:Headline' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Content' => true,
        'TYPO3.Neos.NodeTypes:TitleMixin' => true,
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'icon' => 'icon-header',
        'position' => 100,
      ),
    ),
    'TYPO3.Neos.NodeTypes:Text' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Content' => true,
        'TYPO3.Neos.NodeTypes:TextMixin' => true,
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'icon' => 'icon-file-text',
        'position' => 200,
      ),
    ),
    'TYPO3.Neos.NodeTypes:Image' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Content' => true,
        'TYPO3.Neos.NodeTypes:ContentImageMixin' => true,
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'icon' => 'icon-picture',
        'position' => 300,
      ),
    ),
    'TYPO3.Neos.NodeTypes:TextWithImage' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Content' => true,
        'TYPO3.Neos.NodeTypes:TextMixin' => true,
        'TYPO3.Neos.NodeTypes:ContentImageMixin' => true,
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'icon' => 'icon-picture',
        'position' => 400,
      ),
    ),
    'TYPO3.Neos.NodeTypes:Html' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Content' => true,
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'icon' => 'icon-code',
        'position' => 500,
        'inspector' => 
        array (
          'groups' => 
          array (
            'html' => 
            array (
              'label' => 'i18n',
              'icon' => 'icon-code',
              'position' => 10,
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        'source' => 
        array (
          'type' => 'string',
          'defaultValue' => '<p>Enter HTML here</p>',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadIfChanged' => true,
            'inspector' => 
            array (
              'group' => 'html',
              'editor' => 'TYPO3.Neos/Inspector/Editors/CodeEditor',
              'editorOptions' => 
              array (
                'buttonLabel' => 'i18n',
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.NodeTypes:Menu' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Content' => true,
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'group' => 'structure',
        'icon' => 'icon-sitemap',
        'position' => 100,
        'inspector' => 
        array (
          'groups' => 
          array (
            'options' => 
            array (
              'label' => 'i18n',
              'position' => 30,
              'icon' => 'icon-sliders',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        'startLevel' => 
        array (
          'type' => 'string',
          'defaultValue' => '0',
          'ui' => 
          array (
            'reloadIfChanged' => true,
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'options',
              'editor' => 'TYPO3.Neos/Inspector/Editors/SelectBoxEditor',
              'editorOptions' => 
              array (
                'values' => 
                array (
                  -4 => 
                  array (
                    'label' => 'i18n',
                  ),
                  -3 => 
                  array (
                    'label' => 'i18n',
                  ),
                  -2 => 
                  array (
                    'label' => 'i18n',
                  ),
                  -1 => 
                  array (
                    'label' => 'i18n',
                  ),
                  0 => 
                  array (
                    'label' => 'i18n',
                  ),
                  1 => 
                  array (
                    'label' => 'i18n',
                  ),
                  2 => 
                  array (
                    'label' => 'i18n',
                  ),
                  3 => 
                  array (
                    'label' => 'i18n',
                  ),
                  4 => 
                  array (
                    'label' => 'i18n',
                  ),
                  5 => 
                  array (
                    'label' => 'i18n',
                  ),
                  6 => 
                  array (
                    'label' => 'i18n',
                  ),
                ),
              ),
            ),
          ),
        ),
        'selection' => 
        array (
          'type' => 'references',
          'ui' => 
          array (
            'reloadIfChanged' => true,
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'options',
            ),
          ),
        ),
        'startingPoint' => 
        array (
          'type' => 'reference',
          'ui' => 
          array (
            'reloadIfChanged' => true,
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'options',
            ),
          ),
        ),
        'maximumLevels' => 
        array (
          'type' => 'string',
          'defaultValue' => '1',
          'ui' => 
          array (
            'reloadIfChanged' => true,
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'options',
              'editor' => 'TYPO3.Neos/Inspector/Editors/SelectBoxEditor',
              'editorOptions' => 
              array (
                'values' => 
                array (
                  1 => 
                  array (
                    'label' => '1',
                  ),
                  2 => 
                  array (
                    'label' => '2',
                  ),
                  3 => 
                  array (
                    'label' => '3',
                  ),
                  4 => 
                  array (
                    'label' => '4',
                  ),
                  5 => 
                  array (
                    'label' => '5',
                  ),
                  6 => 
                  array (
                    'label' => '6',
                  ),
                  7 => 
                  array (
                    'label' => '7',
                  ),
                  8 => 
                  array (
                    'label' => '8',
                  ),
                  9 => 
                  array (
                    'label' => '9',
                  ),
                  10 => 
                  array (
                    'label' => '10',
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.NodeTypes:Column' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Content' => true,
      ),
      'abstract' => true,
      'ui' => 
      array (
        'group' => 'structure',
        'label' => 'i18n',
        'icon' => 'icon-columns',
        'inlineEditable' => true,
        'inspector' => 
        array (
          'groups' => 
          array (
            'column' => 
            array (
              'label' => 'i18n',
              'position' => 10,
              'icon' => 'icon-columns',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        'layout' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'column',
              'editor' => 'TYPO3.Neos/Inspector/Editors/SelectBoxEditor',
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.NodeTypes:TwoColumn' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos.NodeTypes:Column' => true,
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'position' => 200,
      ),
      'childNodes' => 
      array (
        'column0' => 
        array (
          'type' => 'TYPO3.Neos:ContentCollection',
        ),
        'column1' => 
        array (
          'type' => 'TYPO3.Neos:ContentCollection',
        ),
      ),
      'properties' => 
      array (
        'layout' => 
        array (
          'defaultValue' => '50-50',
          'ui' => 
          array (
            'reloadIfChanged' => true,
            'inspector' => 
            array (
              'editorOptions' => 
              array (
                'values' => 
                array (
                  '50-50' => 
                  array (
                    'label' => '50% / 50%',
                  ),
                  '75-25' => 
                  array (
                    'label' => '75% / 25%',
                  ),
                  '25-75' => 
                  array (
                    'label' => '25% / 75%',
                  ),
                  '66-33' => 
                  array (
                    'label' => '66% / 33%',
                  ),
                  '33-66' => 
                  array (
                    'label' => '33% / 66%',
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.NodeTypes:ThreeColumn' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos.NodeTypes:Column' => true,
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'position' => 300,
      ),
      'childNodes' => 
      array (
        'column0' => 
        array (
          'type' => 'TYPO3.Neos:ContentCollection',
        ),
        'column1' => 
        array (
          'type' => 'TYPO3.Neos:ContentCollection',
        ),
        'column2' => 
        array (
          'type' => 'TYPO3.Neos:ContentCollection',
        ),
      ),
      'properties' => 
      array (
        'layout' => 
        array (
          'defaultValue' => '33-33-33',
          'ui' => 
          array (
            'reloadIfChanged' => true,
            'inspector' => 
            array (
              'editorOptions' => 
              array (
                'values' => 
                array (
                  '33-33-33' => 
                  array (
                    'label' => '33% / 33% / 33%',
                  ),
                  '50-25-25' => 
                  array (
                    'label' => '50% / 25% / 25%',
                  ),
                  '25-50-25' => 
                  array (
                    'label' => '25% / 50% / 25%',
                  ),
                  '25-25-50' => 
                  array (
                    'label' => '25% / 25% / 50%',
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.NodeTypes:FourColumn' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos.NodeTypes:Column' => true,
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'position' => 400,
      ),
      'childNodes' => 
      array (
        'column0' => 
        array (
          'type' => 'TYPO3.Neos:ContentCollection',
        ),
        'column1' => 
        array (
          'type' => 'TYPO3.Neos:ContentCollection',
        ),
        'column2' => 
        array (
          'type' => 'TYPO3.Neos:ContentCollection',
        ),
        'column3' => 
        array (
          'type' => 'TYPO3.Neos:ContentCollection',
        ),
      ),
      'properties' => 
      array (
        'layout' => 
        array (
          'defaultValue' => '25-25-25-25',
          'ui' => 
          array (
            'reloadIfChanged' => true,
            'inspector' => 
            array (
              'editorOptions' => 
              array (
                'values' => 
                array (
                  '25-25-25-25' => 
                  array (
                    'label' => '25% / 25% / 25% / 25%',
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.NodeTypes:Form' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Content' => true,
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'icon' => 'icon-envelope-alt',
        'position' => 600,
        'inspector' => 
        array (
          'groups' => 
          array (
            'form' => 
            array (
              'label' => 'i18n',
              'position' => 30,
              'icon' => 'icon-envelope-alt',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        'formIdentifier' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadIfChanged' => true,
            'inspector' => 
            array (
              'group' => 'form',
              'editor' => 'TYPO3.Neos/Inspector/Editors/SelectBoxEditor',
              'editorOptions' => 
              array (
                'placeholder' => 'i18n',
                'dataSourceIdentifier' => 'neos-nodetypes-form-definitions',
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.NodeTypes:AssetList' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Content' => true,
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'icon' => 'icon-files-o',
        'position' => 700,
        'inspector' => 
        array (
          'groups' => 
          array (
            'resources' => 
            array (
              'label' => 'i18n',
              'position' => 5,
              'icon' => 'icon-files-o',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        'assets' => 
        array (
          'type' => 'array<TYPO3\\Media\\Domain\\Model\\Asset>',
          'ui' => 
          array (
            'inspector' => 
            array (
              'group' => 'resources',
            ),
            'label' => 'i18n',
            'reloadIfChanged' => true,
          ),
        ),
      ),
    ),
    'TYPO3.Neos.NodeTypes:ContentReferences' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Content' => true,
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'icon' => 'icon-copy',
        'position' => 800,
        'inspector' => 
        array (
          'groups' => 
          array (
            'references' => 
            array (
              'label' => 'i18n',
              'position' => 10,
              'icon' => 'icon-copy',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        'references' => 
        array (
          'type' => 'references',
          'ui' => 
          array (
            'inspector' => 
            array (
              'group' => 'references',
              'editorOptions' => 
              array (
                'nodeTypes' => 
                array (
                  0 => 'TYPO3.Neos:Content',
                ),
              ),
            ),
            'label' => 'i18n',
            'reloadIfChanged' => true,
          ),
        ),
      ),
    ),
    'TYPO3.Neos.NodeTypes:Records' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos.NodeTypes:ContentReferences' => true,
      ),
      'abstract' => true,
    ),
    'TYPO3.Neos.NodeTypes:Page' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Document' => true,
      ),
      'childNodes' => 
      array (
        'main' => 
        array (
          'type' => 'TYPO3.Neos:ContentCollection',
        ),
      ),
      'properties' => 
      array (
        'layout' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadIfChanged' => true,
            'inspector' => 
            array (
              'group' => NULL,
              'position' => 100,
              'editor' => 'TYPO3.Neos/Inspector/Editors/SelectBoxEditor',
              'editorOptions' => 
              array (
                'placeholder' => 'i18n',
                'values' => 
                array (
                  '' => 
                  array (
                    'label' => '',
                  ),
                ),
              ),
            ),
          ),
        ),
        'subpageLayout' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => NULL,
              'position' => 110,
              'editor' => 'TYPO3.Neos/Inspector/Editors/SelectBoxEditor',
              'editorOptions' => 
              array (
                'placeholder' => 'i18n',
                'values' => 
                array (
                  '' => 
                  array (
                    'label' => '',
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'icon' => 'icon-file',
        'position' => 100,
        'inspector' => 
        array (
          'groups' => 
          array (
            'document' => 
            array (
              'label' => 'i18n',
            ),
            'layout' => 
            array (
              'label' => 'i18n',
              'icon' => 'icon-paint-brush',
              'position' => 150,
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.NodeTypes:TitleMixin' => 
    array (
      'abstract' => true,
      'properties' => 
      array (
        'title' => 
        array (
          'type' => 'string',
          'defaultValue' => '<h1>Enter headline here</h1>',
          'ui' => 
          array (
            'inlineEditable' => true,
            'aloha' => 
            array (
              'format' => 
              array (
                'p' => false,
                'h1' => true,
                'h2' => true,
                'h3' => true,
                'removeFormat' => true,
              ),
              'link' => 
              array (
                'a' => true,
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.NodeTypes:TextMixin' => 
    array (
      'abstract' => true,
      'properties' => 
      array (
        'text' => 
        array (
          'type' => 'string',
          'defaultValue' => '',
          'ui' => 
          array (
            'inlineEditable' => true,
            'aloha' => 
            array (
              'placeholder' => 'i18n',
              'autoparagraph' => true,
              'format' => 
              array (
                'strong' => true,
                'em' => true,
                'u' => false,
                'sub' => false,
                'sup' => false,
                'del' => false,
                'p' => true,
                'h1' => true,
                'h2' => true,
                'h3' => true,
                'pre' => true,
                'removeFormat' => true,
              ),
              'table' => 
              array (
                'table' => true,
              ),
              'list' => 
              array (
                'ol' => true,
                'ul' => true,
              ),
              'link' => 
              array (
                'a' => true,
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.NodeTypes:ImageMixin' => 
    array (
      'abstract' => true,
      'ui' => 
      array (
        'inspector' => 
        array (
          'groups' => 
          array (
            'image' => 
            array (
              'label' => 'i18n',
              'position' => 5,
              'icon' => 'icon-image',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        'image' => 
        array (
          'type' => 'TYPO3\\Media\\Domain\\Model\\ImageInterface',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadIfChanged' => true,
            'inspector' => 
            array (
              'group' => 'image',
              'position' => 50,
            ),
          ),
        ),
        'alternativeText' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadIfChanged' => true,
            'inspector' => 
            array (
              'group' => 'image',
              'position' => 100,
            ),
          ),
        ),
        'title' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadIfChanged' => true,
            'inspector' => 
            array (
              'group' => 'image',
              'position' => 150,
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.NodeTypes:ImageCaptionMixin' => 
    array (
      'abstract' => true,
      'properties' => 
      array (
        'hasCaption' => 
        array (
          'type' => 'boolean',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadIfChanged' => true,
            'inspector' => 
            array (
              'group' => 'image',
              'position' => 200,
            ),
          ),
        ),
        'caption' => 
        array (
          'type' => 'string',
          'defaultValue' => '',
          'ui' => 
          array (
            'inlineEditable' => true,
            'aloha' => 
            array (
              'placeholder' => 'i18n',
              'autoparagraph' => true,
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.NodeTypes:ImageAlignmentMixin' => 
    array (
      'abstract' => true,
      'properties' => 
      array (
        'alignment' => 
        array (
          'type' => 'string',
          'defaultValue' => '',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadIfChanged' => true,
            'inspector' => 
            array (
              'group' => 'image',
              'position' => 400,
              'editor' => 'TYPO3.Neos/Inspector/Editors/SelectBoxEditor',
              'editorOptions' => 
              array (
                'placeholder' => 'i18n',
                'values' => 
                array (
                  '' => 
                  array (
                    'label' => '',
                  ),
                  'center' => 
                  array (
                    'label' => 'i18n',
                  ),
                  'left' => 
                  array (
                    'label' => 'i18n',
                  ),
                  'right' => 
                  array (
                    'label' => 'i18n',
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.NodeTypes:LinkMixin' => 
    array (
      'abstract' => true,
      'properties' => 
      array (
        'link' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadIfChanged' => true,
            'inspector' => 
            array (
              'position' => 300,
              'editor' => 'TYPO3.Neos/Inspector/Editors/LinkEditor',
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.NodeTypes:ContentImageMixin' => 
    array (
      'abstract' => true,
      'superTypes' => 
      array (
        'TYPO3.Neos.NodeTypes:ImageMixin' => true,
        'TYPO3.Neos.NodeTypes:LinkMixin' => true,
        'TYPO3.Neos.NodeTypes:ImageCaptionMixin' => true,
        'TYPO3.Neos.NodeTypes:ImageAlignmentMixin' => true,
      ),
      'properties' => 
      array (
        'link' => 
        array (
          'ui' => 
          array (
            'inspector' => 
            array (
              'group' => 'image',
            ),
          ),
        ),
      ),
    ),
    'Flowpack.Neos.FrontendLogin:LoginForm' => 
    array (
      'superTypes' => 
      array (
        'TYPO3.Neos:Plugin' => true,
      ),
      'ui' => 
      array (
        'label' => 'i18n',
        'icon' => 'icon-key',
        'inspector' => 
        array (
          'groups' => 
          array (
            'pluginSettings' => 
            array (
              'label' => 'i18n',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        'redirectAfterLogin' => 
        array (
          'type' => 'reference',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadIfChanged' => false,
            'inspector' => 
            array (
              'group' => 'pluginSettings',
              'editorOptions' => 
              array (
                'nodeTypes' => 
                array (
                  0 => 'TYPO3.Neos:Document',
                ),
              ),
            ),
          ),
        ),
        'redirectAfterLogout' => 
        array (
          'type' => 'reference',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadIfChanged' => false,
            'inspector' => 
            array (
              'group' => 'pluginSettings',
              'editorOptions' => 
              array (
                'nodeTypes' => 
                array (
                  0 => 'TYPO3.Neos:Document',
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.Seo:TitleTagMixin' => 
    array (
      'abstract' => true,
      'properties' => 
      array (
        'titleOverride' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadIfChanged' => true,
            'inspector' => 
            array (
              'group' => 'document',
              'position' => 10000,
              'editor' => 'TYPO3.Neos/Inspector/Editors/TextAreaEditor',
              'editorOptions' => 
              array (
                'placeholder' => 'Used in <title> tag, max. 60 chars',
              ),
            ),
          ),
          'validation' => 
          array (
            'TYPO3.Neos/Validation/StringLengthValidator' => 
            array (
              'maximum' => 60,
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.Seo:SeoMetaTagsMixin' => 
    array (
      'abstract' => true,
      'ui' => 
      array (
        'inspector' => 
        array (
          'groups' => 
          array (
            'seometa' => 
            array (
              'label' => 'i18n',
              'icon' => 'icon-tags',
              'position' => 100,
              'tab' => 'seo',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        'metaDescription' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'seometa',
              'position' => 10,
              'editor' => 'TYPO3.Neos/Inspector/Editors/TextAreaEditor',
              'editorOptions' => 
              array (
                'placeholder' => 'max. 156 characters',
              ),
            ),
          ),
          'validation' => 
          array (
            'TYPO3.Neos/Validation/StringLengthValidator' => 
            array (
              'maximum' => 156,
            ),
          ),
        ),
        'metaKeywords' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'seometa',
              'position' => 20,
              'editor' => 'TYPO3.Neos/Inspector/Editors/TextAreaEditor',
              'editorOptions' => 
              array (
                'placeholder' => 'comma separated, max. 255 chars',
              ),
            ),
          ),
          'validation' => 
          array (
            'TYPO3.Neos/Validation/StringLengthValidator' => 
            array (
              'maximum' => 255,
            ),
          ),
        ),
        'metaRobotsNoindex' => 
        array (
          'type' => 'boolean',
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'seometa',
              'position' => 30,
            ),
          ),
        ),
        'metaRobotsNofollow' => 
        array (
          'type' => 'boolean',
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'seometa',
              'position' => 40,
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.Seo:TwitterCardMixin' => 
    array (
      'abstract' => true,
      'ui' => 
      array (
        'inspector' => 
        array (
          'groups' => 
          array (
            'twittercard' => 
            array (
              'label' => 'i18n',
              'icon' => 'icon-twitter',
              'position' => 400,
              'tab' => 'seo',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        'twitterCardType' => 
        array (
          'type' => 'string',
          'defaultValue' => NULL,
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'twittercard',
              'editor' => 'TYPO3.Neos/Inspector/Editors/SelectBoxEditor',
              'editorOptions' => 
              array (
                'values' => 
                array (
                  '' => 
                  array (
                    'label' => 'None',
                  ),
                  'summary' => 
                  array (
                    'label' => 'Summary Card',
                  ),
                  'summary_large_image' => 
                  array (
                    'label' => 'Summary Card with Large Image',
                  ),
                  'photo' => 
                  array (
                    'label' => 'Photo Card',
                  ),
                ),
              ),
            ),
          ),
        ),
        'twitterCardCreator' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'twittercard',
              'editorOptions' => 
              array (
                'placeholder' => '@johnexample',
              ),
            ),
          ),
          'validation' => 
          array (
            'TYPO3.Neos/Validation/RegularExpressionValidator' => 
            array (
              'regularExpression' => '/@[a-z0-9_]{1,15}/i',
            ),
          ),
        ),
        'twitterCardTitle' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'twittercard',
              'editor' => 'TYPO3.Neos/Inspector/Editors/TextAreaEditor',
              'editorOptions' => 
              array (
                'placeholder' => 'max. 70 characters',
              ),
            ),
          ),
          'validation' => 
          array (
            'TYPO3.Neos/Validation/StringLengthValidator' => 
            array (
              'maximum' => 70,
            ),
          ),
        ),
        'twitterCardDescription' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'twittercard',
              'editor' => 'TYPO3.Neos/Inspector/Editors/TextAreaEditor',
              'editorOptions' => 
              array (
                'placeholder' => 'max. 200 characters',
              ),
            ),
          ),
          'validation' => 
          array (
            'TYPO3.Neos/Validation/StringLengthValidator' => 
            array (
              'maximum' => 200,
            ),
          ),
        ),
        'twitterCardImage' => 
        array (
          'type' => 'TYPO3\\Media\\Domain\\Model\\ImageInterface',
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'twittercard',
              'editorOptions' => 
              array (
                'features' => 
                array (
                  'crop' => true,
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.Seo:CanonicalLinkMixin' => 
    array (
      'abstract' => true,
      'ui' => 
      array (
        'inspector' => 
        array (
          'groups' => 
          array (
            'canonicallink' => 
            array (
              'label' => 'i18n',
              'icon' => 'icon-link',
              'position' => 200,
              'tab' => 'seo',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        'canonicalLink' => 
        array (
          'type' => 'string',
          'defaultValue' => '',
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'canonicallink',
              'position' => 30,
              'editorOptions' => 
              array (
                'placeholder' => 'Paste a url, or use the default.',
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.Seo:OpenGraphMixin' => 
    array (
      'abstract' => true,
      'ui' => 
      array (
        'inspector' => 
        array (
          'groups' => 
          array (
            'openGraph' => 
            array (
              'label' => 'i18n',
              'icon' => 'icon-share-alt',
              'position' => 300,
              'tab' => 'seo',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        'openGraphType' => 
        array (
          'type' => 'string',
          'defaultValue' => NULL,
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'openGraph',
              'editor' => 'TYPO3.Neos/Inspector/Editors/SelectBoxEditor',
              'editorOptions' => 
              array (
                'values' => 
                array (
                  '' => 
                  array (
                    'label' => 'None',
                  ),
                  'website' => 
                  array (
                    'label' => 'Website',
                  ),
                  'article' => 
                  array (
                    'label' => 'Article',
                  ),
                ),
              ),
            ),
          ),
        ),
        'openGraphTitle' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'reloadIfChanged' => true,
            'inspector' => 
            array (
              'group' => 'openGraph',
              'editor' => 'TYPO3.Neos/Inspector/Editors/TextAreaEditor',
              'editorOptions' => 
              array (
                'placeholder' => 'Used as og:title , max. 60 chars',
              ),
            ),
          ),
          'validation' => 
          array (
            'TYPO3.Neos/Validation/StringLengthValidator' => 
            array (
              'maximum' => 60,
            ),
          ),
        ),
        'openGraphDescription' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'openGraph',
              'editor' => 'TYPO3.Neos/Inspector/Editors/TextAreaEditor',
              'editorOptions' => 
              array (
                'placeholder' => 'max. 200 characters',
              ),
            ),
          ),
          'validation' => 
          array (
            'TYPO3.Neos/Validation/StringLengthValidator' => 
            array (
              'maximum' => 200,
            ),
          ),
        ),
        'openGraphImage' => 
        array (
          'type' => 'TYPO3\\Media\\Domain\\Model\\ImageInterface',
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'openGraph',
              'editorOptions' => 
              array (
                'features' => 
                array (
                  'crop' => true,
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'TYPO3.Neos.Seo:XmlSitemapMixin' => 
    array (
      'abstract' => true,
      'ui' => 
      array (
        'inspector' => 
        array (
          'groups' => 
          array (
            'xmlsitemap' => 
            array (
              'label' => 'i18n',
              'icon' => 'icon-sitemap',
              'position' => 500,
              'tab' => 'seo',
            ),
          ),
        ),
      ),
      'properties' => 
      array (
        'xmlSitemapChangeFrequency' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'xmlsitemap',
              'position' => 10,
              'editor' => 'TYPO3.Neos/Inspector/Editors/SelectBoxEditor',
              'editorOptions' => 
              array (
                'values' => 
                array (
                  '' => 
                  array (
                    'label' => 'none (unspecified)',
                  ),
                  'always' => 
                  array (
                    'label' => 'Always',
                  ),
                  'hourly' => 
                  array (
                    'label' => 'Hourly',
                  ),
                  'daily' => 
                  array (
                    'label' => 'Daily',
                  ),
                  'weekly' => 
                  array (
                    'label' => 'Weekly',
                  ),
                  'monthly' => 
                  array (
                    'label' => 'Monthly',
                  ),
                  'yearly' => 
                  array (
                    'label' => 'Yearly',
                  ),
                  'never' => 
                  array (
                    'label' => 'Never (Archived)',
                  ),
                ),
              ),
            ),
          ),
        ),
        'xmlSitemapPriority' => 
        array (
          'type' => 'string',
          'ui' => 
          array (
            'label' => 'i18n',
            'inspector' => 
            array (
              'group' => 'xmlsitemap',
              'position' => 20,
              'editorOptions' => 
              array (
                'placeholder' => 'between 0 and 1',
              ),
            ),
          ),
          'validation' => 
          array (
            'TYPO3.Neos/Validation/NumberRangeValidator' => 
            array (
              'minimum' => 0,
              'maximum' => 1,
            ),
          ),
        ),
      ),
    ),
  ),
);