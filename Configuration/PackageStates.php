<?php
# PackageStates.php

# This file is maintained by Flow's package management. You shouldn't edit it manually
# manually, you should rather use the command line commands for maintaining packages.
# You'll find detailed information about the typo3.flow:package:* commands in their
# respective help screens.

# This file will be regenerated automatically if it doesn't exist. Deleting this file
# should, however, never become necessary if you use the package commands.

return array (
  'packages' => 
  array (
    'neos/diff' => 
    array (
      'state' => 'active',
      'packageKey' => 'Neos.Diff',
      'packagePath' => 'Application/Neos.Diff/',
      'composerName' => 'neos/diff',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Neos\\Diff\\' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'neos/utility-files' => 
    array (
      'state' => 'active',
      'packageKey' => 'Neos.Utility.Files',
      'packagePath' => 'Libraries/neos/utility-files/',
      'composerName' => 'neos/utility-files',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Flow\\Utility' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'neos/utility-lock' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.Flow.Utility.Lock',
      'packagePath' => 'Libraries/neos/utility-lock/',
      'composerName' => 'neos/utility-lock',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Flow\\Utility\\Lock' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'neos/utility-objecthandling' => 
    array (
      'state' => 'active',
      'packageKey' => 'Neos.Utility.ObjectHandling',
      'packagePath' => 'Libraries/neos/utility-objecthandling/',
      'composerName' => 'neos/utility-objecthandling',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Flow\\Reflection' => 'Classes',
          'TYPO3\\Flow\\Utility' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'neos/utility-arrays' => 
    array (
      'state' => 'active',
      'packageKey' => 'Neos.Utility.Arrays',
      'packagePath' => 'Libraries/neos/utility-arrays/',
      'composerName' => 'neos/utility-arrays',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Flow\\Utility' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'neos/utility-mediatypes' => 
    array (
      'state' => 'active',
      'packageKey' => 'Neos.Utility.MediaTypes',
      'packagePath' => 'Libraries/neos/utility-mediatypes/',
      'composerName' => 'neos/utility-mediatypes',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Flow\\Utility' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'neos/utility-opcodecache' => 
    array (
      'state' => 'active',
      'packageKey' => 'Neos.Utility.OpcodeCache',
      'packagePath' => 'Libraries/neos/utility-opcodecache/',
      'composerName' => 'neos/utility-opcodecache',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Flow\\Utility' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'neos/utility-pdo' => 
    array (
      'state' => 'active',
      'packageKey' => 'Neos.Utility.Pdo',
      'packagePath' => 'Libraries/neos/utility-pdo/',
      'composerName' => 'neos/utility-pdo',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Flow\\Utility' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'neos/error-messages' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.Flow.Error',
      'packagePath' => 'Libraries/neos/error-messages/',
      'composerName' => 'neos/error-messages',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Flow\\Error' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'neos/utility-schema' => 
    array (
      'state' => 'active',
      'packageKey' => 'Neos.Utility.Schema',
      'packagePath' => 'Libraries/neos/utility-schema/',
      'composerName' => 'neos/utility-schema',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Flow\\Utility' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'neos/utility-unicode' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.Flow.Utility.Unicode',
      'packagePath' => 'Libraries/neos/utility-unicode/',
      'composerName' => 'neos/utility-unicode',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Flow\\Utility\\Unicode' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'paragonie/random_compat' => 
    array (
      'state' => 'active',
      'packageKey' => 'paragonie.randomcompat',
      'packagePath' => 'Libraries/paragonie/random_compat/',
      'composerName' => 'paragonie/random_compat',
      'autoloadConfiguration' => 
      array (
        'files' => 
        array (
          0 => 'lib/random.php',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'ramsey/uuid' => 
    array (
      'state' => 'active',
      'packageKey' => 'ramsey.uuid',
      'packagePath' => 'Libraries/ramsey/uuid/',
      'composerName' => 'ramsey/uuid',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Ramsey\\Uuid\\' => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'doctrine/collections' => 
    array (
      'state' => 'active',
      'packageKey' => 'Doctrine.Common.Collections',
      'packagePath' => 'Libraries/doctrine/collections/',
      'composerName' => 'doctrine/collections',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'Doctrine\\Common\\Collections\\' => 'lib/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'doctrine/inflector' => 
    array (
      'state' => 'active',
      'packageKey' => 'Doctrine.Common.Inflector',
      'packagePath' => 'Libraries/doctrine/inflector/',
      'composerName' => 'doctrine/inflector',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'Doctrine\\Common\\Inflector\\' => 'lib/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'doctrine/cache' => 
    array (
      'state' => 'active',
      'packageKey' => 'doctrine.cache',
      'packagePath' => 'Libraries/doctrine/cache/',
      'composerName' => 'doctrine/cache',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Doctrine\\Common\\Cache\\' => 'lib/Doctrine/Common/Cache',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'doctrine/lexer' => 
    array (
      'state' => 'active',
      'packageKey' => 'Doctrine.Common.Lexer',
      'packagePath' => 'Libraries/doctrine/lexer/',
      'composerName' => 'doctrine/lexer',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'Doctrine\\Common\\Lexer\\' => 'lib/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'doctrine/annotations' => 
    array (
      'state' => 'active',
      'packageKey' => 'doctrine.annotations',
      'packagePath' => 'Libraries/doctrine/annotations/',
      'composerName' => 'doctrine/annotations',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Doctrine\\Common\\Annotations\\' => 'lib/Doctrine/Common/Annotations',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'doctrine/common' => 
    array (
      'state' => 'active',
      'packageKey' => 'doctrine.common',
      'packagePath' => 'Libraries/doctrine/common/',
      'composerName' => 'doctrine/common',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Doctrine\\Common\\' => 'lib/Doctrine/Common',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'doctrine/dbal' => 
    array (
      'state' => 'active',
      'packageKey' => 'Doctrine.DBAL',
      'packagePath' => 'Libraries/doctrine/dbal/',
      'composerName' => 'doctrine/dbal',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'Doctrine\\DBAL\\' => 'lib/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'doctrine/instantiator' => 
    array (
      'state' => 'active',
      'packageKey' => 'doctrine.instantiator',
      'packagePath' => 'Libraries/doctrine/instantiator/',
      'composerName' => 'doctrine/instantiator',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Doctrine\\Instantiator\\' => 'src/Doctrine/Instantiator/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'state' => 'active',
      'packageKey' => 'symfony.polyfillmbstring',
      'packagePath' => 'Libraries/symfony/polyfill-mbstring/',
      'composerName' => 'symfony/polyfill-mbstring',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Symfony\\Polyfill\\Mbstring\\' => '',
        ),
        'files' => 
        array (
          0 => 'bootstrap.php',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'psr/log' => 
    array (
      'state' => 'active',
      'packageKey' => 'psr.log',
      'packagePath' => 'Libraries/psr/log/',
      'composerName' => 'psr/log',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Psr\\Log\\' => 'Psr/Log/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'symfony/debug' => 
    array (
      'state' => 'active',
      'packageKey' => 'symfony.debug',
      'packagePath' => 'Libraries/symfony/debug/',
      'composerName' => 'symfony/debug',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Symfony\\Component\\Debug\\' => '',
        ),
        'exclude-from-classmap' => 
        array (
          0 => '/Tests/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'symfony/console' => 
    array (
      'state' => 'active',
      'packageKey' => 'symfony.console',
      'packagePath' => 'Libraries/symfony/console/',
      'composerName' => 'symfony/console',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Symfony\\Component\\Console\\' => '',
        ),
        'exclude-from-classmap' => 
        array (
          0 => '/Tests/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'doctrine/orm' => 
    array (
      'state' => 'active',
      'packageKey' => 'Doctrine.ORM',
      'packagePath' => 'Libraries/doctrine/orm/',
      'composerName' => 'doctrine/orm',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'Doctrine\\ORM\\' => 'lib/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'symfony/yaml' => 
    array (
      'state' => 'active',
      'packageKey' => 'symfony.yaml',
      'packagePath' => 'Libraries/symfony/yaml/',
      'composerName' => 'symfony/yaml',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Symfony\\Component\\Yaml\\' => '',
        ),
        'exclude-from-classmap' => 
        array (
          0 => '/Tests/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'zendframework/zend-eventmanager' => 
    array (
      'state' => 'active',
      'packageKey' => 'zendframework.zendeventmanager',
      'packagePath' => 'Libraries/zendframework/zend-eventmanager/',
      'composerName' => 'zendframework/zend-eventmanager',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Zend\\EventManager\\' => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'zendframework/zend-code' => 
    array (
      'state' => 'active',
      'packageKey' => 'zendframework.zendcode',
      'packagePath' => 'Libraries/zendframework/zend-code/',
      'composerName' => 'zendframework/zend-code',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Zend\\Code\\' => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'ocramius/proxy-manager' => 
    array (
      'state' => 'active',
      'packageKey' => 'ocramius.proxymanager',
      'packagePath' => 'Libraries/ocramius/proxy-manager/',
      'composerName' => 'ocramius/proxy-manager',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'ProxyManager\\' => 'src',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'doctrine/migrations' => 
    array (
      'state' => 'active',
      'packageKey' => 'doctrine.migrations',
      'packagePath' => 'Libraries/doctrine/migrations/',
      'composerName' => 'doctrine/migrations',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Doctrine\\DBAL\\Migrations\\' => 'lib/Doctrine/DBAL/Migrations',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'symfony/dom-crawler' => 
    array (
      'state' => 'active',
      'packageKey' => 'symfony.domcrawler',
      'packagePath' => 'Libraries/symfony/dom-crawler/',
      'composerName' => 'symfony/dom-crawler',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Symfony\\Component\\DomCrawler\\' => '',
        ),
        'exclude-from-classmap' => 
        array (
          0 => '/Tests/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'neos/composer-plugin' => 
    array (
      'state' => 'active',
      'packageKey' => 'neos.composerplugin',
      'packagePath' => 'Libraries/neos/composer-plugin/',
      'composerName' => 'neos/composer-plugin',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Neos\\ComposerPlugin\\' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'behat/transliterator' => 
    array (
      'state' => 'active',
      'packageKey' => 'Behat.Transliterator',
      'packagePath' => 'Libraries/behat/transliterator/',
      'composerName' => 'behat/transliterator',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'Behat\\Transliterator' => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'gedmo/doctrine-extensions' => 
    array (
      'state' => 'active',
      'packageKey' => 'gedmo.doctrineextensions',
      'packagePath' => 'Libraries/gedmo/doctrine-extensions/',
      'composerName' => 'gedmo/doctrine-extensions',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'Gedmo\\' => 'lib/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'league/commonmark' => 
    array (
      'state' => 'active',
      'packageKey' => 'league.commonmark',
      'packagePath' => 'Libraries/league/commonmark/',
      'composerName' => 'league/commonmark',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'League\\CommonMark\\' => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'typo3/fluid' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.Fluid',
      'packagePath' => 'Framework/TYPO3.Fluid/',
      'composerName' => 'typo3/fluid',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Fluid' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
        'className' => 'TYPO3\\Fluid\\Package',
        'pathAndFilename' => 'Classes/TYPO3/Fluid/Package.php',
      ),
      'error-sorting-limit-reached' => true,
    ),
    'typo3/eel' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.Eel',
      'packagePath' => 'Framework/TYPO3.Eel/',
      'composerName' => 'typo3/eel',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Eel' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
        'className' => 'TYPO3\\Eel\\Package',
        'pathAndFilename' => 'Classes/TYPO3/Eel/Package.php',
      ),
      'error-sorting-limit-reached' => true,
    ),
    'typo3/flow' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.Flow',
      'packagePath' => 'Framework/TYPO3.Flow/',
      'composerName' => 'typo3/flow',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Flow' => 'Classes',
        ),
        'psr-4' => 
        array (
          'TYPO3\\Flow\\Core\\Migrations\\' => 'Scripts/Migrations',
        ),
      ),
      'packageClassInformation' => 
      array (
        'className' => 'TYPO3\\Flow\\Package',
        'pathAndFilename' => 'Classes/TYPO3/Flow/Package.php',
      ),
    ),
    'typo3/typoscript' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.TypoScript',
      'packagePath' => 'Application/TYPO3.TypoScript/',
      'composerName' => 'typo3/typoscript',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\TypoScript' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
        'className' => 'TYPO3\\TypoScript\\Package',
        'pathAndFilename' => 'Classes/TYPO3/TypoScript/Package.php',
      ),
    ),
    'typo3/imagine' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.Imagine',
      'packagePath' => 'Application/TYPO3.Imagine/',
      'composerName' => 'typo3/imagine',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Imagine' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'typo3/media' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.Media',
      'packagePath' => 'Application/TYPO3.Media/',
      'composerName' => 'typo3/media',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Media' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
        'className' => 'TYPO3\\Media\\Package',
        'pathAndFilename' => 'Classes/TYPO3/Media/Package.php',
      ),
    ),
    'typo3/party' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.Party',
      'packagePath' => 'Application/TYPO3.Party/',
      'composerName' => 'typo3/party',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Party' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'typo3/form' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.Form',
      'packagePath' => 'Application/TYPO3.Form/',
      'composerName' => 'typo3/form',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Form' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'typo3/twitter-bootstrap' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.Twitter.Bootstrap',
      'packagePath' => 'Application/TYPO3.Twitter.Bootstrap/',
      'composerName' => 'typo3/twitter-bootstrap',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Twitter\\Bootstrap' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'typo3/setup' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.Setup',
      'packagePath' => 'Application/TYPO3.Setup/',
      'composerName' => 'typo3/setup',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Setup' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
        'className' => 'TYPO3\\Setup\\Package',
        'pathAndFilename' => 'Classes/TYPO3/Setup/Package.php',
      ),
    ),
    'typo3/typo3cr' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.TYPO3CR',
      'packagePath' => 'Application/TYPO3.TYPO3CR/',
      'composerName' => 'typo3/typo3cr',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\TYPO3CR' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
        'className' => 'TYPO3\\TYPO3CR\\Package',
        'pathAndFilename' => 'Classes/TYPO3/TYPO3CR/Package.php',
      ),
    ),
    'typo3/neos' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.Neos',
      'packagePath' => 'Application/TYPO3.Neos/',
      'composerName' => 'typo3/neos',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Neos' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
        'className' => 'TYPO3\\Neos\\Package',
        'pathAndFilename' => 'Classes/TYPO3/Neos/Package.php',
      ),
    ),
    'typo3/neos-nodetypes' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.Neos.NodeTypes',
      'packagePath' => 'Application/TYPO3.Neos.NodeTypes/',
      'composerName' => 'typo3/neos-nodetypes',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Neos\\NodeTypes' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'neos/outfit' => 
    array (
      'state' => 'active',
      'packageKey' => 'Neos.Outfit',
      'packagePath' => 'Sites/Neos.Outfit/',
      'composerName' => 'neos/outfit',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'Neos\\Outfit' => 'Classes/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'mh/demosite' => 
    array (
      'state' => 'inactive',
      'packageKey' => 'MH.DemoSite',
      'packagePath' => 'Inactive/Sites/MH.DemoSite/',
      'composerName' => 'mh/demosite',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'MH\\DemoSite' => 'Classes/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'flowpack/neos-frontendlogin' => 
    array (
      'state' => 'active',
      'packageKey' => 'Flowpack.Neos.FrontendLogin',
      'packagePath' => 'Plugins/Flowpack.Neos.FrontendLogin/',
      'composerName' => 'flowpack/neos-frontendlogin',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'Flowpack\\Neos\\FrontendLogin' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'webmozart/assert' => 
    array (
      'state' => 'active',
      'packageKey' => 'webmozart.assert',
      'packagePath' => 'Libraries/webmozart/assert/',
      'composerName' => 'webmozart/assert',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Webmozart\\Assert\\' => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'symfony/css-selector' => 
    array (
      'state' => 'active',
      'packageKey' => 'symfony.cssselector',
      'packagePath' => 'Libraries/symfony/css-selector/',
      'composerName' => 'symfony/css-selector',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Symfony\\Component\\CssSelector\\' => '',
        ),
        'exclude-from-classmap' => 
        array (
          0 => '/Tests/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'state' => 'active',
      'packageKey' => 'sebastian.codeunitreverselookup',
      'packagePath' => 'Libraries/sebastian/code-unit-reverse-lookup/',
      'composerName' => 'sebastian/code-unit-reverse-lookup',
      'autoloadConfiguration' => 
      array (
        'classmap' => 
        array (
          0 => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'sebastian/diff' => 
    array (
      'state' => 'active',
      'packageKey' => 'sebastian.diff',
      'packagePath' => 'Libraries/sebastian/diff/',
      'composerName' => 'sebastian/diff',
      'autoloadConfiguration' => 
      array (
        'classmap' => 
        array (
          0 => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'sebastian/recursion-context' => 
    array (
      'state' => 'active',
      'packageKey' => 'sebastian.recursioncontext',
      'packagePath' => 'Libraries/sebastian/recursion-context/',
      'composerName' => 'sebastian/recursion-context',
      'autoloadConfiguration' => 
      array (
        'classmap' => 
        array (
          0 => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'sebastian/exporter' => 
    array (
      'state' => 'active',
      'packageKey' => 'sebastian.exporter',
      'packagePath' => 'Libraries/sebastian/exporter/',
      'composerName' => 'sebastian/exporter',
      'autoloadConfiguration' => 
      array (
        'classmap' => 
        array (
          0 => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'sebastian/comparator' => 
    array (
      'state' => 'active',
      'packageKey' => 'sebastian.comparator',
      'packagePath' => 'Libraries/sebastian/comparator/',
      'composerName' => 'sebastian/comparator',
      'autoloadConfiguration' => 
      array (
        'classmap' => 
        array (
          0 => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'sebastian/environment' => 
    array (
      'state' => 'active',
      'packageKey' => 'sebastian.environment',
      'packagePath' => 'Libraries/sebastian/environment/',
      'composerName' => 'sebastian/environment',
      'autoloadConfiguration' => 
      array (
        'classmap' => 
        array (
          0 => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'sebastian/global-state' => 
    array (
      'state' => 'active',
      'packageKey' => 'sebastian.globalstate',
      'packagePath' => 'Libraries/sebastian/global-state/',
      'composerName' => 'sebastian/global-state',
      'autoloadConfiguration' => 
      array (
        'classmap' => 
        array (
          0 => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'sebastian/object-enumerator' => 
    array (
      'state' => 'active',
      'packageKey' => 'sebastian.objectenumerator',
      'packagePath' => 'Libraries/sebastian/object-enumerator/',
      'composerName' => 'sebastian/object-enumerator',
      'autoloadConfiguration' => 
      array (
        'classmap' => 
        array (
          0 => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'sebastian/resource-operations' => 
    array (
      'state' => 'active',
      'packageKey' => 'sebastian.resourceoperations',
      'packagePath' => 'Libraries/sebastian/resource-operations/',
      'composerName' => 'sebastian/resource-operations',
      'autoloadConfiguration' => 
      array (
        'classmap' => 
        array (
          0 => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'sebastian/version' => 
    array (
      'state' => 'active',
      'packageKey' => 'sebastian.version',
      'packagePath' => 'Libraries/sebastian/version/',
      'composerName' => 'sebastian/version',
      'autoloadConfiguration' => 
      array (
        'classmap' => 
        array (
          0 => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'phpunit/php-file-iterator' => 
    array (
      'state' => 'active',
      'packageKey' => 'phpunit.phpfileiterator',
      'packagePath' => 'Libraries/phpunit/php-file-iterator/',
      'composerName' => 'phpunit/php-file-iterator',
      'autoloadConfiguration' => 
      array (
        'classmap' => 
        array (
          0 => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'phpunit/php-token-stream' => 
    array (
      'state' => 'active',
      'packageKey' => 'phpunit.phptokenstream',
      'packagePath' => 'Libraries/phpunit/php-token-stream/',
      'composerName' => 'phpunit/php-token-stream',
      'autoloadConfiguration' => 
      array (
        'classmap' => 
        array (
          0 => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'phpunit/php-text-template' => 
    array (
      'state' => 'active',
      'packageKey' => 'phpunit.phptexttemplate',
      'packagePath' => 'Libraries/phpunit/php-text-template/',
      'composerName' => 'phpunit/php-text-template',
      'autoloadConfiguration' => 
      array (
        'classmap' => 
        array (
          0 => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'phpunit/php-code-coverage' => 
    array (
      'state' => 'active',
      'packageKey' => 'phpunit.phpcodecoverage',
      'packagePath' => 'Libraries/phpunit/php-code-coverage/',
      'composerName' => 'phpunit/php-code-coverage',
      'autoloadConfiguration' => 
      array (
        'classmap' => 
        array (
          0 => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'phpunit/php-timer' => 
    array (
      'state' => 'active',
      'packageKey' => 'phpunit.phptimer',
      'packagePath' => 'Libraries/phpunit/php-timer/',
      'composerName' => 'phpunit/php-timer',
      'autoloadConfiguration' => 
      array (
        'classmap' => 
        array (
          0 => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'phpunit/phpunit-mock-objects' => 
    array (
      'state' => 'active',
      'packageKey' => 'phpunit.phpunitmockobjects',
      'packagePath' => 'Libraries/phpunit/phpunit-mock-objects/',
      'composerName' => 'phpunit/phpunit-mock-objects',
      'autoloadConfiguration' => 
      array (
        'classmap' => 
        array (
          0 => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'state' => 'active',
      'packageKey' => 'phpdocumentor.reflectioncommon',
      'packagePath' => 'Libraries/phpdocumentor/reflection-common/',
      'composerName' => 'phpdocumentor/reflection-common',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'phpDocumentor\\Reflection\\' => 
          array (
            0 => 'src',
          ),
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'state' => 'active',
      'packageKey' => 'phpdocumentor.typeresolver',
      'packagePath' => 'Libraries/phpdocumentor/type-resolver/',
      'composerName' => 'phpdocumentor/type-resolver',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'phpDocumentor\\Reflection\\' => 
          array (
            0 => 'src/',
          ),
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'state' => 'active',
      'packageKey' => 'phpdocumentor.reflectiondocblock',
      'packagePath' => 'Libraries/phpdocumentor/reflection-docblock/',
      'composerName' => 'phpdocumentor/reflection-docblock',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'phpDocumentor\\Reflection\\' => 
          array (
            0 => 'src/',
          ),
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'phpspec/prophecy' => 
    array (
      'state' => 'active',
      'packageKey' => 'phpspec.prophecy',
      'packagePath' => 'Libraries/phpspec/prophecy/',
      'composerName' => 'phpspec/prophecy',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'Prophecy\\' => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'myclabs/deep-copy' => 
    array (
      'state' => 'active',
      'packageKey' => 'myclabs.deepcopy',
      'packagePath' => 'Libraries/myclabs/deep-copy/',
      'composerName' => 'myclabs/deep-copy',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'DeepCopy\\' => 'src/DeepCopy/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'phpunit/phpunit' => 
    array (
      'state' => 'active',
      'packageKey' => 'phpunit.phpunit',
      'packagePath' => 'Libraries/phpunit/phpunit/',
      'composerName' => 'phpunit/phpunit',
      'autoloadConfiguration' => 
      array (
        'classmap' => 
        array (
          0 => 'src/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'mikey179/vfsStream' => 
    array (
      'state' => 'active',
      'packageKey' => 'org.bovigo.vfs',
      'packagePath' => 'Libraries/mikey179/vfsStream/',
      'composerName' => 'mikey179/vfsStream',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'org\\bovigo\\vfs\\' => 'src/main/php',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'imagine/imagine' => 
    array (
      'state' => 'active',
      'packageKey' => 'imagine.imagine',
      'packagePath' => 'Libraries/imagine/imagine/',
      'composerName' => 'imagine/imagine',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'Imagine' => 'lib/',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'typo3/neos-seo' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.Neos.Seo',
      'packagePath' => 'Application/TYPO3.Neos.Seo/',
      'composerName' => 'typo3/neos-seo',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Neos\\Seo' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'neos/demo' => 
    array (
      'state' => 'inactive',
      'packageKey' => 'Neos.Demo',
      'packagePath' => 'Inactive/Sites/Neos.Demo/',
      'composerName' => 'neos/demo',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'Neos\\Demo' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'typo3/kickstart' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.Kickstart',
      'packagePath' => 'Framework/TYPO3.Kickstart/',
      'composerName' => 'typo3/kickstart',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Kickstart' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
        'className' => 'TYPO3\\Kickstart\\Package',
        'pathAndFilename' => 'Classes/TYPO3/Kickstart/Package.php',
      ),
    ),
    'flowpack/behat' => 
    array (
      'state' => 'active',
      'packageKey' => 'Flowpack.Behat',
      'packagePath' => 'Application/Flowpack.Behat/',
      'composerName' => 'flowpack/behat',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'Flowpack\\Behat' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'neos/redirecthandler' => 
    array (
      'state' => 'active',
      'packageKey' => 'Neos.RedirectHandler',
      'packagePath' => 'Application/Neos.RedirectHandler/',
      'composerName' => 'neos/redirecthandler',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Neos\\RedirectHandler\\' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'neos/redirecthandler-databasestorage' => 
    array (
      'state' => 'active',
      'packageKey' => 'Neos.RedirectHandler.DatabaseStorage',
      'packagePath' => 'Application/Neos.RedirectHandler.DatabaseStorage/',
      'composerName' => 'neos/redirecthandler-databasestorage',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Neos\\RedirectHandler\\DatabaseStorage\\' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
    'neos/redirecthandler-neosadapter' => 
    array (
      'state' => 'active',
      'packageKey' => 'Neos.RedirectHandler.NeosAdapter',
      'packagePath' => 'Application/Neos.RedirectHandler.NeosAdapter/',
      'composerName' => 'neos/redirecthandler-neosadapter',
      'autoloadConfiguration' => 
      array (
        'psr-4' => 
        array (
          'Neos\\RedirectHandler\\NeosAdapter\\' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
        'className' => 'Neos\\RedirectHandler\\NeosAdapter\\Package',
        'pathAndFilename' => 'Classes/Package.php',
      ),
    ),
    'typo3/neos-kickstarter' => 
    array (
      'state' => 'active',
      'packageKey' => 'TYPO3.Neos.Kickstarter',
      'packagePath' => 'Application/TYPO3.Neos.Kickstarter/',
      'composerName' => 'typo3/neos-kickstarter',
      'autoloadConfiguration' => 
      array (
        'psr-0' => 
        array (
          'TYPO3\\Neos\\Kickstarter' => 'Classes',
        ),
      ),
      'packageClassInformation' => 
      array (
      ),
    ),
  ),
  'version' => 6,
);